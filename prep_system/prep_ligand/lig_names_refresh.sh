#!/bin/bash

# lig_names_refresh.sh
# The purpose of this script is to update any ligands that may be missing from lig_names.txt

## USAGE EXAMPLE: 

# ALGORITHM:
#   - Load lig_names.txt file
#   - Loop through the available ligands
#   - Find if the ligand is not there --- if so, extract the details

# Created on: 04/19/2018
# Written by: Alex K. Chew (alexkchew@gmail.com)

## LOADING BASH SCRIPT
source "../../bin/nanoparticle_rc.sh"

## STORING PREVIOUS IFS (separator)
OLDIFS="$IFS"

## PRINTING 
echo -e "\n ----- lig_names_refresh.sh ----- \n"

## STORING IFS
OLDIFS="$IFS"

######################################
### DEFINING IMPORTANT DIRECTORIES ###
######################################

## DEFINING LIGAND DIRECTORY
ligand_dir="final_ligands"

## DEFINING TEXT FILE
ligand_txt="lig_names.txt"

#################################
### ANALYZING THE LIGAND FILE ###
#################################

## READING THE LIGAND NAME FILE
## READING THE FILE WITHOUT COMMENTS AND SPACES
read_file="$(grep -v -e '^$' "${ligand_txt}" | grep -v -e '^;')"

### READING FILES AS ARRAYS
readarray -t l_lig_names <<< "$(echo "${read_file}" | awk -F', ' '{print $1}')"
readarray -t l_res_names <<< "$(echo "${read_file}" | awk -F', ' '{print $2}')"

##################################
### ANALYZING LIGAND DIRECTORY ###
##################################

### GETTING LIGAND NAMES (WITHOUT PARENT DIRECTORY)
readarray -t l_dir_lig_names <<< "$(ls ${ligand_dir} | xargs -n 1 basename)"

## CREATING ARRAY TO STORE WHICH LIGANDS WERE ADDED
store_ligand_names=()

### LOOPING THROUGH EACH LIGAND
for each_ligand in ${l_dir_lig_names[@]}; do
    ## GETTING LIGAND WITHOUT THE .LIG FLAGA
    ligand_nolig=${each_ligand%.lig}
    ## PRINTING
    # echo "Checking ligand: ${ligand_nolig}"
    ## CHECKING IF LIGAND IS WITHIN LIGAND TEXT
    if [[ " ${l_lig_names[@]} " =~ " ${each_ligand} " ]]; then
        ## LIGAND IS IN THIS LIST
        echo "${each_ligand} is in the list, not doing anything further!"
    else
        echo "${each_ligand} is not in the list!"
        ## READING LIGAND FILE
        itp_resname=$(extract_itp_resname "${ligand_dir}/${each_ligand}/${each_ligand}.itp")
        echo "Molecule name: ${each_ligand}"
        echo "Residue name: ${itp_resname}"
        ## ADDING TO LIGAND FILE
        echo "${each_ligand}, ${itp_resname}" >> "${ligand_txt}"
        ## ADDING TO STORAGE
        store_ligand_names+="${each_ligand} "
    fi
done

echo -e "\n~~~~~ SUMMARY ~~~~~"
echo "Added the following ligands to ${ligand_txt}: ${store_ligand_names[@]}"
