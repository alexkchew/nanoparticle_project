prep_ligand

This folder contains energy minimization procedures. The goal is to simply energy minimize incoming ligands to ensure stability for subsequent simulations.

Written by: Alex K. Chew (02/01/2018)

Directories:
	input_files: contains files to run energy minimization
	minimized_structures: contains minimized ligand structures
	final_ligands: final ligand structure as a .gro, .itp, and .prm
