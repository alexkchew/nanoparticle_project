prep_system

This folder contains all preparation files for ligands, gold core, etc.

Written by: Alex K. Chew (05/01/2018)

Directories:
	prep_ligand: contains all preparation tools for ligands (energy minimization, etc.)
	prep_gold: contains all simulations for preparating the gold core
	