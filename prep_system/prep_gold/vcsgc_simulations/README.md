These files represent the LAMMPS code that should be compiled to enable the variance-constrained semi-grand canonical ensemble method of 
Rahm and Erhart, Nano Letters 2017. 

This fix runs Monte Carlo steps in which atoms are inserted or removed from the system without maintaining a constant system composition. Rather than penalize insertions/removals by a chemical potential (as in the semi-grand or grand canonical ensembles), this method instead provides a penalty based on the total lattice occupancy assuming a finite number of sites can be occupied. One of the key assumptions is that a "vacancy" atom type is always one of the types that can be inserted or removed, such that inserting a vacancy in place of an atom decreases the occupancy of the lattice. The parameters phi and kappa control the occupancy penalty, such that the total lattice occupancy (and therefore nanoparticle size) can be varied continuously. Note that this is based on the fix atom/swap class; I removed one line that could potentially be important, in that I removed call to modify->end_of_step() during the energy_full() call. This could affect fixes that modify the energy at the end of each step. I removed this so that fix print, which prints to a file after each step, does not get called for every MC move. Finally, also note that the code forces the rebuild of neighbor lists for each MC step so that energies are properly computed for swaps for systems in which independent neighbor lists are built for different atom types.

fix_vcsgc.cpp and fix_vcsgc.h and other LAMMPS source code can be placed directly in the /src directory of a LAMMPS distribution built with the MC package installed. Usage is:

fix ID group-ID vcsgc nevery ncycle seed temp phi kappa vacancy_type 

nevery = how often (in timesteps) this fix should be run; nevery=1 runs every timestep.
ncycle = how many MC steps to take per time step
seed = random number seed to use during MC
temp = temperature to use for Boltzmann calculation during MC
phi = phi as defined by Erhart. Should be bounded between 0.001 and -2.0; small values prefer small NPs, large negative values prefer large NPs.
kappa = kappa as defined by Erhart. Provides energetic penalty that controls the variance of the energy.
vacancy_type = the type that is assigned to vacancies in LAMMPS. 

Other keywords include:
type N M Q ...

Each type listed refers to an atom that can be inserted/removed from the system. The type corresponding to vacancies should not be included.

ke yes or no - this affects kinetic energies, but since our system does not have moving atoms the flag does not matter.

To help visualize the system, a new xyzu dump command is added. This is identical to xyz except it outputs only a single atomtype in the first column and prints a numeric atom type in the last column. This allows the last column to be read into VMD (via the load_user command) to color-code atoms by their type in each timestep. This workaround is necessary because VMD cannot handle time-dependent changes in atom type. If coordination numbers are output, then vacancies will have user=0 and all other atoms have user=coordination number. If coordination numbers are not output, then vacancies will have user=2 and atoms will have user=1. Note that coordination numbers are only output by the analyze_vcsgc script (see below).

The LAMMPS script initializes VCSGC simulations using one of two initial starting configurations: either a small number of atoms are created in an initially spherical configuration, or a single configuration is loaded in from a previous xyz file using the "read dump" command. To facilitate reading xyz data with types in the user field, I created a new "reader" derived class based on reader_xyz that correctly reads atom type information. 

The output from the VCSGC simulation is a xyz file containing structures and a dump file that outputs the potential energy and number of atoms for each phi value.This data is then post-processed using the "analyze_vcsgc" python script. This script approximates the diameter of each configuration as the maximum distance between any pair of atoms. It can also replace the user column of the xyz file with the coordination number of each atom if the appropriate flag is set; this is helpful for visualizing facets. Finally, you can also specify a range of particle diameters as command line input - the algorithm picks out the structure (or structures) that has a diameter within this range 
and minimizes the potential energy. The idea of this is to find the actual NP diameter of interest for further ligand binding, etc.

File list/descriptions:

- analyze_vcsgc.py: script written in Python3 to analyze the output of vcsgc sims. It is run at the end of the LAMMPS workflow.
- Au-Grochola-JCP05.eam.alloy: Tabulated Embedded Atom Potential used to model Au-Au interactions, from Grochola, Russo, and Snook, JCP 2005, 123, 204719.
- dump_xyz_user.cpp/.h: C++ source and header files for new LAMMPS dump (i.e., output) method, that writes a XYZ file + atom type in the user field.
- execute_lammps.sh: Script to run LAMMPS with VCSGC parameters and analyze results automatically.
- fix_vcsgc.cpp/.h: C++ source and header files for LAMMPS fix that implements VCSGC method. Based on fix_atom_swap.cpp included as part of normal LAMMPS distribution.
- LAMMPS_developer_guide.pdf: Helpful code resource for interpreting function calls in fix_vcsgc.cpp
- lmp_rvl_execute: compiled version of LAMMPS with the modified files (based on Aug 11, 2017 version of LAMMPS).
- load_user.vmd: VMD proc that can be called to load user data in from a XYZ file output using dump_xyz_user. Note that this is pretty slow.
- make_lammps_script.sh: Script to compile LAMMPS with needed packages.
- reader_xyz_user.cpp/.h: Files to read in XYZ files generated using the dump_xyz_user command. Used for starting from previously generated configurations.
- run_vcsgc.lmpin: Script to run VCGSC simulations using LAMMPS, including options for loading from file.
- lammps-stable-Aug17.tar.gz: Tarball with LAMMPS distribution to which the other cpp/h files were added.

POTENTIAL IMPROVEMENTS:

Suggested improvements to this workflow include:

1) Centering each particle on the center of the lattice before advancing to the next value of phi. Sometimes particles when growing begin to impinge on the boundaries of
   the sphere of vacancies, leading to surface energies that may not be optimal. It could be possible to recenter the particle by forcing swaps between a series of atoms/vacancies.
2) Improve facet visualization in VMD - visualizing by coordination number helps identify key features, but it may be possible to more explicitly identify 111 / 110 / 100 facets
   based on known nearest-neighbor distances, for example. 
3) Add energy minimization for each value of phi - the energies computed right now assume that the lattice is identical for any arrangement of particles. However, it's likely that the lattice
   would deform away from its bulk structure for small particles, potentially affecting energetics. Adding an energy minimization step for each set of atoms output could give more
   realistic particle structures and energies. If included as part of the main loop, however, the bulk lattice should likely be restored by testing the next value of phi. This procedure
   is implemented in the Erhart paper. Alternatively, you could just perform a minimization of the "desired" particle diameter.
4) Output coordination numbers for "desired" diameter - right now the analyze_vcsgc script only outputs coordination numbers for an entire xyz trajectory, but it should be straightforward
   to modify the script to output this for the desired particle configuration as well.
5) Test other EAM potentials - the Grochola potential seems to work reasonably well, but is a bit older now (2005). I tried the Marchal potential used by Erhart, but for whatever reason it is unstable for pure Au particles, even though the actual energies I measure match their literature. It may be a problem with how I tabulated the potential, which was done using the "atsim" package. Other EAM potentials may do a better job of capturing icosahedra at low atom numbers. 
6) Improve efficiency with neighbor list settings - in the current workflow, neighbor lists are regenerated for every attempted atom swap since the cutoff between the non-interacting LJ potential   used for vacancy-vacancy and vacancy-atom interactions is different from the EAM potential. This was implemented because using a single EAM potential and simply ignoring interactions with vacancies using the "pair_coeff none" setting does not allow active sites to be calculated, since vacancies are not included in the neighbor lists at all. However, neighbor list generation is likely very slow, and there could be settings that improve how this is treated.
7) Improve load_user efficiency - the load_user VMD procedure is very slow, since VMD (and really Tcl/Tk) is very slow at handling file input. An alternative would be to build a new plugin to read  xyz files with user fields populated, and then recompile VMD. I had done this a very long time ago and it's probably not worthwhile, but is possible.
8) Add additional surface relaxation steps to algorithm - the biggest restriction of the VCSGC algorithm is that the system is fixed in a FCC lattice at all times. This may not be realistic for small particles, and may be a contributing reason why "magic numbers" of icosahedra are missed (even though Erhart et al claim to get them, which may reflect a difference in EAM potential). It may be possible to further improve the algorithm by adding steps that allow the existing atoms to be displaced, for example, to reach energy minimizing configurations. Again, the issue here is displacing atoms while also moving vacancies to realistic positions, so some careful thought would have to go into implementation.