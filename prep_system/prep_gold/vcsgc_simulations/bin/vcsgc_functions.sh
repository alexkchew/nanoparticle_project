#!/bin/bash

# vcsgc_functions.sh
# This script contains all global functions
# Created on: 03/22/2018
# Author(s):
#   Alex K. Chew (alexkchew@gmail.com)

### FUNCTION LIST
# check_output_dir: Checks output directory
# run_vcsgc_extract: runs vcsgc extract python script
# seed_random_num: Finds random number


### FUNCTION TO CHECK IF OUTPUT DIRECTORY EXISTS -- IF SO, DELETE
# $1: OUTPUT DIRECTORY
function check_output_dir () {
if [ -e "$1" ];
	then
        echo "Removing duplicate output directory: $1"
        echo "Deleting..... pause 5 seconds in case you want to cancel"
        sleep 5
		rm -rfv "$1"
fi

mkdir -p "$1"
}

## USAGE: run_vcsgc_extract --dir $(pwd) --xyz vcsgc.xyz --dump vcsgc.dump --out vcsgc.pickle
function run_vcsgc_extract () {
    ## DEFINING PATH TO VCSGC_EXTRACT SCRIPT
    path_vcsgc="${HOME}/scratch/LigandBuilder/Prep_Gold_Core/vcsgc_files/scripts/analyze_vcsgc.py"
    
    ## PRINTING
    echo "Running vcsgc from: ${path_vcsgc}"
    echo "Command: python3 ${path_vcsgc} $@"
    python3 "${path_vcsgc}" "$@"

}

### FUNCTION TO RUN GENERAL VCSGC
function run_vcsgc_extract_general () {
    run_vcsgc_extract --dir $(pwd) --xyz vcsgc.xyz --dump vcsgc.dump --out vcsgc.pickle
}


### FUNCTION TO OUTPUT A RANDOM NUMBER BETWEEN 1,000,000 to 2,000,0000
## INPUTS:
#   void
## OUTPUTS:
#   random number as a integer
## USAGE: test=$(seed_random_num)
function seed_random_num () {
    ## DEFINING RANDOM NUMBER
    awk 'BEGIN {
       # seed
       srand()
         print int(1000000 + rand() * 999999)
    }'
}