#!/bin/bash

# vcsgc_global_vars.sh
# This contains all global variables
# Created on: 03/22/2018
# Author(s):
#   Alex K. Chew (alexkchew@gmail.com)

### PRE-DEFINED GLOBAL VARIABLES
# CURRENTWD: Path to current working directory
# PATH_SCRIPTS: Path to scripts
# PATH_SIM: Path to simulations
# PATH_BIN: Path to bin

#######################################
### DEFINING OTHER GLOBAL VARIABLES ###
#######################################

### INPUT FILES
PATH_INPUT="${CURRENTWD}/input_files" # Path to input_files
PATH_INPUT_ALLOY="${PATH_INPUT}/alloy" # Path to alloy files
PATH_INPUT_EXECUTE="${PATH_INPUT}/execute" # Path to executible files
PATH_INPUT_LAMMPS="${PATH_INPUT}/lammps_files" # Path to all lammps files
PATH_INPUT_RUN="${PATH_INPUT}/run_files" # Path to all run files
PATH_INPUT_SUBMISSION="${PATH_INPUT}/submission" # Path to all *.sh files
PATH_INPUT_FINAL_STRUCT="${CURRENTWD}/final_structures" # Path to all final structures

## JOB FILES
PATH_JOB_FILE="${PATH_SCRIPTS}/job_list.txt"




