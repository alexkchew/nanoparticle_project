#!/bin/bash

# vcsgc_rc.sh
# This contains all functions/global variables
# Created on: 03/22/2018
# Author(s):
#   Alex K. Chew (alexkchew@gmail.com)

## PRINTING
echo "-------- Reloading vcsgc_rc.sh --------"

## DEFINING MAIN DIRECTORIES
CURRENTWD="/home/akchew/scratch/LigandBuilder/Prep_Gold_Core/vcsgc_files"

## DEFINING PATHS
PATH_SCRIPTS="${CURRENTWD}/scripts" # Path to scripts
PATH_SIM="${CURRENTWD}/simulations" # Simulation path
PATH_BIN="${CURRENTWD}/bin" # Where this script is stored

### LOADING GLOBAL VARIABLES
source "${PATH_BIN}/vcsgc_global_vars.sh"

### LOADING GLOBAL FUNCTIONS
source "${PATH_BIN}/vcsgc_functions.sh"