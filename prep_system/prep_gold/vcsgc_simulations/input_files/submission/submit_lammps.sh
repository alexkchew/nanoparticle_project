#!/bin/bash 
# submit_lammps.sh
# This script simply submits lammps by executing it
# ADJUSTABLE VARIABLES:
#   EXECUTE_LAMMPS_SCRIPT <-- Script to run
#   JOB_NAME <-- name of the job

#SBATCH -p compute
#SBATCH -t 48:00:00
#SBATCH -J JOB_NAME
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts


####################
### RUN COMMANDS ###
####################
bash EXECUTE_LAMMPS_SCRIPT
