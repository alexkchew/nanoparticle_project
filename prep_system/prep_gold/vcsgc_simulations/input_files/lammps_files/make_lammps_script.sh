# Script for compiling LAMMPS with desired optional packages

cd ../lammps-rvl-modified/src/
make yes-kspace
make yes-manybody
make yes-molecule
make yes-mc
make yes-python

#make swarm_head
make g++_mpich
# make g++_openmpi

# removed so that I can rebuild easily
make clean-all

# mv lmp_g++_openmpi ../lmp_rvl_execute
mv lmp_g++_mpich ../lmp_rvl_execute
cd ..
cp lmp_rvl_execute ../vcsgc_combo
