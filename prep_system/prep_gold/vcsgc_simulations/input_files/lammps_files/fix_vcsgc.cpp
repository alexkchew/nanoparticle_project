/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

/* ----------------------------------------------------------------------
   Contributing authors: Written by Reid Van Lehn based on algorithm 
   published by Rahm and Erhart, Nano Letters 2017
------------------------------------------------------------------------- */

#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#include "fix_vcsgc.h"
#include "atom.h"
#include "atom_vec.h"
#include "atom_vec_hybrid.h"
#include "update.h"
#include "modify.h"
#include "fix.h"
#include "comm.h"
#include "compute.h"
#include "group.h"
#include "domain.h"
#include "region.h"
#include "random_park.h"
#include "force.h"
#include "pair.h"
#include "bond.h"
#include "angle.h"
#include "dihedral.h"
#include "improper.h"
#include "kspace.h"
#include "math_const.h"
#include "memory.h"
#include "error.h"
#include "thermo.h"
#include "output.h"
#include "neighbor.h"
#include <iostream>

using namespace std;
using namespace LAMMPS_NS;
using namespace FixConst;
using namespace MathConst;

/* ---------------------------------------------------------------------- */

FixVCSGC::FixVCSGC(LAMMPS *lmp, int narg, char **arg) :
  Fix(lmp, narg, arg),
  idregion(NULL), type_list(NULL), 
  sqrt_mass_ratio(NULL), local_swap_atom_list(NULL), 
  random_equal(NULL), random_unequal(NULL), c_pe(NULL)
{
  if (narg < 10) error->all(FLERR,"Illegal fix vcsgc command");

  dynamic_group_allow = 1;

  //I assume these are all default flags. 
  vector_flag = 1;
  size_vector = 7; //7 vector elements returned 
  global_freq = 1;
  extvector = 0;
  restart_global = 1;
  time_depend = 1;
  
  // RVL NOTE: require 2 new args for phi / kappa
  // Also note that the order of these arguments is hard-coded
  nevery = force->inumeric(FLERR,arg[3]);
  ncycles = force->inumeric(FLERR,arg[4]);
  seed = force->inumeric(FLERR,arg[5]);
  double temperature = force->numeric(FLERR,arg[6]);
  // new args here
  phi = force->numeric(FLERR,arg[7]);
  kappa = force->numeric(FLERR,arg[8]);
  //type of vacancies (which are required in this approach)
  int vactype = force->inumeric(FLERR,arg[9]);

  // calculates beta here
  beta = 1.0/(force->boltz*temperature);

  if (nevery <= 0) error->all(FLERR,"Illegal fix vcsgc command");
  if (ncycles < 0) error->all(FLERR,"Illegal fix vcsgc command");
  if (seed <= 0) error->all(FLERR,"Illegal fix vcsgc command");

  // defines maximum number of types in system
  memory->create(type_list,atom->ntypes,"vcsgc:type_list");
  // RVL Set first value of type list to be vacancy types
  type_list[0] = vactype;

  // read options from end of input line
  // need to keep this for the "types" keyword
  options(narg-10,&arg[10]);

  // random number generator, same for all procs
  random_equal = new RanPark(lmp,seed);

  // random number generator, not the same for all procs
  random_unequal = new RanPark(lmp,seed);

  // set up reneighboring
  force_reneighbor = 1;
  next_reneighbor = update->ntimestep + 1;

  // zero out counters
  nswap_attempts = 0.0;
  nswap_successes = 0.0;

  atom_swap_nmax = 0;
  // These get defined as needed
  local_swap_atom_list = NULL;

  // set comm size needed by this Fix

  if (atom->q_flag) comm_forward = 2;
  else comm_forward = 1;

}

/* ----------------------------------------------------------------------
   parse optional parameters at end of input line
------------------------------------------------------------------------- */

// RVL NOTES: took out parsing of mu and semi-grand flags, so those will be assumed. 
// Type list, region, and ke are retained. 
void FixVCSGC::options(int narg, char **arg)
{
  if (narg < 0) error->all(FLERR,"Illegal fix vcsgc command");
  regionflag = 0;
  conserve_ke_flag = 0;
  force_rebuild = 1;
  ///RVL: Default nswaptypes to 1 to account for vacancy type
  nswaptypes = 1;
  iregion = -1;

  int iarg = 0;
  while (iarg < narg) {
    if (strcmp(arg[iarg],"region") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal fix vcsgc command");
        iregion = domain->find_region(arg[iarg+1]);
      if (iregion == -1)
        error->all(FLERR,"Region ID for fix vcsgc does not exist");
      int n = strlen(arg[iarg+1]) + 1;
      idregion = new char[n];
      strcpy(idregion,arg[iarg+1]);
      regionflag = 1;
      iarg += 2;
    } else if (strcmp(arg[iarg],"ke") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal fix vcsgc command");
      if (strcmp(arg[iarg+1],"no") == 0) conserve_ke_flag = 0;
      else if (strcmp(arg[iarg+1],"yes") == 0) conserve_ke_flag = 1;
      else error->all(FLERR,"Illegal fix vcsgc command");
      iarg += 2;
    } else if (strcmp(arg[iarg],"rebuild") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal fix vcsgc command");
      if (strcmp(arg[iarg+1],"no") == 0) force_rebuild = 0;
      else if (strcmp(arg[iarg+1],"yes") == 0) force_rebuild = 1;
      else error->all(FLERR,"Illegal fix vcsgc command");
      iarg += 2;
    } else if (strcmp(arg[iarg],"types") == 0) {
      if (iarg+2 > narg) error->all(FLERR,"Illegal fix vcsgc command");
      iarg++;
      while (iarg < narg) {
        if (isalpha(arg[iarg][0])) break;
	      if (nswaptypes >= atom->ntypes) error->all(FLERR,"Illegal fix vcsgc command");
        type_list[nswaptypes] = force->numeric(FLERR,arg[iarg]);
        if (type_list[nswaptypes] == type_list[0]) error->all(FLERR,"Type is already assigned to vacancies");
	      nswaptypes++;
        iarg++;
      }
    } else error->all(FLERR,"Illegal fix vcsgc command");
  }
}

/* ---------------------------------------------------------------------- */

// RVL NOTES: This is just for memory clean up, keep to avoid memory leaks but remove references to mu
FixVCSGC::~FixVCSGC()
{
  memory->destroy(type_list);
  memory->destroy(sqrt_mass_ratio);
  if (regionflag) delete [] idregion;
  delete random_equal;
  delete random_unequal;
}

/* ---------------------------------------------------------------------- */

// RVL Not sure what this does
int FixVCSGC::setmask()
{
  int mask = 0;
  mask |= PRE_EXCHANGE;
  return mask;
}

/* ---------------------------------------------------------------------- */

//RVL NOTES: Assumes semi-grand flag is on
void FixVCSGC::init()
{
  char *id_pe = (char *) "thermo_pe";
  int ipe = modify->find_compute(id_pe);
  c_pe = modify->compute[ipe];

  int *type = atom->type;

  if (nswaptypes < 2)
    error->all(FLERR,"Must specify at least 1 non-vacancy type in fix vcsgc command");

  // RVL NOTE: Checks to make sure types are all valid
  for (int iswaptype = 0; iswaptype < nswaptypes; iswaptype++)
    if (type_list[iswaptype] <= 0 || type_list[iswaptype] > atom->ntypes)
      error->all(FLERR,"Invalid atom type in fix vcsgc command");

  // RVL NOTE: Defines masses so that kinetic energy can be conserved when atom types change.
  memory->create(sqrt_mass_ratio,atom->ntypes+1,atom->ntypes+1,"vcsgc:sqrt_mass_ratio");
  for (int itype = 1; itype <= atom->ntypes; itype++)
    for (int jtype = 1; jtype <= atom->ntypes; jtype++)
      sqrt_mass_ratio[itype][jtype] = sqrt(atom->mass[itype]/atom->mass[jtype]);

  // check to see if itype and jtype cutoffs are the same
  // if not, reneighboring will be needed between swaps

  double **cutsq = force->pair->cutsq;
  unequal_cutoffs = false;
  for (int iswaptype = 0; iswaptype < nswaptypes; iswaptype++)
    for (int jswaptype = 0; jswaptype < nswaptypes; jswaptype++)
      for (int ktype = 1; ktype <= atom->ntypes; ktype++)
        if (cutsq[type_list[iswaptype]][ktype] != cutsq[type_list[jswaptype]][ktype])
          unequal_cutoffs = true;

  // check that no swappable atoms are in atom->firstgroup
  // swapping such an atom might not leave firstgroup atoms first
  // RVL NOTE: Might be able to remove in future
  if (atom->firstgroup >= 0) {
    int *mask = atom->mask;
    int firstgroupbit = group->bitmask[atom->firstgroup];

    int flag = 0;
    for (int i = 0; i < atom->nlocal; i++)
      if ((mask[i] == groupbit) && (mask[i] && firstgroupbit)) flag = 1;

    int flagall;
    MPI_Allreduce(&flag,&flagall,1,MPI_INT,MPI_SUM,world);

    if (flagall)
      error->all(FLERR,"Cannot do vcsgc on atoms in atom_modify first group");
  }
}

/* ----------------------------------------------------------------------
   attempt Monte Carlo swaps
------------------------------------------------------------------------- */
// RVL NOTE: Called every timestep
void FixVCSGC::pre_exchange()
{
  // just return if should not be called on this timestep

  if (next_reneighbor != update->ntimestep) return;

  if (domain->triclinic) domain->x2lamda(atom->nlocal);
  domain->pbc();
  comm->exchange();
  comm->borders();
  if (domain->triclinic) domain->lamda2x(atom->nlocal+atom->nghost);
  if (modify->n_pre_neighbor) modify->pre_neighbor();
  neighbor->build();

  // RVL NOTE: Calculates full system energy pre-swap
  energy_stored = energy_full();

  int nsuccess = 0;
  // RVL NOTE: assumes semi-grand now - calls update to count atoms, etc. prior to attempting swap
  update_semi_grand_atoms_list();
  // Perform swap attempts ncycles times
  for (int i = 0; i < ncycles; i++) nsuccess += attempt_semi_grand();

  nswap_attempts += ncycles;
  nswap_successes += nsuccess;

  energy_full();
  // RVL NOTE: Defines next timestep at which to call this
  next_reneighbor = update->ntimestep + nevery;
}

/* ----------------------------------------------------------------------
Note: atom charges are assumed equal and so are not updated
------------------------------------------------------------------------- */

//RVL This is really where the only changes need to happen
int FixVCSGC::attempt_semi_grand()
{
  //RVL NOTE: If no swappable atoms are defined, return. Should never happen
  // since update_semi_grand_atoms_list is called before this. 
  if (nswap == 0) return 0;

  double energy_before = energy_stored;

  int itype,jtype,jswaptype;
  // RVL NOTE: Picks atom at random. Maybe can improve to define "active vacancies"?
  int i = pick_semi_grand_atom();
  /// Loop does a lot of work to randomly choose a new type. Could probably simplify this 
  /// in the event that there are only two types. 
  /// Note that pick_semi_grand_atom returns -1 for all processors other than the processor 
  /// on which the specifically chosen atom's index is stored. So execution of this function is effectively serial. 
  if (i >= 0) {
    jswaptype = static_cast<int> (nswaptypes*random_unequal->uniform());
    jtype = type_list[jswaptype];
    itype = atom->type[i];
    while (itype == jtype) {
      jswaptype = static_cast<int> (nswaptypes*random_unequal->uniform());
      jtype = type_list[jswaptype];
    }
    atom->type[i] = jtype;
  }

  // RVL NOTE: I added forced rebuilding of the neighbor list here to prevent cases where due to the definition of interactions
  // between vacancies / atoms (i.e. LJ interactions with epsilon=0.0) there could be no measurable interaction upon a change 
  // of type since the atom would still have a neighbor list associated with its old type. Rebuilding removes that issue. 
  if (unequal_cutoffs || force_rebuild) {
    if (domain->triclinic) domain->x2lamda(atom->nlocal);
    domain->pbc();
    comm->exchange();
    comm->borders();
    if (domain->triclinic) domain->lamda2x(atom->nlocal+atom->nghost);
    if (modify->n_pre_neighbor) modify->pre_neighbor();
    neighbor->build();
  } else {
    comm->forward_comm_fix(this);
  }

  if (force->kspace) force->kspace->qsum_qsq();
  double energy_after = energy_full();

  // RVL NOTE: Success criterion is redefined to remove the mu criteria and replace with 
  // the fraction of occupancy criterion. 
  int success = 0;
  if (i >= 0) {
    /// RVL NOTE: Calculate VCSGC energy here (need to type case)
    double frac_before, frac_after;
    double nvac_double = static_cast<double>(nvac);
    frac_before = 1.0-nvac_double/nswap;
    double vcsgc_before = energy_before + nswap*kappa*(frac_before + 0.5*phi)*(frac_before + 0.5*phi);
    /// RVL NOTE: Determine fraction after depending on type of swap
    /// RIght now simply assume 2 types...
    if (jtype == type_list[0]) {
      // increases number of vacancies
      frac_after = 1.0-(nvac_double+1.0)/nswap;
    } else {
      frac_after = 1.0-(nvac_double-1.0)/nswap;
    }
    double vcsgc_after = energy_after + nswap*kappa*(frac_after + 0.5*phi)*(frac_after + 0.5*phi);
    // RVL: Now run Boltzmann probability
    if (random_unequal->uniform() <
      exp(-beta*(vcsgc_after - vcsgc_before))) success = 1;
    //cout << "Atom swapped " << atom->tag[i] << " Type " << itype << " Energy diff " << vcsgc_after - vcsgc_before << " Success " << success << "\n"; 
    //for (int k=0; k<nswap; ++k) cout << "    Index " << k << " Type " << atom->type[k] << " \n";
  }
  int success_all = 0;
  MPI_Allreduce(&success,&success_all,1,MPI_INT,MPI_MAX,world);

  if (success_all) {
    update_semi_grand_atoms_list();
    energy_stored = energy_after;
    if (conserve_ke_flag) {
      if (i >= 0) {
        atom->v[i][0] *= sqrt_mass_ratio[itype][jtype];
        atom->v[i][1] *= sqrt_mass_ratio[itype][jtype];
        atom->v[i][2] *= sqrt_mass_ratio[itype][jtype];
      }
    }
    return 1;
  } else {
    // RVL NOTE: Reverts atom type back, along with neighbor lists, etc.
    if (i >= 0) {
      atom->type[i] = itype;
    }
    if (force->kspace) force->kspace->qsum_qsq();
    energy_stored = energy_before;

    if (unequal_cutoffs || force_rebuild) {
      if (domain->triclinic) domain->x2lamda(atom->nlocal);
      domain->pbc();
      comm->exchange();
      comm->borders();
      if (domain->triclinic) domain->lamda2x(atom->nlocal+atom->nghost);
      if (modify->n_pre_neighbor) modify->pre_neighbor();
      neighbor->build();
    } else {
      comm->forward_comm_fix(this);
    }
  }
  return 0;
}


/* ----------------------------------------------------------------------
   compute system potential energy
------------------------------------------------------------------------- */

double FixVCSGC::energy_full()
{
  int eflag = 1;
  int vflag = 0;

  if (modify->n_pre_neighbor) modify->pre_neighbor();
  if (modify->n_pre_force) modify->pre_force(vflag);

  if (force->pair) force->pair->compute(eflag,vflag);

  if (atom->molecular) {
    if (force->bond) force->bond->compute(eflag,vflag);
    if (force->angle) force->angle->compute(eflag,vflag);
    if (force->dihedral) force->dihedral->compute(eflag,vflag);
    if (force->improper) force->improper->compute(eflag,vflag);
  }

  if (force->kspace) force->kspace->compute(eflag,vflag);

  if (modify->n_post_force) modify->post_force(vflag);
  // REMOVED For capability with fix print, which would get called every time during an energy calculation if present.
  //if (modify->n_end_of_step) modify->end_of_step();

  update->eflag_global = update->ntimestep;
  double total_energy = c_pe->compute_scalar();

  return total_energy;
}

/* ----------------------------------------------------------------------
------------------------------------------------------------------------- */
// RVL NOTE: This picks a single atom across all processors, and returns its local ID on the processor of interest. 
int FixVCSGC::pick_semi_grand_atom()
{
  int i = -1;
  // RVL Picks random integer out of all possible swappable atoms
  int iwhichglobal = static_cast<int> (nswap*random_equal->uniform());
  // RVL If atom index is within range of indices that are "owned" by this processor, 
  // then return its actual atom id. Otherwise, return -1. 
  if ((iwhichglobal >= nswap_before) &&
      (iwhichglobal < nswap_before + nswap_local)) {
    int iwhichlocal = iwhichglobal - nswap_before;
    i = local_swap_atom_list[iwhichlocal];
  }

  return i;
}

/* ----------------------------------------------------------------------
   update the list of gas atoms
------------------------------------------------------------------------- */

void FixVCSGC::update_semi_grand_atoms_list()
{
  int nlocal = atom->nlocal; // Number of atoms owned by a given processor. This is NOT equaL to all atoms in system unless only run on a single processor. Hmm.  
  double **x = atom->x; // atom positions, needed for "region" command

  if (atom->nmax > atom_swap_nmax) {
    // RVL NOTE: Safely frees pointers, so returns if pointer is null
    // Thus, I think this is where the swap lists are first initialized
    memory->sfree(local_swap_atom_list);
    atom_swap_nmax = atom->nmax;
    local_swap_atom_list = (int *) memory->smalloc(atom_swap_nmax*sizeof(int),
     "MCSWAP:local_swap_atom_list");
  }

  nswap_local = 0;

  nvac_local = 0;
  // RVL NOTE: Assigns type to each potentially swappable atom using nswap_local as a counter
  if (regionflag) {
    for (int i = 0; i < nlocal; i++) {
      if (domain->regions[iregion]->match(x[i][0],x[i][1],x[i][2]) == 1) {
        if (atom->mask[i] & groupbit) {
      	  int itype = atom->type[i];
      	  int iswaptype;
      	  for (iswaptype = 0; iswaptype < nswaptypes; iswaptype++)
      	    if (itype == type_list[iswaptype]) break;
      	  if (iswaptype == nswaptypes) continue;
          local_swap_atom_list[nswap_local] = i;
          nswap_local++;
          if (itype == type_list[0]) nvac_local++; // vacancy type
        }
      }
    }
  } else {
    // Iterate over all local atoms (including atoms that are not necessarily swappable)
    for (int i = 0; i < nlocal; i++) {
      // RVL NOTE: Still not sure what the whole mask thing does
      if (atom->mask[i] & groupbit) {
    	  int itype = atom->type[i];
    	  int iswaptype;
        //Check if atomtype is actually in list
    	  for (iswaptype = 0; iswaptype < nswaptypes; iswaptype++)
    	    if (itype == type_list[iswaptype]) break;
    	  //Continues if we reached end of list without finding match
        if (iswaptype == nswaptypes) continue;
        //Assigns atom (whose type was found) to the swappable atom list and increments number of swappable atoms.
        local_swap_atom_list[nswap_local] = i;
        nswap_local++;
        if (itype == type_list[0]) nvac_local++; // vacancy type
        //cout << " tag " << atom->tag[i] << " type " << atom->type[i] << " \n";
        }
      }
  }
  /// Could count number of atoms vs. vacancies here

  // RVL NOTE: Here is where we count nswap by summing data from all processors
  MPI_Allreduce(&nswap_local,&nswap,1,MPI_INT,MPI_SUM,world);
  // RVL NOTE: Count total number of vacancies as well
  MPI_Allreduce(&nvac_local,&nvac,1,MPI_INT,MPI_SUM,world);
 
  // RVL NOTE: What this is does is sums up the value of nswap_local for processes 0->i and store i in n_swap_before for process i.
  // This basically allows me to know what range of values of nswap are owned by this process
  MPI_Scan(&nswap_local,&nswap_before,1,MPI_INT,MPI_SUM,world);
  // Substracting nswap_local means that nswap_before is now the index corresponding to the first of the nswap_local atoms stroed on this processor. 
  nswap_before -= nswap_local;
}

// Calculates diameter of system as the maximum pair-wise distance between particles
// FAILS FOR PARALLEL; Correct following template above (30 mins)
// Doesn't work at all?
double FixVCSGC::calculate_diameter() {
  double **x = atom->x; 
  double maxdist2=0.0;
  double curdist2;
  for (int i = 0; i < nswap; i++) {
    for (int j=i+1; j < nswap; j++) {
      // Only calculate for non-vacancies
      int itype = atom->type[i];
      int jtype = atom->type[j];
      //cout << "Types: " << itype << " " << jtype << " \n";
      int iswaptype, jswaptype;
      //Check if both atomtypes are actually in list and are not vacancies (type 0)
      for (iswaptype = 1; iswaptype < nswaptypes; iswaptype++)
        if (itype == type_list[iswaptype]) break;
      for (jswaptype = 1; jswaptype < nswaptypes; jswaptype++)
        if (jtype == type_list[jswaptype]) break;
      //Continues if we reached end of list without finding match
      if (iswaptype == nswaptypes) continue;
      if (jswaptype == nswaptypes) continue;
      curdist2 = (x[i][0]-x[j][0])*(x[i][0]-x[j][0]) + (x[i][1]-x[j][1])*(x[i][1]-x[j][1]) + (x[i][2]-x[j][2])*(x[i][2]-x[j][2]);
      //cout << "Dist: " << sqrt(curdist2) << " \n";
      if (curdist2 > maxdist2) maxdist2 = curdist2;
    }
  }
  return sqrt(maxdist2);
}


/* ---------------------------------------------------------------------- */

int FixVCSGC::pack_forward_comm(int n, int *list, double *buf, int pbc_flag, int *pbc)
{
  int i,j,m;

  int *type = atom->type;
  double *q = atom->q;

  m = 0;

  if (atom->q_flag) {
    for (i = 0; i < n; i++) {
      j = list[i];
      buf[m++] = type[j];
      buf[m++] = q[j];
    }
  } else {
    for (i = 0; i < n; i++) {
      j = list[i];
      buf[m++] = type[j];
    }
  }

  return m;
}

/* ---------------------------------------------------------------------- */

void FixVCSGC::unpack_forward_comm(int n, int first, double *buf)
{
  int i,m,last;

  int *type = atom->type;
  double *q = atom->q;

  m = 0;
  last = first + n;

  if (atom->q_flag) {
    for (i = first; i < last; i++) {
      type[i] = static_cast<int> (buf[m++]);
      q[i] = buf[m++];
    }
  } else {
    for (i = first; i < last; i++)
      type[i] = static_cast<int> (buf[m++]);
  }
}

/* ----------------------------------------------------------------------
  return acceptance ratio
------------------------------------------------------------------------- */

// RVL NOTE: Values returned from the f_*[0] and f_*[1] variable references. 
double FixVCSGC::compute_vector(int n)
{
  if (n == 0) return nswap_attempts;
  if (n == 1) return nswap_successes;
  if (n == 2) {
    if (nswap_attempts > 0) return (double)nswap_successes/nswap_attempts;
    else return 0;
  }
  if (n == 3) return nvac;
  if (n == 4) return nswap;
  if (n == 5) {
    if (nswap > 0) return (double)nvac/nswap;
    else return 0;
  }
  if (n == 6) return calculate_diameter();
  return 0.0;
}

/* ----------------------------------------------------------------------
   memory usage of local atom-based arrays
------------------------------------------------------------------------- */

double FixVCSGC::memory_usage()
{
  double bytes = atom_swap_nmax * sizeof(int);
  return bytes;
}

/* ----------------------------------------------------------------------
   pack entire state of Fix into one write
------------------------------------------------------------------------- */

void FixVCSGC::write_restart(FILE *fp)
{
  int n = 0;
  double list[4];
  list[n++] = random_equal->state();
  list[n++] = random_unequal->state();
  list[n++] = next_reneighbor;

  if (comm->me == 0) {
    int size = n * sizeof(double);
    fwrite(&size,sizeof(int),1,fp);
    fwrite(list,sizeof(double),n,fp);
  }
}

/* ----------------------------------------------------------------------
   use state info from restart file to restart the Fix
------------------------------------------------------------------------- */

void FixVCSGC::restart(char *buf)
{
  int n = 0;
  double *list = (double *) buf;

  seed = static_cast<int> (list[n++]);
  random_equal->reset(seed);

  seed = static_cast<int> (list[n++]);
  random_unequal->reset(seed);

  next_reneighbor = static_cast<int> (list[n++]);
}
