#!/bin/bash

# execute_lammps.sh
# Script for running full LAMMPS workflow
# Runs VCSGC simulations to scan for particle diameters
# The analysis tool finds the energy minimiizing particle diameter(s) within
# the specified range and outputs them

## ADJUSTABLE VARAIBLES
#   EXECUTE_FILE <-- executible file
#   RUN_FILE <-- equivalent to mdp file for gromacs in lammps format
#   PREFIX <-- prefix that would be outputted
#   LOG_NAME <-- name of the log file
#   SEED <-- number that is used a seed for generating random sims

### LOADING BASHRC FOR THIS TYPE
source "${HOME}/scratch/LigandBuilder/Prep_Gold_Core/vcsgc_files/bin/vcsgc_rc.sh"

##### PARAMETERS #####

# output file names
executible_file="EXECUTE_FILE"
run_file="RUN_FILE"
lammps_output_prefix="PREFIX"
log_name="LOG_NAME"

# mpi cores to use 
num_cores=28

# random seed to use - MUST CHANGE to obtain different LAMMPS results on different runs
seed="SEED" # 1012424

##### LAMMPS EXECUTION #####
# Remember to adjust script to set flags for initial phi values, reading data from files/creating new lattice, etc.
# It's critical to supply the random seed here

mpirun -np ${num_cores} "$(pwd)/${executible_file}" -in "${run_file}" -var prefix ${lammps_output_prefix} -var seed ${seed} -log "${log_name}"

##### ANALYSIS
# Remember to adjust script to set flags for outputting coordination number, particle diameter of interest, etc. if desired
# particle diameter to search for (in angstroms) 

run_vcsgc_extract --dir $(pwd) --xyz ${lammps_output_prefix}.xyz --dump ${lammps_output_prefix}.dump --out ${lammps_output_prefix}.pickle

##### TODO:
# Add minimization step here for extracted file
# Add coordination numbers to selected output frames (adjust python script)
