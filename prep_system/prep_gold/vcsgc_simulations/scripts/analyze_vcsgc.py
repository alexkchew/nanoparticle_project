"""
analyze_vcsgc.py
Analyzes output xyz_files from the Variance-constrained semi-grand canonical ensemble
simulations to compute particle size and identify lowest-energy particle within
a desired size range. Can also output coordination number of each atom in "user" field
to assist visualization of facets / symmetry in VMD.

Created on: 02/02/2018

DEFINITIONS:
    calc_xyz_dist_matrix: calculates the xyz distance matrix given the coordinates
    calc_total_distance2_matrix: calculates the total distances^2 given the coordinates
    color_y_axis: changes color of y-axis for plotting
    load_pickle_vcsgc: loads pickles for vcsgc
    
CLASSES:
    read_vcsgc_xyz: reads vcsgc xyz outputs
    read_vcsgc_dump: reads vcsgc dump outputs
    extract_vcsgc_xyz: extraction of the outputs by math manipulation (i.e. calculating diameters, etc.)
    plot_vcsgc: plots all vcsgc results
    export_vcsgc: exports vcsgc results to a pickle

AUTHOR(S):
    Reid C. Van Lehn
    Alex K. Chew
    
UPDATES:
    2018-03-04: Major update -- changing everything to classes
"""
## IMPORTING MODULES
import numpy as np
import sys
import pickle
from MDBuilders.applications.nanoparticle.read_tools import read_vcsgc_xyz
from MDBuilders.core.import_tools import load_and_clean_text_file

## PLOTTING MODULES
if sys.prefix != '/usr':
    import matplotlib.pyplot as plt
    from mpl_toolkits.mplot3d import Axes3D # For 3D axes

## DEFINING GLOBAL PLOT PROPERTIES
LABELS = {
            'fontname': "Arial",
            'fontsize': 16
            }
## DEFINING LINE STYLE
LINE_STYLE={
            "linewidth": 1.6, # width of lines
            }

## DEFINING ATOMIC DETAILS
ATOM_PLOT_INFO = {
        'marker':'o',
        'edgecolor':'black',
        'linewidth':1, # 1.5
        }

########################
### GLOBAL VARIABLES ###
########################
GOLD_LATTICE_CONSTANT=0.40788 # nm
FCC_GOLD_NEAREST_NEIGHBOR=GOLD_LATTICE_CONSTANT*np.sqrt(2)/2.0
FCC_GOLD_NEAREST_NEIGHBOR_APPROX_SQUARED_ANGS=np.round((FCC_GOLD_NEAREST_NEIGHBOR*10)**2,decimals=2)

###################
### DEFINITIONS ###
###################
### FUNCTION TO GET THE X, Y, AND Z DISTANCE MATRIX GIVEN THE COORDINATES
def calc_xyz_dist_matrix(coordinates):
    '''
    The purpose of this script is to take coordinates and find the difference between i and j
    INPUTS:
        Coordinates - Numpy array
    OUTPUTS:
        deltaXarray - Array for x-differences (j-i)
        deltaYarray - Array for y-differences (j-i)
        deltaZarray - Array for z-differences (j-i)
    '''    
    def makeDistArray(Vector):
        '''
        This script simply takes a vector of x's, y's, or z's and creates a distance matrix for them
        INPUTS:
            Vector - A list of x coordinates for example
        OUTPUTS:
            Array - Distance matrix j - i type
        '''
        vectorSize = len(Vector)
        
        # Start by creating a blank matrix
        myArray = np.zeros( (vectorSize, vectorSize ) )
        
        # Use for-loop to input into array and find distances
        for i in range(0,vectorSize-1):
            for j in range(i,vectorSize):
                myArray[i,j] = Vector[j]-Vector[i]
        
        return myArray # - myArray.T

    deltaXarray = makeDistArray(coordinates.T[0])  # X-values
    deltaYarray = makeDistArray(coordinates.T[1])  # Y-values
    deltaZarray = makeDistArray(coordinates.T[2])  # Z-values
    return deltaXarray, deltaYarray, deltaZarray


### FUNCTION TO CALCULATE DISTANCE BETWEEN PAIRS (TAKEN FROM MD.TRAJ)
def calc_dist2_btn_pairs(coordinates, pairs):
    """        
    Distance squared between pairs of points in each coordinate
    INPUTS:
        coordinates: N x 3 numpy array
        pairs: M x 2 numpy array, which are pairs of atoms
    OUTPUTS:
        distances: distances in the form of a M x 1 array
    """
    delta = np.diff(coordinates[pairs], axis=1)[:, 0]
    return (delta ** 2.).sum(-1) # ** 0.5 

### FUNCTION TO CALCULATE TOTAL DISTANCE MATRIX
def calc_total_distance2_matrix(coordinates, force_vectorization=False, atom_threshold=2000):
    '''
    This function calls for calc_xyz_dist_matrix and simply uses its outputs to calculate a total distance matrix that is squared
    INPUTS:
        coordinates: numpy array (n x 3)
        force_vectorization: If True, it will force vectorization every time
        atom_threshold: threshold of atoms, if larger than this, we will use for loops for vectorization
    OUTPUTS:
        dist2: distance matrix (N x N)
    '''
    ## FINDING TOTAL LENGTH OF COORDINATES
    total_atoms = len(coordinates)
    
    ## SEEING IF WE NEED TO USE LOOPS FOR TOTAL DISTANCE MATRIX
    if total_atoms < atom_threshold:
        num_split=0
    else:
        num_split = int(np.ceil(total_atoms / atom_threshold))
        
    if num_split == 0 or force_vectorization is True:
        deltaXarray, deltaYarray, deltaZarray = calc_xyz_dist_matrix(coordinates)
        # Finding total distance^2
        dist2 = deltaXarray*deltaXarray + deltaYarray*deltaYarray + deltaZarray*deltaZarray
    else:
        print("Since number of atoms > %s, we are shortening the atom list to prevent memory error!"%(atom_threshold))
        ## SPLITTING ATOM LIST BASED ON THE SPLITTING
        atom_list = np.array_split( np.arange(total_atoms), num_split )
        ## CREATING EMPTY ARRAY
        dist2 = np.zeros((total_atoms, total_atoms))
        total_atoms_done = 0
        ## LOOPING THROUGH EACH ATOM LIST
        for index, current_atom_list in enumerate(atom_list):
            ## FINDING MAX AN MINS OF ATOM LIST
            atom_range =[ current_atom_list[0], current_atom_list[-1] ]
            ## FINDING CURRENT TOTAL ATOMS
            current_total_atoms = len(current_atom_list)
            ## PRINTING
            print("--> WORKING ON %d ATOMS, ATOMS LEFT: %d"%(current_total_atoms, total_atoms - total_atoms_done) )
            ## GETTING ATOM PAIRS
            pairs = np.array([[y, x] for y in range(atom_range[0], atom_range[1]+1 ) for x in range(y+1, total_atoms) ] )
            ## CALCULATING DISTANCES
            current_distances = calc_dist2_btn_pairs(coordinates, pairs)
            ## ADDING TO TOTAL ATOMS
            total_atoms_done += current_total_atoms
            ## STORING DISTANCES
            dist2[pairs[:,0],pairs[:,1]] = current_distances
            
            ##TODO: inclusion of total time
            ##TODO: optimization of atom distances
    return dist2

### FUNCTION TO CHANGE THE COLOR OF Y AXIS
def color_y_axis(ax, color):
    """Color your axes."""
    for t in ax.get_yticklabels():
        t.set_color(color)
    return None


### FUNCTION TO PLOT THE NON-VACANT SITES IN COLOR
def plot_nonvacant_color(ax, geometry, coord_num):
    '''
    The purpose of this function is to plot the nonvacant sites in color
    INPUTS:
        self: class object
        ax: axis of the plot
        geometry: geometry from extract_xyz_file.geometry_nonvacant
        coord_num: coordination number lists from extract_xyz_file.coord_num
    OUTPUTS:
        ax: returns axis of the plot
        scatter_plot_storage: handles to the scatter plots as a list
    '''
    ### FUNCTION TO GIVE A COLOR FOR SPECIFIC COORDINATION NUMBERS
    def color_coordination_nums(coord_num):
        '''
        The purpose of this function is to output a color given a coordination number
        INPUTS:
            coord_num: [int] integer value of the number of atoms coordinated
        OUTPUTS:
            coord_color: [string] string of a color for matplotlib
        '''
        coord_color_dict = {
                                '4':'b',
                                '5':'g',
                                '6':'r',
                                '7':'c',
                                '8':'m',
                                '9':'y',
                                }
        
        if coord_num >= 4 and coord_num <= 9:
            coord_color = coord_color_dict[str(coord_num)]
        elif coord_num < 4:
            coord_color = 'gray'
        else:
            coord_color = 'black'
        return coord_color
    
    #### MAIN SCRIPT ####
    ## FINDING UNIQUE COORDINATION NUMBERS
    unique_coord_nums= np.unique(coord_num[:,1])[::-1]
    # RETURNS: numpy array of unique coordination numbers from largest to smallest (e.g. array([12,  6,  5,  4], dtype=int64))
    
    ## APPENDING SCATTER PLOTS
    scatter_plot_storage = []
    
    ## DEFINING ALPHA VALUES
    alpha_values = np.linspace(1.0, 0.7, len(unique_coord_nums) )
    
    ## LOOPING THROUGH EACH COORDINATION NUMBER
    for index, each_coord_num in enumerate(unique_coord_nums):
        # print("PLOTTING COORDINATION NUMBER: %s"%(each_coord_num))
        ## FINDING THE COLOR
        current_coord_num_color = color_coordination_nums(each_coord_num)
        ## FINDING ALL THE INDEXES WITH THIS COORDINATION NUMBER
        index_with_coord = np.where(coord_num[:,1]==each_coord_num)
        ## PLOTTING GEOMETRY            
        scatter_plot = ax.scatter( geometry[index_with_coord,0], geometry[index_with_coord,1], geometry[index_with_coord,2], 
                   color=current_coord_num_color, alpha=alpha_values[index], s = 150, label = str(each_coord_num), **ATOM_PLOT_INFO )
        ## STORING SCATTER PLOT
        scatter_plot_storage.append(scatter_plot)

    return ax, scatter_plot_storage

### FUNCTION TO PLOT GEOMETRY FOR A FRAME
def plot_structure_for_frame(vacant_geometry, nonvacant_geometry, nonvacant_coord_num, iteration=0, diameter=None,
                              phi_value=None, potential_energy = None, fig = None, ax = None, scatter_plot_storage = None, title=''):
    '''
    The purpose of this function is to plot the atoms for a single iteration
    INPUTS:
        ## ESSENTIALS:
            vacant_geometry: [np.array, shape=(num_vacant,3)] geometry of all vacant sites
            nonvacant_geometry: [np.array, shape=(num_nonvacant,3)] geometry of all nonvacant sites
            nonvacant_coord_num: [np.array, shape=(num_nonvacant,1)] coordination number of each atom in nonvacant site
            
        ## TEXT ON THE PLOT INPUTS
            iteration: (int) iteration you are interested in
            diameter: (float) diameter of the nonvacant lattice
            phi_value: (float) phi value of the iteration
            potential_energy: (float) potential energy of the iteraion
        ## FIGURE DETAILS
            fig: figure you want to add into [default: None]
            ax: axis you want to add to [default: None]
            title: title of the figure
    OUTPUTS:
        fig, ax: figure and axis of the plot
    '''
    ## CREATING FIGURE
    if fig is None or ax is None:
        fig = plt.figure(); ax = fig.add_subplot(111, projection='3d', aspect='equal')
        ## SETTING LABELS
        ax.set_xlabel('x (Angs)')
        ax.set_ylabel('y (Angs)')
        ax.set_zlabel('z (Angs)')
        
        ## PLOTTING VACANT SITES
        ax.scatter( vacant_geometry[:,0], vacant_geometry[:,1], vacant_geometry[:,2], alpha = 0.02, s = 3, color='gray', **ATOM_PLOT_INFO )
        
    if scatter_plot_storage is not None:
        ## CLEAR PLOT
        [each_plot.remove() for each_plot in scatter_plot_storage]
    
    ## QUICK CALCULATIONS
    num_nonvacant_atoms = len(nonvacant_geometry)
    
    ## WRITING ON THE PLOT
    text = title  + '\n'
    text += 'Current iteration: %d\n'%(iteration)
    text +='Number of atoms: %d\n'%(num_nonvacant_atoms)
    text +='Diameter: %.2f Angs\n'%(diameter)
    text +='Phi value: %.2f\n'%(phi_value)
    text +='Potential energy: %.2f\n'%(potential_energy)
    ax.set_title(text, loc='left')
    ## PLOTTING NON-VACANT SITES 
    ax, scatter_plot_storage = plot_nonvacant_color(ax, nonvacant_geometry, nonvacant_coord_num)
    # ax.scatter( nonvacant_geometry[:,0], nonvacant_geometry[:,1], nonvacant_geometry[:,2], color='orange', 
      #         alpha=1.00, s = 150, **ATOM_PLOT_INFO )
    ## SHOWING LEGEND
    
    ax.legend()
    return fig, ax, scatter_plot_storage

#########################################
### CLASS TO EXTRACT VCSGC DUMP FILES ###
#########################################
class read_vcsgc_dump:
    '''
    The purpose of this class is to read the dump file for vcsgc
    INPUTS:
        input_dump_file: full path to dump file for vcsgc
    OUTPUTS:
        self.input_dump_file: file path to dump file
        self.dump_file_lines: (list) list of strings of the dump file
        self.extract_data: (dict) contains all extracted information in a form of a dictionary
            'phi': numpy array of all phi values, shape=(num_iter, 1)
            'num_atoms': numpy array of number of atoms, shape=(num_iter, 1)
            'potential_energy': numpy array ofpotential energy, shape=(num_iter, 1)
    FUNCTIONS:
        extract_lines: extracts the lines of the VCSGC
    '''
    ### INITIALIZING
    def __init__(self, input_dump_file, ):
        ### STORING
        self.input_dump_file = input_dump_file
        ## READING FILE
        self.dump_file_lines = load_and_clean_text_file(self.input_dump_file)
        ## EXTRACTING LINES
        self.extract_lines()
        return
    
    ### FUNCTION TO EXTRACT THE LINES
    def extract_lines(self):
        '''
        The purpose of this function is to extract the line of the dump file
        INPUTS:
            self: class object
        OUTPUTS:
            self.extract_data: (dict) contains all extracted information in a form of a dictionary
                'phi': numpy array of all phi values, shape=(num_iter, 1)
                'num_atoms': numpy array of number of atoms, shape=(num_iter, 1)
                'potential_energy': numpy array ofpotential energy, shape=(num_iter, 1)
            
        '''
        ## IGNORING ALL LINES WITH HASHTAG
        data_no_comments = np.array([ line.split() for line in self.dump_file_lines if '#' not in line]).astype('float')
         # e.g.  '-0.2498 100 14 -32.8109090688289',
         # We know that the list breaks apart 
         #  0   --- phi value
         #  1   --- final temperature in K
         #  2   --- number of atoms (note that this starts counting from zero, so 12 means 13 atoms, and so on)
         #  3   --- potential energy from dump file
        self.extract_data= \
               {
                'phi'               : data_no_comments[:,0],                # Phi values
                'num_atoms'         : data_no_comments[:,2].astype('int'),  # Number of atoms as integers
                'potential_energy'  : data_no_comments[:,3],                # potential energy
                }

##################################
### CLASS TO EXTRACT XYZ FILES ###
##################################
class extract_vcsgc_xyz:
    '''
    The purpose of this class is to take the read_vcsgc_xyz class and run some calculations (e.g. diameters)
    INPUTS:
        xyz_file: class object from read_vcsgc_xyz
    OUTPUTS:
        ## CALCULATIONS OF EACH ITERATION
        self.diameters: (numpy array, shape=(num_iter, 1)) diameters in Angstroms for each iteration
        self.geometry_nonvacant: (list of numpy arrays for geometries, length = num_iter) geometries for nonvacants (i.e. type 1)
        self.geometry_vacant: (list of numpy arrays for geometries, length = num_iter) geometries for vacants (i.e. type 2)
        self.num_nonvacant_atoms: (list) total number of atoms that are nonvacant
    FUNCTIONS:
        calc_each_iter_diameters: calculates diameters at each iteration
        calc_coordination_num_dist2: [staticmethod] calculation of coordination number based on distance^2
    '''
    ### INITIALIZING
    def __init__(self, xyz_file):
        ## STORING OBJECT
        self.xyz_file = xyz_file
        ## CALCULATING DIAMETERS
        self.calc_each_iter()
        
    ### FUNCTION TO CALCULATE COORDINATION NUMBER GIVEN THE DISTANCE SQUARED MATRIX
    @staticmethod
    def calc_coordination_num_dist2( dist2, cutoff = FCC_GOLD_NEAREST_NEIGHBOR_APPROX_SQUARED_ANGS ):
        '''
        The purpose of this function is to calculate the coordination number of gold atoms given the distance squared matrix
        INPUTS:
            dist2: [np.array, shape=(N x N)] distance squared matrix
                e.g.
                [ 0, 0.15, ....]
                [ 0, 0   , 0.23, ...]
                [ 0, ... , 0]
        OUTPUTS:
            index_coord: numpy stacked version of: 
                index: [np.array, shape=(Nx1)] atom index of the gold atoms
                coordination_number: [np.array, shape=(Nx1)] number of gold atoms coordinated for the index
                e.g.
                [[0, 5], [1,7]] <-- atom index 0 has 5 nearest neighbors, and so on
        '''
        ## FINDING LOGICAL FOR MATCHING CRITERIA (greater than zero and less than some cutoff)
        match_criteria = np.where( (dist2 < cutoff) & (dist2 >0))
        # RETURNS: match_criteria is a tuple of size two: 
        #   First tuple contains unique indexes of the atoms
        #   Second tuple contains number of occurances
        ## FINDING INDEX AND COORDINATION NUMBER: Simply count the unique number of times you find a bond
        index, coordination_number = np.unique(match_criteria, return_counts=True)
        ## CONCATENATING RESULTS
        index_coord = np.column_stack( (index, coordination_number) )
        return index_coord
        
    ### FUNCTION TO CALCULATE DIAMETERS FOR EACH ITERATION
    def calc_each_iter(self):
        '''
        This function calculates things like diameter for each iteration. We will only look for atom types 1. 
        INPUTS:
            self: class object
        OUTPUTS:
            self.diameters: (numpy array, shape=(num_iter, 1)) diameters in Angstroms for each iteration
            self.geometry_nonvacant: (list of numpy arrays for geometries, length = num_iter) geometries for nonvacants (i.e. type 1)
            self.geometry_vacant: (list of numpy arrays for geometries, length = num_iter) geometries for vacants (i.e. type 2)
            self.num_nonvacant_atoms: (numpy) total number of atoms that are nonvacant
        '''
        ## CREATING STORAGE SPACE FOR THE DIAMETERS
        self.diameters, self.geometry_nonvacant, self.geometry_vacant, self.num_nonvacant_atoms, self.coord_num = [], [], [], [], []
        ## LOOPING THROUGH EACH ITERATION
        for each_iteration in range(self.xyz_file.total_iterations):
            if (each_iteration % 50 == 0):
                print("Calculating iteration %d"%(each_iteration))
            ## FINDING ATOM TYPE INDEXES THAT ARE 1
            atom_index = np.where(self.xyz_file.extract_atomtypes[each_iteration] == 1)[0]
            ## FINDING NUMBER OF ATOMS
            num_atoms = len(atom_index)
            ## FINDING ALL GEOMETRIES THAT ARE WITHIN THE ATOM INDEX THAT ARE NON-VACANT
            geometry_within_index = self.xyz_file.extract_geometry[each_iteration][atom_index, :]
            ## FINDING DISTANCES SQUARED
            dist2 = calc_total_distance2_matrix(geometry_within_index, atom_threshold=1000)
            ## FINDING COORDINATION NUMBER
            coord_num = self.calc_coordination_num_dist2(dist2)
            
            ## FINDING MAXIMUM DIAMETER
            max_diameter2 = np.max(dist2)
            
            ## FINDING ATOM INDICES THAT ARE 2
            atom_index_2 = np.where(self.xyz_file.extract_atomtypes[each_iteration] == 2)[0]
            geometry_outside_index = self.xyz_file.extract_geometry[each_iteration][atom_index_2, :]
            
            ## STORING
            self.num_nonvacant_atoms.append(num_atoms) # Stores atom index
            self.geometry_nonvacant.append(geometry_within_index) # Stores geometry within the atom index
            self.geometry_vacant.append(geometry_outside_index) # Stores geometry of vancant sites
            self.diameters.append(max_diameter2) # Stores diameters ^2
            self.coord_num.append(coord_num)
            
        ## NOW, TAKING SQUARE ROOT AND CONVERTING TO NUMPY
        self.diameters = np.sqrt(np.array(self.diameters))
        self.num_nonvacant_atoms = np.array(self.num_nonvacant_atoms)
        return
    
#################################
### CLASS TO PLOT VCSGC FILES ###
#################################
class plot_vcsgc:
    '''
    The purpose of this script is to plot xyz file. We would like to see how the morphology changes.
    INPUTS:
        extract_xyz_file: (class object) extracted xyz details from extract_vcsgc_xyz class
        extract_dump_file: (class object) extracted dump details from read_vcsgc_dump class
        want_all_plots: True if you want all 2D plots
    ACTIVE FUNCTIONS:
        plot_num_atom_vs_phi: plots number of atoms versus phi (also plots diameter versus phi)
        plot_phi_vs_num_atom: plots phi versus number of atoms
        plot_diameter_vs_num_atom: plots diameter versus number of atoms
        plot_hist_nonvacant: plots number of atom and diameter histogram
        plot_potential_energy_vs_phi: plots potential energy per phi
        plot_potential_energy_vs_diameter: plots potential energy versus diameter
        plot_potential_energy_vs_num_atoms: plots potential energy versus number of atoms
        plot_structure_for_frame: plots a single structure of a frame iteration
        plot_structure_all_frames: plots all frame iterations and shows it via animation
    '''
    ### INITIALIZING
    def __init__(self, extract_xyz_file = None, extract_dump_file=None, want_all_plots = False):
        
        ## STORING
        self.extract_xyz_file = extract_xyz_file
        self.extract_dump_file = extract_dump_file
        
        if extract_xyz_file is None or extract_dump_file is None:
            print("Possible error! extract_xyz_file or extract_dump_file does not exist!")
            print("Please check your inputs for plot_vcsgc")
            print("Stopping here to prevent errors!")
            sys.exit()
        
        ## PLOTTING
        if want_all_plots is True:
            self.plot_num_atom_vs_phi()
            self.plot_phi_vs_num_atom() # clearly non-linear
            self.plot_diameter_vs_num_atom()
            self.plot_hist_nonvacant()
            self.plot_potential_energy_vs_phi()
            self.plot_potential_energy_vs_diameter()
            self.plot_potential_energy_vs_num_atoms()
            self.plot_structure_for_frame()
            
        # self.plot_structure_all_frames(stride=5)

    ### FUNCTION TO PLOT THE NUMBER OF ATOMS VERSUS PHI
    def plot_num_atom_vs_phi(self):
        '''
        The purpose of this function is to plot the number of atoms versus iteration
        INPUTS:
            self: class object
        OUTPUTS:
            plot of number of atoms versus phi
        '''
        ## CREATING PLOT
        fig, ax = plt.subplots()
        ## DEFINING X AND Y AXIS
        ax.set_xlabel('Phi', **LABELS)
        ax.set_ylabel('Number of atoms', **LABELS)
        ## PLOTTING
        ax.plot( self.extract_dump_file.extract_data['phi'], self.extract_xyz_file.num_nonvacant_atoms, color='k', **LINE_STYLE )
        ## ADDING DIAMETER
        ax2 = ax.twinx()
        ax2.set_ylabel('Diameter (Angs)',color='b', **LABELS)
        ax2.plot( self.extract_dump_file.extract_data['phi'], self.extract_xyz_file.diameters, color='b', **LINE_STYLE )
        color_y_axis(ax2, 'b')
        return
    
    ### FUNCTION TO PLOT THE DIAMETER VS NUMBER OF ATOMS
    def plot_diameter_vs_num_atom(self):
        '''
        The purpose of this function is to plot the number of atoms versus iteration
        INPUTS:
            self: class object
        OUTPUTS:
            plot of diameter versus number of atoms
        '''
        ## CREATING PLOT
        fig = plt.figure(); ax = fig.add_subplot(111)
        ## DEFINING X AND Y AXIS
        ax.set_xlabel('Number of atoms', **LABELS)
        ax.set_ylabel('Diameter (Angs)', **LABELS)
        ## PLOTTING
        ax.scatter( self.extract_xyz_file.num_nonvacant_atoms, self.extract_xyz_file.diameters, color='k', **LINE_STYLE )
        return
        
    ### FUNCTION TO PLOT PHI VS NUMBER OF ATOMS
    def plot_phi_vs_num_atom(self):
        '''
        The purpose of this function is to plot phi versus the number of atoms
        INPUTS:
            self: class object
        OUTPUTS:
            plot of phi versus number of atoms
        '''
        ## CREATING PLOT
        fig = plt.figure(); ax = fig.add_subplot(111)
        ## DEFINING X AND Y AXIS
        ax.set_xlabel('Number of atoms', **LABELS)
        ax.set_ylabel('Phi', **LABELS)
        ## PLOTTING
        ax.scatter( self.extract_xyz_file.num_nonvacant_atoms, self.extract_dump_file.extract_data['phi'], color='k', **LINE_STYLE )
        return
    
    ### FUNCTION TO GET THE HISTOGRAM OF NUMBER OF ATOMS AND DIAMETERS
    def plot_hist_nonvacant(self):
        '''
        The purpose of this function is to plot a histogram of the number of atoms and diameter sizes
        INPUTS:
            self: class object
        OUTPUTS:
            histogram of the number of atoms
            histogram of the diameters
        '''
        ## CREATING PLOT
        f, (ax1, ax2) = plt.subplots(1, 2, sharey=False)
        ## DEFINING X AND Y AXIS
        ax1.set_xlabel('Number of atoms', **LABELS)
        ax2.set_xlabel('Diameter (Angs)', **LABELS)
        ax1.set_ylabel('Number of occurances', **LABELS)
        ax2.set_ylabel('Number of occurances', **LABELS)
        ## PLOTTING
        ax1.hist(self.extract_xyz_file.num_nonvacant_atoms, color='black')
        ax2.hist(self.extract_xyz_file.diameters, color='red')
        return
    
    ### FUNCTION TO PLOT POTENTIAL ENERGY OVER PHI
    def plot_potential_energy_vs_phi(self):
        '''
        The purpose of this function is to plot the potential energy versus phi
        INPUTS:
            self: class object
        OUTPUTS:
            plot of potential energy versus phi
        '''
        ## CREATING PLOT
        fig = plt.figure(); ax = fig.add_subplot(111)
        ## DEFINING X AND Y AXIS
        ax.set_xlabel('Phi', **LABELS)
        ax.set_ylabel('Potential energy', **LABELS)
        ## PLOTTING
        ax.plot( self.extract_dump_file.extract_data['phi'], self.extract_dump_file.extract_data['potential_energy'], color='k', **LINE_STYLE )
        
    ### FUNCTION TO PLOT POTENTIAL ENERGY VERSUS DIAMETER
    def plot_potential_energy_vs_diameter(self):
        '''
        The purpose of this function is to plot potential energy versus diameter
        INPUTS:
            self: class object
        OUTPUTS:
            plot of potential energy versus diameter
        '''
        ## CREATING PLOT
        fig = plt.figure(); ax = fig.add_subplot(111)
        ## DEFINING X AND Y AXIS
        ax.set_xlabel('Diameter (Angs)', **LABELS)
        ax.set_ylabel('Potential energy', **LABELS)
        ## PLOTTING
        ax.scatter( self.extract_xyz_file.diameters, self.extract_dump_file.extract_data['potential_energy'], color='k', **LINE_STYLE)
        
    ### FUNCTION TO PLOT POTENTIAL ENERGY VERSUS NUMBER OF ATOMS
    def plot_potential_energy_vs_num_atoms(self):
        '''
        The purpose of this function is to plot potential energy versus number of atoms
        INPUTS:
            self: class object
        OUTPUTS:
            plot of potential energy versus number of atoms
        '''
        ## CREATING PLOT
        fig = plt.figure(); ax = fig.add_subplot(111)
        ## DEFINING X AND Y AXIS
        ax.set_xlabel('Number of atoms', **LABELS)
        ax.set_ylabel('Potential energy', **LABELS)
        ## PLOTTING
        ax.scatter( self.extract_xyz_file.num_nonvacant_atoms, self.extract_dump_file.extract_data['potential_energy'], color='k', **LINE_STYLE)
        
    ### FUNCTION TO PLOT ATOM COORDINATES OF A SINGLE ITERATION
    def plot_structure_for_frame(self, iteration=0, fig = None, ax = None, scatter_plot_storage = None):
        '''
        The purpose of this function is to plot the atoms for a single iteration
        INPUTS:
            self: class object
            iteration: (int) iteration you are interested in
            fig: figure you want to add into [default: None]
            ax: axis you want to add to [default: None]
        OUTPUTS:
            fig, ax: figure and axis of the plot
        '''
        ## PLOTTING STRUCTURE
        fig, ax, scatter_plot_storage = plot_structure_for_frame(vacant_geometry = self.extract_xyz_file.geometry_vacant[iteration], 
                                                                 nonvacant_geometry = self.extract_xyz_file.geometry_nonvacant[iteration], 
                                                                 nonvacant_coord_num = self.extract_xyz_file.coord_num[iteration], 
                                                                 iteration=iteration, 
                                                                 diameter= self.extract_xyz_file.diameters[iteration],
                                                                 phi_value= self.extract_dump_file.extract_data['phi'][iteration], 
                                                                 potential_energy =  self.extract_dump_file.extract_data['potential_energy'][iteration], 
                                                                 fig = fig, 
                                                                 ax = ax, 
                                                                 scatter_plot_storage = scatter_plot_storage)
        
        return fig, ax, scatter_plot_storage
        
    
    ### FUNCTION TO PLOT ALL ITERATIONS
    def plot_structure_all_frames(self, pause_time=0.001, stride=1):
        '''
        The purpose of this function is to plot all iterations using the plot_structure_for_frame function
        INPUTS:
            self: class object
            pause_time: time to pause
            stride: (int) place value here if you want to skip simulations
        OUTPUTS:
            animation plot with the structures drawn
        '''
        ## DEFINING FIGURE
        fig = None; ax = None; scatter_plot_storage = None
        
        ## LIST OF ITERATIONS
        iteration_list = np.arange(0, self.extract_xyz_file.xyz_file.total_iterations, stride)
        
        ## LOOPING THROUGH EACH ITERATION
        for each_iter in iteration_list:
            ## PRINTING
            if (each_iter % 10 == 0):
                print("--> PLOTTING ITERATION: %d"%(each_iter))
            ## PLOTTING THE ITERATION
            fig, ax, scatter_plot_storage = self.plot_structure_for_frame(iteration = each_iter,
                                               fig = fig,
                                               ax = ax,
                                               scatter_plot_storage = scatter_plot_storage
                                               )
            ## PAUSING SO YOU CAN VIEW IT
            plt.pause(pause_time) # Pause so you can see the changes            
        
#############################
### CLASS TO EXPORT VCSGC ###
#############################
class export_vcsgc:
    '''
    The purpose of this function is to export xyz and dump file
    INPUTS:
        extract_xyz_file: class object from extract_vcsgc_xyz
        extract_dump_file: class object from read_vcsgc_dump
    OUTPUTS:
        self.extract_xyz_file: stored class from extract_vcsgc_xyz
        self.extract_dump_file: stored class from read_vcsgc_dump
        self.pickle_output_name: output name for the pickle            
    FUNCTIONS:
        write_xyz_file: writes pickle file
    ALGORITHM:
        - Create output xyz file
        - Output to xyz file (including coordination numbers, number of atoms, etc.)
        - Create a dump file
        - Output to dump file
    '''
    ### INITIALIZING
    def __init__(self, extract_xyz_file=None, extract_dump_file=None, pickle_output_name='out.pickle'):
        ## DEFINING SELF VARIABLES
        self.extract_xyz_file = extract_xyz_file
        self.extract_dump_file = extract_dump_file
        self.pickle_output_name = pickle_output_name
        
        ## STORING INTO PICKLE
        self.write_xyz_file()
        
        ## PRINTING SUMMARY
        self.print_summary()
        
        return
    ### FUNCTION TO PRINT SUMMARY
    def print_summary(self):
        ''' This function prints the summary '''
        print("EXPORTING OUTPUT TO: %s"%(self.pickle_output_name) )
    ### FUNCTION TO OUTPUT XYZ FILE
    def write_xyz_file(self):
        '''
        The purpose of this function is to write the output file
        INPUTS:
            self: class property
        OUTPUTS:
            pickle file with the property stored
        '''
        with open(self.pickle_output_name, 'wb') as f:  # Python 3: open(..., 'wb')
            pickle.dump([self], f, protocol=-1) 
        
### FUNCTION TO LOAD PICKLE FILE
def load_pickle_vcsgc(pickle_file_name='out.pickle'):
    '''
    The purpose of this file is to load the pickle file for a given vcsgc.
    INPUTS:
        pickle_file_name: name of the pickle file
    OUTPUTS:
        Extracted vcsgc class
    '''
    print("LOADING PICKLE FROM: %s"%(pickle_file_name))
    with open(pickle_file_name,'rb') as f:  # Python 3: open(..., 'rb')
        exported_vcsgc_results = pickle.load(f)
    return exported_vcsgc_results[0]
        
#%% MAIN SCRIPT
if __name__ == "__main__":
    ### SEEING IF TEST IS ONE
    testing = False # False if you are on command line
    
    ################################
    ### DEFINING INPUT VARIABLES ###
    ################################
    if testing is True:
        ## INPUT DIRECTORIES
        '''
        input_dir = r'R:\scratch\LigandBuilder\Prep_Gold_Core\vcsgc_files\simulations\8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc' 
        input_xyz_file = 'vcsgc.xyz'
        input_dump_file = 'vcsgc.dump' # dump file has all the energies, etc.
        '''
        input_dir = r'R:\scratch\LigandBuilder\Prep_Gold_Core\vcsgc_files\test_files' 
        input_xyz_file = 'test.xyz'
        input_dump_file = 'test.dump' # dump file has all the energies, etc.
        # '''
        ## DIAMETER EXTRACTION IN ANGSTROMS

        
        ## OUTPUT DIRECTORY
        output_pickle_file = 'output.pickle'
    
    else:
        ## ON COMMAND LINE
        from optparse import OptionParser # Used to allow commands within command line
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        ### ADDING OPTIONS
        parser.add_option("--dir", dest="input_dir", action="store", type="string", help="Input directory", default="test_dir")
        parser.add_option("--xyz", dest="input_xyz_file", action="store", type="string", help="Input xyz file", default="test.xyz")
        parser.add_option("--dump", dest="input_dump_file", action="store", type="string", help="Input dump file", default="test.dump")
        parser.add_option("--out", dest="output_pickle_file", action="store", type="string", help="Output pickle file", default="test.pickle")
        ### TAKING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
    
        input_dir = options.input_dir # Input directory
        input_xyz_file = options.input_xyz_file # Input xyz file
        input_dump_file = options.input_dump_file # Input dump file
        output_pickle_file = options.output_pickle_file # Output pickle file
    
    ###################
    ### MAIN SCRIPT ###
    ###################
    print("***BEGINNING VCSGC ANALYSIS***")    
    ## DEFINING PATHS
    path_input_xyz_file = input_dir + '/' + input_xyz_file
    path_input_dump_file = input_dir + '/' + input_dump_file
    ### CLASS TO READ XYZ FILES
    xyz_file = read_vcsgc_xyz(path_input_xyz_file)    
    ### CLASS TO EXTRACT XYZ FILE
    extract_xyz_file = extract_vcsgc_xyz(xyz_file)
    ### CLASS TO EXTRACT DUMP FILE
    extract_dump_file = read_vcsgc_dump(path_input_dump_file)
    ## CLOSING ALL FIGURES
    # plt.close('all')
            
    ### CLASS TO PLOT VCSGC DETAILS
    # plot_extract_vcsgc = plot_vcsgc(extract_xyz_file, extract_dump_file, want_all_plots=False)
    # plot_extract_vcsgc.plot_structure_for_frame(21)
    # plot_extract_vcsgc.plot_structure_all_frames()
    
    ### RUNNING EXPORT XYZ SCRIPT
    exported_xyz = export_vcsgc(extract_xyz_file, extract_dump_file, output_pickle_file)
    ### FUNCTION TO LOAD PICKLE
    # exported_vcsgc_results = load_pickle_vcsgc('out.pickle')
    
