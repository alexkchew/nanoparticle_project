# -*- coding: utf-8 -*-
"""
pickle_example.py
This will go through a tutorial on using pickles. Pickles are great for saving variables and reloading them, without going through the trouble of 

Created on: 04/06/2018

Author(s):
    Alex K. Chew (alexkchew@gmail.com)

"""
#%% STORING PICKLE FILES
import pickle
filename='test.pickle'

my_list = [1, 3, 4, 5, 7]

with open(filename, 'wb') as f:  # Python 3: open(..., 'wb')
    pickle.dump([my_list], f, protocol=-1) 
    
    
#%% LOADING FILE
    
## ASSUMING YOU RESTART THE IN SPYDER KERNEL (CTRL + .), YOU CAN LOAD THE PICKLE FILE:
import pickle
filename='test.pickle'

print("LOADING PICKLE FROM: %s"%(filename))
with open(filename,'rb') as f:  # Python 3: open(..., 'rb')
    my_list = pickle.load(f)
    
print(my_list)
# NOTE THAT THE LIST IS SAVED WITHIN A LIST OF LIST. THIS IS BECAUSE YOU SAVED A DUMP FILE AS A LIST. THIS ALLOWS YOU TO STORE MULTIPLE ARGUMENTS WITHIN THE PICKLE!
    
    