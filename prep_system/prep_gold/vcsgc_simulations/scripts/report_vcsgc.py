# -*- coding: utf-8 -*-
"""
report_vcsgc.py
The purpose of this script is to import all pickles from vcsgc and run subsequent analysis

Created on: 04/03/2018

FUNCTIONS:
    find_atom_closest_center: function to find the closest atom to some center of a lattice
    write_xyz_input_lammps: writes xyz inputs for lammps
    write_dump_input_lammps: writes dump input for lammps (e.g. diameter, phi, etc.)
CLASSES:
    create_fcc_lattice: class to create fcc lattice
    report_vcsgc: reports the results for vcsgc (major function that can output the input files for lammps)
        NOTE: This class will output some output.xyz and output.parameters, which you can use as inputs for another script

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)    
"""
### IMPORTING MODULES
import numpy as np
import sys
from analyze_vcsgc import read_vcsgc_xyz,read_vcsgc_dump, extract_vcsgc_xyz, export_vcsgc, plot_vcsgc, load_pickle_vcsgc, plot_structure_for_frame
## IMPORTING GLOBAL VARIABLES
from analyze_vcsgc import LABELS, LINE_STYLE

### DEFINING GLOBAL VARIABLES
# GOLD_FCC_LATTICE_CONSTANT=4.09 # Angstroms Same as the lattice constant for the runs in LAMMPS

### FUNCTION TO FIND THE ATOM CLOSEST TO THE CENTER
def find_atom_closest_center(geometry):
    '''
    The purpose of this script is to find the atom closest to the center of any lattice
    INPUTS:
        geometry: [np.array, shape=(N,3)] geometry of the entire lattice
    OUTPUTS:
        center_atom_index: [int] atom index of the center based on geometry
        center_atom_positions: [np.array, shape(1,3)] atomic positions of the center atoms
        radius: [float] radius of the lattice (found from maximum distance from center)
    '''
    ## FINDING CENTER OF THE LATTICE
    center = np.mean(geometry, axis = 0)
    ## FINDING DISTANCES OF ALL ATOMS FROM THE CENTER
    distance_from_center = np.sqrt(  np.sum( (geometry - center)**2, axis=1 ) )
    ## FINDING MAX DISTANCE (i.e. radius)
    radius = np.max(distance_from_center)
    ## FINDING CLOSEST TO CENTER
    center_atom_index = np.argmin(distance_from_center)
    center_atom_positions = geometry[center_atom_index]
    return center_atom_index, center_atom_positions, radius
        
### FUNCTION TO WRITE OUTPUT OF GEOMETRY, ATOM NUMBER, ETC. INTO A XYZ DUMP FILE AS AN INPUT
def write_xyz_input_lammps(file_name, geometry, atom_types ):
    '''
    The purpose of this function is to output the xyz data and atom type into a xyz file
    INPUTS:
        file_name: [string] file name to output to
        geometry: [np.array,shape=(N,3)] geometry of the lattice
        atom_types: [np.array,shape=(N,1)] atom type, where 1 represents nonvacant and 2 represents vacant
    OUTPUTS:
        writes a file into file_name
    '''
    ## FINDING TOTAL ATOMS BASED ON GEOMETRY
    total_atoms = len(geometry)
    ## OPENING AND CREATING FILE
    print("CREATING FILE: %s"%(file_name))
    with open(file_name, 'w') as xyz_file:
        ## WRITING TOTAL ATOMS
        xyz_file.write("%d\n"%(total_atoms))
        ## WRITING USUAL TITLE
        xyz_file.write("Atoms. This file was generated from report_vcsgc.py\n")
        ## LOOPING THROUGH GEOMETRY AND WRITING THE FILE
        for index, each_atom in enumerate(geometry):
            if atom_types is not None:
                xyz_file.write("1 %.4f %.4f %.4f %d %d\n"%( each_atom[0],
                                                                      each_atom[1],
                                                                      each_atom[2],
                                                                      index+1,
                                                                      atom_types[index],
                                                                      ))        
            else:
                xyz_file.write("1 %.4f %.4f %.4f %d\n"%( each_atom[0],
                                                                      each_atom[1],
                                                                      each_atom[2],
                                                                      index+1,
                                                                      ))   
    return

### FUNCTION TO WRITE OUT SIMULATION DETAILS AS A DUMP FILE
def write_dump_input_lammps(file_name, diameter, num_atoms, phi=0.000, swaprad = 0.000 ):
    '''
    The purpose of thi function is to write a dump file of the diameter and phi value
    INPUTS:
        diameter: diameter in nm
        phi: phi value in 
        swaprad: cutoff radius for fcc lattice
        num_atoms: [int] number of atoms
    '''
    ## OPENING AND CREATING FILE
    print("CREATING FILE: %s"%(file_name))
    with open(file_name, 'w') as file:
        ## WRITING DUMP INPUTS
        file.write("diameter(nm), %.4f\n"%(diameter))
        file.write("phi, %.4f\n"%(phi))
        file.write("swaprad(Angs), %.4f\n"%(swaprad))
        file.write("numatoms, %d\n"%(num_atoms))
    return

###################################
### CLASS TO CREATE FCC LATTICE ###
###################################
class create_fcc_lattice():
    '''
    The purpose of this function is to create fcc lattice geometry as a rectangular shape, then truncate with spherical radius
    INPUTS:
        lattice_constant: lattice constant
        radius: spherical radius of fcc lattice (same units as fcc lattice constant)
    OUTPUTS:
        ## INPUTS:
            self.lattice_constant, self.radius
        ## LATTICE
            self.basis_vectors: [np.array, shape=(4, 3)] basis vectors (4 atoms) that can be translated to create a fcc lattice
            self.num_expansion: number of times to expand the lattice in all three dimensions
        ## GEOMETRY
            self.geometry: [np.array, shape=(N,3)] geometry of the entire lattice
            self.spherical_geometry: [np.array,shape=(sphere_pts, 3) geometry of just a spherical lattice
            
        ## CENTER ATOM INFORMATION
        self.center_atom_index: [int] index of self.geometry that is the center
        self.center_atom_position: [np.array, shape=(1,3)] position of the center atom                
    FUNCTIONS:
        find_basis_vectors: locates basis vectors based on the fcc lattice
        plot_lattice: plots the current lattice given the coordinates
    ALGORITHM:
        - Find basis vectors
        - Expansion of basis vectors in all three-dimensions
        - Truncation of the lattice into a sphere
    USAGE EXAMPLE: fcc_lattice = create_fcc_lattice(GOLD_FCC_LATTICE_CONSTANT, radius=20, debug_mode=True)
    '''
    ### INITIALIZATION
    def __init__(self, fcc_lattice_constant, radius=20, debug_mode=False):
        ## DEFINING INPUT VARIABLES
        self.lattice_constant = fcc_lattice_constant
        self.radius = radius
        ## FINDING BASIS VECTORS
        self.find_basis_vectors()
        ## FINDING NUMBER OF TIMES WE NEED TO REPEAT
        self.num_expansion = np.ceil( self.radius*2 / self.lattice_constant )
        ## EXPANDING LATTICE
        self.expand_lattice()
        ## FINDING ATOMS CLOSEST TO THE CENTER
        self.center_atom_index, self.center_atom_position, _ = find_atom_closest_center(self.geometry)
        ## FINDING GEOMETRY BY CUTTING A SPHERE
        self.cut_lattice_spherical()
        ## PLOTTING IF DEBUG MODE IS ON
        if debug_mode is True:
            ax, fig = self.plot_lattice(self.basis_vectors)
            ax, fig = self.plot_lattice(self.geometry)
            ax, fig = self.plot_lattice(self.spherical_geometry)
        
    ### FUNCTION TO DEFINE BASIS VECTORS
    def find_basis_vectors(self):
        '''
        The purpose of this function is to get the basis vectors for a fcc lattice.
        INPUTS:
            self: class object
        OUTPUTS:
            self.basis_vectors: [np.array, shape=(4, 3)] basis vectors (4 atoms) that can be translated to create a fcc lattice
        '''
        ## MAGNITUDE OF VECTOR
        mag_vector = self.lattice_constant/2.0
        ## FCC basis vectors defined as a/2(i + k), a/2(i + j), and a/2 (j+k), etc.
        self.basis_vectors = np.array([
                                [0, 0, 0 ],
                                [1, 0, 1 ],
                                [0, 1, 1 ],
                                [1, 1, 0 ],
                ]) * mag_vector

    ### FUNCTION TO EXPAND BASIS VECTOR
        
    ### FUNCTION TO PLOT THE LATTICE
    @staticmethod
    def plot_lattice(coordinates, fig = None, ax = None):
        '''
        The purpose of this function is to plot the lattice given the coordinates
        INPUTS:
            coordinates: [np.array, shape=(nx3)] coordinates of the atoms
        OUTPUTS:
            fig, ax: figure and axis for the figure
        '''
        import matplotlib.pyplot as plt # matlab plotting tools
        from mpl_toolkits.mplot3d import Axes3D # For 3D axes
        
        ## CREATING FIGURE
        if fig is None and ax is None:
            fig = plt.figure()
            ax = fig.add_subplot(111, projection='3d')
        
        ## SETTING X,Y,Z LABELS
        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.set_zlabel('z')
        
        ## PLOTTING SCATTER PLOT
        ax.scatter(coordinates[:,0], 
                   coordinates[:,1],
                   coordinates[:,2],
                   color='r', s=20)
        return fig, ax
    
    ### FUNCTION TO EXPAND THE LATTICE
    def expand_lattice(self ):
        '''
        The purpose of this function is to expand the lattice
        INPUTS:
            self: class object
        OUTPUTS:
            self.geometry: [np.array, shape=(N,3)] geometry of the entire lattice
        '''
        ## CREATING GEOMETRY, APPENDING BASIS VECTORS
        self.geometry = self.basis_vectors[:]
        ## DEFINING EXPANSION VECTOR
        self.expansion_vector = np.arange(1,self.num_expansion)*self.lattice_constant # 0, 1, 2, .. and so on * lattice constant
        ## LOOPING THROUGH EACH DIMENSION (X, Y, Z)
        for each_dimension in range(3):
            ## DEFINING CURRENT GEOMETRY
            current_geometry = np.copy(self.geometry[:])
            ## NOW, LOOPING THROUGH EACH EXPANSION AND APPENDING
            for each_expansion in self.expansion_vector:
                ## NEW GEOMETRY
                new_geometry = np.copy(current_geometry[:])
                new_geometry[:, each_dimension] += each_expansion
                ## STORING INTO GEOMETRY
                self.geometry = np.concatenate( (self.geometry, new_geometry) )
        return
    ### FUNCTION TO CUT SPHERICAL LATTICE
    def cut_lattice_spherical(self):
        '''
        The purpose of this function is to cut the lattice into a sphere
        INPUTS:
            self: class object
        OUTPUTS:
            self.spherical_geometry: [np.array, shape=(n,3)] geometry of a spherical cut fcc lattice
        '''
        ## FINDING DISTANCES FROM THE SPHERE CENTER
        distance_to_center_atom = np.sqrt(  np.sum( (self.geometry - self.center_atom_position)**2, axis=1 ) )
        ## FIND ALL ATOMS WITHIN THE SPHERE
        self.spherical_geometry = self.geometry[np.where(distance_to_center_atom <= self.radius)]
        return


###########################################
### CLASS TO RUN CALCULATIONS FOR VCSGC ###
###########################################
class report_vcsgc:
    '''
    The purpose of this function is to analyze the vcsgc results
    INPUTS:
        exported_vcsgc_results: results from the vcsgc pickle file
        diam: diameter in nm that you desire
        output_dir: directory to output the files
        output_file: output file names
        delta_diam: diameter that gets subtracted by the diameter (forms a threshold diameter)
        diam_range: diameter interval below diam-delta_diam to look for the closest shape
        delta_diam_threshold_cutoff: [float] diameter threshold cutoff that you would like
        min_diam: (int) minimum diameter to look at
        max_diam: (int) maximum diameter to look at
        want_figures: True or False. If True, this class will plot energy vs. diameter, and final configuration
        report_type: type of reporting
            'rerun': [default] creates input lammps file for rerunning the vcsgc simulation
            'extract': extracts a single iteration with the lowest energy. It will simply output an xyz file with the coordinates of ONLY the nonvacant sites!
        
    OUTPUTS:
        ## INITIAL VARIABLES ARE STORED
            self.exported_vcsgc_results, self.min_diam, self.max_diam
        
        ## FINDING ITERATIONS WITHIN DIAMETER RANGE
            self.min_diam: [float] minimum diameter range
            self.max_diam: [float] maximum diameter range
        
            self.iter_within_diam_index: [numpy array] index of iterations that fall within the diameter range
            self.total_iter_within_diam_index: [int] total number of iterations within diameter range
        
        ## LOWEST ENERGY VALUES
            self.lowest_energy_iteration_index: index of the lowest energy iteration
            self.lowest_energy_iteration_value: value of the lowest energy iteration
            self.lowest_energy_diameter_nm: diameter in nm of the lowest energy
            self.lowest_energy_phi: phi of the lowest energy
        
        ## FINDING LATTICE PARAMETERS
            self.lattice_center: [np.array, shape=(3,1)] center of the lattice, approximated by the averaging of all atom positions
            self.lattice_radius: [float] approximate lattice radius
            self.lattice_closest_center_atom_positions: [np.array, shape=(3,1)] position of the point closest to the lattice center
        
        ## TRUNCATED SPHERE PARAMETERS
            self.radius_cutoff: [float] radius cutoff for the spherical lattice in angstroms
            self.lattice_truncated: [np.array, shape=(N,3)] geometry of the lattice that is truncatedS
        
    FUNCTIONS:
        find_iterations_within_diameter_range: finds the iterations within the diameter range
        find_lowest_energy: finds the lowest energy of the iterations within diameter range
        plot_energy_vs_diameter: plots energy versus diameter range of interest and highlighting minima energy
        plot_lowest_energy_config: plots the lowest energy configuration
        find_lattice_info: finds the lattice information (e.g. center, radii, etc.)
        move_lattice_center: function to move the nonvacant sites to the center of the lattice
        truncate_lattice_sites_spherical: truncates the lattice sites to a spherical shape based on some cutoff
        update_coordinate_list: function to update the coordinates list for each lattice site
        
    ALGORITHM:
        - Find iterations within the diameter range
        - Find the iteration closest to the desired diameter (diam)
        - Find the lattice information
        - Recenter vacant sites to the center of the lattice
        - Shave off spherical lattice to match approximately the spherical diameter of interest (obviously overapproxiamte to give room for the lattice to grow)
        - Update atom coordinate list
        - Use a function to output the coordinate list into a dump file for re-running of the input file
        - Use a function to output the run parameters (?), e.g. phi value, diameter, energy, etc.
    '''
    ### INITIALIZING
    def __init__(self, exported_vcsgc_results, diam, output_dir = '.', output_file = 'output_file', delta_diam=0.1, diam_range=0.5, 
                 delta_diam_threshold_cutoff = 4, new_lattice_radius=50, want_figures=False, report_type='rerun'):
        ## STORING RESULTS
        self.exported_vcsgc_results = exported_vcsgc_results
        self.diam = diam
        self.delta_diam = delta_diam
        self.diam_range = diam_range
        self.delta_diam_threshold_cutoff = delta_diam_threshold_cutoff
        
        ## USING PLOT VCSGC FUNCTION
        self.plot_vcsgc_results = plot_vcsgc(self.exported_vcsgc_results.extract_xyz_file, self.exported_vcsgc_results.extract_dump_file )
        
        ## FINDING DIAMETER RANGES
        if report_type == 'rerun':
            self.min_diam = self.diam - self.delta_diam - self.diam_range
            self.max_diam = self.diam - self.delta_diam
        elif report_type == 'extract':
            self.min_diam = self.diam - self.delta_diam
            self.max_diam = self.diam + self.delta_diam
        
        ## DEFINING DIAMETERS IN NANOMETERS
        self.diameters_list_nm = self.exported_vcsgc_results.extract_xyz_file.diameters / 10.0
        
        ## FINDING ITERATIONS THAT ARE WITHIN THE DIAMETER RANGE
        self.find_iterations_within_diameter_range()
        
        ### RUNNING RERUN SCRIPT
        if report_type == 'rerun':
            ## FINDING CLOSEST DIAMETER
            self.find_closest_diameter()
            
            ## FINDING DETAILS ABOUT THE FINAL ITERATION
            self.find_lattice_info(self.iteration_index)
            
            ## MOVING LATTICE TO THE CENTER
            self.move_lattice_center(lattice_closest_center_atom_positions=self.lattice_closest_center_atom_positions,
                                     lattice_geometry = self.lattice_geometry)
            
            ### PLOT FOR WHEN THE LATTICE IS MOVED TO CENTER
            if want_figures is True:
                fig, ax, scatter_plot_storage = plot_structure_for_frame(vacant_geometry = self.lattice_geometry, 
                                                                         nonvacant_geometry = self.nonvacant_geom_center, 
                                                                         nonvacant_coord_num = self.exported_vcsgc_results.extract_xyz_file.coord_num[self.iteration_index], 
                                                                         iteration=self.iteration_index, 
                                                                         diameter= self.iteration_diameter,
                                                                         phi_value= self.exported_vcsgc_results.extract_dump_file.extract_data['phi'][self.iteration_index], 
                                                                         potential_energy =  self.exported_vcsgc_results.extract_dump_file.extract_data['potential_energy'][self.iteration_index], 
                                                                         title = 'CENTERING THE NONVACANT LATTICE'
                                                                         )
            
            ## FINDING THRESHOLD AND SHORTENING LATTICE GEOMETRY LIST
            self.truncate_lattice_sites_spherical()
            
            ## PLOT FOR WHEN THE LATTICE IS SHRUNK BY SPHERICAL CUTOFF
            if want_figures is True:    
                fig, ax, scatter_plot_storage = plot_structure_for_frame(vacant_geometry = self.lattice_truncated, 
                                                                         nonvacant_geometry = self.nonvacant_geom_center, 
                                                                         nonvacant_coord_num = self.exported_vcsgc_results.extract_xyz_file.coord_num[self.iteration_index], 
                                                                         iteration=self.iteration_index, 
                                                                         diameter= self.iteration_diameter,
                                                                         phi_value= self.exported_vcsgc_results.extract_dump_file.extract_data['phi'][self.iteration_index], 
                                                                         potential_energy =  self.exported_vcsgc_results.extract_dump_file.extract_data['potential_energy'][self.iteration_index], 
                                                                         title = 'SPHERICAL CUTOFF SHRINKING'
                                                                         )
            
            ## FINDING UPDATE COORDINATE NUMBER BASED ON SPHERICAL LATTICE SITES
            self.update_coordinate_list( self.lattice_truncated, self.nonvacant_geom_center )
            
            ## MOVING THE CENTER OF THE SPHERE INTO A REASONABLE BOX LENGTH BASED ON CUTOFF RADIUS
            self.move_coordinates_box_center()            
            
            ## PLOT FOR WHEN THE LATTICE IS SHRUNK BY SPHERICAL CUTOFF
            if want_figures is True:
                fig, ax, scatter_plot_storage = plot_structure_for_frame(vacant_geometry = self.lattice_truncated, 
                                                                         nonvacant_geometry = self.nonvacant_geom_center, 
                                                                         nonvacant_coord_num = self.exported_vcsgc_results.extract_xyz_file.coord_num[self.iteration_index], 
                                                                         iteration=self.iteration_index, 
                                                                         diameter= self.iteration_diameter,
                                                                         phi_value= self.exported_vcsgc_results.extract_dump_file.extract_data['phi'][self.iteration_index], 
                                                                         potential_energy =  self.exported_vcsgc_results.extract_dump_file.extract_data['potential_energy'][self.iteration_index], 
                                                                         title = 'SHRUNK LATTICE, MOVED TO CENTER OF BOX'
                                                                         )
            ## DUMPING THE ITERATION FILE INTO XYZ
            write_xyz_input_lammps(file_name = output_dir + '/' + output_file + '.xyz',
                                   geometry = self.lattice_truncated,
                                   atom_types = self.atom_type
                                   )
            
            ## DUMPING THE SIMULATION PARAMETERS INTO A DUMP FILE
            write_dump_input_lammps( file_name = output_dir + '/' + output_file + '.parameters',
                                     diameter = self.iteration_diameter,
                                     phi = self.exported_vcsgc_results.extract_dump_file.extract_data['phi'][self.iteration_index],
                                     swaprad = self.radius_cutoff, # Cutoff radius in angstroms for the fcc lattice
                                     num_atoms = len(self.nonvacant_geom_center) # Number of nonvacant atoms
                                    )
            
        ### RUNNING EXTRACTION SCRIPT
        elif report_type == 'extract':
            ## FINDING THE LOWEST ENERGY INDEX
            self.find_lowest_energy()
            
            if want_figures is True:
                ## PLOTTING ENERGY VERSUS DIAMETER
                self.plot_energy_vs_diameter()
                
                ## FINDING LATTICE INFORMATION
                self.plot_lowest_energy_config()
            
            ## FINDING NONVACANT GEOMETRY
            self.nonvacant_geom= self.exported_vcsgc_results.extract_xyz_file.geometry_nonvacant[self.lowest_energy_iteration_index]   
            
            ## DUMPING THE ITERATION FILE INTO XYZ
            write_xyz_input_lammps(file_name = output_dir + '/' + output_file + '.xyz',
                                   geometry = self.nonvacant_geom,
                                   atom_types = np.ones( (len(self.nonvacant_geom), 1))
                                   )
            
            ## DUMPING THE SIMULATION PARAMETERS INTO A DUMP FILE
            write_dump_input_lammps( file_name = output_dir + '/' + output_file + '.parameters',
                                     diameter = self.lowest_energy_diameter_nm,
                                     phi = self.lowest_energy_phi,
                                     num_atoms = len(self.nonvacant_geom) # Number of nonvacant atoms
                                    )
        
        
    ### FUNCTION TO FIND THE ITERATIONS THAT ARE IN THE MINIMUM AND MAXIMUM DIAMETERS
    def find_iterations_within_diameter_range(self):
        '''
        The purpose of this function is to find the iterations that are within the diameter ranges
        INPUTS:
            self: class object
        OUTPUTS:
            self.diameters_list_nm: [np.array, shape=(N,1)] list of diameters for each iteration
            self.iter_within_diam_index: [numpy array] index of iterations that fall within the diameter range
                e.g. array([  0,   1,   2,   3,   4, ....])
            self.total_iter_within_diam_index: [int] total number of iterations within diameter range
            self.diameters_within_range: [np.array, shape=(iter, 1)] diameters within the iteration index (i.e. diameters available to us given the constraints)
        '''
        
        ## PRINTING
        print("\n***FINDING DIAMETERS WITHIN RANGE OF %.3f TO %.3f nm***"%(self.min_diam, self.max_diam))
        print("MIN DIAMETER: %.3f nm"%(np.min(self.diameters_list_nm)))
        print("MAX DIAMETER: %.3f nm"%(np.max(self.diameters_list_nm)))
        
        ## FINDING DIAMETERS WITHIN THE RANGE
        self.iter_within_diam_index = np.where( (self.diameters_list_nm>self.min_diam) & (self.diameters_list_nm<self.max_diam) )[0]
        self.diameters_within_range = self.diameters_list_nm[self.iter_within_diam_index]
        
        ## FINDING TOTAL ITERATIONS WITHIN DIAMETER INDEX
        self.total_iter_within_diam_index = len(self.iter_within_diam_index)
        
        ## CHECKING IT ITERATION IS WITHIN THE DIAMETER INDEX
        if len(self.iter_within_diam_index) == 0:
            print("ERROR! NO DIAMETER WITHIN THAT RANGE IS FOUND!")
            print("DOUBLE CHECK YOUR DIAMETER INPUTS!")
            print("EXITING HERE!")
            sys.exit()
        else:
            print("TOTAL ITERATIONS FALLING WITHIN DIAMETER RANGE: %d"%(self.total_iter_within_diam_index))
    
    ### FUNCTION TO FIND THE CLOSEST POINT OF THE ITERATIONS
    def find_closest_diameter(self):
        '''
        This function finds the closest diameter to the maximum diameter range
        INPUTS:
            self: class object
        OUTPUTS:
            self.iteration_index: [int] index of the largest diameter within the range of diameter interest
            self.large_diameter_within_diam_value: [float] diameter value in nanometers
            self.largest_energy: [float] energy of the iteration
            self.largest_diameter_phi: [float] phi value of the iteration
        '''
        ## FINDING LARGEST DIAMETER
        self.iteration_index  = self.iter_within_diam_index[np.argmax(self.diameters_within_range)]
        self.iteration_diameter  = self.diameters_list_nm[self.iteration_index]
        
        ## FINDING OTHER INFORMATION
        self.largest_num_atoms = self.exported_vcsgc_results.extract_xyz_file.num_nonvacant_atoms[self.iteration_index]
        self.largest_energy = self.exported_vcsgc_results.extract_dump_file.extract_data['potential_energy'][self.iteration_index] # Potential energy
        self.largest_diameter_phi = self.exported_vcsgc_results.extract_dump_file.extract_data['phi'][self.iteration_index] # PHI
        
        ## PRINTING
        print("\n***FINDING DIAMETER CLOSEST TO MAXIMA WITHIN THIS RANGE***")
        print("DIAMETER CLOSEST VALUE (nm): %.3f"%(self.iteration_diameter) )
        print("TOTAL NUMBER OF NONVACANT ATOMS: %d"%(self.largest_num_atoms) )
        print("ENERGY VALUE: %.3f"%(self.largest_energy))
        print("PHI VALUE: %.3f"%(self.largest_diameter_phi))
        return
    
    ### FUNCTION TO FIND THE LOWEST ENERGY OF THE ITERATIONS
    def find_lowest_energy(self):
        '''
        The purpose of this function is to find the lowest energy out of the iterations given the iterations
        INPUTS:
            self: class object
        OUTPUTS:
            self.lowest_energy_iteration_index: index of the lowest energy iteration
            self.lowest_energy_iteration_value: value of the lowest energy iteration
            self.lowest_energy_diameter_nm: diameter in nm of the lowest energy
            self.lowest_energy_phi: phi of the lowest energy
        '''
        ## DEFINING ENERGIES
        energies = self.exported_vcsgc_results.extract_dump_file.extract_data['potential_energy']
        ## FINDING LOWEST ENERGIES
        self.lowest_energy_iteration_index = self.iter_within_diam_index[np.argmin( energies[self.iter_within_diam_index] )]
        ## FINDING LOWEST ENERGY VALUE
        self.lowest_energy_iteration_value = energies[self.lowest_energy_iteration_index]
        ## FINDING CORRESPONDING DIAMETER
        self.lowest_energy_diameter_nm = self.exported_vcsgc_results.extract_xyz_file.diameters[self.lowest_energy_iteration_index] / 10.0 
        ## FINDING CORRESPONDING PHI VALUE
        self.lowest_energy_phi = self.exported_vcsgc_results.extract_dump_file.extract_data['phi'][self.lowest_energy_iteration_index]
        ## PRINTING
        print("\n***FINDING LOWEST ENERGY WITHIN DIAMETER RANGE***")
        print("LOWEST ENERGY INDEX: %d"%(self.lowest_energy_iteration_index))
        print("LOWEST ENERGY VALUE: %.3f"%(self.lowest_energy_iteration_value))
        print("LOWEST ENERGY DIAMETER (nm): %.3f"%(self.lowest_energy_diameter_nm))
        print("PHI VALUE: %.3f"%(self.lowest_energy_phi))
        
    ### FUNCTION TO PLOT ENERGY VS DIAMETER
    def plot_energy_vs_diameter(self):
        '''
        The purpose of this function is to plot energy versus diameter of the diameters within the range. Then, this function will delineate the lowest energy value
        INPUTS:
            self: class object
        OUTPUTS:
            plot with potential energy versus diameter (nm)
        '''
        ## IMPORTING MODULES
        import matplotlib.pyplot as plt
        ## CLOSING ALL FIGURES
        plt.close('all')
        
        ## DEFINING INPUT VARIABLES
        energies = self.exported_vcsgc_results.extract_dump_file.extract_data['potential_energy'][self.iter_within_diam_index]
        diameters_nm = self.exported_vcsgc_results.extract_xyz_file.diameters[self.iter_within_diam_index] / 10.0
        ## CREATING PLOT
        fig = plt.figure(); ax = fig.add_subplot(111)
        ## SETTING X-AXIS
        ax.set_xlim( (self.min_diam, self.max_diam) )
        ## DEFINING X AND Y AXIS
        ax.set_xlabel('Diameter (nm)', **LABELS)
        ax.set_ylabel('Potential energy', **LABELS)
        ## PLOTTING ALL POINTS WITHIN THE RANGE
        ax.scatter( diameters_nm, energies, color='k', **LINE_STYLE)
        ## PLOTTING MINIMUM
        ax.axvline( x = self.lowest_energy_diameter_nm, linestyle = '--', color='red', label='Lowest energy',**LINE_STYLE )
        ax.axhline( y = self.lowest_energy_iteration_value, linestyle = '--', color='red', **LINE_STYLE )
        ## ADDING LEGEND
        ax.legend()
    
    ### FUNCTION TO PLOT LOWEST ENERGY CONFIGURATION
    def plot_lowest_energy_config(self):
        '''
        The purpose of this function is to plot the configuration with the lowest energy
        INPUTS:
            self: class object
        OUTPUTS:
            plot of the configuration
        '''
        ## PLOTTING THE ITERATION
        self.plot_vcsgc_results.plot_structure_for_frame(self.lowest_energy_iteration_index)
        return
    
    ### FUNCTION TO FIND THE DETAILS ABOUT THE ENTIRE LATTICE
    def find_lattice_info(self, iteration_index):
        '''
        The purpose of this function is to find the center and the radius of the spherical lattice. It will also find the closest point to the center. 
        INPUTS:
            self: class object
            iteration_index: iteration to look for in terms of lattice information
        OUTPUTS:
            self.lattice_geometry: [np.array, shape=(N, 3)] Geometry of the entire lattice
            self.lattice_center: [np.array, shape=(3,1)] center of the lattice, approximated by the averaging of all atom positions
            self.lattice_radius: [float] approximate lattice radius
            self.lattice_closest_center_atom_index: [int] intex of atom that is the center
            self.lattice_closest_center_atom_positions: [np.array, shape=(3,1)] position of the point closest to the lattice center
        '''
        ## DEFINING ENTIRE GEOMETRY OF THE LATTICE
        self.lattice_geometry = self.exported_vcsgc_results.extract_xyz_file.xyz_file.extract_geometry[iteration_index]
        ## FINDING CENTER OF THE LATTICE
        self.lattice_closest_center_atom_index, self.lattice_closest_center_atom_positions , self.lattice_radius = find_atom_closest_center( self.lattice_geometry )
        
        ## PRINTING
        print("\n*** FINDING LATTICE INFORMATION ***")
        print("LATTICE RADIUS: %s"%(self.lattice_radius))
        print("CLOSEST CENTER LATTICE: %s"%(np.array2string(self.lattice_closest_center_atom_positions)) )
        
    ### FUNCTION TO MOVE LATTICE TO THE CENTER
    def move_lattice_center(self, lattice_closest_center_atom_positions, lattice_geometry, dist_threshold=0.200):
        '''
        The purpose of this function is to move the lattice to the center. This is done by finding the approximate center of the nonvacant sites, then moving the entire residue to the center
        INPUTS:
            self: class object
            lattice_closest_center_atom_positions: [np.array, shape=(1,3)] lattice position closest to the center
            lattice_geometry: [np.array, shape=(N, 3)] geometry of the entire lattice
            dist_threshold: distance in angstroms to allow for error in moving the atoms to the center
        OUTPUTS:
            self.nonvacant_geom: [np.array, shape=(nonvacant, 3)] geometry of nonvacant sites (prior to centering)
            self.distance_from_center: [np.array, shape=(3, 1)] distance from centers of the nonvacant center atom and the center of the lattice
        '''
        ## FINDING ALL OCCUPIED LATTICE GEOMETRIES
        self.nonvacant_geom= self.exported_vcsgc_results.extract_xyz_file.geometry_nonvacant[self.iteration_index]            
        ## FINDING CENTER OF GEOMETRY
        nonvacant_center = np.mean(self.nonvacant_geom, axis = 0)
        ## FINDING DISTANCES FROM THE CENTER
        nonvacant_distance_from_center = np.sqrt(  np.sum( (self.nonvacant_geom - nonvacant_center)**2, axis=1 ) )
        ## FINDING CLOSEST CENTER ATOM TO THE CENTER
        nonvacant_closest_center_atom_index = np.argmin(nonvacant_distance_from_center)
        self.nonvacant_closest_center_atom_geom = self.nonvacant_geom[nonvacant_closest_center_atom_index]
        ## FINDING DISTANCE VECTOR BETWEEN CENTER OF NONVACANT AND ENTIRE SHELL
        self.distance_from_center = lattice_closest_center_atom_positions - self.nonvacant_closest_center_atom_geom
        ## FINDING NEW GEOMETRY FOR THE LATTICE
        self.nonvacant_geom_center = self.nonvacant_geom + self.distance_from_center
        ## START MATCHING NONVACANT MOVED POINTS TO LATTICE POINTS
        ## NOTE: This is necessary because of the error associated with three decimal places
        for index, each_nonvacant_center in enumerate(self.nonvacant_geom_center):
            ## FINDING DISTANCES BETWEEN LATTICE POINTS AND NONVACANT GEOMETRY
            distances = np.sqrt(np.sum((lattice_geometry - each_nonvacant_center)**2, axis=1))
            ## FINDING MINIMUM DISTANCE 
            index_of_min = np.argmin(distances)
            dist_of_min = np.round(distances[index_of_min],decimals = 4)
            if dist_of_min > dist_threshold:
                print("POSSIBLE ERROR! MOVING NONVACANT ATOMS TO CENTER HAS A GREATER DISTANCE THRESHOLD OF %.3f!"%(dist_threshold))
                print("DISTANCE TO MINIMUM POINT: %.3f"%(dist_of_min) )
            ## CHANGING LOCATION
            self.nonvacant_geom_center[index] =lattice_geometry[index_of_min]
        return
    
    ### FUNCTION TO TRUNCATE THE SPHERICAL LATTICE
    def truncate_lattice_sites_spherical(self):
        '''
        The purpose of this function is to truncate the lattice sites in a form of spheres
        INPUTS:
            self: class object
        OUTPUTS:
            self.radius_cutoff: [float] radius cutoff for the spherical lattice in angstroms
            self.lattice_truncated: [np.array, shape=(N,3)] geometry of the lattice that is truncated
        '''
        ## FINDING CUTOFF RADIUS
        self.radius_cutoff = ( self.diam + self.delta_diam_threshold_cutoff ) / 2.0 * 10 # in angstroms
        ## FINDING DISTANCES FROM THE CENTER ATOM
        distance_from_center = np.sqrt(np.sum((self.lattice_geometry - self.lattice_closest_center_atom_positions)**2,axis=1))
        ## FINDING LATTICE THAT IS CUTOFF
        index_within_radius = np.where(distance_from_center<self.radius_cutoff)
        
        if index_within_radius[0].size == 0:
            print("NO INDEX FOUND WITHIN THIS RADIUS! POSSIBLE ERROR, CHECK YOUR CUTOFF RADIUS")
        self.lattice_truncated =  self.lattice_geometry[index_within_radius]
        return
        
    ### FUNCTION TO GET AN UPDATED COORDINATE LIST
    def update_coordinate_list(self, lattice_geometry, nonvacant_geometry, distance_threshold = 0.01 ):
        '''
        The purpose of this function is to get an updated coordinate list
        INPUTS:
            self: class object
            lattice_geometry: [np.array, shape=(N,3)] lattice geometry of all the points
            nonvacant_geometry: [np.array, shape=(num_nonvacant,3)] lattice geometry of all nonvacant points.
                NOTE: It is assumed that nonvacant geometries occupy spaces of lattice geometries! If not, this function will not work correctly (how would you tell if it is a vacant lattice site then?)
            distance_threshold: distance threshold where if greater than, the python script will yell at you
        OUTPUTS:
            self.atom_type: [np.array, shape=(N,1)] Type 1 represents nonvacant, and type 2 represents vacant. This lists updated vacant and nonvacant sites based on lattice geometry
        '''
        ## GENERATING DEFAULT ATOM LIST
        self.atom_type = np.ones( (len(lattice_geometry), 1) )*2 # Start with 2 to represent vacant
        ## NOW, LOOPING THROUGH NONVACANT GEOMETRY AND FINDING THE INDEX IN THE LATTICE GEOMETRY
        for index, each_nonvacant_coord in enumerate(nonvacant_geometry):
            ## FINDING THE CLOSEST COORDINATE
            distance =  np.sqrt(np.sum((lattice_geometry - each_nonvacant_coord)**2, axis=1))
            min_index = np.argmin(distance)
            min_distance = distance[min_index]
            ## CHECKING DISTANCE
            if min_distance > distance_threshold:
                print("ERROR! MINIMUM DISTANCE (%.3f) IS GREATER THAN DISTANCE THRESHOLD OF (%.3f)"%(min_distance , distance_threshold))
                print("THERE MAY BE AN ERROR ON THE COORDINATE LIST FOR ATOM: %d"%(min_index))
            else:
                if (index % 50) == 0:
                    print("UPDATED COORDINATE LISTS FOR INDEX: %d"%(index))
            ## UPDATING THE ATOM TYPE, 1 REPRESENTS NONVACANT
            self.atom_type[min_index] = 1
        return
    
    ### FUNCTION TO MOVE ENTIRE COORDINATE SYSTEM TO THE CENTER OF THE BOX
    def move_coordinates_box_center(self):
        '''
        The purpose of this function is to take your current lattice, center position, and radius to move the entire lattice back to some zero value
        INPUTS:
            self: class object
        OUTPUTS:
            self.box_length: [float] box length of final spherical lattice
            self.box_center [np.array, shape=(1,3)]: updated box center
            self.lattice_truncated: [np.array, shape=(N,3) updated truncated lattice geometry
        '''
        ## FINDING BOX LENGTH BASED ON CUTOFF RADIUS
        self.box_length = self.radius_cutoff*2.0 
        ## FINDING CENTER OF THE BOX
        self.box_center = np.array( [self.radius_cutoff]*3 ) # CENTER BASED ON THE RADIUS OF THE CUTOFF
        ## FINDING TRANSLATIONAL VECTOR
        translation_vector = self.box_center - self.lattice_closest_center_atom_positions
        ## TRANSLATING ENTIRE LATTICE
        self.lattice_truncated =  self.lattice_truncated + translation_vector
        self.nonvacant_geom_center = self.nonvacant_geom_center + translation_vector
        return

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### SEEING IF TEST IS TRUE
    testing = True # False if you are on command line
    
    ################################
    ### DEFINING INPUT VARIABLES ###
    ################################
    if testing is True:
        ## DIRECTORY AND FILES 
        # input_file=r"transfer_7nm_PHITRANS-0.00008_PHIINC-0.00001_from_10nm_10kappa_vcsgc_-0.15to-.75phi_-0.01inc_Trial_3" # "transfer_2nm_PHITRANS-0.05_PHIINC-0.0002_from_8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc_Trial_3"
        input_file=r"10nm_10kappa_vcsgc_-0.15to-.75phi_-0.01inc" # "transfer_2nm_PHITRANS-0.05_PHIINC-0.0002_from_8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc_Trial_3"
        input_dir = r'R:\scratch\LigandBuilder\Prep_Gold_Core\vcsgc_files\simulations' + '/' + input_file # 8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc' 
        input_dir = r'R:\scratch\nanoparticle_project\prep_system\prep_gold\vcsgc_simulations\simulations' + '/' + input_file # 8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc' 
        # input_dir = r'/Volumes/akchew/scratch/LigandBuilder/Prep_Gold_Core/vcsgc_files/simulations' + '/' + input_file # 8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc' 
        # input_dir = r'R:\scratch\LigandBuilder\Prep_Gold_Core\vcsgc_files\test_files' 
        input_pickle_file = 'vcsgc.pickle'        
        ## DEFINING DIAMETER RANGES
        diam = 4 # 2
        ## DEFINING OUTPUT DIRECTORY
        output_dir = r'R:\scratch\LigandBuilder\Prep_Gold_Core\vcsgc_files\scripts'
        ## DEFINING OUTPUT FILES
        output_file = 'input_vcsgc'
        ## DEFINING FIGURESs
        want_figures = True
    else:
        ## ON COMMAND LINE
        from optparse import OptionParser # Used to allow commands within command line
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        ### ADDING OPTIONS
        parser.add_option("--dir", dest="input_dir", action="store", type="string", help="Input directory", default="test_dir")
        parser.add_option("--pickle", dest="input_pickle_file", action="store", type="string", help="Input pickle", default="vcsgc.pickle")
        parser.add_option("--odir", dest="output_dir", action="store", type="string", help="Output directory", default='input_vcsgc')
        parser.add_option("--out", dest="output_file", action="store", type="string", help="Input pickle", default='input_vcsgc')
        parser.add_option("--diam", dest="diam", action="store", type="float", help="Desired diameter", default='2')

        ### TAKING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
    
        input_dir = options.input_dir                       # Input directory
        input_pickle_file = options.input_pickle_file       # Input pickle file
        output_dir = options.output_dir                     # Output directory
        output_file = options.output_file                   # Output file names
        diam = options.diam                                 # Diameter in nms
        want_figures = False
    ###################
    ### MAIN SCRIPT ###
    ###################
    ## DEFINING PATHS
    path_input_pickle = input_dir + '/' + input_pickle_file
    
    ## LOADING PICKLE FILE
    exported_vcsgc_results = load_pickle_vcsgc(path_input_pickle)
    
    #'''
    if testing is True:
        plot_vcsgc_results = plot_vcsgc(exported_vcsgc_results.extract_xyz_file, exported_vcsgc_results.extract_dump_file )
        plot_vcsgc_results.plot_num_atom_vs_phi()
        plot_vcsgc_results.plot_structure_for_frame(0)
        ## DEFINING VARIABLES
        diameters = plot_vcsgc_results.extract_xyz_file.diameters
        phi = plot_vcsgc_results.extract_dump_file.extract_data['phi']
        number_atoms = plot_vcsgc_results.extract_xyz_file.num_nonvacant_atoms
    # plot_vcsgc_results.plot_structure_all_frames()
    #'''
    #%%
    ### ANALYZING RESULTS
    vcsgc_analyzed = report_vcsgc(exported_vcsgc_results, diam, output_dir, output_file, want_figures=want_figures)
    