#!/bin/bash

# transfer_vcsgc.sh
# The purpose of this script is to get the vcsgc simulation results, then find diameter ranges of our own interest. This script is important for re-running vcsgc simulations to get accurate diameter sizes. We will use a python script to analyze the current simulations pickle file. We assume that you have already run analyze_vcsgc.py script, which would analyze the xyz + dump file and create a pickle file for you.
# Created on: 04/06/2018
# Author(s):
#   Alex K. Chew (alexkchew@gmail.com)

## USAGE: bash transfer_vcsgc.sh

## LOADING GLOBAL VARIABLES
source "../bin/vcsgc_rc.sh"

#######################
### INPUT VARIABLES ###
#######################

## PYTHON SCRIPT TO ENABLE TRANSFER VCSGC
transfer_python="${PATH_SCRIPTS}/report_vcsgc.py"

## DEFINING INPUT DIRECTORY AND PICKLE FILE
# NOTE: This is important, the directory and pickle file should have the diameter range you care about. If not, then there no phi value, and we cannot guess it!
# input_dir="8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc"
input_dir="10nm_10kappa_vcsgc_-0.15to-.75phi_-0.01inc"
input_pickle="vcsgc.pickle"

## DIAMETER: Desired diameter that you want
desired_diameter="7" # in nms

## DEFINING TRIAL NUMBER
trial_num="3"

## INPUT FILES
input_run_file="${PATH_INPUT_RUN}/run_vcsgc_adjustable_transfer.lmpin" # run file for lammps
    # ADJUSTABLE PARAMETERS:
        # CAPRADIUS: cap swapping radius in Angstroms (e.g. 30)
        # INITRAD: Initial radius
        # SPHERECENTER: larger radius in Angstroms (e.g. 60)
        # BOXLENGTH: Box length (needs to fit the radius)
        # KAPPA: Constant for embedded atom potential
        # PHI_START: Starting value for PHI
        # PHI_LOOP: Number of phi loops
        # PHI_INC: increments of phi
        # INPUTXYZ: input xyz file
    ## NOTE: "read_position variable is set to 1 -- automatic insertion of xyz file

input_execute="${PATH_INPUT_EXECUTE}/lmp_rvl_execute" # executible that lammps uses
input_run_lammps_script="${PATH_INPUT_RUN}/execute_lammps.sh" # script that runs lammps
input_submission="${PATH_INPUT_SUBMISSION}/submit_lammps.sh" # script that submits to slurm/jobs
input_alloy="${PATH_INPUT_ALLOY}/Au-Grochola-JCP05.eam.alloy" # alloy information

## SIMULATION PARAMETERS

## KAPPA: Defines sampling of particle size
KAPPA="10" # Previously: 10.0

## PHI
PHI_INC="-0.00001" #  -0.0002 Default: -0.0002
PHI_TRANS="-0.00008" # "-0.0001"  #  -0.01 Differences in translation of phi -0.02 -0.05 -0.04

# for 7 nm: -0.0001 -> -0.00008
# for 6 nm: -0.005 -> -0.0001
# PHI_INC: -0.00001

########################
### OUTPUT VARIABLES ###
########################

## DEFINING OUTPUT IFLES
output_run_file="run_vscgc.lmpin" # run file for lammps
output_submit_file="submit.sh" # submission file
output_execute_lammps="execute_lammps.sh" # executable file

## DEFINING OUTPUT DIRECTORY AND FILE NAME
output_dir="transfer_${desired_diameter}nm_PHITRANS${PHI_TRANS}_PHIINC${PHI_INC}_from_${input_dir}_Trial_${trial_num}"

## DEFINING OUTPUT FILE NAMES FOR XYZ AND PARS
output_name="init_vcsgc" # output name used for transferring coordinates

## OUTPUT PREFIX
output_prefix="vcsgc" # prefix used for the output

######################
### DEFINING PATHS ###
######################

## PATH TO INPUT FOLDER
input_path="${PATH_SIM}/${input_dir}"

## PATH TO OUTPUT FOLDER
output_path="${PATH_SIM}/${output_dir}"

###################
### MAIN SCRIPT ###
###################

## FINDING RANDOM NUMBER: USED TO GENERATE RANDOM MONTE CARLO SIMS
random_num=$(seed_random_num)

## CREATING DIRECTORY
check_output_dir "${output_path}"

## GOING INTO DIRECTORY
cd "${output_path}"

## RUNNING PYTHON SCRIPT
python3 "${transfer_python}" --dir "${input_path}" --pickle "${input_pickle}" --odir "${output_path}" --diam "${desired_diameter}" --out "${output_name}"

## CHECK IF FILES EXIST, IF SO, THEN KEEP GOING. OTHERWISE, STOP HERE!
if [ ! -e "${output_name}.xyz" ] || [ ! -e "${output_name}.parameters" ]; then
    echo "ERROR! Either ${output_name}.xyz or ${output_name}.parameters does not exist!"
    echo "We cannot continue without these files. Stopping here!"
    exit
else
    echo "All ${output_name} xyz and parameter files correctly loaded! Continuing..."
fi

## COPYING ALL IMPORTANT FILES
cp -rv {${input_alloy},${input_execute}} "${output_path}"
cp -rv "${input_run_file}" "${output_run_file}" # Run file for lammps
cp -rv "${input_run_lammps_script}" "${output_execute_lammps}" # Executing lammps script
cp -rv "${input_submission}" "${output_submit_file}" # Submission file

######################################
### CALCULATING PARAMETERS FOR SIM ###
######################################

## FINDING DIAMETER AND PHI FROM INPUT FILES
diameter=$(grep "diameter" ${output_name}.parameters  | awk '{print $2}')
PHI_START=$(grep "phi" ${output_name}.parameters  | awk '{print $2}')
swaprad=$(grep "swaprad" ${output_name}.parameters  | awk '{print $2}') # swap radius in angstroms

## DEFINING PHI BOUNDS
# END POINT FOR PHI
PHI_END=$(awk -v phi_start=${PHI_START} -v phi_trans="${PHI_TRANS}" 'BEGIN{ printf "%.6f",(phi_start+phi_trans)'})
# NUMBER OF PHI LOOPS
num_phi_loops=$(awk -v phi_start=${PHI_START} -v phi_end="${PHI_END}" -v phi_inc="${PHI_INC}" 'BEGIN{ printf "%d",(phi_end-phi_start)/(phi_inc)}')

## DEFINING BOX BOUNDS
# INITIAL RADIUS
radius_initial_ang=$(awk -v diam=${diameter} 'BEGIN{ printf "%.4f",(diam/2.0*10)}')
# BOX LENGTH
box_length=$(awk -v rad=${swaprad} 'BEGIN{ printf "%.3f",rad*2}')
sphere_center=$(awk -v box_length=${box_length} 'BEGIN{ printf "%.3f",box_length/2.0}')

##################################
### EDITING EXECUTABLE SCRIPT ####
##################################
## EDITING RUN FILE
echo "EDITING ${output_run_file}..."
## INITIAL RADIUS
sed -i "s/INITRAD/${radius_initial_ang}/g" "${output_run_file}" # Radius of initial
## PHI VALUES
sed -i "s/PHI_START/${PHI_START}/g" "${output_run_file}" # Starting value for phi
sed -i "s/PHI_LOOP/${num_phi_loops}/g" "${output_run_file}" # Starting value for phi
sed -i "s/PHI_INC/${PHI_INC}/g" "${output_run_file}" # Increment for phi
## KAPPA VALUES
sed -i "s/KAPPA/${KAPPA}/g" "${output_run_file}" # Kappa
## LARGEST RADIUS
sed -i "s/CAPRADIUS/${swaprad}/g" "${output_run_file}" # Radius that is a maximum
## SPHERE CENTER
sed -i "s/SPHERECENTER/${sphere_center}/g" "${output_run_file}" # Larger radius for vacuum-like
## BOX LENGTH
sed -i "s/BOXLENGTH/${box_length}/g" "${output_run_file}" # Box length  

## EDITING XYZ FILE NAME
sed -i "s/INPUTXYZ/${output_name}.xyz/g" "${output_run_file}" # XYZ file 

## EDITING EXECUTABLE LAMMPS
echo "EDITING ${output_execute_lammps}..."
sed -i "s/EXECUTE_FILE/$(basename "${input_execute}")/g" "${output_execute_lammps}" # Editing executible file
sed -i "s/RUN_FILE/${output_run_file}/g" "${output_execute_lammps}" # Editing run file
sed -i "s/PREFIX/${output_prefix}/g" "${output_execute_lammps}" # Editing output prefix
sed -i "s/LOG_NAME/${output_prefix}.log/g" "${output_execute_lammps}" # Editing log name
sed -i "s/SEED/${random_num}/g" "${output_execute_lammps}" # Seed number

## EDITING SUBMISSION SCRIPT
echo "EDITING ${output_submit_file}..."
sed -i "s/JOB_NAME/${output_dir}/g" "${output_submit_file}" # Job name
sed -i "s/EXECUTE_LAMMPS_SCRIPT/${output_execute_lammps}/g" "${output_submit_file}" # Execute lammps script

## PRINTING SUMMARY
echo -e "\n*** SUMMARY ***"
echo "STARTING PHI VALUE: ${PHI_START}"
echo "PHI INCREMENT: ${PHI_INC}"
echo "END PHI VALUE: ${PHI_END}"
echo "TOTAL PHI LOOPS: ${num_phi_loops}"
echo "---------------------------------"
echo "BOX LENGTH (Angstroms): ${box_length}"
echo "SPHERE CENTER (Angstroms): ${sphere_center}, ${sphere_center}, ${sphere_center}"
echo "---------------------------------"
echo "SEED: ${random_num}"
echo "DESIRED DIAMETER (nm): ${desired_diameter}"
echo "INITIAL DIAMETER (nm): ${diameter}"
echo "INPUT DIRECTORY: ${input_dir}"
echo "OUTPUT DIRECTORY: ${output_dir}"

## ADDING TO JOB FILE
echo "ADDING ${output_dir} TO ${PATH_JOB_FILE}"
echo "${output_path}" >> ${PATH_JOB_FILE}