#!/bin/bash

# prep_vcsgc.sh
# The purpose of this script is to prepare vcsgc simulations. We will have to vary things like the initial radius, etc. We will need to edit the "run_vcsgc.lmpin" script to appropriately run the jobs.
# Created on 03/22/2018
# Author(s):
#   Alex K. Chew (alexkchew@gmail.com)

## LOADING GLOBAL VARIABLES
source "../bin/vcsgc_rc.sh"

#######################
### INPUT VARIABLES ###
#######################

## DIAMETER: Shape of desired sphere
diameter="10" # in nms

## KAPPA: Defines sampling of particle size
KAPPA="10" # Previously: 10.0

## PHI
PHI_START="-0.15" # "-0.15" # Lower limit: 0.001
PHI_END="-.75" # SHOULD NOT BE MORE THAN -2
PHI_INC="-0.01" # Default: -0.0002

## INPUT FILES
input_run_file="${PATH_INPUT_RUN}/run_vcsgc_adjustable.lmpin" # run file for lammps
# ADJUSTABLE PARAMETERS:
    # CAPRADIUS: cap swapping radius in Angstroms (e.g. 30)
    # INITRAD: Initial radius
    # SPHERECENTER: larger radius in Angstroms (e.g. 60)
    # BOXLENGTH: Box length (needs to fit the radius)
    # KAPPA: Constant for embedded atom potential
    # PHI_START: Starting value for PHI
    # PHI_LOOP: Number of phi loops
    # PHI_INC: increments of phi
    
input_execute="${PATH_INPUT_EXECUTE}/lmp_rvl_execute" # executible that lammps uses
input_run_lammps_script="${PATH_INPUT_RUN}/execute_lammps.sh" # script that runs lammps
input_submission="${PATH_INPUT_SUBMISSION}/submit_lammps.sh" # script that submits to slurm/jobs
input_alloy="${PATH_INPUT_ALLOY}/Au-Grochola-JCP05.eam.alloy"

########################
### OUTPUT VARIABLES ###
########################

## OUTPUT PARENT
output_parent_dir="${PATH_SIM}" # path to simulations

## OUTPUT DIRECTORY
output_dir="${diameter}nm_${KAPPA}kappa_vcsgc_${PHI_START}to${PHI_END}phi_${PHI_INC}inc"

## OUTPUT FULL PATH
output_full_path_dir="${output_parent_dir}/${output_dir}"

## OUTPUT PREFIX
output_prefix="vcsgc" # prefix used for the output

## OUTPUT FILES
output_run_file="run_vscgc.lmpin" # run file for lammps
output_submit_file="submit.sh" # submission file
output_execute_lammps="execute_lammps.sh" # executable file

###################
### MAIN SCRIPT ###
###################

## CHECKING THE OUTPUT DIRECTORY
check_output_dir "${output_full_path_dir}"

## GOING INTO DIRECTORY
cd "${output_full_path_dir}"

## COPYING OVER IMPORTANT FILES
echo "*** COPYING INPUT FILES ***"
cp -rv {${input_alloy},${input_execute}} "${output_full_path_dir}" # Alloy and executable
cp -rv "${input_run_file}" "${output_run_file}" # Run file for lammps
cp -rv "${input_run_lammps_script}" "${output_execute_lammps}" # Executing lammps script
cp -rv "${input_submission}" "${output_submit_file}" # Submission file

## DEFINING INITIAL RADIUS
radius_initial_ang="3" # Angstroms

## DEFINING BOUNDS
# diameter_lower_ang=$(awk -v diam=${diameter} 'BEGIN{ printf "%.3f",diam-.25}')
# diameter_upper_ang=$(awk -v diam=${diameter} 'BEGIN{ printf "%.3f",diam+.25}')
# radius_initial_ang=$(awk -v diam=${diameter_lower_ang} 'BEGIN{ printf "%.3f",diam/2.0*10}') # Initial radius (e.g. 3 Angstroms)
radius_upper_ang=$(awk -v diam=${diameter} 'BEGIN{ printf "%.3f",diam/2.0*10}') # Largest radius possible in angstroms

## FINDING RADIUS OF LARGER
# large_radius_ang=$(awk -v init_rad=${radius_upper_ang} 'BEGIN{ printf "%.3f",init_rad*2}')

## FINDING BOX LENGTH
box_length=$(awk -v rad=${radius_upper_ang} 'BEGIN{ printf "%.3f",rad*2}')

## FINDING SPHERE CENTER
sphere_center=$(awk -v box_length=${box_length} 'BEGIN{ printf "%.3f",box_length/2.0}')

## FINDING PHI LOOPS
num_phi_loops=$(awk -v phi_start=${PHI_START} -v phi_end="${PHI_END}" -v phi_inc="${PHI_INC}" 'BEGIN{ printf "%d",(phi_end-phi_start)/(phi_inc)}')

#####################
### EDITING FILES ###
#####################
echo -e "\n*** EDITING FILES ***\n"

## FINDING RANDOM NUMBER: USED TO GENERATE RANDOM MONTE CARLO SIMS
random_num=$(seed_random_num)

## EDITING EXECUTABLE SCRIPT
echo "EDITING ${output_run_file}..."
sed -i "s/INITRAD/${radius_initial_ang}/g" "${output_run_file}" # Radius of initial
sed -i "s/PHI_START/${PHI_START}/g" "${output_run_file}" # Starting value for phi
sed -i "s/PHI_LOOP/${num_phi_loops}/g" "${output_run_file}" # Starting value for phi
sed -i "s/PHI_INC/${PHI_INC}/g" "${output_run_file}" # Increment for phi
sed -i "s/KAPPA/${KAPPA}/g" "${output_run_file}" # Kappa
sed -i "s/CAPRADIUS/${radius_upper_ang}/g" "${output_run_file}" # Radius that is a maximum
sed -i "s/SPHERECENTER/${sphere_center}/g" "${output_run_file}" # Larger radius for vacuum-like
sed -i "s/BOXLENGTH/${box_length}/g" "${output_run_file}" # Box length

## EDITING SUBMISSION SCRIPT
echo "EDITING ${output_submit_file}..."
sed -i "s/JOB_NAME/${output_dir}/g" "${output_submit_file}" # Job name
sed -i "s/EXECUTE_LAMMPS_SCRIPT/${output_execute_lammps}/g" "${output_submit_file}" # Execute lammps script

## EDITING EXECUTABLE LAMMPS
echo "EDITING ${output_execute_lammps}..."
sed -i "s/EXECUTE_FILE/$(basename "${input_execute}")/g" "${output_execute_lammps}" # Editing executible file
sed -i "s/RUN_FILE/${output_run_file}/g" "${output_execute_lammps}" # Editing run file
sed -i "s/PREFIX/${output_prefix}/g" "${output_execute_lammps}" # Editing output prefix
sed -i "s/LOG_NAME/${output_prefix}.log/g" "${output_execute_lammps}" # Editing log name
sed -i "s/SEED/${random_num}/g" "${output_execute_lammps}" # Seed number

## SUMMARY
echo -e "\n*** SUMMARY ***"
echo "DESIRED DIAMETER (nm): ${diameter}"
echo "OUTPUT DIRECTORY: ${output_dir}"
echo "INITIAL SPHERE RADIUS (Angstroms): ${radius_initial_ang}"
# echo "INITIAL LARGE SPHERE RADIUS (Angstroms): ${large_radius_ang}"
echo "LARGEST SPHERE RADIUS POSSIBLE (Angstroms): ${radius_upper_ang}"
echo "BOX LENGTH (Angstroms): ${box_length}"
echo "SPHERE CENTER (Angstroms): ${sphere_center}, ${sphere_center},${sphere_center}"
echo "STARTING PHI VALUE: ${PHI_START}"
echo "PHI INCREMENT: ${PHI_INC}"
echo "END PHI VALUE: ${PHI_END}"
echo "TOTAL PHI LOOPS: ${num_phi_loops}"

## ADDING TO JOB FILE
echo "ADDING ${output_dir} TO ${PATH_JOB_FILE}"
echo "${output_full_path_dir}" >> ${PATH_JOB_FILE}