#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
dump_frame.py
The purpose of this script is to read the xyz file and dump the last frame

Created on: 04/10/2018
    
AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)   
"""
### IMPORTING MODULES
from analyze_vcsgc import read_vcsgc_xyz,read_vcsgc_dump, extract_vcsgc_xyz, export_vcsgc, plot_vcsgc, load_pickle_vcsgc, plot_structure_for_frame, calc_total_distance2_matrix
from report_vcsgc import write_xyz_input_lammps, write_dump_input_lammps
import numpy as np

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### SEEING IF TEST IS TRUE
    testing = False # False if you are on command line
    
    ################################
    ### DEFINING INPUT VARIABLES ###
    ################################
    if testing is True:
        ## INPUT FILE
        input_dir = r'R:\scratch\LigandBuilder\Prep_Gold_Core\vcsgc_files/final_structures/2/2nm_from_transfer_2nm_PHITRANS-0.1_PHIINC-0.0002_from_8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc' # 8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc' 
        input_file = r'vcsgc_em.xyz'
       
        ## OUTPUT FILE
        output_file = r'vcsgc_em_final'
        output_dir = r'R:\scratch\LigandBuilder\Prep_Gold_Core\vcsgc_files/final_structures/2/2nm_from_transfer_2nm_PHITRANS-0.1_PHIINC-0.0002_from_8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc'
    
    else:
        ## ON COMMAND LINE
        from optparse import OptionParser # Used to allow commands within command line
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        ### ADDING OPTIONS
        parser.add_option("--idir", dest="input_dir", action="store", type="string", help="Input directory", default="test_dir")
        parser.add_option("--ixyz", dest="input_file", action="store", type="string", help="Input directory", default="test_dir")
        parser.add_option("--odir", dest="output_dir", action="store", type="string", help="Output directory", default='input_vcsgc')
        parser.add_option("--out", dest="output_file", action="store", type="string", help="Ouptut file", default='input_vcsgc')
        
        ### TAKING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        input_dir = options.input_dir                       # Input directory
        input_file = options.input_file                     # Input file (xyz)
        output_dir = options.output_dir                     # Output directory
        output_file = options.output_file                   # Output file names
        
    ## DEFINING PATHS
    input_path = input_dir + '/' + input_file
    output_path_xyz = output_dir + '/' + output_file + '.xyz'
    output_path_dump = output_dir + '/' + output_file + '.parameters'
    
    ## EXTRACTING VCSGC XYZ
    extract_xyz = read_vcsgc_xyz(input_path)
    
    ## GETTING LAST FRAME GEOMETRY
    geometry = extract_xyz.extract_geometry[-1]
    
    ## CALCULATING DISTANCES SQUARED TO GET DIAMETER
    dist2 = calc_total_distance2_matrix(geometry) # Dist^2 in Angstroms
    diameter = np.sqrt(np.max(dist2)) / 10.0 # in nm
    
    ## OUTPUTTING TO DUMP
    write_dump_input_lammps(output_path_dump, diameter = diameter, num_atoms = len(geometry))
    
    ## WRITING TO OUTPUT
    write_xyz_input_lammps(output_path_xyz, geometry, atom_types = np.ones((len(geometry),1) ))
    