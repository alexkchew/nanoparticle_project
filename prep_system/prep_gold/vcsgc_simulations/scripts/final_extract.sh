#!/bin/bash

# final_extract.sh
# The purpose of this script is to take a completed vcsgc simulation, get the lowest energy, output to an xyz file and run a quick energy minimization

# Created on: 04/08/2018
# Author(s):
#   Alex K. Chew (alexkchew@gmail.com)

## LOADING GLOBAL VARIABLES
source "../bin/vcsgc_rc.sh"

#######################
### INPUT VARIABLES ###
#######################

## PYTHON SCRIPT TO ENABLE TRANSFER VCSGC
extract_python="${PATH_SCRIPTS}/extract_vcsgc.py"

## PYTHON SCRIPTS THAT CAN EXTRACT FAMES
frame_python="${PATH_SCRIPTS}/dump_frame.py"

## DEFINING INPUT DIRECTORY AND PICKLE FILE
input_dir="transfer_7nm_PHITRANS-0.00008_PHIINC-0.00001_from_10nm_10kappa_vcsgc_-0.15to-.75phi_-0.01inc_Trial_3"
#"transfer_2nm_PHITRANS-0.05_PHIINC-0.0002_from_10nm_10kappa_vcsgc_-0.15to-.75phi_-0.01inc_Trial_4"
input_pickle="vcsgc.pickle"
input_parameter="init_vcsgc.parameters"

## DEFINING FINAL STRUCTURE TEXT FILE
final_structure_text_file="${PATH_INPUT_FINAL_STRUCT}/final_structure_list.txt"

## DIAMETER: Desired diameter that you want
desired_diameter="7" # in nms

## DELTA DIAMETER: Diameter range to look into, e.g. 0.1 means diam +- 0.1
delta_diam="0.2"  # 0.1 (typical)
# 0.2 for 7 nm case

## DEFINING TRIAL NUMBER
trial_num="3"

## INPUT FILES
input_run_file="${PATH_INPUT_RUN}/run_vcsgc_adjustable_energy_minimization.lmpin" # run file for lammps
input_execute="${PATH_INPUT_EXECUTE}/lmp_rvl_execute" # executible that lammps uses
input_run_lammps_script="${PATH_INPUT_RUN}/execute_lammps_energy_minimization.sh" # script that runs lammps
input_submission="${PATH_INPUT_SUBMISSION}/submit_lammps.sh" # script that submits to slurm/jobs
input_alloy="${PATH_INPUT_ALLOY}/Au-Grochola-JCP05.eam.alloy" # alloy information

########################
### OUTPUT VARIABLES ###
########################

## DEFINING OUTPUT NAME
output_name="output_init"

## DEFINING OUTPUT FILES
output_run_file="run_vscgc_em.lmpin" # run file for lammps
output_submit_file="submit.sh" # submission file
output_execute_lammps="execute_lammps_em.sh" # executable file

## DEFINING OUTPUT DIRECTORY AND FILE NAME
output_dir="${desired_diameter}nm_Trial_${trial_num}"

## OUTPUT PREFIX
output_prefix="vcsgc_em" # prefix used for the output

######################
### DEFINING PATHS ###
######################

## PATH TO INPUT FOLDER
input_path="${PATH_SIM}/${input_dir}"

## PATH TO OUTPUT FOLDER
output_path="${PATH_INPUT_FINAL_STRUCT}/${desired_diameter}/${output_dir}"

###################
### MAIN SCRIPT ###
###################

## CHECKING IF THIS DIRECTORY IS ALREADY STORED ONTO FINAL STRUCTURE LIST
final_struct_check_dir=$(grep -r "\b${input_dir}\b" ${final_structure_text_file})
echo "${final_struct_check_dir}"
if [[ ! -z "${final_struct_check_dir}" ]]; then
    echo "Error! ${input_dir} is already in  ${final_structure_text_file}"
    echo "This input directory is probably done!"
    echo "Stopping here to prevent potential errors!"
    exit
fi

## CREATING DIRECTORY
check_output_dir "${output_path}"

## GOING INTO DIRECTORY
cd "${output_path}"

## RUNNING PYTHON SCRIPT
python3 "${extract_python}" --dir "${input_path}" --pickle "${input_pickle}" --deldiam "${delta_diam}" --diam "${desired_diameter}" --out "${output_name}" --odir "${output_path}"

## CHECK IF FILES EXIST, IF SO, THEN KEEP GOING. OTHERWISE, STOP HERE!
if [ ! -e "${output_name}.xyz" ] || [ ! -e "${output_name}.parameters" ]; then
    echo "ERROR! Either ${output_name}.xyz or ${output_name}.parameters does not exist!"
    echo "We cannot continue without these files. Stopping here!"
    exit
else
    echo "All ${output_name} xyz and parameter files correctly loaded! Continuing..."
fi

## COPYING ALL IMPORTANT FILES
cp -rv {${input_alloy},${input_execute}} "${output_path}"
cp -rv "${input_run_file}" "${output_run_file}" # Run file for lammps
cp -rv "${input_run_lammps_script}" "${output_execute_lammps}" # Executing lammps script
cp -rv "${input_submission}" "${output_submit_file}" # Submission file

## EXTRACTING IMPORTANT INFORMATION FROM PARAMETER FILE
swaprad=$(grep "swaprad" "${input_path}/${input_parameter}"  | awk '{print $2}') # swap radius in angstroms
# BOX LENGTH
box_length=$(awk -v rad=${swaprad} 'BEGIN{ printf "%.3f",rad*2}')

##################################
### EDITING EXECUTABLE SCRIPT ####
##################################
## EDITING RUN FILE
echo "EDITING ${output_run_file}..."
## EDITING XYZ FILE NAME
sed -i "s/INPUTXYZ/${output_name}.xyz/g" "${output_run_file}" # XYZ file 
## LARGEST RADIUS
sed -i "s/CAPRADIUS/${swaprad}/g" "${output_run_file}" # Radius that is a maximum
## BOX LENGTH
sed -i "s/BOXLENGTH/${box_length}/g" "${output_run_file}" # Box length  

## EDITING EXECUTABLE LAMMPS
echo "EDITING ${output_execute_lammps}..."
sed -i "s/EXECUTE_FILE/$(basename "${input_execute}")/g" "${output_execute_lammps}" # Editing executible file
sed -i "s/RUN_FILE/${output_run_file}/g" "${output_execute_lammps}" # Editing run file
sed -i "s/PREFIX/${output_prefix}/g" "${output_execute_lammps}" # Editing output prefix
sed -i "s/LOG_NAME/${output_prefix}.log/g" "${output_execute_lammps}" # Editing log name
sed -i "s/SEED/${random_num}/g" "${output_execute_lammps}" # Seed number

## EDITING SUBMISSION SCRIPT
echo "EDITING ${output_submit_file}..."
sed -i "s/JOB_NAME/${output_dir}/g" "${output_submit_file}" # Job name
sed -i "s/EXECUTE_LAMMPS_SCRIPT/${output_execute_lammps}/g" "${output_submit_file}" # Execute lammps script

## RUNNING ENERGY MINIMIZATION
bash "${output_execute_lammps}"

## CHECKING IF ENERGY MINIMIZATION CORRECTLY COMPLETED
if [ ! -e "${output_prefix}.xyz" ]; then
    echo "ERROR! ${output_prefix}.xyz does not exist!"
    echo "We cannot continue energy minimized file, stopping here!"
    echo "Check energy minimization!"
    exit
else
    echo "${output_prefix}.xyz is correctly available. Continuing!"
fi

#################################
### EXTRACTION OF LAST FRAME  ###
#################################

## RUNNING PYTHON SCRIPT
python3 ${frame_python} --idir ${output_path} --odir ${output_path} --ixyz ${output_prefix}.xyz --out "${output_prefix}_final"

## EXTRACTING FINAL DIAMETER
final_diameter=$(grep "diameter" "${output_prefix}_final.parameters"  | awk '{print $2}')

## ADDING TO THE FINAL STRUCTURES LIST
echo "${desired_diameter}, ${trial_num}, ${output_dir}, ${input_dir}, ${final_diameter}" >> "${final_structure_text_file}"

## PRINTING SUMMARY
echo -e "\n*** SUMMARY ***"
echo "INPUT DIR: ${input_dir}"
echo "DIAMETER (nm): ${desired_diameter}"
echo "DELTA DIAM(nm): ${delta_diam}"
echo "OUTPUT DIR: ${output_path}"
echo "---------------------------------"
echo "FINAL STRUCTURE FILE: ${output_prefix}_final.xyz"
echo "FINAL PARAMETER FILE: ${output_prefix}_final.parameters"
echo "FINAL DIAMETER (nm): ${final_diameter}"
