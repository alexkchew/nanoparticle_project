# -*- coding: utf-8 -*-
"""
extract_vcsgc.py
The purpose of this script is to import pickle from vcsgc and export to something readable by transfer ligands. This should be run after analyze_vcsgc.py, which gets you the pickle file. Furthermore, report_vcsgc.py is useful to re-run jobs

Created on: 04/09/2018

AUTHOR(S):
    Alex K. Chew (alexkchew@gmail.com)  
"""
from analyze_vcsgc import read_vcsgc_xyz,read_vcsgc_dump, extract_vcsgc_xyz, export_vcsgc, plot_vcsgc, load_pickle_vcsgc, plot_structure_for_frame
from report_vcsgc import report_vcsgc

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### SEEING IF TEST IS TRUE
    testing = True # False if you are on command line
    
    ################################
    ### DEFINING INPUT VARIABLES ###
    ################################
    if testing is True:
        ## DIRECTORY AND FILES
        input_file=r"transfer_7nm_PHITRANS-0.0001_PHIINC-0.00001_from_10nm_10kappa_vcsgc_-0.15to-.75phi_-0.01inc_Trial_1" # "transfer_2nm_PHITRANS-0.05_PHIINC-0.0002_from_8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc_Trial_3"
        input_dir = r'R:\scratch\LigandBuilder\Prep_Gold_Core\vcsgc_files\simulations' + '/' + input_file # 8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc' 
        input_dir = r'R:\scratch\nanoparticle_project\prep_system\prep_gold\vcsgc_simulations\simulations' + '/' + input_file # 8nm_10kappa_vcsgc_-0.15to-2phi_-0.02inc' 
        input_pickle_file = 'vcsgc.pickle'    
        ## DEFINING DESIRED DIAMETER
        diam = 7
        ## DEFINING DIAMETER RANGE
        delta_diam = 0.2 # 0.1
        ## WANT FIGURES?
        want_figures = True
        ## DEFINING OUTPUT DIRECTORY
        output_dir='.'
        output_file = 'vcsgc_out.xyz'
        ## Want Figures?
        want_figures = True
    else:
        ## ON COMMAND LINE
        from optparse import OptionParser # Used to allow commands within command line
        # Adding options for command line input (e.g. --ligx, etc.)
        use = "Usage: %prog [options]"
        parser = OptionParser(usage = use)
        ### ADDING OPTIONS
        parser.add_option("--dir", dest="input_dir", action="store", type="string", help="Input directory", default="test_dir")
        parser.add_option("--pickle", dest="input_pickle_file", action="store", type="string", help="Input pickle", default="vcsgc.pickle")
        parser.add_option("--diam", dest="diam", action="store", type="float", help="Desired diameter", default='2')
        parser.add_option("--deldiam", dest="delta_diam", action="store", type="float", help="Deviation from desired diameter", default='0.1')
        parser.add_option("--odir", dest="output_dir", action="store", type="string", help="Output directory", default='input_vcsgc')
        parser.add_option("--out", dest="output_file", action="store", type="string", help="Input pickle", default='input_vcsgc')
        
        ### TAKING ARGUMENTS
        (options, args) = parser.parse_args() # Takes arguments from parser and passes them into "options" and "argument"
        
        input_dir = options.input_dir                       # Input directory
        input_pickle_file = options.input_pickle_file       # Input pickle file
        output_dir = options.output_dir                     # Output directory
        output_file = options.output_file                   # Output file names
        diam = options.diam                                 # Diameter in nms
        delta_diam = options.delta_diam                     # Deviations from diameter in nm
        
        ## Want figures?
        want_figures = False
        
    ###################
    ### MAIN SCRIPT ###
    ###################
    ## DEFINING PATHS
    path_input_pickle = input_dir + '/' + input_pickle_file
    
    ## LOADING PICKLE FILE
    exported_vcsgc_results = load_pickle_vcsgc(path_input_pickle)
    ##
    if testing is True:
        plot_vcsgc_results = plot_vcsgc(exported_vcsgc_results.extract_xyz_file, exported_vcsgc_results.extract_dump_file )
    

    ## ANALYZING VCSGC
    vcsgc_analyzed = report_vcsgc(exported_vcsgc_results, diam, output_dir = output_dir, output_file = output_file,
                                  delta_diam=delta_diam, want_figures=want_figures, report_type='extract')
    
    
    #%%
    
    ## ENERGIES
    potential_energy = vcsgc_analyzed.exported_vcsgc_results.extract_dump_file.extract_data['potential_energy'] # [self.iter_within_diam_index]
    diameters_nm = vcsgc_analyzed.exported_vcsgc_results.extract_xyz_file.diameters / 10.0 # [self.iter_within_diam_index]
    
    
    
    
    