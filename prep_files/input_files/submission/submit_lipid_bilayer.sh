#!/bin/bash 

# submit_lipid_bilayer.sh
# This submission script runs lipid bilayer NPT equilibration
# VARIABLES TO CHANGE:
#   _JOB_NAME_ <-- job name
#   _BASHSCRIPT_ <-- bash script to run

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

# INSTRUCTIONS:
# The -t command specifies how long this simulation will run for (in HOURS:MINUTES:SECONDS). Try to estimate this as best you can
# as the simulation will temrinate after this time.
# The -J flag species the name of the simulation
# the --mail-user command will send you email when the job runs / terminates
# Do not change the other flags, really.

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname

## DEFINING BASH SCRIPT
bash_script="_BASHSCRIPT_"

## RUNNING BASH SCRIPT
bash "${bash_script}"

# OLD RUN COMMANDS
#
### DEFINING INPUT FILES
#em_mdp="_MDPEM_"
#mdp_equil="_MDPEQUIL_"
#output_prefix="_OUTPUTPREFIX_"
#
### ENERGY MINIMIZATION
#gmx grompp -f ${em_mdp} -c ${output_prefix}.gro -p ${output_prefix}.top -o "${output_prefix}_em.tpr"
#gmx mdrun -v -nt 1 -deffnm "${output_prefix}_em"
#
### NVT EQUILIBRATION
#gmx grompp -f "${mdp_equil}" -o ${output_prefix}_equil.tpr -c ${output_prefix}_em.gro -p ${output_prefix}.top -maxwarn 5
#gmx mdrun -nt 28 -v -deffnm ${output_prefix}_equil
