#!/bin/bash 

# run_analysis_num_contacts.sh
# The purpose of this script is to run several windows of bash script for number of contacts
# sampling simulations. 
# VARIABLES:
#   _RUNSCRIPT_ <-- run script
#   _INITIALINDEX_ <-- initial index
#   _FINALINDEX_ <-- final index

## DEFINING INPUTS
num_cores="${1-28}"
em_num_cores="${2-28}"
mdrun_command="${3-gmx mdrun -nt}"
mdrun_command_suffix="${4:-""}"

## DEFINING BASH SCRIPT
run_script="_RUNSCRIPT_"

## DEFINING WINDOWS
initial_index="_INITIALINDEX_"
final_index="_FINALINDEX_"

## ECHOING
echo "---------------------------BEGIN---------------------------"

## LOOPING JOB INDEX
for job_index in $(seq ${initial_index} ${final_index}); do
    echo "------------- Running Window: ${job_index} -------------"
    
    ## RUNNING EACH LAMBDA
    bash "${run_script}" "${job_index}" \
                         "${num_cores}" \
                         "${em_num_cores}" \
                         "${mdrun_command}" \
                         "${mdrun_command_suffix}"
                         
done

echo "---------------------------END---------------------------"
