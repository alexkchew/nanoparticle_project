#!/bin/bash

# run_nplm_umbrella_sampling.sh
# The purpose of this code is to run the umbrella sampling 
# simulation. 
#
# Written by: Alex K. Chew (01/15/2020)
# 
# VARIABLES
#   $1: index that you care about 
#   $2: number of cores
#   $3: number of em cores
#   $4: md run information
#   $5: md run suffix
# VARIABLES TO CHANGE:
#   _INPUTTOPFILE_ <-- topology file name within input folder
#   _OUTPUTPREFIX_ <-- output prefix
#   _MAXWARN_ <-- max warn
#   _EQUIL_MDPFILE_ <-- equilibration MDP file
#   _EM_MDPFILE_ <-- energy minimization mdp file
#   _PROD_MDPFILE_ <-- production mdp file

#######################
### INPUT VARIABLES ###
#######################

## DEFINING VARIABLES
num_cores="${1:-28}"

## ENERGY MINIMIZATION NUMBER OF CORES
em_num_cores="${2:-28}"

## DEFINING GMX MDRUN
mdrun_command="${3-gmx mdrun -nt}"
# mdrun_command="ibrun -np ${num_cores} mdrun_mpi"

## DEFINING MDRUN COMMAND SUFFIX
mdrun_command_suffix="${4:-""}"

## EM AND PROD
em_run_command="${mdrun_command} ${em_num_cores} ${mdrun_command_suffix}"
prod_run_command="${mdrun_command} ${num_cores} ${mdrun_command_suffix}"

##########################
### EDITABLE VARAIBLES ###
##########################

## GETTING DIRECTORY LOCATION
current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING CURRENT SIMULATION PATH
sim_path=$(cd ${current_dir}; pwd)

## DEFINING TOP FILE
input_top_file="_INPUTTOPFILE_"

## DEFINING OUTPUT PREFIX
output_prefix="_OUTPUTPREFIX_"

## DEFINING MDP FILE
em_mdp_file="_EM_MDPFILE_"
equil_mdp_file="_EQUIL_MDPFILE_"
prod_mdp_file="_PROD_MDPFILE_"

## DEFINING MAXWARN
max_warn="_MAXWARN_"

####################
### RUNNING CODE ###
####################

## DEFINING TOP FILE
path_top_file="${sim_path}/${input_top_file}"

## DEFINING PATH TO SIMULATION FILE
path_sim="${sim_path}/${sim_folder}"

## PRINTING
echo "Current simulation folder: ${path_sim}"
cd "${path_sim}"

###########################
### ENERGY MINIMIZATION ###
###########################
echo "------------------------------"
echo "Part 1 - Energy minimization"
echo "------------------------------"

## DEFINING SIM PREFIX
sim_prefix="${output_prefix}_em"

## GENERATING GROMPP
gmx grompp -f "${em_mdp_file}" \
           -o "${sim_prefix}.tpr" \
           -c "${output_prefix}.gro" \
           -p "${path_top_file}" \
           -maxwarn "${max_warn}"

## NPT EQUILIBRATION
if [ ! -e "${sim_prefix}.gro" ]; then
    ## SEEING IF XTC EXISTS
    if [ -e "${sim_prefix}.xtc" ]; then
		## RESTARTING
		${em_run_command} -v \
                            -s "${sim_prefix}.tpr" \
                            -cpi "${sim_prefix}.cpt" \
                            -append \
                            -deffnm "${sim_prefix}"
    else
        ## RUNNING JOB
        ${em_run_command} -v -deffnm "${sim_prefix}"
    fi
fi

#####################
### EQUILIBRATION ###
#####################

echo "------------------------------"
echo "Part 2 - Equilibration sim"
echo "------------------------------"

## DEFINING SIM PREFIX
sim_prefix="${output_prefix}_equil"

## GENERATING GROMPP
gmx grompp -f "${equil_mdp_file}" \
           -o "${sim_prefix}.tpr" \
           -c "${output_prefix}_em.gro" \
           -p "${path_top_file}" \
           -maxwarn "${max_warn}"

## NPT EQUILIBRATION
if [ ! -e "${sim_prefix}.gro" ]; then
    ## SEEING IF XTC EXISTS
    if [ -e "${sim_prefix}.xtc" ]; then
		## RESTARTING
		${prod_run_command} -v \
                            -s "${sim_prefix}.tpr" \
                            -cpi "${sim_prefix}.cpt" \
                            -append \
                            -deffnm "${sim_prefix}"
    else
        ## RUNNING JOB
        ${prod_run_command} -v -deffnm "${sim_prefix}"
    fi
fi

##################
### PRODUCTION ###
##################

echo "------------------------------"
echo "Part 3: Production simulation"
echo "------------------------------"

## DEFINING SIM PREFIX
sim_prefix="${output_prefix}_prod"

## GENERATING GROMPP
gmx grompp -f "${prod_mdp_file}" \
           -o "${sim_prefix}.tpr" \
           -c "${output_prefix}_equil.gro" \
           -p "${path_top_file}" \
           -maxwarn "${max_warn}"

## NPT PRODUCTION 
if [ ! -e "${sim_prefix}.gro" ]; then
    ## SEEING IF XTC EXISTS
    if [ -e "${sim_prefix}.xtc" ]; then
		## RESTARTING
		${prod_run_command} -v \
                            -s "${sim_prefix}.tpr" \
                            -cpi "${sim_prefix}.cpt" \
                            -append \
                            -deffnm "${sim_prefix}"
    else
        ## RUNNING JOB
        ${prod_run_command} -v -deffnm "${sim_prefix}"
    fi
fi

## REMOVING ANY EXTRA MATERIALS
rm -f \#*
