#!/bin/bash

# run_nplm_unbiased_sims_after_US_plumed.sh
# The purpose of this code is to run the unbiased simulation after US Plumed sims
#
# Written by: Alex K. Chew (01/15/2020)
# 
# VARIABLES
#   $1: index that you care about 
#   $2: number of cores
#   $3: number of em cores
#   $4: md run information
#   $5: md run suffix
# VARIABLES TO CHANGE:
#   _INPUTTOPFILE_ <-- topology file name within input folder
#   _OUTPUTPREFIX_ <-- output prefix
#   _MAXWARN_ <-- max warn
#   _EQUIL_MDPFILE_ <-- equilibration MDP file
#   _EM_MDPFILE_ <-- energy minimization mdp file
#   _PROD_MDPFILE_ <-- production mdp file

#######################
### INPUT VARIABLES ###
#######################

## DEFINING VARIABLES
num_cores="${1:-28}"

## DEFINING GMX MDRUN
mdrun_command="${2-gmx mdrun -nt}"
# mdrun_command="ibrun -np ${num_cores} mdrun_mpi"

## DEFINING MDRUN COMMAND SUFFIX
mdrun_command_suffix="${3:-""}"

## DEFINING GROMACS COMMAND
gromacs_command="${4-gmx}"

## DEFINING RUN COMMAND
prod_run_command="${mdrun_command} ${num_cores} ${mdrun_command_suffix}"

##########################
### EDITABLE VARAIBLES ###
##########################

## GETTING DIRECTORY LOCATION
current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING CURRENT SIMULATION PATH
sim_path=$(cd ${current_dir}; pwd)

## DEFINING RC FILE
rc_file="_RCFILE_"

## LOADING RC
source "${sim_path}/${rc_file}"

## DEFINING TOP FILE
path_top_file="_INPUTTOPFILE_"

## DEFINING OUTPUT PREFIX
output_prefix="_OUTPUTPREFIX_"

## DEFINING INDEX FILE
index_file="_INDEXFILE_"

## DEFINING MAXWARN
max_warn="_MAXWARN_"

## DEFINING MDP FILES
declare -a mdp_files_array=(_MDP_FILE_ARRAY_)

## DECLARING OUTPUT PREFIXES
declare -a sim_prefix_array=(_SIM_PREFIX_ARRAY_)

## DECLARING PLUMED ARRAYS
declare -a plumed_array=(_PLUMEDARRAY_)

## DEFINING PREVIOUS PREFIX
previous_sim_prefix="${output_prefix}"

## LOOPING THROUGH EACH ITERATION
for index in $(seq 0 $(( ${#mdp_files_array[@]}-1)) ); do

    ## PRINTING
    echo "------------------------------"
    echo "Part ${index}"
    echo "------------------------------"

    ## DEFINING FILES
    mdp_file="${mdp_files_array[index]}"
    sim_prefix="${sim_prefix_array[index]}"
    plumed_file="${plumed_array[index]}"

    echo "MDP file: ${mdp_file}"
    echo "Simulation prefix: ${sim_prefix}"
    echo "Previous sim prefix: ${previous_sim_prefix}"

    ## RUNNING MD
    run_grompp_and_mdrun "${mdp_file}" \
                         "${sim_prefix}" \
                         "${previous_sim_prefix}" \
                         "${path_top_file}" \
                         "${max_warn}" \
                         "${gromacs_command}" \
                         "${prod_run_command}" \
                         "${index_file}" \
                         "${plumed_file}"

    ## RE-STORING PREVIOUS PREFIX
    previous_sim_prefix="${sim_prefix}"

done

#### JOB EXTENSION ####

## DEFINING TOTAL PRODUCTION TIME
total_prod_time_ps="_TOTALTIMEPRODPS_"

## FINDING LAST SIM PREFIX
sim_prefix="${sim_prefix_array[-1]}"
last_plumed_file="${plumed_array[-1]}"

## EXTENDING PRODUCTION SIMULATIONS
check_xtc_time prod_xtc_time "${sim_prefix}" "${gromacs_command}"

## CHECKING TO SEE IF PRODUCTION TIME MAKES SENSE
if [ -z "${total_prod_time_ps}" ]; then
    total_prod_time_ps="${prod_xtc_time}"
fi

## IF NOT EQUAL
if [[ "${prod_xtc_time}" != "${total_prod_time_ps}" ]]; then
    ## CONVERTING TPR
    ${gromacs_command} convert-tpr -s "${sim_prefix}.tpr" \
                    -until "${total_prod_time_ps}" \
                    -o "${sim_prefix}.tpr"

    ## GENERATING RESTART PLUMED FILE
    plumed_file_restart="${last_plumed_file%.*}_restart.dat"

    ## MODIFYING RESTART FILE
    echo "Modifying PLUMED file to hae a RESTART flag"
    echo "${last_plumed_file} --> ${plumed_file_restart}"

    ## COPYING
    echo "RESTART" > "${plumed_file_restart}"
    cat "${last_plumed_file}" >> "${plumed_file_restart}"


    ## RESTARTING AND APPENDING
    ${prod_run_command} -v \
                        -s "${sim_prefix}.tpr" \
                        -cpi "${sim_prefix}.cpt" \
                        -append \
                        -deffnm "${sim_prefix}" \
                        -px "${sim_prefix}_pullx.xvg" \
                        -pf "${sim_prefix}_pullf.xvg" \
                        -plumed "${plumed_file_restart}"
else
    echo "Total production time is: ${prod_xtc_time}"
    echo "That is equal to the desired production time: ${total_prod_time_ps}"
fi

#### JOB EXTENSION ####

## REMOVING ANY EXTRA MATERIALS
rm -f \#*
