#!/bin/bash 
# submit_prep_mixed_solvents.sh

## VARIABLES:
#   _JOB_NAME_ <-- job name
#   _NUM_CORES_ <-- number of cores
#   _INPUT_EQUIL_MDP_FILE_ <-- input equilibration file
#	_OUTPUTPREFIX_ <-- output prefix
#	_TOPOLOGY_FILE_ <-- topology file

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## DEFINING NUMBER OF CORES
num_cores="_NUM_CORES_"

###SERVER_SPECIFIC_COMMANDS_END

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname

## DEFINING VARIABLES
input_equil_mdp_file="_INPUT_EQUIL_MDP_FILE_"
output_prefix="_OUTPUTPREFIX_"
topology_file="_TOPOLOGY_FILE_"

## RUNNING EQUILIBRATION
## SEEING IF NOT RUN YET
if [ ! -e "${output_prefix}_equil" ]; then
	gmx grompp -f "${input_equil_mdp_file}" -c "${output_prefix}_em.gro" -p "${topology_file}" -o "${output_prefix}_equil"
	gmx mdrun -v -nt "${num_cores}" -deffnm "${output_prefix}_equil"
fi