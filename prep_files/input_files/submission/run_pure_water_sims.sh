#!/bin/bash

# run_pure_water_sims.sh
# The purpose of this code is to run the pure water simulations
# simulation. 
#
# Written by: Alex K. Chew (03/24/2020)
# 
# VARIABLES
#   $1: index that you care about 
#   $2: number of cores
#   $3: number of em cores
#   $4: md run information
#   $5: md run suffix
# VARIABLES TO CHANGE:
#   _INPUTTOPFILE_ <-- topology file name within input folder
#   _OUTPUTPREFIX_ <-- output prefix
#   _MAXWARN_ <-- max warn
#   _EQUIL1_MDPFILE_ <-- equilibration MDP file
#   _EQUIL2_MDPFILE_ <-- equilibration MDP file
#   _PROD_MDPFILE_ <-- production mdp file

### FUNCTION TO GENERATE GROMPP AND RUN GROMACS
# The purpose of this function is to run grompp and mdrun. 
# INPUTS:
#   $1: mdp file to use
#   $2: simulation prefix to output
#   $3: previous simulation prefix
#   $4: topology file
#   $5: max warn
#   $6: gmx command
#   $7: gmx production run command
# OUTPUTS:
# This function will run the grompp and mdrun from GROMACS
function run_grompp_and_mdrun () {
    ## DEFINING INPUTS
    mdp_file_="$1"
    sim_prefix_="$2"
    previous_sim_prefix_="$3"
    top_file_="$4"
    max_warn_="${5-5}"
    gromacs_command_="${6-gmx}"
    prod_run_command_="${7-gmx mdrun -nt 28}"

  ## GENERATING GROMPP
  ${gromacs_command} grompp -f "${mdp_file_}" \
             -o "${sim_prefix_}.tpr" \
             -c "${previous_sim_prefix_}.gro" \
             -p "${top_file_}" \
             -maxwarn "${max_warn_}"

  ## NPT EQUILIBRATION
  if [ ! -e "${sim_prefix_}.gro" ]; then
      ## SEEING IF XTC EXISTS
      if [ -e "${sim_prefix_}.xtc" ]; then
      ## RESTARTING
      ${prod_run_command_} -v \
                              -s "${sim_prefix_}.tpr" \
                              -cpi "${sim_prefix_}.cpt" \
                              -append \
                              -deffnm "${sim_prefix_}" \
                              -px "${sim_prefix}_pullx.xvg" \
                              -pf "${sim_prefix}_pullf.xvg" 
      else
          ## RUNNING JOB
          ${prod_run_command_} -v -deffnm "${sim_prefix_}"
      fi
  fi

}

#######################
### INPUT VARIABLES ###
#######################

## DEFINING VARIABLES
num_cores="${1:-28}"

## DEFINING GMX MDRUN
mdrun_command="${2-gmx mdrun -nt}"
# mdrun_command="ibrun -np ${num_cores} mdrun_mpi"

## DEFINING MDRUN COMMAND SUFFIX
mdrun_command_suffix="${3:-""}"

## DEFINING GROMACS COMMAND
gromacs_command="${4-gmx}"

## EM AND PROD
em_run_command="${mdrun_command} ${em_num_cores} ${mdrun_command_suffix}"
prod_run_command="${mdrun_command} ${num_cores} ${mdrun_command_suffix}"

##########################
### EDITABLE VARAIBLES ###
##########################

## GETTING DIRECTORY LOCATION
current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING CURRENT SIMULATION PATH
sim_path=$(cd ${current_dir}; pwd)

## DEFINING TOP FILE
input_top_file="_INPUTTOPFILE_"

## DEFINING OUTPUT PREFIX
output_prefix="_OUTPUTPREFIX_"

## DEFINING MDP FILE
equil1_mdp_file="_EQUIL1_MDPFILE_"
equil2_mdp_file="_EQUIL2_MDPFILE_"
prod_mdp_file="_PROD_MDPFILE_"

## DEFINING MAXWARN
max_warn="_MAXWARN_"

####################
### RUNNING CODE ###
####################

## DEFINING TOP FILE
path_top_file="${sim_path}/${input_top_file}"

## DEFINING PATH TO SIMULATION FILE
path_sim="${sim_path}/${sim_folder}"

## PRINTING
echo "Current simulation folder: ${path_sim}"
cd "${path_sim}"

#####################
### EQUILIBRATION ###
#####################

echo "------------------------------"
echo "Part 1 - NPT Equilibration sim"
echo "------------------------------"

## GETTING PREVIOUS SIM PREFIX
previous_sim_prefix="${output_prefix}_em"
## DEFINING SIM PREFIX
sim_prefix="${output_prefix}_equil1"
## DEFINING MDP FILE
mdp_file="${equil1_mdp_file}"

## RUNNING MD
run_grompp_and_mdrun "${mdp_file}" \
                     "${sim_prefix}" \
                     "${previous_sim_prefix}" \
                     "${input_top_file}" \
                     "${max_warn}" \
                     "${gromacs_command}" \
                     "${prod_run_command}"

#####################
### EQUILIBRATION ###
#####################

echo "------------------------------"
echo "Part 2 - NPT Equilibration sim"
echo "------------------------------"

## GETTING PREVIOUS SIM PREFIX
previous_sim_prefix="${sim_prefix}"
## DEFINING NEW SIM PREFIX
sim_prefix="${output_prefix}_equil2"
## DEFINING MDP FILE
mdp_file="${equil2_mdp_file}"

## RUNNING MD
run_grompp_and_mdrun "${mdp_file}" \
                     "${sim_prefix}" \
                     "${previous_sim_prefix}" \
                     "${input_top_file}" \
                     "${max_warn}" \
                     "${gromacs_command}" \
                     "${prod_run_command}"

#####################
### EQUILIBRATION ###
#####################

echo "------------------------------"
echo "Part 3 - NPT PRODUCTION sim"
echo "------------------------------"

## GETTING PREVIOUS SIM PREFIX
previous_sim_prefix="${sim_prefix}"
## DEFINING NEW SIM PREFIX
sim_prefix="${output_prefix}_prod"
## DEFINING MDP FILE
mdp_file="${prod_mdp_file}"

## RUNNING MD
run_grompp_and_mdrun "${mdp_file}" \
                     "${sim_prefix}" \
                     "${previous_sim_prefix}" \
                     "${input_top_file}" \
                     "${max_warn}" \
                     "${gromacs_command}" \
                     "${prod_run_command}"

