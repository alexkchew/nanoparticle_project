#!/bin/bash

# run_nplm_pushing_post_us.sh
# The purpose of this function is to run nplm pushing after umbrella sampling 
# simulations. 
#
# Written by: Alex K. Chew (01/23/2020)
#
# VARIABLES:
#   $1: number of cores
# 
# EDITABLE VARIABLES
#   _OUTPUTPREFIX_: output prefix

#######################
### INPUT VARIABLES ###
#######################

## DEFINING VARIABLES
num_cores="${1:-28}"

## DEFINING GMX MDRUN
mdrun_command="${2-gmx mdrun -nt}"
# mdrun_command="ibrun -np ${num_cores} mdrun_mpi"

## DEFINING MDRUN COMMAND SUFFIX
mdrun_command_suffix="${3:-""}"

## DEFINING PROD COMMANDS
prod_run_command="${mdrun_command} ${num_cores} ${mdrun_command_suffix}"

##########################
### EDITABLE VARAIBLES ###
##########################

## GETTING DIRECTORY LOCATION
current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING OUTPUT PREFIX
output_prefix="_OUTPUTPREFIX_"

####################
### RUNNING CODE ###
####################

## DEFINING SIM PREFIX
sim_prefix="${output_prefix}"

## NPT EQUILIBRATION
if [ ! -e "${sim_prefix}.gro" ]; then
    ## SEEING IF XTC EXISTS
    if [ -e "${sim_prefix}.xtc" ]; then
		## RESTARTING
		${prod_run_command} -v \
                            -s "${sim_prefix}.tpr" \
                            -cpi "${sim_prefix}.cpt" \
                            -append \
                            -deffnm "${sim_prefix}"
    else
        ## RUNNING JOB
        ${prod_run_command} -v -deffnm "${sim_prefix}"
    fi
fi


