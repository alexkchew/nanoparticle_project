#!/bin/bash 
# submit_hydration_sims.sh
# This script runs submission for hydration sims
# VARIABLES:
#   _JOB_NAME_ <-- job name
#	_NUMBER_OF_CORES_ <-- number of cores to run job
#	_OUTPUTPREFIX_ <-- output prefix
#	_MDPPROD_ <-- production file

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="_NUMBER_OF_CORES_" # number of cores you want to run with

###SERVER_SPECIFIC_COMMANDS_END

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname

## DEFINING MDP FILES
mdp_file_prod="_MDPPROD_"

## DEFINING MAX WARN
max_warn="_MAXWARN_"

## DEFINING PREFIX
output_prefix="_OUTPUTPREFIX_"

## DEFINING EM PREFIX
em_prefix="_EM_PREFIX_"

## DEFINING INDEX FILE
index_file="_INDEXFILE_"

## DEFINING TOP FILE
top_file="_TOPFILE_"

##################
### PRODUCTION ###
##################

echo "==== RUNNING PRODUCTION ===="

## DEFINING NEW PREFIX
old_prefix="${em_prefix}"
new_prefix="${output_prefix}_prod"
mdp_file="${mdp_file_prod}"

## CHECKING IF COMPLETE
if [ ! -e "${new_prefix}.gro" ]; then
  ## SEEING IF XTC FILE EXISTS
  if [ -e "${new_prefix}.xtc" ]; then
    ## RESTARTING
    gmx mdrun -nt "${num_cores}" -v \
                                     -s "${new_prefix}.tpr" \
                                     -cpi "${new_prefix}.cpt" \
                                     -append -deffnm "${new_prefix}"
    else
        ## STARTING FROM BEGINNING
        gmx grompp -f "${mdp_file}" \
                   -o "${new_prefix}.tpr" \
                   -c "${old_prefix}.gro" \
                   -p "${top_file}" \
                   -n "${index_file}" \
                   -maxwarn ${max_warn} 
        gmx mdrun -nt "${num_cores}" \
                  -v \
                  -deffnm "${new_prefix}"
    fi
fi

## COMPETION
echo "Job is complete!"
