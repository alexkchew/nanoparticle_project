#!/bin/bash 

# submit_sam_double_planar.sh
# This submission script goes through nvt and nvt equilibration with ligands frozen prior to real simulation times.
# NVT #1 goes through water equilibration with ions frozen on the z-coordinate
# NVT #2 goes through ligand equilibration with the water / ions frozen entirely

# VARIABLES YOU WILL NEED TO EDIT:
# job_name: JOB_NAME
# output_file_name: OUTPUT_FILE_NAME
# nvt_equil_frozen_ligand_mdp: NVT_EQUIL_MDP_FROZEN_LIGANDS
# nvt_equil_frozen_water_ion_mdp: NVT_EQUIL_MDP_FROZEN_WATER_ION
# equil_mdp_file_name: EQUIL_MDP_FILE_NAME
# path2ligand_builder: PATH2LIGANDBUILDER
# position_restraint: POSITION_RESTRAINT_FILE_NAME

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J JOB_NAME
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

# INSTRUCTIONS:
# The -t command specifies how long this simulation will run for (in HOURS:MINUTES:SECONDS). Try to estimate this as best you can
# as the simulation will temrinate after this time.
# The -J flag species the name of the simulation
# the --mail-user command will send you email when the job runs / terminates
# Do not change the other flags, really.

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname

### LIGANDBUILDER FUNCTIONS
path2ligand_builder="PATH2LIGANDBUILDER"
source "${path2ligand_builder}"

### INPUT VARIABLES
## DEFINING NAME
output_file_name="OUTPUT_FILE_NAME"

# POSITION RESTRAINT FILE NAME
position_restraint="POSITION_RESTRAINT_FILE_NAME"

# INCLUSION OF INDEX FILE
index_file="INDEX_FILE_NAME"

## MDP FILES
# FROZEN LIGAND RUNS
nvt_equil_frozen_ligand_mdp="NVT_EQUIL_MDP_FROZEN_LIGANDS"
nvt_equil_frozen_water_ion_mdp="NVT_EQUIL_MDP_FROZEN_WATER_ION"

# EQUIL AND PRODUCTION RUNS
equil_mdp_file_name="EQUIL_MDP_FILE_NAME"
prod_mdp_file_name="PROD_MDP_FILE_NAME"

### NVT EQUILIBRATION
## FROZEN LIGAND RUNS
gmx grompp -f ${nvt_equil_frozen_ligand_mdp} -o ${output_file_name}_equil_nvt_frozen_lig.tpr -c ${output_file_name}_em.gro -p ${output_file_name}.top -n ${index_file} -maxwarn 5
gmx mdrun -nt 28 -v -deffnm ${output_file_name}_equil_nvt_frozen_lig

## NPT EQUILIBRATION
## FROZEN WATER / ION RUNS
gmx grompp -f ${nvt_equil_frozen_water_ion_mdp} -o ${output_file_name}_equil_nvt_frozen_solvent.tpr -c ${output_file_name}_equil_nvt_frozen_lig.gro -p ${output_file_name}.top -maxwarn 5
gmx mdrun -nt 28 -v -deffnm ${output_file_name}_equil_nvt_frozen_solvent

### EQUILIBRATION RUN
gmx grompp -f ${equil_mdp_file_name} -o ${output_file_name}_equil_npt.tpr -c ${output_file_name}_equil_nvt_frozen_solvent.gro -p ${output_file_name}.top -maxwarn 5
gmx mdrun -nt 28  -v -deffnm ${output_file_name}_equil_npt

#### PRODUCTION RUN
gmx grompp -f ${prod_mdp_file_name} -o ${output_file_name}_prod.tpr -c ${output_file_name}_equil_npt.gro -p ${output_file_name}.top -maxwarn 5
gmx mdrun -nt 28  -v -deffnm ${output_file_name}_prod

