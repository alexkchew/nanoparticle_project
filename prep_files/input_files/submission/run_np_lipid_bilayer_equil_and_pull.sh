#!/bin/bash 

# run_np_lipid_bilayer_equil_and_pull.sh
# This run script runs nanoparticle lipid membrane equilibration and 
# pulling. The main idea is to equilibrate the nanoparticle at a 
# distance away, then pull the nanoparticle in a separate simulation. 
# 
# Written by: Alex K. Chew (01/09/2020)
# Updated 06/08/2020 - Removal of the pushing away of NPs
# VARIABLES:
#   $1: number of cores
#   $2: num cores for energy minimization
#   $3: md run command
#
# VARIABLES TO CHANGE:
#   _MDPEM_ <-- energy minimization MDP file
#   _MDPEQUIL1_ <-- equilibration MDP file 1
#   _MDPEQUIL2_ <-- equilibration MDP file 2
#   _OUTPUTPREFIX_ <-- output prefix to update
#   _MAXWARN_ <-- number of max warnings from grompp

#   VARIABLES FOR PULLING INFORMATIONS
#   _MDPINPUTPULL_ <-- input pull mdp file
#   _MDPOUTPUTPULL_ <-- output pull mdp file
#   _MDPINPUTPUSH_ <-- input pushing mdp file
#   _MDPOUTPUTPUSH_ <-- output pushing mdp file
#   _RESPULL1_ <-- residue 1, which is being pulled towards (e.g. lipid bilayer)
#   _RESNPGOLD_ <-- residue nanoparticle gold atoms
#   _RESNPLIG_ <-- residue nanoparticle ligands
#   _SUBMITNP2LIPIDBILAYER_ <-- submit file name for moving NP to lipid bilayer
#   _SUBMITNP_PUSH_FROM_LIPIDBILAYER_ <-- submit file for pushing from bilayer
#   _SPRINGCONSTANT_ <-- spring constant for pulling
#   _PULLRATE_ <-- pull rate
#   _TEMP_ <-- temperature in K
#   _TIMESTEP_ <-- time step for pulling in ps
#   _MEMBRANEHALFTHICK_ <-- membrane half thickness

## DEFINING VARIABLES
num_cores="${1:-28}"

## ENERGY MINIMIZATION NUMBER OF CORES
em_num_cores="${2:-28}"

## DEFINING GMX MDRUN
mdrun_command="${3-gmx mdrun -nt}"
# mdrun_command="ibrun -np ${num_cores} mdrun_mpi"

## DEFINING MDRUN COMMAND SUFFIX
mdrun_command_suffix="${4:-""}"
# mdrun_mpi

## EM AND PROD
em_run_command="${mdrun_command} ${em_num_cores} ${mdrun_command_suffix}"
prod_run_command="${mdrun_command} ${num_cores} ${mdrun_command_suffix}"

##########################
### EDITABLE VARAIBLES ###
##########################

## DEFINING INPUT FILES
output_prefix="_OUTPUTPREFIX_"

## DEFINING PULLING INFORMATION
mdp_input_pull="_MDPINPUTPULL_"
mdp_output_pull="_MDPOUTPUTPULL_"

## DEFINING PUSHING INFORMATION
mdp_input_push="_MDPINPUTPUSH_"
mdp_output_push="_MDPOUTPUTPUSH_"

## DEFINING RESIDUES OF PULLING
res_pull_1="_RESPULL1_" # e.g. DOPC
res_np_gold="_RESNPGOLD_" # e.g. AUNP
res_np_lig="_RESNPLIG_"  # e.g. RO1

## DEFINING PULLING SUBMIT
np_to_lipid_bilayer_submit="_SUBMITNP2LIPIDBILAYER_"
## DEFINING PUSHING SUBMIT
np_from_lipid_bilayer_submit="_SUBMITNP_PUSH_FROM_LIPIDBILAYER_"

## MDP FILES
em_mdp="_MDPEM_"
mdp_equil_1="_MDPEQUIL1_"
mdp_equil_2="_MDPEQUIL2_"

## DEFINING MAX WARN
max_warn="_MAXWARN_"

## DEFINING SPRING CONSTANT
us_spring_constant="_SPRINGCONSTANT_"
us_pull_rate="_PULLRATE_"
temp="_TEMP_"
time_step="_TIMESTEP_"
membrane_half_thickness="_MEMBRANEHALFTHICK_"

####################
### RUNNING CODE ###
####################

## ENERGY MINIMIZATION
gmx grompp -f ${em_mdp} -c ${output_prefix}.gro -p ${output_prefix}.top -o "${output_prefix}_em.tpr"
## SEEING IF ENERGY MINIMIZED
if [ ! -e "${output_prefix}_em.gro" ]; then
    ${em_run_command} -deffnm "${output_prefix}_em"
fi

##################################
### NPT EQUILIBRATION ALL FREE ###
##################################

## DEFINING SIM PREFIX
sim_prefix="${output_prefix}_equil"

## GENERATING GROMPP
gmx grompp -f "${mdp_equil_1}" \
           -o "${sim_prefix}.tpr" \
           -c "${output_prefix}_em.gro" \
           -p "${output_prefix}.top" \
           -maxwarn "${max_warn}"

## NVT EQUILIBRATION
if [ ! -e "${sim_prefix}.gro" ]; then
    ## SEEING IF XTC EXISTS
    if [ -e "${sim_prefix}.xtc" ]; then
		## RESTARTING
		${prod_run_command} -v \
                            -s "${sim_prefix}.tpr" \
                            -cpi "${sim_prefix}.cpt" \
                            -append \
                            -deffnm "${sim_prefix}"
    else
        ## RUNNING EQUILIBRATION
        ${prod_run_command} -v -deffnm "${sim_prefix}"
    fi
fi

###################################
#### NVT EQUILIBRATION (FROZEN) ###
###################################
#
### DEFINING SIM PREFIX
#sim_prefix="${output_prefix}_equil2"
#
### GENERATING GROMPP
#gmx grompp -f "${mdp_equil_2}" \
#           -o "${sim_prefix}.tpr" \
#           -c "${output_prefix}_equil1.gro" \
#           -p "${output_prefix}.top" \
#           -maxwarn "${max_warn}"
#
### NVT EQUILIBRATION
#if [ ! -e "${sim_prefix}.gro" ]; then
#    ## SEEING IF XTC EXISTS
#    if [ -e "${sim_prefix}.xtc" ]; then
#		## RESTARTING
#		${prod_run_command}  -v \
#                             -s "${sim_prefix}.tpr" \
#                             -cpi "${sim_prefix}.cpt" \
#                             -append \
#                             -deffnm "${sim_prefix}"
#    else
#        ## RUNNING EQUILIBRATION
#        ${prod_run_command} -v -deffnm "${sim_prefix}"
#    fi
#fi

################################################
### RUNNING CODE TO PULL NP TO LIPID BILAYER ###
################################################
bash "${np_to_lipid_bilayer_submit}" \
                "$(pwd)" \
                "${sim_prefix}.gro" \
                "${sim_prefix}.tpr" \
                "${res_pull_1}" \
                "${res_np_gold}" \
                "${res_np_lig}" \
                "${mdp_input_pull}" \
                "${mdp_output_pull}" \
                "${us_spring_constant}" \
                "${us_pull_rate}" \
                "${time_step}" \
                "${temp}" \
                "${membrane_half_thickness}"

## DEFINING SIM PREFIX
sim_prefix="${output_prefix}_pull"

## PULLING SIMULATION
gmx grompp -f "${mdp_output_pull}" \
           -o "${sim_prefix}.tpr" \
           -c "${output_prefix}_equil.gro" \
           -p "${output_prefix}.top" \
           -maxwarn "${max_warn}"

## RUNNING SIMULATION
if [ ! -e "${sim_prefix}.gro" ]; then
    ## SEEING IF XTC EXISTS
    if [ -e "${sim_prefix}.xtc" ]; then
		## RESTARTING
		${prod_run_command} -v \
                            -s "${sim_prefix}.tpr" \
                            -cpi "${sim_prefix}.cpt" \
                            -append \
                            -deffnm "${sim_prefix}"
    else
        ## RUNNING SIMULATION
        ${prod_run_command} -v -deffnm "${sim_prefix}"
    fi
fi


## COMMENTED OUT FOR NOW

# ##################################################
# ### RUNNING CODE TO PUSH NP FROM LIPID BILAYER ###
# ##################################################

# bash "${np_from_lipid_bilayer_submit}" \
#                 "$(pwd)" \
#                 "${mdp_output_pull}" \
#                 "${mdp_input_push}" \
#                 "${mdp_output_push}" \
#                 "${temp}"

# ## DEFINING SIM PREFIX
# sim_prefix="${output_prefix}_push"

# ## PULLING SIMULATION
# gmx grompp -f "${mdp_output_push}" \
#            -o "${sim_prefix}.tpr" \
#            -c "${output_prefix}_pull.gro" \
#            -p "${output_prefix}.top" \
#            -maxwarn "${max_warn}"

# ## RUNNING SIMULATION
# if [ ! -e "${sim_prefix}.gro" ]; then
#     ## SEEING IF XTC EXISTS
#     if [ -e "${sim_prefix}.xtc" ]; then
# 		## RESTARTING
# 		${prod_run_command} -v \
#                             -s "${sim_prefix}.tpr" \
#                             -cpi "${sim_prefix}.cpt" \
#                             -append \
#                             -deffnm "${sim_prefix}"
#     else
#         ## RUNNING SIMULATION
#         ${prod_run_command} -v -deffnm "${sim_prefix}"
#     fi
# fi

## PRINTING
echo "The job is finished!"
