#!/bin/bash 

# run_nplm_us_sims_multiple_index_array.sh
# The purpose of this script is to run several windows of umbrella 
# sampling simulations. 
# VARIABLES:
#   _RUNSCRIPT_ <-- run script
#	_INDEXARRAY_ <-- index array, separated by spaces

## DEFINING INPUTS
num_cores="${1-28}"
em_num_cores="${2-28}"
mdrun_command="${3-gmx mdrun -nt}"
mdrun_command_suffix="${4:-""}"
gromacs_command="${5:gmx}"

## DEFINING BASH SCRIPT
run_script="_RUNSCRIPT_"

## DEFINING WINDOWS
declare -a index_array=(_INDEXARRAY_)

## ECHOING
echo "---------------------------BEGIN---------------------------"

## LOOPING JOB INDEX
for job_index in ${index_array[@]}; do
    echo "------------- Running Window: ${job_index} -------------"
    
    ## RUNNING EACH LAMBDA
    bash "${run_script}" "${job_index}" \
                         "${num_cores}" \
                         "${em_num_cores}" \
                         "${mdrun_command}" \
                         "${mdrun_command_suffix}" \
                         "${gromacs_command}"
                         
done

echo "---------------------------END---------------------------"
