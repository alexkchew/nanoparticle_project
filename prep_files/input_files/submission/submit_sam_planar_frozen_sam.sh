#!/bin/bash 
# submit_sam_planar_frozen_sam.sh
# This submission script is to submit for a planar SAM
## VARIABLES:
#   _JOB_NAME_ <-- name of the job
#   _NUM_CORES_ <-- Number of cores
#   _EQUILMDP_ <-- equilibration mdp file
#   _PRODMDP_ <-- production mdp file
#   _OUTPUTPREFIX_ <-- output prefix
#   _MAXWARN_ <-- max warnings
#   _EQUILMDP2_ <-- equilibration part 2


###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## DEFINING NUMBER OF CORES
num_cores="_NUM_CORES_"

###SERVER_SPECIFIC_COMMANDS_END

## DEFINING FUNCTIONS
functions_rc="_FUNCTIONSRC_"

## SOURCING THE FUNCTIONS
source "${functions_rc}"

## DEFINING INDEX FILE
index_file="_INDEXFILE_"

## DEFINING EQUIL AND PROD MDP
equil_mdp="_EQUILMDP_"
equil_mdp2="_EQUILMDP2_"
prod_mdp="_PRODMDP_"

## DEFINING DESIRED DISTANCE AFTER EQUIL
vacuum_layer_z_above="_DISTAFTEREQUIL_"

## DEFINING OUTPUT NAME
output_prefix="_OUTPUTPREFIX_"

## DEFINING MAX WARN
max_warn="_MAXWARN_"

#####################
### EQUILIBRATION1 ###
#####################

## DEFINING NEW PREFIX
old_prefix="${output_prefix}"
new_prefix="${output_prefix}_equil1"
mdp_file="${equil_mdp}"

## CHECKING IF COMPLETE
if [ ! -e "${new_prefix}.gro" ]; then
  ## SEEING IF XTC FILE EXISTS
  if [ -e "${new_prefix}.xtc" ]; then
    ## RESTARTING
    gmx mdrun -nt "${num_cores}" -v \
                                     -s "${new_prefix}.tpr" \
                                     -cpi "${new_prefix}.cpt" \
                                     -append -deffnm "${new_prefix}"
    else
        ## STARTING FROM BEGINNING
        gmx grompp -f "${mdp_file}" \
                   -o "${new_prefix}.tpr" \
                   -c "${old_prefix}.gro" \
                   -p "${output_prefix}.top" \
                   -n "${index_file}" \
                   -maxwarn ${max_warn} 
        gmx mdrun -nt "${num_cores}" \
                  -v \
                  -deffnm "${new_prefix}"
    fi
fi


#####################
### EQUILIBRATION2 ###
#####################

if [ ! -z "${equil_mdp2}" ]; then
## DEFINING NEW PREFIX
old_prefix="${output_prefix}_equil1"
new_prefix="${output_prefix}_equil2"
mdp_file="${equil_mdp2}"

## CHECKING IF COMPLETE
if [ ! -e "${new_prefix}.gro" ]; then
  ## SEEING IF XTC FILE EXISTS
  if [ -e "${new_prefix}.xtc" ]; then
    ## RESTARTING
    gmx mdrun -nt "${num_cores}" -v \
                                     -s "${new_prefix}.tpr" \
                                     -cpi "${new_prefix}.cpt" \
                                     -append -deffnm "${new_prefix}"
    else
        ## STARTING FROM BEGINNING
        gmx grompp -f "${mdp_file}" \
                   -o "${new_prefix}.tpr" \
                   -c "${old_prefix}.gro" \
                   -p "${output_prefix}.top" \
                   -n "${index_file}" \
                   -maxwarn ${max_warn} 
        gmx mdrun -nt "${num_cores}" \
                  -v \
                  -deffnm "${new_prefix}"
    fi
fi

fi


###########################
### ADDING VACUUM LAYER ###
###########################


## DEFINING NEW PREFIX
if [ ! -z "${equil_mdp2}" ]; then
  old_prefix="${output_prefix}_equil2"
else
  old_prefix="${output_prefix}_equil1"
fi

## STORING OUTPUT PREFIX
stored_output_prefix="${output_prefix}"

## RESNAME
gold_resname="AUI"

## SEEING IF VACUUM LAYER IS DESIRED
if [[ "${vacuum_layer_z_above}" != "0" ]]; then
    echo "Adding vacuum layer of: ${vacuum_layer_z_above} nm"
    cp -rv "${old_prefix}.gro" "${old_prefix}_copy.gro"
    ## DEFINING OUTPUT PREFIX
    output_prefix="${old_prefix}_copy"

    ## GETTING GRO SIZE
    read -a gro_box_size <<< $(gro_measure_box_size "${output_prefix}.gro")

    ## GETTING THE Z DIMENSION TOTAL SIZE
    total_z_size=$(awk -v z_dim=${gro_box_size[2]} -v new_z_layer=${vacuum_layer_z_above}} 'BEGIN{ printf "%.3f", z_dim + 2 * new_z_layer }')

    ## RE-SIZING (no centering)
    gmx editconf -f "${output_prefix}.gro" \
                 -box ${gro_box_size[0]} ${gro_box_size[1]} ${total_z_size} \
                 -o "${output_prefix}_vacuum.gro" \
                 -noc

    ## TRANSLATING
    translate_planar_sam_z "${output_prefix}_vacuum.gro" \
                           "${old_prefix}.tpr" \
                           "${gold_resname}" \
                           "${output_prefix}_vacuum_translated.gro"

    ## CHECKING IF TRANSLATED CORRECTLY
    if [ -e "${output_prefix}_vacuum_translated.gro" ]; then
        ## CREATING SETUP FILES AND RENAMING
        if [ ! -e "setup_files" ]; then
          mkdir setup_files
        fi
        mv "${output_prefix}.gro" "${output_prefix}_vacuum.gro" "${output_prefix}_vacuum_translated.gro" setup_files
        cp -r "setup_files/${output_prefix}_vacuum_translated.gro" "${output_prefix}.gro" 
    fi

fi


##################
### PRODUCTION ###
##################

echo "==== RUNNING PRODUCTION ===="

old_prefix="${output_prefix}"

## DEFINING OUTPUT PREFIX
output_prefix="${stored_output_prefix}"
new_prefix="${output_prefix}_prod"
mdp_file="${prod_mdp}"

## CHECKING IF COMPLETE
if [ ! -e "${new_prefix}.gro" ]; then
  ## SEEING IF XTC FILE EXISTS
  if [ -e "${new_prefix}.xtc" ]; then
    ## RESTARTING
    gmx mdrun -nt "${num_cores}" -v \
                                     -s "${new_prefix}.tpr" \
                                     -cpi "${new_prefix}.cpt" \
                                     -append -deffnm "${new_prefix}"
    else
        ## STARTING FROM BEGINNING
        gmx grompp -f "${mdp_file}" \
                   -o "${new_prefix}.tpr" \
                   -c "${old_prefix}.gro" \
                   -p "${output_prefix}.top" \
                   -n "${index_file}" \
                   -maxwarn ${max_warn} 
        gmx mdrun -nt "${num_cores}" \
                  -v \
                  -deffnm "${new_prefix}"
    fi

fi
## COMPETION
echo "Job is complete!"
