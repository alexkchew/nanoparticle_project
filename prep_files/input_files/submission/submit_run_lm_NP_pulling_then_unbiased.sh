#!/bin/bash 

# submit_run_lm_NP_pulling_then_unbiased.sh
# This submission script runs pure water simulations
# VARIABLES TO CHANGE:
#   _JOB_NAME_ <-- job name
#   _NUMBER_OF_CORES_ <-- number of cores to use
#   _RUNSCRIPT_ <-- run script
#   _MDRUNCOMMAND_ <-- md run command
#   _MDRUNCOMMANDSUFFIX_ <-- suffix for the command

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="_NUMBER_OF_CORES_" # number of cores you want to run with
mdrun_command="_MDRUNCOMMAND_" # command to run code
mdrun_command_suffix="_MDRUNCOMMANDSUFFIX_" # suffix
gromacs_command="_GROMACSCOMMAND_"

###SERVER_SPECIFIC_COMMANDS_END

## DEFINING RUN SCRIPT
run_script="_RUNSCRIPT_"

## RUNNING COMMANDS
bash "${run_script}" "${num_cores}" \
                     "${mdrun_command}" \
                     "${mdrun_command_suffix}" \
                     "${gromacs_command}"
