#!/bin/bash 
# submit_transfer_ligand_builder.sh
# This script is designed to run NPT simulations with GNPs (transfer ligands)
## VARIABLES:
#   _JOB_NAME_ <-- job name
#	_NUMBER_OF_CORES_ <-- number of cores to run job
#	_OUTPUTPREFIX_ <-- output prefix
#   _EM_GRO_FILE_ <-- energy minimized gro file
#   _EQUILMDPFILE_ <-- mdp equilibration file
#   _PRODMDPFILE_ <-- mdp production file
# 	_TOPFILE_ <-- topology file

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="_NUMBER_OF_CORES_" # number of cores you want to run with

###SERVER_SPECIFIC_COMMANDS_END

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname

## DEFINING EM OUTPUT
em_gro_file="_EM_GRO_FILE_"
top_file="_TOPFILE_"

## DEFINING PREFIX
output_prefix="_OUTPUTPREFIX_"

## DEFINING MDP FILE
equil_mdp_file="_EQUILMDPFILE_"
prod_mdp_file="_PRODMDPFILE_"

### EQUILIBRATION
gmx grompp -f ${equil_mdp_file} -c "${em_gro_file}" -p "${top_file}" -o "${output_prefix}_equil" -maxwarn 1
### RUNNING
if [ ! -e "${output_prefix}_equil.gro" ]; then
	## SEEING IF XTC FILE EXISTS
	if [ -e "${output_prefix}_equil.xtc" ]; then
		## RESTARTING
		gmx mdrun -nt "${num_cores}" -v -s "${output_prefix}_equil.tpr" -cpi "${output_prefix}_equil.cpt" -append -deffnm "${output_prefix}_equil"
	else
		## STARTING
		gmx mdrun -nt "${num_cores}" -v -deffnm "${output_prefix}_equil"
	fi
fi


### PRODUCTION
## CREATING MDP FILE
gmx grompp -f ${prod_mdp_file} -c "${output_prefix}_equil.gro" -p "${top_file}" -o "${output_prefix}_prod" -maxwarn 1

## SEEING IF GRO FILE EXISTS
if [ ! -e "${output_prefix}_prod.gro" ]; then
	## SEEING IF XTC FILE EXISTS
	if [ -e "${output_prefix}_prod.xtc" ]; then
		## RESTARTING
		gmx mdrun -nt "${num_cores}" -v -s "${output_prefix}_prod.tpr" -cpi "${output_prefix}_prod.cpt" -append -deffnm "${output_prefix}_prod"
	else
		## STARTING
		gmx mdrun -nt "${num_cores}" -v -deffnm "${output_prefix}_prod"
	fi
else
	echo "Job has completed!"
fi


