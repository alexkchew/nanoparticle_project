#!/bin/bash 
# submit_cosolvent_map.sh
# This submission script is designed to submit hydration maps
## VARIABLES:
#   _USER_ <-- User to email details to
#   _JOBNAME_ <-- job name
#   _JOBTIME_ <-- job time for this script
#   _BASHSCRIPT_ <-- bash script

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t _JOBTIME_
#SBATCH -J _JOBNAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=_USER_@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

# 108:00:00

## ADDING PATH VARIABLES
export PYTHONPATH="/usr/lib64/python3.6/site-packages:${PYTHONPATH}"

###SERVER_SPECIFIC_COMMANDS_END

## DEFINING THE BASH SCRIPT
bash_script="_BASHSCRIPT_"

## RUNNING BASH SCRIPT
time bash "${bash_script}"

