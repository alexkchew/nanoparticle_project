#!/bin/bash

# general_rc.sh
# This contains general scripts that could be used for running gromacs
#
# FUNCTIONS AVAILABLE:
#   run_grompp_and_mdrun:
#     generate mdp file and perform an MDRUN
#   check_xtc_time
#     checks the trajectory time

### FUNCTION TO GENERATE GROMPP AND RUN GROMACS
# The purpose of this function is to run grompp and mdrun. 
# INPUTS:
#   $1: mdp file to use
#   $2: simulation prefix to output
#   $3: previous simulation prefix
#   $4: topology file
#   $5: max warn
#   $6: gmx command
#   $7: gmx production run command
# OUTPUTS:
#   This function will run the grompp and mdrun from GROMACS
function run_grompp_and_mdrun () {
    ## DEFINING INPUTS
    mdp_file_="$1"
    sim_prefix_="$2"
    previous_sim_prefix_="$3"
    top_file_="$4"
    max_warn_="${5-5}"
    gromacs_command_="${6-gmx}"
    prod_run_command_="${7-gmx mdrun -nt 28}"
    index_file="${8-None}"
    plumed_file="${9-None}"

  ## GENERATING GROMPP
  if [[ "${index_file}" == "None" ]]; then
  ${gromacs_command} grompp -f "${mdp_file_}" \
                            -o "${sim_prefix_}.tpr" \
                            -c "${previous_sim_prefix_}.gro" \
                            -p "${top_file_}" \
                            -maxwarn "${max_warn_}"
  else

  ${gromacs_command} grompp -f "${mdp_file_}" \
                            -o "${sim_prefix_}.tpr" \
                            -c "${previous_sim_prefix_}.gro" \
                            -p "${top_file_}" \
                            -maxwarn "${max_warn_}" \
                            -n "${index_file}"

  fi


  ## NPT EQUILIBRATION
  if [ ! -e "${sim_prefix_}.gro" ]; then
      ## SEEING IF XTC EXISTS
      if [ -e "${sim_prefix_}.xtc" ]; then
      ## RESTARTING
      if [[ "${plumed_file}" == None ]]; then
        ${prod_run_command_} -v \
                                -s "${sim_prefix_}.tpr" \
                                -cpi "${sim_prefix_}.cpt" \
                                -append \
                                -deffnm "${sim_prefix_}" \
                                -px "${sim_prefix}_pullx.xvg" \
                                -pf "${sim_prefix}_pullf.xvg" 
      else
        ## GENERATING RESTART PLUMED FILE
        plumed_file_restart="${plumed_file%.*}_restart.dat"
        ## COPYING
        echo "RESTART" > "${plumed_file_restart}"
        cat "${plumed_file}" >> "${plumed_file_restart}"

        ${prod_run_command_} -v \
                                -s "${sim_prefix_}.tpr" \
                                -cpi "${sim_prefix_}.cpt" \
                                -append \
                                -deffnm "${sim_prefix_}" \
                                -px "${sim_prefix}_pullx.xvg" \
                                -pf "${sim_prefix}_pullf.xvg" \
                                -plumed "${plumed_file_restart}"

      fi
      else
          ## RUNNING JOB
          if [[ "${plumed_file}" == None ]]; then
            ${prod_run_command_} -v -deffnm "${sim_prefix_}"
          else
            ${prod_run_command_} -v -deffnm "${sim_prefix_}" -plumed "${plumed_file}"
          fi
      fi
  fi

}

### FUNCTION TO CHECK XTC TIME
check_xtc_time () 
{ 
    ## DEFINING LOCAL VARIABLE
    local __xtctime="${1:xtc_time}";
    prefix_="${2-sam_prod}";
    gromacs_command="${3-gmx}"

    temp_file_="${prefix_}_temp.txt";

    ${gromacs_command} check -f "${prefix_}".cpt 2> "${temp_file_}";

    last_frame=$(grep "Last frame" "${temp_file_}" | awk '{print $NF}');
    
    eval $__xtctime="${last_frame}";
    if [ -e "${temp_file_}" ]; then
        rm "${temp_file_}";
    fi
}
