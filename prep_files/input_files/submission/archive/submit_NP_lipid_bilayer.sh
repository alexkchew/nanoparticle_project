#!/bin/bash 

# submit_NP_lipid_bilayer.sh
# This submission script runs lipid bilayer NPT equilibration
# VARIABLES TO CHANGE:
#   _MDPEM_ <-- energy minimization MDP file
#   _MDPEQUIL_ <-- equilibration MDP file
#   _MDPPROD_ <-- production MDP file
#   _OUTPUTPREFIX_ <-- output prefix to update
#   _JOB_NAME_ <-- job name
#   _NUMBER_OF_CORES_ <-- number of cores to use

#   VARIABLES FOR PULLING INFORMATIONS
#   _MDPINPUTPULL_ <-- input pull mdp file
#   _MDPOUTPUTPULL_ <-- output pull mdp file
#   _RESPULL1_ <-- residue 1, which is being pulled towards (e.g. lipid bilayer)
#   _RESNPGOLD_ <-- residue nanoparticle gold atoms
#   _RESNPLIG_ <-- residue nanoparticle ligands
#   _SUBMITNP2LIPIDBILAYER_ <-- submit file name for moving NP to lipid bilayer

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="_NUMBER_OF_CORES_" # number of cores you want to run with

###SERVER_SPECIFIC_COMMANDS_END

## DEFINING INPUT FILES
output_prefix="_OUTPUTPREFIX_"

## DEFINING PULLING INFORMATION
mdp_input_pull="_MDPINPUTPULL_"
mdp_output_pull="_MDPOUTPUTPULL_"

## DEFINING RESIDUES OF PULLING
res_pull_1="_RESPULL1_" # e.g. DOPC
res_np_gold="_RESNPGOLD_" # e.g. AUNP
res_np_lig="_RESNPLIG_"  # e.g. RO1

## DEFINING SUBMIT FILES
np_to_lipid_bilayer_submit="_SUBMITNP2LIPIDBILAYER_"

## MDP FILES
em_mdp="_MDPEM_"
mdp_equil="_MDPEQUIL_"
mdp_prod="_MDPPROD_"

## ENERGY MINIMIZATION
gmx grompp -f ${em_mdp} -c ${output_prefix}.gro -p ${output_prefix}.top -o "${output_prefix}_em.tpr"
gmx mdrun -v -nt 1 -deffnm "${output_prefix}_em"

## NVT EQUILIBRATION
gmx grompp -f "${mdp_equil}" -o ${output_prefix}_equil.tpr -c ${output_prefix}_em.gro -p ${output_prefix}.top -maxwarn 5
gmx mdrun -nt ${num_cores} -v -deffnm ${output_prefix}_equil

## RUNNING CODE TO PULL NP TO LIPID BILAYER
bash "${np_to_lipid_bilayer_submit}" "$(pwd)" "${output_prefix}_equil.gro" "${output_prefix}_equil.tpr" "${res_pull_1}" "${res_np_gold}" "${res_np_lig}" "${mdp_input_pull}" "${mdp_output_pull}"

## PULLING SIMULATION
gmx grompp -f "${mdp_output_pull}" -o ${output_prefix}_pull.tpr -c ${output_prefix}_equil.gro -p ${output_prefix}.top -maxwarn 5
gmx mdrun -nt ${num_cores} -v -deffnm ${output_prefix}_pull

## PRODUCTION RUN
gmx grompp -f "${mdp_prod}" -o ${output_prefix}_prod.tpr -c ${output_prefix}_pull.gro -p ${output_prefix}.top -maxwarn 5
gmx mdrun -nt ${num_cores} -v -deffnm ${output_prefix}_prod

