# DESCRIPTION
The purpose of this folder is to contain all submission files

# FILES
- submit_sam_double_planar.sh: submission file for double planar with the following:
  - NVT equilibration (frozen ligands)
  - NPT equilibration (frozen ligands)
  - NPT equilibration
  - NPT production
- submit_sam_double_planar_nvt_nvt.sh: submission file for double planar with the following:
  - NVT equilibration (frozen ligands)
  - NPT equilibration (frozen water)
  - NPT equilibration
  - NPT production
- submit_sam_planar_simple.sh: submission file for simple simulation:
  - NPT equilibration
  - NPT simulation
- submit_lipid_bilayer.sh: submission file for lipid bilayers
  - NPT equilibration
