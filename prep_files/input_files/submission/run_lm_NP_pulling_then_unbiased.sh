#!/bin/bash

# run_nplm_plumed_contacts_0_generate_configs.sh.sh
# this code runs the plumed number of contacts configuration generations
#
# Written by: Alex K. Chew (04/29/2020)
# 
# VARIABLES
#   $1: index that you care about 
#   $2: number of cores
#   $3: number of em cores
#   $4: md run information
#   $5: md run suffix
# VARIABLES TO CHANGE:
#   _RCFILE_ <-- RC FILE
#   _INPUTTOPFILE_ <-- topology file name within input folder
#   _OUTPUTPREFIX_ <-- output prefix
#   _MAXWARN_ <-- max warn
#   _EM_MDP_FILE_ <-- energy minimization mdp file
#   _PULL_MDP_FILE_ <-- pull file
#   _EQUIL_MDP_FILE_ <-- equilibration mdp file
#   _PROD_MDPFILE_ <-- production mdp file
#   _INDEXFILE_ <-- index file
#   _INITIALPREFIX_ <-- initial prefix that is inputted

#######################
### INPUT VARIABLES ###
#######################

## DEFINING VARIABLES
num_cores="${1:-28}"

## DEFINING GMX MDRUN
mdrun_command="${2-gmx mdrun -nt}"
# mdrun_command="ibrun -np ${num_cores} mdrun_mpi"

## DEFINING MDRUN COMMAND SUFFIX
mdrun_command_suffix="${3:-""}"

## DEFINING GROMACS COMMAND
gromacs_command="${4-gmx}"

## EM AND PROD
prod_run_command="${mdrun_command} ${num_cores} ${mdrun_command_suffix}"

##########################
### EDITABLE VARAIBLES ###
##########################

## DEFINING RC FILE
rc_file="_RCFILE_"

## GETTING DIRECTORY LOCATION
current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING CURRENT SIMULATION PATH
sim_path=$(cd ${current_dir}; pwd)

## LOADING RC
source "${sim_path}/${rc_file}"

## DEFINING TOP FILE
input_top_file="_INPUTTOPFILE_"

## DEFINING OUTPUT PREFIX
output_prefix="_OUTPUTPREFIX_"

## DEFINING INDEX FILE
index_file="_INDEXFILE_"

## DEFINING MAX WARN
max_warn="_MAXWARN_"

## DEFINING MDP FILES
declare -a mdp_files_array=("_EM_MDP_FILE_" "_PULL_MDP_FILE_" "_EQUIL_MDP_FILE_" "_PROD_MDP_FILE_")
# "_EQUIL1_MDPFILE_" "_EQUIL2_MDPFILE_" "_PROD_MDPFILE_"

## DECLARING OUTPUT PREFIXES
declare -a sim_prefix_array=("${output_prefix}_1_em" "${output_prefix}_2_pull" "${output_prefix}_3_equil" "${output_prefix}_prod")

## DEFINING PREVIOUS PREFIX
previous_sim_prefix="_INITIALPREFIX_"

## LOOPING THROUGH EACH ITERATION
for index in $(seq 0 $(( ${#mdp_files_array[@]}-1)) ); do
    ## PRINTING
    echo "------------------------------"
    echo "Part ${index}"
    echo "------------------------------"

    ## DEFINING FILES
    mdp_file="${mdp_files_array[index]}"
    sim_prefix="${sim_prefix_array[index]}"

    echo "MDP file: ${mdp_file}"
    echo "Simulation prefix: ${sim_prefix}"
    echo "Previous sim prefix: ${previous_sim_prefix}"

    ## RUNNING MD
    run_grompp_and_mdrun "${mdp_file}" \
                         "${sim_prefix}" \
                         "${previous_sim_prefix}" \
                         "${input_top_file}" \
                         "${max_warn}" \
                         "${gromacs_command}" \
                         "${prod_run_command}" \
                         "${index_file}"

    ## RE-STORING PREVIOUS PREFIX
    previous_sim_prefix="${sim_prefix}"

done

echo "Complete!"
