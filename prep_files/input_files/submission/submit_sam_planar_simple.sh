#!/bin/bash 
# submit_sam_planar_simple.sh
# This submission script is to submit for a planar SAM
## VARIABLES:
#   _JOB_NAME_ <-- name of the job
#   _NUM_CORES_ <-- Number of cores
#   _EQUILMDP_ <-- equilibration mdp file
#   _PRODMDP_ <-- production mdp file
#   _OUTPUTPREFIX_ <-- output prefix
#   _MAXWARN_ <-- max warnings
#   _INDEXNDX_ <-- index file

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## DEFINING NUMBER OF CORES
num_cores="_NUM_CORES_"

###SERVER_SPECIFIC_COMMANDS_END

## DEFINING EQUIL AND PROD MDP
equil_mdp_1="_EQUILMDP1_" # npt_double_equil_gmx5_charmm36.mdp
equil_mdp_2="_EQUILMDP2_" # npt_double_equil_gmx5_charmm36.mdp
prod_mdp="_PRODMDP_" # npt_double_prod_gmx5_charmm36.mdp 

## DEFINING INDEX FILE
index_file="_INDEXNDX_"

## DEFINING OUTPUT NAME
output_prefix="_OUTPUTPREFIX_"

## DEFINING MAX WARN
max_warn="_MAXWARN_"

######################
### EQUILIBRATION1 ###
######################

echo "==== RUNNING EQUILIBRATION 1 ===="

## DEFINING NEW PREFIX
old_prefix="${output_prefix}_em"
new_prefix="${output_prefix}_equil1"
mdp_file="${equil_mdp_1}"

## CHECKING IF COMPLETE
if [ ! -e "${new_prefix}.gro" ]; then
  ## SEEING IF XTC FILE EXISTS
  if [ -e "${new_prefix}.xtc" ]; then
    ## RESTARTING
    gmx mdrun -nt "${num_cores}" -v \
                                     -s "${new_prefix}.tpr" \
                                     -cpi "${new_prefix}.cpt" \
                                     -append -deffnm "${new_prefix}"
    else
        ## STARTING FROM BEGINNING
        gmx grompp -f "${mdp_file}" \
                   -o "${new_prefix}.tpr" \
                   -c "${old_prefix}.gro" \
                   -p "${output_prefix}.top" \
                   -n "${index_file}" \
                   -maxwarn ${max_warn} 
        gmx mdrun -nt "${num_cores}" \
                  -v \
                  -deffnm "${new_prefix}"
    fi
fi

######################
### EQUILIBRATION2 ###
######################

echo "==== RUNNING EQUILIBRATION 2 ===="

## DEFINING NEW PREFIX
if [ ! -z "${equil_mdp_2}" ]; then
  old_prefix="${output_prefix}_equil1"
  new_prefix="${output_prefix}_equil2"
  mdp_file="${equil_mdp_2}"

  ## CHECKING IF COMPLETE
  if [ ! -e "${new_prefix}.gro" ]; then
      ## SEEING IF XTC FILE EXISTS
      if [ -e "${new_prefix}.xtc" ]; then
          ## RESTARTING
          gmx mdrun -nt "${num_cores}" -v \
                                       -s "${new_prefix}.tpr" \
                                       -cpi "${new_prefix}.cpt" \
                                       -append -deffnm "${new_prefix}"
      else
          ## STARTING FROM BEGINNING
          gmx grompp -f "${mdp_file}" \
                     -o "${new_prefix}.tpr" \
                     -c "${old_prefix}.gro" \
                     -p "${output_prefix}.top" \
                     -n "${index_file}" \
                     -maxwarn ${max_warn} 
          gmx mdrun -nt "${num_cores}" \
                    -v \
                    -deffnm "${new_prefix}"
      fi
  fi
fi

##################
### PRODUCTION ###
##################

echo "==== RUNNING PRODUCTION ===="

## DEFINING NEW PREFIX
if [ ! -z "${equil_mdp_2}" ]; then
  old_prefix="${output_prefix}_equil2"
else
  old_prefix="${output_prefix}_equil1"
  fi
new_prefix="${output_prefix}_prod"
mdp_file="${prod_mdp}"

## CHECKING IF COMPLETE
if [ ! -e "${new_prefix}.gro" ]; then
    ## SEEING IF XTC FILE EXISTS
    if [ -e "${new_prefix}.xtc" ]; then
        ## RESTARTING
        gmx mdrun -nt "${num_cores}" -v \
                                     -s "${new_prefix}.tpr" \
                                     -cpi "${new_prefix}.cpt" \
                                     -append -deffnm "${new_prefix}"
    else
        ## STARTING FROM BEGINNING
        gmx grompp -f "${mdp_file}" \
                   -o "${new_prefix}.tpr" \
                   -c "${old_prefix}.gro" \
                   -p "${output_prefix}.top" \
                   -n "${index_file}" \
                   -maxwarn ${max_warn} 
        gmx mdrun -nt "${num_cores}" \
                  -v \
                  -deffnm "${new_prefix}"
    fi
fi

## COMPETION
echo "Job is complete!"
