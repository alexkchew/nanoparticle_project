#!/bin/bash 
# submit_most_likely_np_with_grid.sh
# This script runs submission for most likely nanoparticle bundling configuration, and also for generating bundling configurations
# VARIABLES:
#   _JOB_NAME_ <-- job name
#	_NUMBER_OF_CORES_ <-- number of cores to run job
#	_OUTPUTPREFIX_ <-- output prefix
#   _GRIDBASH_ <-- code to run bash code for gridding

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="_NUMBER_OF_CORES_" # number of cores you want to run with

## DEFINING BASH SCRIPT
bash_script="_BASHSCRIPT_"

## RUNNING BASH SCRIPT
bash "${bash_script}" "${num_cores}"
