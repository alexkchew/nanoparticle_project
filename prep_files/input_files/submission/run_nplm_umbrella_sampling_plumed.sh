#!/bin/bash

# run_nplm_umbrella_sampling_plumed.sh
# The purpose of this code is to run the umbrella sampling with PLUMED implemented
# simulation. 
#
# Written by: Alex K. Chew (01/15/2020)
# 
# VARIABLES
#   $1: index that you care about 
#   $2: number of cores
#   $3: number of em cores
#   $4: md run information
#   $5: md run suffix
# VARIABLES TO CHANGE:
#   _SIMFOLDER_ <-- simulation folder
#   _INPUTFOLDER_ <-- molecular information folder
#   _INPUTTOPFILE_ <-- topology file name within input folder
#   _OUTPUTPREFIX_ <-- output prefix
#   _STATUSFILE <-- status file that contains each index
#   _CURRENTJOBSTATUS_ <-- current job status
#   _MAXWARN_ <-- max warn
#   _EQUIL_MDPFILE_ <-- equilibration MDP file

## FUNCTION TO MONITOR JOBS THAT ARE COMPLETE
# The purpose of this function is to simply monitor the status of the jobs
# This will print either the job has been completed or needs to be rerun
# INPUTS:
#   $1: true/false: true if your job completed
#   $2: [optional] path to job info
# OUTPUTS:
#   This function will output to: "job_status_info"
function print_job_status () {
    ## DEFINING INPUTS
    current_job_status="$1"
    job_info_file="${2}"
    job_index="${3}"
    
    ## SEEING IF JOB INFO FILE EXISTS
    touch "${job_info_file}"
    
    ## DEFINING TEMP FILE
    temp_job_info="temp.info"
    
    ## FINDING LAMBDA NAME
    lambda_name="${job_index}"
    
    ## FINDING JOB STATUS LINE
    job_status_line=$(grep -nE "${lambda_name}" ${job_info_file} | sed 's/\([0-9]*\).*/\1/' | head -n1)
    job_status_line_text="$(grep -E "${lambda_name}" ${job_info_file} | head -n1)"
    
    ## DEFINING SUCCESD/FAIL LINES
    success_line="${lambda_name} SUCCESSFULLY COMPLETED"
    fail_line="${lambda_name} FAILED"
    
    ## SEEING IF JOB FAILED
    if [[ "${current_job_status}" == "false" ]]; then
        current_job_line="${fail_line}"
    ## SEEING IF JOB COMPLETED
    else
        current_job_line="${success_line}"
    fi
    
    ## SEEING IF JOB LIST IS EMPTY
    if [ -z "${job_status_line}" ]; then
         echo "${current_job_line}" >> ${job_info_file}
    else    
        ## REPLACE THE LINE
        sed -i "s/${job_status_line_text}/${current_job_line}/g" ${job_info_file}
    fi
    
    ## SORTING JOB INFO
    cat "${job_info_file}" | sort > "${temp_job_info}"
    cp -r "${temp_job_info}" ${job_info_file}
    if [ -e "${temp_job_info}" ]; then
        rm "${temp_job_info}"
    fi
}

#######################
### INPUT VARIABLES ###
#######################

## DEFINING INDEX TO RUN ON
index="$1"

## DEFINING VARIABLES
num_cores="${2:-28}"

## ENERGY MINIMIZATION NUMBER OF CORES
em_num_cores="${3:-28}"

## DEFINING GMX MDRUN
mdrun_command="${4-gmx mdrun -nt}"
# mdrun_command="ibrun -np ${num_cores} mdrun_mpi"

## DEFINING MDRUN COMMAND SUFFIX
mdrun_command_suffix="${5:-""}"

## DEFINING GROMACS COMMAND
gromacs_command="${6-gmx}"

## EM AND PROD
em_run_command="${mdrun_command} ${em_num_cores} ${mdrun_command_suffix}"
prod_run_command="${mdrun_command} ${num_cores} ${mdrun_command_suffix}"

##########################
### EDITABLE VARAIBLES ###
##########################

## GETTING DIRECTORY LOCATION
current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING RC FILE
rc_file="_RCFILE_"

## LOADING RC
source "${current_dir}/${rc_file}"

## DEFINING CURRENT SIMULATION PATH
sim_path=$(cd ${current_dir}/../; pwd)

## DIRECTORY STRUCTURE
sim_folder="_SIMFOLDER_"

## DEFINING INPUT FOLDER
input_folder="_INPUTFOLDER_"
input_top_file="_INPUTTOPFILE_"

## DEFINING OUTPUT PREFIX
output_prefix="_OUTPUTPREFIX_"

## DEFINING MDP FILE
equil_mdp_file="_EQUIL_MDPFILE_"
us_mdp_file="_US_MDPFILE_"

## DEFINING STATUS INFO
status_file="_STATUSFILE" # status.info

## DEFINING JOB STATUS
job_status="_CURRENTJOBSTATUS_" # current_job_status.info

## DEFINING MAXWARN
max_warn="_MAXWARN_"

## DEFINING PLUMED FILE
plumed_input="_PLUMEDINPUT_"

## DEFINING INDEX FILE
index_file="_INDEXFILE_"

####################
### RUNNING CODE ###
####################

## DEFINING TOP FILE
path_top_file="${sim_path}/${input_folder}/${input_top_file}"

## DEFINING PATH TO STATUS
path_status="${sim_path}/${status_file}"

## DEFINING PATH TO JOB
path_job="${sim_path}/${job_status}"

#######################################
### FINDING EXACT SIMULATION TO RUN ###
#######################################

## LINE NUMBER IN STATUS
line_num=$(awk '{print $1}' ${path_status} | grep -nE "\b${index}\b" | sed 's/\([0-9]*\).*/\1/')

## GETTING SIM FOLDER
current_sim_folder="$(sed "${line_num}q;d" ${path_status} | awk '{print $2}')"

## GETTING PROD RAME
total_prod_time_ps="$(sed "${line_num}q;d" ${path_status} | awk '{print $3}')"

## DEFINING PATH TO SIMULATION FILE
path_sim="${sim_path}/${sim_folder}/${current_sim_folder}"

## PRINTING
echo "Current simulation folder: ${path_sim}"
cd "${path_sim}"

###########################
### ENERGY MINIMIZATION ###
###########################
echo "-------------------------------------------"
echo "Job ${index} - Part 1 - Energy minimization"
echo "-------------------------------------------"
if [ ! -e "${output_prefix}_em.gro" ]; then
    ${em_run_command} -deffnm "${output_prefix}_em"
fi

if [ -e "${output_prefix}_em.gro" ]; then
    echo "Energy minimization complete! ${output_prefix}_em.gro is available"
else
    echo "Error! Energy minimization did not complete!"
    print_job_status "false" "${path_job}" "${index}"
    exit 1
fi

###################
### SIMULATIONS ###
###################

## DEFINING MDP FILES
# "_PULL_MDP_FILE_"
declare -a mdp_files_array=(_MDP_FILE_ARRAY_)
# "_EQUIL1_MDPFILE_" "_EQUIL2_MDPFILE_" "_PROD_MDPFILE_"

## DECLARING OUTPUT PREFIXES
declare -a sim_prefix_array=(_SIM_PREFIX_ARRAY_)

## DECLARING PLUMED ARRAYS
declare -a plumed_array=(_PLUMEDARRAY_)

## DEFINING PREVIOUS PREFIX
previous_sim_prefix="${output_prefix}_em"

## LOOPING THROUGH EACH ITERATION
for index in $(seq 0 $(( ${#mdp_files_array[@]}-1)) ); do

    ## PRINTING
    echo "------------------------------"
    echo "Part ${index}"
    echo "------------------------------"

    ## DEFINING FILES
    mdp_file="${mdp_files_array[index]}"
    sim_prefix="${sim_prefix_array[index]}"
    plumed_file="${plumed_array[index]}"

    echo "MDP file: ${mdp_file}"
    echo "Simulation prefix: ${sim_prefix}"
    echo "Previous sim prefix: ${previous_sim_prefix}"

    ## RUNNING MD
    run_grompp_and_mdrun "${mdp_file}" \
                         "${sim_prefix}" \
                         "${previous_sim_prefix}" \
                         "${path_top_file}" \
                         "${max_warn}" \
                         "${gromacs_command}" \
                         "${prod_run_command}" \
                         "${index_file}" \
                         "${plumed_file}"

    ## RE-STORING PREVIOUS PREFIX
    previous_sim_prefix="${sim_prefix}"

done

## FINDING LAST SIM PREFIX
sim_prefix="${sim_prefix_array[-1]}"
last_plumed_file="${plumed_array[-1]}"

## EXTENDING PRODUCTION SIMULATIONS
check_xtc_time prod_xtc_time "${sim_prefix}" "${gromacs_command}"

## CHECKING TO SEE IF PRODUCTION TIME MAKES SENSE
if [ -z "${total_prod_time_ps}" ]; then
    total_prod_time_ps="${prod_xtc_time}"
fi

## IF NOT EQUAL
if [[ "${prod_xtc_time}" != "${total_prod_time_ps}" ]]; then
    ## CONVERTING TPR
    ${gromacs_command} convert-tpr -s "${sim_prefix}.tpr" \
                    -until "${total_prod_time_ps}" \
                    -o "${sim_prefix}.tpr"

    ## GENERATING RESTART PLUMED FILE
    plumed_file_restart="${last_plumed_file%.*}_restart.dat"

    ## MODIFYING RESTART FILE
    echo "Modifying PLUMED file to hae a RESTART flag"
    echo "${last_plumed_file} --> ${plumed_file_restart}"

    ## COPYING
    echo "RESTART" > "${plumed_file_restart}"
    cat "${last_plumed_file}" >> "${plumed_file_restart}"


    ## RESTARTING AND APPENDING
    ${prod_run_command} -v \
                        -s "${sim_prefix}.tpr" \
                        -cpi "${sim_prefix}.cpt" \
                        -append \
                        -deffnm "${sim_prefix}" \
                        -px "${sim_prefix}_pullx.xvg" \
                        -pf "${sim_prefix}_pullf.xvg" \
                        -plumed "${plumed_file_restart}"
else
    echo "Total production time is: ${prod_xtc_time}"
    echo "That is equal to the desired production time: ${total_prod_time_ps}"
fi

## REMOVING ANY EXTRA MATERIALS
rm -f \#*

## COMPLETE, PRINTING
if [ -e "${sim_prefix}.gro" ]; then
    ## PRINTING
    echo "Job ${index} - Job completed"
    echo "Job location: ${path_sim}"
    ## ADDING TO CURRENT JOB STATUS
    print_job_status "true" "${path_job}" "${index}"

else
    print_job_status "false" "${path_job}" "${index}"
fi