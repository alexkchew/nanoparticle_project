#!/bin/bash 
## VARIABLES:
#   _JOB_NAME_ <-- job name
#   _OUTPUTNAME_ <-- output file name


###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

###SERVER_SPECIFIC_COMMANDS_END

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname

name="_OUTPUTNAME_"

## RUNNING EQUILIBRATION
gmx grompp -f npt_double_equil_gmx5_charmm36.mdp -o ${name}_equil.tpr -c ${name}_em.gro -p ${name}.top -maxwarn 5
gmx mdrun -nt 28  -v -deffnm ${name}_equil

## RUNNING PRODUCTION
gmx grompp -f npt_double_prod_gmx5_charmm36.mdp -o ${name}_prod.tpr -c ${name}_equil.gro -p ${name}.top -maxwarn 5
gmx mdrun -nt 28  -v -deffnm ${name}_prod

