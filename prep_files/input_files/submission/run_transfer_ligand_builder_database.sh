#!/bin/bash 
# run_transfer_ligand_builder_database.sh
# These codes contain information on running scripts


#######################
### INPUT VARIABLES ###
#######################

## DEFINING VARIABLES
num_cores="${1:-28}"

## DEFINING GMX MDRUN
mdrun_command="${2-gmx mdrun -nt}"
# mdrun_command="ibrun -np ${num_cores} mdrun_mpi"

## DEFINING MDRUN COMMAND SUFFIX
mdrun_command_suffix="${3:-""}"

## DEFINING GROMACS COMMAND
gromacs_command="${4-gmx}"

## EM AND PROD
prod_run_command="${mdrun_command} ${num_cores} ${mdrun_command_suffix}"

##########################
### EDITABLE VARAIBLES ###
##########################

## DEFINING RC FILE
rc_file="_RCFILE_"

## GETTING DIRECTORY LOCATION
current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING CURRENT SIMULATION PATH
sim_path=$(cd ${current_dir}; pwd)

## LOADING RC
source "${sim_path}/${rc_file}"

## DEFINING EM OUTPUT
em_gro_file="_EM_GRO_FILE_"
input_top_file="_TOPFILE_"

## DEFINING PREFIX
output_prefix="_OUTPUTPREFIX_"

## DEFINING INDEX FILE
index_file="_INDEXFILE_"

## DEFINING MAX WARN
max_warn="_MAXWARN_"

## DEFINING MDP FILES
declare -a mdp_files_array=("_EQUILMDPFILE_" "_PRODMDPFILE_")

## DECLARING OUTPUT PREFIXES
declare -a sim_prefix_array=("${output_prefix}_equil" "${output_prefix}_prod")

## DEFINING PREVIOUS PREFIX
previous_sim_prefix="_INITIALPREFIX_"

## LOOPING THROUGH EACH ITERATION
for index in $(seq 0 $(( ${#mdp_files_array[@]}-1)) ); do
    ## PRINTING
    echo "------------------------------"
    echo "Part ${index}"
    echo "------------------------------"

    ## DEFINING FILES
    mdp_file="${mdp_files_array[index]}"
    sim_prefix="${sim_prefix_array[index]}"

    ## DEFINING MDP FILE
    echo "MDP file: ${mdp_file}"
    echo "Simulation prefix: ${sim_prefix}"
    echo "Previous sim prefix: ${previous_sim_prefix}"

    ## RUNNING MD
    run_grompp_and_mdrun "${mdp_file}" \
                         "${sim_prefix}" \
                         "${previous_sim_prefix}" \
                         "${input_top_file}" \
                         "${max_warn}" \
                         "${gromacs_command}" \
                         "${prod_run_command}" \
                         "${index_file}"

    ## RE-STORING PREVIOUS PREFIX
    previous_sim_prefix="${sim_prefix}"


done

## PRINTING
echo "Job has completed!"

