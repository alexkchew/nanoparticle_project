#!/bin/bash 

# submit_nplm_us_sims.sh
# This submission scripts run umbrella sampling simulations
# VARIABLES TO CHANGE:
#   _JOB_NAME_ <-- job name
#   _NUMBER_OF_CORES_ <-- number of cores to use
#   _RUNSCRIPT_ <-- run script
#   _EMNUMCORES_ <-- number of energy minimization cores
#   _MDRUNCOMMAND_ <-- md run command
#   _MDRUNCOMMANDSUFFIX_ <-- suffix for the command

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="_NUMBER_OF_CORES_" # number of cores you want to run with
em_num_cores="_EMNUMCORES_" # number of cores to energy minimize
mdrun_command="_MDRUNCOMMAND_" # command to run code
mdrun_command_suffix="_MDRUNCOMMANDSUFFIX_" # suffix
gromacs_command="_GROMACSCOMMAND_"

## LOADING GROMACS
load_gmx_2016-6_plumed_2-5-1

###SERVER_SPECIFIC_COMMANDS_END

## DEFINING RUN SCRIPT
run_script="_RUNSCRIPT_"

## DEFINING OUTPUT LOG FILE
run_script_basename=$(basename ${run_script})
output_file="output_${run_script_basename%.sh}.log"

## RUNNING COMMANDS
bash "${run_script}" "${num_cores}" \
                     "${em_num_cores}" \
                     "${mdrun_command}" \
                     "${mdrun_command_suffix}" \
                     "${gromacs_command}" > ${output_file} 2>&1
