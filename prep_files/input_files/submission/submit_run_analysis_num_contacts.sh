#!/bin/bash 

# submit_run_analysis_num_contacts
# The purpose of this script is to run the analysis for number of contacts
# This submission scripts run umbrella sampling simulations
# VARIABLES TO CHANGE:
#   _JOB_NAME_ <-- job name
#   _RUNSCRIPT_ <-- run script

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

###SERVER_SPECIFIC_COMMANDS_END

## DEFINING RUN SCRIPT
run_script="_RUNSCRIPT_"

## RUNNING COMMANDS
bash "${run_script}"