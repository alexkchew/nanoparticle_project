#!/bin/bash 
# submit_most_likely_np
# This script runs submission for most likely nanoparticle bundling configuration
# VARIABLES:
#   _JOB_NAME_ <-- job name
#	_NUMBER_OF_CORES_ <-- number of cores to run job
#	_OUTPUTPREFIX_ <-- output prefix
#	_MDPEQUIL1_ <-- equilibration file
#	_MDPEQUIL2_ <-- equilbration file 3
#	_MDPPROD_ <-- production file

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J _JOB_NAME_
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="_NUMBER_OF_CORES_" # number of cores you want to run with

###SERVER_SPECIFIC_COMMANDS_END


# SUBMIT THIS SCRIPT using the command sbatch thisscriptname

## DEFINING MDP FILES
mdp_file_equil1="_MDPEQUIL1_"
mdp_file_equil2="_MDPEQUIL2_"
mdp_file_prod="_MDPPROD_"

## DEFINING MAX WARN
max_warn="_MAXWARN_"

## DEFINING PREFIX
output_prefix="_OUTPUTPREFIX_"

## DEFINING INDEX FILE
index_file="_INDEXFILE_"

######################
### EQUILIBRATION1 ###
######################

echo "==== RUNNING EQUILIBRATION 1 ===="

## DEFINING NEW PREFIX
old_prefix="${output_prefix}"
new_prefix="${output_prefix}_equil1"
mdp_file="${mdp_file_equil1}"

## CHECKING IF COMPLETE
if [ ! -e "${new_prefix}.gro" ]; then
  ## SEEING IF XTC FILE EXISTS
  if [ -e "${new_prefix}.xtc" ]; then
    ## RESTARTING
    gmx mdrun -nt "${num_cores}" -v \
                                     -s "${new_prefix}.tpr" \
                                     -cpi "${new_prefix}.cpt" \
                                     -append -deffnm "${new_prefix}"
    else
        ## STARTING FROM BEGINNING
        gmx grompp -f "${mdp_file}" \
                   -o "${new_prefix}.tpr" \
                   -c "${old_prefix}.gro" \
                   -p "${output_prefix}.top" \
                   -n "${index_file}" \
                   -maxwarn ${max_warn} 
        gmx mdrun -nt "${num_cores}" \
                  -v \
                  -deffnm "${new_prefix}"
    fi
fi

######################
### EQUILIBRATION2 ###
######################

echo "==== RUNNING EQUILIBRATION 2 ===="

## DEFINING NEW PREFIX
old_prefix="${output_prefix}_equil1"
new_prefix="${output_prefix}_equil2"
mdp_file="${mdp_file_equil2}"

## CHECKING IF COMPLETE
if [ ! -e "${new_prefix}.gro" ]; then
  ## SEEING IF XTC FILE EXISTS
  if [ -e "${new_prefix}.xtc" ]; then
    ## RESTARTING
    gmx mdrun -nt "${num_cores}" -v \
                                     -s "${new_prefix}.tpr" \
                                     -cpi "${new_prefix}.cpt" \
                                     -append -deffnm "${new_prefix}"
    else
        ## STARTING FROM BEGINNING
        gmx grompp -f "${mdp_file}" \
                   -o "${new_prefix}.tpr" \
                   -c "${old_prefix}.gro" \
                   -p "${output_prefix}.top" \
                   -n "${index_file}" \
                   -maxwarn ${max_warn} 
        gmx mdrun -nt "${num_cores}" \
                  -v \
                  -deffnm "${new_prefix}"
    fi
fi


##################
### PRODUCTION ###
##################

echo "==== RUNNING PRODUCTION ===="

## DEFINING NEW PREFIX
old_prefix="${output_prefix}_equil2"
new_prefix="${output_prefix}_prod"
mdp_file="${mdp_file_prod}"

## CHECKING IF COMPLETE
if [ ! -e "${new_prefix}.gro" ]; then
  ## SEEING IF XTC FILE EXISTS
  if [ -e "${new_prefix}.xtc" ]; then
    ## RESTARTING
    gmx mdrun -nt "${num_cores}" -v \
                                     -s "${new_prefix}.tpr" \
                                     -cpi "${new_prefix}.cpt" \
                                     -append -deffnm "${new_prefix}"
    else
        ## STARTING FROM BEGINNING
        gmx grompp -f "${mdp_file}" \
                   -o "${new_prefix}.tpr" \
                   -c "${old_prefix}.gro" \
                   -p "${output_prefix}.top" \
                   -n "${index_file}" \
                   -maxwarn ${max_warn} 
        gmx mdrun -nt "${num_cores}" \
                  -v \
                  -deffnm "${new_prefix}"
    fi
fi

## COMPETION
echo "Job is complete!"
