#!/bin/bash 

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J JOB_NAME
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

# INSTRUCTIONS:
# The -t command specifies how long this simulation will run for (in HOURS:MINUTES:SECONDS). Try to estimate this as best you can
# as the simulation will temrinate after this time.
# The -J flag species the name of the simulation
# the --mail-user command will send you email when the job runs / terminates
# Do not change the other flags, really.

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname

name=sam_8x7_6C_amine_methanol

gmx grompp -f npt_double_equil_gmx5_opls.mdp -o ${name}_equil.tpr -c ${name}_em.gro -p ${name}.top
gmx mdrun -nt 28  -v -deffnm ${name}_equil

gmx grompp -f npt_double_prod_gmx5_opls.mdp -o ${name}_prod.tpr -c ${name}_equil.gro -p ${name}.top
gmx mdrun -nt 28  -v -deffnm ${name}_prod

