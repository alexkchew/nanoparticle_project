#!/bin/bash 

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J JOB_NAME
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

# INSTRUCTIONS:
# The -t command specifies how long this simulation will run for (in HOURS:MINUTES:SECONDS). Try to estimate this as best you can
# as the simulation will temrinate after this time.
# The -J flag species the name of the simulation
# the --mail-user command will send you email when the job runs / terminates
# Do not change the other flags, really.

# SUBMIT THIS SCRIPT using the command sbatch thisscriptname
# Done after energy minimization
gro_file=GRO_NAME
top_file=TOP_NAME
equil_mdp=EQUIL_MDP_NAME
prod_mdp=PROD_MDP_NAME

gro_file_nogro=${gro_file%.gro}

gmx grompp -f $equil_mdp -o ${gro_file_nogro}_equil.tpr -c ${gro_file_nogro}.gro -p $top_file
gmx mdrun -nt 28 -v -deffnm ${gro_file_nogro}_equil

#gmx grompp -f $prod_mdp -o ${gro_file_nogro}_prod.tpr -c ${gro_file_nogro}_equil.gro -p $top_file
#gmx mdrun -nt 28 -v -deffnm ${gro_file_nogro}_prod