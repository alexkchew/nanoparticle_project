#!/bin/bash

# define input variables that we assume are passed into this script
sam_name=$1 # input gro file
top_name=$2 # output top file
methanol_z=$3 # meter box z-size in nm
numthreads=1
sam_name_nogro=${sam_name%.gro}

# get current box vectors
echo "Adding methanol box to SAM (GROMACS output is suppressed; errors will not appear):"

#get box vectors, save into different components
output=$(tail -n 1 $sam_name)
# create as array, can now access each component separately
array=($output)
box_x=${array[0]}
box_y=${array[1]}
box_z=${array[2]}
echo "   Current box vectors: $box_x $box_y $box_z"

#get new z
new_z=$(awk "BEGIN {print $box_z + $methanol_z; exit}")

echo "   New box z vector: $new_z"
#don't center the system; just increase z dimension
gmx editconf -f $sam_name -o ${sam_name_nogro}_resize.gro -box $box_x $box_y $new_z -noc &>/dev/null

# now make new box of required size, pure solvent. Note input is from previously equilibrated methanol box with 128 molecules.
gmx solvate -cs methanol_equil.gro -o metbox.gro -box $box_x $box_y $methanol_z &>/dev/null

#resize methanol box, translate to appropriate position relative to system
gmx editconf -f metbox.gro -o metbox_resize.gro -box $box_x $box_y $new_z -noc -translate 0 0 $box_z &>/dev/null

# Now combine files, recenter them, renumber them, remove unnecessary lines
# first - determine how many atoms final file will have
sam_num_atoms=$(head -n 2 "$sam_name_nogro"_resize.gro | tail -n 1)
sam_trim="${sam_num_atoms%"${sam_num_atoms##*[![:space:]]}"}"
#remove leading whitespace too
sam_trim="${sam_trim#"${sam_trim%%[![:space:]]*}"}"

# repeat for methanol box
met_num_atoms=$(head -n 2 metbox_resize.gro | tail -n 1)
met_trim="${met_num_atoms%"${met_num_atoms##*[![:space:]]}"}"
#remove leading whitespace too
met_trim="${met_trim#"${met_trim%%[![:space:]]*}"}"

total_num_atoms=$(($met_trim + $sam_trim))
echo "   Previous number of atoms = $sam_trim ; Methanol number of atoms = $met_trim ; Total = $total_num_atoms"
sleep 2

# adjust gro files and concatenate
sed -i '1d' metbox_resize.gro
sed -i '1d' metbox_resize.gro
sed -i '$d' "$sam_name_nogro"_resize.gro

# now concatenate with previous box
cat "$sam_name_nogro"_resize.gro metbox_resize.gro > "$sam_name_nogro"_addmet.gro

# replace number of atoms with combined number of atoms
sed -i -e 0,/$sam_trim/s/$sam_trim/$total_num_atoms/ "$sam_name_nogro"_addmet.gro

# renumber using genconf, don't center (since we do not have topology info)
gmx genconf -f "$sam_name_nogro"_addmet.gro -o "$sam_name_nogro"_addmet.gro -renumber &>/dev/null

# clean up unnecessary files
rm metbox.gro
rm metbox_resize.gro
rm "$sam_name_nogro"_resize.gro

# update topology file
echo "   Updating topology file"
cp $top_name ${top_name}.backup
# divide by 6, since that is number of atoms in methanol
new_num_met=$(($met_trim/6))
echo " MET    $new_num_met" >> $top_name
