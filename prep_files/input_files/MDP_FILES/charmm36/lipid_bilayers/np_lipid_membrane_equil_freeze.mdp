; np_lipid_membrane_equil_freeze.mdp
; This MDP file is equilibration of nanoparticle above the lipid membrane

; NPT equilibrium semianisotropic
; ADJUSTABLE VARIABLES
;   _GROUP_1_NAME_ <-- group 1 name
;   _GROUP_2_NAME_ <-- group 2 name
;   _DT_ <-- time step
;   _NSTEPS_ <-- number of steps
;   _TEMPERATURE_ <-- temperature of the system in K
;   _PULLKCONSTANT_ <-- pull K constant
;   _PULLINIT_ <-- initial pulling

; DEFINING TITLE
title		= Lipid membrane NPT equilibration

; INTEGRATOR
integrator	= md		; leap-frog integrator
cutoff-scheme	= Verlet
nsteps		= _NSTEPS_	; 4 ns
dt		= _DT_		; 2 fs

; OUTPUT CONTROL
nstxout		= 0		    ;
nstvout		= 0		    ;
nstxtcout	= 50000  	; save every 100 ps (for initial equil)
nstenergy	= 100		; save energies every 0.2 ps
nstlog		= 100		; update log file every 0.2 ps
nstcalcenergy	= 100	; calculate energy every 100 steps

; VELOCITIES
continuation   = yes    ; yes if you want continuation
gen_vel		   = no   ; assign velocities from Maxwell distribution

; CENTER OF MASS REMOVAL
comm-mode	= Linear	; remove COM motion; possibly freeze Au atoms?
nstcomm		= 100		; remove every 100 steps

; CONSTRAINTS
constraint_algorithm = lincs	    ; holonomic constraints
constraints	         = h-bonds    ; all bonds (even heavy atom-H bonds) constrained  -- could be "h-bonds", typical for CHARMM
lincs_iter	         = 1		    ; more accurate for initial equil
lincs_order	         = 4		    ; increase for more accuracy (can decrease to 5 later)

; VDW INTERACTIONS
vdwtype		   = Cutoff	        ; VDW cutoff type
vdw-modifier   = force-switch	; According to CHARMM, http://www.gromacs.org/Documentation/Terminology/Force_Fields/CHARMM
rvdw		   = 1.2		    ; for switching function -- updated for CHARMM
rvdw-switch	   = 1.0		    ; when to start switching off force -- updated for CHARMM

; ELECTROSTATIC INTERACTIONS
coulombtype	= PME
rcoulomb	= 1.2		; ibid -- updated for CHARMM

; NEIGHBOR SEARCHING
ns-type		= grid		; include all atoms in neighbor list
nstlist		= 10		; recommended for verlet
rlist		= 1.2		; for CHARMM

; TEMPERATURE COUPLING
tcoupl		= v-rescale ; Used to get correct thermostat
ref_t		= _TEMPERATURE_
tau_t		= 1.0            ; 0.1 to 1.0
tc-grps		= System

; PRESSURE COUPLING
pcoupl		    = no ; no temperature coupling in umbrella periodic
pcoupltype      = semiisotropic 
tau_p		    = 5.0
compressibility    = 4.5e-5  4.5e-5
ref_p              = 1.0     1.0

; COORDINATE SCALING
refcoord_scaling   = com

; CORRECTION
DispCorr	= EnerPres

; PERIODIC BOUNDARY CONDITIONS
pbc		    = xyz ; PBC in all direction

; PULL CODE
pull                    = yes
pull_ngroups            = 2
pull_ncoords            = 1
pull_group1_name        = _GROUP_1_NAME_
pull_group2_name        = _GROUP_2_NAME_
pull_coord1_type        = umbrella          ; harmonic biasing force
pull_coord1_geometry    =  direction-periodic  ; pulling based on distance
pull_coord1_groups      = 1 2               ; Pull 1 to 2
pull-coord1-vec 		= 0.0 0.0 1.0		; pull along the z-axis
pull-coord1-init        = _PULLINIT_        ; Initial pulling distances
pull_coord1_dim         = Y Y Y                     ; pull moves x, y, z direction
pull_coord1_start       = no                   ; initiate by starting position (turn off init)
; pull-coord1-init 		= yes	                    ; use the initial distance as a first frame of puling
pull_coord1_rate        = 0.000                 ; No changes in rate
pull_coord1_k           = _PULLKCONSTANT_           ; kJ mol^-1 nm^-2     

