; npt_double_prod_gmx5_charmm36_10ps
; Same as npt_double_prod_gmx5_charmm36 but with 10 ps output
title		= AuNP NPT Production @ TEMPERATURE K Spherical Ligand Case CHARMM36
; Run parameters
integrator	= md		; leap-frog integrator
cutoff-scheme	= Verlet
nsteps		= NSTEPS	; 4 ns
dt		= 0.002		; 2 fs

; Output control
nstxout		= 0		;
nstvout		= 0		;
nstxtcout	= 5000  		; save every 10 ps (for initial equil)
nstenergy	= 100		; save energies every 0.2 ps
nstlog		= 100		; update log file every 0.2 ps

; Bond parameters
continuation	= yes		;
constraint_algorithm = lincs	; lincs -- updated 02/25/2018 holonomic constraints
constraints	= all-bonds	; all bonds (even heavy atom-H bonds) constrained  -- could be "h-bonds", typical for CHARMM
;lincs_iter	= 1		; more accurate for initial equil
;lincs_order	= 4		; inccrease for more accuracy (can decreaseto 5 later)

; Neighborsearching
ns_type		= grid		; include all atoms in neighbor list
nstlist		= 10		; recommended for verlet
rlist		= 1.2		; for CHARMM
rcoulomb	= 1.2		; ibid -- updated for CHARMM
rvdw		= 1.2		; for switching function -- updated for CHARMM
rvdw-switch	= 1.0		; when to start switching off force -- updated for CHARMM

; Electrostatics
vdwtype		= Cutoff	;
vdw-modifier 	= force-switch	; According to CHARMM, http://www.gromacs.org/Documentation/Terminology/Force_Fields/CHARMM
coulombtype	= PME

; Temperature coupling is on
tcoupl		= v-rescale		; temp coupling
ref_t		= TEMPERATURE
tau_t		= 0.1
tc-grps		= System

; Pressure coupling is on -- Using Berendsen for quick equilibartion.
pcoupl		= Parrinello-Rahman 	; no pressure coupling in NVT Parrinello-Rahman correctly outputs NPT ensemble!
pcoupltype	= isotropic	; allow for all pressure directions
tau_p		= 1
ref_p		= 1
compressibility	= 4.5e-5
refcoord-scaling = all

; Periodic boundary conditions
pbc		= xyz
; Dispersion correction
DispCorr	= EnerPres ; Questionable -- need to re-evaluate, should this be "no" ?s
; Velocity generation
gen_vel		= no		; assign velocities from Maxwell distribution <- No velocity generation after equilibration!
comm-mode	= Linear	; remove COM motion; possibly freeze Au atoms?
nstcomm		= 10		; remove every step
nstcalcenergy	= 5		; calculate energy every step

; Updates by Alex Chew (04/01/2017) -- Linear to Spherical Case
; Changed refcoord-scaling
; Made pcoupl Parrinello-Rahman for mdp file production
; changed pcoupltype to isotropic
; changed ref_p from "0 0 1 0 0 0" to 1
; changed compressibility from "0 0 4.5e-5 0 0 0" to "4.5e-5"
