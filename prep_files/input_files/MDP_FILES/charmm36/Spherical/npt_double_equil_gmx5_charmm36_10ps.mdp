; npt_double_equil_gmx5_charmm36_10ps.mdp
; This simply equilibrates your system
; Same as npt_double_equil_gmx5_charmm36 but with 10 ps outputs
; VARIABLES:
;   NSTEPS <-- total number of steps
;   TEMPERATURE <-- desired temperature

title		= NPT Equilibration @ TEMPERATURE K Spherical Case CHARMM36
; Run parameters
integrator	= md		; leap-frog integrator
cutoff-scheme	= Verlet
nsteps		= NSTEPS	; 4 ns
dt		= 0.002		; 2 fs

; Output control
nstxout		= 0		;
nstvout		= 0		;
nstxtcout	= 5000  		; save every 10 ps (for initial equil)
nstenergy	= 100		; save energies every 0.2 ps
nstlog		= 100		; update log file every 0.2 ps

; Bond parameters
continuation	= no		;
constraint_algorithm = lincs	; lincs -- updated 02/25/2018 holonomic constraints
constraints	= all-bonds	; all bonds (even heavy atom-H bonds) constrained  -- could be "h-bonds", typical for CHARMM
;lincs_iter	= 1		; more accurate for initial equil
;lincs_order	= 4		; inccrease for more accuracy (can decrease to 5 later)

; Neighborsearching
ns_type		= grid		; include all atoms in neighbor list
nstlist		= 10		; recommended for verlet
rlist		= 1.2		; for CHARMM
rcoulomb	= 1.2		; ibid -- updated for CHARMM
rvdw		= 1.2		; for switching function -- updated for CHARMM
rvdw-switch	= 1.0		; when to start switching off force -- updated for CHARMM

; Electrostatics
vdwtype		= Cutoff	;
vdw-modifier 	= force-switch	; According to CHARMM, http://www.gromacs.org/Documentation/Terminology/Force_Fields/CHARMM
coulombtype	= PME
; Temperature coupling is on
tcoupl		= v-rescale		; temp coupling
ref_t		= TEMPERATURE
tau_t		= 0.1
tc-grps		= System
; Pressure coupling is on -- Using Berendsen for quick equilibartion.
pcoupl		= Berendsen 	; Berendsen no pressure coupling in NVT
pcoupltype	= isotropic	; allow for all pressure directions
tau_p		= 1
ref_p		= 1
compressibility	= 4.5e-5
refcoord-scaling = all

; Periodic boundary conditions
pbc		= xyz
; Dispersion correction
DispCorr	= EnerPres ; Questionable -- need to re-evaluate, should this be "no" ?s
; Velocity generation
gen_vel		= yes		; assign velocities from Maxwell distribution
comm-mode	= Linear	; remove COM motion; possibly freeze Au atoms?
nstcomm		= 10		; remove every step
nstcalcenergy	= 5		; calculate energy every step
