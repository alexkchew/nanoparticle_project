; nvt_double_prod_gmx5_charmm36_frozen_sam.mdp
; This is a production simulation for planar SAMs with the NVT ensemble
; This uses freeze groups
; VARIABLES:
;   _NSTEPS_ <-- Number of steps
;   _TEMPERATURE_ <-- temperature of the system in K
;	_GOLDRESNAME_ <-- gold residue name
; 	_LIGANDRESNAME_ <-- ligand residue name

; TITLE
title		= AuNP NVT Production @ _TEMPERATURE_ K Planar Frozen Ligand Case CHARMM36

; RUN PARAMETERS
integrator	= md		; leap-frog integrator
cutoff-scheme	= Verlet
nsteps		= _NSTEPS_	; 4 ns
dt		= 0.002		; 2 fs

; FREEZE GROUPS
freezegrps  = _GOLDRESNAME_ _LIGANDRESNAME_
freezedim   = Y Y Y Y Y Y

; OUTPUT CONTROL
nstxout		= 0		
nstvout		= 0		
nstxtcout	= 500  		; save every 1 ps (slow snapshots)
nstenergy	= 100		; save energies every 0.2 ps
nstlog		= 100		; update log file every 0.2 ps

; BONDING PARAMETERS
constraint_algorithm = lincs	    ; holonomic constraints
constraints	         = all-bonds	; all bonds (even heavy atom-H bonds) 
lincs-order          = 4            ; default
lincs-iter           = 1            ; default
lincs-warnangle       = 30           ; default

; NEIGHBOR SEARCHING
; According to CHARMM, http://www.gromacs.org/Documentation/Terminology/Force_Fields/CHARMM
ns_type		= grid		; include all atoms in neighbor list
nstlist		= 10		; recommended for verlet
rlist		= 1.2		; for CHARMM

; VAN DER WAALS
vdwtype			= Cutoff		;
vdw-modifier 	= force-switch	
rvdw			= 1.2			; for switching function -- updated for CHARMM
rvdw-switch		= 1.0			; when to start switching off force -- updated for CHARMM

; ELECTROSTATICS
coulombtype		= PME
rcoulomb		= 1.2				; ibid -- updated for CHARMM

; TEMPERATURE COUPLING
tcoupl		= v-rescale		; temp coupling
ref_t		= _TEMPERATURE_
tau_t		= 0.1
tc-grps		= System

; PRESSURE COUPLING
pcoupl		= no 			; No pressure coupling for NVT (Fixed box size)
pcoupltype	= anisotropic	; only allow coupling in z-direction
tau_p		= 1
ref_p		= 0 0 1 0 0 0
compressibility	= 0 0 4.5e-5 0 0 0
refcoord-scaling = all

; PERIODIC BOUNDARY CONDITIONS
pbc		= xyz
; DISPERSION CORRECTION
DispCorr	= EnerPres 

; VELOCITY GENERATION
continuation	= yes       ; continuation of the simulation
gen_vel		    = no	    ; No velocity generation after equilibration!
comm-mode	    = Linear	; remove COM motion; possibly freeze Au atoms?
nstcomm		    = 10		; remove every step
nstcalcenergy	= 5		    ; calculate energy every step
