#!/bin/bash
# add_water_gmx5.sh
# The purpose of this script is to create a water box given the dimensions of your system. 
# VARIABLES:
#   $1: sam name
#   $2: topology name
#   $3: Water z length
#   $4: empty z length to prevent blowing up

# define input variables that we assume are passed into this script
sam_name=$1 # output gro file from prep_surface_gromos.pys
top_name=$2 # output top file
water_z=$3 # water box z-size in nm
numthreads=1
sam_name_nogro=${sam_name%.gro}

# get current box vectors
echo "Adding water to SAM (GROMACS output is suppressed; errors will not appear):"

#get box vectors, save into different components
output=$(tail -n 1 $sam_name)
# create as array, can now access each component separately
array=($output)
box_x=${array[0]}
box_y=${array[1]}
box_z=${array[2]}
echo "   Current box vectors: $box_x $box_y $box_z"

# Creating an empty layer to prevent blowing up in the beginning
empty_z="${4:-0.05}"
# "0.05" # "0.01" # "0.1" # 0.8 in nms

# Getting new z
beg_z=$(awk "BEGIN {print $box_z + ${empty_z}; exit}")
#get new z
new_z=$(awk "BEGIN {print $box_z + $water_z + 2*${empty_z}; exit}") #170427 -- Added a 0.1 nm box length to edge
#new_z=$(echo "$box_z + $water_z" | bc)

echo "   New box z vector: $new_z"
#don't center the bilayer; just increase z dimension (increase box size)
gmx editconf -f $sam_name -o ${sam_name_nogro}_resize.gro -box $box_x $box_y $new_z -noc &>/dev/null

# now make new box of required size, pure solvent
gmx solvate -cs spc216.gro -o watbox.gro -box $box_x $box_y $water_z &>/dev/null

#resize water box, translate to appropriate position relative to bilayer
gmx editconf -f watbox.gro -o watbox_resize.gro -box $box_x $box_y $new_z -noc -translate 0 0 $beg_z &>/dev/null # Changing box_z to beg_z

# Now combine files, recenter them, renumber them, remove unnecessary lines
# first - determine how many atoms final file will have
sam_num_atoms=$(head -n 2 "$sam_name_nogro"_resize.gro | tail -n 1)
sam_trim="${sam_num_atoms%"${sam_num_atoms##*[![:space:]]}"}"
#remove leading whitespace too
sam_trim="${sam_trim#"${sam_trim%%[![:space:]]*}"}"

# repeat for water box
wat_num_atoms=$(head -n 2 watbox_resize.gro | tail -n 1) # Gets total number of water molecules
wat_trim="${wat_num_atoms%"${wat_num_atoms##*[![:space:]]}"}"
#remove leading whitespace too
wat_trim="${wat_trim#"${wat_trim%%[![:space:]]*}"}"

total_num_atoms=$(($wat_trim + $sam_trim))
echo "   SAM number of atoms = $sam_trim ; Water number of atoms = $wat_trim ; Total = $total_num_atoms"
sleep 2

# adjust gro files and concatenate
sed -i '1d' watbox_resize.gro
sed -i '1d' watbox_resize.gro
sed -i '$d' "$sam_name_nogro"_resize.gro

# now concatenate with previous box
cat "$sam_name_nogro"_resize.gro watbox_resize.gro > "$sam_name_nogro"_addwat.gro

# replace number of atoms with combined number of atoms
sed -i -e 0,/$sam_trim/s/$sam_trim/$total_num_atoms/ "$sam_name_nogro"_addwat.gro

# renumber using genconf, don't center (since we do not have topology info)
gmx genconf -f "$sam_name_nogro"_addwat.gro -o "$sam_name_nogro"_addwat.gro -renumber &>/dev/null

# clean up unnecessary files
rm watbox.gro
rm watbox_resize.gro
rm "$sam_name_nogro"_resize.gro

# update topology file
echo "   Updating topology file"
cp $top_name ${top_name}.backup
new_num_wat=$(($wat_trim/3))
echo " SOL    $new_num_wat" >> $top_name
