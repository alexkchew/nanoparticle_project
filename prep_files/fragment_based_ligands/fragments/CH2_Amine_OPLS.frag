# Methylammonium end group (for alkane chain); protonated
# Based on lysine end group; should double check parameterization

#   H   H
#   |   |
# - C - N - H +
#   |   |
#   H   H

# Define atom number
NUM_ATOMS 7
NUM_BONDS 7

# Flag - if 1, invert y coordinates of next fragment; if 1, does not invert
INVERT_Y 1

# atom types to define for each, along with charge, etc.
[ ATOMTYPES ]
# atom number, type, name, charge, mass
1 opls_292    C    0.190    12.011
2 opls_140    H    0.060     1.008
3 opls_140    H    0.060     1.008
4 opls_287    N   -0.300    14.007
5 opls_290    H    0.330     1.008
6 opls_290    H    0.330     1.008
7 opls_290    H    0.330     1.008

[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. Hard coded.
1   0.0      0.0     0.0
2   0.088    0.064   0.0
3  -0.088    0.064   0.0
4   0.0     -0.085   0.247
5   0.082   -0.142   0.247
6  -0.082   -0.142   0.247
7   0.0      0.028   0.334
# include N out of necessity but will not be used
N   0.0      0.0     0.0

# math for 2 hyd: x = +-sin(0.5*HCH angle)*C_H bond = 0.088 nm
#                 y = + cos(0.5*HCH angle)*C_H bond = 0.064 nm
#  HCH angle = 107.8 degrees (1.8815 rad), C_H bond = 0.109 nm

# For nitrogen = tetrahedral geometry; approximate angle as 110 degrees
# math for next: x = 0.0
#                y =  cos(0.5*angle)*C_H bond =  0.085 nm
#                z =  sin(0.5*angle)*C_H bond =  0.121 nm + 0.126 nm =
# C-N bond = 0.148 nm

# next 3 hydrogen atoms: same basic math as above, but use N-H bond (0.1 nm), multiply
# y by -1, assume 110 degree tetrahedral angle approximately.


# Single line indicated "dangling" bond for previous/next residue
# No next fragment atom for this one by definition

[ BONDS ]
1
1 2
1 3
1 4
4 5
4 6
4 7
