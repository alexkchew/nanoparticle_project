# Created by frag_builder.py 
# INPUT PDB File: /home/akchew/scratch/LigandBuilder/Prep_System/prep_ligand/minimized_structures/dodecanethiol/dodecanethiol.pdb 
# INPUT ITP File: /home/akchew/scratch/LigandBuilder/Prep_System/prep_ligand/minimized_structures/dodecanethiol/dodecanethiol.itp 
# Note: All comments with hashtag is not read
# Atom ranges: (1, 39)

# Defining number of atoms and bonds 
NUM_ATOMS 38 
NUM_BONDS 37 

# Inversion flag: -1 inverts y coordinates of next fragment; if 1 -- do not invert
INVERT_Y 1

# Atom types - defining each atom type in fragment
[ ATOMTYPES ]
# atom number, type, name, charge, mass 
1    SG311  S  -0.082    32.060    
2    CG321  C  -0.099    12.011    
3    HGA2   H  0.090     1.008     
4    HGA2   H  0.090     1.008     
5    CG321  C  -0.179    12.011    
6    HGA2   H  0.090     1.008     
7    HGA2   H  0.090     1.008     
8    CG321  C  -0.180    12.011    
9    HGA2   H  0.090     1.008     
10   HGA2   H  0.090     1.008     
11   CG321  C  -0.180    12.011    
12   HGA2   H  0.090     1.008     
13   HGA2   H  0.090     1.008     
14   CG321  C  -0.180    12.011    
15   HGA2   H  0.090     1.008     
16   HGA2   H  0.090     1.008     
17   CG321  C  -0.180    12.011    
18   HGA2   H  0.090     1.008     
19   HGA2   H  0.090     1.008     
20   CG321  C  -0.180    12.011    
21   HGA2   H  0.090     1.008     
22   HGA2   H  0.090     1.008     
23   CG321  C  -0.180    12.011    
24   HGA2   H  0.090     1.008     
25   HGA2   H  0.090     1.008     
26   CG321  C  -0.180    12.011    
27   HGA2   H  0.090     1.008     
28   HGA2   H  0.090     1.008     
29   CG321  C  -0.180    12.011    
30   HGA2   H  0.090     1.008     
31   HGA2   H  0.090     1.008     
32   CG321  C  -0.180    12.011    
33   HGA2   H  0.090     1.008     
34   HGA2   H  0.090     1.008     
35   CG331  C  -0.270    12.011    
36   HGA3   H  0.090     1.008     
37   HGA3   H  0.090     1.008     
38   HGA3   H  0.090     1.008     

# Geom - Defining each atom xyz coordinates 
[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. 
1    0.000     0.000     0.000     
2    -0.048    -0.078    0.156     
3    -0.006    -0.181    0.162     
4    -0.159    -0.087    0.161     
5    0.001     0.002     0.278     
6    0.112     0.011     0.274     
7    -0.041    0.106     0.274     
8    -0.040    -0.063    0.410     
9    0.003     -0.166    0.414     
10   -0.150    -0.072    0.414     
11   0.011     0.017     0.530     
12   0.121     0.026     0.526     
13   -0.031    0.120     0.525     
14   -0.030    -0.049    0.662     
15   0.012     -0.152    0.666     
16   -0.141    -0.057    0.666     
17   0.020     0.032     0.782     
18   0.130     0.041     0.778     
19   -0.022    0.135     0.777     
20   -0.021    -0.034    0.914     
21   0.021     -0.137    0.918     
22   -0.131    -0.042    0.918     
23   0.029     0.047     1.034     
24   0.139     0.055     1.029     
25   -0.013    0.150     1.029     
26   -0.012    -0.019    1.166     
27   0.030     -0.121    1.170     
28   -0.122    -0.028    1.169     
29   0.037     0.062     1.286     
30   0.148     0.071     1.281     
31   -0.004    0.165     1.281     
32   -0.002    -0.003    1.418     
33   0.040     -0.107    1.422     
34   -0.113    -0.013    1.422     
35   0.047     0.076     1.538     
36   0.158     0.085     1.537     
37   0.016     0.026     1.632     
38   0.005     0.179     1.536     
# There is no next atom, but writing so ligand builder works
N    0.000     0.000     0.000     

# Bonds - Defining each bond 
[ BONDS ]
1  2  
2  3  
2  4  
2  5  
5  8  
5  6  
5  7  
8  9  
8  10 
8  11 
11 12 
11 13 
11 14 
14 16 
14 17 
14 15 
17 18 
17 19 
17 20 
20 21 
20 22 
20 23 
23 24 
23 25 
23 26 
26 27 
26 28 
26 29 
29 32 
29 30 
29 31 
32 33 
32 34 
32 35 
35 36 
35 37 
35 38 

