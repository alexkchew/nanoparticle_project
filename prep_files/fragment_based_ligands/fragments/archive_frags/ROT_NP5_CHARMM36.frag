# Created by frag_builder.py 
# INPUT PDB File: rot_np5_ini.pdb 
# INPUT ITP File: rot_np5.itp 
# Note: All comments with hashtag is not read
# Atom ranges: (1, 85)

# Defining number of atoms and bonds 
NUM_ATOMS 84 
NUM_BONDS 83 

# Inversion flag: -1 inverts y coordinates of next fragment; if 1 -- do not invert
INVERT_Y 1

# Atom types - defining each atom type in fragment
[ ATOMTYPES ]
# atom number, type, name, charge, mass 
1    SG311  S  -0.082    32.060    
2    CG321  C  -0.099    12.011    
3    HGA2   H  0.090     1.008     
4    HGA2   H  0.090     1.008     
5    CG321  C  -0.179    12.011    
6    HGA2   H  0.090     1.008     
7    HGA2   H  0.090     1.008     
8    CG321  C  -0.180    12.011    
9    HGA2   H  0.090     1.008     
10   HGA2   H  0.090     1.008     
11   CG321  C  -0.180    12.011    
12   HGA2   H  0.090     1.008     
13   HGA2   H  0.090     1.008     
14   CG321  C  -0.180    12.011    
15   HGA2   H  0.090     1.008     
16   HGA2   H  0.090     1.008     
17   CG321  C  -0.180    12.011    
18   HGA2   H  0.090     1.008     
19   HGA2   H  0.090     1.008     
20   CG321  C  -0.180    12.011    
21   HGA2   H  0.090     1.008     
22   HGA2   H  0.090     1.008     
23   CG321  C  -0.180    12.011    
24   HGA2   H  0.090     1.008     
25   HGA2   H  0.090     1.008     
26   CG321  C  -0.180    12.011    
27   HGA2   H  0.090     1.008     
28   HGA2   H  0.090     1.008     
29   CG321  C  -0.181    12.011    
30   HGA2   H  0.090     1.008     
31   HGA2   H  0.090     1.008     
32   CG321  C  -0.010    12.011    
33   HGA2   H  0.090     1.008     
34   HGA2   H  0.090     1.008     
35   OG301  O  -0.338    15.999    
36   CG321  C  -0.011    12.011    
37   HGA2   H  0.090     1.008     
38   HGA2   H  0.090     1.008     
39   CG321  C  -0.011    12.011    
40   HGA2   H  0.090     1.008     
41   HGA2   H  0.090     1.008     
42   OG301  O  -0.338    15.999    
43   CG321  C  -0.011    12.011    
44   HGA2   H  0.090     1.008     
45   HGA2   H  0.090     1.008     
46   CG321  C  -0.011    12.011    
47   HGA2   H  0.090     1.008     
48   HGA2   H  0.090     1.008     
49   OG301  O  -0.338    15.999    
50   CG321  C  -0.011    12.011    
51   HGA2   H  0.090     1.008     
52   HGA2   H  0.090     1.008     
53   CG321  C  -0.011    12.011    
54   HGA2   H  0.090     1.008     
55   HGA2   H  0.090     1.008     
56   OG301  O  -0.347    15.999    
57   CG321  C  -0.008    12.011    
58   HGA2   H  0.090     1.008     
59   HGA2   H  0.090     1.008     
60   CG324  C  0.205     12.011    
61   HGA2   H  0.090     1.008     
62   HGA2   H  0.090     1.008     
63   NG3P2  N  -0.411    14.007    
64   CG324  C  0.197     12.011    
65   HGA2   H  0.090     1.008     
66   HGA2   H  0.090     1.008     
67   CG321  C  -0.183    12.011    
68   HGA2   H  0.090     1.008     
69   HGA2   H  0.090     1.008     
70   CG321  C  -0.181    12.011    
71   HGA2   H  0.090     1.008     
72   HGA2   H  0.090     1.008     
73   HGP2   H  0.330     1.008     
74   HGP2   H  0.330     1.008     
75   CG321  C  -0.181    12.011    
76   HGA2   H  0.090     1.008     
77   HGA2   H  0.090     1.008     
78   CG321  C  -0.180    12.011    
79   HGA2   H  0.090     1.008     
80   HGA2   H  0.090     1.008     
81   CG331  C  -0.270    12.011    
82   HGA3   H  0.090     1.008     
83   HGA3   H  0.090     1.008     
84   HGA3   H  0.090     1.008     

# Geom - Defining each atom xyz coordinates 
[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. 
1    0.000     0.000     0.000     
2    -0.057    -0.087    0.149     
3    -0.017    -0.189    0.146     
4    -0.166    -0.093    0.146     
5    -0.009    -0.015    0.274     
6    0.100     -0.008    0.275     
7    -0.048    0.088     0.275     
8    -0.057    -0.087    0.401     
9    -0.018    -0.190    0.401     
10   -0.166    -0.094    0.401     
11   -0.010    -0.015    0.527     
12   0.100     -0.008    0.527     
13   -0.048    0.088     0.527     
14   -0.057    -0.087    0.653     
15   -0.018    -0.190    0.653     
16   -0.166    -0.094    0.653     
17   -0.010    -0.015    0.780     
18   0.100     -0.009    0.780     
19   -0.048    0.088     0.780     
20   -0.057    -0.088    0.906     
21   -0.018    -0.190    0.906     
22   -0.166    -0.094    0.906     
23   -0.010    -0.015    1.032     
24   0.100     -0.009    1.032     
25   -0.048    0.088     1.032     
26   -0.057    -0.087    1.159     
27   -0.018    -0.190    1.159     
28   -0.166    -0.094    1.159     
29   -0.010    -0.015    1.285     
30   0.100     -0.007    1.285     
31   -0.047    0.088     1.285     
32   -0.056    -0.086    1.411     
33   -0.016    -0.188    1.414     
34   -0.165    -0.091    1.414     
35   -0.008    -0.012    1.524     
36   -0.047    -0.073    1.647     
37   -0.008    -0.175    1.653     
38   -0.157    -0.079    1.653     
39   0.006     0.010     1.763     
40   0.116     0.016     1.757     
41   -0.033    0.112     1.757     
42   -0.033    -0.050    1.887     
43   0.016     0.025     1.998     
44   0.125     0.031     1.995     
45   -0.023    0.127     1.995     
46   -0.028    -0.043    2.127     
47   0.011     -0.146    2.130     
48   -0.138    -0.049    2.130     
49   0.020     0.031     2.239     
50   -0.019    -0.029    2.362     
51   0.020     -0.131    2.369     
52   -0.128    -0.035    2.369     
53   0.036     0.055     2.478     
54   0.145     0.061     2.471     
55   -0.004    0.157     2.471     
56   -0.002    -0.003    2.603     
57   0.047     0.072     2.713     
58   0.156     0.077     2.709     
59   0.006     0.174     2.709     
60   0.002     0.003     2.842     
61   0.042     -0.099    2.847     
62   -0.108    -0.002    2.847     
63   0.050     0.077     2.963     
64   0.009     0.014     3.092     
65   0.050     -0.087    3.093     
66   -0.100    0.010     3.093     
67   0.064     0.098     3.207     
68   0.173     0.105     3.200     
69   0.025     0.201     3.200     
70   0.025     0.038     3.342     
71   0.064     -0.064    3.349     
72   -0.084    0.032     3.349     
73   0.153     0.084     2.959     
74   0.014     0.174     2.959     
75   0.079     0.122     3.458     
76   0.188     0.128     3.451     
77   0.040     0.224     3.451     
78   0.040     0.062     3.593     
79   0.079     -0.040    3.601     
80   -0.069    0.056     3.601     
81   0.094     0.145     3.708     
82   0.203     0.150     3.705     
83   0.065     0.100     3.804     
84   0.055     0.247     3.705     
# There is no next atom, but writing so ligand builder works
N    0.000     0.000     0.000     

# Bonds - Defining each bond 
[ BONDS ]
1  2  
2  3  
2  4  
2  5  
5  8  
5  6  
5  7  
8  9  
8  10 
8  11 
11 12 
11 13 
11 14 
14 16 
14 17 
14 15 
17 18 
17 19 
17 20 
20 21 
20 22 
20 23 
23 24 
23 25 
23 26 
26 27 
26 28 
26 29 
29 32 
29 30 
29 31 
32 33 
32 34 
32 35 
35 36 
36 37 
36 38 
36 39 
39 40 
39 41 
39 42 
42 43 
43 44 
43 45 
43 46 
46 48 
46 49 
46 47 
49 50 
50 51 
50 52 
50 53 
53 56 
53 54 
53 55 
56 57 
57 58 
57 59 
57 60 
60 61 
60 62 
60 63 
63 64 
63 73 
63 74 
64 65 
64 66 
64 67 
67 68 
67 69 
67 70 
70 72 
70 75 
70 71 
75 78 
75 76 
75 77 
78 80 
78 81 
78 79 
81 82 
81 83 
81 84 

