# CH3 group (for alkane chain)
#     H
#     |
#  -  C  - H
#     |
#     H

# Define atom number
NUM_ATOMS 4
NUM_BONDS 4

# Flag - if 1, invert y coordinates of next fragment; if 1, does not invert
INVERT_Y 1

# atom types to define for each, along with charge, etc.
[ ATOMTYPES ]
# atom number, type, name, charge, mass
1 opls_135   C   -0.222    12.011
2 opls_140   H    0.074     1.008
3 opls_140   H    0.074     1.008
4 opls_140   H    0.074     1.008

[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. Hard coded.
1   0.0      0.0     0.0
2   0.088    0.064   0.0
3  -0.088    0.064   0.0
4   0.0     -0.064   0.088
# include N out of necessity but will not be used
N   0.0      0.0     0.0

# math for 2 hyd: x = +-sin(0.5*HCH angle)*C_H bond = 0.088 nm
#                 y = + cos(0.5*HCH angle)*C_H bond = 0.064 nm
#  HCH angle = 107.8 degrees (1.8815 rad), C_H bond = 0.109 nm

# For last hydrogen = tetrahedral geometry
# math for next: x = 0.0
#                y = -cos(0.5*HCH angle)*C_H bond = -0.064 nm
#                z =  sin(0.5*HCH angle)*C_H bond =  0.088 nm


# 0 = previous fragment atom; N = next fragment atom
# No next fragment atom for this one by definition
[ BONDS ]
1
1 2
1 3
1 4
