# Created by frag_builder.py 
# INPUT PDB File: rot_sn_ini.pdb 
# INPUT ITP File: rot_sn.itp 
# Note: All comments with hashtag is not read
# Atom ranges: (1, 95)

# Defining number of atoms and bonds 
NUM_ATOMS 94 
NUM_BONDS 93 

# Inversion flag: -1 inverts y coordinates of next fragment; if 1 -- do not invert
INVERT_Y 1

# Atom types - defining each atom type in fragment
[ ATOMTYPES ]
# atom number, type, name, charge, mass 
1    SG311  S  -0.082    32.060    
2    CG321  C  -0.099    12.011    
3    HGA2   H  0.090     1.008     
4    HGA2   H  0.090     1.008     
5    CG321  C  -0.179    12.011    
6    HGA2   H  0.090     1.008     
7    HGA2   H  0.090     1.008     
8    CG321  C  -0.180    12.011    
9    HGA2   H  0.090     1.008     
10   HGA2   H  0.090     1.008     
11   CG321  C  -0.180    12.011    
12   HGA2   H  0.090     1.008     
13   HGA2   H  0.090     1.008     
14   CG321  C  -0.180    12.011    
15   HGA2   H  0.090     1.008     
16   HGA2   H  0.090     1.008     
17   CG321  C  -0.180    12.011    
18   HGA2   H  0.090     1.008     
19   HGA2   H  0.090     1.008     
20   CG321  C  -0.180    12.011    
21   HGA2   H  0.090     1.008     
22   HGA2   H  0.090     1.008     
23   CG321  C  -0.180    12.011    
24   HGA2   H  0.090     1.008     
25   HGA2   H  0.090     1.008     
26   CG321  C  -0.180    12.011    
27   HGA2   H  0.090     1.008     
28   HGA2   H  0.090     1.008     
29   CG321  C  -0.181    12.011    
30   HGA2   H  0.090     1.008     
31   HGA2   H  0.090     1.008     
32   CG321  C  -0.010    12.011    
33   HGA2   H  0.090     1.008     
34   HGA2   H  0.090     1.008     
35   OG301  O  -0.338    15.999    
36   CG321  C  -0.011    12.011    
37   HGA2   H  0.090     1.008     
38   HGA2   H  0.090     1.008     
39   CG321  C  -0.011    12.011    
40   HGA2   H  0.090     1.008     
41   HGA2   H  0.090     1.008     
42   OG301  O  -0.338    15.999    
43   CG321  C  -0.011    12.011    
44   HGA2   H  0.090     1.008     
45   HGA2   H  0.090     1.008     
46   CG321  C  -0.011    12.011    
47   HGA2   H  0.090     1.008     
48   HGA2   H  0.090     1.008     
49   OG301  O  -0.338    15.999    
50   CG321  C  -0.011    12.011    
51   HGA2   H  0.090     1.008     
52   HGA2   H  0.090     1.008     
53   CG321  C  -0.011    12.011    
54   HGA2   H  0.090     1.008     
55   HGA2   H  0.090     1.008     
56   OG301  O  -0.338    15.999    
57   CG321  C  -0.011    12.011    
58   HGA2   H  0.090     1.008     
59   HGA2   H  0.090     1.008     
60   CG321  C  -0.011    12.011    
61   HGA2   H  0.090     1.008     
62   HGA2   H  0.090     1.008     
63   OG301  O  -0.331    15.999    
64   CG321  C  0.099     12.011    
65   HGA2   H  0.090     1.008     
66   HGA2   H  0.090     1.008     
67   CG2D1O C  0.464     12.011    
68   OG312  O  -0.967    15.999    
69   NG2D1  N  -0.521    14.007    
70   SG3O2  S  0.411     32.060    
71   OG2P1  O  -0.377    15.999    
72   OG2P1  O  -0.377    15.999    
73   CG321  C  0.070     12.011    
74   HGA2   H  0.090     1.008     
75   HGA2   H  0.090     1.008     
76   CG321  C  -0.180    12.011    
77   HGA2   H  0.090     1.008     
78   HGA2   H  0.090     1.008     
79   CG324  C  -0.106    12.011    
80   HGP5   H  0.250     1.008     
81   HGP5   H  0.250     1.008     
82   NG3P0  N  -0.597    14.007    
83   CG334  C  -0.349    12.011    
84   HGP5   H  0.250     1.008     
85   HGP5   H  0.250     1.008     
86   HGP5   H  0.250     1.008     
87   CG334  C  -0.349    12.011    
88   HGP5   H  0.250     1.008     
89   HGP5   H  0.250     1.008     
90   HGP5   H  0.250     1.008     
91   CG334  C  -0.349    12.011    
92   HGP5   H  0.250     1.008     
93   HGP5   H  0.250     1.008     
94   HGP5   H  0.250     1.008     

# Geom - Defining each atom xyz coordinates 
[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. 
1    0.000     0.000     0.000     
2    -0.050    -0.087    0.152     
3    -0.004    -0.186    0.154     
4    -0.159    -0.097    0.154     
5    -0.004    -0.007    0.274     
6    0.105     0.003     0.272     
7    -0.050    0.092     0.272     
8    -0.046    -0.080    0.402     
9    -0.000    -0.180    0.404     
10   -0.155    -0.090    0.404     
11   -0.000    -0.000    0.524     
12   0.109     0.010     0.522     
13   -0.046    0.099     0.522     
14   -0.042    -0.074    0.651     
15   0.003     -0.173    0.653     
16   -0.152    -0.084    0.653     
17   0.004     0.006     0.774     
18   0.113     0.016     0.772     
19   -0.042    0.106     0.772     
20   -0.039    -0.067    0.901     
21   0.007     -0.166    0.903     
22   -0.148    -0.077    0.903     
23   0.008     0.013     1.024     
24   0.117     0.023     1.022     
25   -0.038    0.113     1.022     
26   -0.035    -0.060    1.151     
27   0.011     -0.160    1.153     
28   -0.144    -0.070    1.153     
29   0.011     0.020     1.274     
30   0.121     0.030     1.271     
31   -0.034    0.119     1.271     
32   -0.031    -0.053    1.401     
33   0.015     -0.153    1.403     
34   -0.140    -0.064    1.403     
35   0.012     0.020     1.514     
36   -0.032    -0.055    1.625     
37   0.014     -0.154    1.622     
38   -0.141    -0.065    1.622     
39   0.009     0.016     1.754     
40   0.119     0.026     1.757     
41   -0.036    0.116     1.757     
42   -0.034    -0.059    1.865     
43   0.009     0.015     1.978     
44   0.118     0.025     1.976     
45   -0.037    0.114     1.976     
46   -0.034    -0.058    2.105     
47   0.012     -0.158    2.107     
48   -0.143    -0.068    2.107     
49   0.009     0.015     2.218     
50   -0.035    -0.060    2.329     
51   0.011     -0.159    2.326     
52   -0.144    -0.070    2.326     
53   0.007     0.011     2.458     
54   0.116     0.021     2.461     
55   -0.039    0.111     2.461     
56   -0.037    -0.064    2.569     
57   0.006     0.010     2.682     
58   0.115     0.020     2.680     
59   -0.040    0.109     2.680     
60   -0.037    -0.063    2.809     
61   0.009     -0.163    2.811     
62   -0.146    -0.073    2.811     
63   0.006     0.010     2.922     
64   -0.037    -0.065    3.033     
65   0.008     -0.164    3.030     
66   -0.147    -0.075    3.030     
67   0.004     0.006     3.162     
68   0.065     0.113     3.157     
69   -0.030    -0.052    3.293     
70   0.031     0.054     3.424     
71   0.175     0.067     3.413     
72   -0.030    0.185     3.413     
73   -0.011    -0.018    3.586     
74   0.036     -0.117    3.594     
75   -0.119    -0.028    3.594     
76   0.042     0.072     3.697     
77   0.151     0.082     3.689     
78   -0.004    0.171     3.689     
79   0.007     0.011     3.833     
80   0.053     -0.088    3.842     
81   -0.102    0.002     3.842     
82   0.059     0.102     3.945     
83   0.211     0.115     3.933     
84   0.257     0.016     3.942     
85   0.248     0.180     4.013     
86   0.236     0.159     3.836     
87   -0.006    0.240     3.933     
88   0.020     0.284     3.836     
89   0.032     0.305     4.013     
90   -0.115    0.231     3.942     
91   0.024     0.041     4.081     
92   0.070     -0.058    4.090     
93   -0.085    0.031     4.090     
94   0.061     0.106     4.161     
# There is no next atom, but writing so ligand builder works
N    0.000     0.000     0.000     

# Bonds - Defining each bond 
[ BONDS ]
1  2  
2  3  
2  4  
2  5  
5  8  
5  6  
5  7  
8  9  
8  10 
8  11 
11 12 
11 13 
11 14 
14 16 
14 17 
14 15 
17 18 
17 19 
17 20 
20 21 
20 22 
20 23 
23 24 
23 25 
23 26 
26 27 
26 28 
26 29 
29 32 
29 30 
29 31 
32 33 
32 34 
32 35 
35 36 
36 37 
36 38 
36 39 
39 40 
39 41 
39 42 
42 43 
43 44 
43 45 
43 46 
46 48 
46 49 
46 47 
49 50 
50 51 
50 52 
50 53 
53 56 
53 54 
53 55 
56 57 
57 58 
57 59 
57 60 
60 61 
60 62 
60 63 
63 64 
64 65 
64 66 
64 67 
67 68 
67 69 
69 70 
70 72 
70 73 
70 71 
73 74 
73 75 
73 76 
76 77 
76 78 
76 79 
79 80 
79 81 
79 82 
82 91 
82 87 
82 83 
83 84 
83 85 
83 86 
87 88 
87 89 
87 90 
91 92 
91 93 
91 94 

