# FORCE FIELD PARAMETERS BASED ON THE FOLLOWING PAPERS:
# Automating paper: Djebaili, T., Richardi, J., Abel, S. & Marchi, M. Atomistic simulations of the surface coverage of large gold nanocrystals. J. Phys. Chem. C 117, 17791–17800 (2013).
# United atom force field params: Dubbeldam, D. et al. United atom force field for alkanes in nanoporous materials. J. Phys. Chem. B 108, 12301–12313 (2004).
# LJ params: Pool, R., Schapotschnikow, P. & Vlugt, T. J. H. Solvent effects in the adsorption of alkyl thiols on gold structures: A molecular simulation study. J. Phys. Chem. C 111, 10201–10212 (2007).

# S-CH2 group (for thiol group)
#        H
#        |
#   S -  C  -
#        |
#        H

# Define atom number
NUM_ATOMS 2
NUM_BONDS 2

# Flag - if 1, invert y coordinates of next fragment; if 1, does not invert
INVERT_Y -1

# atom types to define for each, along with charge, etc.
[ ATOMTYPES ]
# atom number, type, name, charge, mass
1 SH    S   0.000    33.068000
2 CH2    C    0.000    14.027000

[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. Hard coded.
1   0.0      0.0     0.0
2   0.0      0.103   0.151
N   0.0      0.080   0.277

# math for first carbon: x = 0
#                        y =  cos(0.5*CCC angle)*C_S bond =  0.103 nm
#                        z =  sin(0.5*CCC angle)*C_S bond =  0.151 nm
# C-S bond = 0.183 nm

# math for 2 hyd: x = +-sin(0.5*HCH angle)*C_H bond = 0.088 nm
#                 y = + cos(0.5*HCH angle)*C_H bond + first C_y = 0.167 nm
#                 z = first C_z = 0.151 nm
#  HCH angle = 107.8 degrees (1.8815 rad), C_H bond = 0.109 nm

# N = next atom in chain. ASSUME NEXT ATOM IS C
# math for next: x = 0.0
#                y = -cos(0.5*CCC angle)*C_C bond + prev C_Y = 0.08 nm
#                z =  sin(0.5*CCC angle)*C_C bond + prev C_Z = 0.277 nm
# CCC angle = 111.0 degrees (CHECK THIS?), C-C bbond = 0.153 nm

# P = previous fragment atom; N = next fragment atom
# No previous fragment atom for this one by definition
[ BONDS ]
1 2
2 

