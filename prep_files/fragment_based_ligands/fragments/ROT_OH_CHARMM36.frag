# Created by frag_builder.py 
# INPUT PDB File: rot_oh_ini.pdb 
# INPUT ITP File: rot_oh.itp 
# Note: All comments with hashtag is not read
# Atom ranges: (1, 65)

# Defining number of atoms and bonds 
NUM_ATOMS 64 
NUM_BONDS 63 

# Inversion flag: -1 inverts y coordinates of next fragment; if 1 -- do not invert
INVERT_Y 1

# Atom types - defining each atom type in fragment
[ ATOMTYPES ]
# atom number, type, name, charge, mass 
1    SG311  S  -0.082    32.060    
2    CG321  C  -0.099    12.011    
3    HGA2   H  0.090     1.008     
4    HGA2   H  0.090     1.008     
5    CG321  C  -0.179    12.011    
6    HGA2   H  0.090     1.008     
7    HGA2   H  0.090     1.008     
8    CG321  C  -0.180    12.011    
9    HGA2   H  0.090     1.008     
10   HGA2   H  0.090     1.008     
11   CG321  C  -0.180    12.011    
12   HGA2   H  0.090     1.008     
13   HGA2   H  0.090     1.008     
14   CG321  C  -0.180    12.011    
15   HGA2   H  0.090     1.008     
16   HGA2   H  0.090     1.008     
17   CG321  C  -0.180    12.011    
18   HGA2   H  0.090     1.008     
19   HGA2   H  0.090     1.008     
20   CG321  C  -0.180    12.011    
21   HGA2   H  0.090     1.008     
22   HGA2   H  0.090     1.008     
23   CG321  C  -0.180    12.011    
24   HGA2   H  0.090     1.008     
25   HGA2   H  0.090     1.008     
26   CG321  C  -0.180    12.011    
27   HGA2   H  0.090     1.008     
28   HGA2   H  0.090     1.008     
29   CG321  C  -0.181    12.011    
30   HGA2   H  0.090     1.008     
31   HGA2   H  0.090     1.008     
32   CG321  C  -0.010    12.011    
33   HGA2   H  0.090     1.008     
34   HGA2   H  0.090     1.008     
35   OG301  O  -0.338    15.999    
36   CG321  C  -0.011    12.011    
37   HGA2   H  0.090     1.008     
38   HGA2   H  0.090     1.008     
39   CG321  C  -0.011    12.011    
40   HGA2   H  0.090     1.008     
41   HGA2   H  0.090     1.008     
42   OG301  O  -0.338    15.999    
43   CG321  C  -0.011    12.011    
44   HGA2   H  0.090     1.008     
45   HGA2   H  0.090     1.008     
46   CG321  C  -0.011    12.011    
47   HGA2   H  0.090     1.008     
48   HGA2   H  0.090     1.008     
49   OG301  O  -0.338    15.999    
50   CG321  C  -0.011    12.011    
51   HGA2   H  0.090     1.008     
52   HGA2   H  0.090     1.008     
53   CG321  C  -0.011    12.011    
54   HGA2   H  0.090     1.008     
55   HGA2   H  0.090     1.008     
56   OG301  O  -0.338    15.999    
57   CG321  C  -0.011    12.011    
58   HGA2   H  0.090     1.008     
59   HGA2   H  0.090     1.008     
60   CG321  C  0.050     12.011    
61   HGA2   H  0.090     1.008     
62   HGA2   H  0.090     1.008     
63   OG311  O  -0.650    15.999    
64   HGP1   H  0.420     1.008     

# Geom - Defining each atom xyz coordinates 
[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. 
1    0.000     0.000     0.000     
2    -0.050    -0.086    0.152     
3    -0.006    -0.186    0.151     
4    -0.159    -0.097    0.151     
5    -0.005    -0.008    0.275     
6    0.104     0.003     0.274     
7    -0.049    0.092     0.274     
8    -0.047    -0.080    0.403     
9    -0.003    -0.181    0.405     
10   -0.156    -0.092    0.405     
11   -0.002    -0.003    0.528     
12   0.107     0.009     0.526     
13   -0.045    0.098     0.526     
14   -0.044    -0.075    0.656     
15   0.000     -0.176    0.658     
16   -0.153    -0.087    0.658     
17   0.001     0.002     0.780     
18   0.110     0.014     0.779     
19   -0.042    0.103     0.779     
20   -0.041    -0.070    0.909     
21   0.003     -0.170    0.910     
22   -0.150    -0.082    0.910     
23   0.004     0.007     1.033     
24   0.113     0.019     1.031     
25   -0.039    0.108     1.031     
26   -0.038    -0.065    1.161     
27   0.006     -0.165    1.163     
28   -0.147    -0.077    1.163     
29   0.007     0.013     1.285     
30   0.116     0.026     1.284     
31   -0.035    0.114     1.284     
32   -0.034    -0.058    1.413     
33   0.011     -0.158    1.418     
34   -0.143    -0.068    1.418     
35   0.011     0.019     1.524     
36   -0.024    -0.041    1.649     
37   0.021     -0.141    1.656     
38   -0.133    -0.052    1.656     
39   0.027     0.046     1.763     
40   0.136     0.057     1.756     
41   -0.017    0.146     1.756     
42   -0.008    -0.014    1.888     
43   0.037     0.064     1.998     
44   0.146     0.076     1.994     
45   -0.007    0.165     1.994     
46   -0.002    -0.004    2.128     
47   0.042     -0.104    2.133     
48   -0.111    -0.015    2.133     
49   0.043     0.074     2.239     
50   0.008     0.014     2.363     
51   0.053     -0.086    2.370     
52   -0.101    0.003     2.370     
53   0.059     0.101     2.478     
54   0.168     0.112     2.471     
55   0.014     0.201     2.471     
56   0.024     0.041     2.602     
57   0.069     0.119     2.713     
58   0.178     0.130     2.708     
59   0.025     0.219     2.708     
60   0.029     0.051     2.843     
61   0.074     -0.049    2.848     
62   -0.079    0.039     2.848     
63   0.074     0.127     2.954     
64   0.046     0.079     3.034     
# There is no next atom, but writing so ligand builder works
N    0.000     0.000     0.000     

# Bonds - Defining each bond 
[ BONDS ]
1  2  
2  3  
2  4  
2  5  
5  8  
5  6  
5  7  
8  9  
8  10 
8  11 
11 12 
11 13 
11 14 
14 16 
14 17 
14 15 
17 18 
17 19 
17 20 
20 21 
20 22 
20 23 
23 24 
23 25 
23 26 
26 27 
26 28 
26 29 
29 32 
29 30 
29 31 
32 33 
32 34 
32 35 
35 36 
36 37 
36 38 
36 39 
39 40 
39 41 
39 42 
42 43 
43 44 
43 45 
43 46 
46 48 
46 49 
46 47 
49 50 
50 51 
50 52 
50 53 
53 56 
53 54 
53 55 
56 57 
57 58 
57 59 
57 60 
60 61 
60 62 
60 63 
63 64 

