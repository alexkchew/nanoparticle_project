# Created by frag_builder.py 
# INPUT PDB File: /home/akchew/scratch/LigandBuilder/Prep_System/prep_ligand/minimized_structures/C11COOm/C11COOm.pdb 
# INPUT ITP File: /home/akchew/scratch/LigandBuilder/Prep_System/prep_ligand/minimized_structures/C11COOm/C11COOm.itp 
# Note: All comments with hashtag is not read
# Atom ranges: (1, 38)

# Defining number of atoms and bonds 
NUM_ATOMS 37 
NUM_BONDS 36 

# Inversion flag: -1 inverts y coordinates of next fragment; if 1 -- do not invert
INVERT_Y 1

# Atom types - defining each atom type in fragment
[ ATOMTYPES ]
# atom number, type, name, charge, mass 
1    SG311  S  -0.082    32.060    
2    CG321  C  -0.099    12.011    
3    HGA2   H  0.090     1.008     
4    HGA2   H  0.090     1.008     
5    CG321  C  -0.179    12.011    
6    HGA2   H  0.090     1.008     
7    HGA2   H  0.090     1.008     
8    CG321  C  -0.180    12.011    
9    HGA2   H  0.090     1.008     
10   HGA2   H  0.090     1.008     
11   CG321  C  -0.180    12.011    
12   HGA2   H  0.090     1.008     
13   HGA2   H  0.090     1.008     
14   CG321  C  -0.180    12.011    
15   HGA2   H  0.090     1.008     
16   HGA2   H  0.090     1.008     
17   CG321  C  -0.180    12.011    
18   HGA2   H  0.090     1.008     
19   HGA2   H  0.090     1.008     
20   CG321  C  -0.180    12.011    
21   HGA2   H  0.090     1.008     
22   HGA2   H  0.090     1.008     
23   CG321  C  -0.180    12.011    
24   HGA2   H  0.090     1.008     
25   HGA2   H  0.090     1.008     
26   CG321  C  -0.180    12.011    
27   HGA2   H  0.090     1.008     
28   HGA2   H  0.090     1.008     
29   CG321  C  -0.179    12.011    
30   HGA2   H  0.090     1.008     
31   HGA2   H  0.090     1.008     
32   CG321  C  -0.283    12.011    
33   HGA2   H  0.090     1.008     
34   HGA2   H  0.090     1.008     
35   CG2O3  C  0.622     12.011    
36   OG2D2  O  -0.760    15.999    
37   OG2D2  O  -0.760    15.999    

# Geom - Defining each atom xyz coordinates 
[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. 
1    0.000     0.000     0.000     
2    -0.047    -0.078    0.158     
3    -0.003    -0.180    0.164     
4    -0.157    -0.088    0.164     
5    0.002     0.003     0.278     
6    0.113     0.014     0.275     
7    -0.041    0.106     0.275     
8    -0.037    -0.062    0.411     
9    0.007     -0.164    0.417     
10   -0.148    -0.072    0.417     
11   0.012     0.019     0.531     
12   0.122     0.030     0.527     
13   -0.032    0.123     0.526     
14   -0.028    -0.046    0.662     
15   0.015     -0.148    0.668     
16   -0.138    -0.057    0.667     
17   0.021     0.035     0.783     
18   0.132     0.045     0.779     
19   -0.022    0.137     0.779     
20   -0.019    -0.031    0.915     
21   0.025     -0.134    0.919     
22   -0.129    -0.042    0.919     
23   0.029     0.050     1.035     
24   0.140     0.060     1.031     
25   -0.014    0.152     1.031     
26   -0.010    -0.017    1.166     
27   0.034     -0.119    1.172     
28   -0.121    -0.027    1.172     
29   0.038     0.064     1.287     
30   0.149     0.074     1.283     
31   -0.006    0.166     1.283     
32   -0.002    -0.003    1.418     
33   0.043     -0.104    1.427     
34   -0.112    -0.012    1.426     
35   0.049     0.082     1.535     
36   0.165     0.055     1.576     
37   -0.030    0.171     1.576     
# There is no next atom, but writing so ligand builder works
N    0.000     0.000     0.000     

# Bonds - Defining each bond 
[ BONDS ]
1  2  
2  3  
2  4  
2  5  
5  8  
5  6  
5  7  
8  9  
8  10 
8  11 
11 12 
11 13 
11 14 
14 16 
14 17 
14 15 
17 18 
17 19 
17 20 
20 21 
20 22 
20 23 
23 24 
23 25 
23 26 
26 27 
26 28 
26 29 
29 32 
29 30 
29 31 
32 33 
32 34 
32 35 
35 36 
35 37 

# Dihedrals - Defining custom dihedral angles and functional groups 
[ DIHEDRALS ]
# ai, aj, ak, al, func 
35   36   37   32   2    

