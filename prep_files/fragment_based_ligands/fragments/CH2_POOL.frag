# FORCE FIELD PARAMETERS BASED ON THE FOLLOWING PAPERS:
# Automating paper: Djebaili, T., Richardi, J., Abel, S. & Marchi, M. Atomistic simulations of the surface coverage of large gold nanocrystals. J. Phys. Chem. C 117, 17791–17800 (2013).
# United atom force field params: Dubbeldam, D. et al. United atom force field for alkanes in nanoporous materials. J. Phys. Chem. B 108, 12301–12313 (2004).
# LJ params: Pool, R., Schapotschnikow, P. & Vlugt, T. J. H. Solvent effects in the adsorption of alkyl thiols on gold structures: A molecular simulation study. J. Phys. Chem. C 111, 10201–10212 (2007).

# CH2 group (for alkane chain) 
#     H
#     |
#  -  C  -
#     |
#     H

# Define number of atoms/bonds
# num bonds used to read in entries
NUM_ATOMS 1
NUM_BONDS 2

# Flag - if 1, invert y coordinates of next fragment; if 1, does not invert
INVERT_Y -1

# atom types to define for each, along with charge, etc.
[ ATOMTYPES ]
# atom number, type, name, charge, mass
1 CH2    C   0.000    14.027000

[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. Hard coded.
1   0.0      0.0     0.0
N   0.0     -0.087   0.126

# math for 2 hyd: x = +-sin(0.5*HCH angle)*C_H bond = 0.088 nm
#                 y = + cos(0.5*HCH angle)*C_H bond = 0.064 nm
#  HCH angle = 107.8 degrees (1.8815 rad), C_H bond = 0.109 nm

# N = next atom in chain. ASSUME NEXT ATOM IS C
# math for next: x = 0.0
#                y = -cos(0.5*CCC angle)*C_C bond = -0.087 nm
#                z =  sin(0.5*CCC angle)*C_C bond =  0.126 nm
# CCC angle = 111.0 degrees (CHECK THIS?), C-C bbond = 0.153 nm

# single entry = bond with previous / next
[ BONDS ]
1
1

