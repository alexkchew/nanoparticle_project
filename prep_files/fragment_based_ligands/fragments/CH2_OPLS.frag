# CH2 group (for alkane chain) 
#     H
#     |
#  -  C  -
#     |
#     H

# Define number of atoms/bonds
# num bonds used to read in entries
NUM_ATOMS 3
NUM_BONDS 4

# Flag - if 1, invert y coordinates of next fragment; if 1, does not invert
INVERT_Y -1

# atom types to define for each, along with charge, etc.
[ ATOMTYPES ]
# atom number, type, name, charge, mass
1 opls_136    C   -0.148    12.011
2 opls_1400   H    0.074     1.008
3 opls_1400   H    0.074     1.008

[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. Hard coded.
1   0.0      0.0     0.0
2   0.088    0.064   0.0
3  -0.088    0.064   0.0
N   0.0     -0.087   0.126

# math for 2 hyd: x = +-sin(0.5*HCH angle)*C_H bond = 0.088 nm
#                 y = + cos(0.5*HCH angle)*C_H bond = 0.064 nm
#  HCH angle = 107.8 degrees (1.8815 rad), C_H bond = 0.109 nm

# N = next atom in chain. ASSUME NEXT ATOM IS C
# math for next: x = 0.0
#                y = -cos(0.5*CCC angle)*C_C bond = -0.087 nm
#                z =  sin(0.5*CCC angle)*C_C bond =  0.126 nm
# CCC angle = 111.0 degrees (CHECK THIS?), C-C bbond = 0.153 nm

# single entry = bond with previous / next
[ BONDS ]
1
1 2
1 3
1
