# Created by frag_builder.py 
# INPUT PDB File: /home/akchew/scratch/LigandBuilder/Prep_System/prep_ligand/minimized_structures/C11OH/C11OH.pdb 
# INPUT ITP File: /home/akchew/scratch/LigandBuilder/Prep_System/prep_ligand/minimized_structures/C11OH/C11OH.itp 
# Note: All comments with hashtag is not read
# Atom ranges: (1, 37)

# Defining number of atoms and bonds 
NUM_ATOMS 36 
NUM_BONDS 35 

# Inversion flag: -1 inverts y coordinates of next fragment; if 1 -- do not invert
INVERT_Y 1

# Atom types - defining each atom type in fragment
[ ATOMTYPES ]
# atom number, type, name, charge, mass 
1    SG311  S  -0.082    32.060    
2    CG321  C  -0.099    12.011    
3    HGA2   H  0.090     1.008     
4    HGA2   H  0.090     1.008     
5    CG321  C  -0.179    12.011    
6    HGA2   H  0.090     1.008     
7    HGA2   H  0.090     1.008     
8    CG321  C  -0.180    12.011    
9    HGA2   H  0.090     1.008     
10   HGA2   H  0.090     1.008     
11   CG321  C  -0.180    12.011    
12   HGA2   H  0.090     1.008     
13   HGA2   H  0.090     1.008     
14   CG321  C  -0.180    12.011    
15   HGA2   H  0.090     1.008     
16   HGA2   H  0.090     1.008     
17   CG321  C  -0.180    12.011    
18   HGA2   H  0.090     1.008     
19   HGA2   H  0.090     1.008     
20   CG321  C  -0.180    12.011    
21   HGA2   H  0.090     1.008     
22   HGA2   H  0.090     1.008     
23   CG321  C  -0.180    12.011    
24   HGA2   H  0.090     1.008     
25   HGA2   H  0.090     1.008     
26   CG321  C  -0.180    12.011    
27   HGA2   H  0.090     1.008     
28   HGA2   H  0.090     1.008     
29   CG321  C  -0.181    12.011    
30   HGA2   H  0.090     1.008     
31   HGA2   H  0.090     1.008     
32   CG321  C  0.051     12.011    
33   HGA2   H  0.090     1.008     
34   HGA2   H  0.090     1.008     
35   OG311  O  -0.650    15.999    
36   HGP1   H  0.420     1.008     

# Geom - Defining each atom xyz coordinates 
[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. 
1    0.000     0.000     0.000     
2    -0.045    -0.079    0.157     
3    0.000     -0.180    0.163     
4    -0.156    -0.092    0.162     
5    0.001     0.004     0.277     
6    0.111     0.016     0.273     
7    -0.045    0.105     0.273     
8    -0.036    -0.062    0.410     
9    0.009     -0.163    0.415     
10   -0.147    -0.075    0.415     
11   0.010     0.021     0.530     
12   0.121     0.034     0.525     
13   -0.035    0.122     0.524     
14   -0.028    -0.045    0.662     
15   0.018     -0.146    0.667     
16   -0.138    -0.057    0.666     
17   0.019     0.039     0.781     
18   0.130     0.052     0.776     
19   -0.026    0.140     0.776     
20   -0.018    -0.027    0.914     
21   0.027     -0.128    0.920     
22   -0.128    -0.040    0.919     
23   0.029     0.057     1.033     
24   0.139     0.069     1.028     
25   -0.017    0.157     1.028     
26   -0.008    -0.009    1.166     
27   0.036     -0.111    1.171     
28   -0.119    -0.023    1.171     
29   0.038     0.074     1.286     
30   0.148     0.087     1.280     
31   -0.008    0.176     1.280     
32   -0.001    0.007     1.418     
33   0.046     -0.093    1.424     
34   -0.110    -0.004    1.424     
35   0.044     0.087     1.527     
36   0.140     0.089     1.520     
# There is no next atom, but writing so ligand builder works
N    0.000     0.000     0.000     

# Bonds - Defining each bond 
[ BONDS ]
1  2  
2  3  
2  4  
2  5  
5  8  
5  6  
5  7  
8  9  
8  10 
8  11 
11 12 
11 13 
11 14 
14 16 
14 17 
14 15 
17 18 
17 19 
17 20 
20 21 
20 22 
20 23 
23 24 
23 25 
23 26 
26 27 
26 28 
26 29 
29 32 
29 30 
29 31 
32 33 
32 34 
32 35 
35 36 

