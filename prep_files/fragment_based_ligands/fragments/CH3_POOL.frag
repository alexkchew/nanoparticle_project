# FORCE FIELD PARAMETERS BASED ON THE FOLLOWING PAPERS:
# Automating paper: Djebaili, T., Richardi, J., Abel, S. & Marchi, M. Atomistic simulations of the surface coverage of large gold nanocrystals. J. Phys. Chem. C 117, 17791–17800 (2013).
# United atom force field params: Dubbeldam, D. et al. United atom force field for alkanes in nanoporous materials. J. Phys. Chem. B 108, 12301–12313 (2004).
# LJ params: Pool, R., Schapotschnikow, P. & Vlugt, T. J. H. Solvent effects in the adsorption of alkyl thiols on gold structures: A molecular simulation study. J. Phys. Chem. C 111, 10201–10212 (2007).

# CH3 group (for alkane chain)
#     H
#     |
#  -  C  - H
#     |
#     H

# Define atom number
NUM_ATOMS 1
NUM_BONDS 1

# Flag - if 1, invert y coordinates of next fragment; if 1, does not invert
INVERT_Y 1

# atom types to define for each, along with charge, etc.
[ ATOMTYPES ]
# atom number, type, name, charge, mass
1 CH3   C   0.000    15.035000

[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. Hard coded.
1   0.0      0.0     0.0
# include N out of necessity but will not be used
N   0.0      0.0     0.0

# math for 2 hyd: x = +-sin(0.5*HCH angle)*C_H bond = 0.088 nm
#                 y = + cos(0.5*HCH angle)*C_H bond = 0.064 nm
#  HCH angle = 107.8 degrees (1.8815 rad), C_H bond = 0.109 nm

# For last hydrogen = tetrahedral geometry
# math for next: x = 0.0
#                y = -cos(0.5*HCH angle)*C_H bond = -0.064 nm
#                z =  sin(0.5*HCH angle)*C_H bond =  0.088 nm


# 0 = previous fragment atom; N = next fragment atom
# No next fragment atom for this one by definition
[ BONDS ]
1

