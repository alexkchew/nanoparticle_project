# ethylguanidinum end group (for alkane chain); protonated
# Based on arginine end group; should double check parameterization

# NOTE: THIS ONE IS COMPLICATED DUE TO NEED FOR IMPROPERS; DO LATER! WORK ON LYSINE FIRST.

#                  H
#                  |
#    H   H   H     N - H
#    |   |   |    //
# -  C - C - N - C +
#    |   |        \
#    H   H         N - H
#                  |
#                  H

# Define atom number
NUM_ATOMS 15
NUM_BONDS 15

# Flag - if 1, invert y coordinates of next fragment; if 1, does not invert
INVERT_Y 1

# atom types to define for each, along with charge, etc.
[ ATOMTYPES ]
# atom number, type, name, charge, mass
1 opls_308    C   -0.050    12.011
2 opls_140    H    0.060     1.008
3 opls_140    H    0.060     1.008
4 opls_307    C    0.190    12.011
5 opls_140    H    0.060     1.008
6 opls_140    H    0.060     1.008
7 opls_303    N   -0.700    14.007
8 opls_304    H    0.440     1.008
9 opls_302    C    0.640    12.011
10 opls_300   N  -0.800    14.007
11 opls_301   H   0.460     1.008
12 opls_301   H   0.460     1.008
13 opls_300   N   -0.800    14.007
14 opls_301   H    0.460     1.008
15 opls_301   H    0.460     1.008

[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. Hard coded.
1   0.0      0.0     0.0
2   0.088    0.064   0.0
3  -0.088    0.064   0.0
4   0.0      0.087   0.126
5   0.088    0.151   0.126
6  -0.088    0.151   0.126
7   0.0      0.0     0.252
# include N out of necessity but will not be used
N   0.0      0.0     0.0

# math for 2 hyd: x = +-sin(0.5*HCH angle)*C_H bond = 0.088 nm
#                 y = + cos(0.5*HCH angle)*C_H bond = 0.064 nm
#  HCH angle = 107.8 degrees (1.8815 rad), C_H bond = 0.109 nm

# For C-N bond
#

# Single line indicated "dangling" bond for previous/next residue
# No next fragment atom for this one by definition
[ BONDS ]
1
1 2
1 3
1 4
4 5
4 6
4 7
7 8
7 9
9 10
9 13
10 11
10 12
13 14
13 15
