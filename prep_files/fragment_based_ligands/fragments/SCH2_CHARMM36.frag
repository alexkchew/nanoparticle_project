# S-CH2 group (for thiol group) -- Taken from ethanethiol
#        H
#        |
#   S -  C  -
#        |
#        H
# Updates:
#   180419-Changed S and C charge from -0.71 to -0.82, and -0.109 to -0.099, respectively. This is based on the itp file for dodecanethiol

# Define atom number
NUM_ATOMS 4
NUM_BONDS 4

# Flag - if 1, invert y coordinates of next fragment; if 1, does not invert
INVERT_Y -1

# atom types to define for each, along with charge, etc.
[ ATOMTYPES ]
# atom number, type, name, charge, mass -- Sulfur adjusted to include hydrogen
1 SG311    S   -0.082     32.060
2 CG321    C    -0.099     12.011
3 HGA2    H    0.090      1.008 
4 HGA2    H    0.090      1.008 

[ GEOMETRY ]
# atomnum, x coord, y coord, z coord. Hard coded.
1   0.0      0.0     0.0
2   0.0      0.103   0.151
2   0.088    0.167   0.151
3  -0.088    0.167   0.151
N   0.0      0.080   0.277

# math for first carbon: x = 0
#                        y =  cos(0.5*CCC angle)*C_S bond =  0.103 nm
#                        z =  sin(0.5*CCC angle)*C_S bond =  0.151 nm
# C-S bond = 0.183 nm

# math for 2 hyd: x = +-sin(0.5*HCH angle)*C_H bond = 0.088 nm
#                 y = + cos(0.5*HCH angle)*C_H bond + first C_y = 0.167 nm
#                 z = first C_z = 0.151 nm
#  HCH angle = 107.8 degrees (1.8815 rad), C_H bond = 0.109 nm

# N = next atom in chain. ASSUME NEXT ATOM IS C
# math for next: x = 0.0
#                y = -cos(0.5*CCC angle)*C_C bond + prev C_Y = 0.08 nm
#                z =  sin(0.5*CCC angle)*C_C bond + prev C_Z = 0.277 nm
# CCC angle = 111.0 degrees (CHECK THIS?), C-C bbond = 0.153 nm

# P = previous fragment atom; N = next fragment atom
# No previous fragment atom for this one by definition
[ BONDS ]
1 2
2 3
2 4
2 

