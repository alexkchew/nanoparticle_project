#!/bin/bash

# convert_database_smiles_to_mol2.sh
# The purpose of this script is to convert a database of smiles into 
# mol2 files. 

# Written by: Alex K. Chew (08/27/2020)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

################################
### DEFINING INPUT VARIABLES ###
################################

## DEFINING PATH TO DATABASE FILE
database_file="logP_exp_data_LIGANDS.csv"
## DEFINING PATH TO DATABASE
path_to_database="${PATH2DATABASE}/${database_file}"

## GETTING NEW PATH
path_to_database="/home/akchew/bin/pythonfiles/modules/gnpdescriptors/database/rotello_gnp_database_LIGANDS.csv"
# logP_cell_uptake_zeta_potential_database_LIGANDS.csv"
# logP_cell_uptake_database_LIGANDS.csv"

## DEFINING OUTPUT LOCATION
path_to_output_folder="${CURRENTWD}/np_descriptor_ligands"

## MAKING DIRECTORY OUTPUT
if [ ! -e "${path_to_output_folder}" ]; then
	mkdir -p "${path_to_output_folder}"
fi


#####################################
### STEP 1: READING DATABASE FILE ###
#####################################

## CHECKING IF EXISTING
stop_if_does_not_exist "${path_to_database}"

## LOOPING
while read -r line; do
	## EXTRACTING INFORMATION FROM EACH LINE
	IFS=',' read -r -a array <<< "${line}"

	## GETTING INFO
	current_ligname="${array[0]}"
	current_lig_resname="${array[1]}"
	current_lig_smiles="${array[2]}"

	## DEFINING MOL2 FILE
	mol2_file="${current_ligname}.mol2"

	## PATH TO MOL2
	path_to_output_mol2="${path_to_output_folder}/${mol2_file}"

	## CHECKING IF FILE EXISTS
	if [ ! -e "${path_to_output_mol2}" ]; then
		echo "Working on: ${mol2_file}"
		## USING OBABEL TO CONVERT TO MOL2
		obabel -:"${current_lig_smiles}" -omol2 -h --gen3d > "${path_to_output_mol2}"

		## CHECKING IF MOL2 EXISTS
		if [ -e "${path_to_output_mol2}" ]; then
			echo "--> Success!"
		else
			echo "--> Error! Missing mol2: ${mol2_file}"
			echo "--> Input: ${array[@]}"
			echo "--> Pausing so you could see!"
			sleep 2
		fi

	else
		echo "${mol2_file} exists! Skipping!"
	fi
	

done <<< "$(tail -n+2 "${path_to_database}")"