#!/bin/bash

# compute_most_likely.sh
# This script computes the most likely bundling configuration given a set of simulation parameters. This code will output the most likely bundls as a pickle and as a .summary file. In addition, you could use this script to generalize in terms of generating the most likely configuration. If you are unsure of whether the calculation is done correctly, you could turn on recalc logical. This will recompute bundling groups. Note that bundling groups use the last heavy atom as a bundling group. 
#
# INPUTS:
#   $1: input_sim_path - input simulation path
#   $2: lig_residue_name - ligand residue name
#   $3: most_likely_index - most likely index that you desire
#   $4: True/False. True if you want to recalculate
#   $5: displacement vector type: avg_heavy_atom
# OUTPUTS:
#   The following 
#   SUMMARY FILE: np_most_likely.summary
#   INDEX FILE OF GOLD WITH LIGAND: gold_with_ligand.ndx

## USAGE
# bash compute_most_likely.sh /home/akchew/scratch/nanoparticle_project/simulations/HYDROPHOBICITY_PROJECT_C11/EAM_300.00_K_2_nmDIAM_C11COOH_CHARMM36jul2017_Trial_1 COO 1

# Written by: Alex K. Chew (10/16/2019)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../../bin/${global_file_name}"

## ENABLING EXTERNAL MODULES
shopt -s extglob
# ^ Allows you to move all except 1

## PRINTING SCRIPT NAME
print_script_name

#########################################
### MOST LIKELY CONFIGURATION DETAILS ###
#########################################

## DEFINING PYTHON CODE
python_most_likely="${NP_MOST_LIKELY_CODE}"

## DEFINING SUMMARY FILE
most_likely_output_summary_file="np_most_likely.summary"

## DEFINING GOLD RESIDUE NAME
gold_residue_name="AUNP"

## DEFINING INDEX FILE
index_file="gold_with_ligand.ndx"

#######################
### INPUT VARIABLES ###
#######################

## DEFINING INPUT SIMULATION PATH
input_sim_path="$1"
# "/home/akchew/scratch/nanoparticle_project/simulations/HYDROPHOBICITY_PROJECT_C11/EAM_300.00_K_2_nmDIAM_C11COO_CHARMM36jul2017_Trial_1"

## DEFINING INPUT LIGAND NAME
lig_residue_name="$2"
# "COM"

## DEFINING DESIRED NP INDEX
most_likely_index="${3-1}"
## DEFINING IF YOU WANT TO RECALC
want_recalc="${4-False}"
# "False"

## DEFINING TYPE OF SIMS
displacement_vector_type="${5-avg_heavy_atom}"

######################
### DEFINING FILES ###
######################

## DISPLAYING MOST LIKELY
most_likely_dir="most_likely-${displacement_vector_type}"

## DEFINING PREFIX
sim_prefix="sam_prod"

## DEFINING SIMULATION FILES
sim_gro_file="${sim_prefix}.gro" # GRO FILE
sim_tpr_file="${sim_prefix}.tpr" # TPR FILE
sim_xtc_file="${sim_prefix}.xtc"

## DEFINING INDEX FILE
index_file="gold_with_ligand.ndx"

#### TRUNCTION DEFINITIONS
truncation_time="10000" # ps truncation
trunc_output_xtc_file="sam_prod_10_ns_whole.xtc"

#### CENTERING DETAILS
extract_sim_xtc_file="sam_prod_10_ns_whole_center.xtc" # 

#### ROT TRANS DETAILS
analysis_sim_xtc_file="sam_prod_10_ns_whole_rot_trans.xtc"

## INDEX FILE WITH GOLD ONLY
center_output_ndx_file="gold.ndx"

##########################################
### DEFINING PATH TO STORE INFORMATION ###
##########################################
## PATH MOST LIKELY
path_most_likely="${input_sim_path}/${most_likely_dir}"

## CREATING DIRECTORY
if [ ! -e "${path_most_likely}" ]; then
    mkdir -p "${path_most_likely}"
fi

## DEFINING INPUT SUMMARY
path_output_summary="${path_most_likely}/${most_likely_output_summary_file}"

## DEFINING MOST LIKELY GRO FILE
output_np_gro_file="Idx_${most_likely_index}_NP_only.gro"
output_system_gro_file="Idx_${most_likely_index}_System.gro"

## DEFINING SYSTEM NAME
system_selection="System"

#################
### MAIN CODE ###
#################

## CHECKING IF INPUT DIRECTORY EXISTS
stop_if_does_not_exist "${input_sim_path}"

## GOING TO DIRECTORY
cd "${input_sim_path}"

#################################################
### GMX CODE TO GENERATE EXTRACT SIM XTC FILE ###
#################################################

## TRUNCATING THE FROM THE LAST 10 NS
if [ ! -e "${trunc_output_xtc_file}" ] || [ "${want_recalc}" == "True" ]; then
    ### TRUNCATING THE SYSTEM
    trunc_ "${sim_tpr_file}" "${sim_xtc_file}" "${system_selection}" "${trunc_output_xtc_file}" "${truncation_time}"
fi

## CENTERING AUNP
if [ ! -e "${extract_sim_xtc_file}" ] || [ "${want_recalc}" == "True" ]; then
create_xtc_centered_ "${sim_tpr_file}" "${trunc_output_xtc_file}" "${gold_residue_name}" "${extract_sim_xtc_file}" "${center_output_ndx_file}"

fi

### RESTRAINING ROTATION AND TRANSLATIONAL DEGREES OF FREEDOM
if [ ! -e "${analysis_sim_xtc_file}" ] || [ "${want_recalc}" == "True" ]; then
rot_trans_most_likely_ "${sim_tpr_file}" "${extract_sim_xtc_file}" "${gold_residue_name}" "${system_selection}" "${analysis_sim_xtc_file}" "${center_output_ndx_file}"
fi

############################################
### PYTHON SCRIPT FOR MOST LIKELY CONFIG ###
############################################

## SEEING IF SUMMARY EXISTS
if [ ! -e "${path_output_summary}" ] || [ "${want_recalc}" == "True" ]; then
    ## RUNNING PYTHON CODE /usr/bin/python3.4
    python3.6 "${python_most_likely}" -i "${input_sim_path}" \
                                      -o "${path_most_likely}" \
                                      -s "${most_likely_output_summary_file}" \
                                      -g "${sim_gro_file}" \
                                      -x "${analysis_sim_xtc_file}" \
                                      -p \
                                      --bundling_type "${displacement_vector_type}"
else
    echo "${most_likely_output_summary_file} already exists! Continuing script..."
fi

######################################################
### LOCATING MOST LIKELY CONFIGURATION AND DUMPING ###
######################################################
## DEFINING LINES TO LOOK UP TO
lines_to_look_up="$((${most_likely_index}+1))"

## FINDING NEAREST TRAJECTORY TIME
most_likely_time=$(head -n "${lines_to_look_up}" "${path_output_summary}" | tail -n1 | awk '{print $4}')
echo "The most likely time found: ${most_likely_time} ps"; sleep 1

## CREATING INDEX FILE
make_ndx_gold_with_ligand "${input_sim_path}/${sim_tpr_file}" \
                          "${input_sim_path}/${index_file}"   \
                          "${lig_residue_name}"   \
                          "${gold_residue_name}"

# if [[ ! -e "${input_sim_path}/${index_file}" ]] || [ "${want_recalc}" == "True" ]; then



# fi
# Sometimes if this code does not work, you may need to rerun make gold index

## DEFINING COMBINED NAME
combined_name="GNP_LIGANDS"
# "${gold_residue_name}_${lig_residue_name}"

## DUMPING NEAREST TRAJECTORY OF NP ONLY
if [[ ! -e "${path_most_likely}/${output_np_gro_file}" ]] || [ "${want_recalc}" == "True" ]; then
    ## PRINTING
    echo "Creating ${output_np_gro_file} from ${input_sim_path}..."
    ## USING TRJCONV
gmx trjconv -f "${input_sim_path}/${extract_sim_xtc_file}" \
            -s "${input_sim_path}/${sim_tpr_file}" \
            -o "${path_most_likely}/${output_np_gro_file}" \
            -n "${input_sim_path}/${index_file}" \
            -dump "${most_likely_time}" \
            -pbc mol -center >/dev/null 2>&1 << INPUTS
${gold_residue_name}
${combined_name}
INPUTS

else
    echo "${output_np_gro_file} already exists, continuing..."
fi

## DUMPING NEAREST FOR NP + WATER
if [[ ! -e "${path_most_likely}/${output_system_gro_file}" ]] || [ "${want_recalc}" == "True" ]; then
    ## PRINTING
    echo "Creating ${output_system_gro_file} from ${input_sim_path}..."
    ## USING TRJCONV 
gmx trjconv -f "${input_sim_path}/${extract_sim_xtc_file}" \
            -s "${input_sim_path}/${sim_tpr_file}" \
            -o "${path_most_likely}/${output_system_gro_file}" \
            -n "${input_sim_path}/${index_file}" \
            -dump "${most_likely_time}" \
            -pbc mol -center >/dev/null 2>&1 << INPUTS
${gold_residue_name}
${system_selection}
INPUTS
else
    echo "${output_system_gro_file} already exists, continuing..."
fi

## CHCKING FOR EXISTANCE
if [[ ! -e "${path_most_likely}/${output_np_gro_file}" ]] || [[ ! -e "${path_most_likely}/${output_system_gro_file}" ]]; then
    echo "Error, ${output_np_gro_file} or ${output_system_gro_file} does not exist!"
    echo "Check the path: ${path_most_likely}"
    sleep 5
    exit
fi

## REMOVING ANY EXTRAS
find . -type f -name "\#*" -delete

## GOING TO ALIGN THE FILE
cd "${path_most_likely}"

## GENERATING PDB FOR NP ONLY
if [[ ! -e "${output_np_gro_file%.gro}.pdb" ]] || [ "${want_recalc}" == "True" ]; then
    gmx editconf -f "${output_np_gro_file}" \
                 -o "${output_np_gro_file%.gro}.pdb"
fi

## GENERATING PDB FOR NP + WATER SYSTEM
if [[ ! -e "${output_system_gro_file%.gro}.pdb" ]] || [ "${want_recalc}" == "True" ]; then
    gmx editconf -f "${output_system_gro_file}" \
                 -o "${output_system_gro_file%.gro}.pdb"
fi
