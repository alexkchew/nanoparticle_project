#!/bin/bash

# compute_most_likely_loop.sh
# This script looks through computations of most likely. 
# The main idea is to generate a script that could do this in a quick fashion. 
# We assume that the input system is a spherical GNP. 


# Written by: Alex K. Chew (02/11/2020)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name


##################
### USER INPUT ###
##################

## DEFINING MAIN PATH TO RUN ON
sim_dir_name="EAM_COMPLETE"

### MOST LIKELY CONFIG B ASH CODE
bash_most_likely_config="${BASH_MOST_LIKELY_SCRIPT}"

## DEFINING MOST LIKELY INDEX
most_likely_index="1"
want_recalc="False"

## DEFINING DISPLACEMENT VECTOR ARRAY
declare -a displacement_vector_array=("avg_heavy_atom" "terminal_heavy_atom")

#################
### MAIN CODE ###
#################

## DEFINING PATH TO SIM
path_to_sims="${PATH2SIM}/${sim_dir_name}"

## DEFINING SIM ARRAY
read -a sim_array <<< $(ls -d ${path_to_sims}/*)

## DEFINING SIM ARRAY
declare -a sim_array=("${path_to_sims}/EAM_300.00_K_2_nmDIAM_C11CONH2_CHARMM36jul2017_Trial_1")

## LOOP THROUGH EACH SIM
for each_sim in ${sim_array[@]}; do
    ## LOOPING THROUGH EACH TYPE
    for displacement_vector_type in ${displacement_vector_array[@]}; do
    
        ## PRINTING
        echo "Working on ${each_sim}"
        ## DEFINING BASENAME
        sim_basename=$(basename ${each_sim})

        ## EXTRACTING THE NAME FOR EACH SIM
        read -a extracted_names <<< $(extract_output_name_gnp_water ${sim_basename})

        ## DEFINING LIGAND NAME
        ligand_name="${extracted_names[2]}"

        ## FINDING RESIDUE NAME
        residue_name="$(itp_get_resname ${each_sim}/${ligand_name}.itp)"

        ## RUNNING MOST LIKELY NP SIMS
        bash "${bash_most_likely_config}" "${each_sim}" \
                                          "${residue_name}" \
                                          "${most_likely_index}" \
                                          "${want_recalc}" \
                                          "${displacement_vector_type}"
                                      
    done

done




