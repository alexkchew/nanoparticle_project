#!/bin/bash

# prep_energy_min.sh
# This script runs energy minimizations on specific ligands to quickly assess if the force field parameters are correct.

## USAGE: bash prep_energy_min.sh rot_np5

# Originally written by Alex K. Chew (02/01/2018)
# ** UPDATES **
#   180418: Updating script to work with full_script ligand preparation

## VARIABLES:
# $1: molecule name
# $2: residue name
# $3: molecule pdb file name
# $4: molecule itp file name
# $5: molecule prm file name
# $6: output directory
# $7: force field

## GLOBAL VARIABLES:
#   PREP_LIGAND_INPUT_DIR: input directory for preparation ligands
#   PREP_LIGAND_INPUT_TOP: input topology directory for preparation ligands
#   PREP_LIGAND_INPUT_MDP: input mdp files for preparation ligands

### LOADING PRE-DEFINED VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"
echo -e "\n ----- prep_energy_min.sh ----- \n"

### INPUT VARIABLES
input_molecule_name="$1" # Name of the molecule you care about
input_molecule_resname="$2" # Residue name of the molecule
# NOTE! *.itp, *.prm, and *.pdb file should be available!
# INPUT FILE PATH
input_itp_file_path="$3" # itp file
input_pdb_file_path="$4" # pdb file
input_prm_file_path="$5" # parameter file
### OUTPUT DIRECTORY
output_dir="$6"
## DEFINING FORCE FIELD
forcefield="$7"

## DEFINING BOX EXPANSION SIZE
box_size="5" # Length of the box


## DEFINING LOGICALS
skip_creation="True" # if True, you skip all creation and go straight to converting gro to energy minimization

## GETTING FILE NAME
input_pdb_file_name="$(basename ${input_pdb_file_path})"

### FORCE FIELD
input_force_field_file_path="${INPUT_DIR}/${forcefield}"

### TOPOLOGY FILE
## NEED TO CHANGE "MOLECULE" WITHIN TOPOLOGY
input_topology_file_name="em.top" # default topology file
input_topology_file_path="${PREP_LIGAND_INPUT_TOP}/${input_topology_file_name}" 

### MDP FILE
input_mdp_file_name="em.mdp"
input_mdp_file_path="${PREP_LIGAND_INPUT_MDP}/${input_mdp_file_name}"

########################
### OUTPUT VARIABLES ###
########################
### DEFINING OUTPUT NAMES
output_gro_file_name="${input_molecule_name}.gro"

clean_up_dir="${output_dir}/setup_files" # Directory to clean up

###################
### MAIN SCRIPT ###
###################

if [[ "${skip_creation}" != "True" ]]; then
    ## CREATING DIRECTORY
    check_output_dir "${output_dir}"

    ## GOING INTO DIRECTORY
    cd "${output_dir}"; echo "Going into: ${output_dir}"

    ## COPYING PDB FILE, PRM FILE, ITP FILE, ETC.
    echo "Copying pdb, prm, and itp file for ${input_molecule_name} to ${output_dir}"
    cp -r "${input_itp_file_path}" "${input_pdb_file_path}" "${input_prm_file_path}" "${output_dir}"

    ## COPYING FORCE FIELD TO DIRECTORY
    echo "Copying force field: ${forcefield} to output directory"
    cp -r "${input_force_field_file_path}" "${output_dir}"

    ## COPYING MDP FILE TO DIRECTORY
    echo "Copying energy minimization file to output directory"
    cp -r "${input_mdp_file_path}" "${output_dir}"

    ## COPYING TOPOLOGY FILE TO DIRECTORY
    echo "Copying topology for ${input_molecule_name}"
    cp -r "${input_topology_file_path}" "${output_dir}"
    sed -i "s/MOLECULE/${input_molecule_name}/g" "${input_topology_file_name}"
    sed -i "s/RESNAME/${input_molecule_resname}/g" "${input_topology_file_name}"


    ### FIX 09282018 ###
    ## PDB files sometime has >100 atoms, which mess up the pdb file! We will edit the input pdb file to ensure no errors for this.
    ## ADDING SPACE FOR PDB FILE
    # sed -i "s/${input_molecule_resname}/ ${input_molecule_resname}/g" "${input_pdb_file_name}"
    ## DOESN'T WORK, pdb is strictly space dependent -- well known issue presented https://mailman-1.sys.kth.se/pipermail/gromacs.org_gmx-users/2015-February/095271.html

    ## COUNTING TOTAL NUMBER OF ATOMS
    pdb_total_atoms=$(grep "ATOM" "${input_pdb_file_name}" | wc -l)

    ## CHECKING IF TOTAL ATOMS IS GREATER THAN 99 -- IF SO, IT MAY BE AN ISSUE!
    if [ ${pdb_total_atoms} -gt 99 ]; then
        echo "NOTE: You have more than 99 atoms!"  
        echo "Total atoms: ${pdb_total_atoms}"
        echo "GROMACS is known to have an error with editconf when the atom number becomes too large!"
        echo "As a result, we will use a custom python file that will correct for this issue"
        echo "Pausing here so you can see this message!"
        echo "You may need to edit ${output_gro_file_name} such that it is correctly labeled!"
        sleep 3
    fi
    
    ## CENTER THE MOLECULE
    echo "----- CENTERING MOLECULE -----"
    gmx editconf -f "${input_pdb_file_name}" -o ${output_gro_file_name} -box "${box_size}" "${box_size}" "${box_size}"
else
    ## GOING INTO DIRECTORY
    cd "${output_dir}"; echo "Going into: ${output_dir}"
fi

## RUNNING ENERGY MINIMIZATION
gmx grompp -f ${input_mdp_file_name} -c ${output_gro_file_name} -p ${input_topology_file_name} -o "${input_molecule_name}"_em.tpr
gmx mdrun -v -nt 1 -deffnm "${input_molecule_name}"_em

## MAKING BOX SIZE TO FIT THE MOLECULE
gmx editconf -f "${input_molecule_name}_em.gro" -o "${input_molecule_name}_em.gro" -d 0

### CREATING A PDB FILE FOR THE SAKE OF VISUALIZATION
gmx editconf -f "${input_molecule_name}_em.gro" -o "${input_molecule_name}_em.pdb"

## CLEANING UP FILES
mkdir -p "${clean_up_dir}"

## MOVE EVERYTHING BUT PDB file
if [ -e "${input_molecule_name}_em.gro" ]; then
    mv * ${clean_up_dir} 2>/dev/null
    mv "${clean_up_dir}/${input_molecule_name}_em.gro" "${output_dir}/${input_molecule_name}.gro"
    mv "${clean_up_dir}/${input_molecule_name}_em.pdb" "${output_dir}/${input_molecule_name}.pdb"
    mv "${clean_up_dir}/${input_molecule_name}.itp" "${output_dir}/${input_molecule_name}.itp"
    mv "${clean_up_dir}/${input_molecule_name}.prm" "${output_dir}/${input_molecule_name}.prm"
    
    ### PRINTING SUMMARY
    echo "********** DONE *********"
    echo "Finalized GRO after minimization: ${input_molecule_name}.gro"
    echo "Available in directory: ${output_dir}"
    echo "Now, you can use the GRO file for subsequent calculations"
    echo "Good luck!"

else
    echo "ERROR! ${input_molecule_name}_em.gro does not exist! Something went wrong with the energy minimization"
    echo "Stopping here, check the output directory: ${output_dir}"
    echo "Pausing here so you can see the error!"
    sleep 5
    exit
fi