#!/bin/bash

# full_prep_cosolvent_mapping_sims.sh
# This script is designed for cosolvent mapping simulations

## TODO: NEED TO UPDATE SCRIPT ***

# Written by Alex K. Chew (04/17/2020)

## USAGE:
#   bash full_prep_cosolvent_mapping_sims.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

### DEFINING PATH TO SCRIPT
SCRIPT_PATH="$PATH2SCRIPTS/prep_cosolvent_mapping_sims.sh"

###########################################
### USER-DEFINED VARIABLES 
###########################################

## DEFINING SIMULATION DIR
parent_dir="20200401-Renewed_GNP_sims_with_equil"
parent_dir="20200618-Most_likely_GNP_water_sims_FINAL"
# "20200419-unsaturated_frozen"
# parent_dir="20200403-Planar_SAMs-5nmbox_vac_with_npt_equil"

## DEFINING INPUT PATH
input_path="/home/shared/np_hydrophobicity_project/simulations/${parent_dir}"

if [[ "${parent_dir}" == "20200403-Planar_SAMs-5nmbox_vac_with_npt_equil" ]]; then
  want_planar=true
  ## DEFINING PREFIX AND SUFFIX
  input_sim_prefix="NVTspr_50_Planar_300.00_K_"
  input_sim_suffix="_10x10_CHARMM36jul2017_intffGold_Trial_1-5000_ps"
  # NVTspr_50_Planar_300.00_K_C11CF3_10x10_CHARMM36jul2017_intffGold_Trial_1-5000_ps
else
  want_planar=false
  ## DEFINING PREFIX AND SUFFIX
  input_sim_prefix="MostlikelynpNVTspr_50-EAM_300.00_K_2_nmDIAM_"
  input_sim_suffix="_CHARMM36jul2017_Trial_1_likelyindex_1"
fi

## DEFINING OUTPUT DIRECTORY NAME
parent_outputdir="20200625-GNP_cosolvent_mapping_sims_unsat_branched"
# "20200427-planar_cosolvent_mapping_planar_rerun_GNP_noformic_aci"
# "20200420-cosolvent_mapping_dod"
# parent_outputdir="20200416-cosolvent_mapping"

## DEFINING LIGAND NAMES
declare -a ligand_names=("C11double67OH" "C11branch6OH")
# "C11double67COOH" "dodecen-1-thiol"
# "C11CF3" "C11CONH2" "C11NH2" "C11OH"
# "dodecanethiol" "C11COOH"
# "C11COOH" 
# "C11double67COOH"
# "dodecen-1-thiol"
# "C11COOH"
#  
# "dodecanethiol" 
# "dodecanethiol" "C11COOH"
# "C11COOH"

## DEFINING TRIALS
declare -a trial_num=("1" "2" "3" "4" "5")
# "4"
# "5"
# "1" "2" "3" "4" "5"
#  "3" "4" "5"
#  "2" "3" "4" "5"

# "1" "2" "3" "4"

## DEFINING COSOLVENT EPRC
cosolvent_perc="1"

## DEFINING COSOLVENT INFORMATION
cosolvent_names="propane"
# ,formicacid
# methanol,formicacid

###########################################

## LOOPING THROUGH EACH SOLVENT
for each_ligand in "${ligand_names[@]}"; do
    ## GETTING SIM PATH
    sim_dir="${input_sim_prefix}${each_ligand}${input_sim_suffix}"
    input_sim_path="${input_path}/${sim_dir}"

    ## LOOPING THROUGH TRIAL NUMBERS
    for each_trial in "${trial_num[@]}"; do
        bash "${SCRIPT_PATH}" "${input_sim_path}" \
                              "${each_trial}" \
                              "${parent_outputdir}" \
                              "${cosolvent_names}" \
                              "${cosolvent_perc}" \
                              "${want_planar}"
    done

done

### PRINTING SUMMARY
echo "------SUMMARY------"
echo "Ligand names: ${ligand_names[@]}"
echo "Trial numbers: ${trial_num[@]}"
echo "Cosolvent names: ${cosolvent_names}"
echo "Cosolvent percentages: ${cosolvent_perc}"
