#!/bin/bash

# full_prep_switch_solvents.sh
# This script runs prep_switch_solvents.sh for multiple solvent systems. 

## TODO: NEED TO UPDATE SCRIPT ***

# Written by Alex K. Chew (09/09/2019)

## USAGE:
#   bash full_prep_switch_solvents.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

### DEFINING PATH TO SCRIPT
SCRIPT_PATH="$PATH2SCRIPTS/prep_mixed_solvent_multiple.sh"

###########################################
### USER-DEFINED VARIABLES 
###########################################

# ## DEFINING SIMULATION TO GET DETAILS FROM
# simulation_dir_name="190520-2nm_ROT_Sims_updated_forcefield_new_ligands"

# ## DEFINING NEW SIMULATION DIRECTORY NAME
# output_simulation_dir="190909-Switching_solvents_large_box"

## DEFINING LIGAND NAMES
declare -a cosolvent_perc_list=("10") # $1
# "1"

## DEFINING LIGAND NAMES
declare -a initial_box_length_list=("3") # $1
# "10"

## DEFINING SOLVENTS
declare -a solvent_list=("methane")
# "formate,aceticacid,methylammonium,propane"
# "tetrahydrofuran" "dmso" "dioxane" "methanol"

###########################################

## LOOPING THROUGH EACH LIGAND
for cosolvent_perc in "${cosolvent_perc_list[@]}"; do
	## LOOPING THROUGH EACH LIGAND
	for box_length in "${initial_box_length_list[@]}"; do
	    ## LOOPING THROUGH EACH COSOLVENT
	    for current_solvent in "${solvent_list[@]}"; do
	        ## PRINTING
	        echo "RUNNING THE FOLLOWING COMMAND:"
	        echo "bash "${SCRIPT_PATH}" ${current_solvent} \
	        							${cosolvent_perc} \
	        							${box_length}"
			bash "${SCRIPT_PATH}" ${current_solvent} \
	    						  ${cosolvent_perc} \
			        			  ${box_length}
			done
    done
done

### PRINTING SUMMARY
echo "------SUMMARY------"
echo "Precent systems: ${cosolvent_perc_list[@]}"
echo "Box lengths: ${initial_box_length_list[@]}"
echo "Cosolvents: ${solvent_list[@]}"