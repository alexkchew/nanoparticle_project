#!/bin/bash

# prep_cosolvent_mapping_sims.sh
# 
# The purpose of this script is to prepare cosolvent mapping simulations. 
# The main idea is that we have a nanoparticle with a willard-chandler surface. 
# Now, we mix cosolvents with water to see the extent of binding there is between 
# the solvent environment and the GNP. By performing these simulations, 
# we could map preferential binding around the GNP, allowing us to quantify 
# potential protein binding sites and so forth. 
#
# Written by: Alex K. Chew (04/16/2020)
# 
# VARIABLES:
#   $1: input simulation path
#   $2: trial number
#   $3: parent output directory
#   $4: cosolvent names, separated by comma
#   $5: percentage of cosolvent in mol perc
#   $6: true/false if you have a planar SAM

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING INPUT PATH TO SIMULATION
input_sim_path="${1-/home/shared/np_hydrophobicity_project/simulations/20200401-Renewed_GNP_sims_with_equil/MostlikelynpNVTspr_50-EAM_300.00_K_2_nmDIAM_C11COOH_CHARMM36jul2017_Trial_1_likelyindex_1}"

## DEFINING TRIAL NUMBER
trial_num="${2-1}"

## DEFINING OUTPUT DIRECTORY NAME
parent_outputdir="${3-20200416-cosolvent_mapping}"

## DEFINING COSOLVENT INFORMATION
cosolvent_names="${4-methanol,formicacid}"

## DEFINING COSOLVENT PERCENTAGES
cosolvent_perc="${5-1}" # 1 mol percent cosolvent

## DEFINING IF YOU WANT PLANAR
want_planar="${6-false}"

## SLEEP TIME
sleep_time="0"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING TEMPERATURE
temp="300"

## PREFIX TO THE PRODUCTION RUNS
input_prefix="sam_prod"

## INPUT TOP
input_top="sam.top"

## DEFINING MDP FILE
input_mdp="nvt_double_prod_gmx5_charmm36_frozen_gold.mdp"

## DISTANCE TO EDGE IN NANOMETERS
dist_to_edge="1" # GNPs
dist_z_planar="5" # 5 nm solvent box

## SCRIPT TO EXTRACT GNP ALONE
extract_gnp_alone_script="${PATH2SCRIPTS}/extraction_of_GNP_alone.sh"

## DEFINING GNP ALONE PREFIX
gnp_alone_prefix="gnp_alone"

## DEFINING SUMMARY
gnp_alone_summary="${gnp_alone_prefix}.summary"

## DEFINING PURE WATER NAME
water_residue_name="SOL"
## DEFINING WATER MODEL
water_model="tip3p"
## DEFINING ITP FILE FOR NP
np_itp_file="sam.itp"
## DEFINING FORCEFIELD
forcefield="${FORCEFIELD}"
## DEFINING NUMBER OF CORES
num_cores="28"
## DEFAULT NUMBER OF PURE SOLVENTS
default_num_solvents="216"

## DEFINING PREFIX
output_prefix="sam"

## GRO FILE FOR SOLVATED
path_gro_water_solvated="${INPUT_PACKMOL_PATH}/spc216.gro"

#############################
### MIXED SOLVENT SYSTEMS ###
#############################

## DEFINING TEMPERATURE OF MIXED-SOLVENT
mixed_solv_temp='300'

## CONVERTING THE COSOLVENT NAME TO AN ARRAY
read -a cosolvent_name_array <<< $(str2array_by_delim "${cosolvent_names}" ",")

####################################################
### SOLVENT INFORMATION GETTING MOLECULAR VOLUME ###
####################################################

## CREATING COSOLVENT MOLECULAR VOLUMES IN NM3/MOLECULE
declare -a cosolvent_molecular_volumes

## GETTING COSOLVENT RESIDUE NAME
declare -a cosolvent_residue_name

## LOOPING THROUGH EACH COSOLVENT
for cosolvent in "${cosolvent_name_array[@]}"; do
    
    ## PRINTING
    echo "Computing molecular volume for cosolvent: ${cosolvent}"
    
    ## PATH TO INPUT MOLECULE
    path_input_molecule="${PREP_SOLVENT_FINAL}/${cosolvent}"

    ## CHECKING INPUT FILE
    check_setup_file "${path_input_molecule}" "${cosolvent}"

    ## DEFINING MOLECULE GRO, ITP, PRM FILES
    molecule_gro_file="${cosolvent}.gro"
    molecule_itp_file="${cosolvent}.itp"
    molecule_prm_file="${cosolvent}.prm"

    ##################################
    ### COMPUTING MOLECULAR VOLUME ###
    ##################################

    ### COMPUTING MOLECULAR VOLUME
    mol_volume_cosolvent="$(get_molecular_volume_solvent ${cosolvent})"
    
    ## GETTING RESIDUE NAME
    cos_res_name="$(itp_get_resname ${path_input_molecule}/${molecule_itp_file})"

    ## ADDING MOLECULAR VOLUME
    cosolvent_molecular_volumes+=(${mol_volume_cosolvent})
    cosolvent_residue_name+=(${cos_res_name})
done

## JOINING COSOLVENT RESIDUE NAMES
cosolvent_residue_name_commas=$(join_by "," ${cosolvent_residue_name[@]})

###################################
### DEFINING OUTPUT INFORMATION ###
###################################

## GETTING BASE NAME
sim_basename=$(basename ${input_sim_path})

## EXTRACTING THE NAME
if [[ "${want_planar}" == false ]]; then
    name_without_prefix=${sim_basename#"MostlikelynpNVTspr_"}
else
    name_without_prefix=${sim_basename}
fi

## DEFINING OUTPUT FOLDER
output_folder="comap_${cosolvent_residue_name_commas}_${cosolvent_perc}_${trial_num}_${name_without_prefix}"

## DEFINING PATH TO OUTPUT FOLDER
path_output_folder="${PATH2SIM}/${parent_outputdir}/${output_folder}"

###################
### MAIN SCRIPT ###
###################

## NP EXAMPLES
## RUNNING EXTRACT GNP ALONE
bash "${extract_gnp_alone_script}" "${input_sim_path}" \
								   "${input_prefix}" \
								   "${dist_to_edge}"

## DEFINING GRO FILE
if [[ "${want_planar}" == false ]]; then
    input_gro_file="${gnp_alone_prefix}-d_${dist_to_edge}.gro"
else
    input_gro_file="${gnp_alone_prefix}.gro"
fi
path_input_gro_file="${input_sim_path}/${gnp_alone_prefix}/${input_gro_file}"

## EXTRACTING SUMMARY
path_input_summary_file="${input_sim_path}/${gnp_alone_prefix}/${gnp_alone_summary}"

## GETTING LIGAND NAME
gold_residue_name=$(grep "GOLD_NAME" ${path_input_summary_file} | awk '{print $2}')
ligand_names=$(grep "LIG_NAME" ${path_input_summary_file} | awk '{print $2}')
ligand_res_names=$(grep "LIG_RESNAME" ${path_input_summary_file} | awk '{print $2}')

## CREATING DIRECTORY
create_dir "${path_output_folder}" -f

## GOING INTO DIRECTORY
cd "${path_output_folder}"

######################################
### PART 1: COMPUTING VOLUME OF NP ###
######################################

echo ""
echo "================================"
echo "Part 1 - NP Volume computation"
echo "================================"

## COPYING GRO FILE
cp -r "${path_input_gro_file}" "${path_output_folder}/${output_prefix}.gro"

## COPYING TOPOLOGY
cp -r "${input_sim_path}/${input_top}" "${path_output_folder}/${input_top}"
copy_top_include_files "${input_sim_path}/${input_top}" "${path_output_folder}"

## CLEARING TOP
if [[ "${want_planar}" == false ]]; then
    ## COPYING MDP
    cp -r "${input_sim_path}/${input_mdp}" "${path_output_folder}/${input_mdp}"
    top_clear_molecules "${input_top}"
    ## ADDING ONE SAM
    echo "   sam   1" >> "${input_top}"
else
    ## REMOVE LAST LINE (WATER LINE)
    head -n -1 "${input_top}" > temp.txt ; mv temp.txt "${input_top}"
fi

if [[ "${want_planar}" == false ]]; then
## CREATING TPR FILE WITHOUT CONSTRAINTS
top_without_posre="${input_top%.top}_no_constraints.top"
cp "${input_top}" "${top_without_posre}"
general_comment_out "${top_without_posre}" "lig_posre.itp" ";"
general_comment_out "${top_without_posre}" "sulfur_posre.itp" ";"
general_comment_out "${top_without_posre}" "gold_posre.itp" ";"

## USING GMX GROMPP
echo "--> Creating TPR file without constraints"
gmx grompp -f "${input_mdp}" \
			-p "${top_without_posre}" \
			-c "${output_prefix}.gro" \
			-o "${output_prefix}.tpr" >/dev/null 2>&1

## USING GMX SASA TO COMPUTE VOLUME OF NP
## COPYING VDW RADII
cp -r "${VDW_RADII_LIST}" "${path_output_folder}"
## RUNNING SASA
probe_radius="0.14" # default
volume_xvg="volume.xvg"
echo "--> Computing with gmx sasa, probe radius of ${probe_radius} nm"
gmx sasa -tv "${volume_xvg}" \
		 -f "${output_prefix}.gro" \
		 -s "${output_prefix}.tpr" \
		 -probe "${probe_radius}" >/dev/null 2>&1 << INPUTS
System
INPUTS
stop_if_does_not_exist "${volume_xvg}"
## EXTRACTING VOLUME INFORMATION
np_volume=$(tail -n1 "${volume_xvg}" | awk '{print $2}')

## EXTRACTING BOX VOLUME
box_volume=$(gro_compute_volume "${output_prefix}.gro")

## COMPUTING AVAILABLE VOLUME
available_volume=$(awk -v total=${box_volume} -v np_vol=${np_volume} 'BEGIN{ printf  "%.4f",total - np_vol}')

## CREATING PDB FILE
echo "Creating PDB file with NP only"
gmx trjconv -f "${output_prefix}.gro" \
			-s "${output_prefix}.tpr" \
			-o "${output_prefix}.pdb" >/dev/null 2>&1 << INPUTS
System
INPUTS


## PRINTING
echo "Volume of NP: ${np_volume} nm^3"
echo "Total volume of box: ${box_volume} nm^3"
echo "Available volume: ${available_volume} nm^3"


else
    echo "Since Planar SAMs is on, skipping volume calculation"
    echo "Instead, we will reshape the planar SAMs to minimize wasted space"

    ## READING BOX SIZE
    read -a box_size <<< $(gro_measure_box_size "${output_prefix}.gro")

    ## EDIT CONF TO EDIT 
    echo "Shrinking gro file: ${output_prefix}_shrink.gro"
    gmx editconf -f "${output_prefix}.gro" \
                 -o "${output_prefix}_shrink.gro" \
                 -d "0.1" >/dev/null 2>&1


    ## FIXING THE BOX SIZE
    read -a new_box_size <<< $(gro_measure_box_size "${output_prefix}_shrink.gro")

    ## EDITCONF TO FIX BOX SIZE
    echo "Resizing gro file: ${output_prefix}_shrink_resized.gro"
    echo "New box size: ${box_size[0]} ${box_size[1]} ${new_box_size[2]}"
    gmx editconf -f "${output_prefix}_shrink.gro" \
                 -o "${output_prefix}_shrink_resized.gro" \
                 -box "${box_size[0]}" "${box_size[1]}" "${new_box_size[2]}" >/dev/null 2>&1

    ## MOVING AND GENERATING SETUP FILE
    mkdir setup_files
    mv "${output_prefix}_shrink.gro" "${output_prefix}.gro" "${output_prefix}_shrink_resized.gro" setup_files
    mv setup_files/"${output_prefix}_shrink_resized.gro" ./"${output_prefix}.gro"

    ## DEFINING AVAILABLE VOLUME
    available_volume=$(awk -v box_x=${box_size[0]} \
                           -v box_y=${box_size[1]} \
                           -v box_z=${dist_z_planar} 'BEGIN{ printf "%.5f", box_x*box_y*box_z }') 
fi

## SLEEPING
sleep "${sleep_time}"

############################################
### PART 2: COMPUTING NUMBER OF SOLVENTS ###
############################################

echo ""
echo "==========================================="
echo "Part 2 - Computing total number of solvents"
echo "==========================================="

## COMPUTING MOLAR VOLUME OF WATER
num_density_water=$(gro_compute_num_density ${default_num_solvents} ${path_gro_water_solvated})
## FINDING VOLUME / 1 MOLECULE
mol_volume_water=$(awk -v num_dens=${num_density_water} 'BEGIN{ printf "%.5f", 1/num_dens }') 

## FINDING MOLE FRACTION OF WATER REQUIRED
num_cosolvents=${#cosolvent_name_array[@]}
mol_frac_cosolvents=$(awk -v frac_cos=${cosolvent_perc} 'BEGIN{ printf "%.5f", frac_cos/100 }') 
mol_frac_water=$(awk -v num_cos=${num_cosolvents} -v mol_frac_cos=${mol_frac_cosolvents} 'BEGIN{ printf "%.5f",1 - (num_cos * mol_frac_cos)}') 

## GETTING MOLE FRACTION ARRAY
declare -a mole_frac_array
for cosolvent in "${cosolvent_name_array[@]}"; do mole_frac_array+=("${mol_frac_cosolvents}"); done
mole_frac_array+=("${mol_frac_water}")

## GETTING TOTAL MOLAR VOLUMES ARRAY
declare -a molar_volume_array
for cosolvent_vol in "${cosolvent_molecular_volumes[@]}"; do molar_volume_array+=("${cosolvent_vol}"); done
molar_volume_array+=("${mol_volume_water}")


echo "Total available volume (nm3): ${available_volume}"
echo "Total number of cosolvents: ${num_cosolvents}"
echo "Molar volume of water: ${mol_volume_water} nm^3 / molecule"
echo "Mole fraction of water: ${mol_frac_water}"
echo "Mole fraction array (nm^3): ${mole_frac_array[@]}"
echo "Molar volume array (nm^3): ${molar_volume_array[@]}"

## FINDING TOTAL NUMBER OF MOLECULES
# N = V / (y_a * V_a + y_b * V_b + ...)
denom=0
## LOOPING TO GET DENOMINATOR
for index in $(seq 0 $(( ${#molar_volume_array[@]}-1)) ); do
    denom=$(awk -v denom=${denom} \
    			-v cos_mv=${molar_volume_array[index]} \
    		    -v molfrac=${mole_frac_array[index]} 'BEGIN{ printf "%.8f", denom + cos_mv * molfrac}') 
done
## PRINTING DENOM
total_molecules=$(awk -v denom=${denom} -v vol=${available_volume} 'BEGIN{ printf "%d",vol/denom}') 

## PRINTING
echo "--------------------------------------------"
echo "Total number of molecules: ${total_molecules}"

## FINDING TOTAL MOLES FOR EACH ONE
declare -a num_molecules_array
total_molecules_added=0
for index in $(seq 0 $(( ${#molar_volume_array[@]}-1)) ); do
    ## SEEING IF INDEX IS THE LAST ONE (WATER)
    if [[ "${index}" != "$((${#molar_volume_array[@]}-1))" ]]; then
        ## COMPUTING NUM MOLES
        num_molecules=$(awk -v total=${total_molecules} -v mol_frac=${mole_frac_array[index]} 'BEGIN{ printf "%d",total*mol_frac}') 
        ## ADDING TO TOTAL MOLECULES ADDED
        total_molecules_added=$((${total_molecules_added} + ${num_molecules}))
    else
        ## COMPUTING NUM MOLES
        num_molecules=$(awk -v total=${total_molecules} -v moles_added=${total_molecules_added} 'BEGIN{ printf "%d",total-moles_added}') 
    fi
    ## APPENDING TO ARRAY
    num_molecules_array+=("${num_molecules}")
done

## COMPUTING TOTAL NUMBER OF MOLECULES
echo "Number of molecules array: ${num_molecules_array[@]}"

## SLEEPING
sleep "${sleep_time}"

###############################
### PART 3: ADDING SOLVENTS ###
###############################

## EXAMPLE:
# tolerance 2.0 
# filetype pdb
# output _OUTPUTPDB_


# structure _NPPDB_
#   number 1
#   center
#   fixed _HALFBOX_ _HALFBOX_ _HALFBOX_ 0. 0. 0.
# end structure

# structure _COSOLVENTPDB_
#   number _NUMCOSOLVENT_
#   inside cube 0. 0. 0. _MAXANGS_
# end structure 

# structure _WATERPDB_
#   number _NUMWATER_
#   inside cube 0. 0. 0. _MAXANGS_
# end structure 

## DEFINING PACKMOL INP
packmol_input="packmol.inp"
packmol_output="packmol.out"
output_pdb="${output_prefix}_solvated.pdb"

## READING BOX SIZE
read -a box_size <<< $(gro_measure_box_size "${output_prefix}.gro")

## GNPS
if [[ "${want_planar}" == false ]]; then

    ## GETTING HALF BOX LENGTH
    half_box_length_x_angs=$(awk -v box_dim=${box_size[0]} 'BEGIN{ printf  "%.5f", box_dim / 2 * 10}')
    half_box_length_y_angs=$(awk -v box_dim=${box_size[1]} 'BEGIN{ printf  "%.5f", box_dim / 2 * 10}')
    half_box_length_z_angs=$(awk -v box_dim=${box_size[2]} 'BEGIN{ printf  "%.5f", box_dim / 2 * 10}')

    ## GETTING TOTAL BOX SIZE
    total_box_size_angs=$(awk -v x_dim=${box_size[0]} 'BEGIN{ printf  "%.5f", x_dim * 10}')
else
    ## DEFINING NEW BOX SIZE
    declare -a box_size=(${box_size[0]} ${box_size[1]} ${dist_z_planar})
    ## PLANAR SAMS
    ## GETTING TOTAL BOX SIZE
    total_box_size_angs_x=$(awk -v x_dim=${box_size[0]} 'BEGIN{ printf  "%.5f", x_dim * 10}')
    ## GETTING TOTAL BOX SIZE
    total_box_size_angs_y=$(awk -v x_dim=${box_size[1]} 'BEGIN{ printf  "%.5f", x_dim * 10}')
    ## GETTING TOTAL BOX SIZE
    total_box_size_angs_z=$(awk -v x_dim=${box_size[2]} 'BEGIN{ printf  "%.5f", x_dim * 10}')
fi

echo ""
echo "==========================================="
echo "Part 3 - Adding solvents with PACKMOL"
echo "==========================================="

## CREATING INPUT FILE
echo "Creating packmol input: ${packmol_input}"
echo "seed ${trial_num}" > "${packmol_input}"
echo "tolerance 2.0" >> "${packmol_input}"
echo "filetype pdb" >> "${packmol_input}"
echo "output ${output_pdb}" >> "${packmol_input}"
echo "" >> "${packmol_input}"

## GNPS
if [[ "${want_planar}" == false ]]; then
    echo "structure ${output_prefix}.pdb" >> "${packmol_input}"
    echo "  number 1" >> "${packmol_input}"
    echo "  center" >> "${packmol_input}"
    echo "  fixed ${half_box_length_x_angs} ${half_box_length_y_angs} ${half_box_length_z_angs} 0. 0. 0." >> "${packmol_input}"
    echo "end structure" >> "${packmol_input}"
    echo "" >> "${packmol_input}"
fi

## ADDING COSOLVENTS
## LOOPING FOR EACH SOLVENT
for index in $(seq 0 $(( ${#molar_volume_array[@]}-1)) ); do
    ## GETTING NUMBER OF MOLECULES
    num_molecules=${num_molecules_array[index]}
    ## SEEING IF INDEX IS THE LAST ONE (WATER)
    if [[ "${index}" != "$((${#molar_volume_array[@]}-1))" ]]; then
        ## DEFINING COSOLVENT
        cosolvent=${cosolvent_name_array[index]}
        ## GETTING PDB FILE NAME
        path_pdb="${PREP_SOLVENT_FINAL}/${cosolvent}/prep_files/${cosolvent}.pdb"
        ## DEFINING PDB FILE NAME
        pdb_name=$(basename ${path_pdb})
    else
        ## DEFINING PDB FILE NAME
        pdb_name="water_${water_model}.pdb"
        ## WATER
        path_pdb="${INPUT_PACKMOL_PATH}/${pdb_name}"
    fi
    
    ## COPYING PDB FILE
    cp "${path_pdb}" "${path_output_folder}"
    ## ADDING TO INPUT SCRIPT
if [[ "${want_planar}" == false ]]; then
    echo \
"structure ${pdb_name}
  number ${num_molecules}
  inside cube 0. 0. 0. ${total_box_size_angs}
end structure
" >> "${packmol_input}"

else
echo \
"structure ${pdb_name}
  number ${num_molecules}
  inside box 0. 0. 0. ${total_box_size_angs_x} ${total_box_size_angs_y} ${total_box_size_angs_z}
end structure
" >> "${packmol_input}"
fi
done

## RUNNING PACKMOL
echo "==== Running packmol ===="
echo "Check output in ${packmol_output}"
"${PACKMOL_SCRIPT}" < ${packmol_input} > "${packmol_output}"

echo "Last 10 lines:"
tail -n10 "${packmol_output}"
stop_if_does_not_exist "${output_pdb}"

## DEFINING INITIAL GRO FILE
initial_gro_file="${output_pdb%.pdb}.gro"

## DEFINING SOME BOX TO THE EDGE
edge_to_solute="0.05"

if [[ "${want_planar}" == false ]]; then
## USING EDIT CONF TO GENERATE GRO FILE FOR GNP
gmx editconf -f "${output_pdb}" \
			 -o "${initial_gro_file}" \
             -d "${edge_to_solute}" >/dev/null 2>&1
else
## FOR PLANAR SAM, NEED EXACT BOX SIZES
gmx editconf -f "${output_pdb}" \
             -o "${initial_gro_file}" \
			 -box "${box_size[0]}" "${box_size[1]}" "${box_size[2]}" >/dev/null 2>&1

fi

## CHECKING IF EXISTS
echo "Creating solvated gro file: ${initial_gro_file}"
stop_if_does_not_exist "${initial_gro_file}"

## SLEEPING
sleep "${sleep_time}"

#############################
### PART 4: EDITING FILES ###
#############################

echo ""
echo "==========================================="
echo "Part 4 - Editing topology, mdp, files"
echo "==========================================="

## FOR PLANAR SAMS, ADD SOLVENT BOX ON THE TOP AND BOTTOM
if [[ "${want_planar}" == true ]]; then
    combine_file_1="combine1.gro"
    combine_file_2="combine2.gro"
    buffer_zone="0.05"
    ## TOP SOLVENT
    echo "Combining top solvent with planar SAM --> ${combine_file_1}"
    gro_combine_two_files_by_z "${initial_gro_file}" \
                               "${output_prefix}.gro" \
                               "${combine_file_1}" \
                               "${buffer_zone}"
    ## BOTTOM SOLVENT
    echo "Combining top/planar SAM with bottom water --> ${combine_file_2}"
    gro_combine_two_files_by_z "${combine_file_1}" \
                               "${initial_gro_file}" \
                               "${combine_file_2}" \
                               "${buffer_zone}"

    ## CHECKING IF EXISTING
    stop_if_does_not_exist "${combine_file_2}"
    
    ## MOVING TO SETUP
    mv "${combine_file_1}" "${combine_file_2}" "${initial_gro_file}" setup_files
    mv "setup_files/${combine_file_2}" ./"${initial_gro_file}"

fi

# TOPOLOGY FILE
## ADDING TOTAL NUMBER OF MOLECULES TO TOPOLOGY
## GETTING TOTAL MOLAR VOLUMES ARRAY
declare -a solvent_residue_name
for cosolvent_res_name in "${cosolvent_residue_name[@]}"; do solvent_residue_name+=("${cosolvent_res_name}"); done
solvent_residue_name+=("${water_residue_name}")


top_search_keys_prm="forcefield.itp"
## FINDING ITP
if [[ "${want_planar}" == false ]]; then
    top_search_keys_itp="gold_posre.itp"
else
    top_search_keys_itp="ions.itp"
fi

## LOOPING THROUGH SOLVENTS
for index in $(seq 0 $(( ${#molar_volume_array[@]}-1)) ); do
	## GETTING SOLVENT RESIDUE NAME
	solvent_res_name="${solvent_residue_name[index]}"
	solvent_num="${num_molecules_array[index]}"

    ## DOUBLE SOLVENTS FOR PLANAR SAMS
    if [[ "${want_planar}" == true ]]; then
        solvent_num=$((${solvent_num}+${solvent_num}))
    fi

	## DEFINING COSOLVENT NAME
    ## SEEING IF INDEX IS THE LAST ONE (WATER)
    if [[ "${index}" != "$((${#molar_volume_array[@]}-1))" ]]; then
    	## DEFINING COSOLVENT NAME
		cosolvent=${cosolvent_name_array[index]}

		## ADDING PARAMETERS AND ITP
	    ## PATH TO INPUT MOLECULE
	    path_input_molecule="${PREP_SOLVENT_FINAL}/${cosolvent}"
	    ## DEFINING MOLECULE GRO, ITP, PRM FILES
	    molecule_itp_file="${cosolvent}.itp"
	    molecule_prm_file="${cosolvent}.prm"
	    ## COPYING ITP FILES
	    cp -r "${path_input_molecule}/"{${molecule_itp_file},${molecule_prm_file}} "${path_output_folder}" 

	    ## EDITING GTOP FILE TO INCLUDE PRMS
		topology_add_include_specific "${input_top}" "${top_search_keys_prm}" "#include \"${cosolvent}.prm\""
	    topology_add_include_specific "${input_top}" "${top_search_keys_itp}" "#include \"${cosolvent}.itp\""

	    ## REPLACING PRM AND ITP KEYS
	    top_search_keys_prm="${cosolvent}.prm"
	    top_search_keys_itp="${cosolvent}.itp"

	    echo "Including ${cosolvent} .prm and .itp to ${input_top}"
	fi
	## ADDING TO TOPOLOGY
	echo "   ${solvent_res_name} ${solvent_num}" >> "${input_top}"

done

## MDP FILES
if [[ "${want_planar}" == false ]]; then
    mdp_parent_path="${INPUT_MDP_PATH}/charmm36/Spherical"
    em_mdp_file="minim_sam_gmx5.mdp"
    equil_mdp_file1="npt_double_equil_gmx5_charmm36.mdp"
    equil_mdp_file2="nvt_mixed_solvent_frozen_gnp.mdp"
    prod_mdp_file="npt_double_prod_gmx5_charmm36_10ps.mdp"
else
    mdp_parent_path="${INPUT_MDP_PATH}/charmm36/Planar"
    em_mdp_file="minim_sam_gmx5.mdp"
    equil_mdp_file1="npt_double_equil_gmx5_charmm36.mdp"
    equil_mdp_file2="nvt_mixed_solvent_frozen_gnp.mdp"
    prod_mdp_file="npt_double_prod_gmx5_charmm36.mdp"
fi

## DEFINING DEBUG
want_debug=false
# true

## DEFINING STEPS
equil_mdp_file1_nsteps="1000000" # 2 ns NPT equil
equil_mdp_file2_nsteps="2000000" # 4 ns NVT temperature anneal
prod_mdp_file_nsteps="6000000" # 12 ns NPT production run

if [[ "${want_debug}" == true ]]; then
	echo "DEBUGGING IS TURNED ON, MDP FILE TIMES MAY NOT BE CORRECT!"
	sleep 3
	equil_mdp_file1_nsteps="1000" 
	equil_mdp_file2_nsteps="1000" 
	prod_mdp_file_nsteps="1000" 
fi
## COPYING ALL MDP FILES
cp -r "${mdp_parent_path}/"{${em_mdp_file},${equil_mdp_file1},${equil_mdp_file2},${prod_mdp_file}} "${path_output_folder}" 

if [[ "${want_planar}" == false ]]; then
    nsteps_var="NSTEPS"
    temp_var="TEMPERATURE"
else
    nsteps_var="_NSTEPS_"
    temp_var="_TEMPERATURE_"
fi

## EQUIL 1
sed -i "s/${nsteps_var}/${equil_mdp_file1_nsteps}/g" ${equil_mdp_file1}
sed -i "s/${temp_var}/${temp}/g" ${equil_mdp_file1}

## DEFINING FREEZE GROUP
freeze_group="GNP_LIGANDS"

## EQUIL 2
sed -i "s/NSTEPS/${equil_mdp_file2_nsteps}/g" ${equil_mdp_file2}
sed -i "s/TEMPERATURE/${temp}/g" ${equil_mdp_file2}
sed -i "s/_FREEZEGROUP_/${freeze_group}/g" ${equil_mdp_file2}

## PRODUCTION
sed -i "s/${nsteps_var}/${prod_mdp_file_nsteps}/g" ${prod_mdp_file}
sed -i "s/${temp_var}/${temp}/g" ${prod_mdp_file}

## ADDING TEMPERATURE ANNEAL
echo "
; ANNEALING TEMPERATURE
annealing   = single ; single set of temperature points
annealing-npoints = 5 ; all groups that are not annealed
annealing-time = 0 1000 2000 3000 4000; 0 ns, 1 ns, 2 ns, 3 ns
annealing-temp = 300 600 600 300 300; 300 K to 600 K, stay at 600 K, then back to 300 K
" >> "${equil_mdp_file2}"

## SLEEPING
sleep "${sleep_time}"

if [[ "${want_planar}" == true ]]; then

    ###################################################
    ### STEP 4a: CORRECTING NOMENCLATURE FOR GRO/TOP ###
    ###################################################

    echo -e "\n---------------------------------------"
    echo "--- STEP 4a: Correcting gro and top  ---"
    echo "---------------------------------------"

    ## DEFINING PYTHON PATH
    py_correct_gro="${MDBUILDER}/core/fix_residues_gro_top.py"

    ## DEFINING CORRECTED OUTPUTS
    combine_gro_correct="${output_prefix}_solvated_correct.gro"
    combine_top_correct="${output_prefix}.top" 

    ## DEFINING RESIDUE NAMES TO FIX (TIP3 -> SOL)
    res_resname=""
    # "${lipid_mem_water_res_name},${water_res_name}"
    ## COMBINING RESIDUE INTO A SINGLE ONE
    combined_residues=""
    # "${ligand_residue_name},${np_center_group},${np_residue_name}"

    ## RUNNING PYTHON CODE TO FIX GRO
    python3.6 "${py_correct_gro}" --ifold "${path_output_folder}" \
                                  --igro "${initial_gro_file}" \
                                  --itop "${input_top}" \
                                  --ogro "${combine_gro_correct}" \
                                  --otop "${combine_top_correct}" 

    ## DEFINING NEW TOPTOP
    output_top="${combine_top_correct}"

    ## PRINTING
    echo "Correcting GRO and TOP file based on residue name"
    echo "Corrected gro: ${combine_gro_correct}"
    echo "Corrected top: ${combine_top_correct}"

    ## SLEEPING
    sleep "${sleep_time}"

    ## GENCONF TO CORRECT RESIDUE NUMBERING
    renumbered_gro="${combine_gro_correct%.gro}_renumbered.gro"
    echo "Using genconf to correct residue numbers"
    gmx genconf -f ${combine_gro_correct} -o "${renumbered_gro}" -renumber  >/dev/null 2>&1

    ## CLEANING DIRECTORY
    stop_if_does_not_exist "${renumbered_gro}"
    mv "${combine_gro_correct}" "${renumbered_gro}" "${initial_gro_file}" setup_files

    ## REDEFINING INPUT
    mv "setup_files/${renumbered_gro}" "${initial_gro_file}"

fi

## CLEANING DIRECTORY
rm -f \#* || true

##################################
## PART 5: ENERGY MINIMIZATION ###
##################################

echo ""
echo "==========================================="
echo "Part 5 - Energy minimization"
echo "==========================================="

## GROMPP
gmx grompp -f "${em_mdp_file}" \
		   -c "${initial_gro_file}" \
		   -p "${input_top}" \
		   -o "${output_prefix}_em" -maxwarn 5


## ENERGY MINIMIZING
gmx mdrun -nt 10 -v -deffnm "${output_prefix}_em" 

## DEFINING INDEX FILE
index_file="${output_prefix}.ndx"

## CREATING INDEX FOR OUTPUT
read -a lig_res_name_array <<< "$(str2array_by_delim "${ligand_res_names}" ",")"
residue_name_spaces="$(join_array_to_string " " ${lig_res_name_array[@]})"

## CREATING INDEX FILE
make_ndx_gold_with_ligand "${path_output_folder}/${output_prefix}_em.tpr" \
                          "${path_output_folder}/${index_file}"   \
                          "${residue_name_spaces}"   \
                          "${gold_residue_name}" 

## SLEEPING
sleep "${sleep_time}"

#################################
### PART 6: SUBMISSION SCRIPT ###
#################################

echo ""
echo "==========================================="
echo "Part 6 - Generating run scripts"
echo "==========================================="

## DEFINING OUTPUT
output_run="run.sh"
output_submit="submit.sh"

## DEFAULTS FOR SUBMIT
num_cores="28"
mdrun_command="gmx mdrun -nt"
mdrun_command_suffix=""
gromacs_command="gmx"

## DEFINING MAX WARN
maxwarn="5"

## COPYING RC
cp -r "${PATH_GENERAL_RC}" "${path_output_folder}"

## DEFINING SUBMISSION PATH
run_file="run_cosolvent_mapping_sims.sh"
submit_file="submit_run_cosolvent_mapping_sims.sh"
path_run_file="${PATH2SUBMISSION}/${run_file}"
path_submit_file="${PATH2SUBMISSION}/${submit_file}"

## COPYING
cp -r "${path_run_file}" "${path_output_folder}/${output_run}"
cp -r "${path_submit_file}" "${path_output_folder}/${output_submit}"

## EDITING RUN SCRIPT
sed -i "s/_RCFILE_/${GENERAL_RC_NAME}/g" "${output_run}"
sed -i "s/_INPUTTOPFILE_/${input_top}/g" "${output_run}"
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" "${output_run}"
sed -i "s/_MAXWARN_/${maxwarn}/g" "${output_run}"

sed -i "s/_EQUIL1_MDPFILE_/${equil_mdp_file1}/g" "${output_run}"
sed -i "s/_EQUIL2_MDPFILE_/${equil_mdp_file2}/g" "${output_run}"
sed -i "s/_PROD_MDPFILE_/${prod_mdp_file}/g" "${output_run}"

sed -i "s/_INDEXFILE_/${index_file}/g" "${output_run}"
sed -i "s/_INITIALPREFIX_/${output_prefix}_em/g" "${output_run}"

## EDITTING SUBMISSION SCRIPT
sed -i "s/_JOB_NAME_/${output_folder}/g" "${output_submit}"
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${output_submit}"
sed -i "s/_MDRUNCOMMAND_/${mdrun_command}/g" "${output_submit}"
sed -i "s/_MDRUNCOMMANDSUFFIX_/${mdrun_command_suffix}/g" "${output_submit}"
sed -i "s/_GROMACSCOMMAND_/${gromacs_command}/g" "${output_submit}"
sed -i "s/_RUNSCRIPT_/${output_run}/g" "${output_submit}"

## ADDING SUBMISSION SCRIPT TO JOB LIST
echo "${path_output_folder}/${output_submit}" >> "${JOB_SCRIPT}"
echo "Adding job to job script!"
echo "Path: ${path_output_folder}/${output_submit}"
echo "You should be good to go! Good luck!"

## SLEEPING
sleep "${sleep_time}"

## CREATING SUMMARY FILE
summary_file="transfer_summary.txt"

if [[ "${want_planar}" == true ]]; then

echo "INPUT SIM PATH: ${input_sim_path}
COSOLVENT NAMES: ${cosolvent_names}
COSOLVENT PERC: ${cosolvent_perc}
TRIAL NUM: ${trial_num}
PARENT DIR: ${parent_outputdir}
------------
DIST TO EDGE: ${dist_to_edge}
COSOLVENT RESNAMES: ${cosolvent_residue_name_commas}
COSOLVENT MOLAR VOLUMES (nm3): ${cosolvent_molecular_volumes[@]}
------------
NP VOLUME (nm3): ${np_volume}
BOX VOLUME (nm3): ${box_volume} 
AVAIALBLE VOLUME (nm3): ${available_volume}
------------
MOLAR VOLUME OF WATER (nm3): ${mol_volume_water}
MOLE FRACTION ARRAY: ${mole_frac_array[@]}
TOTAL SOLVENT MOLECULES: ${total_molecules}
NUM SOLVENTS ARRAY: ${num_molecules_array[@]}
------------
GOLD_NAME: ${gold_residue_name}
LIG_NAME: ${ligand_names}
ligand_res_names: ${ligand_res_names}
" > "${summary_file}"

else

echo "INPUT SIM PATH: ${input_sim_path}
COSOLVENT NAMES: ${cosolvent_names}
COSOLVENT PERC: ${cosolvent_perc}
TRIAL NUM: ${trial_num}
PARENT DIR: ${parent_outputdir}
------------
DIST TO EDGE: ${dist_to_edge}
COSOLVENT RESNAMES: ${cosolvent_residue_name_commas}
COSOLVENT MOLAR VOLUMES (nm3): ${cosolvent_molecular_volumes[@]}
------------
DISTANCE Z PLANAR SOLVENT: ${dist_z_planar}
AVAIALBLE VOLUME (nm3): ${available_volume}
------------
MOLAR VOLUME OF WATER (nm3): ${mol_volume_water}
MOLE FRACTION ARRAY: ${mole_frac_array[@]}
TOTAL SOLVENT MOLECULES: ${total_molecules}
NUM SOLVENTS ARRAY: ${num_molecules_array[@]}
------------
GOLD_NAME: ${gold_residue_name}
LIG_NAME: ${ligand_names}
ligand_res_names: ${ligand_res_names}
" > "${summary_file}"

fi

echo "========="
cat "${summary_file}"
echo "========="

echo "Summary file is available: ${summary_file}"

