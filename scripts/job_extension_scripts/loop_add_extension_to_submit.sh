#!/bin/bash
# loop_add_extension_to_submit.sh
# This code adds an extension to the submit.sh file 
# to account for submission scripts
# Run code
#	bash loop_add_extension_to_submit.sh

## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../../bin/${global_file_name}"

echo "Extension script running"

## DEFINING LOCATION TO ADD
# location_to_add="/home/akchew/scratch/nanoparticle_project/simulations/NP_HYDRO_SI_GNP_DEBUGGING_SPRING"
location_to_add="/home/akchew/scratch/nanoparticle_project/simulations/NP_HYDRO_SI_PLANAR_SAMS_DEBUGGING_SPRING"

## TRUE IF YOU WANT TO ADD TO JOB LIST
add_to_job_list=true

## DEFINING PRODUCTION EXTENSION CODE
prod_ext_code="production_extension.sh"

## PATH TO EXT CODE
path_ext_code="${main_dir}/${prod_ext_code}"

## KEY TO LOOK FOR
prod_key_to_search="PRODUCTION EXTENSION"

## DEFINING SUBMISSION SCRIPT
input_submit="submit.sh"
# "orig-submit.sh"
# "submit.sh"

## LOOPING
read -a location_list <<< $(ls ${location_to_add})

echo "Directories to work on:"
echo "======================="
## PRINTING
for each_loc in "${location_list[@]}"; do
	echo "   ${each_loc}"
done
echo "======================="

## LOOPING
for each_loc in "${location_list[@]}"; do
	echo "-----------------------"
	echo "Working on: ${each_loc}"
	## DEFINIGN SCRIPT PATH
	loc_path="${location_to_add}/${each_loc}/${input_submit}"

	## CHECKING IF PATH EXISTS
	if [ ! -e "${loc_path}" ]; then
		echo "Error! Path does not exist."
		echo "Check: ${loc_path}"
		exit 1
	fi

	if [[ -z "$(grep "${prod_key_to_search}" ${loc_path})" ]]; then
		echo "Adding extension code";
		cat "${path_ext_code}" >> "${loc_path}"
	else
		echo "Submission file already has the code!"

	fi
	## ADDING TO JOB LIST
	if [[ "${add_to_job_list}" == true ]]; then
		echo "Adding to job list!"
		echo "${loc_path}" >> "${JOB_SCRIPT}"
	fi

done
