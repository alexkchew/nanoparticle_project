############################
### PRODUCTION EXTENSION ###
############################

### FUNCTION TO CHECK XTC TIME
check_xtc_time () 
{ 
    ## DEFINING LOCAL VARIABLE
    local __xtctime="${1:xtc_time}";
    prefix_="${2-sam_prod}";
    gromacs_command="${3-gmx}"

    temp_file_="${prefix_}_temp.txt";

    ${gromacs_command} check -f "${prefix_}".cpt 2> "${temp_file_}";

    last_frame=$(grep "Last frame" "${temp_file_}" | awk '{print $NF}');
    
    eval $__xtctime="${last_frame}";
    if [ -e "${temp_file_}" ]; then
        rm "${temp_file_}";
    fi
}

## DEFINING SIMULATION TIME
total_prod_time_ps="12000.000"
# "10000.000" # ps

## CHECKING SIM TIME
check_xtc_time "prod_xtc_time" "${new_prefix}" gmx

## CHECKING IF EQUALITY
if [[ "${prod_xtc_time}" != "${total_prod_time_ps}" ]]; then

  ## CONVERTING TPR
  gmx convert-tpr -s "${new_prefix}.tpr" \
                  -until "${total_prod_time_ps}" \
                  -o "${new_prefix}.tpr"

  ## RESTARTING
  gmx mdrun -nt "${num_cores}" -v \
                                   -s "${new_prefix}.tpr" \
                                   -cpi "${new_prefix}.cpt" \
                                   -append -deffnm "${new_prefix}"
fi
