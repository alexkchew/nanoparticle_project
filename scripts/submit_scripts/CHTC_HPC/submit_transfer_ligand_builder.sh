#SBATCH --partition=univ,univ2		# default "univ", if not specified
#SBATCH --time=3-00:00:00		# run time in days-hh:mm:ss
#SBATCH --nodes=1			# require 1 nodes
#SBATCH --ntasks-per-node=16            # (by default, "ntasks"="cpus")
#SBATCH --mem-per-cpu=4000		# RAM per CPU core, in MB (default 4 GB/core)
#SBATCH --ntasks=16        # total number of "tasks" (cores) requested
#SBATCH --cpus-per-task=1  # default "1" if not specified
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J _JOB_NAME_

## DEFINING NUMBER OF CORES
num_cores="16"

echo "----- LOADING GROMACS ------"
## LOADING GROMACS COMMANDS
aci_load_gromacs_mpi

## CREATING ALIAS FOR GMX COMMANDS
alias gmx="gmx_mpi"

