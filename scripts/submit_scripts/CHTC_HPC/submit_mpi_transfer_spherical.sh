#!/bin/bash 
# submit_mpi_transfer_spherical.sh
# CHTC_HPC Submit Script (submit_mpi_transfer_planar.sh)
# This script submits for large jobs (i.e. multiple nodes) for transfer planar submit script

#SBATCH --partition=univ2		# default "univ", if not specified
#SBATCH --time=5-00:00:00		# run time in days-hh:mm:ss -- MAX IS 7 DAYS
#SBATCH --nodes=10		        # require 1 nodes
#SBATCH --ntasks-per-node=20    # (by default, "ntasks"="cpus")
#SBATCH --mem-per-cpu=4000		# RAM per CPU core, in MB (default 4 GB/core)
#SBATCH --ntasks=40        # total number of "tasks" (cores) requested
#SBATCH --cpus-per-task=1  # default "1" if not specified
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J JOB_NAME

#Now list your executable command (or a string of them).

## LOADING GROMACS 2016 MPI VERSION
general_gromacs_load gromacs-2016 mpi

# EXECUTABLE COMMANDS
# Number of threads
NumberOfThreads="200"

## INPUT FILES ##
gro_file="sam.gro"
input_top="sam.top"
output_prefix="sam"

## MDP FILES ##
em_mdp="minim_sam_gmx5.mdp"
equil_mdp="npt_double_equil_gmx5_charmm36.mdp"
prod_mdp="npt_double_prod_gmx5_charmm36.mdp"

## CONVERTING TPR
gmx_mpi convert-tpr -s ${output_prefix}_prod.tpr -until 200000 -o ${output_prefix}_prod.tpr

## PRODUCT EXTENSION
mpirun -np ${NumberOfThreads} gmx_mpi mdrun -v -s ${output_prefix}_prod.tpr -cpi ${output_prefix}_prod.cpt -append -deffnm ${output_prefix}_prod
