#!/bin/bash 
# CHTC_HPC Submit Script (submit.sh)

#SBATCH --partition=univ2,univ		# default "univ", if not specified
#SBATCH --time=5-00:00:00		# run time in days-hh:mm:ss
#SBATCH --nodes=1			# require 1 nodes
#SBATCH --ntasks-per-node=16            # (by default, "ntasks"="cpus")
#SBATCH --mem-per-cpu=4000		# RAM per CPU core, in MB (default 4 GB/core)
#SBATCH --ntasks=16        # total number of "tasks" (cores) requested
#SBATCH --cpus-per-task=1  # default "1" if not specified
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J JOB_NAME

#Now list your executable command (or a string of them).
name="sam"
forcefield_suffix=FORCE_FIELD_SUFFIX # charmm36 or opls

gmx grompp -f npt_double_equil_gmx5_${forcefield_suffix}.mdp -o ${name}_equil.tpr -c ${name}_em.gro -p ${name}.top
gmx mdrun -nt 16 -v -deffnm ${name}_equil

gmx grompp -f npt_double_prod_gmx5_${forcefield_suffix}.mdp -o ${name}_prod.tpr -c ${name}_equil.gro -p ${name}.top
gmx mdrun -nt 16 -v -deffnm ${name}_prod
