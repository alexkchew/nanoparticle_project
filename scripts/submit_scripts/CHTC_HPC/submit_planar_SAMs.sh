#!/bin/bash 
# submit_planar_SAMs.sh
# This script is for planar SAMs, going through multiple NVT/NPT ensembles
# CHTC_HPC Submit Script (submit.sh)

#SBATCH --partition=univ2		# default "univ", if not specified
#SBATCH --time=5-00:00:00		# run time in days-hh:mm:ss
#SBATCH --nodes=1			# require 1 nodes
#SBATCH --ntasks-per-node=20            # (by default, "ntasks"="cpus")
#SBATCH --mem-per-cpu=4000		# RAM per CPU core, in MB (default 4 GB/core)
#SBATCH --ntasks=20        # total number of "tasks" (cores) requested
#SBATCH --cpus-per-task=1  # default "1" if not specified
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J JOB_NAME

#Now list your executable command (or a string of them).
### LIGANDBUILDER FUNCTIONS
path2ligand_builder="/home/akchew/scratch/nanoparticle_project/bin/ligand_builder_rc.sh"
source "${path2ligand_builder}"

## DEFINING NUMBER OF CORES
num_cores="20"

### INPUT VARIABLES
## DEFINING NAME
output_file_name="sam"

# POSITION RESTRAINT FILE NAME
position_restraint="posre.itp"

# INCLUSION OF INDEX FILE
index_file="index.ndx"

## MDP FILES
# FROZEN LIGAND RUNS
nvt_equil_frozen_ligand_mdp="nvt_double_equil_gmx5_freeze_lig_charmm36.mdp"
nvt_equil_frozen_water_ion_mdp="nvt_double_equil_gmx5_freeze_water_ion_charmm36.mdp"

# EQUIL AND PRODUCTION RUNS
equil_mdp_file_name="npt_double_equil_gmx5_charmm36.mdp"
prod_mdp_file_name="npt_double_prod_gmx5_charmm36.mdp"

### NVT EQUILIBRATION
## FROZEN LIGAND RUNS
gmx grompp -f ${nvt_equil_frozen_ligand_mdp} -o ${output_file_name}_equil_nvt_frozen_lig.tpr -c ${output_file_name}_em.gro -p ${output_file_name}.top -n ${index_file} -maxwarn 5
gmx mdrun -nt "${num_cores}" -v -deffnm ${output_file_name}_equil_nvt_frozen_lig

## NPT EQUILIBRATION
## FROZEN WATER / ION RUNS
gmx grompp -f ${nvt_equil_frozen_water_ion_mdp} -o ${output_file_name}_equil_nvt_frozen_solvent.tpr -c ${output_file_name}_equil_nvt_frozen_lig.gro -p ${output_file_name}.top -maxwarn 5
gmx mdrun -nt "${num_cores}" -v -deffnm ${output_file_name}_equil_nvt_frozen_solvent

### EQUILIBRATION RUN
gmx grompp -f ${equil_mdp_file_name} -o ${output_file_name}_equil_npt.tpr -c ${output_file_name}_equil_nvt_frozen_solvent.gro -p ${output_file_name}.top -maxwarn 5
gmx mdrun -nt "${num_cores}"  -v -deffnm ${output_file_name}_equil_npt

#### PRODUCTION RUN
gmx grompp -f ${prod_mdp_file_name} -o ${output_file_name}_prod.tpr -c ${output_file_name}_equil_npt.gro -p ${output_file_name}.top -maxwarn 5
gmx mdrun -nt "${num_cores}"  -v -deffnm ${output_file_name}_prod
