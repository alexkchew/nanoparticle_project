#!/bin/bash 

#SBATCH --partition=univ		# default "univ", if not specified
#SBATCH --time=0-00:05:00		# run time in days-hh:mm:ss -- MAX IS 7 DAYS
#SBATCH --nodes=2		        # require 1 nodes
#SBATCH --ntasks-per-node=16    # (by default, "ntasks"="cpus")
#SBATCH --mem-per-cpu=4000		# RAM per CPU core, in MB (default 4 GB/core)
#SBATCH --cpus-per-task=1  # default "1" if not specified
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J testing

# ---- RUN SCRIPTS BELOW ----- #

# LOADING GROMACS
echo "----- LOADING GROMACS ------"
source "/home/akchew/local_installs/gromacs_2016.6_plumed_2.5.1/bin/GMXRC"

## RUNNING VERSION
echo "Printing version"
gmx_mpi --version
