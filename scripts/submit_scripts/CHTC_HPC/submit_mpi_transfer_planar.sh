#!/bin/bash 
# CHTC_HPC Submit Script (submit_mpi_transfer_planar.sh)
# This script submits for large jobs (i.e. multiple nodes) for transfer planar submit script

#SBATCH --partition=univ2		# default "univ", if not specified
#SBATCH --time=5-00:00:00		# run time in days-hh:mm:ss -- MAX IS 7 DAYS
#SBATCH --nodes=2		        # require 1 nodes
#SBATCH --ntasks-per-node=20    # (by default, "ntasks"="cpus")
#SBATCH --mem-per-cpu=4000		# RAM per CPU core, in MB (default 4 GB/core)
#SBATCH --ntasks=40        # total number of "tasks" (cores) requested
#SBATCH --cpus-per-task=1  # default "1" if not specified
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J JOB_NAME

#Now list your executable command (or a string of them).

## LOADING GROMACS 2016 MPI VERSION
general_gromacs_load gromacs-2016 mpi

NumThreads="40"

### LIGANDBUILDER FUNCTIONS
path2ligand_builder="/home/akchew/scratch/LigandBuilder/Scripts/bin/ligand_builder_rc.sh"
source "${path2ligand_builder}"

### INPUT VARIABLES
## DEFINING NAME
output_file_name="sam"

# POSITION RESTRAINT FILE NAME
position_restraint="posre.itp"

## MDP FILES
# FROZEN LIGAND RUNS
nvt_equil_frozen_ligand_mdp="nvt_double_equil_gmx5_freeze_lig_charmm36.mdp"
npt_equil_frozen_ligand_mdp="npt_double_equil_gmx5_freeze_lig_charmm36.mdp"

# EQUIL AND PRODUCTION RUNS
equil_mdp_file_name="npt_double_equil_gmx5_charmm36.mdp"
prod_mdp_file_name="npt_double_prod_gmx5_charmm36.mdp"

### FROZEN LIGAND RUNS
## NVT EQUILIBRATION
gmx_mpi grompp -f ${nvt_equil_frozen_ligand_mdp} -o ${output_file_name}_nvt_equil.tpr -c ${output_file_name}_em.gro -p ${output_file_name}.top -maxwarn 5
mpirun -np "${NumThreads}" gmx_mpi mdrun -v -deffnm ${output_file_name}_nvt_equil

## NPT EQUILIBRATION
# RUNNING SHORT SIMULATION
gmx_mpi grompp -f ${npt_equil_frozen_ligand_mdp} -o ${output_file_name}_npt_equil.tpr -c ${output_file_name}_nvt_equil.gro -p ${output_file_name}.top -maxwarn 5
mpirun -np "${NumThreads}" gmx_mpi mdrun -v -deffnm ${output_file_name}_npt_equil

### EQUILIBRATION RUN
gmx_mpi grompp -f ${equil_mdp_file_name} -o ${output_file_name}_equil.tpr -c ${output_file_name}_npt_equil.gro -p ${output_file_name}.top -maxwarn 5
mpirun -np "${NumThreads}" gmx_mpi mdrun -v -deffnm ${output_file_name}_equil

#### PRODUCTION RUN
gmx_mpi grompp -f ${prod_mdp_file_name} -o ${output_file_name}_prod.tpr -c ${output_file_name}_equil.gro -p ${output_file_name}.top -maxwarn 5
mpirun -np "${NumThreads}" gmx_mpi mdrun  -v -deffnm ${output_file_name}_prod



## PREVIOUSLY ---- FOR SMALL NUMBER OF JOBS
#gmx grompp -f npt_double_equil_gmx5_charmm36.mdp -o ${name}_equil.tpr -c ${name}_em.gro -p ${name}.top -maxwarn 5
#gmx mdrun -nt 28  -v -deffnm ${name}_equil
#
#gmx grompp -f npt_double_prod_gmx5_charmm36.mdp -o ${name}_prod.tpr -c ${name}_equil.gro -p ${name}.top -maxwarn 5
#gmx mdrun -nt 28  -v -deffnm ${name}_prod
