#!/bin/bash 
# CHTC_HPC Submit Script (submit_mpi.sh)
# This script submits for large jobs (i.e. multiple nodes)

#SBATCH --partition=univ2		# default "univ", if not specified
#SBATCH --time=5-00:00:00		# run time in days-hh:mm:ss -- MAX IS 7 DAYS
#SBATCH --nodes=8		        # require 1 nodes
#SBATCH --ntasks-per-node=20    # (by default, "ntasks"="cpus")
#SBATCH --mem-per-cpu=4000		# RAM per CPU core, in MB (default 4 GB/core)
#SBATCH --ntasks=160        # total number of "tasks" (cores) requested
#SBATCH --cpus-per-task=1  # default "1" if not specified
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J JOB_NAME

#Now list your executable command (or a string of them).
name="sam"
forcefield_suffix=charmm36 # charmm36 or opls

## LOADING GROMACS 2016 MPI VERSION
general_gromacs_load gromacs-2016 mpi

## CREATING EQUILBRATION FILE
gmx_mpi grompp -f npt_double_equil_gmx5_${forcefield_suffix}.mdp -o ${name}_equil.tpr -c ${name}_em.gro -p ${name}.top -maxwarn 5
mpirun -np 160 gmx_mpi mdrun -v -deffnm ${name}_equil

## CREATING PRODUCTION FILE
gmx_mpi grompp -f npt_double_prod_gmx5_${forcefield_suffix}.mdp -o ${name}_prod.tpr -c ${name}_equil.gro -p ${name}.top -maxwarn 5

## RUNNING PRODUCTION
mpirun -np 160 gmx_mpi mdrun -v -deffnm ${name}_prod

## PREVIOUSLY ---- FOR SMALL NUMBER OF JOBS
#gmx grompp -f npt_double_equil_gmx5_charmm36.mdp -o ${name}_equil.tpr -c ${name}_em.gro -p ${name}.top -maxwarn 5
#gmx mdrun -nt 28  -v -deffnm ${name}_equil
#
#gmx grompp -f npt_double_prod_gmx5_charmm36.mdp -o ${name}_prod.tpr -c ${name}_equil.gro -p ${name}.top -maxwarn 5
#gmx mdrun -nt 28  -v -deffnm ${name}_prod
