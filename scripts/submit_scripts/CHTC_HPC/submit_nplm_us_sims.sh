#SBATCH --partition=univ2		# default "univ", if not specified
#SBATCH --time=7-00:00:00		# run time in days-hh:mm:ss -- MAX IS 7 DAYS
#SBATCH --nodes=4		        # require 1 nodes
#SBATCH --ntasks-per-node=20    # (by default, "ntasks"="cpus")
#SBATCH --mem-per-cpu=4000		# RAM per CPU core, in MB (default 4 GB/core)
#SBATCH --cpus-per-task=1  # default "1" if not specified
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J _JOB_NAME_

# #SBATCH --nodes=16		        # require 1 nodes
# num_cores="320" # number of cores you want to run with

# ---- RUN SCRIPTS BELOW ----- #

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="80" # number of cores you want to run with
em_num_cores="${num_cores}" # number of cores to energy minimize
mdrun_command="mpirun -np " # command to run code
mdrun_command_suffix="gmx_mpi mdrun"
gromacs_command="gmx_mpi"

# LOADING GROMACS
echo "----- LOADING GROMACS ------"
aci_load_gromacs_mpi

