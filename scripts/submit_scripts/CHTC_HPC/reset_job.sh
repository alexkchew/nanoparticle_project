#!/bin/bash 
# CHTC_HPC Re-Submit Script (reset_job.sh)

#SBATCH --partition=univ2		# default "univ", if not specified
#SBATCH --time=WALLTIME		# run time in days-hh:mm:ss
#SBATCH --nodes=1			# require 1 nodes
#SBATCH --ntasks-per-node=NUMBEROFCPUS            # (by default, "ntasks"="cpus")
#SBATCH --mem-per-cpu=4000		# RAM per CPU core, in MB (default 4 GB/core)
#SBATCH --ntasks=NUMBEROFCPUS        # total number of "tasks" (cores) requested
#SBATCH --cpus-per-task=1  # default "1" if not specified
#SBATCH --mail-user=NETID@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J JOB_NAME

# Note for CHTC, GROMACS does not have to be loaded since the profile would technically load them (similar to SWARM)
# EXECUTABLE COMMANDS
