#!/bin/bash 
# COMET Submit Script (submit.sh)

#SBATCH --partition=compute
#SBATCH --job-name="JOB_NAME"
#SBATCH --output="slurm.%j.%N.out"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=24
#SBATCH --export=ALL
#SBATCH -t 48:00:00
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -A mit161
#SBATCH --no-requeue

# Defining number of threads
NumberOfThreads="24" # Number of threads

#Set the number of openmp threads
export OMP_NUM_THREADS=${NumberOfThreads}

#Now list your executable command (or a string of them).

# ---- RUN SCRIPTS BELOW ----- #

# LOADING GROMACS
echo "----- LOADING GROMACS ------"
module load gromacs/2016.3
source "${HOME}/gromacs_local_install/gromacs-2016/gromacs-thread/bin/GMXRC"; echo "Completed..."
echo "----- RUNNING GROMACS COMMANDS -----"

name=sam

gmx grompp -f npt_double_equil_gmx5_charmm36.mdp -o ${name}_equil.tpr -c ${name}_em.gro -p ${name}.top -maxwarn 5
gmx mdrun -ntmpi 1 -ntomp "${NumberOfThreads}"  -v -deffnm ${name}_equil

gmx grompp -f npt_double_prod_gmx5_charmm36.mdp -o ${name}_prod.tpr -c ${name}_equil.gro -p ${name}.top -maxwarn 5
gmx mdrun -ntmpi 1 -ntomp "${NumberOfThreads}"  -v -deffnm ${name}_prod