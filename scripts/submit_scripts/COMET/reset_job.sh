#!/bin/bash 
# COMET Re-Submit Script (reset_job.sh)

#SBATCH --partition=compute
#SBATCH --job-name="JOB_NAME"
#SBATCH --output="slurm.%j.%N.out"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=NUMBEROFCPUS
#SBATCH --export=ALL
#SBATCH -t WALLTIME
#SBATCH --mail-user=NETID@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -A mit155
#SBATCH --no-requeue

#Set the number of openmp threads
export OMP_NUM_THREADS=NUMBEROFCPUS

# LOADING GROMACS
echo "----- LOADING GROMACS ------"
source "${HOME}/gromacs_local_install/gromacs-2016/gromacs-thread/bin/GMXRC"; echo "Completed..."
echo "----- RUNNING GROMACS COMMANDS -----"

# EXECUTABLE COMMANDS
