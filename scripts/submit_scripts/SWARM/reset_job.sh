#!/bin/bash 
# SWARM Re-Submit Script (reset_job.sh)

#SBATCH -p compute
#SBATCH -t WALLTIME
#SBATCH --nodes=1
#SBATCH --ntasks=NUMBEROFCPUS        # total number of "tasks" (cores) requested
#SBATCH --ntasks-per-node=NUMBEROFCPUS              # total number of mpi tasks requested
#SBATCH --mail-user=NETID@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J JOB_NAME

# EXECUTABLE COMMANDS
