#!/bin/bash 
# SWARM Submit Script (submit.sh)

#SBATCH -p compute
#SBATCH -t 1000:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=28        # total number of "tasks" (cores) requested
#SBATCH --ntasks-per-node=28              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J JOB_NAME

#Now list your executable command (or a string of them).

# RUN SCRIPTS BELOW 
NumberOfThreads="28" # Number of threads

gro_file="mixed_solv.gro"
input_top="mixed_solv.top"
output_prefix="mixed_solv"

# Defining mdp scripts
em_mdp="minim_LIQ.mdp"
equil_mdp="equil_LIQ.mdp"
prod_mdp="production_LIQ.mdp"

if [ ! -e "${output_prefix}_equil.gro" ]; then

# Need to minimize and equilibrate

# MINIMIZE
gmx grompp -f ${em_mdp} -c ${gro_file} -p ${input_top} -o ${output_prefix}_em -maxwarn 1
gmx mdrun -v -nt ${NumberOfThreads} -deffnm ${output_prefix}_em

# EQUILIBRATION
gmx grompp -f ${equil_mdp} -c ${output_prefix}_em -p ${input_top} -o ${output_prefix}_equil -maxwarn 1
gmx mdrun -v -nt ${NumberOfThreads} -deffnm ${output_prefix}_equil

fi

# MD PRODUCTION
if [ ! -e "${output_prefix}_prod.tpr" ]; then
gmx grompp -f ${prod_mdp} -c ${output_prefix}_equil.gro -p ${input_top} -o ${output_prefix}_prod -maxwarn 1
fi

gmx mdrun -v -nt ${NumberOfThreads} -deffnm ${output_prefix}_prod

