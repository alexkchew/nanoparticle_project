#!/bin/bash 
# SWARM Submit Script (submit.sh)

#SBATCH -p compute
#SBATCH -t 1000:00:00
#SBATCH --nodes=1
#SBATCH --ntasks=28        # total number of "tasks" (cores) requested
#SBATCH --ntasks-per-node=28              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J EXTEND_JOB_NAME

#Now list your executable command (or a string of them).

# RUN SCRIPTS BELOW 
NumberOfThreads="28" # Number of threads

gro_file="mixed_solv.gro"
input_top="mixed_solv.top"
output_prefix="mixed_solv"

# Defining mdp scripts
em_mdp="minim_LIQ.mdp"
equil_mdp="equil_LIQ.mdp"
prod_mdp="production_LIQ.mdp"

if [ -e "${output_prefix}_prod.gro" ]; then

    # Copying over tpr file
    mv ${output_prefix}_prod.tpr ${output_prefix}_prod_saved.tpr
    gmx convert-tpr -s ${output_prefix}_prod_saved.tpr -extend 100000 -o ${output_prefix}_prod.tpr

    # Extending md simulations
    gmx mdrun -nt 28 -v -s ${output_prefix}_prod.tpr -cpi ${output_prefix}_prod.cpt -append -deffnm ${output_prefix}_prod

fi