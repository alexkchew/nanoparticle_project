#!/bin/bash 
# np_lipid_membrane_submit.sh
# STAMPEDE Submit Script (submit.sh)
# Need to change: 
#   _JOB_NAME_: job name
#   _RESNP_LIG_: NP res name

#SBATCH -p skx-normal
#SBATCH -J _JOB_NAME_
#SBATCH --output="slurm.%j.out"
#SBATCH --nodes=8
#SBATCH --ntasks-per-node=48
#SBATCH --export=ALL
#SBATCH -t 48:00:00
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -A TG-CTS170045

## DEFINING NUMBER OF CORES TO RUN THIS ON
total_num_cores="384"
# num_cores="68" # number of cores you want to run with

#Now list your executable command (or a string of them).

# NEED TO USE IBRUN TI LAUNCH MPI CODES ON TAAC SYSTEM (NOT MPIRUN OR MPIEXEC)

# ---- RUN SCRIPTS BELOW ----- #

# LOADING GROMACS
echo "----- LOADING GROMACS ------"
# module load gcc/7.1.0
# source "${HOME}/gromacs_local_install/gromacs-2016/gromacs-thread/bin/GMXRC"; echo "Completed..."
# source "${HOME}/gromacs_local_install/gromacs-2016/gromacs-mpi/bin/GMXRC"; echo "Completed..."
module load intel/18.0.2
module load impi/18.0.2
module load gromacs/2016.4

###SERVER_SPECIFIC_COMMANDS_END

## DEFINING INPUT FILES
output_prefix="combined_correct"

## DEFINING PULLING INFORMATION
mdp_input_pull="pulling.mdp"
mdp_output_pull="pulling_edited.mdp"

## DEFINING RESIDUES OF PULLING
res_pull_1="DOPC" # e.g. DOPC
res_np_gold="AUNP" # e.g. AUNP
res_np_lig="_RESNP_LIG_"  # e.g. RO1

## DEFINING SUBMIT FILES
np_to_lipid_bilayer_submit="pull_np_to_lipid_bilayer.sh"

## MDP FILES
em_mdp="em.mdp"
mdp_equil="equil_npt.mdp"
mdp_prod="prod_npt.mdp"

## ENERGY MINIMIZATION
# gmx grompp -f ${em_mdp} -c ${output_prefix}.gro -p ${output_prefix}.top -o "${output_prefix}_em.tpr"
# gmx mdrun -nt 1 -deffnm "${output_prefix}_em"

## NVT EQUILIBRATION
if [ -e "${output_prefix}_equil.xtc" ]; then
ibrun -np ${total_num_cores} mdrun_mpi -v -deffnm ${output_prefix}_equil -cpi ${output_prefix}_equil.cpt -s ${output_prefix}_equil.tpr -append
else
gmx grompp -f "${mdp_equil}" -o ${output_prefix}_equil.tpr -c ${output_prefix}_em.gro -p ${output_prefix}.top -maxwarn 5
ibrun -np ${total_num_cores} mdrun_mpi -v -deffnm ${output_prefix}_equil
fi

## RUNNING CODE TO PULL NP TO LIPID BILAYER
bash "${np_to_lipid_bilayer_submit}" "$(pwd)" "${output_prefix}_equil.gro" "${output_prefix}_equil.tpr" "${res_pull_1}" "${res_np_gold}" "${res_np_lig}" "${mdp_input_pull}" "${mdp_output_pull}"

## PULLING SIMULATION
gmx grompp -f "${mdp_output_pull}" -o ${output_prefix}_pull.tpr -c ${output_prefix}_equil.gro -p ${output_prefix}.top -maxwarn 5
ibrun -np ${total_num_cores} mdrun_mpi -v -deffnm ${output_prefix}_pull

## PRODUCTION RUN
gmx grompp -f "${mdp_prod}" -o ${output_prefix}_prod.tpr -c ${output_prefix}_pull.gro -p ${output_prefix}.top -maxwarn 5
ibrun -np ${total_num_cores} mdrun_mpi -v -deffnm ${output_prefix}_prod

