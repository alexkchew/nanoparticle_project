# submit_gnp_water_sims.sh
# This submission script for XSEDE stampede2 simulations with PLUMED

#SBATCH -p skx-normal
#SBATCH -J _JOB_NAME_
#SBATCH --output="slurm.%j.out"
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=48
#SBATCH --export=ALL
#SBATCH -t 45:00:00
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -A TG-CTS170045

# NEED TO USE IBRUN TI LAUNCH MPI CODES ON TAAC SYSTEM (NOT MPIRUN OR MPIEXEC)

## TYPICALLY 5 NODES, 48 TASKS, 240 CORES

# ---- RUN SCRIPTS BELOW ----- #

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="48" # number of cores you want to run with
mdrun_command="ibrun -np " # command to run code
mdrun_command_suffix="gmx_mpi mdrun"
gromacs_command="gmx_mpi"

# LOADING GROMACS
echo "----- LOADING GROMACS ------"

## LOADING GROMACS
# PATH TO INSTALLATION
sw_path="${HOME}/local_installs"
## DEFINING LIBRARIES
LIBRARY_PATH=$LIBRARY_PATH:"${sw_path}/plumed_2.5.1/lib"
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:"${sw_path}/plumed_2.5.1/lib"
DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:"${sw_path}/plumed_2.5.1/lib"
export PATH="${PATH}:${sw_path}/mpich_3.3.1/bin"
export PATH=$PATH:"${sw_path}/plumed_2.5.1/bin"
export PATH=$PATH:"${sw_path}/plumed_2.5.1/include"
export PATH="$PATH:${sw_path}/gromacs_2016.6_plumed_2.5.1/bin"
export PLUMED_KERNAL="${sw_path}/plumed_2.5.1/libplumedKernal.so"

## ENABLING GMX
source "${sw_path}/gromacs_2016.6_plumed_2.5.1_skx/bin/GMXRC"
