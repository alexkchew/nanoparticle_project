#!/bin/bash 
# STAMPEDE Re-Submit Script (reset_job.sh)
# Runs on two nodes

#SBATCH -p normal
#SBATCH -J JOB_NAME
#SBATCH --output="slurm.%j.out"
#SBATCH --nodes=2
#SBATCH -n NUMBEROFCPUS
#SBATCH --export=ALL
#SBATCH -t WALLTIME
#SBATCH --mail-user=NETID@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -A TG-ENG170011

#Set the number of openmp threads
# export OMP_NUM_THREADS=NUMBEROFCPUS

# LOADING GROMACS
echo "----- LOADING GROMACS ------"
#module load gcc/7.1.0
#source "${HOME}/gromacs_local_install/gromacs-2016/gromacs-thread/bin/GMXRC"; 
echo "Loading GROMACS on STAMPEDE (Using their modules)"
module restore system
module load intel/17.0.4
module load impi/17.0.3
module spider gromacs/2016.3
module load gromacs/2016.3

# Running to check version
gmx --version


echo "Completed..."

echo "----- RUNNING GROMACS COMMANDS -----"

### RUNNING GROMACS ###
