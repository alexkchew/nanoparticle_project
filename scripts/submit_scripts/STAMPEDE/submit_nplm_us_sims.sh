#SBATCH -p skx-normal
#SBATCH -J _JOB_NAME_
#SBATCH --output="slurm.%j.out"
#SBATCH --nodes=5
#SBATCH --ntasks-per-node=48
#SBATCH --export=ALL
#SBATCH -t 36:00:00
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -A TG-CTS170045

# NEED TO USE IBRUN TI LAUNCH MPI CODES ON TAAC SYSTEM (NOT MPIRUN OR MPIEXEC)

# ---- RUN SCRIPTS BELOW ----- #

## DEFINING NUMBER OF CORES TO RUN THIS ON
num_cores="240" # number of cores you want to run with
em_num_cores="${num_cores}" # number of cores to energy minimize
mdrun_command="ibrun -np " # command to run code
mdrun_command_suffix="mdrun_mpi"
gromacs_command="gmx"

# LOADING GROMACS
echo "----- LOADING GROMACS ------"

## LOADING GROMACS
module load intel/18.0.2
module load impi/18.0.2
module load gromacs/2016.4
