#!/bin/bash

# transfer_ligand_builder_mixed_solvent.sh
# This script transfers the self-assembly gold using new ligands with ligand builder + mixed solvent

# Written by Alex K. Chew (05/30/2019)

### VARIABLES:
# $1: diameter
# $2: temperature
# $3: shape_type (i.e. what shape would you like?)
# $4: ligand_names: name of the ligands
# $5: trial number
# $6: simulation directory
# $XXX: ligand_frac: fraction of ligands (DEFAULT 1 FOR NOW)

## USAGE: 
#   bash transfer_ligand_builder_mixed_solvent.sh 2 300.00 EAM butanethiol 1 test_dir
#   bash transfer_ligand_builder_mixed_solvent.sh 2 300.00 EAM dodecanethiol 1 test_dir
#   bash transfer_ligand_builder_mixed_solvent.sh 2 300.00 EAM C11OH 1 test_dir
#   bash transfer_ligand_builder_mixed_solvent.sh 2 300.00 EAM C11CONH2 1 test_dir
#   bash transfer_ligand_builder_mixed_solvent.sh 2 300.00 EAM C11COOH 1 test_dir
#   bash transfer_ligand_builder_mixed_solvent.sh 2 300.00 EAM C11CF3 1 test_dir
#   bash transfer_ligand_builder_mixed_solvent.sh 2 300.00 EAM C11OH 1 190627-mixed-solvent-leucine l-leucine 50
#   bash transfer_ligand_builder_mixed_solvent.sh 2 300.00 EAM C11OH 1 190628-mixed-solvent dioxane 50

## NOTE: If you are having LINCS warning -- you can update the transfer_ligand.py script and change the transition to half boxlength to outside the box. This serves as a good way of testing your NP bonding. So far, 50,000 does not have errors.

# ** UPDATES **
#   180223 - Added binding gold nanoparticles to sulfur atom script
#   180508 - Added directory functionality

### SOURCING ALL VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

# Getting script name and echoing
print_script_name

### --- USER-SPECIFIC PARAMETERS (BEGIN) --- ###

### INPUT VARIABLES
## FORCE FIELD
forcefield="${FORCEFIELD}" # Force Field (opls-aa or charmm36-nov2016.ff)

## DIAMETER
diameter="$1" # 2 nm

## TEMPERATURE
temp="$2" # "310.15" temperature in K of the system

## DEFINING SHAPE
shape_type="$3"
    # spherical: Spherical shape cutoff from {111} fcc lattice
    # hollow: Hollow spherical shape
    # EAM: embedded atom potential

## DEFINING DESIRED TRIAL
trial_num="$5"

## LIGAND PARAMETERS
ligand_names="$4" # "butanethiol" # name of ligand from .lig file
ligand_fracs="1" # fraction of ligands

## DEFINING SIMULATION DIRECTORY
simulation_dir="$6"

## DEFINING COSOLVENT
cosolvent="$7" # "methanol"

## DEFINING VOLUME FRACTION
vol_fraction_water_perc="$8" # "60" # percentage

## FINDING FRACTION
vol_fraction_water_decimals=$(awk -v vol=${vol_fraction_water_perc} 'BEGIN{ printf "%.3f",vol/100}') # nms

## CALCULATING RADIUS
radius=$(awk -v diam=$diameter 'BEGIN{ printf "%.5f",diam/2}') # nms
dist_to_edge="1" # nm distance from edge of box

## PREPARATION GOLD
prep_gold_folder="${PREP_GOLD_SIM}"
input_gold_folder="${shape_type}/${shape_type}_${diameter}_nmDIAM_300_K_2_nmEDGE_5_AREA-PER-LIG_4_nm_300_K_butanethiol_Trial_${trial_num}"
input_gold_gro="gold_ligand_equil.gro"

## PYTHON SCRIPT
transfer_python_script="transfer_ligand_builder.py"
bind_gold_sulfur_script="bind_nanoparticles.py"
py_transfer_script="${MDBUILDER}/builder/${transfer_python_script}"
py_bind_gold_sulfur_script="${MDBUILDER}/applications/nanoparticle/${bind_gold_sulfur_script}"

## REMOVING SCRIPT
py_remove_atoms_file="${MDBUILDER}/math/remove_atoms.py" # Removes residues within the center core (for hollow shell geometry)

## MDP FILES
mdp_em="minim_sam_gmx5.mdp" # Energy minimization mdp script
mdp_equil="npt_double_equil_gmx5_charmm36.mdp" # Equilibration mdp script
mdp_prod="npt_double_prod_gmx5_charmm36.mdp" # Production mdp script
mdp_dir="$INPUT_DIR/MDP_FILES/charmm36"

## CHECKING MDP FOLDER
if [ "${shape_type}" == "spherical" ] || [ "${shape_type}" == "hollow" ] || [ "${shape_type}" == "EAM" ]; then
    mdp_folder="Spherical"
fi
path2mdp="${mdp_dir}/${mdp_folder}"

## GROMPP OPTIONS
max_warn_index="5" # Number of times for maximum warnings

## JOB SCRIPT FILES
submitScript="submit.sh"

### SIMULATION PARAMETERS
## NUMBER OF STEPS FOR EQUILIBRATION AND PRODUCTION -- # ps / .002 timesteps
n_steps_equil="2500000" # 5 ns equilibration
n_steps_prod="25000000" # 50 ns production
# n_steps_prod="50000000" # 100 ns production
# n_steps_prod="50000000" # 5 ns prod (DEBUG) "50000000" # "25000000" # 50 ns production
# 2500000 -- 5 ns
# 25000000 -- 50 ns
# 50000000 -- 100 ns

### CHECKING FORCEFIELD SUFFIX
forcefield_suffix=$(check_ff_suffix "${forcefield}")

### OUTPUT FILE DETAILS
output_dirname="${shape_type}_${temp}_K_${diameter}_nmDIAM_${ligand_names}_${forcefield_suffix}_Trial_${trial_num}" # _${input_gold_folder}"
output_dirname="mixed_solvent_${cosolvent}_${vol_fraction_water_perc}-${output_dirname}"
output_dir="${PATH2SIM}/${simulation_dir}/$output_dirname" # <-- EDIT THIS IF YOU WANT THE SIMS TO GO TO A DIFFERENT DIRECTORY
output_file_name="sam"
output_itp="${output_file_name}.itp" # Name of itp file
output_top="${output_file_name}.top" # Name of topology
output_gro="${output_file_name}.gro" # Name of gro file
output_pdb="${output_file_name}.pdb" # Name of pdb file
output_ndx="index.ndx" # name of the index file

### --- USER-SPECIFIC PARAMETERS (END) --- ###

### CHECKING IF DIRECTORY EXISTS
check_output_dir "${output_dir}"

### CHANGING DIRECTORY TO OUTPUT DIRECTORY
cd "${output_dir}"

## RUNNING PYTHON SCRIPT TO OUTPUT INTO THE OUTPUT FOLDER
echo "----- RUNNING ${transfer_python_script} SCRIPT ------"
python ${py_transfer_script} --names ${ligand_names} --prep ${PREP_LIGAND_FINAL} --ff ${forcefield} --ofold ${output_dir} --gfold "${prep_gold_folder}" --inputgoldfold "${input_gold_folder}" --goldgro "${input_gold_gro}" --fracs "${ligand_fracs}" --ogro "${output_gro}" --otop "${output_top}" --geom "${shape_type}"

#########################
### PRM AND ITP FILES ###
#########################

## SEE IF WE NEED TO ADD ADDITIONAL PARAMETERS
input_ligand_parameter_file_name="${ligand_names}.prm" # _${forcefield_suffix}
echo "Checking if there are extra ligand parameters for: ${input_ligand_parameter_file_name}"
## DEFINING LIGAND PARAMETER FULL PATH
path_ligand_parameter="${PREP_LIGAND_FINAL}/${ligand_names}/${input_ligand_parameter_file_name}"
if [ -e "${path_ligand_parameter}" ]; then
    # COPYING PARAMETER FILE
    cp -rv "${path_ligand_parameter}" "./"
    # ADDING PARAMETER TO TOPOLOGY FILE
    force_field_line_num=$(grep -nE "forcefield.itp" ${output_top} | grep -Eo '^[^:]+') # Line where you see forcefield.itp
    sed -i "$((${force_field_line_num}+1))i #include \"${input_ligand_parameter_file_name}\"" ${output_top}
else
    echo "No additional ligand parameters found at ${path_ligand_parameter}"
    echo "Continuing without inclusion of *.prm files"
fi


## COPYING OVER ITP FILE
echo "--- COPYING ITP FILES ...---"
cp -rv "${PREP_LIGAND_FINAL}/${ligand_names}/${ligand_names}.itp" ./

## RUNNING PYTHON SCRIPT TO BIND THE SULFUR TO GOLDS
echo "----- RUNNING ${path2bind_gold_sulfur_script} SCRIPT ------"
python ${py_bind_gold_sulfur_script} --names "${ligand_names}" --ifold "${output_dir}"  --igro "${output_gro}" --top "${output_top}" --ores "${output_file_name}" --oitp "${output_itp}" --geom "${shape_type}"

## COPYING FORCE FIELD TO OUTPUT DIRECTORY
echo -e "--- COPYING FORCE FIELD FILES TO CURRENT DIRECTORY ---\n"
cp -r "${PATH_FORCEFIELD}/${forcefield}" "${output_dir}"

## MOVING ALL MDP FILES INTO DIRECTORY
echo -e "--- COPYING MDP FILES TO CURRENT DIRECTORY ---\n"
cp -rv "${path2mdp}/"{${mdp_em},${mdp_equil},${mdp_prod}} ./

### USING SED TO FIX MDP FILE PARAMETERS
echo -e "--- CORRECTING MDP FILE OPTIONS ---\n"
# CHANGING NUMBER OF STEPS
sed -i "s/NSTEPS/${n_steps_equil}/g" ${mdp_equil}
sed -i "s/NSTEPS/${n_steps_prod}/g" ${mdp_prod}

# CHANGING TEMPERATURE
sed -i "s/TEMPERATURE/${temp}/g" ${mdp_equil}
sed -i "s/TEMPERATURE/${temp}/g" ${mdp_prod}

#################################
### CONVERTING NP TO PDB FILE ###
#################################

## DEFINING PACKMOL SCRIPT
packmol_script="${PACKMOL_SCRIPT}"

## DEFINING PACKMOL INPUT
packmol_input="packmol.inp"
path_packmol_input="${INPUT_PACKMOL_PATH}/${packmol_input}"

## DEFINING PDB FILE
cosolvent_pdb="${cosolvent}.pdb"

## DEFINING IMPORTANT PATHS FOR PDB FILE
path_input_cosolvent="${PREP_SOLVENT_FINAL}/${cosolvent}/prep_files/${cosolvent_pdb}"

## DEFINING INPUT WATER
path_input_water="${INPUT_PACKMOL_WATER_PDB}"
water_pdb=$(basename ${path_input_water})

## USING EDIT CONF TO GENERATE PDB FILE
gmx editconf -f ${output_gro} -o ${output_pdb}

## COPYING OVER PDB FILES
cp "${path_input_cosolvent}" "${output_dir}"
cp "${path_input_water}" "${output_dir}"

############################
### PACKMOL -- SOLVATING ###
############################
## FINDING INPUTS FOR PACKMOL
default_num_solvents="216"

###################################
### NUMBER DENSITY OF COSOLVENT ###
###################################

## FINDING GRO COSOLVENT SOLVATED
path_cosolvent_solvated="${NP_PREP_SOLVENT_EQUIL}/${cosolvent}"
path_gro_cosolvent_solvated="${path_cosolvent_solvated}/${cosolvent}.gro"
path_itp_cosolvent_solvated="${path_cosolvent_solvated}/${cosolvent}.itp"
cosolvent_residue_name=$(extract_itp_resname ${path_itp_cosolvent_solvated})

## FINDING NUMBER DENSITY
num_density_cosolvent=$(gro_compute_num_density ${default_num_solvents} ${path_gro_cosolvent_solvated})

## FINDING GRO FILE FOR WATER
path_gro_water_solvated="${INPUT_PACKMOL_PATH}/spc216.gro"
num_density_water=$(gro_compute_num_density ${default_num_solvents} ${path_gro_water_solvated})

## FINDING BOX LENGTH OF NP
read -a np_box_lengths <<< $(gro_measure_box_size "${output_gro}")

## GETTING VOLUME
total_volume="$(compute_volume_from_box_length "${np_box_lengths[@]}")"

## GETTING MAX LENGTH IN ANGS
max_length_angs=$(awk -v lengths=${np_box_lengths[0]} 'BEGIN{ printf "%.5f",lengths*10}')

## FINDING HALF THE BOX LENGTH
half_box_length_angs=$(awk -v lengths=${max_length_angs} 'BEGIN{ printf "%.5f",lengths/2.0}')

####################
### VOLUME OF NP ###
####################

### COPYING OVER VDWRADII
path_vdw_radii="${INPUT_PACKMOL_PATH}/vdwradii.dat"

## COPYING VDW RADII
cp "${path_vdw_radii}" "${output_dir}"

### COMPUTING FREE VOLUME (SUPPRESSING OUTPUT)
free_volume_xvg="freevolume.xvg"
gmx freevolume -s "${output_gro}" -o "${free_volume_xvg}" >/dev/null 2>&1

### EXTRACTING FREE VOLUME
freevolume="$(tail "${free_volume_xvg}" -n1 | awk '{print $2'})"

### BASED ON FREE VOLUME, COMPUTE VOLUME OF WATER AND COSOLVENT
vol_water=$(awk -v free_vol=${freevolume} -v frac=${vol_fraction_water_decimals} 'BEGIN{ printf "%.3f",free_vol*frac}') # nm3
vol_cosolvent=$(awk -v free_vol=${freevolume} -v vol_water=${vol_water} 'BEGIN{ printf "%.3f",free_vol-vol_cosolvent}')

### FINDING NUMBER OF WATER AND COSOLVENTS
num_water=$(awk -v num_density=${num_density_water} -v vol=${vol_water} 'BEGIN{ printf "%d",vol*num_density}')
num_cosolvent=$(awk -v num_density=${num_density_cosolvent} -v vol=${vol_cosolvent} 'BEGIN{ printf "%d",vol*num_density}')

## PRINTING
echo "--- Computing number of water and cosolvents required ---"
echo "Number density of cosolvent: ${num_density_cosolvent}"
echo "Number density of water: ${num_density_water}"
echo "Nanoparticle box lengths: ${np_box_lengths[@]}"
echo "Total available volume: ${total_volume} nm3"
echo "Max length: ${max_length_angs}"
echo "Free volume: ${freevolume} nm3"
echo "Amount of water: ${vol_fraction_water_perc} vol percent"
echo "Volume of water/cosolvent: ${vol_water} / ${vol_cosolvent} nm3"
echo "Number of water/cosolvent: ${num_water} / ${num_cosolvent} molecules"

#################
### SOLVATING ###
#################

## DEFINING OUTPUT PDB
output_solvated_pdb="${output_file_name}_solvated.pdb"

## COPYING INPUT FILE
cp "${path_packmol_input}" "${output_dir}"

## EDITING PACKMOL FILE
# sed -i "s/_NPPDB_/${output_pdb}/g" 
sed -i "s/_OUTPUTPDB_/${output_solvated_pdb}/g" "${packmol_input}"
sed -i "s/_NPPDB_/${output_pdb}/g" "${packmol_input}"
sed -i "s/_COSOLVENTPDB_/${cosolvent_pdb}/g" "${packmol_input}"
sed -i "s/_NUMCOSOLVENT_/${num_cosolvent}/g" "${packmol_input}"
sed -i "s/_WATERPDB_/${water_pdb}/g" "${packmol_input}"
sed -i "s/_NUMWATER_/${num_water}/g" "${packmol_input}"
# ANGS MAX
sed -i "s/_MAXANGS_/${max_length_angs}/g" "${packmol_input}"
# HALFBOX
sed -i "s/_HALFBOX_/${half_box_length_angs}/g" "${packmol_input}"

## RUNNING PACKMOL
"${PACKMOL_SCRIPT}" < packmol.inp

## USING EDIT CONF TO GENERATE GRO FILE
gmx editconf -f ${output_solvated_pdb} -o ${output_solvated_pdb%.pdb}.gro -box "${np_box_lengths[0]}" "${np_box_lengths[1]}" "${np_box_lengths[2]}"

##############################
### UPDATING TOPOLOGY FILE ###
##############################

## COPYING OVER PRM AND ITP FOR COSOLVENT
cp "${path_cosolvent_solvated}/"{${cosolvent}.itp,${cosolvent}.prm} "${output_dir}"

## ADDING PARAMETER AND ITP FILE TO TOPOLOGY
# PRM FILE
topology_add_include_specific "${output_top}" ${ligand_names}.prm "#include \"${cosolvent}.prm\""
# ITP FILE
topology_add_include_specific "${output_top}" "${output_itp}" "#include \"${cosolvent}.itp\""

## ADDING TO TOPOLOGY FILE
echo "   ${cosolvent_residue_name}    ${num_cosolvent}" >> "${output_top}"
echo "   SOL    ${num_water}" >> "${output_top}"

####################
### NEUTRALIZING ###
####################

## NEUTRALIZING IF NECESSARY
echo -e "--- NEUTRALIZING CHARGES ---\n"
echo "Creating TPR file for balancing of charge"
gmx grompp -f ${mdp_em} -c ${output_gro%.gro}_solvated.gro -p ${output_top} -o ${output_gro%.gro}_solvated.tpr -maxwarn ${max_warn_index}
gmx genion -s ${output_gro%.gro}_solvated.tpr -p $output_top -o ${output_gro%.gro}_solvated.gro -neutral << INPUT
SOL
INPUT

## ENERGY MINIMIZING
echo -e "--- ENERGY MINIMIZATION--- \n"
gmx grompp -f ${mdp_em} -c ${output_gro%.gro}_solvated.gro -p $output_top -o ${output_gro%.gro}_em.tpr -maxwarn ${max_warn_index}
gmx mdrun -v -nt 1 -deffnm ${output_gro%.gro}_em

# cp ${output_gro%.gro}_solvated.gro ${output_gro%.gro}_em.gro

## CHECKING RESIDUES WITHIN CORE AFTER ENERGY MINIMIZATION
if [ "${shape_type}" == "hollow" ]; then
    ## MAKING THE MOLECULES WHOLE
gmx trjconv -s ${output_gro%.gro}_em.tpr -f "${output_gro%.gro}_em.gro" -o "${output_gro%.gro}_em.gro" -pbc whole << INPUT
System
INPUT
    
    echo "*** Running ${py_remove_atoms_file} ***"
    python ${py_remove_atoms_file} --rad ${radius} --ofold ${output_dir} --ogro "${output_gro%.gro}_em.gro" --otop "${output_top}"
fi

# cp ${output_gro%.gro}_solvated.gro ${output_gro%.gro}_em.gro

## CREATING JOB SCRIPT FILE
echo -e "--- CREATING SUBMISSION FILE: $submitScript ---"
sed "s/_JOB_NAME_/${output_dirname}/g" "${PATH2SUBMISSION}/${submitScript}" | sed "s/_OUTPUTNAME_/${output_file_name}/g" > ./$submitScript

## CLEANING UP DIRECTORY FILE
echo -e "--- CLEANING UP DIRECTORY FILE ---"
setup_dir="setup_files"
mkdir -p ${setup_dir}

checkNMoveFile "${output_gro}" ${setup_dir}
checkNMoveFile "${output_gro%.gro}_solvated.gro" ${setup_dir}
mv \#* setup_files

## WRITING OUTPUT DIRECTORY TO JOB LIST FILE
echo "$output_dir" >> "${JOB_SCRIPT}"















