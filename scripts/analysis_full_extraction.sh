#!/bin/bash

# analysis_full_extraction.sh
# This bash script will run the following scripts:
#   analysis_extraction.sh -- extract xtc files
#   analysis_xtctruncate.sh -- truncates the xtc files
# This will completely run extraction for a specific directory.
# CREATED ON: 05/06/2018
# AUTHOR: Alex K. Chew (alexkchew@gmail.com)

## LOADING LIGAND BUILDER VARIABLES / FUNCIONS
source "../bin/nanoparticle_rc.sh"

################################
### DEFINING INPUT VARIABLES ###
################################

## DEFINING INPUT DIRECTORY (SIMULATION FOLDER WITH MANY SIMULATIONS)
input_dir_name="190225-2nm_C11_Sims"

## DEFINING OUTPUT DIRECTORY
output_dir_name="190225-2nm_C11_Sims"

## LOGICALS
want_self_assembled_monolayer="False" # True if you want to change 

## NOTE: If you want to change the cutting time and end time -- change the analysis_xtctruncate.sh script!
# CURRENTLY: 
#   - 10 ns - 50 ns (Normal transfer ligand builder)
#   - 0 ns - 50 ns (self_assembled_monolayer)

##################################
### DEFINING IMPORTANT SCRIPTS ###
##################################

bs_analysis_extract="${PATH2SCRIPTS}/analysis_extraction.sh"
bs_analysis_xtc_trunc="${PATH2SCRIPTS}/analysis_xtctruncate.sh"

## RUNNING EXTRACTION PROTOCOL
bash "${bs_analysis_extract}" "${input_dir_name}" "${output_dir_name}" "${want_self_assembled_monolayer}"

## RUNNING XTC TRUNCATION PROTOCOL
bash "${bs_analysis_xtc_trunc}" True True True False "${output_dir_name}" "${want_self_assembled_monolayer}"

###############
### SUMMARY ###
###############
echo "------- SUMMARY -------"
echo "INPUT DIRECTORY: ${input_dir_name}"
echo "OUTPUT DIRECTORY: ${output_dir_name}"
echo "WANT_SELF_ASSEMBLY: ${want_self_assembled_monolayer}"
