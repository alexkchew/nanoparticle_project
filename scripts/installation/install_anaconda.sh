#!/bin/bash

# install_anaconda.sh
# The purpose of this script is to install anaconda on a Linux server

## TRYING TO INSTALL CMAKE
path_installation="${HOME}/anaconda"

## GOING TO INSTALLATION
mkdir -p "${path_installation}"

cd "${path_installation}"

## INSTALLING
wget "https://repo.anaconda.com/archive/Anaconda3-2020.07-Linux-x86_64.sh"

## RUNNING CODE (MAY TAKE SOME TIME!)
bash "Anaconda3-2020.07-Linux-x86_64.sh"

## AFTER INSTALLING, FEEL FREE TO REMOVE THE PATH INSTALLATION
# rm -rv "${path_installation}"

## UPDATING CONDA
#	 ./conda update -n base -c defaults conda
#	 ./conda init bash
#	May not need the below command:
#	echo 'export PATH=${PATH}:/home/akchew/anaconda3/bin' >> ~/.bash_profile
#	
#	conda install -c conda-forge rdkit