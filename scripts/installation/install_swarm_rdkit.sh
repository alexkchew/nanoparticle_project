#!/bin/bash

# install_swarm_rdkit.sh
# The purpose of this script is to install rdkit on swarm
# RDKit allows you to run smiles strings and analyze 
# information from cheminformatics databases
## IMPORTANT NOTE: SCRIPT DOES NOT WORK!!!!
#
# USAGE:
#	bash install_swarm_rdkit.sh
# Written by: Alex K. Chew (08/14/2020)
#
# RESOURCES:
#	https://www.rdkit.org/docs/Install.html


## DEFINING PATH TO INSTALLATION
path_installation="${HOME}/rdkit"

## CREATING DIRECTORY
mkdir -p "${path_installation}"

## GOING TO DIRECTORY
cd "${path_installation}"

## REMOVING ANY *.TGZ
# rm -f *.tgz *.tgz.*

## DEFINING CMAKE
cmake_path="/home/akchew/cmake/cmake-3.14.5/bin"
cmake_new="/home/akchew/cmake/cmake-3.14.5/bin/cmake"

## SELCTING VERSION
version="2019_09_3"
release_name="Release_${version}.tar.gz"
release_name_no_tar_gz="rdkit-${release_name%.tar.gz}"

## DOWNLOADING FILE
if [[ ! -e "${release_name}" ]]; then
	wget "https://github.com/rdkit/rdkit/archive/${release_name}"
fi

## UNLOADING THE PACKAGE
if [[ ! -e "${release_name_no_tar_gz}" ]]; then
	tar zxvf "${release_name}"
fi

## CREATING RDBASE NAME
export RDBASE="${path_installation}/${release_name_no_tar_gz}"
export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${RDBASE}/lib"
## PATH TO CMAKE
export PATH="${PATH}:${cmake_path}"

## GOING 
cd $RDBASE
mkdir build
cd build
${cmake_new} .. -DPYTHON_EXECUTABLE=/usr/bin/python3.6

## CREATING
make
make install
