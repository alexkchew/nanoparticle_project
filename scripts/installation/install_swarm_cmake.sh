#!/bin/bash

# install_swarm_cmake.sh
# This script installs cmake on the swarm cluster
## IMPORTANT NOTES:
#	- Script does install cmake by itself
#	- I did not use this script to install anything else
# 
# Written by: Alex K. chew (08/14/2020)


## RESOURCES FOR CMAKE
#	https://pachterlab.github.io/kallisto/local_build.html#:~:text=The%20easiest%20way%20to%20install,tar.&text=Make%20sure%20to%20set%20the,files%20at%20the%20default%20location.

export PATH=$HOME/bin:$PATH
export LD_LIBRARY_PATH=$HOME/lib/:$LD_LIBRARY_PATH

## TRYING TO INSTALL CMAKE
path_installation="${HOME}/cmake"

## CREATING DIRECTORY
mkdir -p "${path_installation}"

## GOING TO DIRECTORY
cd "${path_installation}"

## GETTING CMAKE VERSION
wget https://github.com/Kitware/CMake/releases/download/v3.14.5/cmake-3.14.5.tar.gz

## UNTARRING
tar -zxvf cmake*.tar.gz

## GOING TO CMAKE
cd cmake*

## CONFIGURING
./configure --prefix=${HOME}

## MAKING INSTALLATION
make
make install
