# -*- coding: utf-8 -*-
"""
plotting_functions.py
This script contains plotting functions that you can use throughout your analysis tools.

Usage: import plotting_functions

Functions:
    plot_xy: Plot x, y as a line    
    plot_xy_err: Plot x, y as a line with error bars

Author: Alex K. Chew (12/03/2017)
"""


### DEFINING IMPORTANT MODULES
import matplotlib # For plotting purposes
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab! <-- For the server
import matplotlib.pyplot as plt

### GLOBAL VARIABLES
fontSize = 14 # Font size
lineWidth = 1.4 # Line width

### FUNCTION TO SIMPLY PLOT X AND Y VALUES AS A LINE PLOT
def plot_xy(x_values,y_values,title=None,xlabel='x',ylabel='x',wantPlot=False  ):
    '''
    The purpose of this function is to plot the desired residue, etc. for a density distribution
    INPUTS:
        x_values: radius values
        y_values: density distribution values
        title: title of the plot
        xlabel: label of the x-axis
        ylabel: label of the y-axis
        wantPlot: True if you want to see the plot
    OUTPUT:
        plot as a matplotlib
    '''
    if wantPlot is True:
        ## PLOTTING CONFIGURATION
        # Changing presentation style
        plt.style.use('classic') 
        
        ### PLOTTING
        plt.figure() # Creating Figures
        
        # Setting title
        plt.title(title)
        
        # Setting x, y labels
        plt.xlabel(xlabel, fontsize=fontSize)
        plt.ylabel(ylabel, fontsize=fontSize)
        
        plt.plot(x_values, y_values, '-', color='black', linewidth=lineWidth) # ,'-',LineStyle)
    return

### FUNCTION TO PLOT X AND Y WITH ERROR BARS
def plot_xy_err(x_values,y_values,x_err = None, y_err=None, title=None,xlabel='x',ylabel='x',wantPlot=False  ):
    '''
    The purpose of this function is to plot the desired residue, etc. for a density distribution
    INPUTS:
        x_values: radius values
        y_values: density distribution values
        title: title of the plot
        xlabel: label of the x-axis
        ylabel: label of the y-axis
        wantPlot: True if you want to see the plot
    OUTPUT:
        plot as a matplotlib
    '''
    if wantPlot is True:
        ## PLOTTING CONFIGURATION
        # Changing presentation style
        plt.style.use('classic') 
        
        ### PLOTTING
        plt.figure() # Creating Figures
        
        # Setting title
        plt.title(title)
        
        # Setting x, y labels
        plt.xlabel(xlabel, fontsize=fontSize)
        plt.ylabel(ylabel, fontsize=fontSize)
        
        # PLOTTING WITH ERROR BARS
        plt.errorbar(x=x_values, y=y_values, fmt='-', xerr = x_err, yerr = y_err, color='black', linewidth=lineWidth) 
    return

