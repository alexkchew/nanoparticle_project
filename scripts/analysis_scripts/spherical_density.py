# -*- coding: utf-8 -*-
"""
spherical_density.py
The purpose of this script is to look into spherical nanoparticles and find the density distribution. This script takes a lot of information from the planar_density case.

Author: Alex K. Chew (12/04/2017)
"""

### INPORTING IMPORTANT MODULES
import initialize # initialization functions like loading gro files, etc.
import plotting_functions # plotting functions
import csv_file_functions # csv file functions
import numpy as np # Math modules
import planar_density # Use rdf functions from the planar case
import mdtraj as md
import matplotlib # For plotting purposes
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab! <-- For the server
import matplotlib.pyplot as plt

### **DEFINING FUNCTIONS**

### FUNCTION TO FIND CENTER OF THE NANOPARTICLE BASED ON SULFUR ATOMS
def find_center_NP( traj, monolayer_sulfur_atom_name="S1"):
    '''
    This function finds the center of the nanoparticle simply by finding the atom names, then averaging all the coordinates
    INPUTS:
        traj: trajectory from md.traj
        monolayer_sulfur_atom_name: Sulfur atom name (i.e. S1)
    OUTPUTS:
        NP_center: # FRAMES x 3 vector
    '''
    ## FINDING SPHERICAL SULFUR ATOMS
    monolayer_sulfur_atom = traj.topology.select( 'name ' + monolayer_sulfur_atom_name )
    
    ## FINDING CENTER OF THE SULFUR ATOMS
    NP_center = np.mean(traj.xyz[:,monolayer_sulfur_atom,:],axis=1)
    return NP_center

### FUNCTION TO FIND THE RADIAL DISTRIBUTION FUNCTION IN THE SPHERICAL CASE
def find_spherical_rdf( traj, residue_name = 'HOH', atom_names = ['O','H1','H2'], residue_group_type = 'COM', bin_width = 0.01, max_radius = None, periodic=False):
    '''
    The purpose of this script is to find the radial distribution for a spherical nanoparticle case given the groups of interest
    INPUTS:
        traj: trajectory from MD.TRAJ
        residue_name: Name of your residue (i.e. 'HOH')
        atom_names: list of atom names within your residue (i.e. ['O','H1','H2'])
        residue_group_type: Type of residue calculation:
            'COM' - center of mass
            'AllAtom' - take into account all atoms individually
        bin_width: Bin width of your RDF
        max_radius: maximum radius you want to look into (by default, this will be half box length)
    OUTPUTS:
        g_r: radial distribution function
        r: radius vector
    '''
    ### FINDING MAXIMUM RADIUS
    if max_radius == None: # Then change max_radius from default
        ## FINDING BOX SIZE DETAILS
        box_size = traj.unitcell_lengths
        max_radius = np.mean(box_size / 2.0); # half box length
        
    ## FINDING RADIUS RANGE
    r_range=[0.0,max_radius]

    ## FINDING NUMBER OF BINS
    n_bins = int((r_range[1] - r_range[0]) / bin_width)
    
    ### FINDING CENTER OF NANOPARTICLE
    NP_center = find_center_NP( traj = traj, monolayer_sulfur_atom_name="S1")
    ## FINDING RESIDUE AND ATOM INDICES
    residue_index, atom_index = planar_density.find_residue_atom_index(traj,residue_name = residue_name, atom_names = atom_names)
    
    ### START BY CHANGING TRAJECTORY SO THAT EACH GROUP IS REPRESENTED AS A SINGLE ATOM
    # CREATING COPY OF TRAJ
    copied_traj = traj[:]
    ## SHIFTING AN ATOM NOT WITHIN ATOM INDEX        
    # GETTING AN ATOM NOT WITHIN ATOM INDEX
    center_index_to_change=np.setdiff1d(  [ current_atom.index for current_atom in traj.topology.atoms ],
                                        [current_atom_value for current_residue_index in atom_index for current_atom_value in current_residue_index ] )[0]
    # CHANGING POSITIONS OF THE SULFUR ATOM (Z-POSITION)
    copied_traj.xyz[:, center_index_to_change, :] = NP_center
    
    # USING CENTER OF MASS GROUPING
    if residue_group_type == 'COM':
        ### FINDING CENTER OF MASS OF MOLECULES OF INTEREST
        center_of_mass = planar_density.find_center_of_mass(traj = copied_traj,
                                       residue_name = residue_name,
                                       atom_names = atom_names)
        # GETTING ALL INDICES OF THE FIRST ATOM IN THE RESIDUE
        atom_index_to_change=[ atom[0] for atom in atom_index ]
        # CHANGING ALL POSITIONS OF THE FIRST ATOM IN RESIDUE TO CENTER OF MASSES
        copied_traj.xyz[:, atom_index_to_change] = center_of_mass[:]
        ## CREATING ATOM PAIRS
        atom_pairs = [ [center_index_to_change, x] for x in atom_index_to_change]
        
    # USING ALL ATOM GROUPING
    elif residue_group_type == 'AllAtom':
        ## CREATING ATOM PAIRS
        atom_pairs = [ [center_index_to_change, current_atom_value] for current_residue_index in atom_index for current_atom_value in current_residue_index]

    ## FINDING DISTANCES
    distances = md.compute_distances(traj=copied_traj, atom_pairs =atom_pairs, periodic=False, opt=True)
        
    ## FINDING TOTAL ATOMS / RESIDUE -- NORMALIZATION FACTOR
    atom_per_residue = float(len(atom_pairs) / len(residue_index))
    # NUMBER OF FRAMES
    num_frames = copied_traj.time.size    
    ## FINDING EDGES -- USING MD.TRAJ'S ALGORITHM
    g_r, edges = np.histogram(distances, range=r_range,  bins=n_bins)
    r = 0.5 * (edges[1:] + edges[:-1])
    binNumbers = np.arange(1, int(n_bins)+1) # Index 1, ... sequential, used to make binVolumes
    binVolumes =  ((binNumbers[:] + 1)**3 - (binNumbers[:])**3) * 4 / 3. * np.pi * bin_width**3
    
    norm =  binVolumes * num_frames  * atom_per_residue
    g_r = g_r.astype(np.float64) / norm  # From int64.
    
    ## USING MDTRAJ TO CALCULATE RDF WITH PBC
#    r, g_r = md.compute_rdf(traj = copied_traj, pairs = atom_pairs, bin_width = bin_width,r_range=[0.0,max_radius], periodic=True )
    
    return g_r, r

### MAIN SCRIPT IMPLEMENTING SPHERICAL RDF
def main(traj, bin_width, groups, output_text_dir, max_radius = None, specific_dir=None, wantText=True, wantPlot=False, ):
    '''
    This script implements "find_spherical_rdf" and exports to a csv file / shows a figure of it
    INPUTS:
        traj: trajectory to run the analysis on
        bin_width: Bin width is the resolution of the density profile
        groups: List of list such that in the first list, you have 3 columns being LABEL (STR), RESIDUE NAME (STR), ATOM LIST (AS A LIST)
        max_radius: Maximum radius you want your RDF to be (by default, half box length)
        output_text_dir: Location you want to export your text file. The name will be based on the "path_output_data -- i.e. date - script name - output text .csv
        specific_dir: Location of your directory of study -- used as an output variable
        wantPlot: True if you want to see plots
        wantText: True if you want to save the figure
    OUTPUTS:
        csv text file based on the output_text_dir with your data        
    '''
    # PRINTING
    print("----------------Running spherical RDF script----------------")
    
    ### CREATING STORAGE BIN
    storage_analysis = []
    if wantPlot is True:
        plt.close('all') # Closing all pre-existing figures
    
    
    ### LOOPING THROUGH EACH GROUP
    for current_group in groups:
        ## DEFINING RESIDUE NAMES + ATOMS FROM GROUPS
        my_label = current_group[0]
        residue_name = current_group[1]
        atom_names = current_group[2]
        residue_group_type = current_group[3] # CENTER OF MASS / ALL ATOM
    
        ## PRINTING
        print("WORKING ON GROUP: %s"%(my_label))
        print("WORKING ON RESIDUE NAME: %s"%(residue_name))
        print("WORKING ON ATOMS NAME: %s"%(atom_names))
        print("TYPE OF JOB: %s"%(residue_group_type))
    
        ## FINDING RDF FOR SPHERICAL CASE
        g_r, r= find_spherical_rdf( traj = traj, residue_name = residue_name, atom_names = atom_names, bin_width = bin_width,residue_group_type=residue_group_type, max_radius = max_radius )
        
        ## STORING EACH GROUP ANALYSIS
        storage_analysis.append({'label': [my_label,residue_name,'-'.join(atom_names)],
                                 'xValues': r,
                                 'yValues': g_r})
    
        ### PLOTTING DENSITY DISTRIBUTION
        plotting_functions.plot_xy(x_values = r,
                     y_values = g_r,
                     title='Density distribution for label: %s, residue name: %s ' %(my_label, residue_name),
                     xlabel='Radius (nm)',
                     ylabel='Density (moiety per nm^3)',
                     wantPlot=wantPlot  )
    
    ## DEFINING LOCATION TO STORE RESULTS
    script_output_name="spherical_density"
    output_text_name=initialize.getDate() + '-' + specific_dir
    path_to_results_folder = output_text_dir + '\\' + script_output_name
    path_output_data= path_to_results_folder + '\\' + output_text_name + '.csv'
    
    ## CHECKING IF DIRECTORY EXISTS
    initialize.check_dir(initialize.checkPath2Server(path_to_results_folder))
    
    ### CHECKING THE PATH
    path_output_data=initialize.checkPath2Server(path_output_data)
    
    ## DEFINING X AND Y LABELS
    x_labels='r (nm)'
    y_labels='density (moiety/nm3)'
    
    ## GETTING COMBINED DATA
    combined_list = [ [ storage_analysis[x]['xValues'], storage_analysis[x]['yValues'] ] for x in range(len(storage_analysis)) ]
    
    ## EXTENDING THE DATA TO BE (X_1, Y_1, X_2, Y_2, ETC.)
    data = [] # Storage
    for current_index in range(len(combined_list)):
        data.extend( [combined_list[current_index][0], combined_list[current_index][1]] )
    
    ## TRANSPOSING THE DATA
    data = np.array(data).T.tolist()
    
    ## DEFINING LABELS FOR FIRST LINE
    ## GETTING THE LABELS
    first_line=[]
    
    # FINDING NUMBER OF STORAGE
    num_data_storage = len(storage_analysis)
    
    # WRITING LABELS
    for current_index in range(num_data_storage):
        # DEFINING LABELS
        current_labels = storage_analysis[current_index]['label']
        # COMBINING LABELS
        combine_labels = '-'.join(current_labels)
        # WRITING LABEL TO FIRST LINE, skipping one every time
        first_line.extend( [ combine_labels, ' ' ])
    
    # FIXING FIRST LINE
    first_line_combined = ', '.join(first_line)

    ### EXPORTING TO CSV
    csv_file_functions.export2csv(path_output_data, first_line_combined, x_labels, y_labels, data )
    


#%% MAIN SCRIPT
if __name__ == "__main__":

    ### DIRECTORY TO WORK ON
    analysis_dir=r"171204-ROT_SPHERICAL" # Analysis directory
    specific_dir=r"Spherical_310.15_2_nm_diam_ROT_SN_Spherical_CHARMM36" # Directory within analysis_dir
    path2AnalysisDir=r"R:\scratch\LigandBuilder\Analysis\\" + analysis_dir + '\\' + specific_dir # PC Side

    ### CHECKING IF WE ARE ON THE SERVER (SWARM)
    path2AnalysisDir = initialize.checkPath2Server(path2AnalysisDir)
    
    ## DEFINING OUTPUT TEXT DIRECTORY
    output_text_dir=r"R:\\scratch\\LigandBuilder\\Scripts\\AnalysisScripts\\results\\"
    
    ### DEFINING FILE NAMES
    gro_file=r"sam_prod.gro"
    xtc_file=r"sam_prod_10_ns_whole.xtc"
    
    ### LOADING TRAJECTORY
    traj,topology = initialize.load_traj_from_dir( directories = path2AnalysisDir, # Directory to analysis
                                                  xtcFile = xtc_file, # trajectories
                                                  structure_File = gro_file # structure file
                                                  )
    #%%
    ### INPUTS
    ## GROUPS OF INTEREST
    groups=[# ['COO', 'COO', ['S1'],'AllAtom'] ,
#            ['H_2O', 'HOH', ['O','H1','H2'],'AllAtom' ],  # LABEL, RESIDUE NAME, ATOM NAMES (LIST) # ,
            ['END', 'RSN', ['N82', 'C83', 'H84', 'H85', 'H86',
                                       'C91', 'H92', 'H93', 'H94',
                                       'C87', 'H89', 'H88', 'H90'] , 'AllAtom' ],
            ['GOLD-S', 'RSN', ['S1'],'AllAtom' ]
             ]
    
    ## BIN WIDTH -- RESOLUTION OF RDF
    bin_width=0.2 # nm
    max_radius = None
    
    ## LOGICALS
    wantPlot=True # True if you want a plot
    wantText=True # True if you want a text file saved


    
    ## RUNNING MAIN SCRIPT
    main(traj, bin_width, groups, output_text_dir, max_radius = None, specific_dir=specific_dir, wantText=True, wantPlot=wantPlot)
    
