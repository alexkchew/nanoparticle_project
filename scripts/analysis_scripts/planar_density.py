# -*- coding: utf-8 -*-
"""
planar_rdf.py
The purpose of this script is to find the radial distribution along the z-direction for SAM planar surface

Script by Alex K. Chew (12/1/2017)

### UPDATES
171204 - Created main script for this to operate outside
180108 - Added error bars
"""
#%% MODULES / FUNCTIONS
### IMPORTING IMPORTANT MODULEs
import numpy as np # Math modules
# Loading trajectory functions
import initialize # initialization functions like loading gro files, etc.
import plotting_functions # plotting functions
import csv_file_functions # csv file functions
import time # to keep track of time
import mdtraj as md # to import xtc and run analysis
import matplotlib # For plotting purposes
matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab! <-- For the server
import matplotlib.pyplot as plt

### **DEFINING FUNCTIONS**
    
### FUNCTION TO FIND TOP AND BOTTOM SULFUR ATOMS IN A PLANAR SURFACE
def find_top_bottom_sulfur_atoms( traj ,monolayer_sulfur_atom_name="S1", monolayer_dist_tolerance = 0.5, ):
    '''
    This function will find the sulfur atoms on top and bottom monolayer for a planar SAM surface. It uses the fact that all sulfurs you care about is "S1". The separation is based on monolayer_dist_tolerance, which is typically 0.5 nm
    INPUTS:
        traj: Trajectory from md.traj
        monolayer_sulfur_atom_name: sulfur atom name in topology for the monolayer (default = 'S1')
        monolayer_dist_tolerance: distance of cutoff between monolayers (default =  0.5 nm)
    OUTPUTS:
        top_sulfur_atoms: Atom index for all top sulfur atoms
        bottom_sulfur_atoms: Atom index for all bottom atoms
    '''

    ## FINDING SURFACE (MY ZERO)
    monolayer_sulfur_atom = traj.topology.select( 'name ' + monolayer_sulfur_atom_name )
    
    # FINDING AVERAGE Z-COORDINATE XYZ
    avg_sulfur_z = np.mean(traj.xyz[:,monolayer_sulfur_atom,2],axis=0)   

    # DIVIDING THE MONOLAYER
    label =['Group_1'] # The first index will always goes to group 1
    
    # LOOPING USING THE TOLERANCE TO SEE WHICH GROUPS ARE WHICH
    for sulfur_index, each_z_coordinate in enumerate(avg_sulfur_z):
        if sulfur_index != 0: # Skipping first index
            if abs(each_z_coordinate - avg_sulfur_z[0]) < monolayer_dist_tolerance:
                label.append('Group_1')
            else:
                label.append('Group_2')
    # NOW, DESIGNATING WHICH ONE IS THE TOP / BOTTOM LAYER
    group_1_index = [ index for index, x in enumerate(label) if x == 'Group_1' ]
    group_2_index = [ index for index, x in enumerate(label) if x == 'Group_2' ]
    
    ## FINDING TOP AND BOTTOM MONOLAYERS
    if np.mean(avg_sulfur_z[group_1_index]) > np.mean(avg_sulfur_z[group_2_index]): # Group 1 is above group 2
        top_sulfur_atoms = monolayer_sulfur_atom[group_1_index]; bottom_sulfur_atoms = monolayer_sulfur_atom[group_2_index]
    else: # the reverse
        top_sulfur_atoms = monolayer_sulfur_atom[group_2_index]; bottom_sulfur_atoms = monolayer_sulfur_atom[group_1_index]
    
    return top_sulfur_atoms, bottom_sulfur_atoms

### FUNCTION TO FIND RESIDUE / ATOM INDICES GIVEN RESIDUE NAME AND ATOM NAMES
def find_residue_atom_index(traj, residue_name = 'HOH', atom_names = ['O','H1','H2']):
    '''
    The purpose of this function is to look at your trajectory's topology and find the atom index that you care about.
    INPUTS:
        traj: trajectory from md.traj
        residue_name: residue name as a string (i.e. 'HOH')
        atom_names: list of atom names within your residue (i.e. ['O','H1','H2'])
    OUTPUTS:
        residue_index: list of residue indices
        atom_index: list of atom indices
    '''
    ## FINDING ALL RESIDUES OF THE TYPE
    residue_index = [ x.index for x in traj.topology.residues if x.name == residue_name]
    ## FINDING ALL ATOMS THAT YOU CARE ABOUT
    atom_index = [ [ atom.index for atom in traj.topology.residue(res).atoms if atom.name in atom_names ] for res in residue_index ]
    return residue_index, atom_index
    
### FUNCTION TO FIND CENTER OF MASS OF THE RESIDUES / ATOMS
def find_center_of_mass( traj, residue_name = 'HOH', atom_names = ['O','H1','H2'] ):
    '''
    The purpose of this function is to find the center of mass of your residues given the residue name and atom names. Note that atom names is a list. 
    INPUTS:
        traj: trajectory from md.traj
        residue_name: residue name as a string (i.e. 'HOH')
        atom_names: list of atom names within your residue (i.e. ['O','H1','H2'])
    OUTPUTS:
        center_of_mass: Numpy array of the center of masses ( NUM FRAMES X NUM ATOMS X 3 (X,Y,Z))
    NOTES: This function may have issues later due to the high vectorization approach!
    '''    
    ## KEEPING TRACK OF TIME
    COM_time=time.time()
    ### INITIALIZATION OF COM
    ## FINDING ATOM AND RESIDUE INDICES
    residue_index, atom_index = find_residue_atom_index(traj, residue_name, atom_names)
    ## FINDING MASS OF A SINGLE GROUP
    atom_mass = [ traj.topology.atom(atom_ind).element.mass for atom_ind in atom_index[0]]
    ## FINDING TOTAL MASS
    totalMass = np.sum(atom_mass)
    print("--- COM CALCULATION FOR %s FRAMES, %s RESIDUE (%s residues), and %s ATOM TYPES (%s atoms) ---" %(len(traj),
                                                                                   residue_name,
                                                                                   len(residue_index),
                                                                                   atom_names,
                                                                                   len(residue_index)*len(atom_names)
                                                                                   ) )
    
    ### COM CALCULATION
    # FINDING POSITION OF ALL ATOMS
    position_all_atoms = traj.xyz[:, atom_index] # Frame x atom x positions
    ## GETTING SHAPE OF ALL POSITIONS
    n_frames, n_residues, n_atoms, n_coordinates = position_all_atoms.shape
    ## REPLICATING MASSES FOR MATRIX MULTIPLICATION
    rep_mass = np.tile(np.transpose(atom_mass).reshape((n_atoms,1)), (n_frames,n_residues,1, 1)) # 1 for x,y,z coordinates already defined
    ## MULTIPLYING TO GET M_i * x_i
    multiplied_numerator = position_all_atoms * rep_mass
    ## SUMMING ALL M_i * X_i
    summed_numerator = np.sum(multiplied_numerator, axis=2 ) # Summing within each of the residues
    ## DIVIDING NUMERATOR TO GET COM
    center_of_mass = summed_numerator / totalMass
    ## PRINTING TOTAL TIME TAKEN
    h, m, s = initialize.convert2HoursMinSec( time.time()-COM_time )
    print('Total time for COM calculation was: %d hours, %d minutes, %d seconds \n' %(h, m, s))
    
    return center_of_mass

### FUNCTION TO SPLIT A LIST IN HALF
def split_list(a_list):
    '''
    This function simply splits a list in half (Note that the second one will have one more if list is not even)
    INPUTS:
        a_list: a list (numpy or regular)
    OUTPUTS:
        a: first half of list
        b: second half of list
    '''
    half = int(len(a_list)/2)
    return a_list[:half], a_list[half:]

### FUNCTION TO GET THE AVERAGE Z DENSITIES
def average_z_densities(density_dist):
    '''
    This function simply averages the left and right side of the z-coordinates in the planar case. This is useful for the current ligand builder system
    INPUTS:
        density_dist: Vector containing bins per radii
    OUTPUTs:
        density_dist_avg: Vector averaged (should be approximately half of that of  bin_storage)
        density_dist_std: Vector standard deviation
    '''
    # SPLITTING EVENLY
    a, b = split_list(density_dist)
    # FLIPPING B
    b = np.flip(b, axis=0)
    ## CHECKING IF THE BIN STORAGE IS ODD/EVEN
    if len(density_dist)% 2 == 0: # EVEN
        density_dist_avg = ( a + b ) / 2.0;
        density_dist_std = np.std([a,b],axis=0) # Standard deviation
    else: # ODD
        # B WILL HAVE ONE LARGER THAN A
        density_dist_avg = ( a + b[:-1] ) / 2.0; # Adding all but the last and averaging
        density_dist_std = np.std([a,b[:-1]],axis=0)
        # Appending the final value of b
        density_dist_avg = np.append(density_dist_avg, b[-1])
        density_dist_std = np.append(density_dist_std, 0) # ODD one in the middle has no error
        
    return density_dist_avg, density_dist_std

### MAIN FUNCTION TO FIND THE PLANAR DENSITY
def find_planar_density(traj, residue_name = 'HOH', atom_names = ['O','H1','H2'], residue_group_type = 'COM', bin_width = 0.01) :
    '''
    The purpose of this script is to take the trajectory and find teh residue names / atom names you are interested in
    INPUTS:
        traj: trajectory from md.traj
        residue_name: Name of your residue (i.e. 'HOH')
        atom_names: list of atom names within your residue (i.e. ['O','H1','H2'])
        residue_group_type: Type of residue calculation:
            'COM' - center of mass
            'AllAtom' - take into account all atoms individually
        bin_width: Bin width of your RDF
    OUTPUTS:
        planar_density: density distribution (y-values) with standard deviations
        r: distance vector (x-values)
    '''
    ## FINDING BOX SIZE DETAILS
    box_size = traj.unitcell_lengths
    box_size_x = box_size[:,0] # NUM_FRAMES X 1 -- x dimensions 
    box_size_y = box_size[:,1] # NUM_FRAMES X 1 -- y dimensions 
    box_size_z = box_size[:,2] # NUM_FRAMES X 1 -- z dimensions 
    
    ## FINDING MONOLAYER TOP AND BOTTOM SULFUR ATOMS
    top_sulfur_atoms, bottom_sulfur_atoms = find_top_bottom_sulfur_atoms( traj = traj ,monolayer_sulfur_atom_name="S1", monolayer_dist_tolerance = 0.5, )
    
    ## FINDING THE Z-POSITION OF THE SULFUR ATOMS EACH FRAME
    z_position_top_sulfur = np.mean(traj.xyz[:,top_sulfur_atoms,2],axis=1) # NUM_FRAMES X 1
    z_position_bot_sulfur = np.mean(traj.xyz[:,bottom_sulfur_atoms,2],axis=1) # NUM_FRAMES X 1
    
    ## FINDING DISTANCE VECTOR BETWEEN TOP AND BOTTOM MONOLAYER
    dist_vector = z_position_bot_sulfur + box_size_z - z_position_top_sulfur
    max_dist = np.max(dist_vector)
    max_bin = int(np.floor(max_dist / bin_width))
    
    ## CREATING STORAGE FOR EACH BIN
    bin_storage = np.zeros(max_bin)
    
    ## FINDING RESIDUE AND ATOM INDICES
    residue_index, atom_index = find_residue_atom_index(traj,residue_name = residue_name, atom_names = atom_names)
    
    ### START BY CHANGING TRAJECTORY SO THAT EACH GROUP IS REPRESENTED AS A SINGLE ATOM
    # CREATING COPY OF TRAJ
    copied_traj = traj[:]
    # FINDING INDEX OF THE FIRST ATOM IN TOP MONOLAYER (OR "BOTTOM" WITH PBC)
    sulfur_index_to_change=top_sulfur_atoms[0]
    # CHANGING POSITIONS OF THE SULFUR ATOM (Z-POSITION)
    copied_traj.xyz[:, sulfur_index_to_change, 2] = z_position_top_sulfur
    
    # USING CENTER OF MASS GROUPING
    if residue_group_type == 'COM':
        ## FINDING CENTER OF MASS OF ALL THE GROUPS I CARE ABOUT
        center_of_mass = find_center_of_mass( traj=traj, residue_name =residue_name, atom_names = atom_names )
        
        # GETTING ALL INDICES OF THE FIRST ATOM IN THE RESIDUE
        atom_index_to_change=[ atom[0] for atom in atom_index ]
        # CHANGING ALL POSITIONS OF THE FIRST ATOM IN RESIDUE TO CENTER OF MASSES
        copied_traj.xyz[:, atom_index_to_change] = center_of_mass[:]

        ### FINDING DISTANCES BETWEEN BOTTOM TO TOP MONOLAYER OF GROUP
        ## CREATING ATOM PAIRS
        atom_pairs = [ [sulfur_index_to_change, x] for x in atom_index_to_change]
    
    # USING ALL ATOM GROUPING
    elif residue_group_type == 'AllAtom':
        ## CREATING ATOM PAIRS
        atom_pairs = [ [sulfur_index_to_change, current_atom_value] for current_residue_index in atom_index for current_atom_value in current_residue_index]

    ## FINDING TOTAL ATOMS / RESIDUE -- NORMALIZATION FACTOR
    atom_per_residue = float(len(atom_pairs) / len(residue_index))
    ## USING MDTRAJ TO CALCULATE DISPLACEMENT WITH NO PERIODIC BOUNDARY CONDITIONS
    displacements = md.compute_displacements( traj=copied_traj, atom_pairs = atom_pairs, periodic = False)
        
    ## GETTING Z DISPLACEMENTS AND SUBTRACTING TO BRING DOWN THE Z-DISTANCE W.R.T SULFUR MONOLAYER
    z_dis = displacements[:,:,2]
    ## CORRECTING Z DISPLACEMENTS FOR PBC
    for time_index in range(len(z_dis)):
        # Finding all z-displacements below the top layer
        isLessThan = np.where(z_dis[time_index] < 0 )
        # For all those displacements, simply add the box length in the z-dimensions
        z_dis[time_index, isLessThan] += box_size_z[time_index] # Adding L
    
    ## DIVIDING DISPLACEMENTS TO GET BIN ASSIGNMENTS
    z_bin_assign = np.floor(z_dis/bin_width).astype('int')
    
    ## FINDING ALL UNIQUE BINS
    bin_indices, bin_occurance = np.unique(z_bin_assign, return_index=False, return_counts=True)
    
    ## FIXING IF THE BIN INDEX IS OUT OF RANGE -- ONLY POSSIBLE IF SOMETHING IS PROTRUDING THROUGH LAYER
    bin_index_logical = bin_indices < max_bin # Logical used to fix bin_indices
    
    # IF TRUE, RE-WRITE BIN INDEX AND OCCURANCES
    if np.any(bin_index_logical):
        print("SOME ATOMS ARE OUTSIDE THE BIN RANGE! THIS COULD CAUSE ERROR IN DENSITY PLOTS")
        bin_indices = bin_indices[bin_index_logical]
        bin_occurance = bin_occurance[bin_index_logical]
    else:
        print("ALL ATOMS ARE WITHIN RANGE -- NO ERROR")
    ## MAY STILL HAVE ERRORS WITH ATOMS PROTRUDING THROUGH THE SULFUR LAYER! -- POSSIBLE SOLUTIONS:
        # FIX SIMULATIONS TO AVOID PROTUSIONS
        # CALCULATE BASED ON THE CENTER OF THE BILAYER
        # ADD THE MISSING OCCURANCES BACK TO THE DENSITY DISTRIBUTION AS A UNIFORM DISTRIBUTION
    
    ## STORING THE BINS
    bin_storage[bin_indices] += bin_occurance
    
    ### NORMALIZATION
    ## GETTING BIN VOLUMES
    # BIN VOLUME
    bin_volume = np.mean(box_size_x * box_size_y * bin_width) # Average bin volume
    # NUMBER OF FRAMES
    num_frames = traj.time.size
    # NUMBER OF GROUPS
#    total_residues = len(residue_index)
#    # TOTAL VOLUME -- debating whether or not we need to divide by density in bulk phase for RDF
    #    box_volume = np.mean( traj.unitcell_volumes )
    #    ideal_gas_density = total_residues / box_volume
    
    # GETTING DENSITY DISTRIBUTION
    density_dist = bin_storage / bin_volume / num_frames / atom_per_residue # / total_residues # / ideal_gas_density <-- currently does not have density
    
    ## AVERAGING BOTH SIDES OF THE PLANE
    density_dist_avg, density_dist_std = average_z_densities(density_dist)
    
    ## COMBINING TO GET AVERAGE AND STD DENSITY DISTRIBUTION
    planar_density = initialize.make_dict_avg_std(density_dist_avg, density_dist_std)
    
    # GETTING DISTANCE VECTOR
    r = np.arange(0, len(density_dist_avg))*bin_width

    return planar_density, r, bin_storage

## MAIN SCRIPT IMPLEMENTING FIND PLANAR DENSITY
def main(traj, bin_width, groups, output_text_dir, specific_dir=None, wantText=True, wantPlot=False):
    '''
    This script implements the "find_planar_density" script and exports to a text file / shows a figure of it (depending on what you want!)
    INPUTS:
        traj: trajectory to run the analysis on
        bin_width: Bin width is the resolution of the density profile
        groups: List of list such that in the first list, you have 3 columns being LABEL (STR), RESIDUE NAME (STR), ATOM LIST (AS A LIST)
        output_text_dir: Location you want to export your text file. The name will be based on the "path_output_data -- i.e. date - script name - output text .csv
        specific_dir: Location of your directory of study -- used as an output variable
        wantPlot: True if you want to see plots
        wantText: True if you want to save the figure
    OUTPUTS:
        csv text file based on the output_text_dir with your data
    '''
    # PRINTING
    print("----------------Running planar density script----------------")
    
    ### CREATING STORAGE BIN
    storage_analysis = []
    if wantPlot is True:
        plt.close('all') # Closing all pre-existing figures
        
    ### LOOPING THROUGH EACH GROUP
    for current_group in groups:
        ## DEFINING RESIDUE NAMES + ATOMS FROM GROUPS
        my_label = current_group[0]
        residue_name = current_group[1]
        atom_names = current_group[2]
        residue_group_type = current_group[3] # CENTER OF MASS / ALL ATOM
    
        ## PRINTING
        print("WORKING ON GROUP: %s"%(my_label))
        print("WORKING ON RESIDUE NAME: %s"%(residue_name))
        print("WORKING ON ATOMS NAME: %s"%(atom_names))
        print("TYPE OF JOB: %s"%(residue_group_type))
    
        ## FINDING DENSITY OF EACH GROUP
        planar_density, r, bin_storage = find_planar_density(traj, residue_name = residue_name, atom_names = atom_names, residue_group_type=  residue_group_type, bin_width = bin_width )
    
        ## STORING EACH GROUP ANALYSIS
        storage_analysis.append({'label': [my_label,residue_name,'-'.join(atom_names)],
                                 'xValues': r,
                                 'yValues': planar_density['avg'],
                                 'y_Err': planar_density['std'],
                                })
    
    ### PLOTTING DENSITY DISTRIBUTION
    plotting_functions.plot_xy_err(x_values = r,
                 y_values = planar_density['avg'],
                 y_err = planar_density['std'],
                 title='Density distribution for label: %s, residue name: %s ' %(my_label, residue_name),
                 xlabel='Distance in the z-dimension (nm)',
                 ylabel='Density (Number of residues / nm^3)',
                 wantPlot=wantPlot  )
    
    ### PREPARATION FOR CSV EXPORT
    ## DEFINING LOCATION TO STORE
    script_output_name="planar_density"
    output_text_name=initialize.getDate() + '-' + specific_dir
    path_to_results_folder = output_text_dir + '\\' + script_output_name
    path_output_data= path_to_results_folder + '\\' + output_text_name + '.csv'
    
    ## CHECKING IF DIRECTORY EXISTS
    initialize.check_dir(initialize.checkPath2Server(path_to_results_folder))
    
    ### CHECKING THE PATH
    path_output_data=initialize.checkPath2Server(path_output_data)
    
    ## CHECKING IF DIRECTORY EXISTS
    initialize.check_dir(path_to_results_folder)
    
    ## DEFINING X AND Y LABELS
    x_labels='r (nm)'
    y_labels='Density (Num/nm^3)'
    
    ## GETTING COMBINED DATA
    combined_list = [ [ storage_analysis[x]['xValues'], storage_analysis[x]['yValues'], storage_analysis[x]['y_Err'],   ] for x in range(len(storage_analysis)) ]
    
    ## EXTENDING THE DATA TO BE (X_1, Y_1, X_2, Y_2, ETC.)
    data = [] # Storage
    for current_index in range(len(combined_list)):
        data.extend( [combined_list[current_index][0], combined_list[current_index][1], combined_list[current_index][2] ] )
    
    ## TRANSPOSING THE DATA
    data = np.array(data).T.tolist()
    
    ## DEFINING LABELS FOR FIRST LINE
    ## GETTING THE LABELS
    first_line=[]
    
    # FINDING NUMBER OF STORAGE
    num_data_storage = len(storage_analysis)
    
    # WRITING LABELS
    for current_index in range(num_data_storage):
        # DEFINING LABELS
        current_labels = storage_analysis[current_index]['label']
        # COMBINING LABELS
        combine_labels = '-'.join(current_labels)
        # WRITING LABEL TO FIRST LINE, skipping one every time
        first_line.extend( [ combine_labels, ' ', ' ' ])
    
    # FIXING FIRST LINE
    first_line_combined = ', '.join(first_line)

    ### EXPORTING TO CSV
    csv_file_functions.export2csv_avg_std(path_output_data, first_line_combined, x_labels, y_labels, data )

    return

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### DIRECTORY TO WORK ON
    analysis_dir=r"171201-ROT_PLANAR" # Analysis directory
    # analysis_dir=r"171205-ROT_PLANAR_GOLP"
    specific_dir=r"Planar_310.15_ROT_TMMA_10x10_CHARMM36_noGold" # Directory within analysis_dir
    # specific_dir=r"Planar_310.15_ROT_TMMA_10x10_CHARMM36_withGOLP" # Directory within analysis_dir
    path2AnalysisDir=r"R:\scratch\LigandBuilder\Analysis\\" + analysis_dir + '\\' + specific_dir # PC Side
    
    ### CHECKING IF WE ARE ON THE SERVER (SWARM)
    path2AnalysisDir = initialize.checkPath2Server(path2AnalysisDir)
    
    ### DEFINING FILE NAMES
    gro_file=r"sam_prod.gro"
    xtc_file=r"sam_prod_10_ns_whole.xtc"
    
    ### LOADING TRAJECTORY
    traj,topology = initialize.load_traj_from_dir( directories = path2AnalysisDir, # Directory to analysis
                                                  xtcFile = xtc_file, # trajectories
                                                  structure_File = gro_file # structure file
                                                  )

    #%% RADIAL DISTRIBUTION FOR PLANAR CASE
    
    ### INPUTS
    ## GROUPS OF INTEREST
    groups=[ ['H_2O', 'HOH', ['O','H1','H2'] ,  'AllAtom'],  # LABEL, RESIDUE NAME, ATOM NAMES (LIST), TYPE_OF_CALC (COM or AllAtom) # ,
            # ['COO', 'COO', ['C67', 'O68', 'O69'], 'AllAtom' ],
            # ['IONS', 'CL', ['CL']]
             ['CC_BACKBONE', 'TMA', [ 'C2', 
                                            'C5',
                                           'C8', # 'H9', 'H10',
                                            'C11',# 'H12', 'H13',
                                            'C14',# 'H15','H16',
                                            'C17',# 'H18','H19',
                                            'C20',# 'H21','H22',
                                            'C23',# 'H24','H25',
                                            'C26',# 'H27','H28',
                                            'C29',# 'H30','H31',
                                            'C32',# 'H33','H34',
                                            ], 'AllAtom' ]
             ]
    
    ## LOGICALS
    wantPlot=True # True if you want a plot
    wantText=True # True if you want a text file saved
    
    ## DEFINING OUTPUT TEXT DIRECTORY
    output_text_dir=r"R:\\scratch\\LigandBuilder\\Scripts\\analysis_scripts\\results\\"

    ## BIN WITH -- RESOLUTION OF RDF
    bin_width=0.2 # nm

    #%%    
    # RUNNING MAIN SCRIPT
    main(traj, bin_width, groups, output_text_dir, specific_dir =specific_dir,  wantText=True, wantPlot=wantPlot )
    


    