# -*- coding: utf-8 -*-
"""
multi_traj_analysis_tool_list.py
The purpose of this script is to load all trajectories, use a corresponding 
function to analyze the trajectories, then save the information for 
subsequent plotting/analysis.

INPUTS:
    directories
    files within directories that you are interested in

Written by: Alex K. Chew (alexkchew@gmail.com, 03/22/2018)
"""
### IMPORTING MODULES
import os

## MULTITRAJ LIST
from MDDescriptors.traj_tools.multi_traj_by_list import multi_traj_by_list
### IMPORTING LIGAND REISDUE NAMES
from MDDescriptors.application.nanoparticle.core.find_ligand_residue_names import ligand_residue_list as ligand_names
## IMPORTING PATHS
from MDDescriptors.core.check_tools import check_multiple_paths, check_dir, check_path

#%% MAIN SCRIPT
if __name__ == "__main__":
    
    ### DEFINING LOGICALS

    ## ANALYSIS TOOLS
    want_ligand_sasa                = False                  # For nanoparticle solvent accessible surface area
    want_nanoparticle_structure     = False                  # For nanoparticle structure
    ## SPATIAL DISTRIBUTION MAPS
    want_spatial_dist_maps          = False                  # For spatial distribution maps
    
    ### GROMACS LOGICALS
    want_analyze_gmx_hbond          = False                  # True if you want hydrogen bonding
    want_analyze_gmx_rmsf           = False                  # True if you want to calculate gmx rmsfS
    want_analyze_gmx_principal      = False                  # True if you want gmx principal
    want_analyze_gmx_gyrate          = False                  # True if you want to calculate gmx rmsfS
    
    ### NEEED TO UPDATE BELOW
    want_analyze_gmx_msd            = False                  # True if you want diffusion coefficients    

    ## SELF ASSMEBLY LIGANDS
    want_self_assembly_ligands      = False                  # For self-assembly ligands
    want_self_assembly_ligands_sulfur_coord = False           # For self-assembly ligands (gold-sulfur coordination)  
    ## BUNDLING GROUPS
    want_bundling_grps              = False                  # True if you want bundling groups
    want_nearby_water_structure     = False                  # True if you want nearby water structure **DEPRECIATED**
    want_nanoparticle_rdf           = False                   # True if you want RDF of the nanoparticle   
    want_gold_ligand_structure      = False                  # True if you want gold-ligand RDFs
    want_sulfur_gold_coordination   = False                  # True if you want sulfur-gold coordination

    
    ## FOR DESCRIPTORS
    want_descriptors = True
    if want_descriptors == True:
        ## GROMACS COMMANDS
        want_analyze_gmx_hbond = True
        want_analyze_gmx_rmsf = True
        want_analyze_gmx_principal = True
        want_analyze_gmx_gyrate = True
        ## SASA CALCULATIONS
        want_ligand_sasa = True
    
    ## DEFINING MAIN DIRECTORY
    main_dir = "191216-ROT_15_16"
    # "191216-ROT_15_16_switch_solvent"
    # "191216-ROT_15_16"
    # "191208-Switching_solvents_ROT_others"
    # "HYDROPHOBICITY_PROJECT_ROTELLO"
    # r"191204-Switching_solvents_ROT_others_rerun"
    # r"191204-Switching_solvents_ROT_others"
    # r"191005-Testing_multiple_cosolvents_NVT_ensemble"
    # r"190909-Switching_solvents_large_box" # <-- ROTELLO LIGANDS DMSO
    # r"190520-2nm_ROT_Sims_updated_forcefield_new_ligands" # <-- ROTELLO LIGANDS (WITH PURE WATER)
    
    # r"190909-Switching_solvents_large_box"
    # r"190913-Most_likely_NP_mixed_solvents_new_solvs_other_solvents"
    # r"190909-Switching_solvents_large_box"
    # r"190520-2nm_ROT_Sims_updated_forcefield_new_ligands"
    # 
    # 
    # r"190911-Most_likely_NP_mixed_solvents_new_solvs_scaled"
    # r"190906-Most_likely_NP_mixed_solvents"
    # r"190906-Most_likely_NP_mixed_solvents_new_solvs"
    # r"190903-Rotello_lig_cyclohexane"
    
    ## DEFINING MAIN DIRECTORY PATH
    path_main_dir = os.path.join( r"R:\scratch\nanoparticle_project\simulations",
                                  main_dir)

    ## DEFINING PATH TO PICKLING DIRECTORY
    path_pickle_dir = r"R:\scratch\nanoparticle_project\scripts\analysis_scripts\PICKLE_ROT"
    
    ### DEFINING EMPTY LISTS
    function_list, function_inputs= [], []
    ### DEFINING SPECIFIC DIRECTORY
    specific_dir = ['MostNP-EAM_300.00_K_2_nmDIAM_C11COOH_CHARMM36jul2017_Trial_1-lidx_1-cosf_10000-formicacid_methanol_phenol_1_molfrac_300',
                    'MostNP-EAM_300.00_K_2_nmDIAM_dodecanethiol_CHARMM36jul2017_Trial_1-lidx_1-cosf_10000-formicacid_methanol_phenol_1_molfrac_300'] # ['switch_solvents-50000-dmso-EAM_300.00_K_2_nmDIAM_ROT006_CHARMM36jul2017_Trial_1']
    specific_dir = [] # Turning specific directory off
    
    
    ## DEFINING SOLVENT LIST
    solvent_list = [ 'HOH', 'MET', 'THF', 'DMSO', 'FMA', 'FMD', 'DMS', 'PHE', 'TOL', 'IND', 'MTH']
    
    ########################
    ### ANALYZE GMX RMSF ###
    ######################## 
    if want_analyze_gmx_rmsf is True:
        from MDDescriptors.application.nanoparticle.extract_gmx_rmsf import analyze_gmx_rmsf
        ## APPENDING THE CLASS
        function_list.append(analyze_gmx_rmsf)
        ## DESCRIPTOR INPUTS
        current_function_input={
                'xvg_file': 'rmsf.xvg'
                }
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
    
    #############################
    ### ANALYZE GMX PRINCIPAL ###
    #############################
    if want_analyze_gmx_principal is True:
        from MDDescriptors.application.nanoparticle.extract_gmx_principal import analyze_gmx_principal
        ## APPENDING THE CLASS
        function_list.append(analyze_gmx_principal)
        ## DESCRIPTOR INPUTS
        current_function_input = {'moi_file': 'moi.xvg'
                                  }    
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
    
    #########################
    ### ANALYZE GMX HBOND ###
    #########################
    if want_analyze_gmx_hbond is True:
        from MDDescriptors.application.nanoparticle.extract_gmx_hbond import analyze_gmx_hbond
        ## APPENDING THE CLASS
        function_list.append(analyze_gmx_hbond)
        ## DESCRIPTOR INPUTS
        current_function_input = { 'hbnum_file': 'hbnum.xvg' }    
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
    
    ##########################
    ### ANALYZE GMX GYRATE ###
    ######################## ##
    if want_analyze_gmx_gyrate is True:
        from MDDescriptors.application.nanoparticle.extract_gmx_gyrate import analyze_gmx_gyrate
        ## APPENDING THE CLASS
        function_list.append(analyze_gmx_gyrate)
        ## DESCRIPTOR INPUTS
        current_function_input = { 'xvg_file': 'gyrate.xvg',
                                   'variable_definition_label': 'GYRATE'}        
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
    
    ###################
    ### LIGAND SASA ###
    ###################
    if want_ligand_sasa is True:
        from MDDescriptors.application.nanoparticle.nanoparticle_sasa import nanoparticle_sasa
        ## APPENDING THE CLASS
        function_list.append(nanoparticle_sasa)
        ## IMPORTING R GROUP
        from MDDescriptors.application.nanoparticle.global_vars import R_GROUPS_PER_LIGAND
        ## DESCRIPTOR INPUTS
        current_function_input = { 'STRUCTURE_FILE': 'sam_prod_10_ns_whole_no_water_center.gro',
                                   'XTC_FILE': 'sam_prod_10_ns_whole_no_water_center.xtc',
                                   'ligand_names': ligand_names,
                                   'itp_file': 'sam.itp',
                                   'group_type' : ['all_atom_ligands'],
                                   'r_group_dict' : R_GROUPS_PER_LIGAND,
                                   'probe_radius' : 0.14,
                                   'n_sphere_points': 960,
                                   'save_disk_space': True,
                                   'debug_mode': False,
                                   'sasa_cutoff': None, # no optimization
                                  }
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
        
    ###################################
    ### NP SPATIAL DISTRIBUTION MAP ###
    ###################################
    if want_spatial_dist_maps is True:
        from MDDescriptors.application.nanoparticle.np_spatial_dist_map import calc_np_spatial_dist_map
        ## APPENDING THE CLASS
        function_list.append(calc_np_spatial_dist_map)
        ## DESCRIPTOR INPUTS
        current_function_input = { 'STRUCTURE_FILE': 'sam_prod.gro',
                                   'XTC_FILE': 'sam_prod.xtc',
                                   'ligand_names': ligand_names,
                                   'itp_file': 'sam.itp',
                                   'solvent_list': solvent_list,
                                   'box_length': 7.5,
                                   'box_size_increments': 0.1, # box cell increments
                                   'map_type'           : 'allatom', # mapping type: 'COM' or 'allatom'
                                   'verbose'            : True,
                                   'separated_ligands'  : False,
                                   'save_disk_space'    : True,
                                  }
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
    
        

    
    # --------------------------------------------------------- NEEDS TO UPDATE
    
    #############################
    ### SELF_ASSEMBLY_LIGANDS ###
    #############################
    if want_self_assembly_ligands is True:
        from MDDescriptors.application.nanoparticle.self_assembly_structure import self_assembly_structure
        ## APPENDING THE CLASS
        function_list.append(self_assembly_structure)
        ## DESCRIPTOR INPUTS
        current_function_input = {
                                'Planar':
                                    {
                                            'gold_shape': 'Planar',
                                            },
                                'hollow':
                                    {
                                            'gold_shape': 'hollow',
                                            },
                                'spherical':
                                    {
                                            'gold_shape': 'spherical',
                                            },
                                'EAM':
                                    {
                                            'gold_shape': 'EAM',
                                            },
                                    }
        ## INCLUDING TO ALL INPUTS
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'ps_per_frame', 50 )  # picosecond per frame
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'split_traj', 200 )  # Number of split frames
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'gold_optimize', False )  # Desired opitmization
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'coord_num_facet_vs_edge_cutoff', 7  )  # Cutoff between facet and edge
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'coord_num_surface_to_bulk_cutoff', 11  )  # Cutoff between surface adn bulk atom coord num
        
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
        
    ##############################################
    ### SELF_ASSEMBLY_LIGANDS GOLD COORDINATES ###
    ##############################################
    if want_self_assembly_ligands_sulfur_coord is True:
        from MDDescriptors.application.nanoparticle.self_assembly_sulfur_gold_coordination import self_assembly_sulfur_gold_coordination
        ## APPENDING THE CLASS
        function_list.append(self_assembly_sulfur_gold_coordination)
        ## DESCRIPTOR INPUTS
        current_function_input = {
                                'hollow':
                                    {
                                            },
                                'spherical':
                                    {
                                            },
                                'EAM':
                                    {
                                            },
                                    }
        ### IMPORTING GLOBAL VARIABLES
        from MDDescriptors.application.nanoparticle.global_vars import GOLD_SULFUR_CUTOFF
        from MDDescriptors.application.nanoparticle.self_assembly_structure import self_assembly_structure
                                    
        ## INCLUDING TO ALL INPUTS
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'frame', -1 )  # which frame to look at
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'save_disk_space', True )  # Saving disk space
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'gold_sulfur_cutoff', GOLD_SULFUR_CUTOFF )  # Saving disk spaceum
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'maximum_num_gold_atoms', 1 )  # max number of gold atoms to look for
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'min_gold_samples', 4 )  #  minimum gold atoms required to be considered a plane
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'analysis_classes', [[ self_assembly_structure, '180814-FINAL' ]] )  #  reloading previous pickle
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'debug', False )  #  turns off debugging
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'want_expand_edges', True )  #  turns off debugging
        
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
        
    ##############################
    ### NANOPARTICLE STRUCTURE ###
    ##############################
    if want_nanoparticle_structure is True:
        from MDDescriptors.application.nanoparticle.nanoparticle_structure import nanoparticle_structure
        ## APPENDING THE CLASS
        function_list.append(nanoparticle_structure)
        ## DESCRIPTOR INPUTS
        current_function_input = {
                                'hollow':
                                    {
                                            
                                            },
                                'spherical':
                                    {
                                            },
                                'EAM':
                                    {
                                            },
                                'Planar':
                                    {
                                            'separated_ligands':True # Separated ligands (they are not bound!)
                                            },
                                            
                                    }
        ## INCLUDING TO ALL INPUTS
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'ligand_names', ligand_names )  # name of ligands
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'itp_file', 'sam.itp' )  # itp file
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'structure_types', ['trans_ratio', 'distance_end_groups' ] )  # structural types
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
        

    
    ####################################
    ### NANOPARTICLE BUNDLING GROUPS ###
    ####################################
    if want_bundling_grps is True:
        from MDDescriptors.application.nanoparticle.nanoparticle_find_bundled_groups import calc_nanoparticle_bundling_groups
        ## APPENDING THE CLASS
        function_list.append(calc_nanoparticle_bundling_groups)
        ## DESCRIPTOR INPUTS
        current_function_input = {
                                'hollow':
                                    {
                                            'separated_ligands': False,  # Separated ligands (they are not bound!)
                                            'itp_file': 'sam.itp',
                                        
                                            },
                                'spherical':
                                    {    'separated_ligands': False,  # Separated ligands (they are not bound!)
                                         'itp_file': 'sam.itp',
                                     
                                            },
                                'EAM':
                                    {   'separated_ligands': False,  # Separated ligands (they are not bound!)
                                         'itp_file': 'sam.itp',
                                            },
                                'Planar':
                                    {
                                            'separated_ligands': True,  # Separated ligands (they are not bound!)
                                            'itp_file': 'match',        # Find all itp files with matching
                                            },
                                    }
        ## INCLUDING TO ALL INPUTS
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'ligand_names', ligand_names )  # name of ligands
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'min_cluster_size', 3 )  # Constraint number of bundling groups
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'weights', [0.5, 0.5] )  # Constraining weights
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'save_disk_space', True )  # Saving disk space
    
        ## INCLUDING INPUTS THAT ARE CUSTOM TO MULTITRAJ
        # current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'structure_file', 'sam_prod_10_ns_whole_no_water_center.gro')  # Solvents you want radial distribution functions for
        # current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'xtc_file', 'sam_prod_10_ns_whole_no_water_center.xtc')  # Solvents you want radial distribution functions for
        
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
    

    ###########################################
    ### NANOPARTICLE NEARBY WATER STRUCTURE ###
    ###########################################
    if want_nearby_water_structure is True:
        from MDDescriptors.application.nanoparticle.nanoparticle_nearby_water_structure import nanoparticle_nearby_water_structure
        ## APPENDING THE CLASS
        function_list.append(nanoparticle_nearby_water_structure)
        ## DESCRIPTOR INPUTS
        current_function_input = {
                                'hollow':
                                    {
                                            'separated_ligands': False,  # Separated ligands (they are not bound!)
                                            'itp_file': 'sam.itp',
                                        
                                            },
                                'spherical':
                                    {    'separated_ligands': False,  # Separated ligands (they are not bound!)
                                         'itp_file': 'sam.itp',
                                     
                                            },
                                'EAM':
                                    {   'separated_ligands': False,  # Separated ligands (they are not bound!)
                                         'itp_file': 'sam.itp',
                                            },
                                'Planar':
                                    {
                                            'separated_ligands': True,  # Separated ligands (they are not bound!)
                                            'itp_file': 'match',        # Find all itp files with matching
                                            },
                                    }
        ## INCLUDING TO ALL INPUTS
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'ligand_names', ligand_names )  # name of ligands
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'save_disk_space', True )  # Saving disk space
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'cutoff_radius', 0.60 )  # Cutoff radius in nm
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'split_traj', 25  )  # Cutoff for splitting trajectories 50
    
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
        
    #########################
    ### NANOPARTICLE RDFs ###
    #########################0
    if want_nanoparticle_rdf is True:
        from MDDescriptors.application.nanoparticle.nanoparticle_rdf import nanoparticle_rdf
        ## APPENDING THE CLASS
        function_list.append(nanoparticle_rdf)
        ## DESCRIPTOR INPUTS
        current_function_input = {
                                'hollow':
                                    {
                                            'separated_ligands': False,  # Separated ligands (they are not bound!)
                                            'itp_file': 'sam.itp',
                                        
                                            },
                                'spherical':
                                    {    'separated_ligands': False,  # Separated ligands (they are not bound!)
                                         'itp_file': 'sam.itp',
                                     
                                            },
                                'EAM':
                                    {   'separated_ligands': False,  # Separated ligands (they are not bound!)
                                         'itp_file': 'sam.itp',
                                            },
                                'Planar':
                                    {
                                            'separated_ligands': True,  # Separated ligands (they are not bound!)
                                            'itp_file': 'match',        # Find all itp files with matching
                                            },
                                    }
        ## INCLUDING TO ALL INPUTS
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'ligand_names', ligand_names )  # name of ligands
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'save_disk_space', True )  # Saving disk space
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'bin_width', 0.02 )  # Bin width for RDF
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'r_range', (0.0, 3.0) )  # Cutoff radius n nm
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'split_traj', 25  )  # Cutoff for splitting trajectories 50
    
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
        
    ##############################
    ### NANOPARTICLE GOLD RDFs ###
    ##############################
    if want_gold_ligand_structure is True:
        from MDDescriptors.application.nanoparticle.nanoparticle_gold_to_ligand_structure import nanoparticle_gold_to_ligand_structure
        ## APPENDING THE CLASS
        function_list.append(nanoparticle_gold_to_ligand_structure)
        ## DESCRIPTOR INPUTS
        current_function_input = {
                                'hollow':
                                    {
                                            'separated_ligands': False,  # Separated ligands (they are not bound!)
                                            'itp_file': 'sam.itp',
                                            'gold_shape': 'hollow',
                                        
                                            },
                                'spherical':
                                    {    'separated_ligands': False,  # Separated ligands (they are not bound!)
                                         'itp_file': 'sam.itp',
                                         'gold_shape': 'spherical',
                                     
                                            },
                                'EAM':
                                    {   'separated_ligands': False,  # Separated ligands (they are not bound!)
                                         'itp_file': 'sam.itp',
                                         'gold_shape': 'EAM',
                                            },
                                'Planar':
                                    {
                                            'separated_ligands': True,  # Separated ligands (they are not bound!)
                                            'itp_file': 'match',        # Find all itp files with matching
                                            'gold_shape': None,
                                            },
                                    }
        ## INCLUDING TO ALL INPUTS
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'ligand_names', ligand_names )  # name of ligands
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'save_disk_space', True )  # Saving disk space
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'bin_width', 0.005 )  # Bin width for RDF
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'r_range', (0.0, 2.0) )  # Cutoff radius n nm
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'split_traj', 25  )  # Cutoff for splitting trajectories 50
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'gold_atom_name', 'Au'  )  # Name for gold
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'coord_num_surface_to_bulk_cutoff', 11  )  # Cutoff between surface adn bulk atom coord num
    
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
        
    #############################################
    ### NANOPARTICLE SULFUR-GOLD COORDINATION ###
    #############################################
    if want_sulfur_gold_coordination is True:
        from MDDescriptors.application.nanoparticle.nanoparticle_sulfur_gold_coordination import nanoparticle_sulfur_gold_coordination
        ## APPENDING THE CLASS
        function_list.append(nanoparticle_sulfur_gold_coordination)
        ## DESCRIPTOR INPUTS
        current_function_input = {
                                'hollow':
                                    {
                                            'separated_ligands': False,  # Separated ligands (they are not bound!)
                                            'itp_file': 'sam.itp',
                                        
                                            },
                                'spherical':
                                    {    'separated_ligands': False,  # Separated ligands (they are not bound!)
                                         'itp_file': 'sam.itp',
                                     
                                            },
                                'EAM':
                                    {   'separated_ligands': False,  # Separated ligands (they are not bound!)
                                         'itp_file': 'sam.itp',
                                            },
                                'Planar':
                                    {
                                            'separated_ligands': True,  # Separated ligands (they are not bound!)
                                            'itp_file': 'match',        # Find all itp files with matching
                                            },
                                    }
        ## IMPORTING IMPORTANT MODULES
        from MDDescriptors.application.nanoparticle.self_assembly_structure import self_assembly_structure
        ## INCLUDING TO ALL INPUTS
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'ligand_names', ligand_names )  # name of ligands
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'save_disk_space', True )  # Saving disk space
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'split_traj', 25  )  # Cutoff for splitting trajectories 50
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'gold_atom_name', 'Au'  )  # Name for gold
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'debug', False  )  # False if you do not want debugging mode on
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'gold_sulfur_cutoff', 0.3600  )  # nm, gold-sulfur cutoff
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'maximum_num_gold_atoms', 1 )  # nm, gold-sulfur cutoff
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'analysis_classes', [[ self_assembly_structure, '180814-FINAL' ]] )  # ANALYSIS CLASS REVIVED
    
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
    
    
    
    #######################
    ### ANALYZE GMX MSD ###
    #######################
    if want_analyze_gmx_msd is True:
        from MDDescriptors.application.nanoparticle.extract_gmx_msd import analyze_gmx_msd
        ## APPENDING THE CLASS
        function_list.append(analyze_gmx_msd)
        ## DESCRIPTOR INPUTS
        current_function_input = {
                                'hollow':
                                    {
                                            },
                                'spherical':
                                    {    
                                            },
                                'EAM':
                                    {   
                                            },
                                'Planar':
                                    {
                                            },
                                    }
        ## INCLUDING TO ALL INPUTS
        current_function_input = add_to_all_dicts_of_dicts(current_function_input, 'xvg_file', 'msd.xvg' )  # Name of moi file
    
        ## APPENDING THE INPUTS
        function_inputs.append(current_function_input)
    

        

        
    

    
    ########################
    ### RUNNING ANALYSIS ###
    ########################
    
    ## DEFINING INPUTS FOR ANALYSIS
    analysis_inputs = {
            'path_main_dir'     : path_main_dir,
            'path_pickle_dir'   : path_pickle_dir,
            'function_list'     : function_list,
            'function_inputs'   : function_inputs,
            'specific_dir'      : specific_dir,
            'verbose'           : True,
            }    
    ## DEFINING ANALYSIS
    analysis = multi_traj_by_list( **analysis_inputs)
    
    

        