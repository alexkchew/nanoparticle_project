#!/bin/bash

# analysis_extraction.sh
# This bash script will take the output files and copy to the current working directory. This is absolutely necessary because we need the .xtc, .gro, and .tpr files for calculating important interactions like hydrogen bonding, etc.
# Written by Alex Chew (03/18/2017)

## VARIABLES:
#   $1: input directory (simulation dir)
#   $2: output directory (analysis dir)
#   $3: want_self_assembled_monolayer: True if you want to swtich to self_assembly directory

## USAGE: bash analysis_extraction.sh INPUT_DIR OUTPUT_DIR True

##*-- Updates --*##
# 17-12-01 - Fixing up script to extract values
# 17-12-15 - Added global variables to LB
# 18-05-06 - Adding variables to script

# Usage: bash analysis_extraction.sh
# NO INPUTS

## LOADING LIGAND BUILDER VARIABLES / FUNCIONS
source "../bin/nanoparticle_rc.sh"

# -- USER-SPECIFIC PARAMETERS -- #

### DEFINING OUTPUT DIRECTORIES
declare -a extractDirList=("$1")  # "Planar_noGold"

# Defining output directory
specific_analysis_dir="$2" # Directory where you want specifically the analysis to occur

## LOGICALS
want_self_assembled_monolayer="$3" # True if you want to change simulation directory to that of the self-assembled monolayer

# -- USER-SPECIFIC PARAMETERS -- #

# Full path for output directory
path2FullAnalysis="$PATH2ANALYSIS/$specific_analysis_dir"

# Making sure there is no duplicate output directory
if [ -e $path2FullAnalysis ]; then
    echo "$path2FullAnalysis already exists, removing duplicates!"
    echo "Waiting 5 seconds until deletion..."
    sleep 5
    rm -r $path2FullAnalysis
fi

# Creating analysis working directory
mkdir -p "${path2FullAnalysis}"
echo "Creating directory for analysis: ${path2FullAnalysis}"

## DEFINING PATH TO SIMULATIONS
path_to_sims="${PATH2SIM}"

if [ "${want_self_assembled_monolayer}" == "True" ]; then
    ## REDEFINING PATH TO SIMS
    path_to_sims="${PREP_GOLD_SIM}"
fi

for extractDir in ${extractDirList[@]}; do
    # Finding all files required
    FilesRequired=$(ls $path_to_sims/$extractDir)
    echo -e "Working on files required in: \n$FilesRequired" 
    echo "---------------------------------------------"

    # First, we need to get all the data files we need - Done by straight copyingcd 
    for currentSeq in $FilesRequired; do
        # Printing working sequence
        echo "Working on: $currentSeq"
        SequenceName=$(basename $currentSeq) # Finds sequence name
        
        # Making directory
        mkdir -p $path2FullAnalysis/$SequenceName
        
        ## DEFINING INPUT AND OUTPUT PATHS
        inputPath="${path_to_sims}/$extractDir/${SequenceName}"
        outputPath="$path2FullAnalysis/$SequenceName"
        
        # Copying xtc, tpr, gro files, etc.

        if [ "${want_self_assembled_monolayer}" == "True" ]; then
            cp -r $inputPath/{*.top,*_equil.xtc,*_equil.tpr,*_equil.gro,*_equil.edr,*.itp} $outputPath 
        else
            cp -r $inputPath/{*.top,*_prod.xtc,*_prod.tpr,*_prod.gro,*_prod.edr,*.itp} $outputPath        
        fi
        
        
        echo -e "Copying xtc, tpr, and gro file from $desiredDirExtr/$currentSeq to $path2FullAnalysis/$SequenceName \n"

    done

done

# Printing what we have done
echo "------------------"
echo "Copied over .xtc, .gro, and .tpr files from:"
echo "${FilesRequired}"
echo "Analysis directory at: $path2FullAnalysis"
