#!/bin/bash

# prep_mixed_solvent_energy_groups.sh
# The purpose of this script is to mix solvents at different lengths.
# The end goal is to use the mixed solvents for screening properties across 
# a nanoparticle surface. 
#
# This script differs from the previous prep_mixed_solvent.sh script by enabling the use of energy groups. 
# By using energy groups, we could disable the ability for cosolvents to aggregate with 
# itself. 
# 
# Note that for a typical nanoparticle simulation, the box size tends to be 
# approximately 7.38 nm large. Therefore, the mixed solvent system should be 
# equivalent or larger -- possibly in 0.50 nm increments. 

# Written by Alex K. Chew (10/03/2019)

## USAGE:
#	bash prep_mixed_solvent.sh dioxane 50 6

## UPDATES
# 20190903 - Patched the box size so atoms are not overlapping that causes LINCS errors

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

### FUNCTION TO GET NAME OF OUTPUT
# The purpose of this function is to get the output name for mixed solvents
# INPUTS:
#	$1: initial_box_length: initial box length of the system
#   $2: system_temp: system temperature
#	$3: cosolvent_perc: percentage of the cosolvent
#	$4: cosolvent_name_: name of the cosolvent system
# OUTPUTS:
#	output_name: output name for mixed solvent directory
function get_output_name_for_mixed_solvents_engrps () {
	## DEFINING INPUTS
    initial_box_length_="$1"
    system_temp="$2"
	cosolvent_perc_="$3"
    cosolvent_name_="$4"
    

	## GETTING OUTPUT NAME
	output_name="${initial_box_length_}_nm-${system_temp}_K-${cosolvent_perc_}_mf-${cosolvent_name_}"
	## PRINTING
	echo "${output_name}"
}


#######################
### INPUT VARIABLES ###
#######################

## DEFINING COSOLVENT NAME
cosolvent_names="methanol,formicacid,phenol" # "$1" # ,phenol

## DEFINING COSOLVENT MASS FRACTION
cosolvent_perc="2"
# "5"
# "$2" # Out of 100

## DEFINING DESIRED SIZE IN NANOMETERS
initial_box_length="7"
# "$3"

## WATER MODEL
water_model="tip3p"

## DEFINING TEMPERATURE
system_temp="300" # Kelvins

## DEFINING TYPE OF PERCENTAGE
perc_type="molfrac" # massfrac

## CONVERTING THE COSOLVENT NAME TO AN ARRAY
IFS=',' read -r -a cosolvent_name_array <<< "${cosolvent_names}"; unset IFS
echo "${cosolvent_name_array[@]}"

## SORTING COSOLVENT NAME ARRAY
IFS=$'\n' cosolvent_name_array=($(sort <<<"${cosolvent_name_array[*]}")); unset IFS
echo "${cosolvent_name_array[@]}"

########################
### DEFINING OUTPUTS ###
########################

## CONVERTING COSOLVENT NAME ARRAY TO STRING
cosolvent_name_string=$(printf "%s_"  "${cosolvent_name_array[@]}")
cosolvent_name_string=${cosolvent_name_string%?} ## REMOVING EXTRA STRING

## GETTING OUTPUT DIR NAME
output_dir_name=$(get_output_name_for_mixed_solvents_engrps ${initial_box_length} ${system_temp} ${cosolvent_perc} ${cosolvent_name_string})
echo "${output_dir_name}"

## DEFINING OUTPUT SUBMIT FILE
output_submit_file="submit.sh"

## DEFINING PATH
path_output="${PREP_MIXED_SOLVENTS_ENGRPS}/${output_dir_name}"

## DEFINING PREFIX
output_prefix="mixed_solvent"

#########################
### DEFAULT VARIABLES ###
#########################
## MOLECULAR WEIGHT OF WATER
MW_water="18.01" # g/mol, molecular weight of water
V_water="0.00996336422" # nm3/molecule
## DEFINING FORCEFIELD
forcefield="${FORCEFIELD}"
## DEFINING NUMBER OF CORES
num_cores="28"
## DEFAULT NUMBER OF PURE SOLVENTS
default_num_solvents="216"
## DEFINING DISTANCE BETWEEN SOLUTE AND EDGE OF BOX
dist="0.05"

####################################################
### SOLVENT INFORMATION GETTING MOLECULAR VOLUME ###
####################################################

## CREATING COSOLVENT MOLECULAR VOLUMES IN NM3/MOLECULE
declare -a cosolvent_molecular_volumes

## SEEING IF MOLECULAR FILE EXISTS
touch "${MOLECULAR_WEIGHT_FILE}"
touch "${MOLECULAR_VOLUME_FILE}"

## LOOPING THROUGH EACH COSOLVENT
for cosolvent in "${cosolvent_name_array[@]}"; do
    
    ## PRINTING
    echo "Computing molecular volume for cosolvent: ${cosolvent}"
    
    ## PATH TO INPUT MOLECULE
    path_input_molecule="${PREP_SOLVENT_FINAL}/${cosolvent}"

    ## CHECKING INPUT FILE
    check_setup_file "${path_input_molecule}" "${cosolvent}"

    ## DEFINING MOLECULE GRO, ITP, PRM FILES
    molecule_gro_file="${cosolvent}.gro"
    molecule_itp_file="${cosolvent}.itp"
    molecule_prm_file="${cosolvent}.prm"

    ##################################
    ### COMPUTING MOLECULAR VOLUME ###
    ##################################

    ### COMPUTING MOLECULAR VOLUME
    mol_volume_cosolvent=$(grep "^${cosolvent}" ${MOLECULAR_VOLUME_FILE} | awk '{print $NF}')

    ## MAKING SURE MW IS CALCULATED, OTHERWISE, ADD IT
    if [ -z "${mol_volume_cosolvent}" ]; then 
        echo "------------------------------------"
        echo "Error! No co-solvent molecular volume"; 
        echo "Attempting to generate molecular volume via number density from:"
        echo "${path_gro_cosolvent_solvated}"

        ## SEEING IF EXISTS
        if [ -e "${path_gro_cosolvent_solvated}" ]; then
            num_density_cosolvent=$(gro_compute_num_density ${default_num_solvents} ${path_gro_cosolvent_solvated})
            ## FINDING VOLUME / 1 MOLECULE
            mol_volume_cosolvent=$(awk -v num_dens=${num_density_cosolvent} 'BEGIN{ printf "%.5f", 1/num_dens }') 
            ## STORING INTO MOLECULAR WEIGHT FILE
            write_molecular_info "${MOLECULAR_VOLUME_FILE}" "${cosolvent}" "${mol_volume_cosolvent}"
            echo "------------------------------------"
        else
            echo "Error! ${path_gro_cosolvent_solvated} does not exist!"
            echo "Run prep_solvents.sh ${cosolvent}"
            echo "Stopping here!"
            sleep 5
            exit 1
        fi
    fi
    
    ## ADDING MOLECULAR VOLUME
    cosolvent_molecular_volumes+=(${mol_volume_cosolvent})
done

#################################################
#### FINDING TOTAL NUMBER OF MOLECULES TO ADD ###
#################################################

## TOTAL VOLUME
total_vol=$(awk -v vol=$initial_box_length 'BEGIN{ printf "%.5f",vol^3}') 

## NUMBER DENSITY OF WATER
path_gro_water_solvated="${INPUT_PACKMOL_PATH}/spc216.gro"
num_density_water=$(gro_compute_num_density ${default_num_solvents} ${path_gro_water_solvated})
## FINDING VOLUME / 1 MOLECULE
mol_volume_water=$(awk -v num_dens=${num_density_water} 'BEGIN{ printf "%.5f", 1/num_dens }') 

## FINDING MOLE FRACTION OF WATER REQUIRED
num_cosolvents=${#cosolvent_name_array[@]}
mol_frac_cosolvents=$(awk -v frac_cos=${cosolvent_perc} 'BEGIN{ printf "%.5f", frac_cos/100 }') 
mol_frac_water=$(awk -v num_cos=${num_cosolvents} -v mol_frac_cos=${mol_frac_cosolvents} 'BEGIN{ printf "%.5f",1 - (num_cos * mol_frac_cos)}') 

## GETTING MOLE FRACTION ARRAY
declare -a mole_frac_array
for cosolvent in "${cosolvent_name_array[@]}"; do mole_frac_array+=("${mol_frac_cosolvents}"); done
mole_frac_array+=("${mol_frac_water}")

## GETTING VOLUME ARRAY
cosolvent_molecular_volumes+=("${mol_volume_water}")

#########################################
### GETTING TOTAL NUMBER OF MOLECULES ###
#########################################
# N = V / (y_a * V_a + y_b * V_b + ...)
denom=0
## LOOPING TO GET DENOMINATOR
for index in $(seq 0 $(( ${#cosolvent_molecular_volumes[@]}-1)) ); do
    denom=$(awk -v denom=${denom} -v cos_mv=${cosolvent_molecular_volumes[index]} -v molfrac=${mole_frac_array[index]} 'BEGIN{ printf "%.8f", denom + cos_mv * molfrac}') 
done
## PRINTING DENOM
total_molecules=$(awk -v denom=${denom} -v vol=${total_vol} 'BEGIN{ printf "%d",vol/denom}') 

## PRINTING
echo "Total number of molecules: ${total_molecules}"

## FINDING TOTAL MOLES FOR EACH ONE
declare -a num_molecules_array
total_molecules_added=0
for index in $(seq 0 $(( ${#cosolvent_molecular_volumes[@]}-1)) ); do
    ## SEEING IF INDEX IS THE LAST ONE (WATER)
    if [[ "${index}" != "$((${#cosolvent_molecular_volumes[@]}-1))" ]]; then
        ## COMPUTING NUM MOLES
        num_molecules=$(awk -v total=${total_molecules} -v mol_frac=${mole_frac_array[index]} 'BEGIN{ printf "%d",total*mol_frac}') 
        ## ADDING TO TOTAL MOLECULES ADDED
        total_molecules_added=$((${total_molecules_added} + ${num_molecules}))
    else
        ## COMPUTING NUM MOLES
        num_molecules=$(awk -v total=${total_molecules} -v moles_added=${total_molecules_added} 'BEGIN{ printf "%d",total-moles_added}') 
    fi
    ## APPENDING TO ARRAY
    num_molecules_array+=("${num_molecules}")
done

################################################
### USING PACKMOL TO ADD WATER AND COSOLVENT ###
################################################
# ADDS COSOLVENT, THEN WATER, shown below
#structure _COSOLVENTPDB_
#  number _NUMCOSOLVENT_
#  inside cube 0. 0. 0. _MAXANGS_
#end structure 
#
#structure _WATERPDB_
#  number _NUMWATER_
#  inside cube 0. 0. 0. _MAXANGS_
#end structure 
#

## CREATING DIRECTORY
create_dir "${path_output}" -f

## GOING INTO DIRECTORY
cd "${path_output}"

## DEFINING PACKMOL SCRIPT
packmol_script="${PACKMOL_SCRIPT}"

## DEFINING PACKMOL INPUT
packmol_input="packmol_mixed_solvents_engrps.inp"
path_packmol_input="${INPUT_PACKMOL_PATH}/${packmol_input}"
#
## DEFINING WATER PDB
path_water_pdb="${INPUT_PACKMOL_PATH}/${water_pdb_name}"

## DEFINING OUTPUT PDB
output_solvated_pdb="${output_prefix}.pdb"

## COPYING INPUT FILE
cp "${path_packmol_input}" "${path_output}"
## EDITING OUTPUT FILE
sed -i "s/_OUTPUTPDB_/${output_solvated_pdb}/g" "${packmol_input}"

## GETTING MAX LENGTH IN ANGS
new_length_angs=$(awk -v lengths=${initial_box_length} 'BEGIN{ printf "%.5f",lengths*10}')

## LOOPING FOR EACH SOLVENT
for index in $(seq 0 $(( ${#cosolvent_molecular_volumes[@]}-1)) ); do
    ## GETTING NUMBER OF MOLECULES
    num_molecules=${num_molecules_array[index]}
    ## SEEING IF INDEX IS THE LAST ONE (WATER)
    if [[ "${index}" != "$((${#cosolvent_molecular_volumes[@]}-1))" ]]; then
        ## DEFINING COSOLVENT
        cosolvent=${cosolvent_name_array[index]}
        ## GETTING PDB FILE NAME
        path_pdb="${PREP_SOLVENT_FINAL}/${cosolvent}/prep_files/${cosolvent}.pdb"
        ## DEFINING PDB FILE NAME
        pdb_name=$(basename ${path_pdb})
    else
        ## DEFINING PDB FILE NAME
        pdb_name="water_${water_model}.pdb"
        ## WATER
        path_pdb="${INPUT_PACKMOL_PATH}/${pdb_name}"
    fi
    
    ## COPYING PDB FILE
    cp "${path_pdb}" "${path_output}"
    ## ADDING TO INPUT SCRIPT
    echo \
"structure ${pdb_name}
  number ${num_molecules}
  inside cube 0. 0. 0. ${new_length_angs}
end structure
" >> "${packmol_input}"
done

## RUNNING PACKMOL
"${PACKMOL_SCRIPT}" < ${packmol_input}

## DEFINING INITIAL GRO FILE
initial_gro_file="${output_prefix}.gro"

## GETTING INITIAL BOX LENGTH
initial_box_length_with_dist=$(awk -v lengths=${initial_box_length} -v distance=${dist} 'BEGIN{ printf "%.5f",lengths + (distance*2) }')

## USING EDIT CONF TO GENERATE GRO FILE
gmx editconf -f ${output_solvated_pdb} -o ${initial_gro_file} -box "${initial_box_length_with_dist}" "${initial_box_length_with_dist}" "${initial_box_length_with_dist}"

## CHECKING IF EXISTS
stop_if_does_not_exist "${initial_gro_file}"

#########################
### DEFINING TOPOLOGY ###
#########################

## DEFINING MDP FILE
input_em_mdp_file="minim_sam_gmx5.mdp"
input_equil_mdp_file="npt_double_equil_gmx5_charmm36_energy_groups.mdp"

## DEFINING MDP FILE
path_mdp_folder="${INPUT_MDP_PATH}/charmm36/Spherical"

## DEFINING NUMBER OF MDP FILE STEPS
mdp_equil_nsteps="5000000" # 10 ns

## DEFINING SUBMISSION FILE
input_submit_file="submit_prep_mixed_solvents_energy_groups.sh"

## DEFINING TOPOLOGY FILE
topology_file="${output_prefix}.top"

## DEFINING PATH
input_path_topology="${INPUT_TOPOLOGY_PATH}/mixed_solvent_engrps.top"

##########################
### COPYING FILES OVER ###
##########################

## COPYING FORCEFIELD FILES 
echo "COPYING FORCE FIELD FILES, ITP, TOPOLOGY, PRM, ETC."
# Force fields
cp -r "${PATH_FORCEFIELD}/${forcefield}" "${path_output}" 
# TOP files
cp -r "${input_path_topology}" "${path_output}/${topology_file}"
# MDP FILES
cp -r "${path_mdp_folder}/"{${input_em_mdp_file},${input_equil_mdp_file}} "${path_output}"
# SUBMISSION FILE
cp -r "${PATH2SUBMISSION}/${input_submit_file}" "${path_output}/${output_submit_file}"

#################################
### EDITING TOPOLOGY/MDP FILE ###
#################################
### FIXING FILES THAT WERE COPIED
sed -i "s/_FORCEFIELD_/${forcefield}/g" "${topology_file}"
sed -i "s/_SOLVENTNAME_/${cosolvent}/g" "${topology_file}" # cosolvent name
sed -i "s/_WATERMODEL_/${water_model}/g" "${topology_file}" # water model

## DEFINING ENERGY GROUPS
energygrps=""
energygrps_exclusion=""

## DEFINING INDEX FILE
index_file="en_grps.ndx"

## MAKING INDEX FILE
gmx make_ndx -f "${initial_gro_file}" -o "${index_file}" >/dev/null 2>&1 << INPUTS
keep 0
q
INPUTS

## COPYING EACH COSOLVENT ITP AND PRM FILE OVER
top_search_keys_prm="_COSOLVENT_PARAMETERS_"
top_search_keys_itp="_COSOLVENT_ITPS_"

## LOOPING THROUGH EACH COSOLVENT
for index in $(seq 0 $(( ${#cosolvent_name_array[@]}-1)) ); do
    ## DEFINING COSOLVENT NAME
    cosolvent="${cosolvent_name_array[index]}"
    ## PATH TO INPUT MOLECULE
    path_input_molecule="${PREP_SOLVENT_FINAL}/${cosolvent}"
    ## DEFINING MOLECULE GRO, ITP, PRM FILES
    molecule_gro_file="${cosolvent}.gro"
    molecule_itp_file="${cosolvent}.itp"
    molecule_prm_file="${cosolvent}.prm"
    ## COPYING ITP FILES
    cp -r "${path_input_molecule}/"{${molecule_itp_file},${molecule_prm_file}} "${path_output}" 
    ## GETTING NUMBER OF MOLECULES
    num_molecules=${num_molecules_array[index]}
    ## FINDING RESIDUE NAME FROM ITP FILE
    resname=$(extract_itp_resname ${molecule_itp_file})
    ## EDITING TOPOLOGY TO INCLUDE PARAMS AND ITP
    # PRM
    topology_add_include_specific "${topology_file}" "${top_search_keys_prm}" "#include \"${cosolvent}.prm\""
    ## DEFINING NEW SEARCH KEY
    top_search_keys_prm="${cosolvent}.prm"
    #ITP
    topology_add_include_specific "${topology_file}" "${top_search_keys_itp}" "#include \"${cosolvent}.itp\""
    ## DEFINING NEW SEARCH KEY
    top_search_keys_itp="${cosolvent}.itp"
    ## EDITING TOPOLOGY TO INCLUDE RESIDUE NUMBERS
    echo "   ${resname}    ${num_molecules}" >> "${topology_file}"
    
    ## ADDING TO ENERGY GROUPS
    energygrps="${energygrps} ${resname}"
    energygrps_exclusion="${energygrps_exclusion} ${resname} ${resname}"

    ## GENERATING INDEX FILE
gmx make_ndx -f "${initial_gro_file}" -n "${index_file}" -o "${index_file}" >/dev/null 2>&1 << INPUTS
r ${resname}
q
INPUTS
    
done

## ADDING WATER MOLECULES TO TOPOLOGY
echo "   SOL    ${num_molecules_array[-1]}" >> "${topology_file}"


## EDITING MDP FILES
sed -i "s/_NSTEPS_/${mdp_equil_nsteps}/g" "${input_equil_mdp_file}"
sed -i "s/_TEMPERATURE_/${system_temp}/g" "${input_equil_mdp_file}"

## ADDING ENERGY GROUP EXCLUSIONS
echo "energygrps = ${energygrps}" >> ${input_equil_mdp_file}
echo "energygrp-excl = ${energygrps_exclusion}" >> ${input_equil_mdp_file}

####### EDITING SUBMISSION FILE
sed -i "s/_JOB_NAME_/${output_dir_name}/g" "${output_submit_file}"
sed -i "s/_NUM_CORES_/${num_cores}/g" "${output_submit_file}"
sed -i "s/_INPUT_EQUIL_MDP_FILE_/${input_equil_mdp_file}/g" "${output_submit_file}"
sed -i "s/_TOPOLOGY_FILE_/${topology_file}/g" "${output_submit_file}"
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" "${output_submit_file}"
sed -i "s/_INDEX_FILE_/${index_file}/g" "${output_submit_file}"

###################################
### RUNNING ENERGY MINIMIZATION ###
###################################
gmx grompp -f ${input_em_mdp_file} -c "${initial_gro_file}" -p "${topology_file}" -o "${output_prefix}_em"
gmx mdrun -v -nt 1 -deffnm "${output_prefix}_em"

## MAKING MOLECULES WHOLE -- necessary for cutoff group
gmx trjconv -f ${output_prefix}_em.gro -s ${output_prefix}_em.tpr  -o ${output_prefix}_em.gro -pbc mol << INPUTS
System
INPUTS

#####################
### PRINTING JOBS ###
#####################
echo -e "--- CREATING SUBMISSION FILE: ${output_submit_file} ---"
echo "Adding ${path_output} to ${JOB_SCRIPT}"
echo "${path_output}/${output_submit_file}" >> ${JOB_SCRIPT}

########################
### PRINTING SUMMARY ###
########################
## DEFINING SUMMARY FILE
summmary_file="${path_output}/summary.txt"
echo "----- SUMMARY -----"
echo "Worked on mixed-solvent for cosolvents: ${cosolvent_name_array[@]}"
echo "Molecular volumes (nm3/mol), water at end: ${cosolvent_molecular_volumes[@]}"
echo "Number of molecules, water at end: ${num_molecules_array[@]}"
echo "Total number of steps for equilibration: ${mdp_equil_nsteps}"
echo "Box size: ${initial_box_length}"
## PRINTING
echo "--- NUMBER OF RESIDUES ---"
echo "YOU'RE ALL GOOD TO GO! Best of luck!"

## ADDING TO SUMMARY
echo "Created by prep_mixed_solvent_energy_groups.sh" >> ${summmary_file}
echo "Worked on mixed-solvent for cosolvents: ${cosolvent_name_array[@]}" >> ${summmary_file}
echo "Molecular volumes (nm3/mol), water at end: ${cosolvent_molecular_volumes[@]}" >> ${summmary_file}
echo "Number of molecules, water at end: ${num_molecules_array[@]}" >> ${summmary_file}
