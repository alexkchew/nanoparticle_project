#!/bin/bash

# calc_gmx_principal.sh
# This bash script will run gmx_principal extraction for the nanoparticle project. 
# USAGE: bash calc_gmx_principal.sh 
# Written by Alex K. Chew (alexkchew@gmail.com, 06/11/2018)

# ------------------ ALGORITHM --------------- #
#INPUTS:
#	• tpr file - contains structure file information
#	• xtc file - contains trajectory information
#	• index file - contains indexing information
#	• selection that we care about (i.e. ligands) <-- may consider including 
#
#OUTPUTS:
#	• moi file - contains principal, minor, etc. axis used to calculate eccentricity
#
#ALGORITHM:
#	1. Define input variables
#	2. Create an index just for ligands only -- could use the ligand list from main directory to incorporate ligand residue name
#Use gmx principal to generate a moi file

## INPUTS:
#   $1: input_tpr_file_: input tpr file 
#   $2: input_xtc_file_: input xtc file
#   $3: input_ndx_file_: input index file
#   $4: output_moment_of_interia_: output moment of interia
#   $5: path to the directory that you are interested in running this script

## UPDATES:
#   20181004-Made it so that only ligands are accounted for --- fixing issues with index files having multiple ligands
#   20190528-Corrected script to correctly load residue names

# NOTE: This script will look into ligand_names based on prep_system

### LOADING PRE-DEFINED VARIABLES / FUNCTIONS
source "../../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

## PRINTING SCRIPT NAME
print_script_name

## DEFINING INPUT VARIABLES
input_tpr_file_="$1" # "sam_prod.tpr"
input_xtc_file_="$2" # "sam_prod_10_ns_whole.xtc"

## DEFINING OUTPUT FILES
output_moment_of_interia_="$3" # "moi.xvg"
output_ligand_index="ligand.ndx"

## DEFINING TEST PATH
path_to_analysis_dir="$4" #"/home/akchew/scratch/nanoparticle_project/analysis/180607-Alkanethiol_Extended_sims/EAM/EAM_310.15_K_4_nmDIAM_hexadecanethiol_CHARMM36_Trial_1"

############################
### FINDING RESIDUE NAME ###
############################

## FINDING NAME OF DIRECTORY
dir_name=$(basename "${path_to_analysis_dir}")

## FINDING LIGAND NAME
ligand_name=$(extract_lig_name_from_dir "${dir_name}")

## FINDING RESIDUE NAME
residue_name_intersection="$(find_residue_name_from_ligand_txt ${ligand_name})"

###################
### MAIN SCRIPT ###
###################

## GOING TO TEST PATH
cd "${path_to_analysis_dir}"

###########################################
### CREATING INDEX FILE FOR LIGAND ONLY ###
###########################################

## CREATING INDEX FILE (LIGAND ONLY)
gmx make_ndx -f "${input_tpr_file_}" -o "${output_ligand_index}" << INPUTS
keep 1
keep 0
r ${residue_name_intersection}
q
INPUTS

#############################
### RUNNING GMX PRINCIPAL ###
#############################
gmx principal -f ${input_xtc_file_} -s ${input_tpr_file_} -n "${output_ligand_index}" -om ${output_moment_of_interia_} << INPUTS
${residue_name_intersection}
INPUTS