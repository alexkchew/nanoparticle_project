#!/bin/bash

# calc_gmx_gyrate.sh
# This bash script will run gmx_gyrate extraction for the nanoparticle project. 
# USAGE: bash calc_gmx_gyrate.sh 
# Written by Alex K. Chew (alexkchew@gmail.com, 12/18/2018)

# ------------------ ALGORITHM --------------- #
#INPUTS:
#	• tpr file - contains structure file information
#	• xtc file - contains trajectory information
#	• index file - contains indexing information
#	• selection that we care about (i.e. ligands) <-- may consider including 
#
#OUTPUTS:
#	• moi file - contains principal, minor, etc. axis used to calculate eccentricity
#
#ALGORITHM:
#	1. Define input variables
#	2. Create an index just for ligands only -- could use the ligand list from main directory to incorporate ligand residue name
#Use gmx principal to generate a moi file

### FUNCTIONS:
#   index_read_list_: reads index list as an array
#   intersection_two_arrays: finds intersection between two arrays

## INPUTS:
#   $1: input_tpr_file_: input tpr file 
#   $2: input_xtc_file_: input xtc file
#   $3: input_ndx_file_: input index file
#   $4: output_moment_of_interia_: output moment of interia
#   $5: path to the directory that you are interested in running this script

## UPDATES:
#   

# NOTE: This script will look into ligand_names based on prep_system

### LOADING PRE-DEFINED VARIABLES / FUNCTIONS
source "../../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

## PRINTING SCRIPT NAME
print_script_name

## DEFINING INPUT VARIABLES
input_tpr_file_="$1" # "sam_prod.tpr"
input_xtc_file_="$2" # "sam_prod_10_ns_whole.xtc"
input_ndx_file_="$3" # "index.ndx"

## DEFINING RESIDUE NAMES
gold_residue_name="AUNP" # Residue name for gold

## DEFINING OUTPUT FILES
output_file="$4" # e.g. gyrate.xvg

## DEFINING TEST PATH
path_to_analysis_dir="$5" #"/home/akchew/scratch/nanoparticle_project/analysis/180607-Alkanethiol_Extended_sims/EAM/EAM_310.15_K_4_nmDIAM_hexadecanethiol_CHARMM36_Trial_1"

### DEFINING OUTPUT FILES
output_index="gyrate.ndx"
output_file="gyrate.xvg" # e.g. gyrate.xvg
output_summary="gyrate.summary"

############################
### READING LIGAND NAMES ###
############################

## READING THE LIGAND NAME FILE
## READING THE FILE WITHOUT COMMENTS AND SPACES
read_file="$(grep -v -e '^$' "${PREP_LIG_FILE}" | grep -v -e '^;')"

### READING FILES AS ARRAYS
readarray -t l_lig_names <<< "$(echo "${read_file}" | awk -F', ' '{print $1}')"
readarray -t l_res_names <<< "$(echo "${read_file}" | awk -F', ' '{print $2}')"

##########################
### READING INDEX FILE ###
##########################

## GOING TO TEST PATH
cd "${path_to_analysis_dir}"

## FINDING THE INDEX LIST
read -a index_list <<< $(index_read_list_ ${input_ndx_file_})

###############################################
#### RUNNING RESIDUE NAME INTERSECTION CODE ###
###############################################
### PRINTING
#echo "FINDING DIFFERENCE BETWEEN LIGAND RESIDUE NAMES:"
#echo "--> Array 1 ${l_res_names[@]}"
#echo "AND INDEX LIST:"
#echo "--> Array 2 ${index_list[@]}"
#
#read -a residue_name_intersection <<< $(intersection_two_arrays "${#index_list[@]}" "${index_list[@]}" "${#l_res_names[@]}" "${l_res_names[@]}")
#echo "RESIDUE NAME FOUND: ${residue_name_intersection[@]}"

############################
### FINDING RESIDUE NAME ###
############################

## FINDING NAME OF DIRECTORY
dir_name=$(basename "${path_to_analysis_dir}")

## FINDING LIGAND NAME
ligand_name=$(extract_lig_name_from_dir "${dir_name}")

## FINDING RESIDUE NAME
residue_name_intersection="$(find_residue_name_from_ligand_txt ${ligand_name})"

### CHECKING IF RESIDUE NAME INTERSECTION CODE SUCCEEDED
if [ -z "${residue_name_intersection}" ]; then
    echo "Error! No residue name found. Check the arrays for index and ligands!"
    echo "Exiting here..."
    exit
else
    echo "Residue name successfully found! Continuing!"
fi
###########################################
### CREATING INDEX FILE FOR LIGAND ONLY ###
###########################################

## CREATING INDEX FILE (LIGAND + GOLD ONLY)
gmx make_ndx -f "${input_tpr_file_}" -o "${output_index}" << INPUTS
keep 0
r ${gold_residue_name} ${residue_name_intersection} 
q
INPUTS

#################################
### RUNNING GMX ANALYSIS TOOL ###
#################################
## CALCULATING GYRATION USING GMX
gmx gyrate -f "${input_xtc_file_}" -s "${input_tpr_file_}" -n "${output_index}" -o "${output_file}" > "${output_summary}" 2>&1 << INPUTS
1
INPUTS