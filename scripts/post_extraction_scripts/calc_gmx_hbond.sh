#!/bin/bash

# calc_gmx_hbond.sh
# This bash script will run gmx hbond
# USAGE: bash calc_gmx_hbond.sh
# Written by Alex K. Chew (alexkchew@gmail.com, 10/04/2018)

## INPUTS:
#   $1: input_tpr_file_: input tpr file 
#   $2: input_xtc_file_: input xtc file
#   $3: input_ndx_file_: input index file
#   $4: input_solvent_residue: input solvent residue name
#   $5: output_hbnum: output hbnum xvg file
#   $6: path to the directory that you are interested in running this script

## OUTPUTS:
#   output_hbnum file that contains number of hydrogen bonds

### LOADING PRE-DEFINED VARIABLES / FUNCTIONS
source "../../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

## PRINTING SCRIPT NAME
print_script_name

## DEFINING INPUT VARIABLES
input_tpr_file_="$1" # "sam_prod.tpr"
input_xtc_file_="$2" # "sam_prod_10_ns_whole.xtc"  
input_ndx_file_="$3" # "index.ndx"

## RESIDUE NAMES
input_solvent_residue="$4" # "SOL"

## DEFINING OUTPUT FILES
output_hbnum="$5" # "hbnum.xvg"
output_index="ligand_water.ndx"

## DEFINING TEST PATH
path_to_analysis_dir="$6" # "/home/akchew/scratch/nanoparticle_project/analysis/180928-2nm_RVL_proposal_runs_fixed/EAM/EAM_310.15_K_2_nmDIAM_ROT001_CHARMM36_Trial_1"

############################
### READING LIGAND NAMES ###
############################

## READING THE LIGAND NAME FILE
## READING THE FILE WITHOUT COMMENTS AND SPACES
read_file="$(grep -v -e '^$' "${PREP_LIG_FILE}" | grep -v -e '^;')"

### READING FILES AS ARRAYS
readarray -t l_lig_names <<< "$(echo "${read_file}" | awk -F', ' '{print $1}')"
readarray -t l_res_names <<< "$(echo "${read_file}" | awk -F', ' '{print $2}')"

##########################
### READING INDEX FILE ###
##########################

## GOING TO TEST PATH
cd "${path_to_analysis_dir}"

## FINDING THE INDEX LIST
read -a index_list <<< $(index_read_list_ ${input_ndx_file_})

############################
### FINDING RESIDUE NAME ###
############################

## FINDING NAME OF DIRECTORY
dir_name=$(basename "${path_to_analysis_dir}")

## FINDING LIGAND NAME
ligand_name=$(extract_lig_name_from_dir "${dir_name}")

## FINDING RESIDUE NAME
residue_name_intersection="$(find_residue_name_from_ligand_txt ${ligand_name})"

###########################################
### CREATING INDEX FILE FOR LIGAND ONLY ###
###########################################

## CREATING INDEX FILE (LIGAND ONLY)
gmx make_ndx -f "${input_tpr_file_}" -o "${output_index}" << INPUTS
keep 1
keep 0
r ${residue_name_intersection}
r ${input_solvent_residue}
q
INPUTS

#########################
### RUNNING GMX HBOND ###
#########################

gmx hbond -f "${input_xtc_file_}" -s "${input_tpr_file_}" -num  "${output_hbnum}" -n ${output_index} << INPUTS
${residue_name_intersection}
${input_solvent_residue}
INPUTS








