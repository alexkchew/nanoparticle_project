#!/bin/bash

# get_gold_with_ligand.sh
# This bash script will only get gold with ligands
# USAGE: bash get_gold_with_ligand.sh
# Written by Alex K. Chew (alexkchew@gmail.com, 09/13/2019)

## INPUTS
#   $1: input tpr file
#   $2: input xtc file
#   $3: output gro file
#   $4: output xtc file
#   $5: output tpr file
#   $6: output pdb file (PYMOL ENABLED)
#   $7: index file
#   $8: path to analysis file

## UPDATES:

## USAGE EXAMPLE:


#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="../../bin/nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${script_dir}/${global_file_name}"
### BEGINNING OF SCRIPT ###

### BEGINNING OF SCRIPT ###

## PRINTING SCRIPT NAME
print_script_name

################################
### DEFINING INPUT FUNCTIONS ###
################################
## DEFINING INPUT VARIABLES
input_tpr_file_="$1" # "sam_prod.tpr"
input_xtc_file_="$2" # "sam_prod_10_ns_whole.xtc"

## DEFINING OUTPUT FILES
output_gro_file_="$3"
output_xtc_file_="$4"
outpur_tpr_file_="$5"
output_pdb_file_="$6"

## DEFINING INDEX FILE
index_file="$7"

## DEFINING TEST PATH
path_to_analysis_dir="$8" #"/home/akchew/scratch/nanoparticle_project/analysis/180607-Alkanethiol_Extended_sims/EAM/EAM_310.15_K_4_nmDIAM_hexadecanethiol_CHARMM36_Trial_1"

## DEFINING CENTERING GOLD RESIDUE NAME
gold_residue_name="AUNP"

###################
### MAIN SCRIPT ###
###################

## GOING TO TEST PATH
cd "${path_to_analysis_dir}"

############################
### FINDING RESIDUE NAME ###
############################

## FINDING NAME OF DIRECTORY
dir_name=$(basename "${path_to_analysis_dir}")

## FINDING LIGAND NAME
ligand_name=$(extract_lig_name_from_dir "${dir_name}")

## FINDING RESIDUE NAME
residue_name_intersection="$(find_residue_name_from_ligand_txt ${ligand_name})"

###########################################
### CREATING INDEX FILE FOR LIGAND ONLY ###
###########################################

## CREATING INDEX FILE (GOLD AND LIGAND)
make_ndx_gold_with_ligand "${input_tpr_file_}" \
                          "${index_file}"   \
                          "${residue_name_intersection}"   \
                          "${gold_residue_name}"  

## DEFINING COMBINED NAME
combined_name="${gold_residue_name}_${residue_name_intersection}"

############################
### RUNNING GMX COMMANDS ###
############################

### FINDING TOTAL FRAMES IN THE CURRENT XTC
checkxtc "${input_xtc_file_}" last_frame total_frames

## WAITING FOR PROCESS TO FINISH
wait

### CREATING GRO AND XTC FILE
## GRO FILE
gmx trjconv -s "${input_tpr_file_}" -f "${input_xtc_file_}" -pbc mol -center -n "${index_file}" -o "${output_gro_file_}" -b "${total_frames}" << INPUTS
${gold_residue_name}
${combined_name}
INPUTS

## XTC FILE
gmx trjconv -s "${input_tpr_file_}" -f "${input_xtc_file_}" -pbc mol -center -n "${index_file}" -o "${output_xtc_file_}" << INPUTS
${gold_residue_name}
${combined_name}
INPUTS
## PDB FILE
gmx trjconv -s "${input_tpr_file_}" -f "${input_xtc_file_}" -pbc mol -center -n "${index_file}" -o "${output_pdb_file_}" << INPUTS
${gold_residue_name}
${combined_name}
INPUTS

## TPR FILE
gmx convert-tpr -s "${input_tpr_file_}" -o "${outpur_tpr_file_}" -n "${index_file}" << INPUTS
${combined_name}
INPUTS

## CLEANING UP ANY EXTRANEOUS DATA
rm \#*
