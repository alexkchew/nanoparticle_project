#!/bin/bash

# rot_trans.sh -- DEPRECIATED?
# This bash script will run gmx_rmsf extraction for the nanoparticle project. 
# USAGE: bash rot_trans.sh
# Written by Alex K. Chew (alexkchew@gmail.com, 04/16/2019)

## INPUTS:
#   $1: input_tpr_file_: input tpr file 
#   $2: input_xtc_file_: input xtc file
#   $3: input_ndx_file_: input index file
#   $4: output_file: output xvg file
#   $5: output xtc file
#   $6: path to the directory that you are interested in running this script

## UPDATES:

## NOTES:
#   This script has been updated to correctly account for rms over time (i.e. see variable output_rms_over_time)

## USAGE EXAMPLE:
#   bash calc_gmx_rmsf.sh sam_prod.tpr sam_prod_10_ns_whole.xtc index.ndx rmsf.xvg sam_prod_10_ns_whole_no_water_center_fit_rotxy_trans.xtc /home/akchew/scratch/nanoparticle_project/analysis/EAM_SPHERICAL_HOLLOW/EAM/EAM_310.15_K_4_nmDIAM_hexadecanethiol_CHARMM36_Trial_1

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="../../bin/nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${script_dir}/${global_file_name}"
### BEGINNING OF SCRIPT ###

### BEGINNING OF SCRIPT ###

## PRINTING SCRIPT NAME
print_script_name

## DEFINING INPUT VARIABLES
input_tpr_file_="$1" # "sam_prod.tpr"
input_xtc_file_="$2" # "sam_prod_10_ns_whole.xtc"
input_ndx_file_="$3" # "index.ndx"

## DEFINING OUTPUT FILES
output_file="$4" # "moi.xvg"
output_xtc_file_="$5"


## DEFINING TEST PATH
path_to_analysis_dir="$6" #"/home/akchew/scratch/nanoparticle_project/analysis/180607-Alkanethiol_Extended_sims/EAM/EAM_310.15_K_4_nmDIAM_hexadecanethiol_CHARMM36_Trial_1"

## INDEX FILE
gold_index_file="gold_system.ndx"
output_ligand_index="ligand.ndx"

## DEFINING CENTER + ROT + TRANS CENTER
center_group="AUNP"

########################
### FILE INFORMATION ###
########################

## DEFINING PDB FILE
rmsf_pdb_file_="xaver.pdb"

## DEFINING OUTPUT RMSF OVER TIME
output_rms_over_time="rmsf_time.xvg"

############################
### READING LIGAND NAMES ###
############################

## READING THE LIGAND NAME FILE
## READING THE FILE WITHOUT COMMENTS AND SPACES
read_file="$(grep -v -e '^$' "${PREP_LIG_FILE}" | grep -v -e '^;')"

### READING FILES AS ARRAYS
readarray -t l_lig_names <<< "$(echo "${read_file}" | awk -F', ' '{print $1}')"
readarray -t l_res_names <<< "$(echo "${read_file}" | awk -F', ' '{print $2}')"

##########################
### READING INDEX FILE ###
##########################

## GOING TO TEST PATH
cd "${path_to_analysis_dir}"

## FINDING THE INDEX LIST
read -a index_list <<< $(index_read_list_ ${input_ndx_file_})

##############################################
### RUNNING RESIDUE NAME INTERSECTION CODE ###
##############################################
## PRINTING
echo "FINDING DIFFERENCE BETWEEN LIGAND RESIDUE NAMES:"
echo "--> Array 1 ${l_res_names[@]}"
echo "AND INDEX LIST:"
echo "--> Array 2 ${index_list[@]}"

read -a residue_name_intersection <<< $(intersection_two_arrays "${#index_list[@]}" "${index_list[@]}" "${#l_res_names[@]}" "${l_res_names[@]}")
echo "RESIDUE NAME FOUND: ${residue_name_intersection[@]}"

### CHECKING IF RESIDUE NAME INTERSECTION CODE SUCCEEDED
if [ -z "${residue_name_intersection}" ]; then
    echo "Error! No residue name found. Check the arrays for index and ligands!"
    echo "Exiting here..."
    exit
else
    echo "Residue name successfully found! Continuing!"
fi
################################################
### CENTERING AND ROTATION/TRANSLATIONAL FIX ###
################################################

## SEEING IF OUTPUT XTC EXISTS
if [ ! -e "${output_xtc_file_}" ]; then

## CREATING INDEX GROUP
index_make_ndx_residue_with_system "${input_tpr_file_}" "${gold_index_file}" "${center_group}"

## FIXING THE PDB
gmx trjconv -f "${input_xtc_file_}" -s "${input_tpr_file_}" -center -pbc mol -o "${output_xtc_file_}" -n "${gold_index_file}" << INPUTS
${center_group}
System
INPUTS

## CREATING XTC FILE WITH CORRECT TRJCONV
gmx trjconv -f "${output_xtc_file_}" -s "${input_tpr_file_}" -center -fit rot+trans -o "${output_xtc_file_}" -n "${gold_index_file}" << INPUTS
${center_group}
${center_group}
System
INPUTS

fi

###########################################
### CREATING INDEX FILE FOR LIGAND ONLY ###
###########################################

## CREATING INDEX FILE (LIGAND ONLY)
gmx make_ndx -f "${input_tpr_file_}" -o "${output_ligand_index}" << INPUTS
keep 1
keep 0
r ${residue_name_intersection}
q
INPUTS

############################
### RUNNING GMX COMMANDS ###
############################

### CREATING INDEX FILE
gmx make_ndx -f "${input_tpr_file}" -o "${index_file}" << INPUT
q
INPUT

### CHECKING IF THE RESIDUE IS WITHIN (Checks if string is empty)
if [ -z "$(grep ${res_name} ${index_file})" ]; then
gmx make_ndx -f "${input_tpr_file}" -o ${index_file} << INPUT
r ${res_name}
q
INPUT
fi

### EXTRACTION AND PLACES CENTER OF MASSES WITHIN BOX
gmx trjconv -s "${input_tpr_file}" -f "${input_xtc_file}" -o "${input_xtc_file%.xtc}_center" -pbc mol -center -n ${index_file} << INPUT
${res_name}
${system_name}
INPUT
# CENTERING ON RESIDUE, OUTPUTTING SYSTEM

### RESTRAINING ROTATION AND TRANSLATIONAL DEGREES OF FREEDOM
gmx trjconv -s "${input_tpr_file}" -f "${input_xtc_file%.xtc}_center" -o "${output_xtc_file}" -fit rot+trans -center -n ${index_file} << INPUT
${res_name}
${res_name}
${system_name}
INPUT
# CENTERING AND ROT+TRANS ON RESIDUE, OUTPUTTING SYSTEM



#-----

## USING RMSD TO GET PDB FILE
gmx rmsf -s "${input_tpr_file_}" -f "${output_xtc_file_}" -ox "${rmsf_pdb_file_}" -n "${output_ligand_index}" -o "${output_file}" << INPUTS
${residue_name_intersection}
INPUTS

## USING RMS COMMAND TO GET THE ROOT MEAN SQUARED ERROR OVER TIME
gmx rms -s "${rmsf_pdb_file_}" -f "${output_xtc_file_}" -o "${output_rms_over_time}" << INPUTS
${residue_name_intersection}
${residue_name_intersection}
INPUTS

## CLEANING UP ANY EXTRANEOUS DATA
rm \#*


# ## RUNNING GMX RMSF
# gmx rmsf -f "${output_xtc_file_}" -s "${input_tpr_file_}" -n "${output_ligand_index}" << INPUTS
# ${residue_name_intersection}
# INPUTS
