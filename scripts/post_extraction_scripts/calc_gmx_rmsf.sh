#!/bin/bash

# calc_gmx_rmsf.sh
# This bash script will run gmx_rmsf extraction for the nanoparticle project. 
# USAGE: bash calc_gmx_principal.sh 
# Written by Alex K. Chew (alexkchew@gmail.com, 04/16/2019)

## INPUTS:
#   $1: input_tpr_file_: input tpr file 
#   $2: input_xtc_file_: input xtc file
#   $3: input_ndx_file_: input index file
#   $4: output_file: output xvg file
#   $5: output xtc file
#   $6: path to the directory that you are interested in running this script

## UPDATES:

## NOTES:
#   This script has been updated to correctly account for rms over time (i.e. see variable output_rms_over_time)

## USAGE EXAMPLE:
#   bash calc_gmx_rmsf.sh sam_prod.tpr sam_prod_10_ns_whole.xtc index.ndx rmsf.xvg sam_prod_10_ns_whole_no_water_center_fit_rotxy_trans.xtc /home/akchew/scratch/nanoparticle_project/analysis/EAM_SPHERICAL_HOLLOW/EAM/EAM_310.15_K_4_nmDIAM_hexadecanethiol_CHARMM36_Trial_1

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="../../bin/nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${script_dir}/${global_file_name}"
### BEGINNING OF SCRIPT ###

### BEGINNING OF SCRIPT ###

## PRINTING SCRIPT NAME
print_script_name

## DEFINING INPUT VARIABLES
input_tpr_file_="$1" # "sam_prod.tpr"
input_xtc_file_="$2" # "sam_prod_10_ns_whole.xtc"

## DEFINING OUTPUT FILES
output_file="$3" # "moi.xvg"

## DEFINING TEST PATH
path_to_analysis_dir="$4" #"/home/akchew/scratch/nanoparticle_project/analysis/180607-Alkanethiol_Extended_sims/EAM/EAM_310.15_K_4_nmDIAM_hexadecanethiol_CHARMM36_Trial_1"

## INDEX FILE
gold_index_file="gold_system.ndx"
output_ligand_index="ligand.ndx"

## DEFINING CENTER + ROT + TRANS CENTER
center_group="AUNP"

########################
### FILE INFORMATION ###
########################

## DEFINING PDB FILE
rmsf_pdb_file_="xaver.pdb"

## DEFINING OUTPUT RMSF OVER TIME
output_rms_over_time="rmsf_time.xvg"

###################
### MAIN SCRIPT ###
###################

## GOING TO TEST PATH
cd "${path_to_analysis_dir}"

############################
### FINDING RESIDUE NAME ###
############################

## FINDING NAME OF DIRECTORY
dir_name=$(basename "${path_to_analysis_dir}")

## FINDING LIGAND NAME
ligand_name=$(extract_lig_name_from_dir "${dir_name}")

## FINDING RESIDUE NAME
residue_name_intersection="$(find_residue_name_from_ligand_txt ${ligand_name})"

###########################################
### CREATING INDEX FILE FOR LIGAND ONLY ###
###########################################

## CREATING INDEX FILE (LIGAND ONLY)
gmx make_ndx -f "${input_tpr_file_}" -o "${output_ligand_index}" << INPUTS
keep 1
keep 0
r ${residue_name_intersection}
q
INPUTS

############################
### RUNNING GMX COMMANDS ###
############################

## USING RMSD TO GET PDB FILE
gmx rmsf -s "${input_tpr_file_}" \
         -f "${input_xtc_file_}" \
         -ox "${rmsf_pdb_file_}" \
         -n "${output_ligand_index}" \
         -o "${output_file}" << INPUTS
${residue_name_intersection}
INPUTS

## USING RMS COMMAND TO GET THE ROOT MEAN SQUARED ERROR OVER TIME
gmx rms -s "${rmsf_pdb_file_}" \
        -f "${input_xtc_file_}" \
        -o "${output_rms_over_time}" << INPUTS
${residue_name_intersection}
${residue_name_intersection}
INPUTS

## CLEANING UP ANY EXTRANEOUS DATA
rm \#*
