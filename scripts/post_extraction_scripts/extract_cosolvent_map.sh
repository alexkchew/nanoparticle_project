#!/bin/bash

# extract_cosolvent_map.sh
# This code use used to run python extraction protocol to analyze the trajectory
# The idea is to run this code and it will run the trajectory, use the cutoff 
# and generate a histogram with counting the number of atoms within a grid
# 
# Written by: Alex K. Chew (alexkchew@gmail.com, 11/2/2019)
# 
# VARIABLES:
#   

#######################
### DEFINING INPUTS ###
#######################

## DEFINING PYTHON SCRIPT
python_script="_PYTHONSCRIPT_"

## NUMBER OF PROCESSORS
n_procs="_NPROCS_"

## SIMULATION FILE
path_sim="_SIMPATH_"
gro_file="_GROFILE_"
xtc_file="_XTCFILE_"

## DEFINING PATH TO GRID
path_grid="_PATHGRID_"

## DEFINING PICKLE PATH
path_pickle="_PICKLEPATH_"

## DEFINING DETAILS FOR HISTOGRAMMING
max_N="_MAXN_"
cutoff="_CUTOFF_"

## RUNNING PYTHON SCRIPT
python3.6 "${python_script}" --path_sim "${path_sim}" \
                             --gro "${gro_file}" \
                             --xtc "${xtc_file}" \
                             --path_pdb "${path_grid}" \
                             --n_procs "${n_procs}" \
                             --max_N "${max_N}" \
                             --cutoff "${cutoff}" \
                             --path_pickle "${path_pickle}"
