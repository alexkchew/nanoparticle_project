#!/bin/bash

# run_cosolvent_map.sh
# The purpose of this code is to generate a cosolvent map 
# around a nanoparticle. The reason we care about these cosolvents 
# is that they have a similarity to proteins. Therefore, 
# by understanding how small molecules could bind to nanoparticles, 
# we could get a deeper understanding to how larger proteins 
# may bind to it. 
#
#
# ALGORITHM:
#   - Get gro and PDB file from gridding functionality -- should output the center of mass of gold
#   - Center the trajectory, then find COM across the trajectory
#   - Center the grid to the COM -- maybe translation tools ?
#   - Use Python to compute the bin locations
#   
# THINGS NEED TO FIGURE OUT:
#   - What cutoff do you use for cosolvent mapping? 0.33 is used for water. We could potentially use the same cutoff.
# VARIABLES:
#   $1: path to main original simulations -- used to get gridding information
#   $2: path to simulation with cosolvent mapping

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="../../bin/nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${script_dir}/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING PATH TO ORIG SIMS
path_main_orig_sims="$1"
# "/home/akchew/scratch/nanoparticle_project/simulations/HYDROPHOBICITY_PROJECT_C11"

## DEFINING PATH TO SIMULATION DIRECTORY
path_sim="$2"
# "/home/akchew/scratch/nanoparticle_project/simulations/191017-mixed_sams_most_likely_sims/MostNP-EAM_300.00_K_2_nmDIAM_C11COOH_CHARMM36jul2017_Trial_1-lidx_1-cosf_10000-aceticacid_formate_methylammonium_propane_1_molfrac_300"

## DEFINING INPUT GRID
nvt_grid_folder="NVT_grid_1" # First most likely configuration

###############################
######## END VARIABLES ########
###############################

#########################
### DEFAULT VARIABLES ###
#########################

## HYD ANALYSIS
hyd_analysis="hyd_analysis"
input_grid_folder="grid-0_1000-0.1"

## DEFINING GRID PDB
grid_pdb="out_willard_chandler.pdb"
## GETTING PREFIX

## DEFINING NEW GRID PDB
new_grid_pdb="wc_aligned.pdb"

## DEFINING PREFIX FOR GRID
grid_sim_prefix="sam_prod"
# "sam_prod-0_1000-watO_grid"
grid_sim_gro="${grid_sim_prefix}.gro"
grid_sim_tpr="${grid_sim_prefix}.tpr"

## DEFINING PREFIX FOR OUTPUT SIMS
output_sim_prefix="sam_prod"

## DEFINING PYTHON  CODE TO GET COM
gold_residue_name="AUNP"
python_get_gro_com="${PYTHON_GET_GRO_COM}"
output_gro_com_summary="com_gro.summary"

## DEFINING INDEX
index_file="gold_ligand.ndx"

## DEFINING MAX N
max_N="10" # Max count for histogramming
cutoff="0.33" # Cutoff for counting

## DIRECTORY TO STORE
output_analysis_dir="cosolvent_mapping"

## DEFINING PATH TO PYTHON SCRIPT
path_python_mapping="${PYTHON_COSOLVENT_MAPPING}"
path_cosolvent_map_bash="${EXTRACT_COSOLVENT_MAPPING}"

## DEFINING NUMBER OF PROCESSORS
n_procs="28"

## SUBMISISON FILE
input_submit="submit_cosolvent_map.sh"
path_input_submit="${PATH2SUBMISSION}/${input_submit}"
output_submit="submit.sh"

## DEFINING SLEEP TIME (USEFUL FOR DEBUGGING)
sleeptime="0"

########################################
### PART 1: FINDING GRIDDING DETAILS ###
########################################
echo "------------------------"
echo "STEP 1: FINDING GRIDDING"
echo "   Sim path: ${path_sim}"
echo ""
## PAUSING
sleep ${sleeptime}

# This portion looks for the gridding information
## GETTING THE BASENAME
sim_name="$(basename ${path_sim})"

## EXTRACTING THE INPUT NAME
read -a extract_sim_name <<< $(extract_mostnp_name ${sim_name})

## GETTING ORIGINAL SIM NAME
orig_sim_name="${extract_sim_name[0]}"

## DEFINING PATH TO ORIGINAL SIMS
path_orig="${path_main_orig_sims}/${orig_sim_name}"
path_orig_grid="${path_orig}/${nvt_grid_folder}"

## DEFINING PATH TO THE GRID
path_grid="${path_orig_grid}/${hyd_analysis}/${input_grid_folder}"
path_grid_pdb="${path_grid}/${grid_pdb}"

## DEFINING GRO FILE
path_orig_gro="${path_orig_grid}/${grid_sim_gro}"
path_output_com_summary="${path_orig_grid}/${hyd_analysis}/${output_gro_com_summary}"

## CHECKING IF EXISTS
stop_if_does_not_exist "${path_orig}"
stop_if_does_not_exist "${path_grid}"

## CHECKING IF SUMMARY WAS MADE
if [ ! -e "${path_output_com_summary}" ]; then
    ## GETTING CENTER OF MASS
    python3.6 "${python_get_gro_com}" --igro "${path_orig_gro}" \
                                      --osum "${path_output_com_summary}" \
                                      --res_list "${gold_residue_name}" 
fi

## EXTRACTING COM OF ORIGINAL
read -a orig_gro_COM <<< $(grep "${gold_residue_name}" "${path_output_com_summary}" | awk {'printf ("%s %s %s", $2 ,$3, $4)'})



##############################################
### PART 2: FINDING COM OF OUTPUT GRO FILE ###
##############################################

## DEFINING PATH OF GRO FILE
path_main_sim_gro="${path_sim}/${output_sim_prefix}.gro"

## GETTING SUMMARY
path_main_sim_summary="${path_sim}/${output_gro_com_summary}"

echo "------------------------"
echo "STEP 2: FINDING COM OF OUTPUT GRO FILE"
echo "   Gro path: ${path_main_sim_gro}"
echo "   Summary path: ${path_main_sim_summary}"
echo ""
sleep ${sleeptime}

## CHECKING IF SUMMARY WAS MADE
if [ ! -e "${path_main_sim_summary}" ]; then
    ## GETTING CENTER OF MASS
    python3.6 "${python_get_gro_com}" --igro "${path_main_sim_gro}" \
                                      --osum "${path_main_sim_summary}" \
                                      --res_list "${gold_residue_name}" 
fi

## EXTRACTING COM OF ORIGINAL
read -a sim_gro_COM <<< $(grep "${gold_residue_name}" "${path_main_sim_summary}" | awk {'printf ("%s %s %s", $2 ,$3, $4)'})

###########################################
### PART 3: COPY GRID AND MOVE THE GRID ###
###########################################

## GOING INTO DIRECTORY
cd "${path_sim}"

echo "------------------------"
echo "STEP 3: COPYING GRID AND TRANSLATING"
echo "   Analysis dir: ${output_analysis_dir}"

## CREATING DIRECTORY FOR ANALYSIS
if [ ! -e "${output_analysis_dir}" ]; then
    mkdir "${output_analysis_dir}"
fi

## DEFINING PATH TO OUTPUT
path_sim_output_analysis="${path_sim}/${output_analysis_dir}"

## DEFINING GRIDDING
path_old_grid_pdb="${path_sim_output_analysis}/${grid_pdb}"
path_new_grid_pdb="${path_sim_output_analysis}/${new_grid_pdb}"

## CHECKING IF GRIDDING OR NEW GRID PDB DOES NOT EXIST
if [[ ! -e "${path_old_grid_pdb}" ]] || [[ ! -e "${path_new_grid_pdb}" ]]; then

    ## GETTING DIFFERENCES 
    declare -a difference_array=()
    for idx in $(seq 0 $((${#orig_gro_COM[@]}-1))); do
        ## GETTING XYZ VALUES
        new_value=${sim_gro_COM[$idx]}
        old_value="${orig_gro_COM[${idx}]}"

        ## COMPUTING DIFFERENCE
        difference_array+=( $(echo "scale=8;${new_value}-${old_value}" | bc ) )
    done

    ## COPYING GRID OVER
    cp "${path_grid_pdb}" "${path_old_grid_pdb}"

    ## MOVING PDB FILE AND TRANSLATING TO CORRECT CENTER OF MASS
    gmx editconf -f "${path_old_grid_pdb}" \
                 -o "${path_new_grid_pdb}" \
                 -translate "${difference_array[@]}"

fi


echo "   Old grid: ${path_old_grid_pdb}"
echo "   New grid: ${path_new_grid_pdb}"
echo ""
sleep ${sleeptime}

###################################################
### PART 4: CREATING GRO FILE WITH THE GNP ONLY ###
###################################################

## DEFINING OUTPUT PDB
frame="10000"
output_pdb="goldnp.pdb"
# "${output_sim_prefix}-gold_lig_only-${frame}.pdb"

echo "------------------------"
echo "STEP 4: GENERATING GRO FILE FOR GNP ONLY"
echo "   Output PDB: ${output_pdb}"
sleep ${sleeptime}

## SEEING IF FILE DOES NOT EXIST
if [ ! -e "${path_sim_output_analysis}/${output_pdb}" ]; then

    ## EXTRACTING NAME OF LIGAND
    read -a extract_orig_name <<< $(np_extract_output_dirname ${orig_sim_name})

    ## GETTING LIGAND NAME
    ligand_name="${extract_orig_name[3]}"
    ## GETTING LIGAND ITP
    lig_itp_file="${ligand_name}.itp"
    ## CHECKING IF PATH EXISTS
    stop_if_does_not_exist "${lig_itp_file}"
    ## FINDING LIGAND RESIDUE NAME
    lig_residue_name="$(itp_get_resname ${lig_itp_file})"

    ## CREATING INDEX FILE
    make_ndx_gold_with_ligand "${path_sim}/${output_sim_prefix}.tpr" \
                              "${path_sim}/${index_file}"   \
                              "${lig_residue_name}"   \
                              "${gold_residue_name}"  

    ## DEFINING COMBINED NAME
    combined_name="${gold_residue_name}_${lig_residue_name}"

## GMX TRJCONV
gmx trjconv -s "${output_sim_prefix}.tpr" \
            -f "${output_sim_prefix}.xtc" \
            -o "${path_sim_output_analysis}/${output_pdb}" \
            -dump "${frame}" \
            -n ${index_file} \
            -pbc mol << INPUTS
${combined_name}
INPUTS

fi

###############################################################
### PART 5: GENERATING SUBMISSION FILE TO RUN ANALYSIS TOOL ###
###############################################################

## DEFINING PICKLE PATH
pickle_name="map-${max_N}-${cutoff}.pickle"
path_pickle="${path_sim_output_analysis}/${pickle_name}"

## DEFINING BASH CODE
cosolvent_map_bash="$(basename ${path_cosolvent_map_bash})"

echo "------------------------"
echo "STEP 5: GENERATING SUBMISSION FILE"
echo "   Bash script: ${cosolvent_map_bash}"
echo "   Submit script: ${output_submit}"
echo ""
sleep ${sleeptime}

## COPYING OVER BASH CODE
cp -r "${path_cosolvent_map_bash}" "${path_sim_output_analysis}/${cosolvent_map_bash}"

## EDITING BASH CODE
sed -i "s#_PYTHONSCRIPT_#${path_python_mapping}#g" "${path_sim_output_analysis}/${cosolvent_map_bash}"
sed -i "s#_NPROCS_#${n_procs}#g" "${path_sim_output_analysis}/${cosolvent_map_bash}"
sed -i "s#_SIMPATH_#${path_sim}#g" "${path_sim_output_analysis}/${cosolvent_map_bash}"
sed -i "s#_GROFILE_#${output_sim_prefix}.gro#g" "${path_sim_output_analysis}/${cosolvent_map_bash}"
sed -i "s#_XTCFILE_#${output_sim_prefix}.xtc#g" "${path_sim_output_analysis}/${cosolvent_map_bash}"
sed -i "s#_PATHGRID_#${path_new_grid_pdb}#g" "${path_sim_output_analysis}/${cosolvent_map_bash}"
sed -i "s#_PICKLEPATH_#${path_pickle}#g" "${path_sim_output_analysis}/${cosolvent_map_bash}"
sed -i "s#_MAXN_#${max_N}#g" "${path_sim_output_analysis}/${cosolvent_map_bash}"
sed -i "s#_CUTOFF_#${cutoff}#g" "${path_sim_output_analysis}/${cosolvent_map_bash}"

## CREATING SUBMISSION FILE
cp -r "${path_input_submit}" "${path_sim_output_analysis}/${output_submit}"

## TOTAL TIME
submission_time="48:00:00" # 2 days

## DEFINING JOB NAME
job_name="${output_analysis_dir}_${sim_name}"

## EDITTING SUBMISSION SCRIPT
sed -i "s#_USER_#${USER}#g" "${path_sim_output_analysis}/${output_submit}"
sed -i "s#_JOBNAME_#${job_name}#g" "${path_sim_output_analysis}/${output_submit}"
sed -i "s#_JOBTIME_#${submission_time}#g" "${path_sim_output_analysis}/${output_submit}"
sed -i "s#_BASHSCRIPT_#${cosolvent_map_bash}#g" "${path_sim_output_analysis}/${output_submit}"

## ADDING TO JOB LIST
echo "Adding to job list: ${path_sim_output_analysis}/${output_submit}"
echo "${path_sim_output_analysis}/${output_submit}" >> "${JOB_SCRIPT}"


