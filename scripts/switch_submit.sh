#!/bin/bash

# switch_submit.sh
# The purpose of this script is to switch the submit.sh file depending on which server you are on, e.g. SWARM or CHTC-HPC, etc.

# Usage: bash switch_submit.sh dir
## VARIBLES:
#   -$1: directory that you want to run this in
#   $2: switch type
#   $3: job type

# Originally written by Alex Chew (04/04/2017)
# 2017-04-09: Updating to work in SWARM too
# 2017-07-13: Using this for side project Huber
# 2017-07-17: Updated directories for this script to be within scripts folder
# 2017-07-26: Updated to include COMET
# 2018-01-05: Incorporated to work with ligandbuilder
# SUBMISSION SCRIPTS
#   submit_transfer_ligand_builder.sh: 
#       submission script for transfer ligand builders
#   submit_nplm_us_sims.sh:
#       submission scripts for nplm umbrella sampling simulations
# submit for US sample:
#   bash switch_submit 20200207-US-sims_NPLM_reverse_ROT001 change_header_multiple umbrella nplm_sims
# submission for cosolvent mapping
#   bash switch_submit 20200424-planar_cosolvent_mapping_planar_rerun change_header cosolvent_mapping
# For stampede plumed uS
#   bash switch_submit.sh 20200615-US_PLUMED_rerun_with_10_spring change_header_multiple umbrella_plumed nplm_sims
# For frozen sams sims
#   

# --- USER-SPECIFIC PARAMETERS (BEGIN) ---

### SOURCING ALL VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

# Getting script name and echoing
script_name=`basename "$0"`
echo -e "\nBEGIN ----- ${script_name} ----- BEGIN \n"

# Defining directories within your simulation folder that you want to work on
declare -a Files_Within_Sim=("$1") # "ETBE" "xylitol" "levoglucosan" declare -a Files_Within_Sim=() # "ETBE" "xylitol" "levoglucosan" 

## DEFINING SWITCH TYPE
switch_type="${2:-change_header}"
# normal: switch normally with changing job name
# change_header: simply changes header of the job
# change_header_multiple: changes header for multiple files. 

## DEFINING HEADER PREFIX 
change_header_multiple_prefix="submit_"

# Defining general submission script name
output_submit_name="submit.sh" # Usually does not change, because of Monitoring jobs script!

# Copies submission script to this if there is already a submission script
# Note that we will always edit the original submission script

## Defining which script you want to switch the submit.sh for ##
read ServerKeyWord walltime numberOfCPUs <<< $(get_hostname_details)

## DEFINING KEY WORD
# ServerKeyWord="STAMPEDE"

## DEFINING JOB TYPE
job_type="${3}"
#   normal: use submit.sh
#   frozen_sams: use frozen sam code
#   umbrella: use umbrella sampling simulation code
#   umbrella_plumed: used umbrella sampling plumed code
#   nplm_pulling: uses nplm pulling code
#   transfer_ligand_builder: uses transfer ligand builder code
#   cosolvent_mapping:
#       cosolvent mapping codes
#   gnp_water: uses 'submit_gnp_water_sims.sh', which is for stampede2

## DEFINING LOCATION TYPE
location_type="${4-sims}"

if [[ "${location_type}" == "sims" ]]; then
    sim_location="${PATH2SIM}"
elif [[ "${location_type}" == "nplm_sims" ]]; then
    sim_location="${PATH2NPLMSIMS}"
else
    echo "Error! Location not defined: ${location_type}"
fi


## SELECTING SUBMISISON FILE
if [[ ${job_type} == "normal" ]]; then
    submitFile="submit.sh" 
elif [[ ${job_type} == "frozen_sams" ]]; then
    submitFile="submit_sam_planar_frozen_sam.sh" 
elif [[ ${job_type} == "umbrella" ]]; then
    submitFile="submit_nplm_us_sims.sh"
elif [[ ${job_type} == "umbrella_plumed" ]]; then
    submitFile="submit_nplm_us_sims_plumed.sh"
elif [[ ${job_type} == "nplm_pulling" ]]; then
    submitFile="submit_run_np_lipid_bilayer_equil_and_pull.sh"
elif [[ ${job_type} == "transfer_ligand_builder" ]]; then
    submitFile="submit_transfer_ligand_builder.sh"
elif [[ ${job_type} == "cosolvent_mapping" ]]; then
    submitFile="submit_cosolvent_mapping.sh"
elif [[ ${job_type} == "gnp_water" ]]; then
    submitFile="submit_gnp_water_sims.sh"
else
    echo "Error! job_type not correctly specified"
    echo "Current job_type: ${job_type}"
    exit 1
fi

echo "----------------------------------------"
echo "Template submission file: ${submitFile}"
echo "----------------------------------------"


## SUBMISSION FILE FOR TRANSFER LIGANDS
# 
## PRINTING
if [[ "${switch_type}" == "change_header_multiple" ]]; then
    echo "Switch type set to change header multiple"
    echo "Prefix we are looking for: ${change_header_multiple_prefix}"
fi

# Defining full path to the current submission file
path_submit_file="${PATH2SUBMITSCRIPT}/${ServerKeyWord}/${submitFile}"

## CHECKING IF EXISTS
if [[ -z "${path_submit_file}" ]]; then
    echo "Error! Submission file is not available"
    echo "Check: ${path_submit_file}"
fi

# Looping through each main simulation directory
for currentSimdir in "${Files_Within_Sim[@]}"; do
    fullPATH2SIM=${sim_location}/${currentSimdir}
    echo "Full path to sim is: ${fullPATH2SIM}"
    
    # Looping through each file within the simulation directory
    for currentdir in $(ls -d ${fullPATH2SIM}/*/ -d); do
        ## PRINTING CURRENT DIRECTORY
        echo "Current Directory is: ${currentdir}" # Full path
        
        ## GOING INTO DIRECTORY
        cd "${currentdir}"
        
        ## IF SWITCH TYPE MULTIPLE, THEN LOOK FOR ALL SUBMISSION SCRIPTS
        if [[ "${switch_type}" == "change_header_multiple" ]]; then
            ## LOCATING ALL PREFIXES
            # read -a submit_array <<< $(ls ${change_header_multiple_prefix}*)
            ## SORTING
            read -a index_array <<< $(ls ${change_header_multiple_prefix}* | sed 's/[^0-9]*//g' | sort -n)
            declare -a submit_array=()
            for each_index in "${index_array[@]}"; do
                submit_array+=("${change_header_multiple_prefix}${each_index}.sh")
            done
            
            
            
        else
            declare -a submit_array=("${output_submit_name}")
        fi
        echo "Total submission files: ${#submit_array[@]}"
        
        ## LOOPING THROUGH EACH SUBMISSION ARRAY
        for output_submit_name in "${submit_array[@]}"; do
            
            # Checking if there is already a submission script. If so, copy it and leave it as submit_copy.sh
            if [ -e ${currentdir}/${output_submit_name} ]; then
                ## DEFINING COPY SUBMIT
                copy_submit_name="orig-${output_submit_name%.sh}.sh" 
                ## CHECKING IF THERE IS AN ORIGINAL COPY
                if [ ! -e ${currentdir}/${copy_submit_name} ]; then
                    echo "${output_submit_name} is already available! Copying to prevent overwrite!" 
                    echo "Backing up ${output_submit_name} to ${copy_submit_name}"
                    cp ${currentdir}/${output_submit_name} ${currentdir}/${copy_submit_name}
                else
                    ## SAVING THE ORIGINAL COPY OVER
                    cp -r "${currentdir}/${copy_submit_name}" "${currentdir}/${output_submit_name}"
                fi
            fi

            # Correcting for the job name
            job_name=$(basename ${currentdir} | sed "s/mdRun\_//g")
            echo "Current job name: ${job_name}"

            ## COPYING JOB NAME
            if [[ "${switch_type}" == "normal" ]]; then
                sed "s/_JOB_NAME_/${job_name}/g" ${path_submit_file} > "${currentdir}/${output_submit_name}"
            ## CHANGING HEADER TYPE
            elif [[ "${switch_type}" == "change_header" ]] || [[ "${switch_type}" == "change_header_multiple" ]]; then
                ## DEFINING TEMPORARY FILE
                temp_submit_file="${currentdir}/${output_submit_name%.sh}_temp.sh"
                ## LOCATING SPECIFIC NUM
                ## SEARCHING LINES FOR EDITABLE REGIONS
                header_line_num="$(grep -n "###SERVER_SPECIFIC_COMMANDS_START" ${output_submit_name} | sed 's/\([0-9]*\).*/\1/' | head -n1)"
                ending_line_num="$(grep -n "###SERVER_SPECIFIC_COMMANDS_END" ${output_submit_name} | sed 's/\([0-9]*\).*/\1/' | tail -n1)"
                ## CREATING TEMP FILE AND RECOMPILING THE SUBMIT FILE
                ### HEADER
                head -n"${header_line_num}" "${output_submit_name}" > "${temp_submit_file}"
                ### MIDDLE
                cat "${path_submit_file}" >> "${temp_submit_file}"
                ### END
                tail -n +"${ending_line_num}" "${output_submit_name}" >>"${temp_submit_file}"
                ## FINDING JOB NAME
                job_name="$(grep "#SBATCH -J" "${output_submit_name}" | awk '{print $NF}')"
                ## COPYING SUBMISSION FILE OVER
                cp -r "${temp_submit_file}" "${output_submit_name}"
                ## CLEANING UP TEMPORARY SUBMIT FILE
                rm "${temp_submit_file}"
                ## UPDATING JOB NAME
                sed -i "s/_JOB_NAME_/${job_name}/g" "${output_submit_name}"

            else
                echo "Error! No switch type: ${switch_type}"
                exit 1
            fi

            ## CHECKING IF THE SUBMISSION IS MEMBRANE SUBMIT -- EXTRA INPUTS
            if [ "${submitFile}" == "np_lipid_membrane_submit.sh" ]; then
                ## READING THE PREVIOUS INFORMATION
                res_np_lig=$(grep "res_np_lig=" ${currentdir}/${copy_submit_name} | grep -o '".*"' | sed 's/"//g')
                ## EDITING SUBMIT FILE
                sed -i "s/_RESNP_LIG_/${res_np_lig}/g" "${currentdir}/${output_submit_name}"
            fi

            # Printing what we did
            echo -e "Adding ${submitFile} from ${path_submit_file} to ${currentdir} \n"

            # Adding to job list
            echo "${currentdir}${output_submit_name}" >> ${JOB_SCRIPT}
        done
        
    done
    
done

### PRINTING SUMMARY ###
echo "----- SUMMARY----- "
if [[ "${switch_type}" != "change_header_multiple" ]]; then
    echo "Replaced ${output_submit_name} with ${ServerKeyWord} server scripts"
fi
echo "Simulation paths replaced: ${Files_Within_Sim[@]}"
echo "Re-added the jobs to ${JOB_SCRIPT}"
echo "Good luck with the job submission!"