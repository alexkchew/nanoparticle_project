#!/bin/bash

# nplm_run_plumed_contacts_analysis.sh
# This script is designed to compute the plumed coordination number 
# across many trajectories. The idea is then to use their 
# switching function as way of computing coordination numbers.
#
# Written by: Alex K. Chew (05/18/2020)
# VARIABLES:
#	$1: simulation directory path
#	$2: input prefix

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## US FORWARD ROT012
input_sim_dir_name="20200120-US-sims_NPLM_rerun_stampede"
input_directory="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

## US FORWARD ROT001
# input_sim_dir_name="20200427-From_Stampede"
# input_directory="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

## USING PLUMED DIRECTORIES

# ROT012
input_sim_dir_name="20200522-plumed_US_initial"
input_directory="US_1-NPLMplumedcontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

# ROT001
input_sim_dir_name="20200522-plumed_US_initial"
input_directory="US_1-NPLMplumedcontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

declare -a distance_array=("5.100")
# 2.900
# "1.300"
#  "1.900" "2.900"
# "1.300"
# "1.900"
# "1.300"
# "5.100"

## LOOPING DISTANCES
for desired_distance in ${distance_array[@]}; do

######################
### DEFINING PATHS ###
######################

## DEFINING INPUT DIRECTORY
path_input_dir="${PATH2NPLMSIMS}/${input_sim_dir_name}/${input_directory}"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING LM RESIDUE NAME
lm_residue_name="DOPC"

## DEFINING GROMACS COMMAND
gromacs_command="gmx_mpi"
# Assume that gmx_mpi version is loaded: load_gmx_2016-6_plumed_2-5-1

## DEFINING INDEX FILE
index_file="plumed_analysis_index.ndx"
plumed_file="plumed_analysis_input.dat"
coord_file="plumed_analysis_COVAR.dat"
perm_file="plumed_analysis_perm_list.dat"
group_file="plumed_analysis_group_list.dat"
log_file="plumed_analysis_group_list.log"
####################################
### DEFINING DIRECTORY STRUCTURE ###
####################################

## FOR INPUT CONFIGS INITIAL
path_input_dir_input_files="${path_input_dir}/1_input_files"
path_input_dir_configs="${path_input_dir}/2_configurations"
path_input_dir_sims="${path_input_dir}/4_simulations"

## DEFINING PATH INPUT SIMS
path_input_dir_sims="${1-${path_input_dir_sims}/${desired_distance}}"

## DEFINING INPUT PREFIX
input_prefix="${2-nplm_prod}"

## DEFINING REFERENCE GRO FILE. IF THIS IS "", THEN WE USE PREFIX GRO
reference_gro="${3-""}"

## DEFINING REWRITE
rewrite="${4-true}"

###################
### MAIN SCRIPT ###
###################

## GOING TO PATH
stop_if_does_not_exist "${path_input_dir_sims}"
cd "${path_input_dir_sims}"

## CREATING INDEX FILE
if [[ ! -e "${index_file}" ]] || [[ "${rewrite}" == true ]]; then
"${gromacs_command}" make_ndx -f "${input_prefix}.tpr" \
							  -o "${index_file}" << INPUTS
keep 0
q
INPUTS

if [[ ! -z "${reference_gro}" ]]; then
	echo "Since reference gro is included, we are using:"
	echo "GRO: ${reference_gro}"
	gro_for_index="${reference_gro}"
else
	gro_for_index="${input_prefix}.gro"
	echo "Since no reference included, using prefix gro: ${gro_for_index}"

fi

## CREATING INDEX FILE FOR TAIL GROUPS THAT ARE ABOVE A Z-CUTOFF
python3.6 "${PYTHON_GET_NPLM_INDEX}" --path_to_sim "${path_input_dir_sims}" \
									 --gro_file "${gro_for_index}" \
									 --index_file "${index_file}" \
									 --lm_residue_name "${lm_residue_name}" \
									 --output_perm_file "${perm_file}" \
									 --output_group_list "${group_file}"

fi


### RUNNING PLUMED
if [[ ! -e "${coord_file}" ]] || [[ "${rewrite}" == true ]]; then

### DEFAULT PLUMED
radius="0.35"
d_max="2"

############################
### CREATING PLUMED FILE ###
############################
## ADDING UNITS
string="# DEFINING UNITS
UNITS LENGTH=nm ENERGY=kj/mol TIME=ps

"
###########################
###### ADDING GROUPS ######
########################### 
string+="# GROUPS
"

for each_line in $(cat "${group_file}"); do

string+="${each_line}: GROUP NDX_FILE=${index_file} NDX_GROUP=${each_line}
"

done

##############################################
###### ADDING COORDINATION CALCULATIONS ######
############################################## 
string+="
# COORDINATION CALCULATIONS
"
for each_line in $(cat "${perm_file}"); do
	## SPLITING THE GROUPS
	group_1="$(awk '{split($0,a,"-"); print a[1]}' <<< ${each_line})"
	group_2="$(awk '{split($0,a,"-"); print a[2]}' <<< ${each_line})"

	## ADDING TO STRING
string+="${each_line}: COORDINATION GROUPA=${group_1} GROUPB=${group_2} SWITCH={RATIONAL NN=6 MM=12 D_0=0.0 R_0=${radius} D_MAX=${d_max}}
"

done


#################################
##### ADDING PRINT FUNCTION #####
#################################

string+="
PRINT ...
    STRIDE=10
    ARG=*
    FILE=${coord_file}
... PRINT

"

## GENERATING PLUMED FILE
echo "${string}" > "${plumed_file}"

## DEFINING TIME STEP
dt="0.002"

## DEFINING MULTI
multi_environ="0"
# default is 0

## RUNNING PLUMED
echo "==> Running PLUMED! Redirecting all outputs to:
${path_input_dir_sims}/${log_file}"
plumed driver --mf_xtc "${input_prefix}.xtc" \
			  --plumed "${plumed_file}" 2>&1 > "${log_file}"

fi

done
