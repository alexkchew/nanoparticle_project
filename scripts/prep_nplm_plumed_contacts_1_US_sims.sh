#!/bin/bash

# prep_nplm_plumed_contacts_1_US_sims.sh
# The purpose of this script is to generate input files 
# for performing umbrella sampling simulations. 
# We will repurpose the umbrella sampling code using gromacs 
# to generate these systems

## FIXING FILE NAMES FOR MISSING DECIMAL PLACES. First run this script to rename 4_simulations
# for each_file in $(ls | sort -V); do
#   old_name="${each_file}"
#     new_name="$(printf "%.1f" "${each_file}")"
#     echo "${old_name} -> ${new_name}"
#     ## MOVING
#     mv "${old_name}" "${new_name}"
# done

## THEN CHANGE: status.info and make sure each one gets a decimal place

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

### ITERATION #1 - C1 AND C10

## USING HYDROPHOBIC CONTACT COORDINATE SYSTEM
input_parent_sim="20200608-Pulling_with_hydrophobic_contacts"
input_sim_folder="NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
# input_sim_folder="NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

## DEFINING OUTPUT
output_parent_sim="20200615-US_PLUMED_rerun_with_10_spring"
output_parent_sim="20200615-US_PLUMED_rerun_with_10_spring_extended"

## ITERATION 2
# input_parent_sim="20200629-Hydrophobic_contacts_iteration_2"
# input_sim_folder="NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-25000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
# input_sim_folder="NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-25000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"
# input_sim_folder="NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-25000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

# input_sim_folder="NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-50000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"
# input_sim_folder="NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT004_1"

## DEFINING ALTERNATIVE PARENT SIM AND PARENT FOLDER -- USED JUST FOR GETTING CONFIGS
# alter_parent_sim="20200629-Pulling_with_hydrophobic_contacts_C10_shorter"
# alter_sim_folder="NPLMplumedhydrophobiccontactspulling-5.100_2_50_10_0.35-50000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

# DEFINING ALTERNATIVE PARENT SIM AND PARENT FOLDER -- USED JUST FOR GETTING CONFIGS
# alter_parent_sim="20200629-Hydrophobic_contacts_iteration_2_shorter"
# alter_sim_folder="NPLMplumedhydrophobiccontactspulling-5.100_2_50_10_0.35-25000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

## DEFINING OUTPUT SIM NAME
# output_parent_sim="20200713-US_PLUMED_iter2"
# "20200615-US_PLUMED_rerun_with_10_spring"
# "20200615-Debugging_C10_spring_hydrophobic_contacts"
# "20200609-US_with_hydrophobic_contacts"
# "20200522-plumed_US_initial"


#### ITERATION 1: FOR BENZENE MODIFIED FF PARAMETERS
# input_parent_sim="20200821-Hydrophobic_contacts_pulling_Bn_new_FF"
# input_sim_folder="NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-50000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"
# input_sim_folder="NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

# DEFINING OUTPUT
# output_parent_sim="20200822-Hydrophobic_contacts_PMF_Bn_new_FF"

## FOR C10 ONLY
if [[ "${input_sim_folder}" == "NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1" ]]; then
declare -a desired_configs=("0.0" "2.5" "5.0" "10.0" "15.0" "20.0" "25.0" "30.0" "35.0" "40.0" "45.0" "50.0" \
                            "55.0" "60.0" "65.0" "70.0" "75.0" "80.0" "85.0" "90.0" "95.0" "100.0" \
                            "7.5" "12.5" "17.5" "22.5" "27.5" "32.5" "37.5" "42.5" "47.5" \
                            "52.5" "57.5" "62.5" "67.5" "72.5" "77.5" "82.5" "87.5" "92.5" "97.5" \
                            "102.5" "105.0" "107.5" "110.0" "112.5" "115.0" "117.5" "120.0" "122.5" "125.0" \
                            "127.0" "130.0" "132.5" "135.0" "137.5" "140.0" "142.5" "145.0" "147.5" "150.0"
                            ) # "127.5"
else

## DEFAULT CONFIGS
declare -a desired_configs=("0.0" "2.5" "5.0" "10.0" "15.0" "20.0" "25.0" "30.0" "35.0" "40.0" "45.0" "50.0" \
                            "55.0" "60.0" "65.0" "70.0" "75.0" "80.0" "85.0" "90.0" "95.0" "100.0" \
                            "7.5" "12.5" "17.5" "22.5" "27.5" "32.5" "37.5" "42.5" "47.5" \
                            "52.5" "57.5" "62.5" "67.5" "72.5" "77.5" "82.5" "87.5" "92.5" "97.5" \
                            "102.5" "105.0" "107.5" "110.0" "112.5" "115.0" "117.5" "120.0" "122.5" "125.0" \
                            "127.5" "130.0" "132.5" "135.0" "137.5" "140.0" "142.5" "145.0" "147.5" "150.0"
                            )

fi

# declare -a desired_configs=(
#                             )

# "102.5" "105.0" "107.5" "110.0" "112.5" "115.0" "117.5" "120.0" "122.5" "125.0" \
# "127.5" "130.0" "132.5" "135.0" "137.5" "140.0" "142.5" "145.0" "147.5" "150.0"

# 

## DEFINING CONFIGS
# declare -a desired_configs=("127.0")
# "128.0"
# "127.5" "145.0" "147.5"
# "2.5"


#  "2.5" "5.0" 
# "0.0"  "5" 

# 

## TRUE OR FALSE TO MOVE ALL SUBMIT TO ARCHIVE
archive_submit=true
# false

## DEFINING NEW DESIRED CONFIG FOR C10
# declare -a desired_configs=("0")

## DEFINING CONTACTS WITH HIGHER SPRING CONSTANTS
declare -a contacts_higher_spring=("")
# "3.0"
# "3.0"
## DEFINING SPRING WITH HIGHER CONSTANT
higher_spring_constant=20

# 7.5 12.5 17.5 22.5 27.5 32.5 37.5 42.5 47.5 52.5 57.5 62.5 67.5 72.5 77.5 82.5 87.5 92.5 97.5

# "15" "25" "35" "45" "55" "65" "75" "85" "95" "105" "115" "125" "135" "145"
# "0" "5" "10" "20" "30" "40" "50" "60" "70" "80" "90" "100" "110" "120" "130" "140" "150"
#  
# 
# "5"
# "110"
# "0" "5" "10" "20" "30" "40" "50" "60" "70" "80" "90" "100" "110" "120" "130" "140" "150"
# "100" "110" "120" "130" "140" "150" "160" "170" "180" "190" "200"
# "230"
#  "240"
# "5"
# "160" "170" "180" "190" "200" "210" "220"
# "110" "120" "130" "140" "150"
# "110"
# "0" "10" "20" "30" "40" "50" "60" "70" "80" "90" "100"
#  "500"
# "5.100" "5.300" "5.500" "5.700" "5.900"

## DEFINING SPRING CONSTANT
declare -a spring_constant=("10")
# "20"
# "25"
# "30" "40"
# "1" "5" "10" "20" "50"
# "1"
# "0" "0.0005" "0.005" "0.05" "1" "5" "10" "25" "50"
# "0.005" "0.05"
# "0.5" "1" "5"
# "10" "25" "50"
# "10" "25" 

## TURNING DEBUG ON OR OFF
# True if you want shorter simulations
want_debug=false
# true
# false
# true
# false

## DEFINING MAX WARN
num_max_warn=5

## CONVERTING CONFIGS TO COMMAS
config_comma_list="$(printf "%s," "${desired_configs[@]}")"
## REMOVING END COMMA
config_comma_list="${config_comma_list%,}"

## DEFINING NUMBER SPLIT
num_split="30"
# Split the job 30 ways
# "20"
# SPLITTING JOBS 25 WAYS

#########################
### DEFAULT VARIABLES ###
#########################

## REDEFINING NANOPARTICLE NAME
np_lig_name="NP_LIGANDS"
lm_grp_name="TOP_TAILGRPS"

## CHECKING IF YOU WANT HYDROPHOBIC CONTACTS AS REACTION COORDINATE
if [[ "${input_sim_folder}" = "NPLMplumedhydrophobiccontactspulling"* ]]; then
    ## REDEFINING NANOPARTICLE NAME
    np_lig_name="NP_ALK_RGRP"
    echo "Since input folder specifies hydrophobic contacts, we are switching reaction coordinate to:"
    echo "NP: ${np_lig_name}"
    echo "LM: ${lm_grp_name}"

    ## DEFINING OUTPUT NAME PREFIX
    output_name_prefix="UShydrophobic"

else
    ## DEFINING OUTPUT NAME PREFIX
    output_name_prefix="US"

fi


## DEFINING CONFIGS THAT NEED EXTENED TIMES
declare -a extended_configs=() # "1.300" "1.500" "1.700"
# "10" "20" "30" "40" "50" "60" "70" "80" "90" "100" "110" "120" "130" "140" "150" "160" "170" "180" "190" "200"

## DEFINING SYSTEM PARAMETERS
temperature="300.00"

## DEFINING INPUT PREFIX
input_prefix="nplm_pulling"

## DEFINING INPUT TOP
input_top="nplm.top"

## DEFINING INPUT INDEX
input_ndx="nplm.ndx"

## DEFINING PLUMED COORD
plumed_column="coord"

## DEFINING PLUMED INPUT
plumed_input="plumed_input.dat"

## PLUMED FILE
input_covar="COVAR.dat"

## PYTHON SCRIPT
python_find_configurations="${MDBUILDER}/umbrella_sampling/umbrella_sampling_get_desired_configs.py"

## DEFINING OUTPUT CONFIG SUMMARY
config_summary="config_summary.csv"

## DEFINING SLEEP TIME
sleeptime="1"

## DEFINING GROMACS
gromacs_command="gmx_mpi"

## DEFINING DEFAULT FILE TO MONITOR
job_info="status.info"

## DEFINING TIME STEP 
dt="0.002"

## DEFINING PRODUCTION TIME STEPS
if [[ "${want_debug}" == true ]]; then
    equil_time_steps="1000"
    prod_time_steps="500000" # 1 ns
    # "2500000"
    # "1000" # 1 ps
    # "5000" # 10 ps
    default_prod_time="1000.000" # 1 ns
else
    equil_time_steps="250000" # 500 ps
    prod_time_steps="15000000" # 30 ns
	prod_time_steps="10000000" # 30 ns
    prod_time_steps="25000000" # 50 ns
    prod_time_steps="40000000" # 80 ns
    # "10000000" # 20 ns
    # "5000000" # 10 ns
    # "25000000" # 50 ns
    # default_prod_time="20000.000" # 20 ns
    # default_prod_time="50000.000" # 20 ns
    default_prod_time="80000.000" # 20 ns
	# default_prod_time="20000.000" # 20 ns
    # "10000.000" # 1 ns
fi

## DEFINING EXTENDED PROD TIME
# extend_prod_time="50000.000" # 20 ns
extend_prod_time="80000.000" # 20 ns
# extend_prod_time="30000.000" # 20 ns
# extend_prod_time="20000.000" # 20 ns

### PLUMED VARIABLES

## DEFINING RADIUS
radius="0.35"
# "0.5" # 0.5 nm cutoff for contacts
# spring="500" # kJ/mol/nm2
# "500"
# "2000" # kJ/mol/nm2
# nl_stride="1" # Frequency to update list

## DEFINING DMAX
d_max="2"
# "4"

## NEIGHBOR LIST CUTOFF
nl_cutoff="${d_max}"

## LOOPING THROUGH SPRING CONSTANT
for spr_const in ${spring_constant[@]}; do

########################
### OUTPUT VARIABLES ###
########################

## DEFINING OUTPUT PREFIX
output_prefix="nplm"

## DEFINING THE NAME 
output_name="${output_name_prefix}_${spr_const}-${input_sim_folder}"

######################
### DEFINING PATHS ###
######################

## INPUT PATH
path_input_sim_folder="${PATH2NPLMSIMS}/${input_parent_sim}/${input_sim_folder}"

## PATH TO ALTER FOLDER
if [[ ! -z "${alter_sim_folder}" ]]; then
    path_alter_folder="${PATH2NPLMSIMS}/${alter_parent_sim}/${alter_sim_folder}"
else
    path_alter_folder=""
fi

## OUTPUT PATH
path_output_sim_folder="${PATH2NPLMSIMS}/${output_parent_sim}/${output_name}"

################################
### MDP AND SUBMISSION FILES ###
################################

## DEFINING MDP FILES
em_mdp="em.mdp"
equil_mdp="plumed_us_equil.mdp"
prod_mdp="plumed_us_prod.mdp"

## SUBMISSION FILE
run_submit_file="run_nplm_umbrella_sampling_plumed.sh"
run_multiple_index="run_nplm_us_sims_multiple_index_array.sh"
submit_script="submit_nplm_us_plumed_sims.sh"
# "submit_nplm_us_sims.sh"
####################################
### DEFINING DIRECTORY STRUCTURE ###
####################################

## CONTAINS ALL INPUT INFORMATION
output_inputs="${path_output_sim_folder}/1_input_files"
## CONTAINS ALL GRO CONFIGURATIONS
output_configs="${path_output_sim_folder}/2_configurations"
## CONTAINS ALL SUBMISSION FILES
output_submit="${path_output_sim_folder}/3_submit_files"
## CONTAINS ALL SIMULATIONS
output_sims="${path_output_sim_folder}/4_simulations"
## CONTAINS ALL ANALYSIS
output_analysis="${path_output_sim_folder}/5_analysis"
## CONTAINS ALL ARCHIVE SUBMISSION
output_archive="${path_output_sim_folder}/6_archive_submission"

## SUB DIRECTORYS WITHIN SIM
output_sub_dir_0_em="0_EM"
output_sub_dir_1_md="1_MD"

## DEFINING SUB DIRECTORY
output_inputs_mdp_folder="${output_inputs}/mdp_files"

##################################
### STEP 0: COPYING FILES OVER ###
##################################

## CREATING DIRECTORY IF NECESSARY
if [ ! -e "${path_output_sim_folder}" ]; then
    mkdir -p "${path_output_sim_folder}"
fi
## GOING TO PATH
cd "${path_output_sim_folder}"

## CREATING SUB FOLDERS
mkdir -p "${output_inputs}" \
         "${output_configs}" \
         "${output_submit}" \
         "${output_sims}" \
         "${output_analysis}" \
         "${output_inputs_mdp_folder}" \
         "${output_archive}"

echo "----------------------------------"
echo "--- STEP 0: COPYING OVER FILES ---"
echo "----------------------------------"

## COPYING TOPOLOGY
cp -r "${path_input_sim_folder}/${input_top}" "${output_inputs}/${output_prefix}.top"

## COPYING ALL FORCE FIELDS ASSOCIATED WITH THE TOP FILE
copy_top_include_files "${path_input_sim_folder}/${input_top}" "${output_inputs}"

## COPYING INDEX FILE
index_file="${output_prefix}.ndx"
cp -r "${path_input_sim_folder}/${input_ndx}" "${output_inputs}/${index_file}"

## MDP FILE PATH
mdp_parent_path="${INPUT_MDP_PATH}/charmm36"
mdp_file_path="${mdp_parent_path}/lipid_bilayers"

## PATH TO MDP FILE
path_output_equil_mdp="${output_inputs_mdp_folder}/${equil_mdp}"
path_output_prod_mdp="${output_inputs_mdp_folder}/${prod_mdp}"

## COPYING MDP FILES
cp "${mdp_file_path}/"{${em_mdp},${equil_mdp},${prod_mdp}} "${output_inputs_mdp_folder}"

## EDITING MDP FILE
sed -i "s#_NSTEPS_#${equil_time_steps}#g" "${path_output_equil_mdp}"
sed -i "s#_NSTEPS_#${prod_time_steps}#g" "${path_output_prod_mdp}"

## EDITING TEMPERATURE
sed -i "s#_TEMPERATURE_#${temperature}#g" "${path_output_equil_mdp}" "${path_output_prod_mdp}"

## CHANGING THE DT TIME
sed -i "s#_DT_#${dt}#g" "${path_output_equil_mdp}" "${path_output_prod_mdp}"


## COPYING SUBMISSION FILE
cp "${PATH2SUBMISSION}/${run_submit_file}" "${output_submit}"
echo "Editing submission script: ${run_submit_file}"

## EDITING SUBMISSION FILE
path_output_run_file="${output_submit}/${run_submit_file}"
## DEFINING JOB STATUS
job_status="current_job_status.info"

sed -i "s/_SIMFOLDER_/$(basename ${output_sims})/g" "${path_output_run_file}"
sed -i "s/_INPUTFOLDER_/$(basename ${output_inputs})/g" "${path_output_run_file}"
sed -i "s/_INPUTTOPFILE_/${output_prefix}.top/g" "${path_output_run_file}"
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" "${path_output_run_file}"
sed -i "s/_US_MDPFILE_/${prod_mdp}/g" "${path_output_run_file}"
sed -i "s/_EQUIL_MDPFILE_/${equil_mdp}/g" "${path_output_run_file}"
sed -i "s/_STATUSFILE/${job_info}/g" "${path_output_run_file}"
sed -i "s/_CURRENTJOBSTATUS_/${job_status}/g" "${path_output_run_file}"
sed -i "s/_MAXWARN_/${num_max_warn}/g" "${path_output_run_file}"

## COPYING RC

## DEFINING MDP FILE ARRAY
declare -a mdp_file_array=("${equil_mdp}" "${prod_mdp}")

## OUTPUT PREFIX
declare -a output_prefix_array=("${output_prefix}_equil" "${output_prefix}_prod")

## DEFINING COVAR OUTPUT
covar_output_array=()
plumed_array=()
## LOOPING THROUGH EACH PREFIX
for each_prefix in ${output_prefix_array[@]}; do
    covar_output_array+=("${each_prefix}_COVAR.dat")
    plumed_array+=("${each_prefix}_plumed_input.dat")
done

## CONVERTING ARRAY TO STRINGS
mdp_file_array_string=$(join_array_to_string " " ${mdp_file_array[@]})
output_prefix_array_string=$(join_array_to_string " " ${output_prefix_array[@]})
plumed_array_string=$(join_array_to_string " " ${plumed_array[@]})

cp -r "${PATH_GENERAL_RC}" "${output_submit}"

sed -i "s/_RCFILE_/${GENERAL_RC_NAME}/g" "${path_output_run_file}"
sed -i "s/_PLUMEDINPUT_/${plumed_input}/g" "${path_output_run_file}"
sed -i "s/_INDEXFILE_/${index_file}/g" "${path_output_run_file}"

sed -i "s/_MDP_FILE_ARRAY_/${mdp_file_array_string}/g" "${path_output_run_file}"
sed -i "s/_SIM_PREFIX_ARRAY_/${output_prefix_array_string}/g" "${path_output_run_file}"
sed -i "s/_PLUMEDARRAY_/${plumed_array_string}/g" "${path_output_run_file}"


########################################################
### STEP 1: GENERATE CONFIGS USING PLUMED COVAR FILE ###
########################################################

echo "-------------------------------"
echo "--- STEP 1: FINDING CONFIGS ---"
echo "-------------------------------"

## DEFINING DECIMALS
decimal_places="%0.1f"

## DEFINING PATH TO OUTPUT CSV
path_config_csv="${output_configs}/${config_summary}"

## DEFINING PATH TO COVAR
path_to_input_covar="${path_input_sim_folder}/${input_covar}"

## PATH TO ALTER FOLDER
if [[ ! -z "${alter_sim_folder}" ]]; then
    echo "Since alternative folder supplied, we are using configs from a separate file"
    echo "Path: ${path_alter_folder}"
    sleep 2
    path_to_input_covar="${path_alter_folder}/${input_covar}"
fi

## RUNNING PYTHON CODE
python3.6 "${python_find_configurations}" \
            --sum "${path_to_input_covar}" \
            --out "${path_config_csv}" \
            --configs "${config_comma_list}" \
            --dim "${plumed_column}" \
            --is_plumed \
            --oformat "${decimal_places}"
            
# --reverse

##################################
### STEP 2: GENERATING CONFIGS ###
##################################

echo "--------------------------------"
echo "--- STEP 2: GENERATE CONFIGS ---"
echo "--------------------------------"

## READING EACH LINE
while read -r line; do
    ## GETTING INDEX
    index=$(awk -F',' '{print $1}' <<< ${line})
    ## GETTING ACTUAL DISTANCE
    desired_dist=$(awk -F',' '{print $2}' <<< ${line})
    ## GETTING ACTUAL DIST
    actual_dist=$(awk -F',' '{print $3}' <<< ${line})
    ## GETTING TIME
    time_ps=$(awk -F',' '{print $4}' <<< ${line})
    
    ## GETTING DISTANCE IN THREE DECIMALS
    desired_dist_3dec=$(printf "${decimal_places}" ${desired_dist}) # "%0.3f"
    
    ## PRINTING
    echo "Generating configurations: ${index}, desired: ${desired_dist_3dec}, actual ${actual_dist}, time ${time_ps} ps"
    
    ## DEFINING NAME
    config_name="${desired_dist_3dec}.gro"
    
    ## GETTING PS TIME - 10
    time_ps_under_10=0
    # $(awk -v time_ps=${time_ps} 'BEGIN{ printf "%.3f", time_ps-100 }')

    ## CHECKING IF TIME IS LESS
    if (( $( echo "${time_ps_under_10} < 0" | bc -l ) )); then
        ## SETTING TO ZERO
        echo "Setting initial time to zero since it is under 10"
        time_ps_under_10=0
    fi

    if (( $( echo "${time_ps} < 10" | bc -l ) )); then
        ## SETTING TO ZERO
        time_ps=0
        echo "Setting time_ps to 0 since the time in picosecond is less than 10"
    fi


## CHECKING IF EXISTING
if [ ! -e "${output_configs}/${config_name}" ]; then

## DEFINING INPUT PATH
if [[ ! -z "${alter_sim_folder}" ]]; then
    path_input="${path_alter_folder}"
else
    path_input="${path_input_sim_folder}"
fi

## EXTRACTING GRO FILE
${gromacs_command} trjconv -f "${path_input}/${input_prefix}.xtc" \
            -s "${path_input}/${input_prefix}.tpr" \
            -o "${output_configs}/${config_name}" \
            -b "${time_ps_under_10}" \
            -dump "${time_ps}" > /dev/null 2>&1 << INPUTS &
System      
INPUTS

else
    echo "Configuration is complete for: ${config_name}"

fi
done <<< "$(tail -n+2 ${path_config_csv})"
## PRINTING
echo "Waiting for configurations to complete! Time: $(date)"
wait
echo "Configuration extraction is complete at: $(date)"

sleep "${sleeptime}"

sleep 10

###############################################
### STEP 3: ADDING MDP FILES AND SUBMISSION ###
###############################################

echo "-------------------------------------------"
echo "--- STEP 3: GENERATE MDP AND SUBMISSION ---"
echo "-------------------------------------------"

## PATH TO MDP FILE
path_output_equil_mdp="${output_inputs_mdp_folder}/${equil_mdp}"
path_output_prod_mdp="${output_inputs_mdp_folder}/${prod_mdp}"

## PATH TO INPDEX
path_output_index="${output_inputs}/${output_prefix}.ndx"

## DEFINING PATH TO JOB OUTPUT
path_job_info="${path_output_sim_folder}/${job_info}"

## READING LAST INDEX
if [ -e "${path_job_info}" ]; then
    index="$(tail -n1 "${path_job_info}" | awk '{print $1}' )"
    ## ADDING TO INDEX
    index=$(( ${index}+1 ))
else
    ## CREATING JOB INFO
    touch "${path_job_info}"
    index="0"
fi

## LOOPING FOR EACH CONFIGURATION
for each_configuration in ${desired_configs[@]}; do
    ## GETTING DISTANCE IN THREE DECIMALS
    desired_dist=$(printf "${decimal_places}" ${each_configuration})
    ## DEFINING NAME
    config_name="${desired_dist}.gro"
    ## DEFINING PATH TO CONFIG
    path_config_gro="${output_configs}/${config_name}"
    ## CHECKING IF SPRING CONSTANT NEEDS TO BE HIGHER
    if [[ ! " ${contacts_higher_spring[@]} " =~ " ${desired_dist} " ]]; then
        ## ADDING SPRING CONSTANT TO THE NAME
        output_sim_name="${desired_dist}"
        ## DEFINING CURRENT KAPPA
        current_kappa="${spr_const}"
    else
        ## ADDING SPRING CONSTANT TO THE NAME
        output_sim_name="${desired_dist}_${higher_spring_constant}"
        ## DEFINING CURRENT KAPPA
        current_kappa="${higher_spring_constant}"
    fi


    ## DEFINING PATH TO SPECIFIC SIM
    path_current_sim="${output_sims}/${output_sim_name}"
    
    ## DEFINING MDP FILE
    path_current_sim_mdp_prod="${path_current_sim}/${prod_mdp}"
    path_current_sim_mdp_equil="${path_current_sim}/${equil_mdp}"
    path_current_sim_mdp_em="${path_current_sim}/${em_mdp}"
    
    ## ADDING EM AND PROD DIRECTORIES
    mkdir -p "${path_current_sim}"
    
    ## GOING TO PATH
    cd "${path_current_sim}"
    
    ## COPYING GRO TO OUTPUT
    cp -r "${path_config_gro}" "${output_prefix}.gro"

    ## COPYING INDEX FILE
    cp -r "${path_output_index}" "${path_current_sim}/${output_prefix}.ndx"

    if [ ! -e "${path_current_sim}/${output_prefix}_em.tpr" ]; then
    ## GENERATING TPR FILE FOR EM STEP
    ${gromacs_command} grompp -f "${output_inputs_mdp_folder}/${em_mdp}" \
               -c "${output_prefix}.gro" \
               -p "${output_inputs}/${output_prefix}.top" \
               -o "${output_prefix}_em.tpr" \
               -n "${output_prefix}.ndx" \
               -maxwarn "${num_max_warn}" > /dev/null 2>&1 &
    fi
    # NOTE: If you ever get "segmentation error" for this, it may be that your file 
    # name is too long. If so, consider shorternining the output file name. 
    
    ## ADDING MDP FILE
    if [ ! -e "${path_current_sim_mdp_em}" ]; then
        cp "${output_inputs_mdp_folder}/${em_mdp}" "${path_current_sim_mdp_em}"
    fi

    ## PRODUCTION FILE
    if [ ! -e "${path_current_sim_mdp_prod}" ]; then
        cp "${path_output_prod_mdp}" "${path_current_sim_mdp_prod}"
    fi
    
    ## EQUIL FILE
    if [ ! -e "${path_current_sim_mdp_equil}" ]; then
        cp "${path_output_equil_mdp}" "${path_current_sim_mdp_equil}"
    fi
    
    ## SEEING TOTAL TIME
    if [[ ! " ${extended_configs[@]} " =~ " ${desired_dist} " ]]; then
        ## DEFINING PROD
        current_prod_time="${default_prod_time}"
    else
        ## EXTENDED TIME
        current_prod_time="${extend_prod_time}"
    fi
    

    ## GENERATING PLUMED INPUT FILE
    for idx in $(seq 0 $((${#output_prefix_array[@]}-1))); do
        ## DEFINING COVAR FILE
        covar_file="${covar_output_array[idx]}"
        plumed_input="${plumed_array[idx]}"

string="
# DEFINING UNITS
UNITS LENGTH=nm ENERGY=kj/mol TIME=ps

# NP GROUP (LIGANDS)
coms1: GROUP NDX_FILE=${index_file} NDX_GROUP=${np_lig_name}

# LIPID MEMBRANE GROUPS (TAIL GROUPS ONLY)
coms2: GROUP NDX_FILE=${index_file} NDX_GROUP=${lm_grp_name}

## DEFINING COORDINATION NUMBER
coord: COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL NN=6 MM=12 D_0=0.0 R_0=${radius} D_MAX=${d_max}} NLIST NL_CUTOFF=${nl_cutoff} NL_STRIDE=1000

# MOVING RESTRAINT
restraint: ...
    RESTRAINT
    ARG=coord
    AT=${desired_dist}
    KAPPA=${current_kappa}
...


PRINT ...
    STRIDE=100
    ARG=*
    FILE="${covar_file}"
... PRINT

ENDPLUMED" 

    ## PRINTING TO PLUMED
    echo "${string}" >"${plumed_input}"

    done

    ## SEEING IF VALUE IS ADDED TO JOB
    read -a distances_array <<< $(awk '{ print $2 }' ${path_job_info})
    ## CHECKING IF VALUE IS THERE
    if [[ ! " ${distances_array[@]} " =~ " ${desired_dist} " ]]; then

        ## ADDING INFORMATION TO JOB
        echo "${index} ${output_sim_name} ${current_prod_time}" >> ${path_job_info}
        ## PRINTING
        echo "Generating TPR file and MDP file for: ${index}, ${output_sim_name}"
        ## ADDING TO INDEX
        index=$(( ${index}+1 ))
    else
        ## LOCATING THE LINE
        line_num=$(awk '{ print $2 }' ${path_job_info} | grep -nE "${output_sim_name}" | sed 's/\([0-9]*\).*/\1/' | head -n1)
        line_text="$(sed -n ${line_num}p ${path_job_info})"
        ## GETTING INDEX
        current_index=$(awk '{print $1}'<<< "${line_text}")
        
        ## DEFINING CURRENT JOB LINE
        current_job_line="${current_index} ${output_sim_name} ${current_prod_time}"
        ## REPLACE THE LINE
        sed -i "s/${line_text}/${current_job_line}/g" ${path_job_info}
    
        ## PRINTING
        echo "Refreshing job index: ${current_index}, desired: ${output_sim_name}"

    fi
    
done 

##################################
### STEP 4: SUBMISSION SCRIPTS ###
##################################

echo "---------------------------------"
echo "--- STEP 4: SPLITTING WINDOWS ---"
echo "---------------------------------"

## GOING TO PATH
cd "${path_output_sim_folder}"

## CLEANING

## MOVING ANY SUBMISSIN FILES TO ARCHIVE (SILENT MOVE)
if [[ "${archive_submit}" == true ]]; then
    echo "Cleaning up output and moving to archive!"
    mv -vf "orig-submit"* "${output_archive}" 2>/dev/null
    mv -vf "submit_"* "${output_archive}" 2>/dev/null 
    mv -vf "slurm."*".out"* "${output_archive}" 2>/dev/null
    mv -vf "slurm-"*".out"* "${output_archive}" 2>/dev/null
    mv -vf "output_run_"*".log"* "${output_archive}" 2>/dev/null
fi

## GETTING TOTAL WINDOWS
total_windows="${#desired_configs[@]}"

if [[ -z "${num_split}" ]]; then

    ## DEFINING NUMBER OF SPLITS
    if [[ "${want_debug}" == true ]]; then
        num_split="1"
    else
        num_split="${total_windows}"
    fi

fi

## FINDING LAST WINDOW BASED ON -1
final_window_based_on_zero=$((${total_windows}-1))

## FINDING DIVIDED VALUES INTO MULTIPLE JOBS (ROUNDING DOWN)
divided_value="$((${total_windows} / ${num_split}))"

## GETTING TOTAL NUMBER OF WINDOWS
echo "Total windows: ${total_windows}"
echo "Total number of splits is: ${num_split}"
echo "Divided value: ${divided_value}"

## OUTPUT SUBMIT
output_submit_prefix="submit"
output_run_prefix="run"

## DEFINING NUMBER OF CORES, ETC.
## RUN INFORMATION
num_cores="28"

## DEFINING EM NUMBER OF CORES
em_num_cores="28"

## DEFINING MD RUN COMMAND
mdrun_command="gmx_mpi mdrun -ntomp"

## DEFINING SUFFIX
mdrun_command_suffix=""

## DEFINING GROMACS COMMAND
gromacs_command="gmx_mpi"

## LOOPING THROUGH EACH SPLIT
for each_split_index in $(seq 0 $((${num_split}-1)) ); do

    ## GETTING INDICES
    ### INITIAL SPLIT
    if [ "${each_split_index}" == 0 ]; then
        initial_lambda="$((${each_split_index}*${divided_value}))"
        final_lambda="$(( ${each_split_index}+${divided_value}-1 ))"
    ### FINAL SPLIT
    elif [ "${each_split_index}" == "$((${num_split}-1))" ]; then
        initial_lambda="$((${final_lambda}+1))"
        final_lambda="${final_window_based_on_zero}"
    ### MIDDLE SPLITS
    else
        initial_lambda="$((${final_lambda}+1))"
        final_lambda="$(( ${initial_lambda}+${divided_value}-1 ))"
    fi

    ## GETTING ARRAY
    read -a array_lambdas <<< $(seq ${initial_lambda} "${final_lambda}")

    ## GETTING EMPTY ARRAY
    declare -a split_index_arrays=()

    ## LOOPING THROUGH EACH INDEX
    for idx in ${array_lambdas[@]}; do
        ## GETTING DISTANCE
        each_configuration="${desired_configs[idx]}"
        ## GETTING DISTANCE IN 0 DECIMALS
        desired_dist=$(printf "${decimal_places}" ${each_configuration})

        ## CHECKING IF SPRING CONSTANT NEEDS TO BE HIGHER
        if [[ ! " ${contacts_higher_spring[@]} " =~ " ${desired_dist} " ]]; then
            ## ADDING SPRING CONSTANT TO THE NAME
            output_sim_name="${desired_dist}"
        else
            ## ADDING SPRING CONSTANT TO THE NAME
            output_sim_name="${desired_dist}_${higher_spring_constant}"
        fi

        ## GETTING LINE NUMBER (Look for 2nd column, then output sim name, removing all details)
        line_num=$(awk '{print $2}' < ${path_job_info} | grep -nE -w "${output_sim_name}" | sed 's/\([0-9]*\).*/\1/')
        ## FINDING THE INDEX
        current_split_index=$(sed -n "${line_num}p" ${path_job_info} | awk '{print $1}' )
        ## STORING EACH SPLIT PATH INDEX
        split_index_arrays+=("${current_split_index}")

    done

    echo "Index ${each_split_index}: ${split_index_arrays[@]}"

    ## DEFINING SUBMISSION SCRIPT
    path_split_submit="${path_output_sim_folder}/${output_submit_prefix}_${each_split_index}.sh"
    path_split_run="${path_output_sim_folder}/${output_run_prefix}_${each_split_index}.sh"

    ## DEFINING JOB NAME
    job_name="${output_name}_${each_split_index}"

    ## PRINTING
    echo "Creating submission and run: $(basename ${path_split_submit}) and $(basename ${path_split_run})"

    ## COPYING SUBMISSION
    cp -r "${PATH2SUBMISSION}/${submit_script}" "${path_split_submit}"
    cp -r "${PATH2SUBMISSION}/${run_multiple_index}" "${path_split_run}"

    ## GETING ARRAY AS LIST
    split_index_arrays_string=$(join_array_to_string " " ${split_index_arrays[@]})

    ## EDITING RUN FILE
    sed -i "s/_INDEXARRAY_/${split_index_arrays_string}/g" "${path_split_run}"
    sed -i "s#_RUNSCRIPT_#./$(basename ${output_submit})/${run_submit_file}#g" "${path_split_run}"

    ## EDITING SUBMISSION FILE
    sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${path_split_submit}"
    sed -i "s/_JOB_NAME_/${job_name}/g" "${path_split_submit}"
    sed -i "s/_EMNUMCORES_/${em_num_cores}/g" "${path_split_submit}"
    sed -i "s#_MDRUNCOMMAND_#${mdrun_command}#g" "${path_split_submit}"
    sed -i "s#_MDRUNCOMMANDSUFFIX_#${mdrun_command_suffix}#g" "${path_split_submit}"
    sed -i "s#_RUNSCRIPT_#./$(basename ${path_split_run})#g" "${path_split_submit}"
    sed -i "s#_GROMACSCOMMAND_#${gromacs_command}#g" "${path_split_submit}"

    ## ADDING TO JOB LIST
    echo "${path_split_submit}" >> "${JOB_SCRIPT}"

done



done