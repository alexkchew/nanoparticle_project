#!/bin/bash

# prep_mixed_solvent.sh
# The purpose of this script is to mix solvents at different lengths.
# The end goal is to use the mixed solvents for screening properties across 
# a nanoparticle surface. 
# 
# Note that for a typical nanoparticle simulation, the box size tends to be 
# approximately 7.38 nm large. Therefore, the mixed solvent system should be 
# equivalent or larger -- possibly in 0.50 nm increments. 

# Written by Alex K. Chew (08/02/2019)

## USAGE:
#	bash prep_mixed_solvent.sh dioxane 50 6

## UPDATES
# 20190903 - Patched the box size so atoms are not overlapping that causes LINCS errors

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

### FUNCTION TO GET NAME OF OUTPUT
# The purpose of this function is to get the output name for mixed solvents
# INPUTS:
#	$1: cosolvent: name of the cosolvent
#	$2: cosolvent_perc: percentage of the cosolvent
#	$3: system_temp: temperature of the mixed-solvent system
#	$4: initial_box_length: initial box length of the system
#	$5: perctype: type of percentage it is (e.g. massfrac)	
# OUTPUTS:
#	output_name: output name for mixed solvent directory
function get_output_name_for_mixed_solvents () {
	## DEFINING INPUTS
	cosolvent_="$1"
	cosolvent_perc_="$2"
	system_temp_="$3"
	initial_box_length_="$4"
	perctype_="${5-massperc}"	
	
	## GETTING OUTPUT NAME
	output_name="${initial_box_length_}_${cosolvent_}_${cosolvent_perc_}_${perctype_}_${system_temp_}"
	## PRINTING
	echo "${output_name}"
}


#######################
### INPUT VARIABLES ###
#######################

## DEFINING COSOLVENT NAME
cosolvent="$1"

## DEFINING COSOLVENT MASS FRACTION
cosolvent_perc="$2" # Out of 100

## DEFINING DESIRED SIZE IN NANOMETERS
initial_box_length="$3"

## WATER MODEL
water_model="tip3p"

## DEFINING TEMPERATURE
system_temp="300" # Kelvins

## DEFINING TYPE OF PERCENTAGE
perc_type="massfrac"

#########################
### DEFAULT VARIABLES ###
#########################
## MOLECULAR WEIGHT OF WATER
MW_water=18.01 # g/mol, molecular weight of water
## DEFINING FORCEFIELD
forcefield="${FORCEFIELD}"
## DEFINING NUMBER OF CORES
num_cores="28"
## DEFAULT NUMBER OF PURE SOLVENTS
default_num_solvents="216"
## DEFINING DISTANCE BETWEEN SOLUTE AND EDGE OF BOX
dist="0.05"

###########################
### SOLVENT INFORMATION ###
###########################

## PATH TO INPUT MOLECULE
path_input_molecule="${PREP_SOLVENT_FINAL}/${cosolvent}"

## CHECKING INPUT FILE
check_setup_file "${path_input_molecule}" "${cosolvent}"

## DEFINING MOLECULE GRO, ITP, PRM FILES
molecule_gro_file="${cosolvent}.gro"
molecule_itp_file="${cosolvent}.itp"
molecule_prm_file="${cosolvent}.prm"

########################
### DEFINING OUTPUTS ###
########################

## GETTING OUTPUT DIR NAME
output_dir_name=$(get_output_name_for_mixed_solvents ${cosolvent} ${cosolvent_perc} ${system_temp} ${initial_box_length} ${perc_type} )

## DEFINING OUTPUT SUBMIT FILE
output_submit_file="submit.sh"

## DEFINING PATH
path_output="${PREP_MIXED_SOLVENTS}/${output_dir_name}"

## DEFINING PREFIX
output_prefix="mixed_solvent"

################################################
### FINDING TOTAL NUMBER OF MOLECULES TO ADD ###
################################################

## NUMBER DENSITY OF WATER
path_gro_water_solvated="${INPUT_PACKMOL_PATH}/spc216.gro"
num_density_water=$(gro_compute_num_density ${default_num_solvents} ${path_gro_water_solvated})
## FINDING VOLUME / 1 MOLECULE
mol_volume_water=$(awk -v num_dens=${num_density_water} 'BEGIN{ printf "%.5f", 1/num_dens }') 

## NUMBER DENSITY OF COSOLVENT
path_cosolvent_solvated="${NP_PREP_SOLVENT_EQUIL}/${cosolvent}"
path_gro_cosolvent_solvated="${path_cosolvent_solvated}/${cosolvent}.gro"

## SEEING IF MOLECULAR FILE EXISTS
touch "${MOLECULAR_WEIGHT_FILE}"
touch "${MOLECULAR_VOLUME_FILE}"

##################################
### COMPUTING MOLECULAR VOLUME ###
##################################

### COMPUTING MOLECULAR VOLUME
mol_volume_cosolvent=$(grep "^${cosolvent}" ${MOLECULAR_VOLUME_FILE} | awk '{print $NF}')

## MAKING SURE MW IS CALCULATED, OTHERWISE, ADD IT
if [ -z "${mol_volume_cosolvent}" ]; then 
	echo "------------------------------------"
    echo "Error! No co-solvent molecular volume"; 
    echo "Attempting to generate molecular volume via number density from:"
    echo "${path_gro_cosolvent_solvated}"
    
    ## SEEING IF EXISTS
    if [ -e "${path_gro_cosolvent_solvated}" ]; then
        num_density_cosolvent=$(gro_compute_num_density ${default_num_solvents} ${path_gro_cosolvent_solvated})
        ## FINDING VOLUME / 1 MOLECULE
        mol_volume_cosolvent=$(awk -v num_dens=${num_density_cosolvent} 'BEGIN{ printf "%.5f", 1/num_dens }') 
        ## STORING INTO MOLECULAR WEIGHT FILE
        write_molecular_info "${MOLECULAR_VOLUME_FILE}" "${cosolvent}" "${mol_volume_cosolvent}"
        echo "------------------------------------"
    else
        echo "Error! ${path_gro_cosolvent_solvated} does not exist!"
        echo "Run prep_solvents.sh ${cosolvent}"
        echo "Stopping here!"
        sleep 5
        exit 1
    fi
fi

##################################
### COMPUTING MOLECULAR WEIGHT ###
##################################

## FINDING MW OF COSOLVENT
path_cosolvent_initial_pdb="${PREP_SOLVENT_FINAL}/${cosolvent}/prep_files/${cosolvent}.pdb"

## GETTING MOLECULAR WEIGHT
MW_cosolvent=$(grep "^${cosolvent}" ${MOLECULAR_WEIGHT_FILE} | awk '{print $NF}')

## MAKING SURE MW IS CALCULATED, OTHERWISE, ADD IT
if [ -z "${MW_cosolvent}" ]; then 
	echo "------------------------------------"
    echo "Error! No co-solvent molecular weight"; 
    echo "Attempting to generate molecular weight using PDB file:"
    echo "${path_cosolvent_initial_pdb}"
	## CALCULATING MOLECULAR WEIGHT OF THE PDB FILE
	MW_cosolvent=$(calc_pdb_molecular_weight "${path_cosolvent_initial_pdb}" ${MOLECULAR_WEIGHT_REF})
	## STORING INTO MOLECULAR WEIGHT FILE
	write_molecular_info "${MOLECULAR_WEIGHT_FILE}" "${cosolvent}" "${MW_cosolvent}"
	echo "------------------------------------"
fi

## GIVEN MOLECULAR WEIGHTS, NUMBER DENSITIES, COMPUTE NUMBER OF WATER AND COSOLVENT MOLECULES REQUIRED
# COMPUTING MASS FRACTIONS IN DECIMALS
cosolvent_frac=$(awk -v perc=${cosolvent_perc} 'BEGIN{ printf "%.3f",perc/100}')
water_frac=$(awk -v frac_one=$cosolvent_frac 'BEGIN{ printf "%.3f",1-frac_one}') 

## GETTING TOTAL MASS
total_mass=$(awk -v frac_one=$water_frac -v frac_two=$cosolvent_frac -v MW_1=$MW_water -v MW_2=$MW_cosolvent 'BEGIN{ printf "%.5f", frac_one/MW_1 + frac_two/MW_2  }') 

## FINDING MOLE FRACTION OF WATER
solvent_one_mol_frac=$(awk -v frac_one=$water_frac -v MW_1=$MW_water -v tot_mass=$total_mass 'BEGIN{ printf "%.5f", frac_one/MW_1/tot_mass }') 

## TOTAL VOLUME
total_vol=$(awk -v vol=$initial_box_length 'BEGIN{ printf "%.5f",vol^3}') 

## TOTAL NUMBER OF MOLECULES
if [ $(awk -v wt_frac=$solvent_one_weight_frac 'BEGIN{ if (wt_frac != 0.00) print 1; else print 0}') -eq 1 ]; then
    # Finding total number of solvent one
    num_solv_one=$(awk -v totvol=${total_vol} -v vol_solvent_1=${mol_volume_water} -v vol_solvent_2=${mol_volume_cosolvent} -v mol_frac_1=${solvent_one_mol_frac} 'BEGIN{ printf "%d",totvol/(vol_solvent_1+(1-mol_frac_1)/mol_frac_1*vol_solvent_2)}') 
    # Finding total number for solvent two
    num_solv_two=$(awk -v num_solv_1=$num_solv_one -v mol_frac_1=${solvent_one_mol_frac} 'BEGIN{ printf "%d",num_solv_1*(1-mol_frac_1)/mol_frac_1}') 
else # No Water
    num_solv_one=0
    num_solv_two=$(awk -v totalVol=$total_vol -v sol_2_vol=$solvent_two_vol 'BEGIN{ printf "%d",totalVol/sol_2_vol}') 
fi


################################################
### USING PACKMOL TO ADD WATER AND COSOLVENT ###
################################################

## CREATING DIRECTORY
create_dir "${path_output}" -f

## GOING INTO DIRECTORY
cd "${path_output}"

## DEFINING PACKMOL SCRIPT
packmol_script="${PACKMOL_SCRIPT}"

## DEFINING PACKMOL INPUT
packmol_input="packmol_mixed_solvents.inp"
path_packmol_input="${INPUT_PACKMOL_PATH}/${packmol_input}"

## DEFINING WATER PDB
water_pdb_name="water_${water_model}.pdb"
path_water_pdb="${INPUT_PACKMOL_PATH}/${water_pdb_name}"

## GETTING PDB FILE NAME
cosolvent_pdb_name=$(basename ${path_cosolvent_initial_pdb})

## DEFINING OUTPUT PDB
output_solvated_pdb="${output_prefix}.pdb"

## COPYING INPUT FILE
cp "${path_packmol_input}" "${path_output}"

## COPYING COSOLVENT/WATER PDB FILE
cp "${path_cosolvent_initial_pdb}" "${path_output}"
cp "${path_water_pdb}" "${path_output}"

## GETTING MAX LENGTH IN ANGS
new_length_angs=$(awk -v lengths=${initial_box_length} 'BEGIN{ printf "%.5f",lengths*10}')

## EDITING PACKMOL FILE
## GENERAL INFO
sed -i "s/_OUTPUTPDB_/${output_solvated_pdb}/g" "${packmol_input}"
sed -i "s/_MAXANGS_/${new_length_angs}/g" "${packmol_input}"
## COSOLVENT
sed -i "s/_COSOLVENTPDB_/${cosolvent_pdb_name}/g" "${packmol_input}"
sed -i "s/_NUMCOSOLVENT_/${num_solv_two}/g" "${packmol_input}"
## WATER
sed -i "s/_WATERPDB_/${water_pdb_name}/g" "${packmol_input}"
sed -i "s/_NUMWATER_/${num_solv_one}/g" "${packmol_input}"

## RUNNING PACKMOL
"${PACKMOL_SCRIPT}" < ${packmol_input}

## DEFINING INITIAL GRO FILE
initial_gro_file="${output_prefix}.gro"

## GETTING INITIAL BOX LENGTH
initial_box_length_with_dist=$(awk -v lengths=${initial_box_length} -v distance=${dist} 'BEGIN{ printf "%.5f",lengths + (distance*2) }')

## USING EDIT CONF TO GENERATE GRO FILE
gmx editconf -f ${output_solvated_pdb} -o ${initial_gro_file} -box "${initial_box_length_with_dist}" "${initial_box_length_with_dist}" "${initial_box_length_with_dist}"

## CHECKING IF EXISTS
stop_if_does_not_exist "${initial_gro_file}"

#########################
### DEFINING TOPOLOGY ###
#########################

## DEFINING MDP FILE
input_em_mdp_file="minim_sam_gmx5.mdp"
input_equil_mdp_file="npt_double_equil_gmx5_charmm36.mdp"

## DEFINING MDP FILE
path_mdp_folder="${INPUT_MDP_PATH}/charmm36/Spherical"

## DEFINING NUMBER OF MDP FILE STEPS
mdp_equil_nsteps="5000000" # 10 ns

## DEFINING SUBMISSION FILE
input_submit_file="submit_prep_mixed_solvents.sh"

## DEFINING TOPOLOGY FILE
topology_file="${output_prefix}.top"

## DEFINING PATH
input_path_topology="${INPUT_TOPOLOGY_PATH}/${topology_file}"

##########################
### COPYING FILES OVER ###
##########################

## COPYING FORCEFIELD FILES 
echo "COPYING FORCE FIELD FILES, ITP, TOPOLOGY, PRM, ETC."
# Force fields
cp -r "${PATH_FORCEFIELD}/${forcefield}" "${path_output}" 
# ITP files
cp -r "${path_input_molecule}/"{${molecule_itp_file},${molecule_prm_file}} "${path_output}" 
# TOP files
cp -r "${input_path_topology}" "${path_output}/${topology_file}"
# MDP FILES
cp -r "${path_mdp_folder}/"{${input_em_mdp_file},${input_equil_mdp_file}} "${path_output}"
# SUBMISSION FILE
cp -r "${PATH2SUBMISSION}/${input_submit_file}" "${path_output}/${output_submit_file}"

## FINDING RESIDUE NAME FROM ITP FILE
resname=$(extract_itp_resname ${molecule_itp_file})

## EDITING MDP FILES
sed -i "s/NSTEPS/${mdp_equil_nsteps}/g" "${input_equil_mdp_file}"
sed -i "s/TEMPERATURE/${system_temp}/g" "${input_equil_mdp_file}"

###### EDITING TOPOLOGY FILE
### FIXING FILES THAT WERE COPIED
sed -i "s/_FORCEFIELD_/${forcefield}/g" "${topology_file}"
sed -i "s/_SOLVENTNAME_/${cosolvent}/g" "${topology_file}" # cosolvent name
sed -i "s/_WATERMODEL_/${water_model}/g" "${topology_file}" # water model
## ADDING MOLECULES
echo "   ${resname}    ${num_solv_two}" >> "${topology_file}"
echo "   SOL    ${num_solv_one}" >> "${topology_file}"

####### EDITING SUBMISSION FILE
sed -i "s/_JOB_NAME_/${output_dir_name}/g" "${output_submit_file}"
sed -i "s/_NUM_CORES_/${num_cores}/g" "${output_submit_file}"
sed -i "s/_INPUT_EQUIL_MDP_FILE_/${input_equil_mdp_file}/g" "${output_submit_file}"
sed -i "s/_TOPOLOGY_FILE_/${topology_file}/g" "${output_submit_file}"
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" "${output_submit_file}"

###################################
### RUNNING ENERGY MINIMIZATION ###
###################################
gmx grompp -f ${input_em_mdp_file} -c "${initial_gro_file}" -p "${topology_file}" -o "${output_prefix}_em"
gmx mdrun -v -nt 1 -deffnm "${output_prefix}_em"

#####################
### PRINTING JOBS ###
#####################
echo -e "--- CREATING SUBMISSION FILE: $submitScript ---"
echo "Adding ${path_output} to ${JOB_SCRIPT}"
echo "${path_output}/${output_submit_file}" >> ${JOB_SCRIPT}

########################
### PRINTING SUMMARY ###
########################
## DEFINING SUMMARY FILE
summmary_file="${path_output}/summary.txt"
echo "----- SUMMARY -----"
echo "Worked on mixed-solvent with: ${cosolvent} (${resname})"
echo "Total number of steps for equilibration: ${mdp_equil_nsteps}"
echo "Box size: ${initial_box_length}"
## PRINTING
echo "--- NUMBER OF RESIDUES ---"
echo "Cosolvent/water mass fraction: ${cosolvent_frac} / ${water_frac}"
echo "Total mass of water and cosolvent: ${total_mass}"
echo "Number of water is ${num_solv_one}"
echo "Number of cosolvent (${cosolvent}) is ${num_solv_two}"
echo "YOU'RE ALL GOOD TO GO! Best of luck!"
## ADDING TO SUMMARY
echo "Created by prep_mixed_solvent.sh" >> ${summmary_file}
echo "Box size: ${initial_box_length}" >> ${summmary_file}
echo "Cosolvent/water mass fraction: ${cosolvent_frac} / ${water_frac}" >> ${summmary_file}
echo "Total mass of water and cosolvent: ${total_mass}" >> ${summmary_file}
echo "Number of water is ${num_solv_one}" >> ${summmary_file}
echo "Number of cosolvent (${cosolvent}) is ${num_solv_two}" >> ${summmary_file}



