#!/bin/bash

# restart_gromacs_job.sh
# The purpose of this script is to look for each file and to see if a job completed. If it did not complete, then re-create a *.sh script that would quickly re-run the job again. 

# Written by Alex Chew (04/06/2017)

## ** Updates ** ##
# 17-07-26: Made restart jobs compatible with the current side project
# 17-07-27: Made restart job compatible with stampede
# 17-07-31: Insertion of -ntmpi and -ntomp (see link: http://www.gromacs.org/Documentation/Acceleration_and_parallelization#Running_simulations)
# 17-08-01: Added functionality to find which server you are in
# 17-11-29: Made this available for Ligandbuilder
# 18-04-25: Added variable functions

## VARIABLES:
# $1: directory within simulations that you care about
# $2: specific directory with simulations
### SOURCING ALL VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

# Getting script name and echoing
print_script_name

# --- USER-SPECIFIC PARAMETERS (BEGIN) ---

## READING SERVER INFORMATION
read ServerKeyWord walltime numberOfCPUs <<< $(get_hostname_details)

# Defining folders within the simulation folder to look into
# "Planar_noGold"
declare -a extractDirList=("$1")

# Declaring production files to look into
production_file_start="$2"

# Defining input files
# ServerKeyWord="CHTC_HPC" # Folder inside submission script
resubmitFile="reset_job.sh" # Submission file for re-submitting

reset_job_list_file="reset_job.txt" #reset_comet_job.txt

# NOTE: Typically, resubmit file has no commands to make it arbitrary. 
# Finding full path to submit file
fullpath2submitFile="$PATH2SUBMITSCRIPT/$ServerKeyWord/$resubmitFile"

# Defining resetjob scripts
resetjob_list_dir="$PATH2SCRIPTS/${reset_job_list_file}" # where you will place the reset jobs

## DEFINING IF YOU WANT A SPECIFIC JOB TYPE
# want_specific_job_type=false # True if you want a specific job type
# specific_job_type="4" # restarting production jobs

## Defining initial inputs for the job ##
# Number of threads
num_threads_input='NumberOfThreads=NUMBEROFCPUS'
# Outputfile name
# Input Files
gro_file_input='gro_file="sam.gro"'
top_file_input='input_top="sam.top"'
output_prefix_input='output_prefix="sam"'
# MDP Files
em_mdp_input='em_mdp="minim_sam_gmx5.mdp"'
equil_mdp_input='equil_mdp="npt_double_equil_gmx5_charmm36.mdp"'
# prod_mdp_input='prod_mdp="npt_double_prod_gmx5_charmm36.mdp"' # <- NPT SIMS
prod_mdp_input='prod_mdp="nvt_double_prod_gmx5_charmm36_frozen_gold.mdp"'

## Checking parameters -- Script will look for this file to see if the job completed ##
output_prefix="sam"
em_gro="${output_prefix}_em.gro"
equil_gro="${output_prefix}_equil.gro"
equil_xtc="${output_prefix}_equil.xtc"
equil_tpr="${output_prefix}_equil.tpr"
prod_gro="${output_prefix}_prod.gro"
prod_xtc="${output_prefix}_prod.xtc"
prod_tpr="${output_prefix}_prod.tpr"
totalSimTime="30000.000" # "100000.000" # "50000.000" # Simulation time in picoseconds (Note the three decimal places!)

# Creating text of strings to potentially add to reset job script

# Energy Minimization
EM_Grompp='gmx grompp -f ${em_mdp} -c ${gro_file} -p ${input_top} -o ${output_prefix}_em -maxwarn 5'
EM_mdrun='gmx mdrun -v -ntmpi 1 -ntomp ${NumberOfThreads} -deffnm ${output_prefix}_em'

# Equilibrium
Equil_Grompp='gmx grompp -f ${equil_mdp} -c ${output_prefix}_em -p ${input_top} -o ${output_prefix}_equil -maxwarn 5'
Equil_mdrun='gmx mdrun -v -ntmpi 1 -ntomp ${NumberOfThreads} -deffnm ${output_prefix}_equil'

# Production
Prod_Grompp='gmx grompp -f ${prod_mdp} -c ${output_prefix}_equil.gro -p ${input_top} -o ${output_prefix}_prod -maxwarn 5'
Prod_mdrun='gmx mdrun -v -ntmpi 1 -ntomp ${NumberOfThreads} -deffnm ${output_prefix}_prod'

## Restart names ##

# Equilibrium job
Restart_Equil_Job='gmx mdrun -ntmpi 1 -ntomp ${NumberOfThreads} -v -s ${output_prefix}_equil.tpr -cpi ${output_prefix}_equil.cpt -deffnm ${output_prefix}_equil -append'

# Production job
Restart_Prod_Job='gmx mdrun -ntmpi 1 -ntomp ${NumberOfThreads} -v -s ${output_prefix}_prod.tpr -cpi ${output_prefix}_prod.cpt -deffnm ${output_prefix}_prod -append'

## Extending Production jobs ##
Extend_Prod_Job_tpr='gmx convert-tpr -s ${output_prefix}_prod.tpr -until TOTAL_PROD_TIME -o ${output_prefix}_prod.tpr'

Extend_Prod_Job_mdrun='gmx mdrun -ntmpi 1 -ntomp ${NumberOfThreads} -v -s ${output_prefix}_prod.tpr -cpi ${output_prefix}_prod.cpt -append -deffnm ${output_prefix}_prod'

# Making edits for STAMPEDE
if [[ $(hostname) == *"stampede"* ]]; then # Check if we are in the smic server

    # Energy minimization
    EM_mdrun='ibrun mdrun_mpi -v -deffnm ${output_prefix}_em'

    # Equilibriation
    Equil_mdrun='ibrun mdrun_mpi -v -deffnm ${output_prefix}_equil'
    Restart_Equil_Job='ibrun mdrun_mpi -v -s ${output_prefix}_equil.tpr -cpi ${output_prefix}_equil.cpt -deffnm ${output_prefix}_equil -append '

    # Production
    Prod_mdrun='ibrun mdrun_mpi -v -deffnm ${output_prefix}_prod'
    Extend_Prod_Job_mdrun='ibrun mdrun_mpi -v -s ${output_prefix}_prod.tpr -cpi ${output_prefix}_prod.cpt -append -deffnm ${output_prefix}_prod'

fi

# --- USER-SPECIFIC PARAMETERS (END) ---

# Creating functions to output the correct submission script depending on the incomplete job (i.e. equilibrium or production)

function addInitialVar() {
    # The purpose of this function is to input the initial parts of the reset script
    # INPUTS
    #   $1: submission file script
    
    # Adding number of threads
    echo "# Number of threads" >> $1
    echo -e "${num_threads_input}\n" >> $1
    
    # Adding input files
    echo "## INPUT FILES ##" >> $1
    echo -e "${gro_file_input}\n${top_file_input}\n${output_prefix_input}\n" >> $1

    # Adding mdp files
    echo "## MDP FILES ##" >> $1
    echo -e "${em_mdp_input}\n${equil_mdp_input}\n${prod_mdp_input}\n" >> $1

}

function addContScript() {
    # The purpose of this function is to take the reset_job script and output the correct job lists. This copies the reset script to current location and adds the correct job lines
    
    # Defining Job Type
    # 0 = Nothing started
    # 1 = Equilibrium did not complete
    # 2 = Production did not complete
    # 3 = Equilibrium completed but production did not complete
    # 4 = Production completed, but the xtc time is not correct
    # 5 = Equilibrium did not even start, but energy minimization completed
    
    jobtype=$1
    
    # Defining reset script path
    resetScriptLocation=${fullpath2submitFile}
    
    # Defining reset script name
    resetScriptName=$(basename $resetScriptLocation)
    
    # Copying file to current directory
    currentScriptLoc="$(pwd)/$resetScriptName"
    echo -e "Copying ${resetScriptLocation} to $(pwd)\n"
    cp -r $resetScriptLocation $currentScriptLoc
    
    # Adding the initial parts of the script
    addInitialVar ${currentScriptLoc}
    echo "*** --- RESET JOB PROMPT --- ***"
    if [ $jobtype == 0 ]; then # Equilibrium re-run and production run
        # Now, adding the extra job lists
        echo "Nothing was complete; Adding EVERYTHING to submission script!"
        
        echo "# Energy minimization" >> "${currentScriptLoc}"
        echo -e "${EM_Grompp}\n${EM_mdrun}\n" >> "${currentScriptLoc}"
        
        echo "# Equilibration" >> "${currentScriptLoc}"
        echo -e "${Equil_Grompp}\n${Equil_mdrun}\n" >> "${currentScriptLoc}"
        
        # Adding production
        echo "# Production Runs" >> "$currentScriptLoc"
        echo -e "${Prod_Grompp}\n" >> "$currentScriptLoc"
        
        # Checking desired simulation time
        echo "# Checking PRODUCTION time" >> "$currentScriptLoc"
        echo -e "${Extend_Prod_Job_tpr}\n" >> "$currentScriptLoc"
        
        # Using sed to change the time
        sed -i "s/TOTAL_PROD_TIME/${totalSimTime}/g" "$currentScriptLoc"
        
        echo "# Running production runs" >> "${currentScriptLoc}"
        echo -e "${Prod_mdrun}\n" >> "${currentScriptLoc}"
    
    elif [ $jobtype == 5 ]; then # Equilibrium did not start!
        # Now, adding the extra job lists
        echo "Equilibration did not start! Adding to submission script!"
        echo "# Equilibration" >> "${currentScriptLoc}"
        echo -e "${Equil_Grompp}\n${Equil_mdrun}\n" >> "${currentScriptLoc}"
        
        # Adding production
        echo "# Production Runs" >> "$currentScriptLoc"
        echo -e "$Prod_Grompp\n" >> "$currentScriptLoc"
        
        # Checking desired simulation time
        echo "# Checking PRODUCTION time" >> "$currentScriptLoc"
        echo -e "${Extend_Prod_Job_tpr}\n" >> "$currentScriptLoc"
                # Using sed to change the time
        sed -i "s/TOTAL_PROD_TIME/${totalSimTime}/g" "$currentScriptLoc"
        
        # Adding production run
        echo "# Running production runs" >> "$currentScriptLoc"
        echo -e "${Prod_mdrun}\n" >> "$currentScriptLoc"
    
    
    elif [ $jobtype == 1 ]; then # Equilibrium re-run and production run
        # Now, adding the extra job lists
        echo "Equilibration did not complete; Adding to submission script!"
        echo "# Equilibrium re-start" >> "$currentScriptLoc"
        echo -e "${Restart_Equil_Job}\n" >> "$currentScriptLoc"
        
        # Adding production
        echo "# Production Runs" >> "$currentScriptLoc"
        echo -e "$Prod_Grompp\n" >> "$currentScriptLoc"
        
        # Checking desired simulation time
        echo "# Checking PRODUCTION time" >> "$currentScriptLoc"
        echo -e "${Extend_Prod_Job_tpr}\n" >> "$currentScriptLoc"
                # Using sed to change the time
        sed -i "s/TOTAL_PROD_TIME/${totalSimTime}/g" "$currentScriptLoc"
        
        # Adding production run
        echo "# Running production runs"
        echo "$Prod_mdrun" >> "$currentScriptLoc"
        
    elif [ $jobtype == 2 ]; then # Production re-run!
        echo "Production run was stopped, but we have an equilibrium file. Re-continuing simulation time!"
        # Checking desired simulation time
        echo "# Checking PRODUCTION time" >> "$currentScriptLoc"
        echo -e "${Extend_Prod_Job_tpr}\n" >> "$currentScriptLoc"
                # Using sed to change the time
        sed -i "s/TOTAL_PROD_TIME/${totalSimTime}/g" "$currentScriptLoc"
    
    
        # Now, adding the production restart
        echo "# Production RESTART" >> "$currentScriptLoc"
        echo "# Production Extension / Continuation" >> "$currentScriptLoc"
        echo "$Extend_Prod_Job_mdrun" >> "$currentScriptLoc"
        
    elif [ $jobtype == 3 ]; then # Production begins!
        echo "Equilibration run completed; Adding PRODUCTION run"
        echo "# Production Run Begins" >> "$currentScriptLoc"
        echo -e "${Prod_Grompp}\n" >> "$currentScriptLoc"
        
        # Checking desired simulation time
        echo "# Checking PRODUCTION time" >> "$currentScriptLoc"
        echo -e "${Extend_Prod_Job_tpr}\n" >> "$currentScriptLoc"
                # Using sed to change the time
        sed -i "s/TOTAL_PROD_TIME/${totalSimTime}/g" "$currentScriptLoc"
        
        echo -e "${Prod_mdrun}\n" >> "$currentScriptLoc"
        
    elif [ $jobtype == 4 ]; then # Production is complete, but we need to extend it 
        echo "Production did not complete; Adding PRODUCTION/EXTENSION to submission script"
        echo "# Production Extension / Continuation" >> "$currentScriptLoc"
        echo -e "${Extend_Prod_Job_mdrun}\n" >> "$currentScriptLoc"
        
        
    else # Error, something is wrong...
        echo "Error, jobtype = ${jobtype} is not equal to any possible job types"
        echo "Stopping now, please check the restart_gromacs_job.sh script in ${PATH2SCRIPTS}"
        exit
    fi
}

### FUNCTION TO CHECK XTC FILE FOR THE TOTAL FRAMES
function checkxtc() {
    # The purpose of this script is to take an xtc file and find the very last frame in time (ps).
    ## INPUTS: 
    #   $1 -- xtc file name
    #   $2 -- Name of your variable to store it in
    ## OUTPUTS:
    #   current_xtc_equil_time: current xtc time
    ## USAGE: checkxtc ${equil_xtc} current_xtc_equil_time
                    
    # Defining local variable to keep my result
    local __lastFrameTime=$2
    
    # Defining files
    xtcfile="$1"
    tempxtcFile="checkxtc.check"
    
    # Creating temporary file using gmx check (standard error output is 2)
    gmx check -f "$1" 2> $tempxtcFile
    
    # Now, let's find the value
    currentFrame=$(grep "Step" ${tempxtcFile} | awk {'print $2'}) # total frames
    timestep=$(grep "Step" ${tempxtcFile} | awk {'print $NF'}) # dt in ps
    
    # Calculating last frame
    last_frame_ps=$(awk -v nFrames=$currentFrame -v dt=$timestep 'BEGIN {printf "%.3f",(nFrames-1)*dt; exit}' )
    
    eval $__lastFrameTime=${last_frame_ps}
    
    # Deleting the temporary file
    if [ -e $tempxtcFile ]; then
        rm $tempxtcFile
    fi
    
    # Printing
    echo "Looking at the XTC file: $1; which has ${last_frame_ps} ps worth of data"
}

# Calculates total time in picoseconds
function calc_time_ps() { 
    # The purpose of this function is to get your number of steps, and your dt in picoseconds to get the total time in ps
    # INPUTS:
    #   $1 is the number of steps
    #   $2 is the dt
    #   $3 is your local variable to store the time in
    
    # Defining local variable to keep result
    local __calculated_time=$3
    
    # Using AWK to do the calculation
    getTime=$(awk -v nSteps=$1 -v dt=$2 'BEGIN {printf "%.3f",nSteps*dt; exit}')
    eval $__calculated_time=${getTime}
    
    # Printing
    echo "Evaluating time using nSteps = $1 and dt = $2 to get $getTime ps"
}

function checkJobScript () {
    # The purpose of this script is to go into a directory of interest, then check if the job is complete. This script uses a lot of global variables like the job names, etc. The only input is the directory you care about.
    #
    # This script will output a reset_job.sh file that you can use to run in the server. If that is not available, your job is probably complete.
    
    # INPUTS:
    #   $1: Absolute path of your job

    # Defining variables
    currentJobDir="$1"
    want_specific_job_type="${2-false}"
    specific_job_type="$3"
    
    # Getting base name of the job
    basenameJobDir=$(basename ${currentJobDir})

    # Defining some local variables
    resetJobName="Reset_${basenameJobDir}"

    # Going to each directory to check if the job is complete
    cd "${currentJobDir}"

    # Delete any reset_job.sh initially

    if [ -e $resubmitFile ]; then
        echo -e "\n --Found initial resubmission file, deleting to avoid overwriting--"
        rm $resubmitFile
    fi

    
    echo -e "\n---------------------------------------------------"
    echo -e "Working on ${currentJobDir}\n"
    
    ## CHECKING IF JOB COMPLETED
    if [ -e ${prod_gro} ]; then # First indication of an incomplete job

        echo "Job was completed at ${currentJobDir}"
        echo -e "${prod_gro} is available, checking if the production job really did complete... \n"

        # Now, checking the TPR file
        currentTPR_nsteps=$(gmx dump -s ${prod_tpr} 2> /dev/null | grep -E "nsteps"| awk {'print $NF'})
        currentTPR_dt=$(gmx dump -s ${prod_tpr} 2> /dev/null | grep -E "dt"| awk {'print $NF'})

        # Calculating time in ps
        calc_time_ps ${currentTPR_nsteps} ${currentTPR_dt} current_prod_TPRTime

    #    current_prod_TPRTime=200000.000
    #    # If the time is not the same time as the desired time, then let's re-create the production.tpr file
        checkIfTPRs_not_equal=$(awk 'BEGIN{ print "'$current_prod_TPRTime'"!="'$totalSimTime'" }')
        if [ "${checkIfTPRs_not_equal}" -eq 1 ];then
            echo "Production TPR and desired TPR do not match!"
            echo "Current Production TPR time, ps: ${current_prod_TPRTime}"
            echo "Desired Production TPR time, ps: ${totalSimTime}"

            # Now, simply re-create the TPR file
            gmx convert-tpr -s ${output_prefix}_prod.tpr -until ${totalSimTime} -o ${output_prefix}_prod.tpr

        else
            echo "Production TPR and desired TPR match, not changing the TPR file"
        fi

        # Now, checking xtc file
        checkxtc ${prod_xtc} current_prod_xtcTime

        # Check if the xtc time matches the desired production time
        checkIfprod_time_not_equal=$(awk 'BEGIN{ print "'$totalSimTime'"!="'$current_prod_xtcTime'" }')
        if [ "${checkIfprod_time_not_equal}" -eq 1 ];then
            addContScript 4  
        fi

    else # Production job is incomplete!

        # Checking if the equilibrium job completed
        if [ -e ${equil_gro} ]; then # First indication of an incomplete job
        
            # Check if production job started
            if [ -e ${prod_xtc} ]; then
                # Clearly, the production file exists, but it did not complete. Let's complete it.
                addContScript 2
            else
                # Need to start production run
                addContScript 3
            
            fi

        else
            # Equilibration was not complete. Let's check if there is an energy minimization
            if [ -e "${em_gro}" ]; then # Indicates that energy minimization was done
                # Now, check to see if equilibrium is complete

                # First see if the job exists
                if [ -e ${equil_xtc} ]; then
                    # Now, checking the TPR file
                    currentTPR_nsteps=$(gmx dump -s ${equil_tpr} 2> /dev/null | grep -E "nsteps"| awk {'print $NF'})
                    currentTPR_dt=$(gmx dump -s ${equil_tpr} 2> /dev/null | grep -E "dt"| awk {'print $NF'})

                    # Calculating time in ps
                    calc_time_ps ${currentTPR_nsteps} ${currentTPR_dt} current_equil_TPRTime

                    # Check XTC file
                    checkxtc ${equil_xtc} current_xtc_equil_time

                    # Checking if the times match
                    checkIfEquil_time_not_equal=$(awk 'BEGIN{ print "'$current_equil_TPRTime'"!="'$current_xtc_equil_time'" }')

                    if [ "${checkIfEquil_time_not_equal}" -eq 1 ];then # Probably unnecessary if statement
                        addContScript 1
                    fi
                else
                    # Equilibrium did not start
                    addContScript 5
                fi

            else # No energy minimization, start from beginning
                addContScript 0

            fi
        fi
    fi

    # Checking the resubmission file
    checkResubmitFile ${currentJobDir} ${resubmitFile}

}

function checkResubmitFile() {
    # This script checks to see if there is a resubmit file. if there is, then it will change things like:
    # NETID, WALLTIME, JOB_NAME, NUMBERSOFCPUS
    # INPUTS:
    #   $1: Current working directory
    #   $2: Resubmit file name
    currentJobDir="$1"
    resubmitFile="$2"
    
    # Adding to re-submit file if it exists
    if [ -e $resubmitFile ]; then
        # Checking if the reset job file is available, then let's change some of the parameters
        # NetID
        sed -i "s/NETID/${NETID}/g" "${currentJobDir}/${resubmitFile}"
        # WallTime
        sed -i "s/WALLTIME/${walltime}/g" "${currentJobDir}/${resubmitFile}"
        # Job Name
        sed -i "s/JOB_NAME/${resetJobName}/g" "${currentJobDir}/${resubmitFile}"
        # Number of CPUS
        sed -i "s/NUMBEROFCPUS/${numberOfCPUs}/g" "${currentJobDir}/${resubmitFile}"

        # Printing out your resetjob
        echo "Now, printing the current working directory to $resetjob_list_dir"
        echo ${currentJobDir} >> ${resetjob_list_dir}

    else # Job is complete
        echo -e "\n*** Job was completed at ${currentJobDir} ***\n"
    fi
}

## Debugging ##
#currentJobDir="$PATH2SIM/${extractDirList[0]}/mdRun_363.15_6_nm_tBuOH_50_WtPercWater_spce_GVL_L"
#checkJobScript ${currentJobDir}
## Debugging ##

## MAIN SCRIPT ##

# Looping over each extraction directory
for extractDir in ${extractDirList[@]}; do

    # Finding all the files that you will need to look into
    FilesRequired=$(echo $PATH2SIM/$extractDir/$production_file_start*)

    # Now, looping through each folder
    for currentSeq in ${FilesRequired}; do
        checkJobScript "$currentSeq"
    
    done
    
done

# Printing what we have done
echo "----------- SUMMARY -----------"
echo "Added possible reset jobs at the following Simulations directories: ${extractDirList[@]}"
echo "The submission used was: ${ServerKeyWord}, ${resubmitFile}"
echo "Current Walltime, CPUs is: ${walltime}, ${numberOfCPUs}"
echo "See job listing in: ${resetjob_list_dir}"
echo "Good luck with the restarts!"

echo -e "\nEND ----- ### ${script_name} ### ----- END\n"
