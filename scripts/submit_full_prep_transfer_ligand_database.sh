#!/bin/bash 

# submit_full_prep_transfer_ligand_database.sh
# This script just runs the submission file

###SERVER_SPECIFIC_COMMANDS_START

#SBATCH -p compute
#SBATCH -t 108:00:00
#SBATCH -J submit_full_prep_transfer_ligand_database
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1              # total number of mpi tasks requested
#SBATCH --mail-user=akchew@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts

## RUNNING
bash_script="full_transfer_ligand_builder_database.sh" 

## LOADING
# conda activate py36_mdd

output_file="output_transfer_ligand_database.txt"

## CREATING EMPTY FILE
> "${output_file}"
## RUNNING BASH SCRIPT
bash "./${bash_script}" >> "${output_file}" 2>&1

## WAITING
wait
echo "PREPARATION IS COMPLETE"
