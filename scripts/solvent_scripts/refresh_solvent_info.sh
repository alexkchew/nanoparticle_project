#!/bin/bash

# refresh_solvent_info.sh
# The purpose of this script is to refresh the solvent information 
# by looking into the gro size, etc. The whole goal is to maintain a 
# global list that could inform about box sizes. Once we have that, 
# we could specify what box size is required for the simulation. 
# 
# VARIABLES:
#   $1: Specific solvent name
#   $2: path to solvent directory
# Written by Alex K. Chew (10/11/2019)


##############
### INPUTS ###
##############

## DEFINING SPECIFIC SOLVENT
specific_solvent="$1"
# "8_nm-300_K-1_mf-aceticacid_formate_methylammonium_propane"

## PATH TO SOLVENTS
path_solvents="$2"

## DEFINING FILE OF INTEREST
output_list_name="solvent_info.txt"

## DEFINING REWRITE
rewrite=false
# true if you want to rewrite file information

############################
### DEFINING INPUT FILES ###
############################

## GRO FILE
input_gro="mixed_solvent_equil.gro"

######################
### DEFINING PATHS ###
######################

## PATH TO LIST
path_output_list="${path_solvents}/${output_list_name}"

## CREATING LIST IF NOT ALREADY
if [ ! -e "${path_output_list}" ]; then
    echo "; solvent names | mole fractions | temp | dir name | gro size (nm)" > "${path_output_list}"
fi

#################
### MAIN CODE ###
#################

## SEEING IF MOLECULE IS WITHIN LIST
molecule_line=$(grep -nE "${specific_solvent}" ${path_output_list} | sed 's/\([0-9]*\).*/\1/')

## SEEING IF YOU NEED TO ADD MOLECULE
if [[ -z "${molecule_line}" ]] || [[ "${rewrite}" == true ]]; then

    #####################################
    ### SOURCING ALL GLOBAL FUNCTIONS ###
    #####################################
    ## DEFINING MAIN DIRECTORY
    main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
    ## DEFINING GLOBAL VARS NAME
    global_file_name="nanoparticle_rc.sh"
    ## SOURCING GLOBAL VARIABLES
    source "${main_dir}/../../bin/${global_file_name}"

    ######################
    ### CHECKING PATHS ###
    ######################
    
    ## DEFINING PATH TO MOLECULE
    path_molecule_equil="${path_solvents}/${specific_solvent}/${input_gro}"
    
    ## ADDING ONLY IF EQUIL EXISTS
    if [ -e "${path_molecule_equil}" ]; then
    
        ## CHECKING IF PATH EXISTS
        stop_if_does_not_exist "${path_molecule_equil}"

        ## GETTING GRO BOX SIZE
        read -a box_size <<< $(gro_measure_box_size "${path_molecule_equil}")

        ## EXTRACTING DETAILS 
        read -a extract_array <<< $(extract_output_name_for_mixed_solvents_multiple ${specific_solvent})

        ## ADDING DETAILS TO OUTPUT
        echo "${extract_array[3]} ${extract_array[2]} "${extract_array[1]}" "${extract_array[0]}" "${specific_solvent}" "${box_size[0]}"" >> "${path_output_list}"
    else
        echo "Skipping ${specific_solvent} since equil does not exist!"
    fi
fi
