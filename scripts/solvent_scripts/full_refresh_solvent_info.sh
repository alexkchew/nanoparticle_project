#!/bin/bash

# full_extract_solvent_details.sh

# The purpose of this script is to run extract solvent details across multiple 
# individual solvents. 

# VARIABLES
#   void

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../../bin/${global_file_name}"

## PATH TO SOLVENTS
path_solvents="${PREP_MIXED_SOLVENTS_MULTIPLE}"

## DEFINING BASH FILE
bash_file="${main_dir}/refresh_solvent_info.sh"

## DEFINING DIRECTORY NAME
directory_list=$(find ${path_solvents}/* -maxdepth 0 -type d | egrep -v 'dump$')

## LOOPING THROUGH EACH DIRECTORY
for solvent_dir in ${directory_list}; do

    ## GETTING SOLVENT NAME
    solvent_name="$(basename ${solvent_dir})"
    
    ## RUNNING SCRIPT
    bash "${bash_file}" "${solvent_name}" "${path_solvents}"

done

