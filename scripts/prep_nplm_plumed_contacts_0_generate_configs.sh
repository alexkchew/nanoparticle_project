#!/bin/bash

# prep_nplm_plumed_contacts_0_generate_configs.sh

# The purpose of this function is to initiate from 
# umbrella sampling simulations a trajectory that 
# has the nanoparticle nearby the lipid membrane. 
# This code will generate initial configurations with 
# varying number of contacts. To do this, we will need 
# the PLUMED plug-in for GROMACS, which enables us to 
# define complex collective variables that are not possible 
# in stand-alone GROMACS. 
#
# Written by: Alex K. Chew (04/28/2020)
#
# USAGE: bash prep_nplm_plumed_contacts_0_generate_configs.sh

## ALGORITHM:
#	- Generate index file
#	- Load gro / topology information from US output simulations
#		- For debugging purposes, turn off all water molecules
#	- Copy over MDP files
#	- Generate a PLUMED file

###########################################

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING INPUT SIMULATION DIR - ROT001
input_sim_dir_name="20200427-From_Stampede"
input_directory="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

input_sim_dir_name="US_Z_PULLING_SIMS"
input_directory="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

## DEFINING INPUT FOR ROT004
# input_sim_dir_name="20200613-US_otherligs_z-com"
# input_directory="US-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT004_1"


# input_sim_dir_name="20200613-US_otherligs_z-com"
# input_directory="US-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"
# input_directory="US-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT011_1"
# input_directory="US-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT010_1"


## FOR UPDATED ROT017 FF
input_sim_dir_name="20200818-Bn_US_modifiedFF"
input_directory="US-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"

## GETTING DISTANCE ARRAY
declare -a distance_array=("5.100")

## DEFINING SPRING ARRAY
declare -a spring_array=("50")
# "25"
# "50"
# "25" 
# "50"
# "100" "500"
# "25" 
#  "100" 
# "10" "25"
# "50"
# "100"
# "500"
#  "1000" "1500"

## DEFINING NUMBER TO REMOVE. IF ZERO, NO TRUNCATION IS DONE
z_nm_solvent_removal="2" 

## DEFINING OUTPUT DIRECTORY
# output_sim_dir_name="20200608-Pulling_with_hydrophobic_contacts"
output_sim_dir_name="20200821-Hydrophobic_contacts_pulling_Bn_new_FF"
# "20200629-Hydrophobic_contacts_iteration_2_shorter"
# "20200629-Hydrophobic_contacts_iteration_2"
# "20200629-Pulling_with_hydrophobic_contacts_C10_shorter"
# "20200519-plumed_test_pulling_new_params_debugging_stride1000_new_grps_8ns"
# "20200505-debugging_nplm_plumed_ROT001_neighbor_list_pt3"

## DEFINING TIME STEP FROM UMBRELLA SAMPLING SIMS
desired_frame="25000"
# "50000"
# "25000"
# "50000"
# "25000" # Different time frame
# "50000"
echo "Desired frame: ${desired_frame} ps"

## DEFINING GROMACS COMMAND
gromacs_command="gmx_mpi"
# Assume that gmx_mpi version is loaded: load_gmx_2016-6_plumed_2-5-1

## DEFINING ENERGY MINIMIZATION 
em_ncores="20"

#####################
### PLUMED INPUTS ###
#####################

## DEFINING RADIUS
radius="0.35"
# "0.5" # 0.5 nm cutoff for contacts
# spring="500" # kJ/mol/nm2
# "500"
# "2000" # kJ/mol/nm2
# nl_stride="1" # Frequency to update list

## DEFINING DMAX
d_max="2"
# "4"

## NEIGHBOR LIST CUTOFF
nl_cutoff="${d_max}"
# 5
# "1.5"
# "1.0"
# "0.7" # .7 nm
nl_stride="10"
# "100" # Update neighbor list every n steps

## NUMBER OF CONTACTS
at0="0"
at2="300"
# "1000"
# "1000"
# "500"
# "1000"
# "500"
# "100"
# "1000"

## DEFINING LOGICALS


## TRUE IF YOU WANT SHORTER AT2
want_smaller_at2=false
# false
# true

## LOGICAL FOR PULLING
want_com_pulling=false
# true if you want center of mass pulling between the gold nanoparticle and 
# the DOPC lipid membrane. This is used as a test to observe if we can get 
# good performance with PLUMED.

## DEFINING IF YOU WANT WATER
want_water=true
# Default is now true. We ran into issue with no water involved. 
# Counterions crash onto nanoparticle and causes LINCS warnings

## TRUE IF YOU WANT TO DEBUG COORDINATION TIME
want_debug_coord_time=false
# false
# true if you want to vary the coordination number for different 
# strides

## TRUE IF YOU WANT CARBON ONLY AS A THE REACTION COORDINATE FOR THE NANOPARTICLES
want_np_hydrophobic_contacts=true
# This has been modified to be alkane and R group together (instead of alone)

## TRUE IF YOU WANT ONLY TOP LIPID MEMBRANES
want_top_lm_indexes_only=true
# true if you want only the top lipid membrane tail groups

if [[ "${want_smaller_at2}" == true ]]; then
	echo "Since smaller at2 selected, changing at2 to 10"
	at2="10"
fi


## LOOPING
for spring in ${spring_array[@]}; do

	## LOOPING DISTANCES
	for desired_distance in ${distance_array[@]}; do


#########################
### DEFAULT VARIABLES ###
#########################

## WATER RESIDUE NAME
water_residue_name="SOL"

## ITP FILE FOR NP
np_itp="sam.itp"

## DEFINING TOP FILE
input_top="nplm.top"

## DEFINING GOLD CORE RESIDUE NAME
gold_core_resname="AUNP"

## DEFINING LM RESIDUE NAME
lipid_name="DOPC"
lm_residue_name="DOPC"

## DEFINING MAX WARN
maxwarn="5"

####################
### OUTPUT NAMES ###
####################

## DEFINING OUTPUT NAME
output_prefix="nplm"

## PREFIX
if [[ "${want_com_pulling}" == true ]]; then

	prefix="NPLMplumedCOMpulling"
	spring="2000" # kJ/mol/nm2
	## DEFINING SPRING CONSTANT
	at0="${desired_distance}"
	at2="2" # 2 nm
	print_radius=""

	## PRINTING
	echo "** COM pulling between gold core and DOPC lipid membrane is turned ON! **"
	echo "Spring constant: ${spring}"
	echo "At 0 distance: ${at0}"
	echo "At 2 distance: ${at2}"
	sleep 1
elif [[ "${want_np_hydrophobic_contacts}" == true ]]; then
	echo "Hydrophobic contacts was selected!"
	## DEFAULT CONTACTS PULLING
	prefix="NPLMplumedhydrophobiccontactspulling"
	print_radius="_${radius}"
else
	echo "Default contacts was selected!"
	## DEFAULT CONTACTS PULLING
	prefix="NPLMplumedcontactspulling"
	print_radius="_${radius}"
fi

## GETTING NEW NAME
truncated_name=$(sed "s/.*NPLM//g" <<< ${input_directory})

## DEFINING OUTPUT NAME
output_directory="${prefix}-${desired_distance}_${z_nm_solvent_removal}_${spring}_${at2}${print_radius}-${desired_frame}_ns${truncated_name}"
echo "Output name: ${output_directory}"

######################
### DEFINING PATHS ###
######################

## DEFINING INPUT DIRECTORY
path_input_dir="${PATH2NPLMSIMS}/${input_sim_dir_name}/${input_directory}"

## DEFINING OUTPUT DIR
path_output_dir="${PATH2NPLMSIMS}/${output_sim_dir_name}/${output_directory}"

####################################
### DEFINING DIRECTORY STRUCTURE ###
####################################

## FOR INPUT CONFIGS INITIAL
path_input_dir_input_files="${path_input_dir}/1_input_files"
path_input_dir_configs="${path_input_dir}/2_configurations"
path_input_dir_sims="${path_input_dir}/4_simulations"

## DEFINING INPUT TOP PATH
path_input_top="${path_input_dir_input_files}/${input_top}"

## DEFINING PREFIX
input_prefix="nplm_prod"

## DEFINING GRO FILES
config_gro="${input_prefix}.gro"
config_tpr="${input_prefix}.tpr"
config_xtc="${input_prefix}.xtc"

## DEFINING PATH TO GRO
path_gro="${path_input_dir_sims}/${desired_distance}/${config_gro}"
path_xtc="${path_input_dir_sims}/${desired_distance}/${config_xtc}"
path_tpr="${path_input_dir_sims}/${desired_distance}/${config_tpr}"

###################
### MAIN SCRIPT ###
###################

## MAKING DIRECTORY
create_dir "${path_output_dir}" -f

## GOING TO DIRECTORY
cd "${path_output_dir}"

######################################
### PART 1: COPYING ALL FILES ########
######################################
echo ""
echo "----------------------------------"
echo "Part 1: Copying gro, top, etc."
echo "----------------------------------"

## COPYING GRO FILE

## COPYING ALL TOPOLOGY FILE
copy_top_include_files "${path_input_top}" "${path_output_dir}"
cp -r "${path_input_top}" "${path_output_dir}"

## CHECK IF NP ITP IS AVAILABLE
stop_if_does_not_exist "${np_itp}"

## GETTING RESIDUE NAME
lig_resname="$(itp_get_residue_name_within_atom ${np_itp})"
echo "Ligand residue name: ${lig_resname}"

## DEFINING INDEX FILE
non_water_ndx="non-water.ndx"
non_water_name="non-Water"
gold_name="GOLD"

## DEVELOPING INDEX FILE
if [[ "${want_water}" == false ]]; then
	output_name="${non_water_name}"
	## CLEARING TOPOLOGY OF WATER
	top_remove_molecules "${input_top}" "${water_residue_name}"
else
	output_name="System"
fi

echo "Creating index file: ${non_water_ndx}"

"${gromacs_command}" make_ndx -f "${path_tpr}" \
			 				  -o "${path_output_dir}/${non_water_ndx}" >/dev/null 2>&1 << INPUTS
keep 0
! r ${water_residue_name}
name 1 ${non_water_name}
r ${gold_core_resname}
name 2 ${gold_name}
q
INPUTS

## COPYING GRO FILE OVER
echo "Creating new gro file: ${output_prefix}.gro"
"${gromacs_command}" trjconv -f "${path_xtc}" \
							 -o "${path_output_dir}/${output_prefix}.gro" \
							 -s "${path_tpr}" \
							 -n "${path_output_dir}/${non_water_ndx}" \
							 -pbc mol -center -dump "${desired_frame}"  << INPUTS
${gold_name}
${output_name}
INPUTS

# >/dev/null 2>&1
sleep 5

## CONVERTING TPR
echo "Creating new tpr file: ${output_prefix}.tpr"
"${gromacs_command}" convert-tpr \
							 -o "${path_output_dir}/${output_prefix}.tpr" \
							 -s "${path_tpr}" \
							 -n "${path_output_dir}/${non_water_ndx}" \
							 >/dev/null 2>&1 << INPUTS
${output_name}
INPUTS

## COPYING MDP FILES OVER
## DEFINING MDP FILES
mdp_em="em.mdp"
mdp_pulling_file="pulling_plumed.mdp"
# "pulling_plumed_debug.mdp"

## COPYING MDP FILE
path_mdp_parent="${INPUT_MDP_PATH}/charmm36"
path_mdp_dir="${path_mdp_parent}/lipid_bilayers"
cp -r "${path_mdp_dir}"/{${mdp_em},${mdp_pulling_file}} "${path_output_dir}"

###########################################
### PART 1A: REMOVAL OF SOLVATION LAYER ###
###########################################
## THIS PORTION REMOVES A SOLVENT LAYER TO SPEED UP CALCULATIONS
## THE REMOVAL OF THE SOLVENT LAYER SHOULD NOT SIGNIFICANTLY INFLUENCE COMPUTATIONS
# ALGORITHM:
#	 - GET GRO SIZE
# 	 - COMPUTE FINAL SIZE WITHOUT THE LAYER
#	 - USE INDEXING TO FIND ALL SOLVENTS TO REMOVE
#	 - USE TRJCONV TO REMOVE THE LAYER
#	 - UPDATE THE TOPOLOGY BASED ON THE REMOVAL OF SOLVENTS

if [[ "${z_nm_solvent_removal}" != 0 ]]; then

echo ""
echo "----------------------------------"
echo "Part 1a: Truncating gro file for speed up!"
echo "----------------------------------"

echo "Truncation in the z-axis: ${z_nm_solvent_removal} nm"

## GETTING GRO SIZE
read -a box_size <<< $(gro_measure_box_size "${output_prefix}.gro")

## FINDING NEW Z DISTANCE
new_z=$(awk -v z_dim=${box_size[2]} \
			-v trunc_z=${z_nm_solvent_removal} \
			'BEGIN{ printf "%.3f", z_dim - trunc_z }')

## CREATING INDEX FILE
truncated_index="truncate.ndx"
echo "Creating index file with items that cannot be deleted: ${truncated_index}"
"${gromacs_command}" make_ndx -f "${output_prefix}.tpr" \
			 				  -o "${truncated_index}" >/dev/null 2>&1 << INPUTS
keep 0
r ${gold_core_resname} | r ${lm_residue_name} | r ${gold_core_resname}
name 1 NPLM
q
INPUTS

## FINDING INDEX FOR 
truncated_index_select="truncate_select.ndx"
echo "Creating selection file for solvents above z-threshold: ${truncated_index_select}"
"${gromacs_command}" select -f "${output_prefix}.gro" \
							-s "${output_prefix}.tpr" \
							-n "${truncated_index}" \
							-on "${truncated_index_select}" \
							-select "not same residue as (z > ${new_z} and not group \"NPLM\")" >/dev/null 2>&1

## TRJCONV TO CREATE A NEW GRO FILE
## COPYING GRO FILE OVER
echo "Truncating the gro file: ${output_prefix}_truncated.gro"
"${gromacs_command}" trjconv -f "${output_prefix}.gro" \
							 -o "${output_prefix}_truncated.gro" \
							 -s "${output_prefix}.tpr" \
							 -n "${truncated_index_select}" >/dev/null 2>&1

## UPDATING TOPOLOGY BASED ON THE REMOVAL
## DEFINING PYTHON PATH
py_correct_gro="${MDBUILDER}/core/fix_residues_gro_top.py"

py_search_gro_residues="${MDBUILDER}/core/count_gro_residues.py"

## RUNNING PYTHON CODE
summary_file="${output_prefix}_truncated.summary"
echo "Computing total number of residues in gro file: ${summary_file}. (This may take some time)"
python3.6 "${py_search_gro_residues}" --igro "${path_output_dir}/${output_prefix}_truncated.gro" \
									  --sum "${path_output_dir}/${summary_file}"

## UPDATING RESIDUE NUMBERS FOR TOPOLOGY
new_top="${input_top%.top}_truncated.top"
cp "${input_top}" "${new_top}"

## UPDATING RESIDUE NUMBERS
echo "--- Correcting residue numbers on topology: ${new_top} ---"
while read line; do
  	res_name=$(awk '{print $1}' <<< ${line})
	num_res=$(awk '{print $2}' <<< ${line})
	top_update_residue_num "${new_top}" "${res_name}" "${num_res}"
    echo "--> ${res_name} ${num_res}"
done < "${summary_file}"

####################
### NEUTRALIZING ###
####################
echo "Re-adding counterions to neutralize system"
## CREATING GROMPP
"${gromacs_command}" grompp -f "${mdp_em}" \
           					-c "${output_prefix}_truncated.gro" \
           					-p "${new_top}" \
           					-o "${output_prefix}_truncated_genion.tpr" \
           					-maxwarn "${maxwarn}"
           					#  >/dev/null 2>&1
# 

stop_if_does_not_exist "${output_prefix}_truncated_genion.tpr"

## CREATING WATER INDEX
water_index="${output_prefix}_truncated_water.ndx"
water_res_name="SOL"
index_make_ndx_residue_ "${output_prefix}_truncated_genion.tpr" "${water_index}" "${water_res_name}" "${gromacs_command}"

## ADDING COUNTER IONS
combine_gro_correct="${output_prefix}_truncated_genion.gro"
echo "Creating new gro file that is neutralized: ${combine_gro_correct}"
"${gromacs_command}" genion -s "${output_prefix}_truncated_genion.tpr" \
       						-p "${new_top}" \
           					-o "${combine_gro_correct}" \
           					-n "${water_index}" \
           					-neutral >/dev/null 2>&1  << INPUT
${water_res_name}
INPUT
## CHECKING IF GRO FILE EXISTS
stop_if_does_not_exist "${combine_gro_correct}"

## UPDATING BOX DIMENSIONS (Small space around)
"${gromacs_command}" editconf -f "${combine_gro_correct}" \
			 				  -o "${combine_gro_correct%.gro}_shrink.gro" \
			 				  -d 0.05 >/dev/null 2>&1

## GETTING GRO SIZE
read -a shrink_box_size <<< $(gro_measure_box_size "${combine_gro_correct%.gro}_shrink.gro")

## EDITCONF TO FIX X, Y DIMENSIONS
"${gromacs_command}" editconf -f "${combine_gro_correct%.gro}_shrink.gro" \
			 				  -o "${combine_gro_correct%.gro}_shrink_xy.gro" \
			 				  -box "${box_size[0]}" "${box_size[1]}" "${shrink_box_size[2]}" >/dev/null 2>&1

## CHECKING IF GRO FILE EXISTS
stop_if_does_not_exist "${combine_gro_correct%.gro}_shrink_xy.gro"

## CLEANING UP FILES
setup_file="setup_file"
mkdir -p "${setup_file}"

## MOVING FILES
echo "Moving files to setup file: ${setup_file}"
mv "${combine_gro_correct}" \
   "${summary_file}" \
   "${output_prefix}_truncated.gro" \
   "${new_top}" \
   "${input_top}" \
   "${output_prefix}_truncated_genion.tpr" \
   "${combine_gro_correct%.gro}_shrink.gro" \
   "${output_prefix}.tpr" \
   "${combine_gro_correct%.gro}_shrink_xy.gro" \
   "${setup_file}"

## RENAMING SOME FILES
mv "${setup_file}/${combine_gro_correct%.gro}_shrink_xy.gro" "${output_prefix}.gro"
mv "${setup_file}/${new_top}" "${input_top}"

else
	echo "Since z_nm_solvent_removal is turned to 0, no truncation is performed!"
	echo "Original simulation size is maintained"
fi

#########################################
### PART 2: CREATING INDEX FILES ########
#########################################
echo ""
echo "----------------------------------"
echo "Part 2: Creating index files"
echo "----------------------------------"

## DEFINNING INDEX FILE
index_file="${output_prefix}.ndx"
np_lig_name="NP_LIGANDS"
lm_grp_name="TAILGRPS"

## CREATING INDEX FILE
nplm_generate_index_file "${gromacs_command}" \
						 "${output_prefix}.gro" \
						 "${index_file}" \
						 "${lig_resname}" \
						 "${np_lig_name}" \
					 	 "${lm_residue_name}" \
					 	 "${lm_grp_name}"


if [[ "${want_com_pulling}" == true ]]; then

## ADDING GOLD AND DOPC LIPID MEMBRANE
"${gromacs_command}" make_ndx -f "${output_prefix}.gro" \
			 				  -o "${index_file}" \
			 				  -n "${index_file}" >/dev/null 2>&1  << INPUTS
r ${gold_core_resname}
r ${lm_residue_name}
q
INPUTS
fi

## ADDING ALL THE GROUPS FOR NANOPARTICLE
if [[ "${want_np_hydrophobic_contacts}" == true ]]; then

	## CREATING INDEX FILE FOR TAIL GROUPS THAT ARE ABOVE A Z-CUTOFF
	python3.6 "${PYTHON_GET_NPLM_INDEX}" --path_to_sim "${path_output_dir}" \
										 --gro_file "${output_prefix}.gro" \
										 --index_file "${index_file}" \
										 --lm_residue_name "${lm_residue_name}"
	## REDEFINING NANOPARTICLE NAME
	np_lig_name="NP_ALK_RGRP"

fi

if [[ "${want_top_lm_indexes_only}" == true ]]; then
	## CREATING INDEX FILE FOR TAIL GROUPS THAT ARE ABOVE A Z-CUTOFF
	python3.6 "${PYTHON_GET_LM_TOP_BOT_INDEX}" --path_to_sim "${path_output_dir}" \
											   --gro_file "${output_prefix}.gro" \
											   --index_file "${index_file}" \
											   --lm_residue_name "${lm_residue_name}"

    ## REDEFINING LIPID MEMBRANE TAIL GROUPS
    lm_grp_name="TOP_TAILGRPS"
echo "Since 'want_top_lm_indexes_only' is true, 
we will only use index on top of lipid membrane"
echo "Setting lipid membrane name: ${lm_grp_name}"

fi



#############################################
### PART 3: MDP AND SUBMISSION FILES ########
#############################################

echo ""
echo "----------------------------------------------------"
echo "Part 3: Generating MDP files and submission scripts"
echo "----------------------------------------------------"

## MDP INPUTS
dt="0.002" # 2 fs
temp="300" # Temperature in Kelvins

## DEFINING OUTPUT FREQUENCY
nstxout="5000" # 0.002 * 5000 = 10 ps output

## DEFINING NUMBER OF STEPS
nsteps="3000000" # 6 ns
# "4000000" # 8 ns
# "1000000" # 2 ns quick runs
# "500000" # 1 ns
# "100000"
if [[ "${want_com_pulling}" == true ]]; then
	nsteps="500000" # 1 ns
fi

if [[ "${want_np_hydrophobic_contacts}" == true ]]; then
	## SLOWER PULLING FOR 10 NS
	echo "For hydrophobic contacts, pulling for 10 ns long"
	sleep 1
	nsteps="5000000" # 10 ns
fi


if [[ "${want_debug_coord_time}" == true ]]; then
	echo "Since debug is turned on, outputting every 1 ps"
	sleep 1
	nstxout="1" # output every step
	nsteps="2000" # total number of steps
fi

if [[ "${want_smaller_at2}" == true ]]; then
	nsteps="2500000" # 5 ns shorter time
	echo "Since smaller at2 is on, changing the pulling time to 5 ns"
fi

## EDITING PULL FILE
sed -i "s/_DT_/${dt}/g" "${mdp_pulling_file}"
sed -i "s/_TEMPERATURE_/${temp}/g" "${mdp_pulling_file}"
sed -i "s/_NSTEPS_/${nsteps}/g" "${mdp_pulling_file}"
sed -i "s/_NSTXTCOUT_/${nstxout}/g" "${mdp_pulling_file}"

###################################
### PART 4: ENERGY MINIMIZATION ###
###################################
echo ""
echo "----------------------------------------------------"
echo "Part 4: Energy minimization"
echo "----------------------------------------------------"

## SEEING IF YOU WANT TO SKIP EM
skip_em=true
# if true, circumvent the code to enable energy minimization from the run file

if [[ "${skip_em}" == false ]]; then

## RUNNING ENERGY MINIMIZATION
"${gromacs_command}" grompp -f "${mdp_em}" \
							-c "${output_prefix}.gro" \
							-p "${input_top}" \
							-o "${output_prefix}_em"
"${gromacs_command}" mdrun -v -deffnm "${output_prefix}_em" -ntomp "${em_ncores}"

else
	echo "Since energy minimization is skipped, it will be moved to the run script!"
	echo "If you want to observe energy minimization, change 'skip_em' flag to true"
	echo "This is designed to speed up the generation of jobs"

fi

#####################################
### PART 5: PLUMED IMPLEMENTATION ###
#####################################

echo ""
echo "----------------------------------------------------"
echo "Part 5: PLUMED development"
echo "----------------------------------------------------"

## DEFINING PLUMED INPUT
plumed_input="plumed_input.dat"

# COORDINATION NUMBER BETWEEN LIGANDS AND TAIL GROUPS WITH A SPECIFIED CUTOFF, NEIGHBORS LIST UPDATED EVERY N FRAMES
# coord: COORDINATION GROUPA=coms1 GROUPB=coms2 R_0=${radius} NLIST NL_CUTOFF=${nl_cutoff} NL_STRIDE=${nl_stride}
# cn2: COORDINATION GROUPA=coms1 GROUPB=coms2 R_0=${radius} NLIST NL_CUTOFF=${nl_cutoff} NL_STRIDE=2
# cn10: COORDINATION GROUPA=coms1 GROUPB=coms2 R_0=${radius} NLIST NL_CUTOFF=${nl_cutoff} NL_STRIDE=10
# cn50: COORDINATION GROUPA=coms1 GROUPB=coms2 R_0=${radius} NLIST NL_CUTOFF=${nl_cutoff} NL_STRIDE=50
# cn1000: COORDINATION GROUPA=coms1 GROUPB=coms2 R_0=${radius} NLIST NL_CUTOFF=${nl_cutoff} NL_STRIDE=1000

# coord: COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL NN=6 MM=12 D_0=0.0 R_0=${radius} D_MAX=${d_max}}

string="
# DEFINING UNITS
UNITS LENGTH=nm ENERGY=kj/mol TIME=ps

# NP GROUP (LIGANDS)
coms1: GROUP NDX_FILE=${index_file} NDX_GROUP=${np_lig_name}

# LIPID MEMBRANE GROUPS (TAIL GROUPS ONLY)
coms2: GROUP NDX_FILE=${index_file} NDX_GROUP=${lm_grp_name}

## DEFINING COORDINATION NUMBER
coord:  COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL NN=6 MM=12 D_0=0.0 R_0=${radius} D_MAX=${d_max}} NLIST NL_CUTOFF=${nl_cutoff} NL_STRIDE=1000

# MOVING RESTRAINT
restraint: ...
	MOVINGRESTRAINT
    ARG=coord
    STEP0=0 		 AT0=0 KAPPA0=0.0
    STEP1=5000 		 AT1=1 KAPPA1=${spring}
    STEP2=${nsteps}  AT2=${at2} KAPPA2=${spring}
...


PRINT ...
    STRIDE=10
    ARG=*
    FILE=COVAR.dat
... PRINT

ENDPLUMED" 

if [[ "${want_com_pulling}" == true ]]; then

string="
# DEFINING UNITS
UNITS LENGTH=nm ENERGY=kj/mol TIME=ps

# NP GROUP (LIGANDS)
atoms1: GROUP NDX_FILE=${index_file} NDX_GROUP=${gold_core_resname}
coms1: COM ATOMS=atoms1

# LIPID MEMBRANE GROUPS (TAIL GROUPS ONLY)
atoms2: GROUP NDX_FILE=${index_file} NDX_GROUP=${lm_residue_name}
coms2: COM ATOMS=atoms2

## DEFINING DISTANCE
dist: DISTANCE ATOMS=coms2,coms1 COMPONENTS

# MOVING RESTRAINT
restraint: ...
	MOVINGRESTRAINT
    ARG=dist.z
    STEP0=0 		 AT0=${at0} KAPPA0=0.0
    STEP1=5000 		 AT1=${at0} KAPPA1=${spring}
    STEP2=${nsteps}  AT2=${at2} KAPPA2=${spring}
...


PRINT ...
    STRIDE=10
    ARG=*
    FILE=COVAR.dat
... PRINT

ENDPLUMED" 


fi


## SEEING IF YOU WANT TO DEBUG 
if [[ "${want_debug_coord_time}" == true ]]; then
string="
# DEFINING UNITS
UNITS LENGTH=nm ENERGY=kj/mol TIME=ps

# NP GROUP (LIGANDS)
coms1: GROUP NDX_FILE=${index_file} NDX_GROUP=${np_lig_name}

# LIPID MEMBRANE GROUPS (TAIL GROUPS ONLY)
coms2: GROUP NDX_FILE=${index_file} NDX_GROUP=${lm_grp_name}

# TESTING DIFFERENT COORDINATION NEIGHBOR LISTS

## DEFINING COORDINATION NUMBER
coord: COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL NN=6 MM=12 D_0=0.0 R_0=${radius} D_MAX=${d_max}}
cn1:  COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL NN=6 MM=12 D_0=0.0 R_0=${radius} D_MAX=${d_max}} NLIST NL_CUTOFF=${nl_cutoff} NL_STRIDE=1
cn10:  COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL NN=6 MM=12 D_0=0.0 R_0=${radius} D_MAX=${d_max}} NLIST NL_CUTOFF=${nl_cutoff} NL_STRIDE=10
cn100:  COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL NN=6 MM=12 D_0=0.0 R_0=${radius} D_MAX=${d_max}} NLIST NL_CUTOFF=${nl_cutoff} NL_STRIDE=100
cn500:  COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL NN=6 MM=12 D_0=0.0 R_0=${radius} D_MAX=${d_max}} NLIST NL_CUTOFF=${nl_cutoff} NL_STRIDE=500
cn1000:  COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL NN=6 MM=12 D_0=0.0 R_0=${radius} D_MAX=${d_max}} NLIST NL_CUTOFF=${nl_cutoff} NL_STRIDE=1000

# MOVING RESTRAINT
restraint: ...
	MOVINGRESTRAINT
    ARG=coord
    STEP0=0 		 AT0=0 KAPPA0=0.0
    STEP1=${nsteps}		 AT1=100 KAPPA1=${spring}
...


PRINT ...
    STRIDE=1
    ARG=*
    FILE=COVAR.dat
... PRINT

ENDPLUMED" 


fi

echo "${string}" > "${plumed_input}"

#################################
### PART 6: SUBMISSION SCRIPT ###
#################################

echo ""
echo "==========================================="
echo "Part 6 - Generating run scripts"
echo "==========================================="

## DEFINING OUTPUT
output_run="run.sh"
output_submit="submit.sh"

## DEFAULTS FOR SUBMIT
num_cores="28"
mdrun_command="gmx_mpi mdrun -ntomp "
mdrun_command_suffix=""

## COPYING RC
cp -r "${PATH_GENERAL_RC}" "${path_output_dir}"

## DEFINING SUBMISSION PATH
run_file="run_nplm_plumed_contacts_0_generate_configs.sh"
submit_file="submit_run_nplm_plumed_contacts_0_generate_configs.sh"
path_run_file="${PATH2SUBMISSION}/${run_file}"
path_submit_file="${PATH2SUBMISSION}/${submit_file}"

# ## RUNNING
# "${gromacs_command}" grompp -f "${mdp_pulling_file}" \
# 							-c "${output_prefix}_em.gro" \
# 							-p "${input_top}" \
# 							-o "${output_prefix}_pulling" \
# 							-n "${index_file}"

# "${gromacs_command}" mdrun -v -deffnm "${output_prefix}_pulling" -plumed "${plumed_input}"

## COPYING
cp -r "${path_run_file}" "${path_output_dir}/${output_run}"
cp -r "${path_submit_file}" "${path_output_dir}/${output_submit}"

if [[ "${skip_em}" == false ]]; then
	## DEFINING MDP FILE ARRAY
	declare -a mdp_file_array=("${mdp_pulling_file}")

	## OUTPUT PREFIX
	declare -a output_prefix_array=("${output_prefix}_pulling")

	## DEFININIG INITIAL PREFIX
	initial_prefix="${output_prefix}_em"
else
	## DEFINING MDP FILE ARRAY
	declare -a mdp_file_array=("${mdp_em}" "${mdp_pulling_file}")

	## OUTPUT PREFIX
	declare -a output_prefix_array=("${output_prefix}_em" "${output_prefix}_pulling")

	## PLUMED ARRAY
	declare -a plumed_array=("None" "${plumed_input}")
	## DEFINING INITIAL PREFIX
	initial_prefix="${output_prefix}"

fi

## CONVERTING ARRAY TO STRINGS
mdp_file_array_string=$(join_array_to_string " " ${mdp_file_array[@]})
output_prefix_array_string=$(join_array_to_string " " ${output_prefix_array[@]})
plumed_array_string=$(join_array_to_string " " ${plumed_array[@]})

## EDITING RUN SCRIPT
sed -i "s/_RCFILE_/${GENERAL_RC_NAME}/g" "${output_run}"
sed -i "s/_INPUTTOPFILE_/${input_top}/g" "${output_run}"
# sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" "${output_run}"
sed -i "s/_MAXWARN_/${maxwarn}/g" "${output_run}"

sed -i "s/_MDP_FILE_ARRAY_/${mdp_file_array_string}/g" "${output_run}"
sed -i "s/_SIM_PREFIX_ARRAY_/${output_prefix_array_string}/g" "${output_run}"

sed -i "s/_INDEXFILE_/${index_file}/g" "${output_run}"
sed -i "s/_INITIALPREFIX_/${initial_prefix}/g" "${output_run}"
sed -i "s/_PLUMEDARRAY_/${plumed_array_string}/g" "${output_run}"

## EDITTING SUBMISSION SCRIPT
sed -i "s/_JOB_NAME_/${output_directory}/g" "${output_submit}"
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${output_submit}"
sed -i "s/_MDRUNCOMMAND_/${mdrun_command}/g" "${output_submit}"
sed -i "s/_MDRUNCOMMANDSUFFIX_/${mdrun_command_suffix}/g" "${output_submit}"
sed -i "s/_GROMACSCOMMAND_/${gromacs_command}/g" "${output_submit}"
sed -i "s/_RUNSCRIPT_/${output_run}/g" "${output_submit}"

## ADDING SUBMISSION SCRIPT TO JOB LIST
echo "${path_output_dir}/${output_submit}" >> "${JOB_SCRIPT}"
echo "Adding job to job script!"
echo "Path: ${path_output_dir}/${output_submit}"
echo "You should be good to go! Good luck!"

## CREATING SUMMARY
output_summary_file="transfer_summary.txt"

## DEFINING STRING
input_str="
INPUT DIR NAME: ${input_sim_dir_name}
DISTANCE CONFIG: ${desired_distance}
INPUT PREFIX: ${input_prefix}
INPUT GRO: ${config_gro}
INPUT TPR: ${config_tpr} 
INPUT XTC: ${config_xtc}
TIME EXTRACTION (ps): ${desired_frame}
------------
LIGAND RESNAME: ${lig_resname}
GOLD RESNAME: ${gold_core_resname}
LIPID MEMBRANE RESNAME: ${lm_residue_name}
WATER RESIDUE NAME: ${water_residue_name}
------------ MDP OPTIONS ------------
DT: ${dt}
TEMP: ${temp}
NSTEPS: ${nsteps}
------------ PLUMED ------------
COORDINATION RADIUS: ${radius}
NL_CUTOFF: ${nl_cutoff}
NL_STRIDE: ${nl_stride}
AT0: ${at0}
AT2: ${at2}
KAPPA: ${spring}
"

## PRINTING SUMMARY
echo "${input_str}" > "${output_summary_file}"
echo "---- SUMMARY ----"
cat "${output_summary_file}"
echo "-----------------"

done 

done


