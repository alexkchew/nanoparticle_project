#!/bin/bash

# generate_gnp_pdb_files.sh
# This function generates gold nanoparticle pdb files by generating an index file, 
# then generating the pdb file using trjconv. The PDBs could then be used to 
# visualize using PyMOL
# 
# Written by: Alex K. Chew (12/09/2020)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="gridrc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/${global_file_name}"

## DEFINING PYTHON SCRIPT
# CHECK IF FILE EXISTS
if [ ! -e "${PYTHON_GET_GNP_INDEX}" ]; then
	PYTHON_GET_GNP_INDEX="/home/akchew/bin/pythonfiles/modules/gnpdescriptors/prep/get_GNP_ligand_indices.py"
fi

## DEFINING PATH TO SIMULATION
path_to_sim="${1-/home/akchew/scratch/nanoparticle_project/simulations/20201111-GNPs_database_water_sims-try28_subset_ligs-retry/spherical_300.00_K_GNP85_CHARMM36jul2020_Trial_1/hydrationmap-20000}"

## DEFINING SIM PREFIX
sim_prefix="${2-sam_prod}"

## DEFINING INDEX FILE
index_file="gnp_index.ndx"

## DEFINING OUTPUT NAME
output_prefix="${sim_prefix}_GNP"

## DEFINING GNP LIGANDS
gnp_ligand_key="GNP_LIGANDS"

## DEFINING RESIDUE NAME
gold_residue_name="AUNP"

## GOING TO PATH
cd "${path_to_sim}"

## GENERATING INDEX FILE
python "${PYTHON_GET_GNP_INDEX}" --path_to_sim "${path_to_sim}" \
								 --input_prefix "${sim_prefix}" \
								 --output_idx_file "${index_file}" \
								 --output_key_name "${gnp_ligand_key}"

## PRINTING
echo "CREATING GNP GRO FILE ALONE: ${output_prefix}_alone.pdb"

## USING GROMACS TO CREATE PDB FOR GNP ONLY
gmx trjconv -s "${sim_prefix}.tpr" \
			-f "${sim_prefix}.gro" \
			-o "${output_prefix}_alone.pdb" \
			-n "${index_file}" \
            -pbc mol \
            -ur compact \
            -center << INPUTS
${gold_residue_name}
${gnp_ligand_key}
INPUTS

echo "CREATING GNP GRO FILE SYSTEM: ${output_prefix}_System.pdb"

## USING GROMACS TO CREATE PDB FOR SYSTEM
gmx trjconv -s "${sim_prefix}.tpr" \
			-f "${sim_prefix}.gro" \
			-o "${output_prefix}_System.pdb" \
			-n "${index_file}" \
            -pbc mol \
            -ur compact \
            -center << INPUTS
${gold_residue_name}
System
INPUTS


