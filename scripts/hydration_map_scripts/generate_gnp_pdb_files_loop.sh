#!/bin/bash

# generate_gnp_pdb_files_loop.sh
# This function runs the gnp function multiple times
# Written by: Alex K. Chew (12/11/2020)
# 
# conda activate py36_mdd
# Run the code: bash generate_gnp_pdb_files_loop.sh


## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## DEFINING SCRIPT
script_path="${main_dir}/generate_gnp_pdb_files.sh"

## GETTING PATH TO SIM FOLDER
path_to_sim_folder="/home/akchew/scratch/nanoparticle_project/simulations/20201111-GNPs_database_water_sims-try28_subset_ligs-retry"

## DEFINING RELATIVE PATH
relative_sim_path="hydrationmap-20000"

## GETTING DIRECTORIES
read -a directories <<< $(ls ${path_to_sim_folder}/*/ -d)

## PRINTING
echo "Working on: "
for each_dir in ${directories[@]}; do
	echo "--> ${each_dir}"
done

echo "--------"
sleep 2

## RUNNING CODE
for each_dir in ${directories[@]}; do
	## DEFINING FULL PATH
	current_path_to_sim="${each_dir}/${relative_sim_path}"

	## RUNNING CODE
	bash "${script_path}" "${current_path_to_sim}"

done


