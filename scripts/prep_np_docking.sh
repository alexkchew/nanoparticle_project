#!/bin/bash

# prep_np_docking.sh
# The purpose of this code is to use docking algorithms to 
# dock proteins onto nanoparticle surfaces. 
# 
# In the ideal world, we could extract NP-water experiments and use 
# docking experiments to see how NPs will interact with large proteins.

# Written by Alex K. Chew (09/17/2019)

### VARIABLES:



#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## ENABLING EXTERNAL MODULES
shopt -s extglob
# ^ Allows you to move all except 1

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

### INPUT VARIABLES
## FORCE FIELD
forcefield="${FORCEFIELD}" # Force Field (opls-aa or charmm36-nov2016.ff)

## DIAMETER
diameter="2" # "$1" # "$1" # 2 nm

## TEMPERATURE
temp="300.00" # "$2" # "310.15" temperature in K of the system

## DEFINING SHAPE
shape_type="EAM" # "$3"
    # spherical: Spherical shape cutoff from {111} fcc lattice
    # hollow: Hollow spherical shape
    # EAM: embedded atom potential
    
## DEFINING DESIRED TRIAL
trial_num="1" # "$5"

## LIGAND NAME
ligand_names="dodecanethiol"

## DEFINING DESIRED NP INDEX
most_likely_index="1" # First most likely index

## DEFINING FRACTION OF LIGANDS
ligand_fracs="1" # fraction of ligands

## DEFINING INPUT SIMULATION DIRECTORY
simulation_dir_name="190509-2nm_C11_Sims_updated_forcefield"

## DEFINING NEW SIMULATION DIRECTORY NAME
output_simulation_dir="190917-docking_expts"

#######################################
### LOGICALS AND MINOR CALCULATIONS ###
#######################################

## DEFINING DISTANCE TO EDGE
dist_to_edge="0.8"

## DEFINING LOGICAL
# True if you want a hard restart in the computation of most likely configs
# Otherwise, False
want_recalc="False"

######################
### DEFINING PATHS ###
######################

## DEFINING SIMULATION DIRECTORY
simulation_dir="/home/akchew/scratch/nanoparticle_project/simulations/${simulation_dir_name}"

###################################
### DEFINING PROTEIN TO DOCK NP ###
###################################

## DEFINING MOLECULE
docking_molecule_name="methanol"

## DEFINING FULL PATH
docking_molecule_path="${PREP_SOLVENT_FINAL}/${docking_molecule_name}"

## GRO, ITP, PRM FILE
docking_molecule_gro="${docking_molecule_name}.gro"
docking_molecule_itp="${docking_molecule_name}.itp"
docking_molecule_prm="${docking_molecule_name}.prm"

######################
### DEFINING FILES ###
######################

## DEFINING SIMULATION GRO FILE
sim_tpr_file="sam_prod.tpr" # TPR FILE
sim_gro_file="sam_prod.gro" # SIMULATION GRO FILE
analyze_sim_xtc_file="sam_prod_10_ns_whole_rot_trans.xtc" # ANALYZE XTC FILE
extract_sim_xtc_file="sam_prod_10_ns_whole_center.xtc" # EXTRACT SIMULATION XTC FILE
sim_top_file="sam.top"

## DEFINING NEW GRO FILE
new_gro_file="sam_initial.gro"

## DEFINING TRANSFER FILE
transfer_summary="transfer.summary"

#########################################
### MOST LIKELY CONFIGURATION DETAILS ###
#########################################

## DEFINING PYTHON CODE
python_most_likely="${NP_MOST_LIKELY_CODE}"

## DEFINING SUMMARY FILE
most_likely_output_summary_file="np_most_likely.summary"

## DEFINING GOLD RESIDUE NAME
gold_residue_name="AUNP"

###############################
### FILE SYSTEM INFORMATION ###
###############################

## DEFINING DIRECTORY NAME
output_dirname="$(find_outputname ${shape_type} ${temp} ${diameter} ${ligand_names} ${forcefield} ${trial_num})"

### DEFINING PATH TO DIRECTORY
path_to_sim_output_directory="${simulation_dir}/${output_dirname}"

## GETTING NEW DIRECTORY NAME
new_dirname="Docking-${docking_molecule_name}-${output_dirname}"

## DEFINING OUTPUT DIRECTORY
path_to_new_sim_output_directory="${PATH2SIM}/${output_simulation_dir}/${new_dirname}"

#################
### MAIN CODE ###
#################

## FINDING LIGAND RESIDUE NAME
lig_residue_name="$(find_residue_name_from_ligand_txt ${ligand_names})"

## CHECKING OUTPUT DIRECTORY
stop_if_does_not_exist "${path_to_sim_output_directory}"

## CREATING DIRECTORY
create_dir "${path_to_new_sim_output_directory}" -f

## SEEING IF THE OUTPUT SUMMARY FILE EXISTS
path_output_summary="${path_to_sim_output_directory}/${most_likely_output_summary_file}"

## GOING INTO PATH
cd "${path_to_new_sim_output_directory}"

############################################
### PYTHON SCRIPT FOR MOST LIKELY CONFIG ###
############################################

## SEEING IF SUMMARY EXISTS
if [ ! -e "${path_output_summary}" ] || [ "${want_recalc}" == "True" ]; then
    ## RUNNING PYTHON CODE
    /usr/bin/python3.4 "${python_most_likely}" -i "${path_to_sim_output_directory}" -o "${path_to_sim_output_directory}" -s "${most_likely_output_summary_file}" -g "${sim_gro_file}" -x "${analyze_sim_xtc_file}" -p
else
    echo "${most_likely_output_summary_file} already exists! Continuing script..."
fi

######################################################
### LOCATING MOST LIKELY CONFIGURATION AND DUMPING ###
######################################################
## DEFINING LINES TO LOOK UP TO
lines_to_look_up="$((${most_likely_index}+1))"

## FINDING NEAREST TRAJECTORY TIME
most_likely_time=$(head -n "${lines_to_look_up}" "${path_output_summary}" | tail -n1 | awk '{print $4}')
echo "The most likely time found: ${most_likely_time} ps"; sleep 1

## DEFINING INDEX FILE
index_file="gold_with_ligand.ndx"

## CREATING INDEX FILE
make_ndx_gold_with_ligand "${path_to_sim_output_directory}/${sim_tpr_file}" \
                          "${path_to_sim_output_directory}/${index_file}"   \
                          "${lig_residue_name}"   \
                          "${gold_residue_name}"  

## DEFINING COMBINED NAME
combined_name="${gold_residue_name}_${lig_residue_name}"

## DUMPING NEAREST TRAJECTORY
echo "Creating ${new_gro_file} from ${path_to_sim_output_directory}..."
gmx trjconv -f "${path_to_sim_output_directory}/${extract_sim_xtc_file}" -s "${path_to_sim_output_directory}/${sim_tpr_file}" -o "${path_to_new_sim_output_directory}/${new_gro_file}" -n "${path_to_sim_output_directory}/${index_file}" -dump "${most_likely_time}" -pbc mol -center >/dev/null 2>&1 << INPUTS
${gold_residue_name}
${combined_name}
INPUTS

## EDITING SAM FILE TO ALIGN AND MAKE DISTANCE OF NP (WITH CUBIC BOX)
gmx editconf -f "${new_gro_file}" -o "${new_gro_file%.gro}_align.gro" -d "${dist_to_edge}" -princ -bt cubic << INPUTS
System
INPUTS

####################################
### COPYING OVER DOCKING DETAILS ###
####################################

## TOPOLOGY
cp "${path_to_sim_output_directory}/${sim_top_file}" "${path_to_new_sim_output_directory}"
# ITP/PRM FILES
cp "${path_to_sim_output_directory}/"{*.itp,*.prm} "${path_to_new_sim_output_directory}"
# FORCE FIELD
cp -r "${path_to_sim_output_directory}/${forcefield}" "${path_to_new_sim_output_directory}"

## COPYING OVER DOCKING MATERIALS
cp -r "${docking_molecule_path}/"{${docking_molecule_gro},${docking_molecule_itp},${docking_molecule_prm}} "${path_to_new_sim_output_directory}"

################################
### CONVERTING INTO PDB FILE ###
################################
## NP SYSTEM
gmx editconf -f "${new_gro_file%.gro}_align.gro" -o "${new_gro_file%.gro}_align.pdb"
## DOCKING MATERIALS
gmx editconf -f "${docking_molecule_gro}" -o "${docking_molecule_gro%.gro}.pdb"

