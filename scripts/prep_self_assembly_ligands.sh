#!/bin/bash

# prep_self_assembly_ligands.sh
# This script will create ligands based on Pool's recommendations and equilibrate it. The main goal is to get ligands on a gold surface and equilibrate.

# This script is based on the work done by 
# Djebaili, T., Richardi, J., Abel, S. & Marchi, M. Atomistic simulations of the surface coverage of large gold nanocrystals. J. Phys. Chem. C 117, 17791–17800 (2013).

# Written by Alex K. Chew (12/17/2017)

### SOURCING ALL VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

## PRINTING SCRIPT NAME
print_script_name

### --- USER-SPECIFIC PARAMETERS (BEGIN) --- ###
ligand_names="bidente6chain"
# "bidente"
# "bidente3chain"
# "butanethiol"
# "bidentate2"
# "butanethiol" # "butanethiol" # Name of ligand
forcefield="pool.ff" # Force field type

## DEFINING DEFAULT PROPERTIES OF LIGANDS
box_size="4"

## DEFINING IF YOU WANT NONFRAGMENT-BASED APPROACH
want_non_fragment=true

# "3" # Box size of ligand solvated in nms
temp="300" # "310" # Temperature to run the equilibration simulation
length2edge="0.2" # Edge from the ligand -- a way from separating ligands from each other

## DEFINING SIMULATION PARAMETERS
num_steps="2500000" # "5000000" # 10 ns

## MAXWARN
max_warn_index="5" # Max warnings from gmx grompp

## MDP FILES -- located in prep_gold folder
energy_min_mdp="minim_sam_gmx5.mdp"
input_equil_mdp="npt_equil_gmx5_charmm36_noPME.mdp"
## OUTPUT EQUIL MDP
equil_mdp="npt_equil_gmx5_charmm36.mdp"
## SUBMISSION FILES
submit_file="submit.sh"
submit_file_loc="${PREP_GOLD_INPUT_FILE}/Submit_Scripts"

## DEFINING IMPORTANT SCRIPTS
prep_ligand_script="${MDBUILDER}/ligand/prep_self_assembly_ligands.py" # Script to get gro and itp file from ligand builder
prep_ligand_non_frag_script="${MDBUILDER}/ligand/prep_self_assembly_ligands_non_fragment_based.py" # Script to get gro and itp file from ligand builder

## DEFINING PARENT FOLDER
if [[ "${want_non_fragment}" == true ]]; then
	parent_folder="${PREP_GOLD_SURF_NONFRAG}"
else
	parent_folder="${PREP_GOLD_SURF}"
fi

## DEFINING OUTPUT NAME
output_name="${box_size}_nm_${temp}_K_${ligand_names}" # Box size, temperature, ligand name
output_dir="${parent_folder}/${output_name}"

### CHECKING IF DIRECTORY EXISTS
check_output_dir "${output_dir}"

## RUNNING PYTHON SCRIPT TO OUTPUT INTO THE OUTPUT FOLDER
echo "----- RUNNING ${prep_ligand_script} SCRIPT ------"
if [[ "${want_non_fragment}" == true ]]; then
	python "${prep_ligand_non_frag_script}" --names "${ligand_names}" \
											   --ofold "${output_dir}" \
											   --database "${DATABASE_SELFASSEMBLY_LIGS}"
else
	python3 "${prep_ligand_script}" --names "${ligand_names}" \
									--ofold "${output_dir}" \
									--prep "${INPUT_FRAG_LIG_PREP}"

fi

## GOING TO DIRECTORY
cd "${output_dir}"

## CHECKING IF EXISTING
stop_if_does_not_exist "${ligand_names}.gro"

#####################
### COPYING FILES ###
#####################
# MDP FILES
cp -rv "${PREP_GOLD_MDP_DIR}/${energy_min_mdp}" "${output_dir}"
cp -rv "${PREP_GOLD_MDP_DIR}/${input_equil_mdp}" "${equil_mdp}"

# SUBMISSION FILE
cp -rv "${submit_file_loc}/${submit_file}" "${output_dir}"

#####################
### EDITING FILES ###
#####################

## EDITING MDP FILE
sed -i "s/NSTEPS/${num_steps}/g" ${equil_mdp} # Changing equilibrium file number of steps
sed -i "s/TEMPERATURE/${temp}/g" ${equil_mdp} # Changing equilibrium file

## EDITING SUBMISSION FILE
sed -i "s/JOB_NAME/${output_name}/g" ${submit_file} # Changing job name to match output
sed -i "s/LIGAND/${ligand_names}/g" ${submit_file} # Changing ligand names
sed -i "s/EQUIL_MDP/${equil_mdp}/g" ${submit_file} # Changing equilibrium file name

## USING EDITCONF TO GET THE CORRECT PBC -- assumes ligand names gro remains the same
echo "----- FIXING LIGAND PBC ------"
gmx editconf -f "${ligand_names}.gro" -o "${ligand_names}.gro" -d "${length2edge}"

## SOLVATING USING GMX SOLVATE
echo "----- SOLVATING ------"
## START BY SOLVATING
gmx solvate -cs "${ligand_names}.gro" -p $"${ligand_names}.top" -o "${ligand_names}_solv.gro" -box ${box_size} ${box_size} ${box_size}
solvate_with_correct_total_atoms "${ligand_names}.gro" "${ligand_names}.top" "${ligand_names}_solv.gro"

## ENERGY MINIMIZING
echo "----- ENERGY MINIMIZING ------"
gmx grompp -f ${energy_min_mdp} -c ${ligand_names}_solv.gro -p ${ligand_names}.top -o ${ligand_names}_em.tpr -maxwarn ${max_warn_index}
gmx mdrun -v -nt 1 -deffnm "${ligand_names}_em"

#### PRINTING SUMMARY
echo "------------- SUMMARY -------------"
echo "Path: ${output_dir}"
echo "Ligands: ${ligand_names}"
echo "Temperature: ${temp}"
echo "Box size: ${box_size}"

