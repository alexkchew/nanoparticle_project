#!/bin/bash

# prep_lipid_bilayers.sh
# The purpose of this script is to simply equilibrate the lipid bilayer according to the conditions that we have. Ideally, we would like physiological temperatures and pressures.
# ASSUMPTIONS:
#   - We are assuming that you have already ran the CHARMM-Gui to generate membranes
#   - Now, we can use your outputs to equilibrate and run subsequent NP simulations
#   - We need the pdb, topology, itp/prm and mdp files to run equilibration runs

# Created on: 11/03/2018
# Written by: Alex K. Chew (alexkchew@gmail.com)

##################################################
### LOADING PRE-DEFINED VARIABLES / FUNCTIONS ####
##################################################
source "../bin/nanoparticle_rc.sh"
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING LIPID NAME
lipid_name="DOPC"

## DEFINING SIZE
lipid_size="196"
# "144"
# "225"
# "100"
# "196" # "196"

## DEFINING TEMPERATURE
temp="300.00"

## FORCE FIELD
forcefield="${FORCEFIELD}" # Force Field (opls-aa or charmm36-nov2016.ff)

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING RUN SCRIPT
input_run_script="run_prep_lipid_bilayers.sh"
output_run_script="run.sh"

## SUBMISSION SCRIPT
input_submit_file="submit_lipid_bilayer.sh"
output_submit_file="submit.sh"

## DEFINING GROMACS FOLDER
gromacs_folder="gromacs"

## NUMBER OF CORES
num_cores="28"

## OUTPUT PREFIX
output_prefix="lipid_membrane"

## MDP FILES
equil_npt_mdp="equil_npt.mdp"

## DEFINING SIMULATION PARAMETERS
sim_timestep="0.002"    # simulation time steps
sim_nsteps="2500000"    # 5 ns equilibration

###################
### INPUT FILES ###
###################

## DEFINE MEMBRANE FOLDER
lipid_membrane_folder=$(get_lipid_membrane_folder_name ${lipid_name} \
                                                       ${temp} \
                                                       ${lipid_size})


######################
### DEFINING PATHS ###
######################

## DEFINING PATH TO SUBMISSION
path_input_submit="${PATH2SUBMISSION}/${input_submit_file}"
path_input_run_script="${PATH_LIPID_MEMBRANE_SCRIPTS}/${input_run_script}"

## DEFINING PATH
path_lipid_membrane="${INPUT_LIPID_MEMBRANE}/${lipid_membrane_folder}/${gromacs_folder}"
# PREP_LIPID_MEMBRANE
## MDP FILE INFORMATION
mdp_parent_path="${INPUT_MDP_PATH}/charmm36"

## MDP FILE PATH
mdp_file_path="${mdp_parent_path}/lipid_bilayers"

###################
### MAIN SCRIPT ###
###################

## CHECKING IF PATH EXISTS
check_dir_exist "${path_lipid_membrane}"

## GOING INTO DIRECTORY
cd "${path_lipid_membrane}"

## COPYING FILES OVER
cp "${mdp_file_path}/${equil_npt_mdp}" "${path_lipid_membrane}"
cp -r "${path_input_submit}" "${output_submit_file}"
cp -r "${path_input_run_script}" "${output_run_script}"

#####################
### EDITING FILES ###
#####################

## SUBMISSION FILE
sed -i "s/_JOB_NAME_/${lipid_membrane_folder}/g" "${output_submit_file}"
sed -i "s/_BASHSCRIPT_/${output_run_script}/g" "${output_submit_file}"

## RUN FILE
sed -i "s/_MDPEQUIL_/${equil_npt_mdp}/g" "${output_run_script}"
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" "${output_run_script}"
sed -i "s/_NUMCORES_/${num_cores}/g" "${output_run_script}"

## EQUIL MDP FILE
sed -i "s/_DT_/${sim_timestep}/g" "${equil_npt_mdp}"
sed -i "s/_NSTEPS_/${sim_nsteps}/g" "${equil_npt_mdp}"
sed -i "s/_TEMPERATURE_/${temp}/g" "${equil_npt_mdp}"

## ADDING TO SUBMISSION FILE
echo "${path_lipid_membrane}/${output_submit_file}" >> "${JOB_SCRIPT}"

#### PRINTING SUMMARY
echo "------------- SUMMARY -------------"
echo "Lipid membrane: ${lipid_name}"
echo "Total lipid size: ${lipid_size} x ${lipid_size}"
echo "Force field: ${forcefield}"
echo "Temperature: ${temp}"
echo "Included equilibration job at: ${JOB_SCRIPT}"



#
#
### PDB, TOP, ITP
#input_structure_file="step5_charmm2gmx.pdb"
#input_topology_dir="toppar"
##input_prm_file="charmm36.itp"
##input_itp_file="DOPC.itp"
#input_top_file="topol.top"
#
### WATER MODEL
#water_itp_file="TIP3.itp"
#
### TEMPERATURE
#temperature="300.00" # K
#
### DEFINING DISTANCE FROM EDGE
#dist_from_edge="0.01" # Default -- 0 
## IF error -- use 0.01 edge <-- will resolve issues with lipid overlap
#
### MDP FILES
#em_mdp="em.mdp" # "em_test.mdp" # 
#equil_npt_mdp="equil_npt.mdp"
#
#
#
####################
#### OUTPUT FILE ###
####################
#
### OUTPUT ITP, TOP
#output_gro="${output_prefix}.gro"
#output_top="${output_prefix}.top"
#
### OUTPUT DIRECTORY NAME
#output_dir_name="${temperature}_K_${lipid_size}_${lipid_size}_${lipid_name}"
#
### OUTPUT SUBMISSION NAME
#output_submit_name="submit.sh"
#
#######################
#### DEFINING PATHS ###
#######################
#
### DEFINING PATH TO SUBMISSION
#input_submit_path="${PATH2SUBMISSION}/${input_submit_file}"
#
### MDP FILE INFORMATION
#mdp_parent_path="${INPUT_MDP_PATH}/charmm36"
#
### MDP FILE PATH
#mdp_file_path="${mdp_parent_path}/lipid_bilayers"
#
### LIPID MEMBRANE PATH
#lipid_membrane_path="${INPUT_LIPID_MEMBRANE}/${lipid_membrane_folder}"
#
### DIRECTORY PATH
#working_directory_path="${PREP_LIPID_MEMBRANE}/${output_dir_name}"
#
#######################################
#### DEFINING SIMULATION PARAMETERS ###
#######################################
#
### DEFINING SIMULATION PARAMETERS
#sim_timestep="0.001"    # simulation time steps
#sim_nsteps="5000000"    # 5 ns equilibration
## "2000000"    # number of steps
#
####################
#### MAIN SCRIPT ###
####################
#
### CHECKING IF DIRECTORY EXISTS
#check_output_dir "${working_directory_path}"
#
### CHANGING DIRECTORY TO OUTPUT DIRECTORY
#cd "${working_directory_path}"
#
###########################
#### COPYING OVER FILES ###
###########################
#
### STRUCTURE FILES
#gmx editconf -f "${lipid_membrane_path}/${input_structure_file}" -o "${working_directory_path}/${output_gro}" -d "${dist_from_edge}"
### ITP FILES
#cp -r "${lipid_membrane_path}/${input_topology_dir}" "${working_directory_path}"
## cp "${lipid_membrane_path}/"{${input_prm_file},${input_itp_file},${water_itp_file}} "${working_directory_path}"
### TOPOLOGY
#cp "${lipid_membrane_path}/${input_top_file}" "${working_directory_path}/${output_top}"
### MDP FILES
#cp "${mdp_file_path}"/{${em_mdp},${equil_npt_mdp}} "${working_directory_path}"
### SUBMISSION FILE
#cp "${input_submit_path}" "${working_directory_path}/${output_submit_name}"
#
#################################
#### EDITING SUBMISSION FILES ###
#################################
#
### EQUIL MDP FILE
#sed -i "s/_DT_/${sim_timestep}/g" "${equil_npt_mdp}"
#sed -i "s/_NSTEPS_/${sim_nsteps}/g" "${equil_npt_mdp}"
#sed -i "s/_TEMPERATURE_/${temperature}/g" "${equil_npt_mdp}"
#
### SUBMISSION FILE
#sed -i "s/_MDPEM_/${em_mdp}/g" "${output_submit_name}"
#sed -i "s/_MDPEQUIL_/${equil_npt_mdp}/g" "${output_submit_name}"
#sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" "${output_submit_name}"
#sed -i "s/_JOB_NAME_/${output_dir_name}/g" "${output_submit_name}"
#
############################
#### ENERGY MINIMIZATION ###
############################
#
### RUNNING ENERGY MINIMIZATION ** DEPRECIATED -- NOW DONE BY SUBMISSION **
## gmx grompp -f ${em_mdp} -c ${output_gro} -p ${output_top} -o "${output_prefix}_em.tpr"
## gmx mdrun -v -nt 1 -deffnm "${output_prefix}_em"
#
### ADDING TO SUBMISSION FILE
#echo "${working_directory_path}" >> "${JOB_SCRIPT}"
#
##### PRINTING SUMMARY
#echo "------------- SUMMARY -------------"
#echo "Lipid membrane: ${lipid_name}"
#echo "Total lipid size: ${lipid_size} x ${lipid_size}"
#echo "Force field: ${forcefield}"
#echo "Temperature: ${temperature}"
#echo "Included equilibration job at: ${JOB_SCRIPT}"
#
#
