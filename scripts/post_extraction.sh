#!/bin/bash

# post_extraction.sh
# This bash script will run post extraction protocols. This is useful if you want to run additional gmx commands without invoking truncation. 
# USAGE: bash post_extraction.sh
# Written by Alex K. Chew (alexkchew@gmail.com, 05/10/2018)

## ** UPDATES ** ##
# 20180706: Inclusion of MSD calculations for diffusion coefficients

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="../bin/nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${script_dir}/${global_file_name}"
### BEGINNING OF SCRIPT ###

## PRINTING SCRIPT NAME
print_script_name

## SETTING SO THAT YOU WILL GET ERRORS IF SOMETHING GOES WRONG ## TURNING OFF FOR NOW
# set -e

### FUNCTION TO CREATE A GRO AND XTC FOR NO WATER AND CENTERED GOLD
# The purpose of this function is to create gro and xtc file that we can analyze using MDDescriptors. We are interested in no water and centered gold atoms. It is generalized so that there are no solvents. 
## INPUTS:
#   $1: input tpr file
#   $2: input xtc file
#   $3: output gro file
#   $4: output xtc file
#   $5: index file
#   $6: centering gold residue name
#   $7: non-water string name
#   $9: output pdb file (PYMOL ENABLED)
#   $10: output tpr file
## DEPRECIATED INPUT: #   $8: frame index for gro file
function no_water_gold_center_gro_xtc_ () {
### FINDING TOTAL FRAMES IN THE CURRENT XTC
checkxtc "$2" last_frame total_frames

### CREATING SPECIFIC INDEX FILE
gmx make_ndx -f "$1" -o "$5" << INPUTS
keep 0
keep 1
r $6
! r SOL
name 1 $7
q
INPUTS

### CREATING GRO AND XTC FILE
## GRO FILE
gmx trjconv -s "$1" -f "$2" -pbc mol -center -n "$5" -o "$3" -b "${total_frames}" << INPUTS
$6
$7
INPUTS

## XTC FILE
gmx trjconv -s "$1" -f "$2" -pbc mol -center -n "$5" -o "$4" << INPUTS
$6
$7
INPUTS
## PDB FILE
gmx trjconv -s "$1" -f "$2" -pbc mol -center -n "$5" -o "$9" << INPUTS
$6
$7
INPUTS

## TPR FILE
gmx convert-tpr -s "$1" -o "${10}" -n "$5" << INPUTS
$7
INPUTS

}


### FUNCTION TO CREATE PDB FILE FOR PYMOL
# The purpose of this function is to create a pdb file that is viewable by pymol. For now, we are interested in the last frame of the xtc trajectory. Note that this will invoke periodic boundary conditions, which will make all the molecules whole and their center of masses placed in the box (i.e. pbc mol)
# INPUTS:
#   $1: input tpr file
#   $2: input gro file
#   $3: output pdb file
#   $4: centering residue name
#   $5: output name
function create_pdb_pymol_ {

### CREATING PDB FILE
gmx trjconv -s "$1" -f "$2" -o "$3" -center -pbc mol << INPUTS
$4
$5
INPUTS
}

### FUNCTION TO USE TRJCONV AND FIX ROTATION + TRANSLATION
# INPUTS:
#   $1: input tpr file
#   $2: input xtc file
#   $3: residue name
#   $4: system name
#   $5: output xtc file
function rot_trans_ {
    ## DEFINING  VARIABLES
    input_tpr_file="$1"
    input_xtc_file="$2"
    res_name="$3"
    system_name="$4"
    output_xtc_file="$5"
    ## DEFINING INDEX FILE
    index_file="rot_trans.ndx"

### CREATING INDEX FILE
gmx make_ndx -f "${input_tpr_file}" -o "${index_file}" << INPUT
keep 0
r ${res_name}
q
INPUT

### CHECKING IF THE RESIDUE IS WITHIN (Checks if string is empty)
if [ -z "$(grep ${res_name} ${index_file})" ]; then
gmx make_ndx -f "${input_tpr_file}" -o ${index_file} << INPUT
r ${res_name}
q
INPUT
fi

### EXTRACTION AND PLACES CENTER OF MASSES WITHIN BOX
gmx trjconv -s "${input_tpr_file}" -f "${input_xtc_file}" -o "${input_xtc_file%.xtc}_center" -pbc mol -center -n ${index_file} << INPUT
${res_name}
${system_name}
INPUT
# CENTERING ON RESIDUE, OUTPUTTING SYSTEM

### RESTRAINING ROTATION AND TRANSLATIONAL DEGREES OF FREEDOM
gmx trjconv -s "${input_tpr_file}" -f "${input_xtc_file%.xtc}_center" -o "${output_xtc_file}" -fit rot+trans -center -n ${index_file} << INPUT
${res_name}
${res_name}
${system_name}
INPUT
# CENTERING AND ROT+TRANS ON RESIDUE, OUTPUTTING SYSTEM
}




### FUNCTION TO CALCULATE THE DIFFUSION COEFFICIENT
# The purpose of this function is to calculate the diffusion coefficient
# INPUTS:
#   $1: input tpr file
#   $2: input xtc file
#   $3: output xvg file (typically msd.xvg)
#   $4: atom selection based on tpr file (can edit later with index)
# OUTPUTS:
#   This function will output an xvg file with the diffusion coefficient
function calc_msd_ {
    ## DEFINING VARIABLES
    input_tpr_file_="$1"
    input_xtc_file_="$2"
    output_xvg_file_="$3"
    atom_selection_="$4"
    
    ## RUNNING GMX COMMAND
gmx msd -f "${input_xtc_file_}" -s "${input_tpr_file_}" -o "${output_xvg_file_}" -trestart 100 -beginfit 0 -endfit 5000 << INPUTS
${atom_selection_}
INPUTS
} # -trestart indicates the time of the sliding window



### FUNCTION TO GET RMSF
# The purpose of this function is to get the root mean squared fluctuation for each molecule
# INPUTS:
#   $1: input tpr file
#   $2: input xtc file
#   $3: residue name
#   $4: index name
#   $5: output rmsf file
function calc_gmx_rmsf_ () {
    ## DEFINING IINPUTS
    input_tpr_file_="$1"
    input_xtc_file_="$2"
    input_ndx_file_="$3"
    residue_name_="$4"
    output_rmsf_file_="$5"
    
    ## CREATING INDEX FILE FOR ONLY THE RESIDUE
    index_make_ndx_residue_ "${input_tpr_file_}"  "${input_ndx_file_}" "${residue_name_}"
    
    ## RUNNING GMX RMSF
gmx rmsf -f "${input_xtc_file_}" -s "${input_tpr_file_}" -n "${input_ndx_file_}" << INPUTS
${residue_name_}
INPUTS

}

### DEFINING IMPORTANT SCRIPTS
calc_gmx_principal_script="${PATH2POST_SCRIPTS}/calc_gmx_principal.sh"
calc_gmx_hbond_script="${PATH2POST_SCRIPTS}/calc_gmx_hbond.sh"
calc_rmsf_script="${PATH2POST_SCRIPTS}/calc_gmx_rmsf.sh"
calc_gmx_gyrate="${PATH2POST_SCRIPTS}/calc_gmx_gyrate.sh"
get_gold_with_ligand="${PATH2POST_SCRIPTS}/get_gold_with_ligand.sh"

#################################
### DEFINING INPUT PARAMETERS ###
#################################

### DEFINING MAIN ANALYSIS DIRECTORY
Main_Extract_Dir="191216-ROT_15_16"
# "191216-ROT_15_16_switch_solvent"
# 
# "191208-Switching_solvents_ROT_others"
# "HYDROPHOBICITY_PROJECT_ROTELLO"
# "191204-Switching_solvents_ROT_others_rerun"
# "191204-Switching_solvents_ROT_others"
# "HYDROPHOBICITY_PROJECT_C11"
# "190520-2nm_ROT_Sims_updated_forcefield_new_ligands"
# 
# "190520-2nm_ROT_Sims_updated_forcefield_new_ligands"
# "190909-Switching_solvents_large_box"
# "190906-Most_likely_NP_mixed_solvents"
# "190906-Most_likely_NP_mixed_solvents"
# "190903-Rotello_lig_cyclohexane"
# "190520-2nm_ROT_Sims_updated_forcefield_new_ligands" 
# Main_Extract_Dir="test_dir" 
# "SELF_ASSEMBLY_FULL" # "180705-Trial_2_3_ALL" 190509-2nm_C11_Sims_updated_forcefield

### DEFINING SPECIFIC EXTRACTION DIRECTORY
specific_extraction_dir="EAM_300.00_K_2_nmDIAM_ROT016_CHARMM36jul2017_Trial_1"
## NEED TO DOUBLE-CHECK TRUNC CODE

# "switch_solvents-50000-dmso-EAM_300.00_K_2_nmDIAM_ROT004_CHARMM36jul2017_Trial_1"
# "EAM_300.00_K_2_nmDIAM_C11COO_CHARMM36jul2017_Trial_1"
# "switch_solvents-50000-dmso-EAM_300.00_K_2_nmDIAM_ROT005_CHARMM36jul2017_Trial_1"
# "switch_solvents-50000-dmso-EAM_300.00_K_2_nmDIAM_ROT014_CHARMM36jul2017_Trial_1"
# "switch_solvents-50000-dmso-EAM_300.00_K_2_nmDIAM_ROT006_CHARMM36jul2017_Trial_1" # empty if loop through all extraction directories
# switch_solvents-50000-dmso-EAM_300.00_K_2_nmDIAM_ROT012_CHARMM36jul2017_Trial_1
# mixed_solvent_methanol_60-EAM_300.00_K_2_nmDIAM_C11OH_CHARMM36jul2017_Trial_1
# EAM_300.00_K_2_nmDIAM_dodecanethiol_CHARMM36jul2017_Trial_1

## TRUE IF YOU WANT SELF ASSEMBLED MONOLAYERS
want_self_assembled_monolayers="False" # True if you want self-assembled monolayers
want_planar_sams="False"

#"mdRun_433.15_6_nm_ACE_10_WtPercWater_spce_dioxane" "mdRun_433.15_6_nm_ACE_10_WtPercWater_spce_dmso" "mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_dioxane" "mdRun_433.15_6_nm_PDO_10_WtPercWater_spce_dmso" "mdRun_433.15_6_nm_PRO_10_WtPercWater_spce_dioxane" "mdRun_433.15_6_nm_PRO_10_WtPercWater_spce_dmso"

### DEFINING INPUT FILES
input_gro_file="sam_prod.gro" # Structure file
input_tpr_file="sam_prod.tpr" # Job file
input_xtc_file="sam_prod.xtc" # Trajectory file

# input_xtc_file="mixed_solv_prod_last_1_ns.xtc" # Trajectory file
if [ "${want_self_assembled_monolayers}" == "True" ]; then
    input_gro_file="gold_ligand_equil.gro" # Structure file
    input_tpr_file="gold_ligand_equil.tpr" # Job file
    input_xtc_file="gold_ligand_equil.xtc" # Trajectory file
fi
## INDEX FILE
index_file="index.ndx"

### DEFINING OUTPUT FILES
output_xtc_file="mixed_solv_prod_10_ns_center_rot_trans_center.xtc"
output_pdb_file="sam_prod_pymol.pdb"
# output_xtc_file="mixed_solv_prod_10_ns_whole.xtc"
# output_xtc_file="mixed_solv_prod_last_1_ns_center_rot_trans_center.xtc"
# output_xtc_file="mixed_solv_prod_last_1_ns.xtc"

### DEFINING JOBS "ROT_TRANS" "ROT_TRANS"
declare -a job_type_list=("TRUNC" "NO_WATER_GOLD_CENTER" "ROT_TRANS" "CALC_GMX_PRINCIPAL" "CALC_GMX_RMSF" "CALC_GMX_GYRATE" "CALC_GMX_HBOND")
#  "TRUNC" "NO_WATER_GOLD_CENTER" "ROT_TRANS" "CALC_GMX_PRINCIPAL" "CALC_GMX_RMSF" "CALC_GMX_GYRATE"
#  "CALC_GMX_HBOND"
# "TRUNC" "NO_WATER_GOLD_CENTER" "ROT_TRANS" "CALC_GMX_PRINCIPAL" "CALC_GMX_RMSF" 
# "CALC_GMX_HBOND"
# "TRUNC" "NO_WATER_GOLD_CENTER" "ROT_TRANS" "CALC_GMX_PRINCIPAL" "CALC_GMX_RMSF" "CALC_GMX_HBOND"
# "TRUNC" "NO_WATER_GOLD_CENTER" "ROT_TRANS")  # 
# "TRUNC" "NO_WATER_GOLD_CENTER" "ROT_TRANS" "CALC_GMX_PRINCIPAL" "CALC_GMX_RMSF" "CALC_GMX_GYRATE"
#  "NO_WATER_GOLD_CENTER" "CALC_GMX_HBOND"
# "TRUNC" "ROT_TRANS" <-- JOBS FOR NEW FORCE FIELD STUFF
#  "NO_WATER_GOLD_CENTER" "ROT_TRANS" "CENTER_XTC" <-- add on
# "TRUNC" "NO_WATER_GOLD_CENTER" "ROT_TRANS" "CALC_GMX_PRINCIPAL" "CALC_GMX_RMSF" "CALC_GMX_GYRATE"
# NOTE: ROT TRANS REQUIRED FOR RMSF 
# "CALC_GMX_HBOND"
# "ROT_TRANS"
# "NO_WATER_GOLD_CENTER"  "CALC_GMX_PRINCIPAL" "CALC_GMX_RMSF" "CALC_GMX_HBOND") # "NO_WATER_GOLD_CENTER") #  "TRUNC" "ROT_TRANS" "CALC_GMX_RMSF"
    # TRUNC: This is designed to truncate the trajectories with pbc whol
    # ROT_TRANS: This is designed for ROT_TRANS fixing for spatial mapping purposes
    # CALC_GMX_PRINCIPAL: This is for calculation of gmx principal
    # PYMOL_PDB: creates pdb file for pymol (last frame)
    # NO_WATER_GOLD_CENTER: This is designed to get gro and xtc files for no water and gold xtc
    # CENTER_XTC: This is designed to center and pbc mol a trajectory
    
    # CALC_GMX_MSD: This calculation is for gmx msd -- diffusion coefficient
    # CALC_GMX_HBOND: This calculates hydrogen bonding information
    # CALC_GMX_RMSF: This calculates the root mean squared fluctuations of the ligands
    # CALC_GMX_GYRATE: This calculates gmx gyrate (radius of gyration)

### PATH TO MAIN EXTRACTION DIRECTORYs
path_main_extract_dir="${PATH2SIM}/${Main_Extract_Dir}"

## ITERATION COUNTER
ITER=0
## LOOPING THROUGH EACH DIRECTORY WITHIN THE CATEGORY
for each_file in $(ls ${path_main_extract_dir}); do

    ## SEEING IF SPECIFIC DIRECTORY SPECIFIED
    if [ ! -z "${specific_extraction_dir}" ]; then
        each_file="${specific_extraction_dir}"
    fi

    ## PRINTING
    echo "-------------------------------------------"
    echo "DIRECTORY: ${each_file}"
    echo "-------------------------------------------"
    sleep 1

    ## DEFINING PATHS
    path_to_each_file="${path_main_extract_dir}/${each_file}"
    
    ## GOING INTO DIRECTORY
    cd "${path_to_each_file}"
    
    ## LOOPING THROUGH EACH JOB
    for job_type in "${job_type_list[@]}"; do

## DEFINING JOB SCRIPTS
# job_hbond="${PATH2ANALYSIS_SCRIPTS}/gromacs_hbond.sh"
# job_hbond_extract="${PATH2ANALYSIS_SCRIPTS}/gromacs_hbond_extract.sh"

## DEFINING GENERAL INPUT XTC 10 NS WHOLE
input_xtc_file_10ns_whole="sam_prod_10_ns_whole.xtc"
input_xtc_file_10ns_whole_rot_trans="sam_prod_10_ns_whole_rot_trans.xtc"

#### ROT_TRANS DEFINITIONS
system_selection="System"
rot_trans_input_xtc_file="${input_xtc_file_10ns_whole}"
rot_trans_output_xtc_file="${input_xtc_file_10ns_whole_rot_trans}"
rot_trans_residue_name="AUNP"

#### TRUNCTION DEFINITIONS
truncation_time="10000" # ps truncation
trunc_output_xtc_file="sam_prod_10_ns_whole.xtc"

#### PYMOL DECLARATIONS
pymol_center_selection="AUNP"
pymol_output_selection="System"

#### NO_WATER_GOLD_CENTER DEFINITIONS
no_water_gold_center_gro_file="sam_prod_10_ns_whole_no_water_center.gro"
no_water_gold_center_xtc_file="sam_prod_10_ns_whole_no_water_center.xtc"
no_water_gold_center_pdb_file="sam_prod_10_ns_whole_no_water_center.pdb"
no_water_gold_center_index_file="sam_prod_10_ns_whole_no_water_center.ndx"
no_water_gold_center_tpr_file="sam_prod_10_ns_whole_no_water_center.tpr"

## SAME DEFINITIONS FOR PLANAR SAMS
if [ "${want_planar_sams}" == "True" ]; then
    pymol_center_selection="System"
    pymol_output_selection="System"
    no_water_gold_center_selection="System"
    no_water_gold_non_water_selection="System"
fi

#### CENTERING DEFINITIONS
center_residue_name="AUNP"
center_output_xtc_file="sam_prod_10_ns_whole_center.xtc"
center_output_ndx_file="gold.ndx"

#### CALC_GMX_PRINCIPAL DEFINITIONS
principal_output_moi_file="moi.xvg"

#### CALC_GMX_MSD DEFINITIONS
msd_atom_selection="non-Water" # "AUNP"
msd_output_file="msd.xvg"

#### CALC_GMX_HBOND DEFINITIONS
hbond_input_solvent_residue="SOL" # Which molecule do you want to study hydrogen bonding for?
hbond_output_hbnum="hbnum.xvg" # Output file for number of hydrogen bonds

#### CALC RMSF INFORMATION
rmsf_output_xvg="rmsf.xvg"
rmsf_input_xtc_file="${input_xtc_file_10ns_whole_rot_trans}"
rmsf_input_tpr_file="sam_prod.tpr"

#### CALC GMX GYRATE INFORMATION
gyrate_xvg="gyrate.xvg"

#### GMX HBOND SCRIPT
hbond_input_xtc_file="${trunc_output_xtc_file}"

######################
### DEFINING PATHS ###
######################



###################
### MAIN SCRIPT ###
###################

        
        ## PRINTING
        echo "-------------------------------------------"
        echo "WORKING ON ${job_type}"
        echo "-------------------------------------------"
        sleep 1
        ########################
        ### GROMACS COMMANDS ###
        ########################
        
        ### TRUNC
        if [ "${job_type}" == "TRUNC" ]; then
            ### TRUNCATING THE SYSTEM
            trunc_ "${input_tpr_file}" "${input_xtc_file}" "${system_selection}" "${trunc_output_xtc_file}" "${truncation_time}"
        
        ### ROT_TRANS
        elif [ "${job_type}" == "ROT_TRANS" ]; then
            ## GOING INTO DIRECTORY
            cd "${path_to_each_file}"
            ### RESTRAINING ROTATION AND TRANSLATIONAL DEGREES OF FREEDOM
            rot_trans_ "${input_tpr_file}" "${rot_trans_input_xtc_file}" "${rot_trans_residue_name}" "${system_selection}" "${rot_trans_output_xtc_file}"
        
        ### GMX PRINCIPAL
        elif [ "${job_type}" == "CALC_GMX_PRINCIPAL" ]; then
            ## RUNNING FROM SCRIPTS
            cd "${PATH2POST_SCRIPTS}"
            ## RUNNING GMX PRINCIPAL SCRIPT
            bash "${calc_gmx_principal_script}" "${input_tpr_file}" \
                                                "${input_xtc_file_10ns_whole}" \
                                                "${principal_output_moi_file}" \
                                                "${path_to_each_file}"
            
        ### GMX RMSF
        elif [ "${job_type}" == "CALC_GMX_RMSF" ]; then
            ## RUNNING FROM SCRIPTS
            cd "${PATH2POST_SCRIPTS}"
            ## RUNNING BASH SCRIPT
            bash "${calc_rmsf_script}" \
                    "${rmsf_input_tpr_file}" \
                    "${rmsf_input_xtc_file}" \
                    "${rmsf_output_xvg}" \
                    "${path_to_each_file}"
        
        ### PYMOL PDB
        elif [ "${job_type}" == "PYMOL_PDB" ]; then
            create_pdb_pymol_ "${input_tpr_file}" \
                              "${input_gro_file}" \
                              "${output_pdb_file}" \
                              "${pymol_center_selection}" \
                              "${pymol_output_selection}"
            
        ### NO_WATER_GOLD_CENTER
        elif [ "${job_type}" == "NO_WATER_GOLD_CENTER" ]; then
            ## RUNNING FROM SCRIPTS
            cd "${PATH2POST_SCRIPTS}"
            bash "${get_gold_with_ligand}" "${input_tpr_file}" \
                                            "${trunc_output_xtc_file}" \
                                            "${no_water_gold_center_gro_file}" \
                                            "${no_water_gold_center_xtc_file}" \
                                            "${no_water_gold_center_tpr_file}" \
                                            "${no_water_gold_center_pdb_file}" \
                                            "${no_water_gold_center_index_file}" \
                                            "${path_to_each_file}"

        ### NO_WATER_GOLD_CENTER
        elif [ "${job_type}" == "CENTER_XTC" ]; then
            create_xtc_centered_ "${input_tpr_file}" \
                                 "${input_xtc_file}" \
                                 "${center_residue_name}" \
                                 "${center_output_xtc_file}" \
                                 "${center_output_ndx_file}"
            

        
        ### GMX MSD
        elif [ "${job_type}" == "CALC_GMX_MSD" ]; then
            ## RUNNING MSD SCRIPT
            calc_msd_ "${input_tpr_file}" "${input_xtc_file}" "${msd_output_file}" "${msd_atom_selection}"
            
        #### GMX HBOND
        elif [ "${job_type}" == "CALC_GMX_HBOND" ]; then
            ## RUNNING FROM SCRIPTS
            cd "${PATH2POST_SCRIPTS}"
            ## RUNNING GMX HBOND SCRIPT
            bash "${calc_gmx_hbond_script}" "${input_tpr_file}" "${hbond_input_xtc_file}" "${index_file}" "${hbond_input_solvent_residue}" "${hbond_output_hbnum}" "${path_to_each_file}"
            

            
        ### GMX GYRATE
        elif [ "${job_type}" == "CALC_GMX_GYRATE" ]; then
            ## RUNNING FROM SCRIPTS
            cd "${PATH2POST_SCRIPTS}"
            ## RUNNING SCRIPTS
            bash "${calc_gmx_gyrate}" "${input_tpr_file}" "${input_xtc_file}" "${index_file}" "${gyrate_xvg}" "${path_to_each_file}"
            
        fi
        
        ## WAITING FOR JOB TO FINISH
        wait
    
    done
    
    ## CANCELING IF SPECIFIED DIRECTORY
    if [ ! -z "${specific_extraction_dir}" ]; then
        break
    fi

    ## INCREASING IN ITERATION
    ITER="$((ITER+1))"

done

###############
### SUMMARY ###
###############

echo "----------SUMMARY----------"
echo "Main extraction directory: ${Main_Extract_Dir}"
if [ ! -z "${specific_extraction_dir}" ]; then
    echo "Worked on specific extraction directory: ${specific_extraction_dir}"
    echo "Full path: ${path_main_extract_dir}/${specific_extraction_dir}"
else
    echo "Full path: ${path_main_extract_dir}"
fi
echo "Job types: ${job_type_list[@]}"
