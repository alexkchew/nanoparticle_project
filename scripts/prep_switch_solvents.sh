#!/bin/bash

# prep_switch_solvents.sh
# The purpose of this script is to switch one pure solvent to another. 
# This is useful when you want to test the effects of cosolvents in another system.

# ASSUMPTIONS:
#   - You have already run the NP in pure water
#   - You have already run post extraction protocols to get the NP with no water

# VARIABLES:
#   $1: simulation_dir_name -- simulation name to get details from
#   $2: output_simulation_dir -- output simulation name
#   $3: ligand_names -- ligand names of interest
#   $4: solvent_list -- cosolvent name of interest

# Written by Alex K. Chew (03/29/2019)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

#################
### FUNCTIONS ###
#################

### FUNCTION TO GET OUTPUTNAME
# The purpose of this function is to get the outputname when switching solvents.
# INPUTS:
#   $1: output_dirname: name of the output directory
#   $2: cosolvent: name of the cosolvent
#   $3: extract_traj_time_ps extracted time in ps
# OUTPUTS:
#   Updated output directory name
function find_outputname_switch_solvents () {
    ## DEFINING PREFIX
    prefix_="switch_solvents"
    ## DEFINING INPUTS
    output_dirname_="$1"
    extract_traj_time_ps_="$2"
    cosolvent_="$3"

    ## DEFINING NOMENCLATURE
    updated_output_dir_name_="${prefix_}-${extract_traj_time_ps_}-${cosolvent_}-${output_dirname_}"
    ## PRINTING
    echo "${updated_output_dir_name_}"
}

#######################
### INPUT VARIABLES ###
#######################

## FORCE FIELD
forcefield="${FORCEFIELD}" # Force Field (opls-aa or charmm36-nov2016.ff)

## DIAMETER
diameter="2" # "$1" # "$1" # 2 nm

## TEMPERATURE
temp="300.00" # "$2" # "310.15" temperature in K of the system

## DEFINING SHAPE
shape_type="EAM" # "$3"
    # spherical: Spherical shape cutoff from {111} fcc lattice
    # hollow: Hollow spherical shape
    # EAM: embedded atom potential
    
## DEFINING DESIRED TRIAL
trial_num="1" # "$5"

## LIGAND NAME
ligand_names="$3"
# "$1"
# "dodecanethiol"
# "ROT001" # "ROT001" # "C11COOH" ROT012
# dodecanethiol ROT012

## DEFINING DESIRED COSOLVENT
cosolvent="$4"
# "methanol"
# "methanol"

## DEFINING DESIRED TRAJECTORY TIME IN PS
extract_traj_time_ps="50000"

## DEFINING OUTPUT DIRECTORY
output_simulation_dir="$2"
# "190904-Switching_solvents_large_box"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING PYTHON FILES
py_count_gro_residues="${PY_COUNT_GRO_FILE}"

## DEFINING DISTANCE TO EDGE
dist_to_edge="1.5" # 1

## DEFINING OUTPUT PREFIX
output_prefix="sam"

## DEFINING INDEX FILE
index_file="gold_with_ligand.ndx"

## DEFINING GOLD RESIDUE NAME
gold_residue_name="AUNP"

## DEFAULT NUMBER OF PURE SOLVENTS
default_num_solvents="216"

## DEFINING SETUP DIRECTORY
setup_file="setup_file"

## DEFINING ITP FILE FOR SAM
np_itp_file="sam.itp"

## DEFINING SUMMARY FILE
gro_summary="sam_gro.summary"

## GROMPP OPTIONS
max_warn_index="5" # Number of times for maximum warnings

## DEFINING TRANSFER FILE
transfer_summary="transfer.summary"

###################
### MDP DETAILS ###
###################

### MDP FILE INFORMATION
mdp_parent_path="${INPUT_MDP_PATH}/charmm36/Spherical"

## MDP FILES
mdp_em="minim_sam_gmx5.mdp" # Energy minimization mdp script
mdp_equil="npt_double_equil_gmx5_charmm36_10ps.mdp" # Equilibration mdp script
mdp_prod="npt_double_prod_gmx5_charmm36_10ps.mdp" # Production mdp script

## DEFINING EQUIL MDP TIME
equil_mdp_time="500000" # 1 ns equilibration
equil_mdp_temp="${temp}"

## DEFINING MDP FILE TIME
mdp_file_time="25000000" # 50 ns
# "15000000"  # 30ns
# 20 ns  - 10000000
mdp_temperature="${temp}"

#######################
### SUBMISSION FILE ###
#######################

## DEFINING SUBMISSION FILE INFORMATION
submit_parent_path="${PATH2SUBMISSION}"
submit_file_name="submit_most_likely_np_mixed_solvents.sh"

## DEFINING OUTPUT SUBMIT NAME
output_submit_name="submit.sh"

## DEFINING NUMBER OF CORES
num_cores="28"

##############################
### COSOLVENT PATH DETAILS ###
##############################
## PATH TO INPUT MOLECULE
path_input_molecule="${NP_PREP_SOLVENT_EQUIL}/${cosolvent}"

## CHECKING INPUT FILE
check_setup_file "${path_input_molecule}" "${cosolvent}"

## DEFINING MOLECULE GRO, ITP, PRM FILES
molecule_gro_file="${cosolvent}.gro"
molecule_itp_file="${cosolvent}.itp"
molecule_prm_file="${cosolvent}.prm"

#########################################
### DEFINING INPUT SIMULATION DETAILS ###
#########################################

## DEFINING SIMULATION DIRECTORY
simulation_dir_name="$1"
# "190509-2nm_C11_Sims_updated_forcefield"
# "190520-2nm_ROT_Sims_updated_forcefield_new_ligands"

simulation_dir="/home/akchew/scratch/nanoparticle_project/simulations/${simulation_dir_name}"

## DEFINING INPUT GRO TPR DETAILS
sim_struc_file="sam_prod.tpr" # TPR FILE
sim_xtc_file="sam_prod.xtc" # XTC FILE
sim_top_file="sam.top"
# sim_struc_file="sam_prod_10_ns_whole_no_water_center.gro" # TPR FILE
# sim_xtc_file="sam_prod_10_ns_whole_no_water_center.xtc" # XTC FILE

##########################################
### DEFINING OUTPUT SIMULATION DETAILS ###
##########################################

## DEFINING DIRECTORY NAME
output_dirname="$(find_outputname ${shape_type} ${temp} ${diameter} ${ligand_names} ${forcefield} ${trial_num})"

### DEFINING PATH TO DIRECTORY
path_to_sim_output_directory="${simulation_dir}/${output_dirname}"

## GETTING NEW DIRECTORY NAME
new_dirname=$(find_outputname_switch_solvents "${output_dirname}" "${extract_traj_time_ps}" "${cosolvent}" )

## DEFINING OUTPUT DIRECTORY
path_to_new_sim_output_directory="${PATH2SIM}/${output_simulation_dir}/${new_dirname}"

#################
### MAIN CODE ###
#################

## FINDING LIGAND RESIDUE NAME
lig_residue_name="$(find_residue_name_from_ligand_txt ${ligand_names})"

## CHECKING IF PATH EXISTS
stop_if_does_not_exist "${path_to_sim_output_directory}"

## CREATING DIRECTORY
create_dir "${path_to_new_sim_output_directory}" -f

## GOING INTO PATH
cd "${path_to_new_sim_output_directory}"

#######################################
### USING TRJCONV TO EXTRACT FRAMES ###
#######################################

## DEFINING INTIAL GRO FILE
initial_gro_file="${output_prefix}_initial.gro"

## CREATING INDEX FILE
make_ndx_gold_with_ligand "${path_to_sim_output_directory}/${sim_struc_file}" \
                          "${path_to_sim_output_directory}/${index_file}"   \
                          "${lig_residue_name}"   \
                          "${gold_residue_name}"  

## DEFINING COMBINED NAME
combined_name="${gold_residue_name}_${lig_residue_name}"

## CREATING FILE
echo "Creating ${initial_gro_file} from ${path_to_sim_output_directory}..."
## RUNNING TRJCONV
gmx trjconv -f "${path_to_sim_output_directory}/${sim_xtc_file}" -s "${path_to_sim_output_directory}/${sim_struc_file}" -o "${path_to_new_sim_output_directory}/${initial_gro_file}" -n "${path_to_sim_output_directory}/${index_file}" -dump "${extract_traj_time_ps}" -pbc mol -center >/dev/null 2>&1 << INPUTS
${gold_residue_name}
${combined_name}
INPUTS

## EDITING SAM FILE TO ALIGN AND MAKE DISTANCE OF NP (WITH CUBIC BOX)
echo "Aligning ${initial_gro_file} to ${initial_gro_file%.gro}_align.gro with distance to edge: ${dist_to_edge}"
gmx editconf -f "${initial_gro_file}" -o "${initial_gro_file%.gro}_align.gro" -d "${dist_to_edge}" -princ -bt cubic >/dev/null 2>&1 << INPUTS
System
INPUTS

############################################
### COPYING OVER FORCE FIELD FILES, ETC. ###
############################################
echo "--- COPYING OVER FORCE FIELD FILES, ETC. ---"
# COSOLVENT ITP AND PRM FILE
cp -r "${path_input_molecule}/"{${molecule_itp_file},${molecule_prm_file}} "${path_to_new_sim_output_directory}"
# FORCE FIELD
cp -r "${path_to_sim_output_directory}/${forcefield}" "${path_to_new_sim_output_directory}"
## ITP AND PRM
cp "${path_to_sim_output_directory}/"{*.itp,*.prm} "${path_to_new_sim_output_directory}"
# TOPOLOGY
cp "${path_to_sim_output_directory}/${sim_top_file}" "${path_to_new_sim_output_directory}/${output_prefix}.top"
# MDP FILE
cp -r "${mdp_parent_path}/"{${mdp_em},${mdp_equil},${mdp_prod}} "${path_to_new_sim_output_directory}"
# SUBMISSION FILE
cp -r "${submit_parent_path}/${submit_file_name}" "${path_to_new_sim_output_directory}/${output_submit_name}"

##############################
### UPDATING TOPOLOGY FILE ###
##############################
## CLEARING TOPOLOGY
top_clear_molecules ${output_prefix}.top
## ADDING NP INTO TOPOLOGY
np_residue_name=$(itp_get_resname ${np_itp_file})
echo "   ${np_residue_name}   1" >> "${path_to_new_sim_output_directory}/${output_prefix}.top" 
## GETTING COSOLVENT RESIDUE NAME
cosolvent_residue_name="$(itp_get_resname ${cosolvent}.itp)"

#################
### SOLVATING ###
#################
echo -e "--- SOLVATING ---"

## SOLVATING
gmx solvate -cs "${path_input_molecule}/${molecule_gro_file}" -cp "${initial_gro_file%.gro}_align.gro" -p ${output_prefix}.top -o "${output_prefix}_solvated.gro" &> /dev/null # Silencing

## CHECKING FILE IF EXISTING
stop_if_does_not_exist "${output_prefix}_solvated.gro"

## STORING SETUP DETAILS
mkdir "${setup_file}"
mv "${initial_gro_file%.gro}_align.gro" "${setup_file}"
mv "${initial_gro_file}" "${setup_file}"

################################
### CORRECTING TOPOLOGY FILE ###
################################

echo "--- CORRECTING TOPOLOGY FILE ---"
## RUNNING PYTHON SCRIPT TO COUNT GRO FILE
/usr/bin/python3.4 "${py_count_gro_residues}" \
        --igro "${path_to_new_sim_output_directory}/${output_prefix}_solvated.gro" \
        --sum "${path_to_new_sim_output_directory}/${gro_summary}"

## FINDING NUMBER OF SOLVENTS
num_solvent=$(get_residue_num_from_sum_file "${cosolvent_residue_name}" "${path_to_new_sim_output_directory}/${gro_summary}")
echo "Updating solvents: ${cosolvent_residue_name} ${num_solvent}"
echo "   ${cosolvent_residue_name}   ${num_solvent}" >> "${path_to_new_sim_output_directory}/${output_prefix}.top" 

## COSOLVENT TOPOLOGY PRM
topology_add_include_specific "${path_to_new_sim_output_directory}/${output_prefix}.top" "${ligand_names}.prm" "#include \"${cosolvent}.prm\""

## COSOLVENT TOPOLOGY ITP
topology_add_include_specific "${path_to_new_sim_output_directory}/${output_prefix}.top" "${np_itp_file}" "#include \"${cosolvent}.itp\""

#################################
### NEUTRALIZING IF NECESSARY ###
#################################
## DEFINING INDEX FILE
cosolvent_index_file="cosolvent.ndx"

echo -e "--- NEUTRALIZING CHARGES ---"
echo "Creating TPR file for balancing of charge"
## CREATING TPR FILE
gmx grompp -f "${mdp_em}" -c "${output_prefix}_solvated.gro" -p "${output_prefix}.top" -o "${output_prefix}_solvated.tpr" -maxwarn "${max_warn_index}"
## CREATING INDEX FOR SOLVENT
index_make_ndx_residue_ "${output_prefix}_solvated.tpr" "${cosolvent_index_file}" "${cosolvent_residue_name}"
## USING GENION
gmx genion -s "${output_prefix}_solvated.tpr" -p "${output_prefix}.top" -o "${output_prefix}.gro" -n "${cosolvent_index_file}" -neutral &> /dev/null << INPUT
${cosolvent_residue_name}
INPUT

########################################
### EDITING MDP AND SUBMISSION FILES ###
########################################
echo -e "--- EDITING MDP AND SUBMISSION FILES ---"
## EQUIL MDP FILES
sed -i "s/NSTEPS/${equil_mdp_time}/g" ${mdp_equil}
sed -i "s/TEMPERATURE/${equil_mdp_temp}/g" ${mdp_equil}

## PROD MDP FILES
sed -i "s/NSTEPS/${mdp_file_time}/g" ${mdp_prod}
sed -i "s/TEMPERATURE/${temp}/g" ${mdp_prod}

## SUBMISSION FILE
sed -i "s/_JOB_NAME_/${new_dirname}/g" ${output_submit_name}
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" ${output_submit_name}
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" ${output_submit_name}
sed -i "s/_EM_GRO_FILE_/${output_prefix}_em.gro/g" ${output_submit_name}
sed -i "s/_EQUILMDPFILE_/${mdp_equil}/g" ${output_submit_name}
sed -i "s/_PRODMDPFILE_/${mdp_prod}/g" ${output_submit_name}
sed -i "s/_TOPFILE_/${output_prefix}.top/g" ${output_submit_name}

##########################################
#### CREATING ENERGY MINIMIZATION FILE ###
##########################################
echo "*** Creating tpr file ***"
gmx grompp -f ${mdp_em} -c "${output_prefix}.gro" -p "${output_prefix}.top" -o "${output_prefix}_em" -maxwarn 1
## ENERGY MINIMIZING
gmx mdrun -nt 10 -v -deffnm "${output_prefix}_em" 

## CREATING SUMMARY FILE
echo "Cosolvent name: ${cosolvent}" >> "${transfer_summary}"
echo "Cosolvent residue name: ${cosolvent_residue_name}" >> "${transfer_summary}"
echo "Distance to edge (nm): ${dist_to_edge}" >> "${transfer_summary}"
echo "Simulation extracted from: ${simulation_dir_name}" >> "${transfer_summary}"
echo "Time step extraction: ${extract_traj_time_ps}" >> "${transfer_summary}"

## CLEANING UP FILES
rm \#*

## ADDING TO JOB LIST
echo "${path_to_new_sim_output_directory}/${output_submit_name}" >> "${JOB_SCRIPT}"
