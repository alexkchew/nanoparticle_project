#!/bin/bash

# generate_surface_grid.sh
# The purpose of this function is to generate a surface grid for a GNP. 
# The idea is that we have GNPs in water that you are interested in computing a grid for. 
# This will build on this simulation by running an NVT simulation with the most likely configuration of the water. 
# 
# Written by: Alex K. Chew (10/16/2019)
#
# INPUTS:
#   $1: simulation_dir - simulation directory path
#   $2: most_likely_index - most likely index
#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## ENABLING EXTERNAL MODULES
shopt -s extglob
# ^ Allows you to move all except 1

## PRINTING SCRIPT NAME
print_script_name

##############
### INPUTS ###
##############

# "HYDROPHOBICITY_PROJECT_C11"
simulation_dir="$1"
# "${PATH2SIM}/${simulation_dir_name}"

## DEFINING DESIRED NP INDEX
most_likely_index="$2"
# "1" # First most likely index

## DEFINING LIGAND NAMES
# ligand_names="$2"

## DEFINING RECALC
want_recalc=false
# true

## GETTING BASE NAME
simulation_dir_name=$(basename ${simulation_dir})

## EXTRACTING OUTPUT NAMES
read -a output_dir_names <<< $(extract_output_name_gnp_water "${simulation_dir_name}")

## FINDING LIGAND NAMES
ligand_names=${output_dir_names[2]}

## FINDING TEMPERATURE
temp=${output_dir_names[0]}

## FORCE FIELD
forcefield="${FORCEFIELD}" # Force Field (opls-aa or charmm36-nov2016.ff)


#############
### FILES ###
#############

## DEFINING SCRIPTS 
most_likely_script="${BASH_MOST_LIKELY_SCRIPT}"

## DEFINING NEW DIRECTORY NAME
new_dir_name="NVT_grid_${most_likely_index}"

## TOPOLOGY FILE
sim_top_file="sam.top"

## DEFINING NEW GRO FILE
new_gro_file="sam_initial.gro"

############# MDP FILES #############
## ENERGY MINIMIIZATION
em_mdp_file="minim_sam_gmx5.mdp"

## DEFINING MDP FILE TIME
mdp_file_time="25000000" # 50 ns

## MDP FILE INFORMATION
mdp_parent_path="${INPUT_MDP_PATH}/charmm36/Spherical"
## DEFINING MDP FILE
mdp_file="nvt_double_prod_gmx5_charmm36_frozen_gold.mdp"

## DEFINING MDP FILE TIME
mdp_file_time="500000" # 1 ns

############# SUBMISSION FILE #############
## DEFINING SUBMISSION FILE INFORMATION
submit_parent_path="${PATH2SUBMISSION}"
submit_file_name="submit_most_likely_np_with_grid.sh"

## DEFINING OUTPUT SUBMIT NAME
output_submit_name="submit.sh"

## DEFINING OUTPUT PREFIX
output_prefix="sam"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING GOLD RESIDUE NAME
gold_residue_name="AUNP"

## OUTPUT FROM GRO FILE
output_most_likely_gro="Idx_${most_likely_index}_System.gro"

## DIRECTORY ON MOST LIKELY SIMS
most_likely_dir="${MOST_LIKELY_DIR}"

## DEFINING NUMBER OF CORES
num_cores="28"

##########################
### GRIDDING VARIABLES ###
##########################
## MESH
mesh="0.1"
## BASHCODE
grid_bash_script="${GRID_BASH_SCRIPT}"
## ANALYSIS DIRECTORY
analysis_dir="hyd_analysis"
## GRID PROCS
grid_procs="20"

## DEFINING GENERATE GRID
run_grid_path="${PATH2SCRIPTS}/run_generate_grid.sh"
run_grid_script="$(basename ${run_grid_path})"

## TODO: copy over the grid
## TODO: use sed to edit the files

#############
### PATHS ###
#############

## DEFINING PATH TO DIRECTORY
path_to_sim_output_directory="${simulation_dir}"
# "${simulation_dir}/${simulation_dir_name}"

## DEFINING PATH TO NEW DIRECTORY
path_to_new_sim_output_directory="${path_to_sim_output_directory}/${new_dir_name}"

############
### MAIN ###
############

## SEEING IF IT DOES NOT EXIST
if [[ ! -e "${path_to_new_sim_output_directory}" ]] || [[ "${want_recalc}" == true ]]; then 

    ## CREATING DIRECTORY
    mkdir -p "${path_to_new_sim_output_directory}"

    ## CHECKING IF FILE EXISTS
    stop_if_does_not_exist "${path_to_sim_output_directory}"

    ## FINDING RESIDUE NAME
    residue_name="$(find_residue_name_from_ligand_txt ${ligand_names})"

    #########################################
    ### RUNNING MOST LIKELY CONFIGURATION ###
    #########################################
    bash "${most_likely_script}" "${path_to_sim_output_directory}" "${residue_name}" "${most_likely_index}"

    ## DEFINING MOST LIKELY GRO
    path_most_likely_gro="${path_to_sim_output_directory}/${most_likely_dir}/${output_most_likely_gro}"

    ## CHECKING IF EXISTS
    stop_if_does_not_exist "${path_most_likely_gro}"

    ## COPYING THE GRO FILE CORRESPONDING TO THE MOST LIKELY CONFIGURATION
    cp "${path_most_likely_gro}" "${path_to_new_sim_output_directory}/${new_gro_file}"

    ## GOING INTO PATH
    cd "${path_to_new_sim_output_directory}"

    ######################################
    ### COPYING EVERYTHING TO NEW PATH ###
    ######################################
    # TOPOLOGY
    cp "${path_to_sim_output_directory}/${sim_top_file}" "${path_to_new_sim_output_directory}"
    # ITP/PRM FILES
    cp "${path_to_sim_output_directory}/"{*.itp,*.prm} "${path_to_new_sim_output_directory}"
    # FORCE FIELD
    cp -r "${path_to_sim_output_directory}/${forcefield}" "${path_to_new_sim_output_directory}"
    # MDP FILE
    cp -r "${mdp_parent_path}/${mdp_file}" "${path_to_new_sim_output_directory}"

    ###########################
    ### CREATING INDEX FILE ###
    ###########################

    ## DEFINING INDEX FILE NAME
    output_index_file="gold_np.ndx"

## CREATING INDEX FILE (LIGAND ONLY)
gmx make_ndx -f "${new_gro_file}" -o "${output_index_file}" << INPUTS
keep 0
r AUNP
r ${residue_name}
q
INPUTS

    ###################################################
    ### CREATING POSITION RESTRAINTS FOR THE SOLUTE ###
    ###################################################

    ## CHANGING MDP FILE
    sed -i "s/_LIGANDRESNAME_/${residue_name}/g" ${mdp_file}

    ########################################
    ### EDITING MDP AND SUBMISSION FILES ###
    ########################################
    ## MDP FILES
    sed -i "s/NSTEPS/${mdp_file_time}/g" ${mdp_file}
    sed -i "s/TEMPERATURE/${temp}/g" ${mdp_file}

    ###########################################
    ### CREATING TPR GIVEN TOPOLOGY AND GRO ###
    ###########################################
    echo "*** Creating tpr file ***"
    gmx grompp -f ${mdp_file} -c ${new_gro_file} -p ${sim_top_file} -o "${output_prefix}_prod" -n "${output_index_file}" -maxwarn 1

fi

##########################
### ADDING BASH SCRIPT ###
##########################

## GOING TO DIRECTORY
cd "${path_to_new_sim_output_directory}"

## COPYING BASH SCRIPT
cp -rv "${run_grid_path}" "${path_to_new_sim_output_directory}/${run_grid_script}"

## EDITING SCRIPT
sed -i "s/_OUTPUTPREFIX_/${output_prefix}_prod/g" ${run_grid_script}
sed -i "s#_GRIDBASH_#${grid_bash_script}#g" ${run_grid_script}
sed -i "s#_MESH_#${mesh}#g" ${run_grid_script}
sed -i "s#_GRIDPROC_#${grid_procs}#g" ${run_grid_script}
sed -i "s#_ANALYSISDIR_#${analysis_dir}#g" ${run_grid_script}
sed -i "s#_PATHSIMS_#${path_to_new_sim_output_directory}#g" ${run_grid_script}

# SUBMISSION FILE
cp -r "${submit_parent_path}/${submit_file_name}" "${path_to_new_sim_output_directory}/${output_submit_name}"

## SUBMISSION FILE
job_name="${new_dir_name}-${simulation_dir_name}"
sed -i "s/_JOB_NAME_/${job_name}/g" ${output_submit_name}
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" ${output_submit_name}
sed -i "s/_BASHSCRIPT_/${run_grid_script}/g" ${output_submit_name}

## ADDING JOB TO JOB LIST
if [[ ! -e "${output_prefix}_prod.gro" ]] || [[ ! -e "${analysis_dir}" ]]; then
    echo "${path_to_new_sim_output_directory}" >> "${JOB_SCRIPT}"

    ## PRINTING SUMMARY
    echo "Creating most likely np configurations for: ${ligand_names}"
    
fi







