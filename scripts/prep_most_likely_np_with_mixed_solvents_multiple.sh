#!/bin/bash

# prep_most_likely_np_with_mixed_solvents_multiple.sh
# This script will find the most likely configuration of a nanoparticle based on 
# bundling groups, then output the nanoparticle centered. Finally, we will run 
# this nanoparticle with all the atoms frozen.

# This code uses multiple cosolvents as a way of finding most likely locations

# Written by Alex K. Chew (10/05/2019)

### VARIABLES:
# $1: input simulation directory
# $2: output simulation directory
# $3: ligand name
# $4: cosolvent name
# $5: cosolvent mass fraction
# $6: cosolvent frame
# $7: initial box length size if ligand_name == NONE

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## ENABLING EXTERNAL MODULES
shopt -s extglob
# ^ Allows you to move all except 1

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

### INPUT VARIABLES
## FORCE FIELD
forcefield="${FORCEFIELD}" # Force Field (opls-aa or charmm36-nov2016.ff)

## DIAMETER
diameter="2" # "$1" # "$1" # 2 nm

## TEMPERATURE
temp="300.00" # "$2" # "310.15" temperature in K of the system

## DEFINING SHAPE
shape_type="EAM" # "$3"
    # spherical: Spherical shape cutoff from {111} fcc lattice
    # hollow: Hollow spherical shape
    # EAM: embedded atom potential
    
## DEFINING DESIRED TRIAL
trial_num="1" # "$5"

## DEFINING DESIRED NP INDEX
most_likely_index="1" # First most likely index

## DEFINING SIMULATION DIRECTORY
simulation_dir_name="$1"

## DEFINING NEW SIMULATION DIRECTORY NAME
output_simulation_dir="$2"
# "191005-Testing_multiple_cosolvents"

## LIGAND NAME
ligand_names="$3"
# "C11COOH"
# "$3"

## DEFINING COSOLVENT
cosolvent_names="$4"
# "methanol,formicacid,phenol"

## DEFINING FRACTION
cosolvent_perc="$5" # molfractions

## DEFINING DESIRED FRAME
desired_cosolvent_frame_ps="$6"
# "9000"
# "9500"
# "10000"

## ADDING INITIAL BOX LENGTH
if [[ ${ligand_names} == "NONE" ]]; then
    initial_box_length="$7"
    # "7" #"$7"
    echo "Since ligand name is NONE, we use initial box length (nm): ${initial_box_length}"
fi

## DEFINING FRACTION OF LIGANDS
ligand_fracs="1" # fraction of ligands

#######################################
### LOGICALS AND MINOR CALCULATIONS ###
#######################################

## DEFINING LOGICAL
# True if you want a hard restart in the computation of most likely configs
# Otherwise, False
want_recalc="False"

## DEFINING LOGICAL IF YOU WANT TEMPERATURE ANNEALING
want_temp_annealing=true

######################
### DEFINING PATHS ###
######################

## DEFINING SIMULATION PATH
simulation_dir="/home/akchew/scratch/nanoparticle_project/simulations/${simulation_dir_name}"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING PURE WATER NAME
water_residue_name="SOL"
## DEFINING WATER MODEL
water_model="tip3p"
## DEFINING ITP FILE FOR NP
np_itp_file="sam.itp"

#############################
### MIXED SOLVENT SYSTEMS ###
#############################

## DEFINING TEMPERATURE OF MIXED-SOLVENT
mixed_solv_temp='300'

## CONVERTING THE COSOLVENT NAME TO AN ARRAY
IFS=',' read -r -a cosolvent_name_array <<< "${cosolvent_names}"; unset IFS

## SORTING COSOLVENT NAME ARRAY
IFS=$'\n' cosolvent_name_array=($(sort <<<"${cosolvent_name_array[*]}")); unset IFS

## CONVERTING COSOLVENT NAME ARRAY TO STRING
cosolvent_name_string=$(printf "%s_"  "${cosolvent_name_array[@]}")
cosolvent_name_string=${cosolvent_name_string%?} ## REMOVING EXTRA STR

## CONVERTING TO STRING WITH COMMAS
cosolvent_name_string_comma=$(printf "%s,"  "${cosolvent_name_array[@]}")
cosolvent_name_string_comma=${cosolvent_name_string_comma%?} ## REMOVING EXTRA STR

## DEFINING TYPE OF PERCENTAGE
perc_type="molfrac"

## DEFINING GRO FILE
cosolvent_tpr_file="mixed_solvent_equil.tpr"
cosolvent_xtc_file="mixed_solvent_equil.xtc"
cosolvent_top_file="mixed_solvent.top"

## DEFINING OUTPUT PREFIX
cosolvent_output_prefix="mixed_solvent_equil"

## DEFINING POSITIONS DATA FILE
positions_dat="positions.dat"

######################
### DEFINING FILES ###
######################

## SCALING THE VDW TO ADD GNP CORRECTLY
VDW_scale="0.75"
# "0.60" -- didn't work correctly
# "0.57" -- default

## DEFINING PYTHON FILES
py_count_gro_residues="${PY_COUNT_GRO_FILE}"
# Code to count gro file residues

## DEFINING SIMULATION GRO FILE
sim_tpr_file="sam_prod.tpr" # TPR FILE
sim_gro_file="sam_prod.gro" # SIMULATION GRO FILE
analyze_sim_xtc_file="sam_prod_10_ns_whole_rot_trans.xtc" # ANALYZE XTC FILE
extract_sim_xtc_file="sam_prod_10_ns_whole_center.xtc" # EXTRACT SIMULATION XTC FILE
sim_top_file="sam.top"

## DEFINING NEW GRO FILE
new_gro_file="sam_initial.gro"

### MDP FILE INFORMATION
mdp_parent_path="${INPUT_MDP_PATH}/charmm36/Spherical"

## ENERGY MINIMIZATION
em_mdp_file="minim_sam_gmx5.mdp" # "minim_sam_gmx5.mdp"
equil_mdp_file="nvt_mixed_solvent_multiple_annealing.mdp"
prod_mdp_file="npt_double_prod_gmx5_charmm36_10ps.mdp"
# "npt_double_prod_gmx5_charmm36_frozen_gold.mdp"

## DEFINING EQUIL MDP TIME
equil_mdp_time="2000000" # 4 ns equilibration
# "1500000" # 3 ns equilibration
equil_mdp_temp="${temp}"

## DEFINING MDP FILE TIME
mdp_file_time="5000000"  # 10 ns
# 30 ns  - 15000000
# 20 ns  - 10000000
mdp_temperature="${temp}"

## DEFINING SETUP FILE
setup_file="setup_files"

## DEFINING SUMMARY FILE
gro_summary="sam_gro.summary"

## DEFINING TRANSFER FILE
transfer_summary="transfer.summary"

#######################
### SUBMISSION FILE ###
#######################

## DEFINING SUBMISSION FILE INFORMATION
submit_parent_path="${PATH2SUBMISSION}"
submit_file_name="submit_most_likely_np_mixed_solvents.sh"

## DEFINING OUTPUT SUBMIT NAME
output_submit_name="submit.sh"

## POSITION RESTRAINT FILE
np_posre="np_posre.itp"
position_restraint_value="5000" # 100000 10000

## DEFINING OUTPUT PREFIX
output_prefix="sam"

## DEFINING NUMBER OF CORES
num_cores="28"

#########################################
### MOST LIKELY CONFIGURATION DETAILS ###
#########################################

## DEFINING BASH CODE
# bash_generate_grid="${main_dir}/generate_surface_grid.sh"

## DEFINING SCRIPTS 
most_likely_script="${BASH_MOST_LIKELY_SCRIPT}"

## DEFINING PYTHON CODE
# python_most_likely="${NP_MOST_LIKELY_CODE}"

## DEFINING SUMMARY FILE
# most_likely_output_summary_file="np_most_likely.summary"

## DEFINING GOLD RESIDUE NAME
gold_residue_name="AUNP"

###############################
### FILE SYSTEM INFORMATION ###
###############################

## DEFINING DIRECTORY NAME
output_dirname="$(find_outputname ${shape_type} ${temp} ${diameter} ${ligand_names} ${forcefield} ${trial_num})"

## GETTING NEW DIRECTORY NAME
new_dirname="MostNP-${output_dirname}-lidx_${most_likely_index}-cosf_${desired_cosolvent_frame_ps}-${cosolvent_name_string}_${cosolvent_perc}_${perc_type}_${mixed_solv_temp}"
# most likely nanoparticle with ligand index, cosolvent frame, etc.
## CHECKING IF LIGAND NAME IS NONE
if [[ "${ligand_names}" == "NONE" ]]; then
   # desired_cosolvent_frame_ps="10000" # "9500" # "10000"  # default
   new_dirname="NoNP-${initial_box_length}_nm-cosf_${desired_cosolvent_frame_ps}-${cosolvent_name_string}_${cosolvent_perc}_${perc_type}_${mixed_solv_temp}"
fi

### DEFINING PATH TO DIRECTORY
path_to_sim_output_directory="${simulation_dir}/${output_dirname}"

## DEFINING OUTPUT DIRECTORY
path_to_new_sim_output_directory="${PATH2SIM}/${output_simulation_dir}/${new_dirname}"

#################
### MAIN CODE ###
#################

## FINDING LIGAND RESIDUE NAME
lig_residue_name="$(find_residue_name_from_ligand_txt ${ligand_names})"

## CHECKING IF PATH EXISTS
if [[ "${ligand_names}" != "NONE" ]]; then
    echo "${ligand_names}"
    stop_if_does_not_exist "${path_to_sim_output_directory}"
fi

## CREATING DIRECTORY
create_dir "${path_to_new_sim_output_directory}" -f

## SEEING IF THE OUTPUT SUMMARY FILE EXISTS
path_output_summary="${path_to_sim_output_directory}/${most_likely_output_summary_file}"

## GOING INTO PATH
cd "${path_to_new_sim_output_directory}"

####################################
### COMPUTING MOST LIKELY CONFIG ###
####################################
## SEEING IF YOU HAVE A LIGAND
if [[ "${ligand_names}" != "NONE" ]]; then
    ## RUNNING MOST LIKELY SCRIPT
    bash "${most_likely_script}" "${path_to_sim_output_directory}" "${lig_residue_name}" "${most_likely_index}"
        
    ## OUTPUT FROM GRO FILE
    most_likely_dir="${MOST_LIKELY_DIR}"
    output_most_likely_gro="Idx_${most_likely_index}_NP_only.gro"
    
    ## DEFINING GRO FILE
    path_most_likely_gro="${path_to_sim_output_directory}/${most_likely_dir}/${output_most_likely_gro}"
 
     ## CHECKING IF EXISTS
    stop_if_does_not_exist "${path_most_likely_gro}"
    
    ## COPYING THE GRO FILE CORRESPONDING TO THE MOST LIKELY CONFIGURATION
    cp "${path_most_likely_gro}" "${path_to_new_sim_output_directory}/${new_gro_file}"
    
#    ## SEEING IF SUMMARY EXISTS
#    if [ ! -e "${path_output_summary}" ] || [ "${want_recalc}" == "True" ]; then
#        ## RUNNING PYTHON CODE
#        /usr/bin/python3.4 "${python_most_likely}" -i "${path_to_sim_output_directory}" -o "${path_to_sim_output_directory}" -s "${most_likely_output_summary_file}" -g "${sim_gro_file}" -x "${analyze_sim_xtc_file}" -p
#    else
#        echo "${most_likely_output_summary_file} already exists! Continuing script..."
#    fi
#    ######################################################
#    ### LOCATING MOST LIKELY CONFIGURATION AND DUMPING ###
#    ######################################################
#    ## DEFINING LINES TO LOOK UP TO
#    lines_to_look_up="$((${most_likely_index}+1))"
#
#    ## FINDING NEAREST TRAJECTORY TIME
#    most_likely_time=$(head -n "${lines_to_look_up}" "${path_output_summary}" | tail -n1 | awk '{print $4}')
#    echo "The most likely time found: ${most_likely_time} ps"; sleep 1
#
#    ## DEFINING INDEX FILE
#    index_file="gold_with_ligand.ndx"
#
#    ## CREATING INDEX FILE
#    make_ndx_gold_with_ligand "${path_to_sim_output_directory}/${sim_tpr_file}" \
#                              "${path_to_sim_output_directory}/${index_file}"   \
#                              "${lig_residue_name}"   \
#                              "${gold_residue_name}"  
#

#
#    ## DUMPING NEAREST TRAJECTORY
#echo "Creating ${new_gro_file} from ${path_to_sim_output_directory}..."
#gmx trjconv -f "${path_to_sim_output_directory}/${extract_sim_xtc_file}" -s "${path_to_sim_output_directory}/${sim_tpr_file}" -o "${path_to_new_sim_output_directory}/${new_gro_file}" -n "${path_to_sim_output_directory}/${index_file}" -dump "${most_likely_time}" -pbc mol -center >/dev/null 2>&1 << INPUTS
#${gold_residue_name}
#${combined_name}
#INPUTS
#
#
#    ## EDITING SAM FILE TO ALIGN AND MAKE DISTANCE OF NP (WITH CUBIC BOX)
#gmx editconf -f "${new_gro_file}" -o "${new_gro_file%.gro}_align.gro" -d "${dist_to_edge}" -princ -bt cubic << INPUTS
#System
#INPUTS

    ## GETTING GRO SIZE
    read -a gro_box_lengths<<<$(read_gro_box_lengths "${new_gro_file}")

    ## FINDING NEAREST WHOLE NUMBER
    initial_box_length=$(round_nearest_integer "${gro_box_lengths[0]}") 

fi

## REFRESH SOLVENT SYSTEMS
bash "${BASH_REFRESH_SOLVENTS}"

## DEFINING OUTPUT TEXT
output_solvent_info="${path_to_new_sim_output_directory}/multiple_solvent_opt.txt"

## GETTING SOLVENTS 
get_multiple_solvents_best_dir "${cosolvent_name_string_comma}" "${cosolvent_perc}" "${initial_box_length}" "${output_solvent_info}"

## GETTING MIXED SOLVENT NAME
mixed_solvent_name=$(grep "BEST_SOLVENT:" "${output_solvent_info}" | awk '{print $2}')
mixed_solvent_size=$(grep "BEST_DISTANCE:" "${output_solvent_info}" | awk '{print $2}')

## CHECKING IF EXISTS
if [ -z "${mixed_solvent_name}" ]; then
    echo "Error! mixed_solvent_name does not exist!"
    echo "This infers that you may have issues with your solvent"
    echo "See the event log:"
    cat "${output_solvent_info}"
    
    echo "--------"
    echo "Stopping here in case this may cause errors"
    sleep 5
    exit 1
else
    echo "Using ${mixed_solvent_name}"
    echo "Desired box size: ${initial_box_length}"
    echo "Solvent box difference (nm): ${mixed_solvent_size}"
    if (( $(echo "${mixed_solvent_size} > 1" | bc -l ) )); then
        echo "***WARNING***: Box size difference is greater than 1 between solvent and nanoparticle. This may slow down your system since your solvent system may be too large. You may want to run different sizes of mixed-solvent environments!"  
    fi
    sleep 5  
fi

####################################
### FINDING MIXED-SOLVENT SYSTEM ###
####################################

### GETTING OUTPUT DIR NAME
#mixed_solvent_name=$(get_output_name_for_mixed_solvents_multiple ${initial_box_length} ${mixed_solv_temp} ${cosolvent_perc} ${cosolvent_name_string})

## DEFINING PATH TO MIXED SOLVENT SYSTEMS
path_mixed_solvents="${PREP_MIXED_SOLVENTS_MULTIPLE}/${mixed_solvent_name}"

## CHECKING IF PATH EXISTS
if [[ ! -e "${path_mixed_solvents}" ]]; then
    echo "Error! ${path_mixed_solvents} does not exist!"
    echo "Sleeping so you can see the error!"
    echo "Perhaps you forgot to run the prep_mixed_solvent.sh script?"
    sleep 5
    exit 1
fi

## DEFINING OUTPUT GRO FILE
cosolvent_output_gro_file="${cosolvent_output_prefix}.gro"

## DUMPING NEAREST TRAJECTORY
echo "Creating ${cosolvent_output_gro_file} from ${path_mixed_solvents}..."
gmx trjconv -f "${path_mixed_solvents}/${cosolvent_xtc_file}" -s "${path_mixed_solvents}/${cosolvent_tpr_file}" -o "${path_to_new_sim_output_directory}/${cosolvent_output_gro_file}" -dump "${desired_cosolvent_frame_ps}" -pbc mol >/dev/null 2>&1 << INPUTS
System
INPUTS

## GETTING GRO SIZE
read -a cosolvent_gro_box_lengths<<<$(read_gro_box_lengths "${path_to_new_sim_output_directory}/${cosolvent_output_gro_file}")

#########################################
### ADDING NP TO MIXED-SOLVENT SYSTEM ###
#########################################

## DEFINING COMBINED GRO
combine_prefix="combined"

## SEEING IF YOU HAVE A LIGAND
if [[ "${ligand_names}" != "NONE" ]]; then

    ## DEFINING RESIZE GRO FILE
    resize_gro_file="${new_gro_file%.gro}_resize.gro"

    ## RESIZING NP SYSTEM
gmx editconf -f "${new_gro_file}" -o "${resize_gro_file}" -box ${cosolvent_gro_box_lengths[0]} ${cosolvent_gro_box_lengths[1]} ${cosolvent_gro_box_lengths[2]} -center 0 0 0 << INPUTS
System    
INPUTS
## FINDING HALF BOX LENGTH
half_box_length="$(awk -v box_length_=${cosolvent_gro_box_lengths[0]} 'BEGIN{ printf "%.5f",box_length_/2}')" # nms

    ## CREATING A POSITIONS FILE
    echo "${half_box_length} ${half_box_length} ${half_box_length}" >> ${positions_dat}
    
    ## COPYING VDW RADII
    cp -rv "${VDW_RADII_LIST}" "${path_to_new_sim_output_directory}"

    ## GMX INSERT MOLECULES REPLACE ANY SOLVENTS (no rotation)
gmx insert-molecules -f ${cosolvent_output_gro_file} -ci "${resize_gro_file}" -ip "${positions_dat}" -o "${combine_prefix}.gro" -replace -rot none -scale "${VDW_scale}" << INPUTS
System
INPUTS

else
    ## JUST COPYING
    cp "${cosolvent_output_gro_file}" "${combine_prefix}.gro"

fi
###########################
### MOVING PREP SYSTEMS ###
###########################

## CHECKING IF EXISTING
if [ ! -e "${combine_prefix}.gro" ]; then
    echo "Error! "${combine_prefix}.gro" does not exist!"
    echo "Something went wrong in prep_most_likely_np_with_mixed_solvents.sh script!"
    echo "Sleeping here and exiting"
    sleep 5
    exit 1
fi

## CREATING DIRECTORY
mkdir -p "${path_to_new_sim_output_directory}/${setup_file}"

## MOVING ALL SETUP FILES
mv ${path_to_new_sim_output_directory}/!(${setup_file}) "${path_to_new_sim_output_directory}/${setup_file}"

## MOVING GRO FILE
mv "${path_to_new_sim_output_directory}/${setup_file}/${combine_prefix}.gro"  "${path_to_new_sim_output_directory}/${output_prefix}.gro"
## MOVING VDW RADII FILE
if [[ "${ligand_names}" != "NONE" ]]; then
    mv "${path_to_new_sim_output_directory}/${setup_file}/$(basename ${VDW_RADII_LIST})" "${path_to_new_sim_output_directory}"
fi
######################################
### COPYING EVERYTHING TO NEW PATH ###
######################################
## COSOLVENT ITP AND PRM
cp "${path_mixed_solvents}/"{*.itp,*.prm} "${path_to_new_sim_output_directory}"
# TOPOLOGY
cp "${path_mixed_solvents}/${cosolvent_top_file}" "${path_to_new_sim_output_directory}/${output_prefix}.top"
## SEEING IF THERE IS A NP
if [[ "${ligand_names}" != "NONE" ]]; then
    # ITP/PRM FILES
    cp "${path_to_sim_output_directory}/"{*.itp,*.prm} "${path_to_new_sim_output_directory}"
    # FORCE FIELD
    cp -r "${path_to_sim_output_directory}/${forcefield}" "${path_to_new_sim_output_directory}"
else
    # FORCE FIELD
    cp -r "${path_mixed_solvents}/${forcefield}" "${path_to_new_sim_output_directory}"
fi

# MDP FILE
cp -r "${mdp_parent_path}/"{${em_mdp_file},${equil_mdp_file},${prod_mdp_file}} "${path_to_new_sim_output_directory}"
# SUBMISSION FILE
cp -r "${submit_parent_path}/${submit_file_name}" "${path_to_new_sim_output_directory}/${output_submit_name}"

## SEEING IF YOU HAVE A LIGAND
if [[ "${ligand_names}" != "NONE" ]]; then

    ##############################
    ### UPDATING TOPOLOGY FILE ###
    ##############################
    echo "** CORRECTING TOPOLOGY FILE **"
    ## RUNNING PYTHON SCRIPT TO COUNT GRO FILE
    /usr/bin/python3.4 "${py_count_gro_residues}" \
            --igro "${path_to_new_sim_output_directory}/${output_prefix}.gro" \
            --sum "${path_to_new_sim_output_directory}/${gro_summary}"
    
    ## STORING SOLVENT NAMES
    declare -a solvent_names=("${water_residue_name}")
    
    ## LOOPING THROUGH EACH COSOLVENT
    for cosolvent in "${cosolvent_name_array[@]}"; do
    
        ## GETTING COSOLVENT RESIDUE NAME
        cosolvent_residue_name="$(itp_get_resname ${cosolvent}.itp)"
        
        ## STORING COSSOLVENT NAME
        solvent_names+=("${cosolvent_residue_name}")
    done

    ## LOOPING THROUGH SOLVENTS
    for current_solvent in "${solvent_names[@]}"; do
        ## FINDING TOTAL SOLVENTS
        num_solvent=$(get_residue_num_from_sum_file ${current_solvent} "${path_to_new_sim_output_directory}/${gro_summary}")
        echo "Correcting for solvents: ${current_solvent} ${num_solvent}"
        ## EDITING TOPOLOGY
        top_update_residue_num "${path_to_new_sim_output_directory}/${output_prefix}.top" "${current_solvent}" "${num_solvent}"
    done

    ## SPLITTING ITP FILE
    itp_extract_prm "${np_itp_file}"

    ## FINDING NP RESIDUE NAME
    np_residue_name="$(itp_get_resname ${np_itp_file})"
    ## PRINTING
    echo "Adding ${np_residue_name} to topology"
    ## ADDING SAM RESIDUE TO TOPOLOGY
    echo "   ${np_residue_name}   1" >> "${path_to_new_sim_output_directory}/${output_prefix}.top" 

    ## CORRECTING TOPOLOGY BY ADDING PARAMETERS
    topology_add_include_specific "${path_to_new_sim_output_directory}/${output_prefix}.top" "${cosolvent}.prm" "#include \"${np_residue_name}.prm\""
    topology_add_include_specific "${path_to_new_sim_output_directory}/${output_prefix}.top" "${cosolvent}.prm" "#include \"${ligand_names}.prm\""

    ## CORRECTING TOPOLOGY BY ADDING ITP FILE
    topology_add_include_specific "${path_to_new_sim_output_directory}/${output_prefix}.top" "${cosolvent}.itp" "#include \"${np_itp_file}\""
    topology_add_include_specific "${path_to_new_sim_output_directory}/${output_prefix}.top" "${cosolvent}.itp" "; including topology for SAMs"

    ###################################################
    ## GENERATING POSITION RESTRAINT FOR NON-WATERS ###
    ###################################################
    ## CREATING INDEX
    make_ndx_gold_with_ligand "${path_to_new_sim_output_directory}/${output_prefix}.gro" \
                              "${path_to_new_sim_output_directory}/${index_file}"   \
                              "${lig_residue_name}"   \
                              "${gold_residue_name}"  

    ## DEFINING COMBINED NAME
    combined_name="${gold_residue_name}_${lig_residue_name}"

    ## GENERATING POSITION RESTRAINTS
gmx genrestr -f "${path_to_new_sim_output_directory}/${output_prefix}.gro" -o "${path_to_new_sim_output_directory}/${np_posre}" -fc "${position_restraint_value}" "${position_restraint_value}" "${position_restraint_value}" -n "${index_file}" >/dev/null 2>&1  << INPUTS
${combined_name}
INPUTS

    # FIXING NUMBERING SYSTEM
    itp_fix_genrestr_numbering "${path_to_new_sim_output_directory}/${np_posre}"

    # ADDING POSITION RESTRAINTS TO TOPOLOGY FILE
    topology_add_include_specific "${path_to_new_sim_output_directory}/${output_prefix}.top" "${np_itp_file}" "#include \"${np_posre}\""

fi

##################################
### CORRECTING FOR COUNTERIONS ###
##################################

## DEFINING SOLVATES TO BALANCE
balance_solvents="formate,methylammonium"

## DEFINING SCALE
balance_solvents_VDW_scale="${VDW_scale}"
#"0.5"

## EXPANDED GRO DISTANCE
expand_gro_name="${output_prefix}_expand.gro"
expand_gro_dist="0.2" # nm
expanded_box=false

## CONVERTING BALANCED SOLVENTS TO ARRAY
IFS=',' read -r -a balance_solvents_array <<< "${balance_solvents}"; unset IFS

## STORING SOLVENT NAMES
declare -a balance_solvent_array_num=()
declare -a balance_solvent_res_name=()

## TODO: Include a way to check cosolvents

## LOOPING THROUGH EACH COSOLVENT
for current_solvent in "${balance_solvents_array[@]}"; do
    ## GETTING COSOLVENT RESIDUE NAME
    current_solvent_resname="$(itp_get_resname ${current_solvent}.itp)"
    ## FINDING TOTAL SOLVENTS
    num_solvent=$(get_residue_num_from_sum_file ${current_solvent_resname} "${path_to_new_sim_output_directory}/${gro_summary}")
    ## STORING COSSOLVENT NAME
    balance_solvent_res_name+=("${current_solvent_resname}")
    balance_solvent_array_num+=("${num_solvent}")
done

## FINDING LARGEST NUMBER OF SOLVENT
largest_num_solvent=$(printf '%s\n' "${balance_solvent_array_num[@]}" |  awk '$1 > m || NR == 1 { m = $1 } END { print m }')

## GETTING DIFFERENCES
for (( i = 0 ; i < ${#balance_solvent_array_num[@]} ; i++ )) do  (( balance_solvent_array_num[$i]="${largest_num_solvent}" - ${balance_solvent_array_num[$i]})) ; done

## LOOPING AND ADDING SOLVENTS
for idx in "${!balance_solvent_array_num[@]}"; do
    ## DEFINING COSOLVENT NAME
    cosolvent_name="${balance_solvents_array[idx]}"
    ## DEFINING EACH SOLVENT RESIDUE NAME
    cosolvent_resname="${balance_solvent_res_name[idx]}"
    ## GETTING NUMBER
    cosolvent_num="${balance_solvent_array_num[idx]}"
    ## ADDING COSOLVENT IF VALUE IS GREATER THAN ZERO
    if (( $(echo "${cosolvent_num} > 0" | bc -l ) )); then
        ## EXPANDING BOX
        if [[ "${expanded_box}" == false ]]; then
            gro_expand_box "${output_prefix}.gro" "${expand_gro_dist}"
            ## TURNING OFF EXPANSION
            expanded_box=true
        fi
    
        ## DEFINING COSOLVENT
        path_input_molecule="${PREP_SOLVENT_FINAL}/${cosolvent_name}/${cosolvent_name}.gro"
        ## GMX INSERT MOLECULES REPLACE ANY SOLVENTS WITH ROTATIONS
        gmx insert-molecules -f "${output_prefix}.gro" -ci "${path_input_molecule}"  -o "${output_prefix}.gro" -scale "${balance_solvents_VDW_scale}" -nmol "${cosolvent_num}" -try 100000000 -rot xyz
        
        #### UPDATING TOPOLOGY INFORMATION
        echo "    ${cosolvent_resname}    ${cosolvent_num}" >> ${output_prefix}.top
    fi
done


########################################
### EDITING MDP AND SUBMISSION FILES ###
########################################
## EQUIL MDP FILES
sed -i "s/NSTEPS/${equil_mdp_time}/g" ${equil_mdp_file}
sed -i "s/TEMPERATURE/${equil_mdp_temp}/g" ${equil_mdp_file}

## PROD MDP FILES
sed -i "s/NSTEPS/${mdp_file_time}/g" ${prod_mdp_file}
sed -i "s/TEMPERATURE/${temp}/g" ${prod_mdp_file}

## SUBMISSION FILE
sed -i "s/_JOB_NAME_/${new_dirname}/g" ${output_submit_name}
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" ${output_submit_name}
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" ${output_submit_name}
sed -i "s/_EM_GRO_FILE_/${output_prefix}_em.gro/g" ${output_submit_name}
sed -i "s/_EQUILMDPFILE_/${equil_mdp_file}/g" ${output_submit_name}
sed -i "s/_PRODMDPFILE_/${prod_mdp_file}/g" ${output_submit_name}
sed -i "s/_TOPFILE_/${output_prefix}.top/g" ${output_submit_name}

#####################################################################
### UPDATING MDP FILE FOR EQUILIBRATION FOR TEMPERATURE ANNEALING ###
#####################################################################
if [[ "${want_temp_annealing}" == true ]]; then
    ## ADDING ANNEALING, 600 K ANNEALING TEMPERATURE
    echo "
; ANNEALING TEMPERATURE
annealing   = single ; single set of temperature points
annealing-npoints = 5 ; all groups that are not annealed
annealing-time = 0 1000 2000 3000 4000; 0 ns, 1 ns, 2 ns, 3 ns
annealing-temp = 300 600 600 300 300; 300 K to 600 K, stay at 600 K, then back to 300 K
" >> ${equil_mdp_file}

fi

#####################################
##### NEUTRALIZING IF NECESSARY #####
#####################################
## DEFINING MAX WARN
max_warn_index=2

echo -e "--- NEUTRALIZING CHARGES ---\n"
echo "Creating TPR file for balancing of charge"
gmx grompp -f ${em_mdp_file} -c "${output_prefix}.gro" -p "${output_prefix}.top" -o "${output_prefix}_solvated.tpr" -maxwarn ${max_warn_index}
gmx genion -s "${output_prefix}_solvated.tpr" -p "${output_prefix}.top" -o "${output_prefix}_withcharge.gro" -neutral << INPUT
SOL
INPUT

##########################################
#### CREATING ENERGY MINIMIZATION FILE ###
##########################################
echo -e "--- ENERGY MINIMIZATION--- \n"
gmx grompp -f ${em_mdp_file} -c "${output_prefix}_withcharge.gro" -p "${output_prefix}.top" -o "${output_prefix}_em" -maxwarn 1
## ENERGY MINIMIZING
gmx mdrun -nt 10 -v -deffnm "${output_prefix}_em" 

## REMOVING EXTRAS
# rm \#*

## CREATING SUMMARY FILE
echo "Cosolvent name: ${cosolvent}" >> "${transfer_summary}"
echo "Cosolvent percentage: ${cosolvent_perc}" >> "${transfer_summary}"
echo "Cosolvent perctype: ${perc_type}" >> "${transfer_summary}"
echo "Cosolvent temp: ${mixed_solv_temp}" >> "${transfer_summary}"
echo "---------" >> "${transfer_summary}"
echo "Mixed solvent name: ${mixed_solvent_name}" >> "${transfer_summary}"
echo "Frame in ps: ${desired_cosolvent_frame_ps}" >> "${transfer_summary}"
echo "Path mixed: ${path_mixed_solvents}" >> "${transfer_summary}"
echo "---------" >> "${transfer_summary}"
echo "Input simulation name: ${simulation_dir_name}" >> "${transfer_summary}"
echo "Ligand name: ${ligand_names}" >> "${transfer_summary}"

## ADDING TO JOB LIST
echo "${path_to_new_sim_output_directory}/${output_submit_name}" >> "${JOB_SCRIPT}"
