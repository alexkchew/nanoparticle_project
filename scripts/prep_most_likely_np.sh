#!/bin/bash

# prep_most_likely_np.sh
# This script will find the most likely configuration of a nanoparticle based on 
# bundling groups, then output the nanoparticle centered. Finally, we will run 
# this nanoparticle with all the atoms frozen.

# Written by Alex K. Chew (03/29/2019)

### VARIABLES:
# $1: diameter
# $2: temperature

### SOURCING ALL VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

# Getting script name and echoing
print_script_name

## SETTING SO THAT YOU WILL GET ERRORS IF SOMETHING GOES WRONG
# set -e

### INPUT VARIABLES
## FORCE FIELD
forcefield="${FORCEFIELD}" # Force Field (opls-aa or charmm36-nov2016.ff)

## DIAMETER
diameter="2"
# "6"
# "6"
# "2"
# "6"
# "2"
# "6"
# "2"
# "2"
# "6"
# "2"
# "6"
# "2" # "$1" # "$1" # 2 nm

## TEMPERATURE
temp="300.00" # "$2" # "310.15" temperature in K of the system

## DEFINING SHAPE
shape_type="EAM" # "$3"
    # spherical: Spherical shape cutoff from {111} fcc lattice
    # hollow: Hollow spherical shape
    # EAM: embedded atom potential
    
## DEFINING DESIRED TRIAL
trial_num="1" # "$5"

## DEFINING DESIRED NP INDEX
most_likely_index="1"
# "4001"
# "1"
# "200"
# "300"
# "401"
# "1" # First most likely index

## DEFINING SIMULATION DIRECTORY
# simulation_dir_name="190509-2nm_C11_Sims_updated_forcefield"
# simulation_dir_name="191101-PEG_ligands"

# "191023-6nm_EAM_particles"
simulation_dir_name="EAM_COMPLETE"
# 
# "20200410-mixed_sams_charged"
# "EAM_COMPLETE"
# "HYDROPHOBICITY_PROJECT_C11"
# "20200122-double_bond_EAM_models"
# "EAM_COMPLETE"
# "191221-Rerun_all_EAM_models_1_cutoff"
# "HYDROPHOBICITY_PROJECT_ROTELLO"

simulation_dir="${PATH2SIM}/${simulation_dir_name}"

## DEFINING ANALYSIS DIRECTORY
# analysis_dir="/home/akchew/scratch/nanoparticle_project/analysis/EAM_SPHERICAL_HOLLOW"

## DEFINING NEW SIMULATION DIRECTORY NAME
output_simulation_dir="20200708-SI_HYDRO_GNP_SIMS_DEBUGGING_SPRING"
# "20200512-6nm_Least_likely_config"
# unsat
# "20200421-6nm_OH"
# "20200421-branch_frozen"
# "20200414-6nm_particles_frozen"
# "20200411-mixed_sam_frozen"
# "20200401-Renewed_GNP_sims_with_equil_other_molecules_PEG"
# "20200327-Branched_OH"
# "20200323-DOD_Sizes"
# "20200302-GNP"
# "20200227-GNP_spr_50"
# "20200215-GNP_spring_const"
# "20200125-Frozen_double_conds"
# "20200117-Frozen_Rot_Particles_PE"
# "191228-Rerun_frozen_particles"
# "191208-Rotello_particles_frozen"
# "191208-Other_most_likely"
# "191025-Most_likely_np_sims_6_nm"

## DEFINING SPRING CONSTANTS
declare -a lig_spring_constant_array=("50")
declare -a lig_spring_constant_array=("0" "25" "50" "100" "200" "300" "400" "500")
# "0"
# "50"
# "600"
# "1200" "1500"
# "200" "400" "600" "800"
# "50" "100" "1000" "2000"
# "500" "1000" "10000"
# "25" "50" "100" "2000"

## LIGAND PARAMETERS
# "dodecanethiol"
# "C11NH3"
#  "dodecen-1-thiol"
declare -a ligand_name_array=("dodecanethiol" "C11OH") # "dodecanethiol" 
# "C11NH3,C11NH2" "C11COO,C11COOH"
# "C11branch6CONH2" "C11branch6COOH" "C11branch6NH2" "C11branch6CF3" "C11branch6CH3" "C11branch6OH" \
#                              "C11double67CF3" "C11double67CONH2" "C11double67COOH" "C11double67NH2" "C11double67OH" "dodecen-1-thiol"
# "dodecanethiol" "C11OH" "C11NH2" "C11COOH"  "C11CONH2" "C11CF3"
# 
# 
# "C11COOH"
# "C11NH3,C11NH2"
# "C11COO,C11COOH"

## DEFINING LIGAND
ligand_fracs="1"
# "0.53,0.47"
# "1"
# 

## DEFINING LOGICAL
# True if you want a hard restart in the computation of most likely configs
# Otherwise, False
want_recalc="False"
want_debug=true # false if you want full 50 ns runs
# false
# true
# "True"

# "ROTPE1" "ROTPE2" "ROTPE3" "ROTPE4"
# "C11double67OH"
# "C11CF3" "C11CONH2" "C11COOH" "C11NH2" "C11branch6OH" "dodecen-1-thiol"
# "C11branch6OH" 
# "C11OH"
# "dodecanethiol"
#  "C11OH"
# "dodecanethiol"
# "C11branch6OH"
#  "ROTPE1" "ROTPE2" "ROTPE3" "ROTPE4"
# "dodecanethiol" "C11OH"
# "C11OH"
#  "C11OH"
# "C11double67OH" "dodecen-1-thiol"
# "dodecanethiol" "C11OH" "C11NH2" "C11COOH"  "C11CONH2" "C11CF3"
# "C11CF3"
# "C11OH" 
# "C11double67OH" "dodecen-1-thiol"
# "ROTPE1" "ROTPE2" "ROTPE3" "ROTPE4"
# "dodecanethiol" "C11OH"
# "ROT002" "ROT003" "ROT004" "ROT005" "ROT006" "ROT007" "ROT008" "ROT009"
# "C11NH2"  #  "C11COOH" "ROT001" 
# "C11CF3"
# "C11NCH33" "C11CCH33"
# "C11OH"
# "C11COO" "C11NH3"
# "dodecanethiol"
# "C11CONH2"
# "dodecanethiol"
# "C11COOH" "C11NH2" "C11OH" 
# "ROT010" "ROT011" "ROT012" "ROT014"
# "C11COOH"
# "ROTPE1" "ROTPE2" "ROTPE3" "ROTPE4"
# "C11OH" "dodecanethiol"
# "C11COO" "C11NH3"
# "ROT008" "ROT009"
#  "ROT001" "ROT002" "ROT003" "ROT004" "ROT005" "ROT006" "ROT007" "ROT008" "ROT009"
# "C11CF3" "C11CONH2" "C11COOH"
# "C11NH2" "C11COOH"  "dodecanethiol"
# "C11CONH2" "C11OH"
# "dodecanethiol" "C11OH"
# "C11CF3" "C11CONH2" "C11COOH"
## LOOPING THROUGH LIGANDS
for ligand_names in ${ligand_name_array[@]}; do

## LOOPING THROUGH SPRING CONSTANTS
for lig_spring_constant in ${lig_spring_constant_array[@]}; do

## CALCULATING RADIUS
radius=$(awk -v diam=${diameter} 'BEGIN{ printf "%.5f",diam/2}') # nms
dist_to_edge="1" # nm distance from edge of box

## DEFINING JOB TYPE
job_type="NVT_spr"
# NPT_spr: GNP with a specified frozen group in the NPT ensemble
# NVT_frozen: GNP with all atoms frozen
# NVT_spr: GNP with spring constants

## DEFINING SPRING CONSTANTS
gold_posre="gold_posre.itp"
gold_spring_constant="500000" # to be consistent with the planar SAM case
sulfur_posre="sulfur_posre.itp"
sulfur_spring_constant="500000"
lig_posre="lig_posre.itp"

## DEFINING TYPE OF SIMS
displacement_vector_type="avg_heavy_atom"

######################
### DEFINING FILES ###
######################

## DISPLAYING MOST LIKELY
most_likely_dir="most_likely-${displacement_vector_type}"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING GOLD RESIDUE NAME
gold_residue_name="AUNP"

###############################
### FILE SYSTEM INFORMATION ###
###############################

### CHECKING FORCEFIELD SUFFIX
forcefield_suffix=$(check_ff_suffix "${forcefield}")

## DEFINING DIRECTORY NAME
# output_dirname="${shape_type}_${temp}_K_${diameter}_nmDIAM_${ligand_names}_${forcefield_suffix}_Trial_${trial_num}" # _${input_gold_folder}"

### OUTPUT FILE DETAILS
if [[ "${ligand_fracs}" == "1" ]]; then
    output_dirname="${shape_type}_${temp}_K_${diameter}_nmDIAM_${ligand_names}_${forcefield_suffix}_Trial_${trial_num}" # _${input_gold_folder}"
else
    output_dirname="${shape_type}_${temp}_K_${diameter}_nmDIAM_${ligand_names}_${ligand_fracs}_${forcefield_suffix}_Trial_${trial_num}" # _${input_gold_folder}"
fi


## DEFINING NEW SIM DIRECTORY NAME
if [[ "${job_type}" == "NVT_frozen" ]]; then
    new_dirname="MostlikelynpNVT_${output_dirname}_likelyindex_${most_likely_index}"
elif [[ "${job_type}" == "NPT_spr" ]]; then
    new_dirname="Mostlikelynp_${output_dirname}_likelyindex_${most_likely_index}"
    
elif [[ "${job_type}" == "NVT_spr" ]]; then
    new_dirname="MostlikelynpNVTspr_${lig_spring_constant}-${output_dirname}_likelyindex_${most_likely_index}"
fi

## DEFINING PATH TO DIRECTORY
path_to_sim_output_directory="${simulation_dir}/${output_dirname}"

## DEFINING PATH TO 
# path_to_analysis_output_directory="${analysis_dir}/${output_dirname}"

## DEFINING SIMULATION GRO FILE
sim_tpr_file="sam_prod.tpr"
sim_gro_file="sam_prod.gro"
analyze_sim_xtc_file="sam_prod_10_ns_whole_rot_trans.xtc"
extract_sim_xtc_file="sam_prod_10_ns_whole_center.xtc"
sim_top_file="sam.top"

## DEFINING NEW GRO FILE
new_gro_file="sam.gro"
# "sam_initial.gro"

## DEFINING OUTPUT DIRECTORY
path_to_new_sim_output_directory="${PATH2SIM}/${output_simulation_dir}/${new_dirname}"

## MDP FILE INFORMATION
mdp_parent_path="${INPUT_MDP_PATH}/charmm36/Spherical"

### MDP FILE ###

## EQUILIBRATION MDP
npt_equil1_n_steps="1000000" # 2 ns equilibration
npt_equil1_mdp_file="npt_double_equil_gmx5_charmm36.mdp"
npt_equil2_n_steps="1000000" # 2 ns equilibration
npt_equil2_mdp_file="npt_double_prod_gmx5_charmm36.mdp"

## PRODUCTION MDP
if [[ "${job_type}" == "NVT_frozen" ]] || [[ "${job_type}" == "NVT_spr" ]]; then
    mdp_file="nvt_double_prod_gmx5_charmm36_frozen_gold.mdp"
elif [[ "${job_type}" == "NPT_spr" ]]; then
    mdp_file="npt_double_prod_gmx5_charmm36_frozen_gold.mdp"
fi

## ENERGY MINIMIIZATION
em_mdp_file="minim_sam_gmx5.mdp"

## DEFINING MDP FILE TIME
mdp_file_time="25000000" # 50 ns

## TRUE IF YOU WANT DEBUGGING
if [[ "${want_debug}" == true ]]; then
    echo "Since debug is True, setting total time to 5 ns"
    mdp_file_time="2500000" # 5 ns
fi

# "50000000" # 100 ns
# "15000000"  # 30ns
# 20 ns  - 10000000
mdp_temperature="${temp}"

## DEFINING SUBMISSION FILE INFORMATION
submit_parent_path="${PATH2SUBMISSION}"
submit_file_name="submit_mostlikely_np.sh"

## DEFINING OUTPUT SUBMIT NAME
output_submit_name="submit.sh"

## POSITION RESTRAINT FILE
np_posre="np_posre.itp"
position_restraint_value="10000" # 100000

## DEFINING OUTPUT PREFIX
output_prefix="sam"

## DEFINING NUMBER OF CORES
num_cores="28"

#########################################
### MOST LIKELY CONFIGURATION DETAILS ###
#########################################

## DEFINING SCRIPTS 
most_likely_script="${BASH_MOST_LIKELY_SCRIPT}"
# "${BUNDLING_SCRIPT_DIR}/compute_most_likely.sh"

## DEFINING SUMMARY FILE
most_likely_output_summary_file="np_most_likely.summary"

###################
### MAIN SCRIPT ###
###################

## DEFINING LIGAND NAME ARRAY
read -a ligand_name_array <<< "$(str2array_by_delim "${ligand_names}" ",")"

declare -a residue_name=()

for each_ligands in ${ligand_name_array[@]}; do
    ## FINDING RESIDUE NAME
    res_name="$(find_residue_name_from_ligand_txt ${each_ligands})"
    ## ADDING RES NAME
    residue_name+=( "${res_name}" )
done

residue_name_list="$(join_array_to_string "," "${residue_name[@]}")"
residue_name_spaces="$(join_array_to_string " " "${residue_name[@]}")"

## CREATING DIRECTORY
create_dir "${path_to_new_sim_output_directory}" -f

## CHECKING IF PATH EXISTS
stop_if_does_not_exist "${path_to_sim_output_directory}"

##################################
### RUNNING MOST LIKELY SCRIPT ###
##################################
bash "${most_likely_script}" "${path_to_sim_output_directory}" \
                             "${residue_name_list}" \
                             "${most_likely_index}" \
                             "${want_recalc}" \
                             "${displacement_vector_type}"

## OUTPUT FROM GRO FILE
output_most_likely_gro="Idx_${most_likely_index}_System.gro"

## DEFINING GRO FILE
path_most_likely_gro="${path_to_sim_output_directory}/${most_likely_dir}/${output_most_likely_gro}"

echo "${path_most_likely_gro}"
 ## CHECKING IF EXISTS
stop_if_does_not_exist "${path_most_likely_gro}"

## COPYING THE GRO FILE CORRESPONDING TO THE MOST LIKELY CONFIGURATION
cp "${path_most_likely_gro}" "${path_to_new_sim_output_directory}/${new_gro_file}"

## GOING INTO PATH
cd "${path_to_new_sim_output_directory}"

## COPYING EVERYTHING TO NEW PATH
# TOPOLOGY
cp "${path_to_sim_output_directory}/${sim_top_file}" "${path_to_new_sim_output_directory}"
# ITP/PRM FILES
cp "${path_to_sim_output_directory}/"{*.itp,*.prm} "${path_to_new_sim_output_directory}"
# FORCE FIELD
cp -r "${path_to_sim_output_directory}/${forcefield}" "${path_to_new_sim_output_directory}"
# MDP FILE
cp -r "${mdp_parent_path}/"{${npt_equil1_mdp_file},${npt_equil2_mdp_file}} "${path_to_new_sim_output_directory}"
cp -r "${mdp_parent_path}/${mdp_file}" "${path_to_new_sim_output_directory}"
# SUBMISSION FILE
cp -r "${submit_parent_path}/${submit_file_name}" "${path_to_new_sim_output_directory}/${output_submit_name}"


###########################
### CREATING INDEX FILE ###
###########################

## DEFINING INDEX FILE NAME
output_index_file="gold_np.ndx"

if [[ "${job_type}" == "NVT_frozen" ]] || [[ "${job_type}" == "NVT_spr" ]]; then

## CREATING INDEX FILE (LIGAND ONLY)
gmx make_ndx -f "${new_gro_file}" -o "${output_index_file}" << INPUTS
keep 0
r ${gold_residue_name}
r ${residue_name_spaces}
q
INPUTS

    ## SPRING CONSTANTS
    if [[ "${job_type}" == "NVT_spr" ]]; then

        ## DEFINING ATOM NAME OF SULFUR
        sulfur_atom_name="${SULFUR_ATOM_NAME}"

## CREATING INDEX FILE (LIGAND ONLY)
gmx make_ndx -f "${new_gro_file}" \
             -n "${output_index_file}" \
             -o "${output_index_file}" << INPUTS
r ${residue_name_spaces} & !a H* & ! a ${sulfur_atom_name}
name 3 LIG_HEAVY_ATOMS
r ${residue_name_spaces} & a ${sulfur_atom_name}
name 4 LIG_SULFURS
q
INPUTS

# ${residue_name_spaces}_heavy_atoms

        ## DEFINING LIG NAME
        lig_res_name_heavy="LIG_HEAVY_ATOMS"
        sulfur_atoms_key="LIG_SULFURS"
        # "${residue_name_spaces}_heavy_atoms"

fi

elif [[ "${job_type}" == "NPT_spr" ]]; then
## CREATING INDEX FILE
gmx make_ndx -f "${new_gro_file}" -o "${output_index_file}" << INPUTS
q
INPUTS


fi

###################################################
### CREATING POSITION RESTRAINTS FOR THE SOLUTE ###
###################################################

if [[ "${job_type}" == "NVT_frozen" ]] || [[ "${job_type}" == "NVT_spr" ]]; then

    ## INSTEAD, SUBSTITUTE MDP FILE INFORMATION
    sed -i "s/_LIGANDRESNAME_/${residue_name_spaces}/g" ${mdp_file}
    
    ## TURNING OFF MDP FROZEN INFORMATION
    if [[ "${job_type}" == "NVT_spr" ]]; then
        ## COMMENTING OUT FREEZE GROUPS
        mdp_comment_out_specific_flags "${mdp_file}" "freezegrps"
        mdp_comment_out_specific_flags "${mdp_file}" "freezedim"
    
    ## DEFINING POSITION RESTRAINTS
    
## GENERATING POSITION RESTRAINTS FOR GOLD
gmx genrestr -f "${new_gro_file}" \
             -o "${gold_posre}" \
             -n "${output_index_file}" \
             -fc "${gold_spring_constant}" \
                 "${gold_spring_constant}" \
                 "${gold_spring_constant}" >/dev/null 2>&1 << INPUTS
${gold_residue_name}
INPUTS

## GENERATING POSITION RESTRAINTS FOR SULFUR
gmx genrestr -f "${new_gro_file}" \
             -o "${sulfur_posre}" \
             -n "${output_index_file}" \
             -fc "${sulfur_spring_constant}" \
                 "${sulfur_spring_constant}" \
                 "${sulfur_spring_constant}" >/dev/null 2>&1 << INPUTS
${sulfur_atoms_key}
INPUTS


## GENERATING POSITION RESTRAINTS FOR LIGAND
gmx genrestr -f "${new_gro_file}" \
             -o "${lig_posre}" \
             -n "${output_index_file}" \
             -fc "${lig_spring_constant}" \
                 "${lig_spring_constant}" \
                 "${lig_spring_constant}" << INPUTS
${lig_res_name_heavy}
INPUTS
#  >/dev/null 2>&1

## ADDING POSITION RESTRAINTS TO TOPOLOGY FILE
topology_add_include_specific "${sim_top_file}" "sam.itp" "#include \"${gold_posre}\""
topology_add_include_specific "${sim_top_file}" "sam.itp" "#include \"${sulfur_posre}\""
topology_add_include_specific "${sim_top_file}" "sam.itp" "#include \"${lig_posre}\""
    
    fi

elif [[ "${job_type}" == "NPT_spr" ]]; then
    ## NPT SIMULATIONS

## GENERATING POSITION RESTRAINT FOR NON-WATERS
gmx genrestr -f "${new_gro_file}" \
             -o "${np_posre}" \
             -fc "${position_restraint_value}" "${position_restraint_value}" "${position_restraint_value}" >/dev/null 2>&1 << INPUTS
non-Water
INPUTS

    ## FIXING NUMBERING SYSTEM
    # itp_fix_genrestr_numbering "${np_posre}"

    ## ADDING POSITION RESTRAINTS TO TOPOLOGY FILE
    topology_add_include_specific "${sim_top_file}" "sam.itp" "#include \"${np_posre}\""
    
fi

########################################
### EDITING MDP AND SUBMISSION FILES ###
########################################

## DEFINING MAXWARN
maxwarn="5"
## MDP FILES ( EQUILIBRATION )
sed -i "s/NSTEPS/${npt_equil1_n_steps}/g" ${npt_equil1_mdp_file}
sed -i "s/TEMPERATURE/${temp}/g" ${npt_equil1_mdp_file}

sed -i "s/NSTEPS/${npt_equil2_n_steps}/g" ${npt_equil2_mdp_file}
sed -i "s/TEMPERATURE/${temp}/g" ${npt_equil2_mdp_file}

## MDP FILES ( PRODUCTION )
sed -i "s/NSTEPS/${mdp_file_time}/g" ${mdp_file}
sed -i "s/TEMPERATURE/${temp}/g" ${mdp_file}

## SUBMISSION FILE
sed -i "s/_JOB_NAME_/${new_dirname}/g" ${output_submit_name}
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" ${output_submit_name}
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" ${output_submit_name}

sed -i "s/_MDPEQUIL1_/${npt_equil1_mdp_file}/g" ${output_submit_name}
sed -i "s/_MDPEQUIL2_/${npt_equil1_mdp_file}/g" ${output_submit_name}
sed -i "s/_MDPPROD_/${mdp_file}/g" ${output_submit_name}
sed -i "s/_MAXWARN_/${maxwarn}/g" ${output_submit_name}
sed -i "s/_INDEXFILE_/${output_index_file}/g" ${output_submit_name}

###########################################
### CREATING TPR GIVEN TOPOLOGY AND GRO ###
###########################################
# echo "*** Creating tpr file ***"
# gmx grompp -f ${mdp_file} \
#            -c ${new_gro_file} \
#            -p ${sim_top_file} \
#            -o "${output_prefix}_prod" \
#            -n "${output_index_file}" \
#            -maxwarn 1

## ADDING JOB TO JOB LIST
echo "${path_to_new_sim_output_directory}/${output_submit_name}" >> "${JOB_SCRIPT}"
echo "Creating most likely np configurations for: ${new_dirname}"

# if [ -e "${output_prefix}_prod.tpr" ]; then
#     echo "${path_to_new_sim_output_directory}" >> "${JOB_SCRIPT}"

#     ## PRINTING SUMMARY
#     echo "Creating most likely np configurations for: ${new_dirname}"
# else
#     echo "Error! ${output_prefix}_prod.tpr not found!"
#     echo "Path: $(pwd)"
#     echo "Exiting ..."
#     sleep 5
#     exit 1
# fi

#############################
### CREATING SUMMARY FILE ###
#############################
summary_file="transfer_summary.txt"

echo "Ligand name: ${ligand_names}" > "${summary_file}"
echo "Ligand spring constant: ${lig_spring_constant}" >> "${summary_file}"
echo "Gold spring constant: ${gold_spring_constant}" >> "${summary_file}"
echo "Sulfur spring constant: ${sulfur_spring_constant}" >> "${summary_file}"
echo "Job type: ${job_type}" >> "${summary_file}"
echo "--------------------------" >> "${summary_file}"
echo "Temperature: ${temp}" >> "${summary_file}"
echo "Bundling vector type: ${displacement_vector_type}" >> "${summary_file}"
echo "Most likely dir: ${most_likely_dir}" >> "${summary_file}"
echo "--------------------------" >> "${summary_file}"
echo "MDP file time steps: ${mdp_file_time}" >> "${summary_file}"

## PRINTING
cat "${summary_file}"

done

done