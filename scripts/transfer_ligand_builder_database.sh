#!/bin/bash

# transfer_ligand_builder_database.sh
# This script transfers the self-assembly gold using new ligands with ligand builder
# This is the same as transfer_ligand_builder, except we are varying the ligand types, etc.
# This will be updated to the full version of transfer_ligand_builder soon!

# Written by Alex K. Chew (08/28/2020)

### VARIABLES:
# $1: diameter
# $2: temperature
# $3: shape_type (i.e. what shape would you like?)
# $4: ligand_names: name of the ligands
# $5: trial number
# $6: simulation directory
# $XXX: ligand_frac: fraction of ligands (DEFAULT 1 FOR NOW)

## USAGE: 
#   bash transfer_ligand_builder.sh 2 310.15 hollow butanethiol 1 test_dir

## NOTE: If you are having LINCS warning -- you can update the transfer_ligand.py script and change the transition to half boxlength to outside the box. This serves as a good way of testing your NP bonding. So far, 50,000 does not have errors.

# ** UPDATES **
#   180223 - Added binding gold nanoparticles to sulfur atom script
#   180508 - Added directory functionality

### SOURCING ALL VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

# Getting script name and echoing
print_script_name

### --- USER-SPECIFIC PARAMETERS (BEGIN) --- ###

### INPUT VARIABLES

## DEFINING GROMACS COMMAND
gromacs_command="gmx"
# gromacs_command="gmx_mpi"

## PROD RUN
gromacs_mdrun="gmx mdrun -v -s -nt"
# gromacs_mdrun="gmx_mpi mdrun -v -s -ntomp"


## DIAMETER
diameter="${1-6.20}" # 2 nm

## TEMPERATURE
temp="${2-300.00}" # "310.15" temperature in K of the system

## DEFINING SHAPE
shape_type="${3-spherical}"
    # spherical: Spherical shape cutoff from {111} fcc lattice
    # hollow: Hollow spherical shape
    # EAM: embedded atom potential

## DEFINING DESIRED TRIAL
trial_num="${5-1}"

## LIGAND PARAMETERS
ligand_names="${4-LIG0}" # "butanethiol" # name of ligand from .lig file

## DEFINING SIMULATION DIRECTORY
simulation_dir="${6-testing}"
ligand_fracs="${7-1}" # fraction of ligands

## DEFINING IF YOU WANT MODIFIED FORCEFIELD
modified_ff="${8-false}"
# If true, you will use modified force field that has new benzene parameters

## DEFINING ASSEMBLY TYPE
assembly_type="${9-C1CCSS1}"

## DEFINING FORCE FIELD
forcefield="${10-FORCEFIELD}" # Force Field (opls-aa or charmm36-nov2016.ff)

## DEFINING GNP NAME
gnp_name="${11-GNP0}"

## DEFINING NUMBER OF MAX LIGANDS
max_num_ligs="${12-None}"

## DEFINING SUMMARY
summary_details="${13-""}"

## DEFINING IF YOU WANT DEBUG
want_debug_np_em=false
# true
# true if you want to debug nanoparticle energy minimization abilities. We will use this 
# flag as a way of checking if all nanoparicles work with MD simulations

## DEFINING IF YOU WANT TO REWRITE
want_rewrite=false

## CALCULATING RADIUS
radius=$(awk -v diam=$diameter 'BEGIN{ printf "%.5f",diam/2}') # nms
dist_to_edge="1" # nm distance from edge of box
# dist_to_edge="1.5" # TESTING, CHANGE BACK TO 1 LATER
# dist_to_edge="2" # nm distance from edge of box <-- TEMP, TESTING

## STORING ORIG FORCE FIELD
orig_ff="${forcefield}"

## PATH
job_script_path=$(dirname ${JOB_SCRIPT})

## DEFINING IF YOU WANT QUICK TEST JOBS
want_quick_test_jobs=false
# true if you watn to test if workflow is correct

###################
### FORCE FIELD ###
###################

## IF MODIFIED FF IS TRUE
if [[ "${modified_ff}" == true ]]; then
    forcefield="${forcefield%.ff}-mod.ff"
    # "${MODIFIED_FF}"
    echo "Using modified force field!"
    ## CHECKING IF FILE EXISTS
    if [[ ! -e "${PATH_FORCEFIELD}/${forcefield}" ]]; then
        echo ""
    ## RUNNING PYTHON
    python3.6 "${PYTHON_MODIFY_FF}" --main_path "${PATH_FORCEFIELD}" \
                                    --input_ff_folder "${FORCEFIELD}" \
                                    --output_ff_folder "${forcefield}" \
                                    --ff_nbfix "${NBFIX_FILE}"
    else
        echo "Modified force field exists! Skipping step for generating new force fields"
        echo "Path: ${PATH_FORCEFIELD}/${forcefield}"
    fi
else
    echo "Using original force field"

fi

#########################
### DEFAULT VARIABLES ###
#########################

## NUMBER OF CORES
num_cores="28"

## PREPARATION GOLD
if [[ "${assembly_type}" == "C1CCSS1" ]]; then
    prep_gold_folder="${PREP_GOLD_SIM_NONFRAG}"
    input_gold_folder="${shape_type}/${shape_type}_${diameter}_nmDIAM_300_K_2_nmEDGE_5_AREA-PER-LIG_4_nm_300_K_bidente_Trial_${trial_num}"
else
    prep_gold_folder="${PREP_GOLD_SIM_NONFRAG}"
    # "${PREP_GOLD_SIM}"
    input_gold_folder="${shape_type}/${shape_type}_${diameter}_nmDIAM_300_K_2_nmEDGE_5_AREA-PER-LIG_4_nm_300_K_butanethiol_Trial_${trial_num}"
fi



## PRINTING
echo "------------"
echo "Assembly type: ${assembly_type}"
echo "Prep gold folder: ${prep_gold_folder}"
echo "Input gold folder: ${input_gold_folder}"

input_gold_gro="gold_ligand_equil.gro"

## PYTHON SCRIPT
transfer_python_script="transfer_ligand_builder.py"
bind_gold_sulfur_script="bind_nanoparticles.py"
py_transfer_script="${MDBUILDER}/builder/${transfer_python_script}"
py_bind_gold_sulfur_script="${MDBUILDER}/applications/nanoparticle/${bind_gold_sulfur_script}"
py_add_gold_script="${MDBUILDER}/applications/nanoparticle/system_prep_add_gold.py"
py_add_np_index_script="${MDBUILDER}/applications/nanoparticle/get_nanoparticles_indices.py"

py_check_lig_duplicates="${MDBUILDER}/applications/nanoparticle/check_ligand_prm_files.py"

## REMOVING SCRIPT
py_remove_atoms_file="${MDBUILDER}/math/remove_atoms.py" # Removes residues within the center core (for hollow shell geometry)

########################
### FILE DIRECTORIES ###
########################

## MDP FILES
# mdp_em="minim_sam_gmx5.mdp" # Energy minimization mdp script
mdp_em="minim_sam_gmx5_finetune.mdp" # Energy minimization mdp script <-- finetune means 500 kJ/mol/nm cutoff
mdp_equil="npt_double_equil_gmx5_charmm36.mdp"
# mdp_prod="npt_double_prod_gmx5_charmm36_10ps.mdp" # production 10 ps script
mdp_prod="npt_double_prod_gmx5_charmm36.mdp" # Production mdp script
mdp_dir="${INPUT_DIR}/MDP_FILES/charmm36"

## CHECKING MDP FOLDER
if [ "${shape_type}" == "spherical" ] || [ "${shape_type}" == "hollow" ] || [ "${shape_type}" == "EAM" ]; then
    mdp_folder="Spherical"
fi
path2mdp="${mdp_dir}/${mdp_folder}"

## DEFINING NUMBER OF CORES
num_cores="28"

## GROMPP OPTIONS
max_warn_index="5" # Number of times for maximum warnings

## JOB SCRIPT FILES
submitScript="submit_transfer_ligand_builder_database.sh"
run_script="run_transfer_ligand_builder_database.sh"
output_run_script="run.sh"
output_submit="submit.sh" # output submit script
# "submit.sh"

### SIMULATION PARAMETERS
## NUMBER OF STEPS FOR EQUILIBRATION AND PRODUCTION -- # ps / .002 timesteps
n_steps_equil="500000" # 1 ns equilibration
# "2500000" # 5 ns equilibration
n_steps_prod="10000000" # 20 ns production

## FOR THE SAKE OF DEBUGGING
if [[ "${want_quick_test_jobs}" == true ]]; then
    n_steps_equil="100000" # 200 ps equil
    n_steps_prod="0" # 0 ns production
fi

# "25000000" # 20 ns production run
# 50 ns production
# n_steps_prod="50000000" # 100 ns production
# n_steps_prod="50000000" # 5 ns prod (DEBUG) "50000000" # "25000000" # 50 ns production
# 2500000 -- 5 ns
# 25000000 -- 50 ns
# 50000000 -- 100 ns

### CHECKING FORCEFIELD SUFFIX
forcefield_suffix=$(check_ff_suffix "${forcefield}")

### OUTPUT FILE DETAILS
if [[ "${ligand_fracs}" == "1" ]]; then
    output_dirname="${shape_type}_${temp}_K_${gnp_name}_${forcefield_suffix}_Trial_${trial_num}" # _${input_gold_folder}"
else
    output_dirname="${shape_type}_${temp}_K_${gnp_name}_${forcefield_suffix}_Trial_${trial_num}" # _${input_gold_folder}"
fi

output_dir="${PATH2SIM}/${simulation_dir}/${output_dirname}" # <-- EDIT THIS IF YOU WANT THE SIMS TO GO TO A DIFFERENT DIRECTORY
output_file_name="sam"
output_itp="${output_file_name}.itp" # Name of itp file
output_top="${output_file_name}.top" # Name of topology
output_gro="${output_file_name}.gro" # Name of gro file
output_ndx="index.ndx" # name of the index file

### --- USER-SPECIFIC PARAMETERS (END) --- ###

###################
### MAIN SCRIPT ###
###################

## DEFINING OUTPUT BOX TYPE
output_box_type="dodecahedron"
# "octahedron"

## DEFINING DEFAULT GOLD TO STORE
gold_surface_increment="0.3"
# "None"
# 0.5
# Increment in nanometers relative to surface

## CHECKING IF OUTPUT DIRECTORY EXISTS
if [[ ! -e "${output_dir}" ]] || [[ "${want_rewrite}" == true ]]; then

### CHECKING IF DIRECTORY EXISTS
check_output_dir "${output_dir}"

## STORING SUMMARY
echo "${summary_details}" > "${output_dir}/transfer_summary.txt"

### CHANGING DIRECTORY TO OUTPUT DIRECTORY
cd "${output_dir}"

## DEFINING FINAL LIGAND
# final_ligand="${PREP_LIGAND_FINAL}"
final_ligand="/home/akchew/scratch/MDLigands/database_ligands/final_structures/${orig_ff}"

## DEFINING INDICES
output_sulfur_index="nearby_sulfurs.ndx"

## PRINTING
echo "======================================================="
echo "Step 1: Running transfer ligand builder: ${transfer_python_script}"
echo "======================================================="
echo ""

## RUNNING PYTHON SCRIPT TO OUTPUT INTO THE OUTPUT FOLDER
python ${py_transfer_script} --names ${ligand_names} \
                             --prep ${final_ligand} \
                             --ff ${forcefield} \
                             --ofold ${output_dir} \
                             --gfold "${prep_gold_folder}" \
                             --inputgoldfold "${input_gold_folder}" \
                             --goldgro "${input_gold_gro}" \
                             --fracs "${ligand_fracs}" \
                             --ogro "${output_gro}" \
                             --otop "${output_top}" \
                             --geom "${shape_type}" \
                             --assembly "${assembly_type}" \
                             --gold_surface_increment "${gold_surface_increment}" \
                             --max_num_ligs "${max_num_ligs}" \
                             --output_sulfur_index "${output_sulfur_index}" \
                             --skip_gold

## CHECKING IF GRO EXISTS
if [[ ! -e "${output_gro}" ]] || [[ ! -e "${output_top}" ]]; then

    echo "Error! No output gro / top file"
    echo "Check transfer ligand step!"
    sleep 3
    ## EXITING COMMAND
    exit
fi

#######################################
### CHECKING FOR LIGANDS PARAMETERS ###
#######################################

## PRINTING
echo "======================================================="
echo "Step 2: Copying ligand parameters"
echo "======================================================="
echo ""

## DEFINING LIGAND NAME ARRAY
read -a ligand_name_array <<< "$(str2array_by_delim "${ligand_names}" ",")"

## STORING LIST OF LIGAND PARAMETER FILES
declare -a ligand_prm_files=()

## LOOPING THROUGH EACH LIGAND NAME AND COPYING PARAMETERS
for each_ligand in ${ligand_name_array[@]}; do

    ## SEE IF WE NEED TO ADD ADDITIONAL PARAMETERS
    input_ligand_parameter_file_name="${each_ligand}.prm" # _${forcefield_suffix}
    echo "Checking if there are extra ligand parameters for: ${input_ligand_parameter_file_name}"
    ## DEFINING LIGAND PARAMETER FULL PATH
    path_ligand_parameter="${final_ligand}/${each_ligand}/${input_ligand_parameter_file_name}"
    if [ -e "${path_ligand_parameter}" ]; then
        # COPYING PARAMETER FILE
        cp -rv "${path_ligand_parameter}" "./"
        ## ADDING PARAMETER FILE TO LIST
        ligand_prm_files+=("${input_ligand_parameter_file_name}")

        # ADDING PARAMETER TO TOPOLOGY FILE
        force_field_line_num=$(grep -nE "forcefield.itp" ${output_top} | grep -Eo '^[^:]+') # Line where you see forcefield.itp
        sed -i "$((${force_field_line_num}+1))i #include \"${input_ligand_parameter_file_name}\"" ${output_top}
    else
        echo "No additional ligand parameters found at ${path_ligand_parameter}"
        echo "Continuing without inclusion of *.prm files"
    fi

    ## COPYING OVER ITP FILE
    echo "--- COPYING ITP FILES ...---"
    cp -rv "${final_ligand}/${each_ligand}/${each_ligand}.itp" ./

done

## GETTING PARAMETERS AS A LIST
ligand_prm_files_string=$(join_array_to_string , "${ligand_prm_files[@]}")

## CHECKING PARAMETERS FOR DUPLICATES
python "${py_check_lig_duplicates}" --path "${output_dir}" \
                                    --prm_files "${ligand_prm_files_string}"

## COPYING FORCE FIELD TO OUTPUT DIRECTORY
echo -e "--- COPYING FORCE FIELD FILES TO CURRENT DIRECTORY ---\n"
cp -r "${PATH_FORCEFIELD}/${forcefield}" "${output_dir}"

## MOVING ALL MDP FILES INTO DIRECTORY
echo -e "--- COPYING MDP FILES TO CURRENT DIRECTORY ---\n"
cp -rv "${path2mdp}/"{${mdp_em},${mdp_equil},${mdp_prod}} ./

### USING SED TO FIX MDP FILE PARAMETERS
echo -e "--- CORRECTING MDP FILE OPTIONS ---\n"
# CHANGING NUMBER OF STEPS
sed -i "s/NSTEPS/${n_steps_equil}/g" ${mdp_equil}
sed -i "s/NSTEPS/${n_steps_prod}/g" ${mdp_prod}

# CHANGING TEMPERATURE
sed -i "s/TEMPERATURE/${temp}/g" ${mdp_equil}
sed -i "s/TEMPERATURE/${temp}/g" ${mdp_prod}

##########################################
### DEFINING ENERGY MINIMIZATION STEPS ###
##########################################

echo "======================================================="
echo "Step 3: Minizing gold structure to prevent clashes"
echo "======================================================="
echo ""

## CREATING INDEX FILE
echo "Creating index file: ${output_ndx}"
"${gromacs_command}" make_ndx -f "${output_gro}" -o "${output_ndx}" &> /dev/null << INPUTS
q
INPUTS

## ADDING INDICES
cat "${output_sulfur_index}" >> "${output_ndx}"

## ADDING TO INDICES
separate_indices_far_atoms="far_indices.txt"
num_bonds_indices="bond_lengths.txt"
python "${py_add_np_index_script}" --input_path "${output_dir}" \
                                   --input_gro "${output_gro}" \
                                   --output_index "${output_ndx}" \
                                   --separate_furthest_indices "${separate_indices_far_atoms}" \
                                   --num_bonds_indices "${num_bonds_indices}"


## SEEING IF YOU WANT TO SKIP NVT
skip_nvt=false
# true
# false
# true

## TRUE IF YOU ALLOW FOR NVT TO FAIL AND SKIP IT
allow_failure_of_nvt=true 

## DEFAULT PARMAETERS
## DEFINING MDP
mdp_em_FEP="minim_sam_gmx5_FEP.mdp"
mdp_em_nvt_sim="minim_quick_nvt_frozen.mdp"
# This MDP file is used for a quick run

## DEFINING NUMBER OF CORES
num_em_cores="28"
num_nvt_cores="28"
# 1

#################################
### ENERGY MINIMIZATION STEPS ###
#################################

## DEFINING LAMBDA STATES
# Short
declare -a vdw_lambda_states=("0.10" "0.30" "0.60" "0.80" "0.90" "0.95" "1.00")
# Long
# declare -a vdw_lambda_states=("0.10" "0.20" "0.30" "0.40" "0.50" "0.60" "0.70" "0.80" "0.90" "1.00") 

## DEFINING STATES
declare -a lambda_states=(${!vdw_lambda_states[@]})

## JOINING LAMBDA STATES
lambda_states_string=$(join_by " " ${vdw_lambda_states[@]})

###########################
#### CREATING MDP FILES ###
###########################
echo "Creating MDP files"
## LOOPING AND RUNNING EM
for each_lambda_state in ${lambda_states[@]}; do

    ## CREATING NEW MDP
    mdp_em_FEP_new="${mdp_em_FEP%.mdp}_${each_lambda_state}.mdp"

    ## COPYING OVER FILE
    cp -r "${path2mdp}/${mdp_em_FEP}" "${mdp_em_FEP_new}"
    echo "   --> ${mdp_em_FEP_new}"
    ## EDITING THE STATE
    sed -i "s/_INITIAL_LAMBDA_STATE_/${each_lambda_state}/g" "${mdp_em_FEP_new}"
    sed -i "s/_VDWSTATES_/${lambda_states_string}/g" "${mdp_em_FEP_new}"

done

### RUNNING QUICK EM CALC

## DEFINING CURRENT GRO
current_gro="${output_gro}"

## LOOPING AND RUNNING EM
for each_lambda_state in ${lambda_states[@]:0:0}; do # 0:4
    ## PRINTING
    echo ""
    echo "======= Running energy minimization (GNP only): ${each_lambda_state} ======="

    ## CREATING NEW MDP
    mdp_em_FEP_new="${mdp_em_FEP%.mdp}_${each_lambda_state}.mdp"

    ## CREATING NEW GRO FILE
    new_prefix="${output_gro%.gro}_initial_FEP_em_${each_lambda_state}"

    ## DEFINING OUTPUT
    output_txt="${new_prefix}.out"

    ## ENERGY MINIMIZING - ADDED FOR NPs
    "${gromacs_command}" grompp -f "${mdp_em_FEP_new}" \
               -c "${current_gro}" \
               -p "${output_top}" \
               -o "${new_prefix}.tpr" \
               -n "${output_ndx}" \
               -maxwarn "${max_warn_index}" > "${output_txt}" 2>&1

    ## PRINTING
    echo "Output text: ${output_dir}/${output_txt}"
    echo ""

    ## RUNNING ENERGY MINIMIZATION
    ${gromacs_mdrun} "${num_em_cores}" -deffnm "${new_prefix}" >> "${output_txt}" 2>&1

    ## CHECKING IF GRO IS THERE
    if [[ ! -e "${new_prefix}.gro" ]] && [[ "${num_em_cores}" != 10 ]]; then
        echo "Energy minimization did not complete! Turning down energy minimization number of cores to 10!"
        echo "This error may be due to domain decomposition errors!"
        num_em_cores=10
        ## RERUNNING
        ${gromacs_mdrun} "${num_em_cores}" -deffnm "${new_prefix}" >> "${output_txt}" 2>&1
    fi

    ## CHECKING IF GRO IS THERE
    if [[ ! -e "${new_prefix}.gro" ]] && [[ "${num_em_cores}" != 1 ]]; then
        echo "Energy minimization did not complete! Turning down energy minimization number of cores to 1!"
        echo "This error may be due to domain decomposition errors!"
        num_em_cores=1
        ## RERUNNING
        ${gromacs_mdrun} "${num_em_cores}" -deffnm "${new_prefix}" >> "${output_txt}" 2>&1
    fi

    ## REDEFINING GRO
    current_gro="${new_prefix}.gro"

done

## STORING POST MINIMUM GRO
post_min_gro="${current_gro}"

## PRINTING EM IS TRUE
echo "First energy minimization is complete!"

## DON'T SKIP NVT
#########################
### NVT EQUILIBRATION ###
#########################

# Turn this on if you are having trouble 
# with energy minimization. This will run a quick 
# NVT simulation where ligands are pulled away 
# from the gold core. The idea would be to 
# ensure that ligands are far enough away 
# from the gold core center. 

## CHECKING IF SKIP NVT IS FALSE
if [[ ${skip_nvt} == false ]]; then

    ## PRINTING
    echo "Initiating quick NVT simulation"

    ## DEFINING CURRENT GRO
    # current_gro="${output_gro}"

    ## LOADING GROMACS MPI VERSION
    load_gmx_2016-6_plumed_2-5-1

    ############################
    ### CREATING PLUMED FILE ###
    ############################

    ## READING ATOM
    read -a far_atom_indices_array <<< $(cat ${separate_indices_far_atoms})
    read -a num_bonds_array <<< $(cat ${num_bonds_indices})

## DEFINING PLUMED STRING
plumed_string='''
# DEFINING UNITS
UNITS LENGTH=nm ENERGY=kj/mol TIME=ps

# NP GROUP (LIGANDS)
grp1: GROUP NDX_FILE=index.ndx NDX_GROUP=AUNP

## GETTING CENTER OF MASS
com1: COM ATOMS=grp1 NOPBC

## DISTANCE
'''

    ## DESCRIBING DISTANCE
    read -a current_gro_box_size <<< $(gro_measure_box_size "${current_gro}")

    ## GETTING DEFAULT BOND LENGTH
    default_bond_length="0.15"
    # 0.10
    # "0.15"
    # nm - C-C bond length is 0.154 nm

    ## GETTING HALF BOX SIZE
    # default_kappa="1000" # <-- default kappa spring constant

    ## GETTING KAPPA ARRAY
    declare -a default_kappa_array=("5000" "4000" "3000" "2000" "1000" "500" "100" "50" "10" "5" "1")

## LOOPING
for default_kappa in ${default_kappa_array[@]}; do

    declare -a arguments=()
    declare -a dist_array=()
    declare -a kappa_array=()

## LOOPING THROUGH EACH ATOM
for atom_idx in ${!far_atom_indices_array[@]}; do
    ## CURRENT INDEX
    current_index=${far_atom_indices_array[${atom_idx}]}
    ## DEFINING CURRENT STRING
    current_string="dist${atom_idx}: DISTANCE ATOMS=${current_index},com1 NOPBC" # 
    ## ADDING TO PLUMED STRING
    plumed_string+="${current_string}
"  

    ## DEFINING DISTANCE
    current_dist=$(awk -v x_dim=${current_gro_box_size[0]} 'BEGIN{ printf "%.3f", x_dim/2.0}')
    # current_dist=$(awk -v bl=${default_bond_length} \
    #                    -v num_bonds=${num_bonds_array[${atom_idx}]} \
    #                    -v diam=${diameter} \
    #                    'BEGIN{ printf "%.3f", diam/2.0 +  num_bonds * bl}')

    ## USING EXTREMELY LONG DISTANCES
    # current_dist="100"
    ## ADDING TO ARGUMENTS
    arguments+=("dist${atom_idx}")
    dist_array+=("${current_dist}")
    kappa_array+=("${default_kappa}")
done

## JOINING ARRAY
arg_array_comma=$(join_by "," ${arguments[@]})
dist_array_comma=$(join_by "," ${dist_array[@]})
kappa_array_comma=$(join_by "," ${kappa_array[@]})

plumed_string+="""

## RESTRAINT
restraint: ...
    RESTRAINT
    ARG=${arg_array_comma}
    AT=${dist_array_comma}
    KAPPA=${kappa_array_comma}
...

## PRINTING
PRINT ...
    STRIDE=100
    ARG=*
    FILE=COVAR.dat
... PRINT

ENDPLUMED
"""

## PRINTING
plumed_file="plumed_input.dat"
echo "${plumed_string}" > ${plumed_file}

############################
#### PERFORMING NVT SIM ####
############################

## RUNNING NVT SIM
nvt_nsteps="2000" # 20 ps

## TEMPERATURE
nvt_temp="300"

## TIME STEP
nvt_dt="0.002"

## LAMDBA DIFFERENCES
nvt_delta_lambda="0"
# "0.0002" # For 10 ps
# "0.0004"
# "0.0002"
# "0.00013333333" # ensures nsteps goes to 1
# "0.0002"
# "0.0004" # Lambda will go 1 rapidly until 10 ps
# Default time dt is 0.001
## DEFINING LONG TIMESCALE
want_long_equil=false

if [[ "${want_long_equil}" == true ]]; then
    ## NUMBER OF STEPS
    nvt_nsteps="15000"
    nvt_delta_lambda="0.00006666666" # For 10 ps
fi

## COPYING OVER FILE
nvt_prefix="${output_gro%.gro}_FEP_NVT"
cp -rv "${path2mdp}/${mdp_em_nvt_sim}" "${mdp_em_nvt_sim}"

## EDITING
sed -i "s/_NSTEPS_/${nvt_nsteps}/g" "${mdp_em_nvt_sim}"
sed -i "s/_TEMPERATURE_/${nvt_temp}/g" "${mdp_em_nvt_sim}"
sed -i "s/_DT_/${nvt_dt}/g" "${mdp_em_nvt_sim}"
sed -i "s/_DELTALAMBDA_/${nvt_delta_lambda}/g" "${mdp_em_nvt_sim}"

## RUNNING
## ENERGY MINIMIZING - ADDED FOR NPs
echo -e "--- NVT SIMULATION--- \n"
gmx_mpi grompp -f "${mdp_em_nvt_sim}" \
           -c "${current_gro}" \
           -p "${output_top}" \
           -o "${nvt_prefix}.tpr" \
           -n "${output_ndx}" \
           -maxwarn "${max_warn_index}"
           
## RUNNING NVT SIMULATIONS
# gromacs_mdrun="gmx_mpi mdrun -v -s -ntomp"
gmx_mpi mdrun -v -s -ntomp "${num_nvt_cores}" -deffnm "${nvt_prefix}" -plumed "${plumed_file}"

echo "NVT simulation is complete!"

## CLEANING UP DATA
rm \#*

if [[ ! -e "${nvt_prefix}.gro" ]]; then
    echo "Current kappa: ${default_kappa}"
    echo "Error! NVT simulation did not complete!"
    echo "Check path: ${output_dir}"

else
    ## BREAKING OUT OF FOR LOOP
    break
fi

done

## DEFINING CURRENT GRO
current_gro="${nvt_prefix}.gro"

## CHECKING
if [[ "${allow_failure_of_nvt}" != true ]]; then
    if [[ ! -e "${nvt_prefix}.gro" ]]; then
        echo "Error! NVT simulation did not complete!"
        echo "Check path: ${output_dir}"
        ## DEFINING FAILED FILE
        echo "${output_dir}" >> "${job_script_path}/failed_transfer_ligand_builder.txt"
        ## LEAVE SCRIPT B/C NVT IS NOT WORKING
        exit
    fi
else

    if [[ ! -e "${nvt_prefix}.gro" ]]; then
        echo "====== NVT simulation failed ======"
        echo "Since allow failure of NVT is turned on and NVT sims failed, we are ignoring these NVT simulations."
        echo "Now, we will continue energy minimization without NVT simulations"

        ## SWITCHING GRO
        current_gro="${post_min_gro}"
        echo "Current gro file: ${current_gro}"
        echo "================================"
    fi

fi

## CHECKING IF COMPLETED



## RELOADING OTHER GROMACS
load_swarm_gromacs_2016

else

## DEFINING CURRENT GRO
current_gro="${output_gro}.gro"

## SKIPPING NVT
fi

############################
#### PERFORMING EM STEPS ###
############################

## COULD ALSO RUN EM STEPS
# gmx mdrun -v -s -nt 1 -deffnm sam_FEP_em_1 > sam_FEP_em_1_output.txt 2>&1

## LOOPING AND RUNNING EM
for each_lambda_state in ${lambda_states[@]}; do
    ## PRINTING
    echo ""
    echo "======= Running energy minimization (GNP only): ${each_lambda_state} ======="

    ## CREATING NEW MDP
    mdp_em_FEP_new="${mdp_em_FEP%.mdp}_${each_lambda_state}.mdp"

    ## CREATING NEW GRO FILE
    new_prefix="${output_gro%.gro}_FEP_em_${each_lambda_state}"

    ## DEFINING OUTPUT
    output_txt="${new_prefix}.out"

    ## ENERGY MINIMIZING - ADDED FOR NPs
    "${gromacs_command}" grompp -f "${mdp_em_FEP_new}" \
               -c "${current_gro}" \
               -p "${output_top}" \
               -o "${new_prefix}.tpr" \
               -n "${output_ndx}" \
               -maxwarn "${max_warn_index}" > "${output_txt}" 2>&1

    ## PRINTING
    echo "Output text: ${output_dir}/${output_txt}"
    echo ""

    ## RUNNING ENERGY MINIMIZATION
    ${gromacs_mdrun} "${num_em_cores}" -deffnm "${new_prefix}" >> "${output_txt}" 2>&1

    ## CHECKING IF GRO IS THERE
    if [[ ! -e "${new_prefix}.gro" ]] && [[ "${num_em_cores}" != 10 ]]; then
        echo "Energy minimization did not complete! Turning down energy minimization number of cores to 10!"
        echo "This error may be due to domain decomposition errors!"
        num_em_cores=10
        ## RERUNNING
        ${gromacs_mdrun} "${num_em_cores}" -deffnm "${new_prefix}" >> "${output_txt}" 2>&1
    fi

    ## CHECKING IF GRO IS THERE
    if [[ ! -e "${new_prefix}.gro" ]] && [[ "${num_em_cores}" != 1 ]]; then
        echo "Energy minimization did not complete! Turning down energy minimization number of cores to 1!"
        echo "This error may be due to domain decomposition errors!"
        num_em_cores=1
        ## RERUNNING
        ${gromacs_mdrun} "${num_em_cores}" -deffnm "${new_prefix}" >> "${output_txt}" 2>&1
    fi

    ## REDEFINING GRO
    current_gro="${new_prefix}.gro"

done

## PRINTING EM IS TRUE
echo "Energy minimization is complete!"

if [ -e "${current_gro}" ]; then
    echo "Gro file exists: ${current_gro}"
else
    echo "Error! Gro file does not exist: ${current_gro}"
    echo "Double check energy minimization protocol!"
fi

###########################################################################
### ADDING GOLD ATOMS (WHICH WERE OMITTED DURING TRANSFER FOR SPEED-UP) ###
###########################################################################

echo "======================================================="
echo "Step 4: Adding back gold atoms within hollow shell"
echo "======================================================="
echo ""

## DEFINING MINIMIZED GRO
minimized_gro="${current_gro}"

## DEFINING DEFAULT GOLD PICLE
default_gold_pickle="skip_gold_info.pickle"

## DEFINING OUTPUT GRO FILE
added_gold_gro="${minimized_gro%.gro}_with_gold.gro"

## RUNNING PYTHON SCRIPT TO ADD GOLD ATOMS
if [ -e "${output_dir}/${default_gold_pickle}" ]; then
    ## DEFINING OUTPUT GRO
    after_added_gold_gro="${added_gold_gro%.gro}_resized.gro"
    ## RUNNING PYTHON SCRIPT TO ADD GOLD ATOMS
    python "${py_add_gold_script}" \
                --path_pickle "${output_dir}/${default_gold_pickle}" \
                --path_input_gro "${output_dir}/${minimized_gro}" \
                --path_output_gro "${output_dir}/${added_gold_gro}" \
                --path_topology "${output_dir}/${output_top}"

    echo "** EXPANDING BOX LENGTH TO SOLUTE-BOX DISTANCE: ${dist_to_edge} **"
    ${gromacs_command} editconf -f "${output_dir}/${added_gold_gro}" \
                                -o "${after_added_gold_gro}" \
                                -d "${dist_to_edge}" \
                                -bt "cubic"

else
    ## DEFINING OUTPUT PATH GRO
    after_added_gold_gro=${minimized_gro}

fi

################################
### ADDING SULFUR-GOLD BONDS ###
################################

echo "======================================================="
echo "Step 5: Binding sulfur-sulfur and sulfur-gold atoms"
echo "======================================================="
echo ""
## ADDING GOLD SULFUR BOND
## RUNNING PYTHON SCRIPT TO BIND THE SULFUR TO GOLDS
echo "----- RUNNING ${bind_gold_sulfur_script} SCRIPT ------"
python ${py_bind_gold_sulfur_script} --names "${ligand_names}" \
                                     --ifold "${output_dir}"  \
                                     --igro "${after_added_gold_gro}" \
                                     --top "${output_top}" \
                                     --ores "${output_file_name}" \
                                     --oitp "${output_itp}" \
                                     --geom "${shape_type}" \
                                     --assembly "${assembly_type}" \
                                     --want_updated_aunp_itp \
                                     --sulfur_ndx "${output_sulfur_index}"

##############################################
### RUNING FULL EM ON GNPS WITHOUT SOLVENT ###
##############################################

echo "======================================================="
echo "Step 6: Energy minimizing gold core with bonds"
echo "======================================================="
echo ""

## ENERGY MINIMIZING - ADDED FOR NPs
echo -e "--- FULL ENERGY MINIMIZATION (GNP ONLY) --- \n"
${gromacs_command} grompp -f "${mdp_em}" \
           -c "${after_added_gold_gro}" \
           -p "${output_top}" \
           -o "${output_gro%.gro}_initial_em_with_bonds.tpr" \
           -n "${output_ndx}" \
           -maxwarn "${max_warn_index}"
echo "Running EM with water, check path: ${output_gro%.gro}_initial_em_with_bonds.out"
${gromacs_mdrun} "${num_em_cores}" \
    -deffnm "${output_gro%.gro}_initial_em_with_bonds" > "${output_gro%.gro}_initial_em_with_bonds.out" 2>&1

## RE-SIZE
${gromacs_command} editconf -f "${output_gro%.gro}_initial_em_with_bonds.gro" \
             -o "${output_gro%.gro}_with_bonds_resized.gro" \
             -d "${dist_to_edge}" \
             -bt "${output_box_type}"


## CHECKING IF FILE EXISTS
if [[ -z "${output_gro%.gro}_with_bonds_resized.gro" ]]; then
    echo "Error! Gro file does not exist: ${output_gro%.gro}_with_bonds_resized.gro"
    echo "Pausing here so you can see it, output dir: ${output_dir}"
    sleep 3
fi

## CHECKING IF YOU WANT NANOPARTICLE ENERGY MINIMIZATION ONLY
if [[ "${want_debug_np_em}" != true ]]; then

############################
### SOLVATING WITH WATER ###
############################

echo "======================================================="
echo "Step 7: Solvating system with water"
echo "======================================================="
echo ""

### SOLVATING WITH WATER
${gromacs_command} solvate -cs "spc216.gro" \
            -cp "${output_gro%.gro}_with_bonds_resized.gro" \
            -p "${output_top}" \
            -o "${output_gro%.gro}_solvated.gro" &> /dev/null # Silencing

# cp ${output_gro} ${output_gro%.gro}_solvated.gro

## REMOVING RESIDUES WITHIN THE CENTERS
if [ "${shape_type}" == "hollow" ]; then
    echo "*** Running ${py_remove_atoms_file} ***"
    python ${py_remove_atoms_file} \
                --rad ${radius} \
                --ofold ${output_dir} \
                --ogro "${output_gro%.gro}_solvated.gro" \
                --otop "${output_top}"
fi

## NEUTRALIZING IF NECESSARY
echo -e "--- NEUTRALIZING CHARGES ---\n"
echo "Creating TPR file for balancing of charge"
${gromacs_command} grompp -f ${mdp_em} \
           -c ${output_gro%.gro}_solvated.gro \
           -p $output_top \
           -o ${output_gro%.gro}_solvated.tpr \
           -maxwarn ${max_warn_index}

## RUNNING GENION
${gromacs_command} genion -s ${output_gro%.gro}_solvated.tpr \
           -p $output_top \
           -o ${output_gro%.gro}_solvated.gro \
           -neutral << INPUT
SOL
INPUT

######################################
### CREATING INDEX AFTER SOLVATION ###
######################################
## DEFINING NEW INDEX
new_ndx="${output_ndx%.ndx}_after_solvate.ndx"
## RUNNING GROMACS INDEX FUNCITON
"${gromacs_command}" make_ndx -f "${output_gro%.gro}_solvated.gro" -o "${new_ndx}" << INPUTS
q
INPUTS

#########################
### ENERGY MINIMIZING ###
#########################

echo "======================================================="
echo "Step 8: Energy minimizing system with water"
echo "======================================================="
echo ""

echo -e "--- ENERGY MINIMIZATION (GNP WITH WATER) --- \n"
${gromacs_command} grompp -f ${mdp_em} \
                          -c ${output_gro%.gro}_solvated.gro \
                          -p ${output_top} \
                          -o ${output_gro%.gro}_em.tpr \
                          -maxwarn ${max_warn_index}
## RUNNING EM
echo "Running EM with water, check path: ${output_dir}/${output_gro%.gro}_em.out"
${gromacs_mdrun} "${num_em_cores}" -deffnm "${output_gro%.gro}_em" > "${output_gro%.gro}_em.out" 2>&1

# cp ${output_gro%.gro}_solvated.gro ${output_gro%.gro}_em.gro


### REMOVING EXCESS WATER IF YOU HAVE A HOLLOW SHELL
if [ "${shape_type}" == "hollow" ]; then
    ## MAKING THE MOLECULES WHOLE
${gromacs_command} trjconv -s ${output_gro%.gro}_em.tpr \
            -f "${output_gro%.gro}_em.gro" \
            -o "${output_gro%.gro}_em.gro" \
            -pbc whole << INPUT
System
INPUT
    
    echo "*** Running ${py_remove_atoms_file} ***"
    python "${py_remove_atoms_file}" \
        --rad "${radius}" \
        --ofold "${output_dir}" \
        --ogro "${output_gro%.gro}_em.gro" \
        --otop "${output_top}"
fi

#########################
### SUBMISSION SCRIPT ###
#########################

## CHECKING IF EXISTING
if [[ -e "${output_gro%.gro}_em.gro" ]]; then

    echo "======================================================="
    echo "Step 9: Creating submission script"
    echo "======================================================="
    echo ""

    ## DEFINING MDRUN COMMANDS
    mdrun_command="gmx mdrun -nt"
    mdrun_command_suffix=""
    gromacs_command="gmx"

    ## DEFINING MAX WARN
    maxwarn="5"

    ## COPYING RC
    cp -r "${PATH_GENERAL_RC}" "${output_dir}"

    ## DEFINING PATH OUTPUT SUBMISSION
    path_output_submit="${output_dir}/${output_submit}"
    path_output_run="${output_dir}/${output_run_script}"

    ## CREATING JOB SCRIPT FILE
    echo -e "--- CREATING SUBMISSION FILE: $submitScript ---"
    cp "${PATH2SUBMISSION}/${submitScript}" "${path_output_submit}"
    cp "${PATH2SUBMISSION}/${run_script}" "${path_output_run}"

    ## EDITING SUBMISSION SCRIPT
    sed -i "s/_JOB_NAME_/${output_dirname}/g" "${path_output_submit}"
    sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${path_output_submit}"
    sed -i "s/_RUNSCRIPT_/${output_run_script}/g" "${path_output_submit}"
    sed -i "s/_MDRUNCOMMAND_/${mdrun_command}/g" "${path_output_submit}"
    sed -i "s/_MDRUNCOMMANDSUFFIX_/${mdrun_command_suffix}/g" "${path_output_submit}"
    sed -i "s/_GROMACSCOMMAND_/${gromacs_command}/g" "${path_output_submit}"

    ## EDITING RUN SCRIPT
    sed -i "s/_RCFILE_/${GENERAL_RC_NAME}/g" "${path_output_run}"
    sed -i "s/_MAXWARN_/${maxwarn}/g" "${path_output_run}"
    sed -i "s/_OUTPUTPREFIX_/${output_file_name}/g" "${path_output_run}"
    sed -i "s/_INITIALPREFIX_/${output_gro%.gro}_em/g" "${path_output_run}"
    sed -i "s/_EQUILMDPFILE_/${mdp_equil}/g" "${path_output_run}"
    sed -i "s/_PRODMDPFILE_/${mdp_prod}/g" "${path_output_run}"
    sed -i "s/_TOPFILE_/${output_top}/g" "${path_output_run}"
    sed -i "s/_INDEXFILE_/${new_ndx}/g" "${path_output_run}"

    ## CLEANING UP DIRECTORY FILE
    echo -e "--- CLEANING UP DIRECTORY FILE ---"
    setup_dir="setup_files"
    mkdir -p ${setup_dir}

    ## MOVING FILES
    checkNMoveFile "${output_gro}" ${setup_dir}
    checkNMoveFile "${output_gro%.gro}_solvated.gro" ${setup_dir}
    mv \#* setup_files

    ## CLEANING UP FEP EM FILES
    mv ${output_gro%.gro}_FEP_em_* \
       "${output_gro%.gro}_with_bonds_resized.gro" \
       ${mdp_em_FEP%.mdp}_* \
       ${added_gold_gro} \
       ${output_gro%.gro}_initial_em_with_bonds* \
       "${nvt_prefix}"* \
       ${output_gro%.gro}_initial_FEP_em* \
       ${setup_dir}

    ## WRITING OUTPUT DIRECTORY TO JOB LIST FILE
    echo "${output_dir}/${output_submit}" >> "${JOB_SCRIPT}"

else
    echo "Error! ${output_gro%.gro}_em.gro does not exist! The job will not start without a completely"
    echo "energy minimized system. Please double-check Step 8."

    ## DEFINING FAILED FILE
    echo "${output_dir}" >> "${job_script_path}/failed_transfer_ligand_builder.txt"


fi

## PRINTING
echo "Check directory: ${output_dir}"
echo "---------------------------------"


else
    echo "Since want_debug_np_em is set to true, we are ignoring all other steps!"

fi

## END IF LOOP FOR CHECKING 
fi