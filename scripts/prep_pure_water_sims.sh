#!/bin/bash

# prep_pure_water_sims.sh
# This script generates pure water sims as a reference state
# This code uses the nanoparticle NPT ensemble MPD files


#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="../bin/nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${script_dir}/${global_file_name}"


### FUNCTION TO GENERATE NAME FOR PURE WATER SIM
# This function gets the pure water name. 
# INPUTS:
#	$1: water box size
#	$2: water model
#	$3: temperature of the system in Kelvins
# OUTPUTS:
# 
function get_name_pure_water () {
	## DEFINING INPUTS
	water_box_size_="$1"
	water_model_="$2"
	temp_="$3"
	## DEFINING PREFIX
	prefix_="wateronly"

	## DEFINING OUTPUTNAME
	output_name_="${prefix_}-${water_box_size_}_nm-${water_model_}-${temp_}_K"

	## PRINTING OUTPUT
	echo "${output_name_}"

}


### DEFINING SIZE OF THE WATER BOX
water_box_size="6" # nm

#########################
### DEFAULT VARIABLES ###
#########################
## TEMPERATURE
temp="300.00" # Kelvins

### DEFINING WATER MODEL
water_model="tip3p"

### DEFINING FORCEFIELD
forcefield="${FORCEFIELD}"

## DEFINING DIR NAME
parent_dir_name="PURE_WATER_SIMS"

## MAX WARN
maxwarn="5"

## SUBMISSION DETAILS
num_cores="28"
gromacs_command="gmx"
mdrun_command="gmx mdrun -nt"
mdrun_suffix=""

####################
### NOMENCLATURE ###
####################

## GETTING OUTPUT NAME
output_name="$(get_name_pure_water ${water_box_size} ${water_model} ${temp})"

## DEFINING PREFIX
prefix="water"

##############################
### MDP, TOP, SUBMIT FILES ###
##############################

## ENERGY MINIMIZATION
em_mdp_file=""minim_sam_gmx5.mdp""
## NPT EQUILIBRATION
equil1_mdp_file="npt_double_equil_gmx5_charmm36.mdp"
equil2_mdp_file="npt_double_prod_gmx5_charmm36_10ps.mdp"

## NVT PRODUCTION
prod_mdp_file="nvt_double_prod_gmx5_charmm36_frozen_gold.mdp"

## DEFINING TIMES
equil1_time="2500000" # 5 ns equilibration
equil2_time="2500000" # 5 ns equilibration
prod_time="25000000" # 50 ns production

## DEFINING TOP FILE
top_file="water.top"

## DEFINING RUN / SUBMIT
run_file="run_pure_water_sims.sh"
submit_file="submit_pure_water_sims.sh"

## DEFINING OUTPUT RUN/SUBMIT
output_run="run.sh"
output_submit="submit.sh"

#######################
#### DEFINING PATHS ###
#######################

## PATH TO SIM
path_to_sim="${PATH2SIM}/${parent_dir_name}/${output_name}"

## PATH TO FF
path_forcefield="${PATH_FORCEFIELD}/${forcefield}"

## PATH TO TOP
path_top="${INPUT_TOPOLOGY_PATH}/${top_file}"
## PATH TO MDP
path_mdp="${PATH_MDP_CHARMM36_SPHERICAL}"
## PATH TO SUBMISSION
path_submit="${PATH2SUBMISSION}"

###################
### MAIN SCRIPT ###
###################

## CREATING DIRECTORY
create_dir "${path_to_sim}" -f

## GOING INTO DIRECTORY
cd "${path_to_sim}"

## COPYING FILES
echo "============================"
echo "Main path: ${path_to_sim}"
echo "============================"
echo "--> Copying forcefield files and topology"
cp -r "${path_forcefield}" "${path_to_sim}"

## COPYING TOPOLOGY
cp -r "${path_top}" "${path_to_sim}/${prefix}.top"

## COPYING MDP FILES
cp -r "${path_mdp}"/{${em_mdp_file},${equil1_mdp_file},${equil2_mdp_file},${prod_mdp_file}} "${path_to_sim}"

## SUBMISSION
cp -r "${path_submit}/${submit_file}" "${output_submit}"
cp -r "${path_submit}/${run_file}" "${output_run}"

## USING GMX SOLVATE
echo "--> Solvating water box with ${water_box_size} box size"
gmx solvate -cs "spc216.gro" \
			-p "${prefix}.top" \
			-o "${prefix}.gro" \
			-box "${water_box_size}" "${water_box_size}" "${water_box_size}" \
			&> /dev/null # Silencing



#####################
### EDITING FILES ###
#####################
echo "--> Editing files..."
## TOPOLOGY
sed -i "s#_FORCEFIELD_#${forcefield}#g" "${prefix}.top"
sed -i "s#_WATER_MODEL_#${water_model}#g" "${prefix}.top"

## EDITING MDP FILE - EQUIL 1
sed -i "s#NSTEPS#${equil1_time}#g" "${equil1_mdp_file}"
sed -i "s#TEMPERATURE#${temp}#g" "${equil1_mdp_file}"

## EDITING MDP FILE - EQUIL 2
sed -i "s#NSTEPS#${equil2_time}#g" "${equil2_mdp_file}"
sed -i "s#TEMPERATURE#${temp}#g" "${equil2_mdp_file}"

## EDITING MDP FILE - PROD
sed -i "s#NSTEPS#${prod_time}#g" "${prod_mdp_file}"
sed -i "s#TEMPERATURE#${temp}#g" "${prod_mdp_file}"
## TURNING OFF FREEZE GROUP ITEMS
mdp_comment_out_specific_flags "${prod_mdp_file}" "freezegrps"
mdp_comment_out_specific_flags "${prod_mdp_file}" "freezedim"

### SUBMISSION
sed -i "s#_JOB_NAME_#${output_name}#g" "${output_submit}"
sed -i "s#_NUMBER_OF_CORES_#${num_cores}#g" "${output_submit}"
sed -i "s#_MDRUNCOMMAND_#${mdrun_command}#g" "${output_submit}"
sed -i "s#_MDRUNCOMMANDSUFFIX_#${mdrun_suffix}#g" "${output_submit}"
sed -i "s#_GROMACSCOMMAND_#${gromacs_command}#g" "${output_submit}"
sed -i "s#_RUNSCRIPT_#${output_run}#g" "${output_submit}"

### RUN FILE
sed -i "s#_INPUTTOPFILE_#${prefix}.top#g" "${output_run}"
sed -i "s#_OUTPUTPREFIX_#${prefix}#g" "${output_run}"
sed -i "s#_EQUIL1_MDPFILE_#${equil1_mdp_file}#g" "${output_run}"
sed -i "s#_EQUIL2_MDPFILE_#${equil2_mdp_file}#g" "${output_run}"
sed -i "s#_PROD_MDPFILE_#${prod_mdp_file}#g" "${output_run}"
sed -i "s#_MAXWARN_#${maxwarn}#g" "${output_run}"

#########################
### ENERGY MINIMIZING ###
#########################
echo "--> Energy minimizing ..."
## CREATING GROMPP FILE
gmx grompp -f "${em_mdp_file}" \
		   -c "${prefix}.gro" \
		   -p "${prefix}.top" \
		   -o "${prefix}_em" \
		   --maxwarn "${maxwarn}"
## RUNNING ENERGY MINIMIZATION
gmx mdrun -v \
		  -nt 1 \
		  -deffnm "${prefix}_em"

### CHECKING IF COMPLETED
if [ -e "${prefix}_em.gro" ]; then
	echo "--> Preparation complete!"
	echo "Job is now added to ${JOB_SCRIPT}"
	echo "${path_to_sim}/${output_submit}" >> "${JOB_SCRIPT}"
else
	echo "Error! Gro file does not exist: ${prefix}_em.gro"
	echo "Check path: ${path_to_sim}"
fi







