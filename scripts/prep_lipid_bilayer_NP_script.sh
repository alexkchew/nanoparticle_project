#!/bin/bash

# prep_lipid_bilayer_NP_script.sh
# The purpose of this script is to prepare the lipid bilayer and nanoparticle simulations. We will need to take several steps to ensure that the nanoparticle is correctly interacting with the lipid bilayer.

## USAGE EXAMPLE: bash prep_lipid_bilayer_NP_script.sh

# Created on: 11/02/2018
# Written by: Alex K. Chew (alexkchew@gmail.com)

## ASSUMPTIONS:
#   We assume you are using the CHARMM36 force field and generated the itp, pdb, and prm file from the CGENFF software
#   We also assume you have lipid bilayers developed by the CHARMM-gui membrane builder.

## ALGORITHM
#   1 - Select the lipid bilayer that is desired
#   2 - Extract last frame of the lipid bilayer
#   3 - Measure lipid bilayer to gold core distance.
#     - Measure thickness of bilayer + lipid membrane COM
#     - Measure water thickness in the z-dimension
#   4 - Select nanoparticle system
#   5 - Extract the nanoparticle by itself
#     - Generate nanoparticle at the center with 0 distance to the edge
#     - Then, compute the delta Z according to this system (simply the z-dimension)
#   6 - Estimate the box size of NP + water     
#   6 - Center the nanoparticle in the box. Move according to location to bilayer
#   7 - Add the nanoparticle with water on top of the lipid bilayer
#   8 - Add an additional 

## UPDATES:
#   20120107 - updated code to include additional space for NP + lipid bilayers

#################################################
### LOADING PRE-DEFINED VARIABLES / FUNCTIONS ###
#################################################
source "../bin/nanoparticle_rc.sh"
print_script_name

#########################
### IMPORTANT SCRIPTS ###
#########################
## PYTHON CODE TO EXTRACT MEMBRANE INFORMATION
python_lipid_membrane_info_extraction="${MDDESCRIPTOR}/application/np_lipid_bilayer/extract_lipid_bilayer_info.py"

## DEFINING PYTHON PATH
py_correct_gro="${MDBUILDER}/core/fix_residues_gro_top.py"

#######################
### INPUT VARIABLES ###
#######################

## VARIABLES
#   $1: nanoparticle main directory to extract from
#   $2: main_dir_name - main directory output location
#   $3: ligand name
#   $4: temperature
#   $5: lipid type
#   $6: lipid size
#   $7: true/false for debugging
#   $8: np-lipid membrane intitial distance

## RUN INFORMATION
num_cores="28"

## DEFINING EM NUMBER OF CORES
em_num_cores="28"

## DEFINING MAIN DIRECTORY TO GET THE INFORMATION FOR NP
np_main_dir_name="$1"
# "191221-Rerun_all_EAM_models_1_cutoff"
np_main_dir_path="${PATH2SIM}/${np_main_dir_name}"
# PATH2NPLMSIMS

## DEFINING MAIN WORKING DIRECTORY
main_dir_name="$2"
# "20200107-NP_lipid_bilayer_196"

## DEFINING NANOPARTICLE INFORMATION
np_shape="EAM"
np_diameter="2"

## DEFINING LIGAND NAME
ligand_name="$3"
# "ROT001" # "ROT001"

## DEFINING TRIAL NUMBER
trial="1"



## DEFINING SYSTEM PARAMETERS
temperature="${4-300.00}"

## DEFINING CENTERING GROUPS
np_residue_name="sam"
np_center_group="AUNP"

## DEFINING ADDITIONAL Z WATER DISTANCE
z_water_dist="2" # nm, distance between nanoparticle and top of the box

## SEEING IF YOU WANT TO DEBUG
want_debug="${7-false}"

## DEFINING DEFAULT NP TO LIPID MEMBRANE DISTANCE
np_lipid_membrane_dist="${8-6}"
# "5" # nm

## DEFINING IF YOU WANT MODIFIED FORCEFIELD
modified_ff="${9-false}"

## FORCE FIELD
if [[ "${modified_ff}" == true ]]; then
  echo "Generating modified force field parameters"
  forcefield="${MODIFIED_FF}"
else
  forcefield="${FORCEFIELD}" # Force Field (opls-aa or charmm36-nov2016.ff)
fi
# true if you want quick runs
want_em=false
# false
# true
# false
# true if you want energy minimizations

#########################
### PULLING VARIABLES ###
#########################

## DEFINING PULL CONSTANT
pull_constant="2000" # kJ/mol/nm2, as defined by previous papers
equil_dt="0.002" # ps (2 fs)

## DEFINING EQUIL 1 DETAILS
if [[ "${want_debug}" == true ]]; then
    ## LOWER STEPS
    num_steps_equil_1="10000" # 1000
    num_steps_equil_2="1000" # testing
    pull_rate="0.01" # Fast pull rate
else
    num_steps_equil_1="5000000" # 10 ns NPT equilibration
    num_steps_equil_2="5000000" # 10 ns NPT equilibration
    # "25000000" # 50 ns equilibration
    pull_rate="0.0005" # nm / ps -- PRODUCTION
fi

## DEFINING TEMPERATURE
equil_temp="${temperature}"

## DEFINING MD RUN COMMAND
mdrun_command="gmx mdrun -nt"

## DEFINING SUFFIX
mdrun_command_suffix=""

#########################
### DEFAULT VARIABLES ###
#########################

## SCALING FACTOR FOR SOLVATION
scale_factor="0.60"
# "0.57"
# "0.60"
# "0.70" # Scaling factor default: 0.57 <-- depreciated

## DEFINING GOLD RESIDUE NAME
gold_residue_name="AUNP"

## DEFINING WATER IN LIPID MEMBRANE
lipid_mem_water_res_name="TIP3"

## DEFINING WATER RESIDUE NAME
water_res_name="SOL" # water residue name for NP project
water_atom_to_res="3" # 3 atom per residue

## DEFINING SLEEP TIME
sleep_time="1"
# "0"
# "1"

## DEFINING MAXWARN
num_max_warn="5"

###################
### INPUT FILES ###
###################
input_np_prefix="sam_prod"
input_np_gro_file="${input_np_prefix}.gro"
input_np_tpr_file="${input_np_prefix}.tpr"
input_np_itp_file="sam.itp"
input_np_top_file="sam.top"

### MDP FILES
em_mdp="em.mdp"
# equil_npt_mdp="equil_npt.mdp"
equil_npt_mdp_1="np_lipid_membrane_equil_npt.mdp"
equil_npt_mdp_2="np_lipid_membrane_equil_freeze.mdp"
# "prod_npt.mdp"
## DEFINING MDP INPUT AND OUTPUT
mdp_pull_input_file="pulling.mdp"

## DEFINING MDP PUSH
mdp_push_input_file="pushing.mdp"

## SUBMISSION SCRIPT
input_submit_file="submit_run_np_lipid_bilayer_equil_and_pull.sh"
input_run_file="run_np_lipid_bilayer_equil_and_pull.sh"
input_pull_script="pull_np_to_lipid_bilayer.sh"

## DEFINING OUTPUT PREFIX
output_prefix="nplm" # nanoparticle lipid membrane

## OUTPUT FILES
output_top="sam.top"
# ${output_prefix}

## OUTPUT SUBMISSION NAME
output_submit_name="submit.sh"
output_run_name="run.sh"

## DEFINING SUBMISSION SCRIPT
submit_pull_script_name="pull_np_to_lipid_bilayer.sh"
submit_push_script_name="push_np_from_lipid_bilayer.sh"

## DEFINING OUTPUT FILE
mdp_pull_output_file="pulling_edited.mdp"
mdp_push_output_file="pushing_edited.mdp"

######################
### DEFINING PATHS ###
######################
## MDP FILE INFORMATION
mdp_parent_path="${INPUT_MDP_PATH}/charmm36"

## MDP FILE PATH
mdp_file_path="${mdp_parent_path}/lipid_bilayers"

## DEFINING PATH TO SUBMISSION
input_submit_path="${PATH2SUBMISSION}/${input_submit_file}"
input_run_path="${PATH2SUBMISSION}/${input_run_file}"

## DEFINING PATH
submit_pull_script_path="${PATH_LIPID_MEMBRANE_SCRIPTS}/${submit_pull_script_name}"
submit_push_script_path="${PATH_LIPID_MEMBRANE_SCRIPTS}/${submit_push_script_name}"

#########################################
### STEP 1: LIPID MEMBRANE EXTRACTION ###
#########################################

## DEFINING LIPID NAME
lipid_name="${5:-DOPC}"

## DEFINING SIZE
lipid_size="${6-144}"

## DEFINING TMEPERATURE
lipid_membrane_temp="300.00"

## DEFINING FOLDER NAME
lipid_membrane_folder=$(get_lipid_membrane_folder_name "${lipid_name}" \
                                                       "${lipid_membrane_temp}" \
                                                       "${lipid_size}" 
                                                       )

## DEFINING GRO AND TPR
lipid_membrane_prefix="step7_10"
input_lipid_membrane_gro="${lipid_membrane_prefix}.gro" # Replace with equil gro later
input_lipid_membrane_tpr="${lipid_membrane_prefix}.tpr"
input_lipid_membrane_xtc="${lipid_membrane_prefix}.trr"

## DEFINING PATH TO LIPID MEMBRANE
path_lipid_membrane="${INPUT_LIPID_MEMBRANE}/${lipid_membrane_folder}/gromacs"

## CHECKING IF EXISTS
stop_if_does_not_exist "${path_lipid_membrane}"

## PRINTING
echo -e "\n--------------------------------------------"
echo "----- STEP 1: Lipid membrane extraction ----"
echo "--------------------------------------------"
echo "Lipid membrane folder: ${lipid_membrane_folder}"
echo "Full path to folder: ${path_lipid_membrane}"
echo "GRO file: ${input_lipid_membrane_gro}"
echo "TPR file: ${input_lipid_membrane_tpr}"

## HARD-CODED LIPID MEMBRANE TOPOLOGY INFORMATION
lipid_membrane_top_file="topol.top"
lipid_membrane_toppar="toppar"
## PARMAETER FILES
lipid_membrane_prm_file="${lipid_membrane_toppar}/charmm36.itp"
lipid_membrane_itp_file="${lipid_membrane_toppar}/${lipid_name}.itp"
lipid_membrane_water_itp_file="${lipid_membrane_toppar}/TIP3.itp"

## GOING INTO DIRECTORY AND COMPUTING DETAILS OF LIPID MEMBRANE
cd "${path_lipid_membrane}"

## DEFINING ANALYSIS DIRECTORY DETAILS
lipid_membrane_analysis="analysis"
lipid_membrane_text_file="lipid_membrane_info.txt"
lipid_membrane_dens_image="lipid_membrane_density.png"

## CHECKING IF ANALYSIS DIRECTORY EXISTS
if [[ ! -e "${lipid_membrane_analysis}" ]]; then
    mkdir "${lipid_membrane_analysis}"
fi

## GETTING LIPID MEMBRANE RESNAME
lipid_membrane_resname="$(itp_get_resname ${lipid_membrane_itp_file})"

## DEFINING INDEX
index_file="analysis.ndx"

## DEFINING STRING FOR HEAD GROUP
head_group_string="r ${lipid_membrane_resname} & a P"

## DEFINING ARRAY TO OUTPUT XVG FILES
declare -a residue_for_density=("${lipid_membrane_resname}" \
                                "${head_group_string}" \
                                "${lipid_mem_water_res_name}" \
                                )
                               
## DEFINING NAME FOR XVG FILE
declare -a output_file_name=("lipid_membrane.xvg" \
                             "head_group.xvg" \
                             "water.xvg")

## GENERATING DENSITY OF EACH GROUP
echo "Creating index file: ${index_file} [OUTPUT SUPPRESSED]"
echo "Path to index file: ${path_lipid_membrane}/${index_file}"
gmx make_ndx -f "${input_lipid_membrane_tpr}" -o "${index_file}" >/dev/null 2>&1 << INPUTS
keep 0
keep 1
r ${lipid_membrane_resname}
${head_group_string}
r ${lipid_mem_water_res_name}
q
INPUTS

## LOOPING THROUGH EACH FILE
for idx in $(seq 0 $((${#residue_for_density[@]}-1)) ); do
    
    ## DEFINING CURRENT NAME
    current_name="${residue_for_density[idx]}"
    output_name="${output_file_name[idx]}"
    
    ## PRINTING
    echo "Working on generating ${output_name}"

## COMPUTING DENSITY
if [[ ! -e "${lipid_membrane_analysis}/${output_name}" ]]; then
gmx density -f "${input_lipid_membrane_xtc}" -s "${input_lipid_membrane_tpr}" -n "${index_file}" -o "${lipid_membrane_analysis}/${output_name}" -d Z << INPUTS
${idx}
INPUTS
fi

done

## REMOVING ANY EXTRA FILES
rm -f ${lipid_membrane_analysis}/\#*

## GETTING XVG INPUT
xvg_input=$(join_array_to_string , "${output_file_name[@]}")

## DEFINING PATH TO TEXT
path_lipid_membrane_text_file="${path_lipid_membrane}/${lipid_membrane_analysis}/${lipid_membrane_text_file}"

## RUNNING PYTHON CODE FOR ANALYSIS
if [[ ! -e "${path_lipid_membrane_text_file}" ]]; then
python3.6 ${python_lipid_membrane_info_extraction} --path "${path_lipid_membrane}/${lipid_membrane_analysis}" \
                                                   --xvg "${xvg_input}" \
                                                   --image "${lipid_membrane_dens_image}" \
                                                   --text "${lipid_membrane_text_file}"
                                                   
fi

## EXTRACTING TEXT FILE
membrane_center_z_pos="$(grep "Membrane_center_position_nm" "${path_lipid_membrane_text_file}" | awk '{print $2}')"
membrane_half_thickness="$(grep "Membrane_half_thickness" "${path_lipid_membrane_text_file}" | awk '{print $2}')"
membrane_full_thickness="$(grep "Membrane_thickness" "${path_lipid_membrane_text_file}" | awk '{print $2}')"

## PRINTING
echo "Membrane full thickness: ${membrane_full_thickness} nm"
echo "Membrane half thickness: ${membrane_half_thickness} nm"
echo "Membrane center z position: ${membrane_center_z_pos} nm"
sleep "${sleep_time}"

#######################################
### STEP 2: NANOPARTICLE EXTRACTION ###
#######################################

## CHECKING FORCEFIELD SUFFIX
forcefield_suffix=$(check_ff_suffix "${forcefield}")

## DEFINING NANOPARTICLE DIRECTORY
np_dir="$(np_get_output_dirname "${np_shape}" "${temperature}" "${np_diameter}" "${ligand_name}" "${forcefield_suffix}" "${trial}")"
## PATH TO NP DIRECTORY
np_dir_path="${np_main_dir_path}/${np_dir}"

## CHECKING IF EXISTS
stop_if_does_not_exist "${np_dir_path}"

## GOING INTO DIRECTORY
cd "${np_dir_path}"

## FINDING INPUT LIGAND ITP FILE
input_ligand_itp_name="${ligand_name}.itp"
input_ligand_itp_path="${np_dir_path}/${input_ligand_itp_name}"

## GETTING LIGAND RESIDUE NAME
ligand_residue_name="$(itp_get_resname "${input_ligand_itp_path}")"

## DEFINING INDEX FILE
np_gold_with_ligand_index="gold_with_ligand.ndx"

## MAKING GOLD INDEX WITH LIGAND
make_ndx_gold_with_ligand "${input_np_tpr_file}" \
                          "${np_gold_with_ligand_index}" \
                          "${ligand_residue_name}" \
                          "${gold_residue_name}"
## DEFINING GOLD WITH LIGAND
gold_with_lig_selection="GNP_LIGANDS"
# "${gold_residue_name}_${ligand_residue_name}"

echo -e "\n---------------------------------"
echo "----- STEP 2: NP extraction -----"
echo "---------------------------------"
echo "Working on NP directory: ${np_dir}"
echo "Gold and ligand index file: ${np_gold_with_ligand_index}"
echo -e "Full path: ${np_dir_path}"
sleep "${sleep_time}"

######################################################################
### STEP 3: CREATING DIR AND EXTRACTING LIPID MEMBRANE INFORMATION ###
######################################################################

echo -e "\n--------------------------------------------------"
echo "--- STEP 3: Lipid membrane information extract ---"
echo "--------------------------------------------------"

## DEFINING OUTPPUT NAME
output_dir_name="$(get_np_lipid_membrane_output_name \
                        ${lipid_name} \
                        ${lipid_size} \
                        ${temperature} \
                        ${np_lipid_membrane_dist} \
                        ${pull_rate} \
                        ${pull_constant} \
                        ${np_shape} \
                        ${np_diameter} \
                        ${ligand_name} \
                        ${trial}
                        )"

# "NPLM-${lipid_membrane_folder}-${np_dir}"
path_output_dir="${PATH2NPLMSIMS}/${main_dir_name}/${output_dir_name}"

## CHECKING IF DIRECTORY EXISTS
create_dir "${path_output_dir}" -f

## GOING INTO DIRECTORY
cd "${path_output_dir}"

## DEFINING OUTPUT PREFIX
output_lipid_membrane_prefix="lipid_membrane"

## COPYING OVER GRO FILE
cp "${path_lipid_membrane}/${input_lipid_membrane_gro}" "${path_output_dir}/${output_lipid_membrane_prefix}.gro"
cp "${path_lipid_membrane}/${input_lipid_membrane_tpr}" "${path_output_dir}/${output_lipid_membrane_prefix}.tpr"

## TOPOLOGY FILE
cp -r "${path_lipid_membrane}/"${lipid_membrane_toppar} "${path_output_dir}"
cp -r "${path_lipid_membrane}/${lipid_membrane_top_file}" "${path_output_dir}/${output_lipid_membrane_prefix}.top"

## GETTING LIPID MEMBRANE BOX SIZE
read -a lipid_membrane_box_size <<<$(read_gro_box_lengths "${output_lipid_membrane_prefix}.gro")

## COMPUTING WATER HEIGHT
water_height=$(awk -v z_dim=${lipid_membrane_box_size[2]} -v bilayer_height=${membrane_full_thickness} 'BEGIN{ printf "%.3f", (z_dim - bilayer_height)/2 }')

## PRINTING
echo "Output directory: ${output_dir_name}"
echo "Full path: ${path_output_dir}"
echo "Output prefix: ${output_lipid_membrane_prefix}"
echo "Lipid membrane box size: ${lipid_membrane_box_size[@]}"
echo "Water height (nm): ${water_height}"
sleep "${sleep_time}"

#########################################
### STEP 4: COPYING NANOPARTICLE INFO ###
#########################################

echo -e "\n----------------------------------"
echo "--- STEP 4: Copying NP details ---"
echo "----------------------------------"

## DEFINING PREFIX
output_np_prefix="np"

## DEFINING GRO FILES
np_gro_1="1_${output_np_prefix}_nosolvent.gro"
np_gro_2="2_${output_np_prefix}_shrink.gro"
np_gro_3="3_${output_np_prefix}_resized.gro"
np_gro_4="4_${output_np_prefix}_translated.gro"
np_gro_5="5_${output_np_prefix}_solvated.gro"

## TRJCONV TO GET NP FILE ONLY
echo "Generating GRO file with no solvent --> ${np_gro_1}"
gmx trjconv -f "${np_dir_path}/${input_np_gro_file}" \
            -s "${np_dir_path}/${input_np_tpr_file}" \
            -n "${np_dir_path}/${np_gold_with_ligand_index}" \
            -o "${np_gro_1}" -center >/dev/null 2>&1 << INPUTS
${gold_residue_name}
${gold_with_lig_selection}
INPUTS

## CHECKING IF EXISTS
stop_if_does_not_exist "${np_gro_1}"

## USING EDITCONF TO SHRINK THE BOX
echo "Shrinking NP to zero distance --> ${np_gro_2}"
gmx editconf -f "${np_gro_1}" -o "${np_gro_2}" -d "0" >/dev/null 2>&1

## CHECKING IF EXISTS
stop_if_does_not_exist "${np_gro_2}"

### TOPOLOGY
cp "${np_dir_path}/${input_np_top_file}" "${output_top}"
### FORCE FIELD

## SLEEPING
sleep "${sleep_time}"

######################################################
### STEP 5: GENERATING NP + LIPID MEMBRANE SYSTEMS ###
######################################################

echo -e "\n------------------------------------------------"
echo "--- STEP 5: Generating NP compatible systems ---"
echo "------------------------------------------------"

## GETTING BOX DIMENSION FOR GRO
read -a np_box_size <<<$(read_gro_box_lengths "${np_gro_2}")
echo "NP box size: ${np_box_size[@]}"

## GETTING COMPLETE Z DIMENSION SIZE
np_height_in_box=$(awk -v water_height=${water_height} \
                       -v dist_btn_np_lipid_membrane=${np_lipid_membrane_dist} \
                 'BEGIN{ printf "%.3f", \
                 dist_btn_np_lipid_membrane - water_height}')

### DEFINING NEW HEIGHT SIZE -- OLD, overestimates box size
#new_z_size=$(awk -v np_height_in_box=${np_height_in_box} \
#                 -v np_z_dim="${np_box_size[2]}" \
#                 -v water_buffer_z="${z_water_dist}" \
#                 'BEGIN{ printf "%.3f", \
#                 np_height_in_box + np_z_dim /2 + water_buffer_z }')
#                 # Box z dimension divided by 2 since height in box should take up space
                 
## DEFINING NEW HEIGHT SIZE
new_z_size=$(awk -v np_height_in_box=${np_height_in_box} \
                 -v np_lipid_membrane_dist="${np_lipid_membrane_dist}" \
                 -v water_buffer_z="${z_water_dist}" \
                 'BEGIN{ printf "%.3f", \
                 np_height_in_box + np_lipid_membrane_dist + water_buffer_z}')
                 # Box z dimension divided by 2 since height in box should take up space
                 
echo "NP height in box: ${np_height_in_box} nm"
echo "New z-dimension size: ${new_z_size} nm"

## RESIZING THE NP GRO FILE
echo "Resizing the nanoparticle file --> ${np_gro_3}"
gmx editconf -f "${np_gro_2}" -o "${np_gro_3}" \
             -box ${lipid_membrane_box_size[0]} \
                  ${lipid_membrane_box_size[1]} \
                  ${new_z_size} >/dev/null 2>&1

## MOVING NP TO THE CORRECT HEIGHT
np_dist_xvg="np_distxyz.xvg"
# COMPUTING DISTANCE
gmx distance -f "${np_gro_3}" -s "${np_gro_3}" -oxyz "${np_dist_xvg}" -select "com of resname ${np_center_group} plus [0.000, 0.000, 0.000]" -nopbc >/dev/null 2>&1
# ${new_z_size}

## READING
read -a dist_origin <<< $(tail -n1 "${np_dist_xvg}")

## COMPUTING NEW X, Y, Z
np_new_x=$(awk -v orig=${dist_origin[1]} \
               -v box_x=${lipid_membrane_box_size[0]} \
                 'BEGIN{ printf "%.3f", \
                 orig + box_x/2}')
np_new_y=$(awk -v orig=${dist_origin[2]} \
               -v box_y=${lipid_membrane_box_size[1]} \
                 'BEGIN{ printf "%.3f", \
                 orig + box_y/2}')
np_new_z=$(awk -v orig=${dist_origin[3]} \
               -v z_pos=${np_height_in_box} \
                 'BEGIN{ printf "%.3f", \
                 orig + z_pos}')

## TRANSLATING
echo "Moving the nanoparticle to correct position --> ${np_gro_4}"
gmx editconf -f "${np_gro_3}" -o "${np_gro_4}" \
             -translate "${np_new_x}" "${np_new_y}" "${np_new_z}" >/dev/null 2>&1

## CLEARING TOPOLOGY
top_clear_molecules "${output_top}"

## ADDING NP
echo "sam      1" >> "${output_top}"

## SOLVATING THE BOX
echo "Solvating np with water --> ${np_gro_5}"
gmx solvate -cs spc216.gro \
            -cp "${np_gro_4}" \
            -p "${output_top}" \
            -o "${np_gro_5}" \
            -scale "${scale_factor}" \
            &> /dev/null # Silencing

## CHECKING IF EXISTS
stop_if_does_not_exist "${np_gro_5}"

## SLEEPING
sleep "${sleep_time}"

##################################################### 
### STEP 5: GENERATING NP + LIID MEMBRANE SYSTEMS ###
#####################################################

echo -e "\n-----------------------------------------------------"
echo "--- STEP 6: Combining NP + lipid membrane systems ---"
echo "-----------------------------------------------------"

## DEFINING BUFFER ZONE
buffer_zone="0.4" # buffer when combining gro files
# Increase this buffer if you are running into energy minimization issues

## DEFINING COMBINE FILES
combine_file_1="${output_prefix}_1_top_with_lipids.gro"
combine_file_2="${combine_file_1}"
# "${output_prefix}_2_with_bottom_water.gro"

## NP/WATER + LIPID MEMBRANE
echo "Combining top (NP + water) and lipid bilayer --> ${combine_file_1}"
gro_combine_two_files_by_z "${np_gro_5}" \
                           "${output_lipid_membrane_prefix}.gro" \
                           "${combine_file_1}" \
                           "${buffer_zone}"

## ADDING TO TOPOLOGY
top_read_molecules "${output_lipid_membrane_prefix}.top" >> "./${output_top}"

### WATER BOX
#water_box_gro="bottom_water_box.gro"
#water_box_top="bottom_water_box.top"
#
### ADDING TEMPORARY TOP
#echo -e "[ molecules ]\n; compound   nr_mol" > "${water_box_top}"
## Note that a new top is necessary since gmx solvate will add solvents 
## based on the current available solvents
#
### GENERATING EMPTY WATER BOX
#echo "Generating water box for bottom layer --> ${water_box_gro}"
#gmx solvate -cs spc216.gro \
#            -o "${water_box_gro}" \
#            -p "${water_box_top}" \
#            -box ${lipid_membrane_box_size[0]} \
#                 ${lipid_membrane_box_size[1]} \
#                 ${new_z_size} >/dev/null 2>&1
#
### ADDING TO TOPOLOGY
#top_read_molecules "${water_box_top}" >> "./${output_top}"
#
### ADDING BOTTOM WATERS
#echo "Combining lipid bilayer and bottom water --> ${combine_file_2}"
#gro_combine_two_files_by_z "${combine_file_1}" \
#                           "${water_box_gro}" \
#                           "${combine_file_2}" \
#                           "${buffer_zone}"


## CHECKING IF EXISTS
# stop_if_does_not_exist "${water_box_gro}"
stop_if_does_not_exist "${combine_file_2}"

## SLEEPING
sleep "${sleep_time}"

###################################################
### STEP 7: CORRECTING NOMENCLATURE FOR GRO/TOP ###
###################################################

echo -e "\n---------------------------------------"
echo "--- STEP 7: Correcting gro and top  ---"
echo "---------------------------------------"

## DEFINING CORRECTED OUTPUTS
combine_gro_correct="${output_prefix}.gro"
combine_top_correct="${output_prefix}.top" 

## DEFINING RESIDUE NAMES TO FIX (TIP3 -> SOL)
res_resname="${lipid_mem_water_res_name},${water_res_name}"
## COMBINING RESIDUE INTO A SINGLE ONE
combined_residues="${ligand_residue_name},${np_center_group},${np_residue_name}"

## DEFINING DESIRED ORDER
desired_res_order="${ligand_residue_name},${np_center_group},${lipid_membrane_resname},${water_res_name}"

## RUNNING PYTHON CODE TO FIX GRO
python3.6 "${py_correct_gro}" --ifold "${path_output_dir}" \
                              --igro "${combine_file_2}" \
                              --itop "${output_top}" \
                              --ogro "${combine_gro_correct}" \
                              --otop "${combine_top_correct}" \
                              --resrename "${res_resname}" \
                              --combres "${combined_residues}" \
                              --resorder "${desired_res_order}"

sleep "${sleep_time}"

## DEFINING NEW TOPTOP
output_top="${combine_top_correct}"

## PRINTING
echo "Correcting GRO and TOP file based on residue name"
echo "Correcting for different water residue names: ${lipid_mem_water_res_name} -> ${water_res_name}"
echo "Corrected gro: ${combine_gro_correct}"
echo "Corrected top: ${combine_top_correct}"

## SLEEPING
sleep "${sleep_time}"

####################################
### STEP 8: NEUTRALIZING CHARGES ###
####################################

echo -e "\n-------------------------------------"
echo "--- STEP 8: Neutralizing charges  ---"
echo "-------------------------------------"

# COPYING MDP FILES
### MDP FILES
cp "${mdp_file_path}"/{${em_mdp},${mdp_pull_input_file},${equil_npt_mdp_1},${equil_npt_mdp_2}} "${path_output_dir}"

## FORCE FIELD
echo "Copying forcefield parameters from nanoparticle folder: ${np_dir_path}/${forcefield}"
cp -r "${np_dir_path}/${forcefield}" "${path_output_dir}"
## COPYING ANY REMAINING ITP PRM
cp "${np_dir_path}/"{*.prm,*.itp} "${path_output_dir}"

## INCLUSION TO ITP FILE
echo "Merging nanoparticle force fields with lipid membrane"
## HARD-CODED ADDITION OF TOPOLOGY
# topology_add_include_specific "${output_top}" "forcefield.itp" "#include \"${lipid_membrane_prm_file}\"" # <-- Ignoring since our forcefield should have these params
topology_add_include_specific "${output_top}" "#include \"sam.itp\"" "#include \"${lipid_membrane_itp_file}\""
# topology_add_include_specific "${output_top}" "#include \"sam.itp\"" "#include \"${lipid_membrane_water_itp_file}\"" # <-- Ignoring since we can use the tip3p model that is already available to us

echo -e "\n--------- NEUTRALIZING ---------"

## DEFINING LIPID MEMBRANE WATER
lipid_mem_index="water.ndx"

### NEUTRALIZING
echo "Neutralizing gro file -> ${combine_gro_correct%.gro}_charged.gro"
## CREATING GROMPP
gmx grompp -f "${em_mdp}" \
           -c "${combine_gro_correct}" \
           -p "${output_top}" \
           -o "${combine_gro_correct%.gro}_solvated.tpr" \
           -maxwarn "${num_max_warn}" >/dev/null 2>&1
           
## MAKING INDEX FILE
## CREATING INDEX FILE FOR ONLY WATER
index_make_ndx_residue_ "${combine_gro_correct%.gro}_solvated.tpr" "${lipid_mem_index}" "${water_res_name}"

## --- ATTEMPTING GMX SELECT TO FIX GENION -- doesn't work due to genion needing continuous solvent
## DISTANCE FROM DOPC
# water_dist="2" # 2 nm distance

### RUNNING GMX SELECT TO GET WATER MOLECULES FAR AWAY FROM DOPC -- doesn't work
#gmx select -f "${combine_gro_correct}" \
#           -s "${combine_gro_correct%.gro}_solvated.tpr" \
#           -on "${lipid_mem_index}" \
#           -select "resname ${water_res_name} and same residue as not within ${water_dist} of (resname ${lipid_membrane_resname} or resname ${ligand_residue_name})"

## ADDING COUNTER IONS
gmx genion -s "${combine_gro_correct%.gro}_solvated.tpr" \
           -p "${output_top}" \
           -o "${combine_gro_correct}" \
           -n "${lipid_mem_index}" \
           -neutral >/dev/null 2>&1 << INPUT
${water_res_name}
INPUT

## DEFINING SETUP FILES
setup_files="setup_files"

## CLEANING
rm -f \#*

## MOVING
echo "Cleaning up directory to: ${setup_files}"
mkdir -p "${setup_files}"
mv "${np_gro_1}" \
   "${np_gro_2}" \
   "${np_gro_3}" \
   "${np_gro_4}" \
   "${np_gro_5}" \
   "${np_dist_xvg}" \
   "${combine_file_1}" \
   "${combine_file_2}" \
   "${output_lipid_membrane_prefix}.gro" \
   "${output_lipid_membrane_prefix}.tpr" \
   "${output_lipid_membrane_prefix}.top" \
   "${setup_files}"

## SLEEPING
sleep "${sleep_time}"

#####################################################
### STEP 8B: FIXING THE NP LIPID BILAYER BOX SIZE ###
#####################################################

echo -e "\n-------------------------------------"
echo "--- STEP 8B: Fixing the size of box  ---"
echo "----------------------------------------"

## NAME
resize_gro="${combine_gro_correct%.gro}_resize.gro"

## BUFFER BOX ZONE
buffer_box_zone="0.2" # 0.2 nm increments to buffer the space

## EDITING BOX
gmx editconf -f "${combine_gro_correct}" \
             -o "${resize_gro}" \
             -d "${buffer_box_zone}" \
            
## GETTING RESIZE GRO BOX SIZE
read -a combined_gro_box_size <<< $(read_gro_box_lengths "${combine_gro_correct}")
read -a resize_gro_box_size <<< $(read_gro_box_lengths "${resize_gro}")

## PRINTING
echo "Combined gro box size: ${combined_gro_box_size[@]}"
echo "Resized gro box size: ${resize_gro_box_size[@]}"
echo "Maintaining x, y dimensions: ${combined_gro_box_size[0]}, ${combined_gro_box_size[1]}, ${resize_gro_box_size[2]}"

## SLEEPING
sleep "${sleep_time}"

## EDITING THE BOX SIZE
gmx editconf -f "${resize_gro}" \
             -o "${combine_gro_correct}" \
             -box ${combined_gro_box_size[0]} ${combined_gro_box_size[1]} ${resize_gro_box_size[2]}\

## MOVING RESIZE GRO TO STORAGE
mv "${resize_gro}" "${setup_files}"

###################################
### STEP 9: ENERGY MINIMIZATION ###
###################################
# SKIPPING STEP, ADDING TO THE LAST STEP
echo -e "\n------------------------------------"
echo "--- STEP 9: Energy minimization  ---"
echo "------------------------------------"

## ENERGY MINIMIZATION
if [[ ${want_em} == true ]]; then
gmx grompp -f "${em_mdp}" -c "${combine_gro_correct}" \
                          -p "${output_top}" \
                          -o "${output_prefix}_em.tpr" \
                          -maxwarn "${num_max_warn}"
gmx mdrun -v -nt 1 -deffnm ${output_prefix}_em
fi

##############################################
### STEP 9: PREPARATION FOR EQUILIBRATION ###
##############################################

echo -e "\n---------------------------------------"
echo "--- STEP 9: Preparation for equil  ---"
echo "---------------------------------------"

## SUBMISSION FILE
cp "${input_submit_path}" "${path_output_dir}/${output_submit_name}"
cp "${input_run_path}" "${path_output_dir}/${output_run_name}"

## COPYING PULLING/PUSHING FILE
cp "${submit_pull_script_path}" "${submit_push_script_path}" "${path_output_dir}"

#################################
### EQUILIBRATION PREPARATION ###
#################################

## DEFINING INITIAL PULLING DISTANCE
initial_pull_distance=$(awk -v half_membrane=${membrane_half_thickness} \
                            -v desired_distance=${np_lipid_membrane_dist} \
                 'BEGIN{ printf "%.3f", \
                 desired_distance + half_membrane}') ## TODO -- FIX PULL DISTANCE

## EDITING MDP FILE FOR EQUIL 1
sed -i "s/_DT_/${equil_dt}/g" "${equil_npt_mdp_1}"
sed -i "s/_NSTEPS_/${num_steps_equil_1}/g" "${equil_npt_mdp_1}"
sed -i "s/_TEMPERATURE_/${equil_temp}/g" "${equil_npt_mdp_1}"
sed -i "s/_PULLKCONSTANT_/${pull_constant}/g" "${equil_npt_mdp_1}"
sed -i "s/_PULLINIT_/${initial_pull_distance}/g" "${equil_npt_mdp_1}"
sed -i "s/_GROUP_1_NAME_/${lipid_membrane_resname}/g" "${equil_npt_mdp_1}"
sed -i "s/_GROUP_2_NAME_/${np_center_group}/g" "${equil_npt_mdp_1}"

## EDITING MDP FILES FOR EQUIL 2
sed -i "s/_GROUP_1_NAME_/${lipid_membrane_resname}/g" "${equil_npt_mdp_2}"
sed -i "s/_GROUP_2_NAME_/${np_center_group}/g" "${equil_npt_mdp_2}"
sed -i "s/_DT_/${equil_dt}/g" "${equil_npt_mdp_2}"
sed -i "s/_NSTEPS_/${num_steps_equil_2}/g" "${equil_npt_mdp_2}"
sed -i "s/_TEMPERATURE_/${equil_temp}/g" "${equil_npt_mdp_2}"
sed -i "s/_PULLKCONSTANT_/${pull_constant}/g" "${equil_npt_mdp_2}"
sed -i "s/_PULLINIT_/${initial_pull_distance}/g" "${equil_npt_mdp_2}"

## SUBMISSION FILE
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${output_submit_name}"
sed -i "s/_JOB_NAME_/${output_dir_name}/g" "${output_submit_name}"
sed -i "s/_RUNSCRIPT_/${output_run_name}/g" "${output_submit_name}"
sed -i "s/_EMNUMCORES_/${em_num_cores}/g" "${output_submit_name}"
sed -i "s/_MDRUNCOMMAND_/${mdrun_command}/g" "${output_submit_name}"
sed -i "s/_MDRUNCOMMANDSUFFIX_/${mdrun_command_suffix}/g" "${output_submit_name}"

## RUN FILE
sed -i "s/_MDPEM_/${em_mdp}/g" "${output_run_name}"
sed -i "s/_MDPEQUIL1_/${equil_npt_mdp_1}/g" "${output_run_name}"
sed -i "s/_MDPEQUIL2_/${equil_npt_mdp_2}/g" "${output_run_name}"
sed -i "s/_OUTPUTPREFIX_/${combine_gro_correct%.gro}/g" "${output_run_name}"

## CORRECTIONS FOR PULLING INFORMATION
sed -i "s/_MDPINPUTPULL_/${mdp_pull_input_file}/g" "${output_run_name}"
sed -i "s/_MDPOUTPUTPULL_/${mdp_pull_output_file}/g" "${output_run_name}"
sed -i "s/_MDPINPUTPUSH_/${mdp_pull_input_file}/g" "${output_run_name}"
sed -i "s/_MDPOUTPUTPUSH_/${mdp_pull_input_file}/g" "${output_run_name}"

sed -i "s/_RESPULL1_/${lipid_name}/g" "${output_run_name}"
sed -i "s/_RESNPGOLD_/${np_center_group}/g" "${output_run_name}"
sed -i "s/_RESNPLIG_/${ligand_name}/g" "${output_run_name}"
sed -i "s/_SUBMITNP2LIPIDBILAYER_/${submit_pull_script_name}/g" "${output_run_name}"
sed -i "s/_SUBMITNP_PUSH_FROM_LIPIDBILAYER_/${submit_push_script_name}/g" "${output_run_name}"

sed -i "s/_SPRINGCONSTANT_/${pull_constant}/g" "${output_run_name}"
sed -i "s/_PULLRATE_/${pull_rate}/g" "${output_run_name}"
sed -i "s/_TEMP_/${equil_temp}/g" "${output_run_name}"
sed -i "s/_MAXWARN_/${num_max_warn}/g" "${output_run_name}"
sed -i "s/_TIMESTEP_/${equil_dt}/g" "${output_run_name}"
sed -i "s/_MEMBRANEHALFTHICK_/${membrane_half_thickness}/g" "${output_run_name}"

### ADDING TO SUBMISSION FILE
echo "${path_output_dir}" >> "${JOB_SCRIPT}"

## PRINTING SUMMARY FILE
transfer_summary="transfer_summary.txt"
echo "NP PATH: ${np_dir_path}" >> ${transfer_summary}
echo "Lipid membrane PATH: ${path_lipid_membrane}" >> ${transfer_summary}

#### PRINTING SUMMARY
echo "------------- SUMMARY -------------"
echo "Developed nanoparticle-lipid membrane system"
echo -e "\n-- NANOPARTICLE --"
echo "  ligand name: ${ligand_name}"
echo "  nanoparticle shape: ${np_shape}"
echo "  nanoparticle diameter: ${np_diameter} nm"
echo "  trial: ${trial}"
echo -e "\n-- LIPID MEMBRANE --"
echo "Lipid membrane: ${lipid_name}"
echo "Total lipid size: ${lipid_size} x ${lipid_size}"
echo -e "\n ---------------------------"
echo "Total number of atoms: $(gro_count_total_atoms ${combine_gro_correct})"
echo "Box size: $(gro_measure_box_size ${combine_gro_correct})"
echo "Force field: ${forcefield}"
echo "Temperature: ${temperature}"
echo "Included equilibration job at: ${JOB_SCRIPT}"

# ------------
## COMPUTING DENSITY OF WATER AND LIPID MEMBRANE
# gmx density -d Z -f lipid_membrane.gro -s lipid_membrane.tpr -o density_water.xvg

## METHOD OF MEASURSING CENTER OF MASS OF DOPC
#  gmx distance -f lipid_membrane.gro -s lipid_membrane.tpr -oxyz distxyz.xvg -select 'cog of group DOPC plus [0.000,0.000,0.000]'
