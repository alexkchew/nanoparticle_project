#!/bin/bash

# prep_system_planar_sam.sh
# This script runs the ligand_builder.py program with input flags to get
# gromacs parameters for a SAM of choice; then solvates, energy minimizes;
# runs quick NPT simulation, and extends box to create vacuum in preparation
# for lengthier NVT equilibration.

# Usage example:  bash prep_system_planar_sam.sh Planar butanethiol charmm36-nov2016.ff True 310.15 180530-sim_testing

# bash prep_system_planar_sam.sh Planar C11NH2,C11NH3p charmm36-nov2016.ff True 310.15 180530-sim_testing

# Originally written by Reid Van Lehn
# Updated by Alex Chew (03/30/2017)
# 17-04-11: Added force field option
# 17-11-19: Updated the script
# 17-11-22: Updating script to take both planar and spherical cases
# 17-11-26: Changed script name to be simply prep ligand builder
# 17-11-26: Added temperature functionality + changing number of steps

# INPUT VARIABLES
# $1: type of monolayer (spherical vs planar)
# $2: ligand names
# $3: Force field (opls-aa or charmm36-nov2016.ff)
# $4: Gold (True/False)
# $5: Temperature
# $6: Simulation directory

### FUNCTIONS:
#   check_extra_ligand_parameters: checks if you have extra ligand parameters 
#   read_ligand_array: reads ligand names in a form of an array
#   make_ndx_ions: makes indexes for ions
#   make_ndx_file: makes ndx files
#   make_ndx_ligands: makes indexes for ligands

## TODO: Update make_ndx script to encode both ligand types (not just one!)
## TODO: Update make_ndx to remove any duplicates
## TODO: Update nvt mdp options to incorporate multiple ligands into freeze groups

### SOURCING ALL VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

# Getting script name and echoing
print_script_name

### DEFINING FUNCTIONS ###

### FUNCTION TO CONVERT SPACES TO COMMA DELININATOR
# The purpose of this function is to separate strings that are deliminated by spaces with commas
## INPUTS:
#   $1: array with spaces
## OUTPUTS:
#   output: string with commas
function convert_delim_space_to_commas () {
    my_str="$@"
    echo "${my_str[@]}" | sed -e 's/\s/,/g'
}

### FUNCTION TO COUNT NUMBER OF COMMAS WITHIN NAME
## INPUTS:
#   $1: any string
## OUTPUTS:
#   num_commas: number of commas
## USAGE: count_commas butanethiol,hexadecanethiol
function count_commas () {
    ## COUNTING THE NUMBER OF COMMAS
    num_commas=$(echo "$1" | grep "," | wc -l)
    ## PRINTING
    echo "${num_commas}"
}

### FUNCTION TO CHECK EXTRA LIGAND PARAMETERS
# The purpose of this function is to see if there are any *prm files. If so, copy over the parameter file to current directory and update the topology
## INPUTS:
#   $1: path to parameter files
#   $2: topology file to edit (in current directory)
function check_extra_ligand_parameters () {
    ## DEFINING THE INPUTS
    input_path_file_="$1"
    # input_top_file_="$2"
    
    ## GETTING BASENAME
    input_file_basename_=$(basename "${input_path_file_}")
    
    if [ -e "${input_path_file_}" ]; then
        # COPYING PARAMETER FILE
        cp -rv "${input_path_file_}" "./"
        # ADDING PARAMETER TO TOPOLOGY FILE
        # force_field_line_num=$(grep -nE "forcefield.itp" ${input_top_file_} | grep -Eo '^[^:]+') # Line where you see forcefield.itp
        # sed -i "$((${force_field_line_num}+1))i #include \"${input_file_basename_}\"" ${input_top_file_}
    else
        echo "No additional ligand parameters found at ${input_path_file_}"
        echo "Continuing without inclusion of *.prm files"
    fi
}

### FUNCTION TO READ LIGAND ARRAY
## INPUTS:
#   $1: ligand_names -- array of ligand names separated by a comma
## OUTPUTS:
#   $2: array: ligand names in a form of an array
## USAGE: IFS=" "; read -a ligand_names_array <<< $(read_ligand_array ${ligand_names}   ); IFS="$OLDIFS"
function read_ligand_array () {
    ## DEFINING INPUTS
    input_ligand_names_="$1"
    ## STORING OLD IFS
    OLDIFS="$IFS"
    IFS=","; read -ra ligand_names_array <<< "${input_ligand_names_}"
    ## PRINTING 
    echo "${ligand_names_array[@]}"
    ## RENEWING IFS
    IFS="${OLDIFS}"
}

### FUNCTION TO CREATE INDEX FILES OF IONS ONLY ###
# $1: gro file
# $2: index file to output
function make_ndx_ions () {
    # Printing
    echo "---- make_ndx_ions ----"
    echo "Making index file for IONS only"
    echo "Using gro file: $1"
    echo "Creating index file: $2"
# Running gmx index
gmx make_ndx -f "$1" -o "$2" &> /dev/null << INPUT
keep 0
keep 1
r CL
r NA
name 0 IONS
q
INPUT
}

### FUNCTION TO MAKE A GENERALIZED NDX FILE
# $1: gro file
# $2: index file
function make_ndx_file () {
    # Printing
    echo "---- make_ndx_file ----"
    echo "Making index file"
    echo "Using gro file: $1"
    echo "Creating index file: $2"
# Running gmx index
gmx make_ndx -f "$1" -o "$2" &> /dev/null << INPUT
q
INPUT
}

### FUNCTION TO CREATE INDEX FILES OF LIGANDS ONLY ### -- Updated to include IONS
# $1: gro file
# $2: index file to output
# $3: ligand residue name
# Usage:  make_ndx_ligands sam_em.gro testing.ndx RSN
function make_ndx_ligands () {
    ## STORING OLD IFS
    OLDIFS="${IFS}"
    ## DEFINING INPUTS
    input_gro_file_="$1"
    input_ndx_file_="$2"
    input_resnames_="$3"
    # Printing
    echo "---- make_ndx_ligands ----"
    echo "Making index file for LIGANDS only"
    echo "Using gro file: $1"
    echo "Creating index file: $2"
    echo "Using residue name: $3"
    
## COUNTING COMMAS
num_commas_=$(count_commas "${input_resnames_}")

## FINDING STRING OF RESIDUE
if [ ${num_commas_} == "0" ]; then
    str_residue="r ${input_resnames_}"
else
    ## LOOPING THROUGH THE NUMBER OF COMMAS
    IFS=","; read -a residue_names <<< $(echo ${input_resnames_}); IFS="${OLDIFS}"
    
    ## DEFINING STRING RESIDUE
    str_residue="r"
    ## FINDING TOTAL NUMBER OF RESIDUES
    # total_residues_=$((${residue_names[#]}+1))
    ## COUNTING
    
    # count_residues_=0
    for each_residue in ${residue_names[@]}; do
        ## SEEING IF LAST RESIDUE
        ## ADDING TO STIRNG
        str_residue="${str_residue} ${each_residue}"

#        if [ "${count_residues_}" == "${total_residues_}" ]; then
#            ## ADDING TO STR RESIDUE
#            str_residue="${str_residue} r ${each_residue}"
#        else
#            str_residue="${str_residue} r ${each_residue} |"
#        fi
        ## ADDING TO THE COUNT
        # count_residues_=$((${count_residues_}+1))
    done
    
fi
    
# Running gmx index
gmx make_ndx -f "$1" -o "$2" << INPUT #  &> /dev/null
keep 0
keep 1
${str_residue}
name 0 LIGANDS
q
INPUT
}

### FUNCTION TO CHECK IF YOUR INDEX FILE HAS IONS
# This function checks index.ndx to see if "Ion" is there. If so, return True
## INPUTS:
# $1: Index file
## OUTPUTS:
#   "True" if you do have an ion
function check_ndx_ions () {
    [ -z "$(grep "Ion" $1)" ] || echo "True"
}


# --- USER-SPECIFIC PARAMETERS (BEGIN) ---

echo -e "\n ----- prep_system_ligandbuilder_planar.sh ----- \n"

## STORING OLD IFS
OLDIFS="$IFS"

### INPUT VARIABLES 
shape_type="$1" # "Spherical" # Planar # Spherical or Planar
ligand_names="$2" # Name of ligand (assuming homogeneous)octanethiol
forcefield="$3" # Force Field (opls-aa or charmm36-nov2016.ff)
wantGold="$4" # True/False, False if you do not want gold
temp="$5" # temperature of the system
simulation_dir="$6" # simulation directory
trial_num="$7"

## WATER AND METHANOL BOX
# size of z-dimension of water box (in nm) to add on top of SAM
# Since each box added has same cross-sectional area, can control volume simply through
# ratio of the two z-dimensions. 
water_box_z="${8-4.0}"
# '4.0' # 4 nm for water
methanol_box_z='0' # No methanol

# Defining ligand fractions
#ligand_fracs=0.5,0.5
# ligand_fracs="0.5,0.5" # Left alone for now
ligand_fracs="1.0"

# number of rows/columns of ligands in SAM; determines box size; x-value should be even
num_lig_x=10 # Previously 8 x 8 -- testing potential issue with size
num_lig_y=10 

# Defining max warns
max_warn_index="5"

### CHECKING FORCEFIELD SUFFIX
forcefield_suffix=$(check_ff_suffix "${forcefield}")

## INTERFACE FORCE FIELD
wantinterfaceff=True

## DEFINING LOGICAL IF YOU WANT TEMPERATURE ANNEALING
want_temp_annealing=false
# true
# This temperature anneals the SAMs before running any production runs

# Specifying virtual sites / polarization / gold
if [[ $wantGold == "True" ]]; then
    # Specifying Interface Force Field
    if [[ $wantinterfaceff == "True" ]]; then # Interface force field is on
        gold_ending="intffGold"
        wantVirtualSites=False
        wantPolarization=False
    else # Interface force field is off
        gold_ending="withGOLP"
        wantVirtualSites=True
        wantPolarization=True
    fi
elif [[ $wantGold == "False" ]]; then
    gold_ending="noGold"
    # Automatically setting virtual + polarization sites off
    wantVirtualSites=False
    wantPolarization=False
fi

### PYTHON SCRIPTS
py_prep_file="${MDBUILDER}/builder/make_planar_sam.py" # ligand builder script that builds the system
py_prep_file="${MDBUILDER}/builder/make_planar_sam_with_fixed_gold_positions.py" # ligand builder script that builds the system
py_remove_atoms_file="${MDBUILDER}/math/remove_atoms.py" # Removes residues within the center core (for hollow shell geometry)
py_add_position_restraint="${MDBUILDER}/core/add_position_restraint.py"

### INPUT FILES
## SUBMISSION SCRIPTS
input_submit_script="submit_sam.sh"

## MDP FILES
mdp_em="minim_sam_gmx5.mdp" # Energy minimization mdp script
mdp_em_freeze_ions="minim_sam_gmx5_freeze_ions.mdp" # Energy minimization freezing ions in the z direction script
mdp_equil_nvt_freeze_lig="nvt_double_equil_gmx5_freeze_lig_charmm36.mdp" # MDP file for NVT freezing ligands for planar case
mdp_equil_nvt_freeze_water_ion="nvt_double_equil_gmx5_freeze_water_ion_charmm36.mdp" # MDP file for NVT freezing water and ions for the planar case
mdp_equil_npt_freeze_lig="npt_double_equil_gmx5_freeze_lig_charmm36.mdp" # MDP file for NPT freezing ligands

## DEFAULT EQUIL AND PROD CODE
if [[ "${want_temp_annealing}" == true ]]; then
    mdp_equil="nvt_double_equil_gmx5_charmm36_annealing_frozen_gold_only.mdp" # Equilibration mdp script
    mdp_equil2="npt_double_equil_gmx5_charmm36.mdp"
else
    mdp_equil="npt_double_equil_gmx5_charmm36.mdp" # Equilibration mdp script
fi
mdp_prod="npt_double_prod_gmx5_charmm36.mdp" # Production mdp script

## IDENTIFYING FORCE FIELD DIRECTORY
if [[ "${forcefield}" == "charmm36-nov2016.ff" ]]; then
    mdp_dir="${INPUT_DIR}/MDP_FILES"
    path2mdp="${mdp_dir}/${forcefield}/${shape_type}"
else
    mdp_dir="$INPUT_DIR/MDP_FILES/charmm36"
    path2mdp="${mdp_dir}/${shape_type}"
fi


### OUTPUT DIRECTORY NAME ###
output_file_name="sam"

if [[ ${shape_type} == "Planar" ]]; then
    output_dirname="${shape_type}_${temp}_K_${ligand_names}_${num_lig_x}x${num_lig_y}_${forcefield_suffix}_${gold_ending}_Trial_${trial_num}"   # _NoGold
    # Defining current type for LB
    current_type="planar"
elif [[ ${shape_type} == "Spherical" ]]; then
    output_dirname="${shape_type}_${temp}_${sphereDiam}_nm_diam_${ligand_names}_Spherical_${forcefield_suffix}_Trial_${trial_num}" 
    # Defining current type for LB
    current_type="spherical"
fi

output_dir="$PATH2SIM/${simulation_dir}/${output_dirname}"
output_top="${output_file_name}.top" # Name of topology
output_gro="${output_file_name}.gro" # Name of gro file
# INDEX FILES
output_ndx="index.ndx" # name of the index file
output_ligand_ndx="ligands.ndx" # name of index file for ligands only -- DEPRECIATED
output_submit_script="submit.sh" # name of the submission file

## DEFINING EMPTY Z LAYER
empty_z="0.10"
# "0.3"
# "0.3"
# "0.2"
# .2 to ensure no overlap
# "0.14"
# 0.14 is the approximate probe radius of a water molecule
# "1"
# "0.10" # nm distance between water and the SAM layer

## WATER AND METHANOL SCRIPTS
water_script="${INPUT_DIR}/add_water_gmx5.sh"
methanol_script="${INPUT_DIR}/add_methanol.sh.sh"

## LAYER SEPARATION - distance between monolayers
num_layers=1 # Number of layers of gold atoms

### SIMULATION PARAMETERS
## NUMBER OF STEPS FOR NVT / NPT PREPARATION
n_steps_equil_nvt_freeze_lig="500000" # 1 ns freeze ligands for NVT ensemble
n_steps_equil_nvt_freeze_water_ion="500000" # 1 ns freeze water/ion for NVT ensemble
n_steps_equil_npt_freeze_lig="500000" # 1 ns freeze ligands for NPT ensemble
npt_position_restraint="100000"
# "100000"
npt_posre_file="posre.itp"

## NUMBER OF STEPS FOR EQUILIBRATION AND PRODUCTION -- # ps / .002 timesteps
n_steps_equil="3000000" # 6 ns equilibration
n_steps_equil2="1000000" # 2 ns equilibration with the Berensen barostat
# "2500000" # 5 ns equilibration
# n_steps_prod="15000000" # 30 ns production "50000000" # 100 ns production "25000000" # 50 ns production
n_steps_prod="2500000" # 5 ns production
# "25000000" # 50 ns production "50000000" # 100 ns production "25000000" # 50 ns production

## DEFINIG GOLD PLANAR
gold_planar_itp="Au_Planar.itp"

## DEFINING MAXWARN
maxwarn="5"
num_cores="28"

# --- USER-SPECIFIC PARAMETERS (END) ---

#########################
### DEFAULT VARIABLES ###
#########################

## GOLD RES NAME
gold_resname="AUI"

## NAME OF SULFUR GROUP
sulfur_group_name="S1"

#######################################
### MAIN CODE 
#######################################

### CHECKING IF DIRECTORY EXISTS
check_output_dir "${output_dir}"

## CREATING DIRECTORY
mkdir -p "${output_dir}"

# COPYING FORCE FIELD TO OUTPUT DIRECTORY
cp -r "${PATH_FORCEFIELD}/${forcefield}" "${output_dir}"
# cp -r "${INPUT_DIR}/$forcefield" "$output_dir"

## RUNNING LIGAND BUILDER PYTHON SCRIPT
if [[ ${shape_type} == "Planar" ]]; then
        python ${py_prep_file} --ogro ${output_gro} \
                               --otop ${output_top} \
                               --ofold ${output_dir} \
                               --ff ${forcefield} \
                               --prep ${PREP_LIGAND_FINAL} \
                               --ligx ${num_lig_x} \
                               --ligy ${num_lig_y} \
                               --names ${ligand_names} \
                               --fracs ${ligand_fracs} \
                               --nlayers ${num_layers}   \
                               --gold ${wantGold} \
                               --vir ${wantVirtualSites} \
                               --pol ${wantPolarization} \
                               --int ${wantinterfaceff}
fi


# set a variable name here used below
sam_name_nogro=${output_gro%.gro}

## CHANGING DIRECTORY TO OUTPUT DIRECTORY
cd ${output_dir}

######################################
### CHECKING ADDITIONAL PARAMETERS ###
######################################

## CHECKING LIGAND NAMES
count_multiple_ligands=$(count_commas "${ligand_names}")

if [ ${count_multiple_ligands} == "0" ]; then
    ## DEFINING INPUT LIGAND PARAMETER NAME
    if [[ "${forcefield}" == "charmm36-nov2016.ff" ]]; then
        input_ligand_parameter_file_name="${ligand_names}_${forcefield_suffix}.prm"
    else
        input_ligand_parameter_file_name="${ligand_names}.prm" # _${forcefield_suffix}
    fi

    echo "Checking if there are extra ligand parameters for: ${input_ligand_parameter_file_name}"
    ## DEFINING LIGAND PARAMETER FULL PATH
    path_ligand_parameter="${PREP_LIGAND_FINAL}/${ligand_names}/${input_ligand_parameter_file_name}"
    ## CHECKING LIGAND PARAMETERS
    check_extra_ligand_parameters "${path_ligand_parameter}" "${output_top}"
## CASE WHEN WE HAVE MULTIPLE LIGANDS DEFINED
    echo "--- COPYING ITP FILES ...---"
    cp -rv "${PREP_LIGAND_FINAL}/${ligand_names}/${ligand_names}.itp" ./${forcefield}

    ## MOVING ITP AND PARM FILES
    mv "${gold_planar_itp}" ./${forcefield}
    mv "${input_ligand_parameter_file_name}" ./${forcefield}

else ## LOOPING THROUGH LIGANDS
    ## FINDING ALL LIGANDS
    IFS=" "; read -a ligand_names_array <<< $(read_ligand_array ${ligand_names}   ); IFS="$OLDIFS"
    ## LOOPING THROUGH LIGAND ARRAY
    for each_ligand in ${ligand_names_array[@]}; do
        ## DEFINING INPUT LIGAND PARAMETER NAME
        input_ligand_parameter_file_name="${each_ligand}_${forcefield_suffix}.prm"
        echo "Checking if there are extra ligand parameters for: ${input_ligand_parameter_file_name}"
        ## DEFINING LIGAND PARAMETER FULL PATH
        path_ligand_parameter="${PREP_LIGAND_FINAL}/${each_ligand}/${input_ligand_parameter_file_name}"
        ## CHECKING LIGAND PARAMETERS
        check_extra_ligand_parameters "${path_ligand_parameter}" "${output_top}"
        echo "--- COPYING ITP FILES ...---"
        cp -rv "${PREP_LIGAND_FINAL}/${each_ligand}/${each_ligand}.itp" ./
        
    done
    
fi

########################################################
### RUNNING PYTHON SCRIPT TO ADD POSITION RESTRAINTS ###
########################################################
python3 "${py_add_position_restraint}" --ifold "${output_dir}/${forcefield}" --names "${ligand_names}"

# Moving all mdp files into directory from force field file
cp -rv "${path2mdp}/"{${mdp_em},${mdp_equil},${mdp_prod}} ./

## COPYING SECOND EQUIL
if [[ "${want_temp_annealing}" == true ]]; then
    cp -rv "${path2mdp}/"${mdp_equil2} ./
fi

# Moving methanol into file
# mv ${forcefield}/methanol_equil.gro ./

########################
### PLANAR MONOLAYER ###
########################

#### PLANAR MONOLAYER LOGICALS
## DEFINING JOB TYPE
planar_monolayer_job_type="simple"
    # simple: energy minimization, npt equilibration and npt production runs
    # 0: nvt equil (frozen ligands) and npt equil (frozen ligands)
    # 1: nvt equil (frozen ligands) and nvt equil (frozen ions)

if [[ ${shape_type} == "Planar" ]]; then
    
    ### DEFINING SUBMISSION SCRIPT AND COPYING OVER MDP FILES
    # NVT EQUIL (FROZEN LIGANDS) / NPT EQUIL (FROZEN LIGANDS)
    if [[ ${planar_monolayer_job_type} == "0" ]]; then
        ## RE-DEFINING INPUT SUBMISSION SCRIPT
        input_submit_script="submit_sam_double_planar.sh"
        ## COPYING ADDITIONAL MDP FILES
        cp -rv "${path2mdp}/"{${mdp_equil_nvt_freeze_lig},${mdp_equil_npt_freeze_lig}} ./
    # NVT EQUIL (FROZEN LIGANDS) / NVT EQUIL (FROZEN WATER/IONS)
    elif [[ ${planar_monolayer_job_type} == "1" ]]; then
        ## RE-DEFINING INPUT SUBMISSION SCRIPT
        input_submit_script="submit_sam_double_planar_nvt_nvt.sh"
        ## COPYING ADDITIONAL MDP FILES
        cp -rv "${path2mdp}/"{${mdp_equil_nvt_freeze_lig},${mdp_equil_nvt_freeze_water_ion}} ./
    # SIMPLE SIMULATIONS
    elif [[ ${planar_monolayer_job_type} == "simple" ]]; then
        ## RE-DEFINING INPUT SUBMISSION SCRIPT
        input_submit_script="submit_sam_planar_simple.sh"
    fi
    
    ### ADDING WATER TO SYSTEM
    if [ $(awk -v box=$water_box_z 'BEGIN{ if (box > 0.0) print 1; else print 0}') -eq 1 ]; then

        ## ADDING WATER TO LIGAND BUILDER SYSTEM
        bash "${water_script}" "${output_gro}" "${output_top}" "${water_box_z}" "${empty_z}"

        ## FINDING VARIABLE NAME FOR ADDED WATER
        sam_name_addwat=${sam_name_nogro}_addwat.gro
        
        # Creating index file
        make_ndx_file ${sam_name_addwat} ${output_ndx}
        
        ###########################
        ### ENERGY MINIMIZATION ###
        ###########################
        echo "Energy minimization"
        # use minim_sam because this has AU groups as freezegrps
        gmx grompp -quiet -f ${mdp_em} -n ${output_ndx} -c ${sam_name_addwat} -p ${output_top} -o "${sam_name_nogro}"_em.tpr -maxwarn ${max_warn_index}

    # neutralize if necessary here using tpr file just generated
gmx genion -s "$sam_name_nogro"_em.tpr -p ${output_top} -o ${sam_name_addwat} -neutral << INPUT
SOL
INPUT
        else

        # just rename variable without adding water
        sam_name_addwat=${sam_name_nogro}.gro

    fi

    ## CREATING INDEX FILE
    make_ndx_file ${sam_name_addwat} ${output_ndx}
    
    ## ADDING TO INDEX FILE AS NECESSARY
    if [[ "${want_temp_annealing}" == true ]]; then
gmx make_ndx -n ${output_ndx} -f "${sam_name_addwat}" -o "${output_ndx}" << INPUTS
a ${sulfur_group_name}
q
INPUTS
    fi
    
    ## CHECKING IF YOU HAVE IONS
    if [[ "$(check_ndx_ions ${output_ndx})" == "True" ]]; then
        ## COPYING MDP FILE
        cp -rv "${path2mdp}/"${mdp_em_freeze_ions} ./
        ## CREATING EM FILE
        gmx grompp -quiet -f ${mdp_em_freeze_ions} -n ${output_ndx} -c $sam_name_addwat -p ${output_top} -o "${sam_name_nogro}"_em.tpr -maxwarn ${max_warn_index}
    else ## NO IONS -- normal energy minimization
        gmx grompp -quiet -f ${mdp_em} -n ${output_ndx} -c ${sam_name_addwat} -p $output_top -o "$sam_name_nogro"_em.tpr -maxwarn ${max_warn_index}
    fi
    ## RUNNING ENERGY MINIMIZATION
    gmx mdrun -v -nt 1 -deffnm "${sam_name_nogro}"_em

    # now add methanol as additional step if necessary

    ### ADDING METHANOL TO SYSTEM
    if [ $(awk -v box=$methanol_box_z 'BEGIN{ if (box > 0.0) print 1; else print 0}') -eq 1 ]; then
        # Running methanol script to add methanol
        bash ${methanol_script} "$sam_name_nogro"_em.gro $output_top $methanol_box_z

    # remake tpr file, re-minimize
        gmx grompp -quiet -f ${mdp_em}  -c ${sam_name_nogro}_em_addmet.gro -p $output_top -o "$sam_name_nogro"_em.tpr -maxwarn ${max_warn_index}
        gmx mdrun -v -nt 1 -deffnm "$sam_name_nogro"_em
    fi
    
    # DEFINING CURRENT GRO FILE
    gro_file_after_em="${sam_name_nogro}_em.gro"
    
    ### --- PREPARATION FOR SHORT NVT ENSEMBLE WITH FROZEN LIGANDS
    ### GETTING RESIDUE NAME
    if [ ${count_multiple_ligands} == "0" ]; then
        resname=$(extract_itp_resname "${forcefield}/${ligand_names}.itp")
    else
         IFS=" "; read -a resname <<< $(extract_itp_multiple_resname ${ligand_names_array[@]}); IFS="$OLDIFS"
         ## CONVERTING RESNAME INTO COMMAS
         resname_commas=$(convert_delim_space_to_commas "${resname[@]}")
         ## CONVERTING RESNAME TO SIMPLY STRINGS
         resname="$(echo ${resname[@]})"
         echo "RESNAME TO COMMAS: ${resname_commas}"
         sleep 3
    fi
    
    #### NVT EQUILIBRATION WITH FROZEN LIGANDS
    ### FIXING ADDITIONAL MDP FILES
    if [[ ${planar_monolayer_job_type} != "simple" ]]; then
        sed -i "s/NSTEPS/${n_steps_equil_nvt_freeze_lig}/g" ${mdp_equil_nvt_freeze_lig}
        sed -i "s/TEMPERATURE/${temp}/g" ${mdp_equil_nvt_freeze_lig}
        sed -i "s/LIGANDS/${resname}/g" ${mdp_equil_nvt_freeze_lig} # ligand names that are frozen
    fi
    # Tentatively removing ligands
    
    ### COPYING SUBMISSION SCRIPT
    cp -r "${PATH2SUBMISSION}/${input_submit_script}" "./${output_submit_script}"
    
    ### --- PREPARATION FOR SHORT NPT ENSEMBLE WITH FROZEN LIGANDS
    
    #### NEXT EQUILIBRATION -- FIXING MDP FILES
    # NVT EQUIL (FROZEN LIGANDS) / NPT EQUIL (FROZEN LIGANDS)
    if [[ ${planar_monolayer_job_type} == "0" ]]; then
        ### FIXING ADDITIONAL MDP FILES
        sed -i "s/NSTEPS/${n_steps_equil_npt_freeze_lig}/g" ${mdp_equil_npt_freeze_lig}
        sed -i "s/TEMPERATURE/${temp}/g" ${mdp_equil_npt_freeze_lig}
        ### FIXING SUBMISSION FILE
        sed -i "s/NPT_EQUIL_MDP_FROZEN_LIGANDS/${mdp_equil_npt_freeze_lig}/g" ${output_submit_script} # equil mdp file for frozen ligands (NPT)
    # NVT EQUIL (FROZEN LIGANDS) / NVT EQUIL (FROZEN WATER/IONS)
    elif [[ ${planar_monolayer_job_type} == "1" ]]; then
        ### FIXING ADDITIONAL MDP FILES
        sed -i "s/NSTEPS/${n_steps_equil_nvt_freeze_water_ion}/g" ${mdp_equil_nvt_freeze_water_ion}
        sed -i "s/TEMPERATURE/${temp}/g" ${mdp_equil_nvt_freeze_water_ion}
        ### FIXING SUBMISSION FILE
        sed -i "s/NVT_EQUIL_MDP_FROZEN_WATER_ION/${mdp_equil_nvt_freeze_water_ion}/g" ${output_submit_script} # equil mdp file for frozen ligands (NPT)
    fi

    ### CREATING INDEX FILE FOR LIGAND ONLY FOR POSITION RESTRAINTS
    if [ ${count_multiple_ligands} == "0" ]; then
        make_ndx_ligands ${gro_file_after_em} ${output_ligand_ndx} ${resname}
    else
        make_ndx_ligands ${gro_file_after_em} ${output_ligand_ndx} ${resname_commas}
    fi
    
    # USING GENRESTR TO CREATE ITP FILE FOR POSITION RESTRAINTS
gmx genrestr -f ${gro_file_after_em} -o "${npt_posre_file}" -fc ${npt_position_restraint} ${npt_position_restraint} ${npt_position_restraint} -n ${output_ligand_ndx} << INPUT
LIGANDS
INPUT
    
    ## FIXING ITP FILE
    if [ ${count_multiple_ligands} == "0" ]; then
        ## FIXING THE POSITION RESTRAINT -- REMOVING ALL ATOMS AFTER THE TOTAL ATOMS OF RESIDUE
        total_atom_in_residue=$(itp_count_atoms "${forcefield}/${ligand_names}")
        itp_fix_genrestr "${npt_posre_file}" "${total_atom_in_residue}"
    fi

    ## ADDING POSITION RESTRAINT TO TOPOLOGY AFTER THE LIGAND NAME
    topology_add_include_specific ${output_top} ${ligand_names}.itp "#ifdef LIGANDPOSRES"
    topology_add_include_specific ${output_top} "#ifdef LIGANDPOSRES" "#include \"${npt_posre_file}\""
    topology_add_include_specific ${output_top} "#include \"${npt_posre_file}\"" "#endif"
    
    ## EDITING SUBMISSION SCRIPT
    sed -i "s#PATH2LIGANDBUILDER#${PATH2BIN}/nanoparticle_rc.sh#g" ${output_submit_script} # equil mdp file for frozen ligands (NVT)
    sed -i "s/NVT_EQUIL_MDP_FROZEN_LIGANDS/${mdp_equil_nvt_freeze_lig}/g" ${output_submit_script} # equil mdp file for frozen ligands (NVT)
    sed -i "s/POSITION_RESTRAINT_FILE_NAME/${npt_posre_file}/g" ${output_submit_script} # Position restraint file
    sed -i "s/INDEX_FILE_NAME/${output_ndx}/g" ${output_submit_script} # index file
fi

### USING SED TO CHANGE MDP FILE PARAMETERS
# CHANGING NUMBER OF STEPS
sed -i "s/_NSTEPS_/${n_steps_equil}/g" ${mdp_equil}
sed -i "s/_NSTEPS_/${n_steps_prod}/g" ${mdp_prod}

# CHANGING TEMPERATURE
sed -i "s/_TEMPERATURE_/${temp}/g" ${mdp_equil}
sed -i "s/_TEMPERATURE_/${temp}/g" ${mdp_prod}


## EDITING MDP FILES FOR EQUIL 2


if [[ "${want_temp_annealing}" == true ]]; then
    sed -i "s/_TEMPERATURE_/${temp}/g" ${mdp_equil2}
    sed -i "s/_NSTEPS_/${n_steps_equil2}/g" ${mdp_equil2}
fi

### USING SED TO CHANGE SUBMISSION FILE NAME
sed -i "s/_JOB_NAME_/${output_dirname}/g" ${output_submit_script} # Output file name
sed -i "s/_OUTPUTPREFIX_/${output_file_name}/g" ${output_submit_script}
sed -i "s/_EQUILMDP1_/${mdp_equil}/g" ${output_submit_script}
sed -i "s/_EQUILMDP2_/${mdp_equil2}/g" ${output_submit_script}
sed -i "s/_PRODMDP_/${mdp_prod}/g" ${output_submit_script}
sed -i "s/_MAXWARN_/${maxwarn}/g" ${output_submit_script}
sed -i "s/_NUM_CORES_/${num_cores}/g" ${output_submit_script}
sed -i "s/_INDEXNDX_/${output_ndx}/g" ${output_submit_script}

###########################
### CORRECTION FOR IONS ###
###########################

## IF NOT TRUE, WE NEED TO EDIT SOME FILES
if [[ "$(check_ndx_ions ${output_ndx})" != "True" ]]; then
    ## LOOPING THROUGH AN ARRAY OF FILES
    declare -a file_array=("${mdp_equil_nvt_freeze_lig}" "${mdp_equil_npt_freeze_lig}" "${mdp_equil_nvt_freeze_water_ion}")
    
    ## LOOPING THROUGH EACH ARRAY AND CHECKING
    for each_file in ${file_array[@]}; do
        ## CHECKING MDP FILE IF IT HAS ION AS A FREEZE GROUP. IF SO, REMOVE IT!
        if [ -e "${each_file}" ]; then # Check if file exists
            mdp_remove_freezegrps "${each_file}" Ion
        fi
    done
fi

#####################################################################
### UPDATING MDP FILE FOR EQUILIBRATION FOR TEMPERATURE ANNEALING ###
#####################################################################
if [[ "${want_temp_annealing}" == true ]]; then
    ## EDITING RESIDUE NAMES
    sed -i "s/_GOLDRESNAME_/${gold_resname}/g" "${mdp_equil}"
    sed -i "s/_SULFURNAME_/${sulfur_group_name}/g" "${mdp_equil}"

    ## ADDING ANNEALING, 600 K ANNEALING TEMPERATURE
#    echo "
#; ANNEALING TEMPERATURE
#annealing   = single ; single set of temperature points
#annealing-npoints = 6; all groups that are not annealed
#annealing-time = 0   500 1000 3000 4000 5000; 0 - 3 ns is 600 K, 4-5 ns is 300 Ks
#annealing-temp = 300 300  500 500  300  300; 
#" >> ${mdp_equil}

## VERSION 2
    echo "
; ANNEALING TEMPERATURE
annealing   = single ; single set of temperature points
annealing-npoints = 6; all groups that are not annealed
annealing-time = 0   500 1000 4000 5000 6000; 0 - 3 ns is 600 K, 4-5 ns is 300 Ks
annealing-temp = 300 300 600  600  300  300; 
" >> ${mdp_equil}


fi

#########################
### CLEANING UP FILES ###
#########################

### CLEANING UP DIRECTORY FILE
mkdir -p setup_files

checkNMoveFile "methanol_equil.gro" "setup_files"
checkNMoveFile "add_methanol.sh" "setup_files"
checkNMoveFile "${sam_name_nogro}_em_addmet.gro" "setup_files"
checkNMoveFile "${sam_name_addwat}" setup_files
checkNMoveFile "${output_gro}" setup_files
mv \#* setup_files

## Writing the job into a file
echo "${output_dir}" >> "${JOB_SCRIPT}"

## PRINTING RESULTS
echo "********** DONE *********"
echo "Can now run anisotropic NPT equilibration for this system"
echo ""
echo ""