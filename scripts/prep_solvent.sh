#!/bin/bash

# prep_solvent.sh
# The purpose of this script is to take the output from CHARMM gui to output solvent details. Furthermore, this script will run energy minimization and 500 ps NPT equilibration on the solvent so subsequent simulations would work correctly.

# Note that each solvent should have a "setup_files" where the *.itp, *.prm, and *.pdb files are placed.
# Written by: Alex K. Chew (alexkchew@gmail.com, 02/27/2018)
# REFITTED TO NANOPARTICLE PROJECT -- 05/30/2019

## ALGORITHM:
# 1. We will find volumes of a single solvent
# 2. Using that volume, expand to a box of 125 solvent molecules
# 3. Run energy minimization
# 4. Run equilibration
# 5. Output volume in the molecular_volume.info
# 6. Output final equilibrated gro file
# 7. Clean up directories

## USAGE: bash prep_solvent.sh methanol

## VARIABLES:
# $1: solvent_name: Name of the solvent

# *** UPDATES *** #
# 180227 - Completed draft of preparation solvents
# 180330 - Updated to include variables
# 180423 - Updated to enable changes to run time parameters for NPT equil

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="../bin/nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${script_dir}/${global_file_name}"

#######################
### LOCAL FUNCTIONS ###
#######################
## FUNCTION TO CHECK INTEGRITY OF SETUP FILE
# $1: Full path to setup file
function check_setup_file () {
    # DEFINING VARIABLES
    setup_file_path="$1" # file path to setup file
    molecule_name="$2"
    # PRINTING
    echo "Checking integrity of setup file........"
    echo "Setup file directory: $1"
    
    # CHECKING FILES
    check_itp="$(check_file_exist ${setup_file_path}/${molecule_name}.itp)"
    check_gro="$(check_file_exist ${setup_file_path}/${molecule_name}.gro)"
    check_prm="$(check_file_exist ${setup_file_path}/${molecule_name}.prm)"
    echo "ITP file exists: ${check_itp}"
    echo "GRO file exists: ${check_gro}"
    echo "PRM file exists: ${check_prm}"
    
    if [ "${check_itp}" == "True" ] && [ "${check_gro}" == "True" ] && [ "${check_prm}" == "True" ]; then
        echo "Integrity intact, itp, gro, and prm file is correctly located"
    else
        echo "ERROR! Check setup files. You should have an itp, gro, and prm file... stopping here."
        exit
    fi
}

### FUNCTION TO ROUND UP
# INPUTS:
#   $1: Value to round
#   $2: Number of decimal places
# OUTPUTS:
#   Rounded value
# USAGE: roundup $VALUE $NUM_DEC_PLACES
function roundup() 
{ echo $(printf %.$2f $(echo "scale=$2;(((10^$2)*$1)+1.0)/(10^$2)" | bc)
); };

### PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################
# -- USER DEFINED PARAMETERS (BEGIN) -- #
solvent_name="$1" # Name of the solvent

## DEFINING NUMBER OF DESIRED SOLVENTS
num_of_res="216" # Number of residues you want for your final .gro file

## DEFINING FUDGE FACTOR
fudge_fact="1.5"
# "1.5" # 1.5 
# 2.5 for propane

## SPECIFYING SOLVENTS WITH NO FUDGING
if [[ "${solvent_name}" == "octanol" ]]; then
    fudge_fact="0.8"
fi

## DEFINING SPECIFIC SOLVENTS THAT ARE TROUBLE ONES
if [[ "${solvent_name}" == "formate" ]]; then
    fudge_fact="2"
fi

### DEFINING LOGICALS
want_long_equil="True" # True if you want longer equilibration (1.5 ns instead of 500 ps)

## DEFINING FORCEFIELD
forcefield="$2" # -${FORCEFIELD}

# -- USER DEFINED PARAMETERS (END) -- #

## SEEING IF YOU WANT SUBMISSION
want_submit=true # true

## DEFINING NUMBER OF CORES
num_cores="28"

##################################
### DEFINING INPUT DIRECTORIES ###
##################################

## PATH TO INPUT MOLECULE
path_input_molecule="${MDLIG_SOLVENTS_FINAL}/${forcefield}/${solvent_name}"

## CHECKING INPUT FILE
check_setup_file "${path_input_molecule}" "${solvent_name}"

## DEFINING MOLECULE GRO, ITP, PRM FILES
molecule_gro_file="${solvent_name}.gro"
molecule_itp_file="${solvent_name}.itp"
molecule_prm_file="${solvent_name}.prm"

###########################
### SOLVENT DIRECTORIES ###
###########################

### DEFINING DIRECTORIES
# SOLVENT DIRECTORIES
path_solvent_dir="${NP_PREP_SOLVENT_EQUIL}/${solvent_name}" # Main directory for current solvent
path_solvent_setup_dir="${path_solvent_dir}/setup_files" # Setup directory for current solvent
path_solvent_sim_dir="${path_solvent_dir}/em_equil" # Directory for energy minimization and equilibration

###################
### MAIN SCRIPT ###
###################

## CHECKING IF THE ENERGY/MINIMIZATION DIRECTORY EXISTS
create_dir "${path_solvent_sim_dir}" -f

## GOING INTO DIRECTORY
cd "${path_solvent_sim_dir}"

######################################################
#### -- SHRINKING AND FINDING MOLECULAR VOLUME -- ####
######################################################
## RUNNING EDITCONF TO GET MOLECULAR VOLUME
gmx editconf -f "${path_input_molecule}/${molecule_gro_file}" -o "${path_solvent_sim_dir}/${solvent_name}_shrink.gro" -d 0.000 -bt cubic >/dev/null 2>&1

## READING BOX LENGTH AND BOX VOLUME
read -a box_length<<<$(read_gro_box_lengths ${solvent_name}_shrink.gro)
molecular_volume="$(compute_volume_from_box_length "${box_length[@]}")"

## FINDING TOTAL VOLUME WITH FUDGE FACTOR
new_volume=$(echo "${num_of_res}*${molecular_volume}" | bc -l)
new_length=$(awk -v vol=${new_volume} -v fudge_fact=${fudge_fact} 'BEGIN{ printf "%.4f",vol**(1/3)*fudge_fact}') # Fudging by 50%
volume_with_fudge=$(echo "${new_length}*${new_length}*${new_length}" | bc -l)

## PRINTING
echo "--- Shrinking to find molecular volume ---"
echo "Box length: ${box_length[@]}"
echo "Volume: ${molecular_volume} nm3"
echo "With ${num_of_res} molecules: ${new_volume} nm3"
echo "Using fudge factor of ${fudge_fact}, new box length is: ${new_length} nm, ${volume_with_fudge} nm3"
echo ""

## STORING INTO FILE
info_file="shrinking.info"
echo "--- Shrinking to find molecular volume ---" > "${info_file}"
echo "Box length: ${box_length[@]}" >> "${info_file}"
echo "Volume: ${molecular_volume} nm3" >> "${info_file}"
echo "With ${num_of_res} molecules: ${new_volume} nm3" >> "${info_file}"
echo "Using fudge factor of ${fudge_fact}, new box length is: ${new_length} nm, ${volume_with_fudge} nm3" >> "${info_file}"
echo ""

###############
### PACKMOL ###
###############

## DEFINING PACKMOL SCRIPT
packmol_script="${PACKMOL_SCRIPT}"

## DEFINING PACKMOL INPUT
packmol_input="packmol_cosolvents.inp"
path_packmol_input="${INPUT_PACKMOL_PATH}/${packmol_input}"

## DEFINING PDB FILE
cosolvent_pdb="${solvent_name}.pdb"

## DEFINING IMPORTANT PATHS FOR PDB FILE
path_input_cosolvent="${path_input_molecule}/prep_files/${cosolvent_pdb}"

## COPYING INPUT FILE
cp "${path_packmol_input}" "${path_solvent_sim_dir}"

## COPYING PDB FILE
cp "${path_input_cosolvent}" "${path_solvent_sim_dir}"

## DEFINING OUTPUT PDB
output_solvated_pdb="${solvent_name}_solvated.pdb"

## GETTING MAX LENGTH IN ANGS
new_length_angs=$(awk -v lengths=${new_length} 'BEGIN{ printf "%.5f",lengths*10}')

## EDITING PACKMOL FILE
# sed -i "s/_NPPDB_/${output_pdb}/g" 
sed -i "s/_OUTPUTPDB_/${output_solvated_pdb}/g" "${packmol_input}"
sed -i "s/_COSOLVENTPDB_/${cosolvent_pdb}/g" "${packmol_input}"
sed -i "s/_NUMCOSOLVENT_/${num_of_res}/g" "${packmol_input}"
# ANGS MAX
sed -i "s/_MAXANGS_/${new_length_angs}/g" "${packmol_input}"

## RUNNING PACKMOL
"${PACKMOL_SCRIPT}" < ${packmol_input}

# DEFINING NAME
gro_with_res_num="${solvent_name}_${num_of_res}.gro"

## USING EDIT CONF TO GENERATE GRO FILE
gmx editconf -f ${output_solvated_pdb} -o ${gro_with_res_num} -box "${new_length}" "${new_length}" "${new_length}"

#########################
### DEFINING TOPOLOGY ###
#########################

## DEFINING MDP FILE
input_em_mdp_file="minim_sam_gmx5.mdp"
input_equil_mdp_file="npt_double_equil_gmx5_charmm36.mdp"

## DEFINING MDP FILE
path_mdp_folder="${INPUT_MDP_PATH}/charmm36/Spherical"

## DEFINING NUMBER OF MDP FILE STEPS
mdp_equil_nsteps="5000000" # 2 ns # "2500000" # 5 ns
# 5000000 <-- 10 ns
mdp_temp="300.00" # Temperature at 300 K

## DEFINING SUBMISSION FILE
input_submit_file="submit_prep_solvents.sh"

## DEFINING OUTPUT SUBMIT FILE
output_submit_file="submit.sh"

## DEFINING TOPOLOGY FILE
topology_file="solvent.top"

## DEFINING PATH
input_path_topology="${INPUT_TOPOLOGY_PATH}/${topology_file}"

#################################
### COPYING FORCE FIELD FILES ###
#################################

## COPYING FORCEFIELD FILES 
echo "COPYING FORCE FIELD FILES, ITP, TOPOLOGY, PRM, ETC."
# Force fields
cp -r "${PATH_FORCEFIELD}/${forcefield}" "${path_solvent_sim_dir}" 
# ITP files
cp -r "${path_input_molecule}/"{${molecule_itp_file},${molecule_prm_file}} "${path_solvent_sim_dir}" 
# TOP files
cp -r "${input_path_topology}" "${path_solvent_sim_dir}/${topology_file}"
# MDP FILES
cp -r "${path_mdp_folder}/"{${input_em_mdp_file},${input_equil_mdp_file}} "${path_solvent_sim_dir}"
# SUBMISSION FILE
cp -r "${PATH2SUBMISSION}/${input_submit_file}" "${path_solvent_sim_dir}/${output_submit_file}"

## FINDING RESIDUE NAME FROM ITP FILE
resname=$(extract_itp_resname ${molecule_itp_file})

## EDITING MDP FILES
sed -i "s/NSTEPS/${mdp_equil_nsteps}/g" "${input_equil_mdp_file}"
sed -i "s/TEMPERATURE/${mdp_temp}/g" "${input_equil_mdp_file}"

## EDITING TOPOLOGY FILE
### FIXING FILES THAT WERE COPIED
sed -i "s/_FORCEFIELD_/${forcefield}/g" "${topology_file}"
sed -i "s/_SOLVENTNAME_/${solvent_name}/g" "${topology_file}"
echo "   ${resname}    ${num_of_res}" >> "${topology_file}"

## EDITING SUBMISSION FILE
## DEFINING OUTPUT NAME
output_job_name="${solvent_name}_${num_of_res}_equil"
sed -i "s/_JOB_NAME_/${output_job_name}/g" "${output_submit_file}"
sed -i "s/_NUM_CORES_/${num_cores}/g" "${output_submit_file}"
sed -i "s/_INPUT_EQUIL_MDP_FILE_/${input_equil_mdp_file}/g" "${output_submit_file}"
sed -i "s/_SOLVENT_NAME_/${solvent_name}/g" "${output_submit_file}"
sed -i "s/_NUM_OF_RES_/${num_of_res}/g" "${output_submit_file}"
sed -i "s/_TOPOLOGY_FILE_/${topology_file}/g" "${output_submit_file}"
## UPDATING SOLVENT PATH
sed -i "s#_PATH_SOLVENT_DIR_#${path_solvent_dir}#g" "${output_submit_file}"

### RUNNING ENERGY MINIMIZATION
gmx grompp -f ${input_em_mdp_file} -c "${gro_with_res_num}" -p "${topology_file}" -o "${solvent_name}_${num_of_res}_em"
gmx mdrun -v -nt 1 -deffnm "${solvent_name}_${num_of_res}_em"

### ADDING TO job list
if [[ "${want_submit}" == true ]]; then
    echo -e "--- CREATING SUBMISSION FILE: $submitScript ---"
    echo "${path_solvent_sim_dir}/${output_submit_file}" >> ${JOB_SCRIPT}
else
    ## RUNNING EQUILIBRATION
    gmx grompp -f "${input_equil_mdp_file}" -c "${solvent_name}_${num_of_res}_em.gro" -p "${topology_file}" -o "${solvent_name}_${num_of_res}_equil"
    gmx mdrun -v -nt 12 -deffnm "${solvent_name}_${num_of_res}_equil"

    ## LASTLY, COPYING EQUILIBRATED FILES TO MAIN DIRECTORY
    # GRO FILE
    cp -r "${solvent_name}_${num_of_res}_equil.gro" "${path_solvent_dir}/${solvent_name}.gro"
    # ITP FILE
    cp -r "${solvent_name}.itp" "${path_solvent_dir}/${solvent_name}.itp"
    # PRM FILE
    cp -r "${solvent_name}.prm" "${path_solvent_dir}/${solvent_name}.prm"

fi

### PRINTING SUMMARY
echo "----- SUMMARY -----"
echo "Worked on solvent: ${solvent_name} (${resname})"
echo "Total number of steps for equilibration: ${mdp_equil_nsteps}"
echo "Total solvent residues desired: ${num_of_res}"
echo "*gro, *prm, and *itp located in ${path_solvent_dir}"
echo "YOU'RE ALL GOOD TO GO! Best of luck!"