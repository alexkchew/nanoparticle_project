#!/bin/bash

# analysis_extraction_within.sh
# This script runs extraction protocol for each simulation.

# CREATED ON: 05/10/2019
# AUTHOR: Alex K. Chew (alexkchew@gmail.com)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="../bin/nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${script_dir}/${global_file_name}"


##################################
### DEFINING IMPORTANT SCRIPTS ###
##################################
bs_analysis_xtc_trunc="${PATH2SCRIPTS}/analysis_xtctruncate_within.sh"

################################
### DEFINING INPUT VARIABLES ###
################################

## DEFINING INPUT DIRECTORY (SIMULATION FOLDER WITH MANY SIMULATIONS)
input_dir_name="190510-2nm_C11_Sims_extraction"

## DEFINING GRO AND XTC FILE
gro_file="sam_prod.gro" # name of gro file
xtc_file="sam_prod.xtc" # name of xtc file
tpr_file="sam_prod.tpr" # name of tpr file

## DEFINING INPUT AND OUTPUT TIMES
first_time_ns="10000"
last_time_ns="50000"


######################
### DEFINING PATHS ###
######################
## PATH TO INPUT DIRECTORY
path_input_dir="${PATH2SIM}/${input_dir_name}"


#####################
### MAIN FUNCTION ###
#####################





## DEFINING OUTPUT DIRECTORY
output_dir_name="190225-2nm_C11_Sims"

## LOGICALS
want_self_assembled_monolayer="False" # True if you want to change 

## NOTE: If you want to change the cutting time and end time -- change the analysis_xtctruncate.sh script!
# CURRENTLY: 
#   - 10 ns - 50 ns (Normal transfer ligand builder)
#   - 0 ns - 50 ns (self_assembled_monolayer)



bs_analysis_extract="${PATH2SCRIPTS}/analysis_extraction.sh"


## RUNNING EXTRACTION PROTOCOL
bash "${bs_analysis_extract}" "${input_dir_name}" "${output_dir_name}" "${want_self_assembled_monolayer}"

## RUNNING XTC TRUNCATION PROTOCOL
bash "${bs_analysis_xtc_trunc}" True True True False "${output_dir_name}" "${want_self_assembled_monolayer}"

###############
### SUMMARY ###
###############
echo "------- SUMMARY -------"
echo "INPUT DIRECTORY: ${input_dir_name}"
echo "OUTPUT DIRECTORY: ${output_dir_name}"
echo "WANT_SELF_ASSEMBLY: ${want_self_assembled_monolayer}"
