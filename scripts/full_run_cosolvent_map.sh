#!/bin/bash

# full_run_cosolvent_map.sh
# The purpose of this script is to run 
# cosolvent mapping protoocl

# Written by: Alex K. Chew (11/02/2019)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## ENABLING EXTERNAL MODULES
shopt -s extglob
# ^ Allows you to move all except 1

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING SIMULATION DIRECTORY
simulation_dir_name="HYDROPHOBICITY_PROJECT_C11"
# simulation_dir_name="HYDROPHOBICITY_PROJECT_ROTELLO"
simulation_dir="${PATH2SIM}/${simulation_dir_name}"

## DEFINING PATH TO SIMULATION
cosolvent_sims_dir="191115-mixed_other_groups"
# "191114-mixed_other_frames_ROT2"
# "191111-mixed_other_frames_ROT"
# "191110-mixed_other_frames"
# "191104-mixed_other_frames"
# "191017-mixed_sams_most_likely_sims_other_frames"
# cosolvent_sims_dir="191017-mixed_sams_most_likely_sims"
# 
# 
path_cosolvent_sims="${PATH2SIM}/${cosolvent_sims_dir}"

## GETTING PATH ARRAY
read -a path_array <<<$(ls "${path_cosolvent_sims}/"MostNP* -d) # C11COOH C11NH3*

##########################
### DECLARING DEFAULTS ###
##########################
## DEFINING BASH CODE
bash_code="${PATH2POST_SCRIPTS}/run_cosolvent_map.sh"

###################
### MAIN SCRIPT ###
###################

## LOOPING THROUGH EACH PATH
for each_sim_path in ${path_array[@]}; do
	## RUNNING CODE
	bash "${bash_code}" "${simulation_dir}" "${each_sim_path}"

done
