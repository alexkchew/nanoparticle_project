#!/bin/bash

# full_prep_self_assembly_gold_ligand.sh
# This script runs ligand equilibration in the large scale
# Author(s):
#   Alex K. Chew (alexkchew@gmail.com)

### SCRIPT VARIABLES (prep_gold_ligand_equil.sh):
# $1: diameter
# $2: temperature
# $3: shape_type (i.e. what shape would you like?)
# $4: ligand_names: name of the ligands

# Run: bash full_prep_self_assembly_gold_ligand.sh


### SOURCING ALL VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

## PRINTING SCRIPT NAME
print_script_name

### DEFINING PATH TO SCRIPTS
SCRIPT_PATH="$PATH2SCRIPTS/prep_self_assembly_gold_ligand.sh"

# parent_database="/home/akchew/scratch/nanoparticle_project/database"
# database_name="logP_diameters.csv"

## DEFINING DATABASE PATH
parent_database="/home/akchew/bin/pythonfiles/modules/gnpdescriptors/database"
# database_name="logP_cell_uptake_zeta_potential_database_DIAMETERS.csv"
database_name="rotello_gnp_database_DIAMETERS.csv"

## DEFINING PATH TO DATABASE
path_database="${parent_database}/${database_name}"

### DEFINING GENERAL VARIABLES
## PARTICLE SHAPEwant_non_fragment
# "spherical" "hollow" "EAM
declare -a shape_type=("spherical")

# ## READING DIAMETERS
# if [ -e "${path_database}" ]; then
# 	## READING DIAMETER
# 	read -a diameter <<< $(cat ${path_database})
# 	## REMOVING FIRST ROW
# 	if [[ "${diameter[0]}" == "diameter" ]]; then
# 		diameter=("${diameter[@]:1}") 
# 	fi

# 	echo "Diameters read from: ${path_database}"
# 	echo "Diameters: ${diameter[@]}"
# 	sleep 5
# else
# 	## DIAMETER
# 	declare -a diameter=("6.50")
# fi


## READING DATABASE
diameter=( $(cut -d ',' -f1 ${path_database} ) )
diameter=("${diameter[@]:1}") 

assembly_type=( $(cut -d ',' -f2 ${path_database} ) )
assembly_type=("${assembly_type[@]:1}") 

## GETTING NEW LIGAND NAMES
declare -a ligand_names_array=()
for each_assembly in "${assembly_type[@]}"; do
	## CREATING LIGAND NAMES
	if [[ "${each_assembly}" == "C1CCSS1" ]]; then
		ligand_names_array+=("bidente")
	elif [[ "${each_assembly}" == "SCCCC" ]]; then
		ligand_names_array+=("butanethiol")
	else
		echo "Warning, assembly ${each_assembly} not found! Check script: full_prep_self_assembly_gold_ligand.sh"
	fi

done

echo "-----------------------"
echo "Diameters: "
echo "${diameter[@]}"
echo "Ligand name array:"
echo "${ligand_names_array[@]}"
echo "-----------------------"
echo "Check if this is correct, pausing 5 secs"

sleep 5

declare -a trial_num=("2")
# "1"
# "4" "5" "6"

## DEFINING LIGAND TO USE
# declare -a ligand_names=("bidente")
# "bidente" "bidente3chain" "bidente6chain"
# 
# "butanethiol"
# "bidentate2"

## DEFINING IF YOU WANT NONFRAGMENT-BASED APPROACH
want_non_fragment=true

## DEFINING REWRITE
rewrite=false
# True if you want to rewrite current existing files

###############
### LOOPING ###
###############
## LOOP NOTES
### LOOP THROUGH DIAMETER
### LOOP THROUGH SPHERICAL TYPES
### LOOP THROUGH TRIAL NUMBER

## LOOPING THROUGH SPHERICAL TYPES
for current_shape_type in "${shape_type[@]}"; do

	## LOOPING THROUGH DIAMETERS
	for diam_index in "${!diameter[@]}"; do 
		## GETTING DIAMETER
		current_diam="${diameter[diam_index]}"
		## GETTING CURRENT LIGAND
		current_lig="${ligand_names_array[diam_index]}"
        ## LOOPING THROUGH TRIAL NUMBERS
        for current_trial in "${trial_num[@]}"; do
            ### RUNNING THE SCRIPT
            echo "RUNNING THE FOLLOWING COMMAND: (pausing for 3 seconds)"
            echo "bash "${SCRIPT_PATH}" "${current_diam}" "${current_shape_type}" "${current_trial}" ${current_lig} ${want_non_fragment} ${rewrite}"
            # sleep 3
            bash "${SCRIPT_PATH}" "${current_diam}" "${current_shape_type}" "${current_trial}" "${current_lig}" "${want_non_fragment}" "${rewrite}"

            # SLEEPING
            # sleep 5
        done
    done
done


### PRINTING SUMMARY
echo "------SUMMARY------"
echo "Shape type: ${shape_type[@]}"
echo "Sphere diameter: ${diameter[@]}"
echo "Ligand names: ${ligand_names_array[@]}"
echo "Trial numbers: ${trial_num[@]}"

