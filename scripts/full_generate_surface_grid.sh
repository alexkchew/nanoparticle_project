#!/bin/bash

# full_generate_surface_grid.sh
# The purpose of this script is to generate a surface grid 
# for multiple surfaces

# Written by: Alex K. Chew (10/16/2019)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## ENABLING EXTERNAL MODULES
shopt -s extglob
# ^ Allows you to move all except 1

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING SIMULATION DIRECTORY
simulation_dir_name="HYDROPHOBICITY_PROJECT_C11"
# "HYDROPHOBICITY_PROJECT_ROTELLO"
# "HYDROPHOBICITY_PROJECT_C11"
simulation_dir="${PATH2SIM}/${simulation_dir_name}"

## GETTING PATH ARRAY
read -a path_array <<<$(ls "${simulation_dir}/"EAM* -d)
# C11NH3

##########################
### DECLARING DEFAULTS ###
##########################
## DEFINING BASH CODE
bash_generate_grid="${main_dir}/generate_surface_grid.sh"
## DEFINING DESIRED NP INDEX
most_likely_index="1" # First most likely index

###################
### MAIN SCRIPT ###
###################

## LOOPING THROUGH EACH PATH
for each_sim_path in ${path_array[@]}; do
	## RUNNING CODE
	bash "${bash_generate_grid}" "${each_sim_path}" "${most_likely_index}"

done
