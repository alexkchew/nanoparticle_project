#!/bin/bash

# extraction_of_GNP_alone.sh
# This function extracts the gold nanoparticle by itself.
# The idea is to use the gold nanoparticle for subsequent simulations.
#
# Written by: Alex K. Chew (04/16/2020)
# 
# USAGE:
#	bash extraction_of_GNP_alone.sh PATH_TO_SIM_DIR PREFIX
# VARIABLES:
#	$1: input simulation path
#	$2: input prefix
#	$3: distance between the GNP and the edge of the box
#	$4: true/false for rewriting current available gro files

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

## DEFINING PATH TO SIMULATION
sim_path="${1-/home/shared/np_hydrophobicity_project/simulations/20200401-Renewed_GNP_sims_with_equil/MostlikelynpNVTspr_50-EAM_300.00_K_2_nmDIAM_C11COOH_CHARMM36jul2017_Trial_1_likelyindex_1}"

## DEFINING PREFIX
prefix="${2-sam_prod}"

## DISTANCE TO EDGE (nm)
dist_to_edge="${3-1}"

## DEFINING REWRITE
rewrite="${4-false}"
# true

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING OUTPUT DIRECTORY
output_prefix="gnp_alone"
output_dir="${output_prefix}"

## GETTING INDEX FILE
index_file="${output_dir}.ndx"

## PATH TO OUTPUT DIRECTORY
path_output_dir="${sim_path}/${output_dir}"

## GETTING INPUT FILES
input_gro="${prefix}.gro"
input_tpr="${prefix}.tpr"

## DEFINING OUTPUT GRO
output_gro="${output_prefix}.gro"
output_gro_shrink="${output_prefix}-d_${dist_to_edge}.gro"
output_tpr="${output_prefix}.tpr"
path_output_gro="${path_output_dir}/${output_gro}"
path_output_gro_shrink="${path_output_dir}/${output_gro_shrink}"

## DEFINING OUTPUT SUMMARY
output_summary="${output_prefix}.summary"

## DEFINING FORCEFIELD
forcefield="${FORCEFIELD}"

###################
### MAIN SCRIPT ###
###################

## CHECKING IF GRO EXISTS
if [ ! -e "${path_output_gro_shrink}" ] || [ "${rewrite}" == true ]; then

## GENERATING GRO FILE WITH ONLY GNP
if [ ! -e "${path_output_gro}" ] || [ "${rewrite}" == true ]; then

## GOING TO DIRECTORY
cd "${sim_path}"

## CREATING DIRECTORY
mkdir -p "${path_output_dir}"

## GETTING BASENAME
sim_basename="$(basename ${sim_path})"

## SEEING IF PLANAR SAM
if [[ "${sim_basename}" == "FrozenGoldPlanar"* ]] || \
    [[ "${sim_basename}" == "FrozenPlanar"* ]] || \
[[ "${sim_basename}" == "NVTspr"* ]]; then
    planar_sam=true
    ## GOLD RESIDUE NAME
    gold_residue_name="AUI"
else
    planar_sam=false
    ## GOLD RESIDUE NAME
    gold_residue_name="AUNP"
fi

echo "Planar SAM: ${planar_sam}"

## EXTRACTING LIGAND NAME
if [[ "${planar_sam}" == false ]]; then
    read -a extract_array <<< $(extract_output_name_gnp_water_spr "${sim_basename}")

    ## FINDING LIGAND NAMES
    lig_name="${extract_array[1]}"
else
    ## FINDING LIG NAMES
    lig_name=$(extract_lig_name_from_dir ${sim_basename})
fi

## DEFINING LIGAND NAME ARRAY
read -a ligand_name_array <<< "$(str2array_by_delim "${lig_name}" ",")"

## STORING LIG RES NAME
declare -a lig_res_name_array=()

## LOOPING THROUGH EACH LIGAND
for lig_name in ${ligand_name_array[@]}; do

    ## DEFINING ITP FILE
    lig_itp_file="${lig_name}.itp"

    ## DEFINING ITP FILE
    if [[ "${planar_sam}" == true ]]; then
        lig_itp_file="${forcefield}/${lig_itp_file}"
    fi

    ## CHECKING IF PATH EXISTS
    stop_if_does_not_exist "${lig_itp_file}"

    ## FINDING LIGAND RESIDUE NAME
    lig_residue_name="$(itp_get_resname ${lig_itp_file})"

    ## STORING LIG RESIDUE NAME
    lig_res_name_array+=( "${lig_residue_name}" )

done

residue_name_spaces="$(join_array_to_string " " ${lig_res_name_array[@]})"
residue_name_underscore="$(join_array_to_string "_" "${lig_res_name_array[@]}")"
residue_name_comma="$(join_array_to_string "," "${lig_res_name_array[@]}")"

## PRINTING LIG NAME 
echo "GOLD_NAME ${gold_residue_name}" > "${path_output_dir}/${output_summary}"
echo "LIG_NAME ${lig_name}" >> "${path_output_dir}/${output_summary}"
echo "LIG_RESNAME ${residue_name_comma}" >> "${path_output_dir}/${output_summary}"

## CREATING INDEX FILE
make_ndx_gold_with_ligand "${sim_path}/${input_tpr}" \
                          "${sim_path}/${index_file}"   \
                          "${residue_name_spaces}"   \
                          "${gold_residue_name}"  

## DEFINING COMBINED NAME
combined_name="GNP_LIGANDS"
# "${gold_residue_name}_${residue_name_underscore}"

## GMX TRJCONV
gmx trjconv -s "${input_tpr}" \
			-f "${input_gro}" \
			-o "${path_output_gro}" \
			-n "${index_file}" \
			-pbc mol \
			-center << INPUTS
${gold_residue_name}
${combined_name}
INPUTS

## GETTING TPR
gmx convert-tpr -s "${input_tpr}" \
				-n "${index_file}" \
				-o "${path_output_dir}/${output_tpr}" << INPUTS
${combined_name}
INPUTS


fi

## EDIT CONF
if [[ "${planar_sam}" == false ]]; then
    gmx editconf -f "${path_output_gro}" \
    			 -o "${path_output_gro_shrink}" \
    			 -d "${dist_to_edge}" \
    			 -bt "cubic"

fi

## CLEANING EXTRA FILES
cd "${sim_path}/${output_dir}"
rm -f \#* || true


else
	echo "${path_output_gro_shrink} exists!"

fi


