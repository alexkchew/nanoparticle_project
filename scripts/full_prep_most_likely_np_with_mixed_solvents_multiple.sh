#!/bin/bash

# full_prep_most_likely_np_with_mixed_solvents_multiple.sh
# This script runs prep_most_likely_np_with_mixed_solvents for multiple solvent systems. 

## TODO: NEED TO UPDATE SCRIPT ***

# Written by Alex K. Chew (10/05/2019)

## USAGE:
#   bash full_prep_most_likely_np_with_mixed_solvents_multiple.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"


### DEFINING PATH TO SCRIPT
SCRIPT_PATH="$PATH2SCRIPTS/prep_most_likely_np_with_mixed_solvents_multiple.sh"

###########################################
### USER-DEFINED VARIABLES 
###########################################

## DEFINING SIMULATION TO GET DETAILS FROM
simulation_dir_name="HYDROPHOBICITY_PROJECT_ROTELLO"
# simulation_dir_name="HYDROPHOBICITY_PROJECT_C11"
# 
# 
# "190509-2nm_C11_Sims_updated_forcefield"

## DEFINING NEW SIMULATION DIRECTORY NAME
# output_simulation_dir="191005-Testing_multiple_cosolvents_NVT_ensemble"
output_simulation_dir="191123-mixed_other_groups_ROT4_5"
# "191017-mixed_sams_most_likely_sims_other_frames"
# "191006-Testing_multiple_cosolvents_NVT_ensemble_3_5_mol_frac"
# "190916-mixed_solvents_multiple_frames"

## DEFINING LIGAND NAMES
declare -a ligand_names=("ROT003" "ROT004")  # "dodecanethiol" "C11COOH" 
#  "ROT005" "ROT006" "ROT007" "ROT008" "ROT009"
# "C11NH2" "C11CONH2" "C11OH"
# "ROT002"
# "C11COO"
# "ROT001"
# "C11NH3"
# "C11NH3"
# "dodecanethiol" 
# "C11COO"
#  "dodecanethiol"
# "C11COO"
#  "NONE" "dodecanethiol"
# "C11COOH" 
# "NONE"
# "C11COOH" "dodecanethiol"
#  NONE
#  C11COOH
#  dodecanethiol

## DEFINING SOLVENTS
declare -a solvent_names=("aceticacid,formate,methylammonium,propane") #  
# "methanol,formicacid,phenol"
# methanol,formicacid
# methanol,formicacid,phenol
# "formicacid" "formamide" "dimethylsulfide" "phenol" "toluene" "indole" "methanethiol"
# "methanol"

## DEFINING MASS FRACTION
declare -a cosolvent_perc=("1")  # "3" "5" "0.5"
# "1"
# "1"
# "50" 90  "90" "50"
# 50
## DEFINING DESIRED COSOLVENT  FRAME
declare -a cosolvent_frame=("10000" "9900" "9800" "9700" "9600" "9500") # "10000"
# "10000"
#  "9900" "9800" "9700" "9600" "9500"
# "10000"
# "9600" "9700" "9800" "9900"
# "9000" "9200" "9400" "9600" "
# "9500" "9600" "9700" "9800" "9900"
# "9500" "10000"
# "9750" "9250"
# "10000" "9500" "9000"

# in PS  "9500" "9000" "10000" 
# "10000" "9500" "9000" "8500"
# "10000" 

## DEFINING BOX LENGTH SIZE
declare -a box_length_sizes=("7")

###########################################

## LOOPING THROUGH EACH SOLVENT
for each_ligand in "${ligand_names[@]}"; do
    ## LOOPING THROUGH EACH MASS FRACTION
    for each_cosolvent in "${solvent_names[@]}"; do
        ## LOOPING THROUGH EACH BOX SIZE
        for each_mass_frac in "${cosolvent_perc[@]}"; do
            ## LOOPING THROUGH EACH FRAME
            for each_frame in "${cosolvent_frame[@]}"; do
                ## PRINTING
                echo "RUNNING THE FOLLOWING COMMAND:"
                ## LOOPING THROUGH NO LIGANDS
                if [[ "${each_ligand}" == "NONE" ]]; then
                    ## LOOPING THROUGH BOX LENGTH SIZES
                    for initial_box_length in "${box_length_sizes[@]}"; do
                        echo "bash "${SCRIPT_PATH}" "${simulation_dir_name}" "${output_simulation_dir}" "${each_ligand}" "${each_cosolvent}" "${each_mass_frac}" "${each_frame}" "${initial_box_length}""
                        ## RUNNING
                        bash "${SCRIPT_PATH}" "${simulation_dir_name}" "${output_simulation_dir}" "${each_ligand}" "${each_cosolvent}" "${each_mass_frac}" "${each_frame}" "${initial_box_length}"
                    done
                
                else

                    echo "bash "${SCRIPT_PATH}" "${each_solvent}" "${each_mass_frac}" "${each_box_size}" "${each_frame}""
                    ## RUNNING
                    bash "${SCRIPT_PATH}" "${simulation_dir_name}" "${output_simulation_dir}" "${each_ligand}" "${each_cosolvent}" "${each_mass_frac}" "${each_frame}"
                fi
                
            done
        done
    done
done

### PRINTING SUMMARY
echo "------SUMMARY------"
echo "Ligand names: ${ligand_names[@]}"
echo "Cosolvent frames: ${cosolvent_frame[@]}"
echo "Created systems for solvents: ${solvent_names[@]}"
echo "Cosolvent mole fractions: ${cosolvent_perc[@]}"
