#!/bin/bash 
# compute_np_rdf.sh
# The purpose of this script is to compute RDFs for 
# nanoparticle with counterions and so on. 
# The whole idea is to get RDFs and get the distance 
# information required for the z-dimension. 
# 
# ASSUMPTIONS:
#   - The residue name is extracted from the simulation path name

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

### FUNCTION TO JOIN ARRAY TO A STRING
# USAGE: join_array_to_string , "${data[@]}"
function join_array_to_string () {
  local IFS="$1"
  shift
  echo "$*"
}


#######################
### INPUT VARIABLES ###
#######################

## DEFINING SIMULATION PATH
sim_path="/home/akchew/scratch/nanoparticle_project/simulations/191221-Rerun_all_EAM_models_1_cutoff/EAM_300.00_K_2_nmDIAM_ROT001_CHARMM36jul2017_Trial_1"

# sim_path="/home/akchew/scratch/nanoparticle_project/simulations/191221-Rerun_all_EAM_models_1_cutoff/EAM_300.00_K_2_nmDIAM_ROT006_CHARMM36jul2017_Trial_1"
# C10
sim_path="/home/akchew/scratch/nanoparticle_project/simulations/ROT_WATER_SIMS/EAM_300.00_K_2_nmDIAM_ROT012_CHARMM36jul2017_Trial_1"
# C1
sim_path="/home/akchew/scratch/nanoparticle_project/simulations/ROT_WATER_SIMS/EAM_300.00_K_2_nmDIAM_ROT001_CHARMM36jul2017_Trial_1"
# Bn
sim_path="/home/akchew/scratch/nanoparticle_project/simulations/ROT_WATER_SIMS_MODIFIEDFF/EAM_300.00_K_2_nmDIAM_ROT017_CHARMM36jul2017mod_Trial_1"

## DEFINING PREFIX
input_prefix="sam_prod"

## RESIDUE NAME TO EXTRACT FROM
itp_file="sam.itp"

## DEFINING FIRST FRAME TO LOOK AT
initial_frame="10000"

## DEFINING LAST FRAME TO LOOK AT
last_frame="50000"

## DEFINING MAXIMUM RADIUS
rmax="6" # nm

## DEFINING BIN WIDTH
bin_width="0.02"

## DEFINING RECALC
want_recalc=true
# want_recalc=true

## DEFINING IF YOU WANT TO SKIP FRAMES
want_skip_frames=true
frame_rate="10" # SKIP EVERY 10 FRAMES

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING PYTHON RDF CODE
python_rdf_code="${PYTHONFILES}/modules/gmx_extraction_tools/plot_rdf.py"

## DEFINING TPR AND XTC FILE
tpr_file="${input_prefix}.tpr"
xtc_file="${input_prefix}.xtc"

## GENERATING NEW XTC FILE
if [[ "${want_skip_frames}" == true ]]; then
    new_xtc_file="${input_prefix}_skip_${frame_rate}.xtc"
fi

## DEFINING RESIDUE NAME
gold_residue_name="AUNP"

## DEFINING WATER AND COUNTERIONS
water_residue_name="SOL"
counter_ion_residue_names="NA CL"

## OUTPUT FILES
output_prefix="np_rdf"
output_AUNP_lig_rdf="${output_prefix}-AUNP_lig.xvg"
output_AUNP_water_rdf="${output_prefix}-AUNP_SOL.xvg"
output_AUNP_ions_rdf="${output_prefix}-AUNP_counterions.xvg"
output_AUNP_ligN_rdf="${output_prefix}-AUNP_lig_nitrogens.xvg"
output_AUNP_gold_rdf="${output_prefix}-AUNP_gold.xvg"

## OUTPUT VOLUME
output_full_volume="${output_prefix}_full_volume.xvg"
output_volume="${output_prefix}_np_volume.xvg"

## DEFINING OUTPUT FILE ARRAY
declare -a output_file_array=("${output_AUNP_gold_rdf}" \
                              "${output_AUNP_water_rdf}" \
                              "${output_AUNP_ions_rdf}" \
                              "${output_AUNP_lig_rdf}" \
                             )

## DEFINING INDEX FILE
index_file="${output_prefix}.ndx"

## DEFINING OUTPUT IMAGE
output_image="${output_prefix}.png"

###################
### MAIN SCRIPT ###
###################

## GOING TO MAIN PATH
cd "${sim_path}"

## FINDING LIGAND NAME
ligand_name="$(itp_get_residue_name_within_atom ${itp_file})"

## CREATING INDEX FILE
gmx make_ndx -f "${tpr_file}" -o "${index_file}" << INPUTS
keep 0
r ${gold_residue_name}
r ${water_residue_name}
r ${counter_ion_residue_names}
r ${ligand_name}
q
INPUTS

# r ${ligand_name} & a N*

## GENERATING XTC FILE
if [[ "${want_skip_frames}" == true ]]; then
    if [[ ! -e "${new_xtc_file}" ]] || [[ "${want_recalc}" == true ]]; then
## CREATING NEW XTC FILE
gmx trjconv -s "${tpr_file}" \
            -f "${xtc_file}" \
            -o "${new_xtc_file}" \
            -skip "${frame_rate}" \
            -n "${index_file}" \
            -center \
            -b "${initial_frame}" \
            -pbc mol << INPUTS
${gold_residue_name}
System
INPUTS
    fi
    ## DEFINING XTC FILE
    xtc_file="${new_xtc_file}"
fi

## DEFINING SELECTION TYPE
selrpos_type="whole_res_com" # Whole center of mass
selpos_type="atom"

## GETTING VOLUME OF SYSTEM
gmx energy -f ${input_prefix}.edr \
           -skip 100 \
           -o "${output_full_volume}" \
           -b "${initial_frame}" << INPUTS
Volume

INPUTS

## GETTING VOLUME
echo "Computing NP volume!"
gmx sasa -f "${xtc_file}" \
         -s "${tpr_file}" \
         -n "${index_file}" \
         -tv "${output_volume}" << INPUTS
group ${gold_residue_name} or group ${ligand_name}
INPUTS

## LOOPING THROUGH EACH OUTPUT FILE
for index in $(seq 1 ${#output_file_array[@]}); do
    ## DEFINING INDEX FOR ARRAY
    array_index="$((index-1))"
    ## DEFINING CURRENT OUTPUT
    current_output_file="${output_file_array[array_index]}"

    ## GETTING INDEX
    current_idx="$((index))" # +1
    
    ## SEEING IF EXISTS
    if [[ "${want_recalc}" == true ]] || [[ ! -e "${current_output_file}" ]]; then
    
        ## COMPUTING RDF
        gmx rdf -f "${xtc_file}" -s "${tpr_file}" -pbc \
                -seltype "${selpos_type}" \
                -selrpos "${selrpos_type}" \
                -bin "${bin_width}" \
                -n "${index_file}" \
                -o "${current_output_file}" \
                -ref 1 \
                -sel "${current_idx}" \
                -rmax "${rmax}" \

    fi
done

## REMOVING ANY EXTRA FILES GENERATED BY BACKUPS
rm -f \#*

## JOINING ARRAY TO STRING
rdf_xvg_input=$(join_array_to_string , "${output_file_array[@]}")

## RUNNING RDF CODE
python3.6 "${python_rdf_code}" --path "${sim_path}" \
                               --rdf_xvg "${rdf_xvg_input}" \
                               --image "${output_image}" \
                               --separated


