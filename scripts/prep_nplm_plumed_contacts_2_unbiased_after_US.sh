#!/bin/bash

# prep_nplm_plumed_contacts_2_unbiased_after_US.sh
# The purpose of this script is to run unbiased simulations 
# after performing plumed contacts code. 
# The idea would be to copy all the production simulations, then 
# perform unbiased simulations to observe trends. 
# For instance, for C1 ligands, we observe lipids that 
# are extracted. It may be helpful to quantify the 
# number of lipid contacts total. 
# 
# Written by: Alex K. Chew (06/19/2020)


#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

### FORWARD SIMULATIONS
## DEFINING INPUT SIMULATION DIR
# "20200615-US_PLUMED_rerun_with_10_spring_stampede"
# "20200615-US_PLUMED_rerun_with_10_spring"

## DEFINING INPUT DIRECTORY
# input_directory="UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

# "UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

input_sim_dir_name="20200615-US_PLUMED_rerun_with_10_spring_extended"
# input_directory="UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-50000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"
input_directory="UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
input_directory="UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"


## FOR BENZENE
# input_sim_dir_name="20200822-Hydrophobic_contacts_PMF_Bn_new_FF"
# input_directory="UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-50000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"

## DEFINING OUTPUT SIM DIRECTORY
# output_sim_dir_name="20200619-Unbiased_after_plumed_US"
# output_sim_dir_name="20200713-Unbiased_after_plumed_US_20ns"
output_sim_dir_name="20200925-Hydrophobic_contacts_Unbiased_sims_50ns"
# "20200822-Hydrophobic_contacts_PMF_Bn_new_FF_UNBIASED_SIMS"

## DEFINING INPUT DISTANCE
declare -a contacts_array=("40.0")
#  "150.0"
#  "120.0"
# "130"
# "100"

## DEFINING TEMPERATURE
temperature="300.00"

## DEFINING DEBUG
want_debug=false
# true

## DEFINING TIME ARRAY
declare -a time_array=("80000.000")
# "50000.000"
# "10000.000" "15000.000"
# "20000.000"
# "10000.000" "15000.000" 

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING NUMBER OF CORES, ETC.
## RUN INFORMATION
num_cores="28"

## DEFINING EM NUMBER OF CORES
em_num_cores="28"

## DEFINING MD RUN COMMAND
mdrun_command="gmx_mpi mdrun -ntomp"

## DEFINING SUFFIX
mdrun_command_suffix=""

## DEFINING GROMACS COMMAND
gromacs_command="gmx_mpi"

## DEFINING MAX WARN
num_max_warn=5

########################
### DEFINING OUTPUTS ###
########################

## DEFINING OUTPUT NAME
output_prefix="nplm"

########################
### MDP FILE DETAILS ###
########################

## DEFINING MDP FILE
mdp_em="em.mdp"
mdp_equil="plumed_us_equil.mdp"
# "umbrella_sampling_equil.mdp"
mdp_prod="unbiased_prod.mdp"

## DEFINING MDP DETAILS
mdp_dt="0.002"

## NUMBER OF STEPS
if [[ ${want_debug} == true ]]; then
    mdp_equil_nsteps="1000" # 500 ps
    mdp_prod_nsteps="1000" # 50 ns production
else
    mdp_equil_nsteps="250000" # 500 ps
    mdp_prod_nsteps="25000000" # 50 ns production
    # "5000000" # 10 ns
    # "25000000" # 50 ns production
fi

if [[ ${input_directory} = "USrev"* ]]; then
	want_reverse=true
else
	want_reverse=false
fi


## COMPUTING TOTAL TIME IN PS
total_time_ps=$(awk -v dt=${mdp_dt} \
					-v mdp_time=${mdp_prod_nsteps} \
					'BEGIN{ printf "%.3f", dt * mdp_time }')

######################
### DEFINING PATHS ###
######################

## DEFINING INPUT DIRECTORY
path_input_dir="${PATH2NPLMSIMS}/${input_sim_dir_name}/${input_directory}"

## DEFINING OUTPUT DIR
path_output_dir="${PATH2NPLMSIMS}/${output_sim_dir_name}/${output_directory}"

## CHECKING IF EXISTS
stop_if_does_not_exist "${path_input_dir}"

## MDP FILE INFORMATION
path_mdp_parent="${INPUT_MDP_PATH}/charmm36"
path_mdp_dir="${path_mdp_parent}/lipid_bilayers"

####################################
### DEFINING DIRECTORY STRUCTURE ###
####################################

## FOR INPUT CONFIGS INITIAL
path_input_dir_input_files="${path_input_dir}/1_input_files"
path_input_dir_configs="${path_input_dir}/2_configurations"
path_input_dir_sims="${path_input_dir}/4_simulations"

## DEFINING PREFIX
input_prefix="nplm_prod"

## DEFINING GRO FILES
config_gro="${input_prefix}.gro"
config_xtc="${input_prefix}.xtc"
config_tpr="${input_prefix}.tpr"

## DEFINING TOP FILE
input_top="nplm.top"
## DEFINING INPUT INDEX
index_file="nplm.ndx"
## DEFINING TOPPAR
toppar_file="toppar"

## DEFINING COVAR
input_plumed_dat="${input_prefix}_plumed_input.dat"
input_covar_dat="${input_prefix}_COVAR.dat"
output_plumed_dat="${output_prefix}_equil_plumed_input.dat"
output_covar_dat="${output_prefix}_equil_COVAR.dat"

#######################
### SUBMISSION FILE ###
#######################

## DEFINING INPUT SUBMISSION
input_submit_file="submit_run_nplm_unbiased_sims_after_US_plumed.sh" ## NEED TO CHANGE SUBMIT FILE
output_submit_file="submit.sh"

## DEFINING INPUT RUN FILE
input_run_file="run_nplm_unbiased_sims_after_US_plumed.sh"
output_run_file="run.sh"

## LOOPING
for time_stamp in ${time_array[@]}; do

	## LOOPING THROUGH THE CONTACTS
	for contacts in ${contacts_array[@]}; do

		####################
		### OUTPUT NAMES ###
		####################

		## DEFINING PREFIX
		prefix="NPLM_unb"

		## DEFINING OUTPUT NAME
		output_directory="${prefix}-${contacts}-${time_stamp}_ps-${input_directory}"
		echo "Output name: ${output_directory}"

		## DEFINING OUTPUT DIR
		path_output_dir="${PATH2NPLMSIMS}/${output_sim_dir_name}/${output_directory}"

		## DEFINING PATH TO INPUT GRO
		path_gro="${path_input_dir_sims}/${contacts}/${config_gro}"
		path_xtc="${path_input_dir_sims}/${contacts}/${config_xtc}"
		path_tpr="${path_input_dir_sims}/${contacts}/${config_tpr}"

		###################
		### MAIN SCRIPT ###
		###################

		## MAKING DIRECTORY
		create_dir "${path_output_dir}" -f

		## GOING TO DIRECTORY
		cd "${path_output_dir}"

		## COPYING OVER GRO FILE
${gromacs_command} trjconv \
				   -s "${path_tpr}" \
				   -f "${path_xtc}" \
				   -o "${output_prefix}.gro" \
				   -pbc mol \
				   -dump "${time_stamp}" << INPUTS
System
INPUTS

		# cp -r "${path_gro}" "${output_prefix}.gro"

		## COPYING ALL FORCE FIELDS ASSOCIATED WITH THE TOP FILE
		copy_top_include_files "${path_input_dir_input_files}/${input_top}" "${path_output_dir}"
		cp "${path_input_dir_input_files}/${input_top}" "${path_output_dir}"

		## COPYING INDEX FILE
		cp "${path_input_dir_input_files}/${index_file}" "${path_output_dir}"

		## COPYING MDP FILE
		cp -r "${path_mdp_dir}"/{${mdp_em},${mdp_equil},${mdp_prod}} "${path_output_dir}"

		## COPYING COVAR
		cp -r "${path_input_dir_sims}/${contacts}/${input_plumed_dat}" "${path_output_dir}/${output_plumed_dat}"

		## EDITING COVAR OUTPUT FILE
		sed -i "s/${input_covar_dat}/${output_covar_dat}/g" "${output_plumed_dat}"

		########################
		### EDITING MDP FILE ###
		########################
		## EDITING MDP FILE
		sed -i "s#_NSTEPS_#${mdp_equil_nsteps}#g" "${mdp_equil}"
		## EDITING TEMPERATURE
		sed -i "s#_TEMPERATURE_#${temperature}#g" "${mdp_equil}"

		## EDITING PRODUCTION
		sed -i "s#_DT_#${mdp_dt}#g" "${mdp_equil}" "${mdp_prod}"
		sed -i "s#_NSTEPS_#${mdp_prod_nsteps}#g" "${mdp_prod}"
		sed -i "s#_TEMPERATURE_#${temperature}#g" "${mdp_prod}"

		###############################
		### EDITING SUBMISSION FILE ###
		###############################
		
		## COPYING GENERAL RC
		cp -r "${PATH_GENERAL_RC}" "${path_output_dir}"

		## COPYING RUN FILE
		cp "${PATH2SUBMISSION}/${input_run_file}" "${output_run_file}"
		## COPYING SUBMISSION FILE
		cp "${PATH2SUBMISSION}/${input_submit_file}" "${output_submit_file}"

		## DEFINING MDP FILE ARRAY
		declare -a mdp_file_array=("${mdp_em}" "${mdp_equil}" "${mdp_prod}")
		declare -a plumed_array=("None" "${output_plumed_dat}" "None")
		declare -a output_prefix_array=("${output_prefix}_em" "${output_prefix}_equil" "${output_prefix}_prod")

		## CONVERTING ARRAY TO STRINGS
		mdp_file_array_string=$(join_array_to_string " " ${mdp_file_array[@]})
		output_prefix_array_string=$(join_array_to_string " " ${output_prefix_array[@]})
		plumed_array_string=$(join_array_to_string " " ${plumed_array[@]})

		## EDITING RUN FILE
		sed -i "s/_RCFILE_/${GENERAL_RC_NAME}/g" "${output_run_file}"

		sed -i "s/_MDP_FILE_ARRAY_/${mdp_file_array_string}/g" "${output_run_file}"
		sed -i "s/_SIM_PREFIX_ARRAY_/${output_prefix_array_string}/g" "${output_run_file}"
		sed -i "s/_PLUMEDARRAY_/${plumed_array_string}/g" "${output_run_file}"

		## EDITING TOP, ETC.
		sed -i "s#_INPUTTOPFILE_#${input_top}#g" "${output_run_file}"
		sed -i "s#_MAXWARN_#${num_max_warn}#g" "${output_run_file}"
		sed -i "s#_OUTPUTPREFIX_#${output_prefix}#g" "${output_run_file}"
		sed -i "s#_INDEXFILE_#${index_file}#g" "${output_run_file}"

		## EDITING TOTAL TIME
		sed -i "s#_TOTALTIMEPRODPS_#${total_time_ps}#g" "${output_run_file}"

		## EDITING SUBMISSION FILE
		sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${output_submit_file}"
		sed -i "s/_JOB_NAME_/${output_directory}/g" "${output_submit_file}"
		sed -i "s#_MDRUNCOMMAND_#${mdrun_command}#g" "${output_submit_file}"
		sed -i "s#_MDRUNCOMMANDSUFFIX_#${mdrun_command_suffix}#g" "${output_submit_file}"
		sed -i "s#_RUNSCRIPT_#${output_run_file}#g" "${output_submit_file}"
		sed -i "s#_GROMACSCOMMAND_#${gromacs_command}#g" "${output_submit_file}"

		##########################
		### ADDING TO JOB LIST ###
		##########################

		## PRINTING
		echo "${path_output_dir}/${output_submit_file}" >> "${JOB_SCRIPT}"

		## CREATING SUMMARY FILE
summary_text="
--- INPUT FILE INFORMATION ---
Input parent directory: ${input_sim_dir_name}
Input directory: ${input_sim_dir_name}
Time stamp: ${time_stamp} ps

--- OUTPUT FILE INFORMATION ---
Output parent directory: ${output_sim_dir_name}
Number of contacts: ${contacts}

--- MDP INFORMATION ---
Total equal steps: ${mdp_equil_nsteps}
Total prod steps: ${mdp_prod_nsteps}
"


		## PRINTING
		summary_file="transfer_summary.txt"
		echo "${summary_text}" > "${summary_file}"
		cat "${summary_file}"



	done

done









