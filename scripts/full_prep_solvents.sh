#!/bin/bash

# full_prep_solvents.sh
# This script runs prep_solvents.sh for multiple solvent systems. 

# Written by Alex K. Chew (09/13/2019)

## USAGE:
#   bash full_prep_solvents.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

### DEFINING PATH TO SCRIPT
SCRIPT_PATH="$PATH2SCRIPTS/prep_solvent.sh"

###########################################
### USER-DEFINED VARIABLES 
###########################################

## DEFINING SOLVENTS
declare -a solvent_names=("octanol")
# "dmso"
# "octanol"
# "dmso" 
# "formate"
# "methylammonium"  "aceticacid"
# ("formicacid" "formamide" "dimethylsulfide" "phenol" "toluene" "indole" "methanethiol") #  

## DEFINING FORCEFIELD
forcefield="charmm36-jul2020.ff"

## LOOPING THROUGH EACH SOLVENT
for each_solvent in "${solvent_names[@]}"; do
    ## PRINTING
    echo "RUNNING THE FOLLOWING COMMAND:"
    echo "bash "${SCRIPT_PATH}" "${each_solvent}" ${forcefield}"
    ## RUNNING
    bash "${SCRIPT_PATH}" "${each_solvent}" "${forcefield}"
done


### PRINTING SUMMARY
echo "------SUMMARY------"
echo "Created equilibrated systems for solvents: ${solvent_names[@]}"
