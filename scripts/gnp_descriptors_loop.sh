#!/bin/bash

# gnp_descriptors_loop.sh
# This script runs gnp_descriptors_switch_solvents.sh for multiple solvent systems. 

## TODO: NEED TO UPDATE SCRIPT ***

# Written by Alex K. Chew (11/25/2020)

## USAGE:
#   bash gnp_descriptors_loop.sh
# Or as a job:
#	sbatch submit_gnp_descriptors_loop.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

### DEFINING PATH TO SCRIPT
SWITCH_SOLVENT_SCRIPT="${PATH2SCRIPTS}/gnp_descriptors_switch_solvents.sh"
HYDRATION_SCRIPT="${PATH2SCRIPTS}/gnp_descriptors_hydration_sims.sh"
HYDRATION_ANALYSIS_SCRIPT="${PATH2SCRIPTS}/gnp_descriptors_hydration_analysis.sh"

###########################################
### USER-DEFINED VARIABLES 
###########################################

## DEFINING SIMULATION TO GET DETAILS FROM
path_to_sim="${PATH2SIM}"
## DEFINING SIMULATION NAME
simulation_dir_name="20201111-GNPs_database_water_sims-try28_all_ligs_completed"
# simulation_dir_name="20201111-GNPs_database_water_sims-try28_subset_ligs-retry"
simulation_dir_name="20201216-GNPs_database_water_sims-try28_all_ligs_revive_failed"
simulation_dir_name="20201216-GNPs_database_water_sims-try28_all_ligs_revive_failed_try2"
# simulation_dir_name="20201216-GNPs_database_water_sims-try28_all_ligs_revive_failed_try3"
simulation_dir_name="20210111-Rerun_GNPs_newGNPs"
simulation_dir_name="20210111-Rerun_GNPs"
simulation_dir_name="20210111-Rerun_GNPs_ROTs"

## GETTING ALL PATHS
read -a all_sim_dirs <<< $(ls ${path_to_sim}/${simulation_dir_name}/* -d)

## PRINTING 
echo "*** Simulation paths ***"
for each_sim in "${all_sim_dirs[@]}"; do
	echo "--> ${each_sim}"
done
echo "-------------"
echo "Check if this is correct!"
sleep 5

## DEFINING SOLVENTS
declare -a solvent_list=("octanol") # "dmso"  
# "octanol"

## DEFINING JOBS
declare -a run_these_jobs=(\
	# "switch_solvent" \
	# "hydration_sims" \
	"hydration_analysis"  \
	)

## DEFINING IF YOU WANT REWRITE
rewrite=false
# false
# true

###########################################

## LOOPING THROUGH EACH LIGAND
for input_sim_path in "${all_sim_dirs[@]}"; do

	## SWITCH SOLVENTS
	if [[ " ${run_these_jobs[@]} " =~ " switch_solvent " ]]; then

	    ## LOOPING THROUGH EACH COSOLVENT
	    for current_cosolvent in "${solvent_list[@]}"; do
	        ## PRINTING
	        echo "RUNNING THE FOLLOWING COMMAND:"
	        echo "bash "${SWITCH_SOLVENT_SCRIPT}" "${input_sim_path}" "${current_cosolvent}" ${rewrite}"
	        ## RUNNING
	        bash "${SWITCH_SOLVENT_SCRIPT}" "${input_sim_path}" "${current_cosolvent}" "${rewrite}"
	    done

	fi

	## HYDRATION SIMS
	if [[ " ${run_these_jobs[@]} " =~ " hydration_sims " ]]; then
		## RUNNING SCRIPT
		bash "${HYDRATION_SCRIPT}" "${input_sim_path}" "${rewrite}"
	fi

	## HYDRATION ANALYSIS
	if [[ " ${run_these_jobs[@]} " =~ " hydration_analysis " ]]; then
		## RUNNING SCRIPT
		bash "${HYDRATION_ANALYSIS_SCRIPT}" "${input_sim_path}" "${rewrite}"
	fi


done

### PRINTING SUMMARY
echo "------SUMMARY------"
echo "Created systems for: ${all_sim_dirs[@]}"
echo "Cosolvents: ${solvent_list[@]}"
