#!/bin/bash

# full_prep_frozen_planar_sam.sh
# The purpose of this script is to run 
# full prep forozen planars

# Written by: Alex K. Chew (11/23/2019)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## ENABLING EXTERNAL MODULES
shopt -s extglob
# ^ Allows you to move all except 1

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DECLARING LIGAND NAMES
declare -a ligand_names=("C11OH" "dodecanethiol")
# "butanethiol" "C3OH" 
# "C11OH" "dodecanethiol" "C11NH2" "C11CONH2" "C11COOH" "C11CF3"
# "C11OH" "dodecanethiol" 
# "dodecanethiol" 
# "dodecanethiol"
#  "C11OH"
# "C11NH2" "C11COOH" "C11CONH2" "C11CF3" "C11OH"
# "dodecanethiol"
#  "C11NH2" "C11COOH" "C11CONH2" "C11CF3" "C11OH"
# "dodecanethiol"
#  "C11OH"
##  "C11OH"
#  "C11NH2" "C11COOH" "C11CONH2" "C11CF3"
#  "C11NH2" "C11COOH" "C11CONH2" "C11CF3"
# "ROTPE1" "ROTPE2" "ROTPE3" "ROTPE4"
# "C11NH2" "C11CONH2" "C11OH" "C11COOH" "C11COO" "C11NH3" "C11CF3"
#  "C11NH2" "C11CONH2" "C11OH" "C11COOH" "C11COO" "C11NH3" "C11CF3" "C11NCCH33" "C11CCH33"
# "C11NCH33" "C11CCH33"
# "C11COO" "C11NH3"
# "C11COOH" "C11NH2" "C11CONH2" "C11OH"
# "dodecanethiol"
#  "C11COOH"
# "C11COOH"
#  "dodecanethiol"
# ("C11CONH2" "C11COOH" "C11OH"
# "C11NH2"
# "C11OH" "C11CONH2" "dodecanethiol"
# "C11COOH" "C11NH2"

## DEFINING INPUT SIMULATION PATH
# input_sim_dirname="20200331-Planar_SAMs_no_temp_anneal_or_vacuum"
input_sim_dirname="NP_HYDRO_PLANAR_SAMS_INITIAL"

# "20200403-Planar_SAMs-5nmbox"
# "20200403-Planar_SAMs-5nmbox_nospring"
# "20200328-Planar_SAMs_new_protocol-shorterequil"
# "20200328-Planar_SAMs_new_protocol-shorterequil"
# "20200326-Planar_SAMs_with_larger_z"
# "20200326-Planar_SAMs_with_larger_z_8nm"
# "20200326-Planar_SAMs_with_larger_z"
# "NP_HYDRO_PLANAR_SIMS"
# "200102-Planar_SAM_temp_annealing_try4"

## GETTING OUTPUT DIR
output_sim_name="20200215-planar_SAM_frozen_600spring"
output_sim_name="20200217-planar_SAM_frozen_debug"

output_sim_name="20200326-Planar_SAMs_with_larger_z_frozen_with_vacuum"
output_sim_name="20200403-Planar_SAMs-5nmbox_novac_nptequil_long"
output_sim_name="20200403-Planar_SAMs-5nmbox_vac_with_npt_equil"

output_sim_name="20200608-SI_HYDRO_PLANAR_SAMS_DEBUGGING_SPRING"
# "20200515-planar_short_lig_frozen"
# "20200413-Planar_SAMs-5nmbox_vac_with_npt_equil_NORESTRAINT"
# "20200402-Planar_sams_no_anneal_npt_prod"
# "20200401-Planar_SAMs_no_temp_anneal_or_vacuum_with_npt_equil"
# "20200331-Planar_SAMs_no_temp_anneal_or_vacuum_withsprconstant"
# "20200328-Planar_SAMs_new_protocol-shorterequil_spr50"
# "20200326-Planar_SAMs_with_larger_z_8nm_frozen"
# "20200326-Planar_SAMs_with_larger_z_frozen"

# output_sim_name="20200224-planar_SAM_spr50"
# "20200114-NP_HYDRO_FROZEN"
# "191214-Planar_SAM_temp_annealing_try4_unfrozen"

## GETTING INPUT SIMULATION DIR
input_sim_path="${PATH2SIM}/${input_sim_dirname}"
## GETTING OUTPUT SIMULATION FILE
output_sim_location="${PATH2SIM}"
output_sim_path="${output_sim_location}/${output_sim_name}"
 
## DEFINING PREFIX
filename_prefix="Planar_300.00_K_"
filename_suffix="_10x10_CHARMM36jul2017_intffGold_Trial_1"

## DEFINING ARRAY FOR POSITION RESTRAINTS
declare -a lig_pos_restraints=("50")
declare -a lig_pos_restraints=("0" "25" "50" "100" "200" "300" "400" "500")
# "50"
# ("0")
# "50"
# "0"
# "50"
# "25" "50" "100" "200" "400" "600" "800" "1000" "2000"
# ("600")

## DEBUGGING
want_debug=true # True if you want 5 ns production sims
# false
# "true"
# "1000"

## DEFINING VACUUM LAYER
vacuum_layer_z_above="5"
# "0"
# "4"
# "4"

## GETTING PATH ARRAY
# read -a path_array <<<$(ls "${path_cosolvent_sims}/"MostNP* -d) # C11COOH C11NH3*

##########################
### DECLARING DEFAULTS ###
##########################
## DEFINING BASH CODE
bash_code="${PATH2SCRIPTS}/prep_frozen_planar_sam.sh"

###################
### MAIN SCRIPT ###
###################

## LOOPING THROUGH EACH PATH
for each_lig in ${ligand_names[@]}; do
    ## LOOPING THROUGH SPRING CONSTANTS
    for lig_spring_constant in "${lig_pos_restraints[@]}"; do
        ## DEFINING FILE NAME
        simulation_dir_name="${filename_prefix}${each_lig}${filename_suffix}"
        ## RUNNING CODE
        bash "${bash_code}" \
                    "${input_sim_path}" \
                    "${simulation_dir_name}" \
                    "${output_sim_path}" \
                    "${lig_spring_constant}" \
                    "${want_debug}" \
                    "${vacuum_layer_z_above}"
        ## ECHOING
        echo "Running command:"
        echo "   "${bash_code}" "${input_sim_path}" "${simulation_dir_name}" "${output_sim_path}" ${want_debug} ${vacuum_layer_z_above}"
        ## SLEEPING
	   sleep 2
    done

done
