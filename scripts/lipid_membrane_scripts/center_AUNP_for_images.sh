#!/bin/bash

# center_AUNP_for_images.sh
# The purpose of this function is to center all the gold particles 
# so we could better visualize the system

# Skipping 10 frames each time
# gmx trjconv -f nplm_prod.xtc -o nplm_prod_skip_10.xtc -s nplm_prod.tpr -skip 10

# Written by Alex K. Chew (01/13/2020)

## VARIABLES


# SAVING MOVIE IN VMD
# record_movie 0 35 NP_DOPC_ROT012

### SOURCING ALL VARIABLES / FUNCTIONS
source "../../bin/nanoparticle_rc.sh"

#######################
### INPUT VARIABLES ### 
#######################

## DEFINING SIMULATION DIRECTORY
simulation_dir="20200113-NP_lipid_bilayer_resize"

## DEFINING SIMULATION
simulation_name="NPLM-DOPC_196-300.00-6_0.0005_2000-EAM_2_ROT001_1"
simulation_name="NPLM-DOPC_196-300.00-6_0.0005_2000-EAM_2_ROT006_1"
# simulation_name="NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

## PULLING SIMULATIONS
simulation_dir="20200123-Pulling_sims_from_US"
simulation_name="pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"
simulation_name="pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT001_1"


## UNBIASED SIMULATIONS - ROT001
simulation_dir="20200205-unbiased_ROT001"
simulation_name="NPLM_unb-1.900_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
# simulation_name="NPLM_unb-2.100_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

## UNBIASED SIMS - ROT012
# simulation_dir="20200128-unbiased_ROT012"
# simulation_name="NPLM_unb-1.300_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
# simulation_name="NPLM_unb-1.900_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
# simulation_name="NPLM_unb-2.100_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
# "pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"


## UNBIASED SIMS
# simulation_dir="20200330-unbiased_C12_at_minimum"
# simulation_name="NPLM_rev_unb-5.300_2000-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"
# simulation_name="NPLM_unb-5.300_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

## UNBIASED C1 AND C10 - 3.5 nm from forward US
simulation_dir="20200414-Unbiased_at_3.500nm_C1_C10"
simulation_name="NPLM_unb-3.500_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
simulation_name="NPLM_unb-3.500_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"


## UNBIASED C1 FROM REVERS SIM
# simulation_dir="20200407-unbiased_ROT001_rev"
# simulation_name="NPLM_rev_unb-4.900_2000-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT001_1"

## UMBRELLA SAMPLING R12 AT Z=5.1 NM, PART 2
simulation_dir="20200120-US-sims_NPLM_rerun_stampede"
simulation_name="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1/4_simulations/5.100"


## PULLING THEN UNBIASED SIMS
simulation_dir="20200423-pulling_unbiased_full"
simulation_name="NPLMpulling2unb-4.900_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
simulation_name="NPLMpulling2unb-5.100_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
simulation_name="NPLMpulling2unb-5.100_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1_extend_equil"

## OTHER UNBIASED SIMS
simulation_dir="20200925-Hydrophobic_contacts_Unbiased_sims_50ns"
simulation_name="NPLM_unb-40.0-50000.000_ps-UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
simulation_name="NPLM_unb-40.0-50000.000_ps-UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
simulation_name="NPLM_unb-40.0-50000.000_ps-UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-50000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"

## MODIFIED R17 RESTRAINED
simulation_dir="20200818-Bn_US_modifiedFF_UNBIASED_AFTER_US"
simulation_name="NPLM_unb-1.900_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"

## DEFINING INPUT PATH
input_path="${PATH2NPLMSIMS}/${simulation_dir}/${simulation_name}"

## DEFINING FIRST FRAME TO READ
first_frame="0"

## DEFINING SKIPPING
skip_frames="10" # -- 0.2 nm increments
# "20" # number of skipped frames -- 0.1 nm increments

#########################
### DEFAULT VARIABLES ### 
#########################
## GOLD
center_residue_name="AUNP"

## LIPID MEMBRANE RESIDUE
lipid_membrane_residue="DOPC"

## OUTPUT
output_residue_name="System"

###################
### MAIN SCRIPT ### 
###################

## DEFINING PBC TYPE
np_pbc_type="mol"
pbc_type="mol"
# "whole"
# "mol"
# pbc_type="whole"

## IF YOU WANT INITIAL FILE
want_initial=false

## GOING INTO PATH
cd "${input_path}"

## DEFINING PREFIX
# input_prefix="nplm_push"
declare -a input_prefix_array=("nplm_em" "nplm_prod")
# "nplm_1_em"  "nplm_2_pull"

for input_prefix in "${input_prefix_array[@]}"; do
# input_prefix="nplm_2_pull"
# "nplm_prod"
# input_prefix="nplm_prod_part2"
# "nplm_prod"
# "nplm_pull"
output_prefix="${input_prefix}_center"

## CREATING INDEX FILE
gmx make_ndx -f "${input_prefix}.tpr" \
             -o "${output_prefix}.ndx" << INPUTS
keep 0
r ${center_residue_name}
r ${lipid_membrane_residue}
q
INPUTS

###########################
### CENTERING GOLD CORE ###
###########################

## CREATING GRO FILE -- PROD FILE
gmx trjconv -f "${input_prefix}.gro" \
            -o "${output_prefix}.gro" \
            -s "${input_prefix}.tpr" \
            -n "${output_prefix}.ndx" \
            -center -pbc "${np_pbc_type}" << INPUTS
${center_residue_name}
${output_residue_name}
INPUTS

if [[ "${want_initial}" == true ]]; then

## CREATING GRO FILE -- INITIAL FILE
gmx trjconv -f "nplm.gro" \
            -o "nplm_center.gro" \
            -s "${input_prefix}.tpr" \
            -n "${output_prefix}.ndx" \
            -center -pbc mol << INPUTS
${center_residue_name}
${output_residue_name}
INPUTS

fi

#####################################
### CENTERING WITH LIPID MEMBRANE ###
#####################################
center_lipid_membrane="${output_prefix}_lm"

## CREATING GRO FILE -- PROD FILE
gmx trjconv -f "${output_prefix}.gro" \
            -o "${center_lipid_membrane}.gro" \
            -s "${input_prefix}.tpr" \
            -n "${output_prefix}.ndx" \
            -center -pbc ${pbc_type} << INPUTS
${lipid_membrane_residue}
${output_residue_name}
INPUTS

if [[ "${want_initial}" == true ]]; then

## CREATING GRO FILE -- INTIIAL FILE
gmx trjconv -f "nplm_center.gro" \
            -o "nplm_center_lm.gro" \
            -s "${input_prefix}.tpr" \
            -n "${output_prefix}.ndx" \
            -center -pbc mol << INPUTS
${lipid_membrane_residue}
${output_residue_name}
INPUTS

fi

### SHIFTING UPWARD WITH GOLD CORE CENTERED
shift_value="2" # DOPC half bilayer is approximately 1.73 nm, so this should work
shift_value_upwards="3.5" # "3"
shifted_gro="${output_prefix}_${shift_value}"

## CREATING GRO FILE
gmx trjconv -f "${output_prefix}.gro" \
            -o "${shifted_gro}.gro" \
            -s "${input_prefix}.tpr" \
            -n "${output_prefix}.ndx" \
            -trans "0" "0" "${shift_value}" \
            -pbc mol  >/dev/null 2>&1  << INPUTS
${output_residue_name}
INPUTS

## CENTERING LM
gmx trjconv -f "${shifted_gro}.gro" \
            -o "${shifted_gro}_lm.gro" \
            -s "${input_prefix}.tpr" \
            -n "${output_prefix}.ndx" \
            -center \
            -pbc mol  >/dev/null 2>&1  << INPUTS
${lipid_membrane_residue}
${output_residue_name}
INPUTS


## XTC FILE


# ## CREATING XTC FILE
# gmx trjconv -f "${input_prefix}.xtc" \
#             -o "${output_prefix}.xtc" \
#             -s "${input_prefix}.tpr" \
#             -b "${first_frame}" \
#             -n "${output_prefix}.ndx" \
#             -skip "${skip_frames}" \
#             -center -pbc "${np_pbc_type}" \
#             << INPUTS
# ${center_residue_name}
# ${output_residue_name}
# INPUTS
# ${pbc_type}



# ## CREATING XTC FILE FOR LIPID MEMBRANE CENTERED
# gmx trjconv -f "${output_prefix}.xtc" \
#             -o "${center_lipid_membrane}.xtc" \
#             -s "${input_prefix}.tpr" \
#             -b "${first_frame}" \
#             -n "${output_prefix}.ndx" \
#             -center -pbc ${pbc_type} << INPUTS
# ${lipid_membrane_residue}
# ${output_residue_name}
# INPUTS


## CLEANING UP ANY EXTRA FILES
rm -f \#*

done
