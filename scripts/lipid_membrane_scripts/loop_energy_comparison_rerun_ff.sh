# loop_energy_comparison_rerun_ff.sh
# This script just loops through the rerun comparisons and runs the code
# 
# Written by: Alex K. Chew (08/12/2020)

## TO RUN THE CODE
# bash loop_energy_comparison_rerun_ff.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

## DEFINING DEFAULT LIST
job_list="energy_comparison_job_list.txt"
# job_list="energy_comparison_job_list_pure_water.txt"
path_job_list="${main_dir}/${job_list}"

## LOOPING THROUGH EACH LINE
while IFS= read -r each_line; do
	echo "Running the following: ${each_line}"
	if [[ ! -z "${each_line}" ]]; then
		bash ${each_line}
	fi
done < "${path_job_list}"
