#!/bin/bash
# run_energy_comparison_rerun_ff.sh

## This script runs the energy comparison code. 

# VARIABLES TO EDIT

## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

## GOING TO MAIN DIRECTORY THAT SCRIPT IS STORED
cd ${main_dir}

## DEFINING NUMBER OF CORES
num_cores="${1-1}"
gromacs_command="${2-gmx}"
rerun="${3-false}"

## DEFINING ARRAY
declare -a prefix_array=(_PREFIXARRAY_)

## DEFINING XTC FILE
xtc_file="_XTCFILE_"

## LOOPING
for each_prefix in ${prefix_array[@]}; do

	## CHECKING IF FILE EXISTS
	if [[ ! -e "${each_prefix}.edr" ]] || [[ "${rerun}" == true ]]; then

		## RUNNING GROMACS
		${gromacs_command} mdrun -v -nt "${num_cores}" \
							  -s "${each_prefix}.tpr" \
							  -e "${each_prefix}.edr" \
							  -rerun "${xtc_file}"
	else
		echo "${each_prefix}.edr exists! Continuing!"
	fi

## RUNNING QUICK ANALYSIS
gmx energy -f "${each_prefix}.edr" \
		   -s "${each_prefix}.tpr" \
		   -o "${each_prefix}.xvg"  << INPUTS
_INPUTENERGYTYPES_

INPUTS

done




