#!/bin/bash

# install_plumed_wham.sh
# The purpose of this script is to install plumed wham
#
# See documentation for installation procedure
#	http://membrane.urmc.rochester.edu/sites/default/files/wham/doc.pdf
#
# USAGE:
#	bash install_plumed_wham.sh
# NOTE:
#	- There may be bugs when using kJ/mol. For now, we 
# 	assume that we are working in kcal/mol. You can easily convert 
# 	into kJ/mol by multiplying 4.184 kJ / 1 kcal
# 
# Written by: Alex K. Chew (05/25/2020)

## DEFINING PATH TO INSTALLATION
path_installation="${HOME}/wham"

## DEFINING IF YOU WANT KJ OR KCAL
# energy_units="kcal" # kcal/mol-K -- default
# energy_units="kJ" # kJ/mol-K -- If kJ, then will change the headers for the k_B value

## MAKING DIRECTORY
mkdir -p "${path_installation}"

## GOING TO DIRECTORY
cd "${path_installation}"

## REMOVING ANY *.TGZ
rm -f *.tgz *.tgz.*

## DEFINING WHAM DIRECTORY
wget http://membrane.urmc.rochester.edu/sites/default/files/wham/wham-release-2.0.9.1.tgz

## UNLOADING THE WHAM PACKAGE
tar zxvf wham-release-2.0.9.1.tgz

## GOING INTO WHAM FOLDER
cd wham
make clean
make

## REMOVING LAST TGZ FILE
cd "${path_installation}"
rm -f wham-release-2.0.9.1.tgz

# THE PATH TO THE WHAM IS: 
# /home/akchew/wham/wham/wham/wham
