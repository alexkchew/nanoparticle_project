#!/bin/bash

# center_AUNP_umbrella_sampling_images.sh
# The purpose of this function is to center the gold core so it is easier to visualize the nanoparticle with the lipid membrane
# RUN ON VMD: vmd_load_nplm_umbrella_sampling

# Written by Alex K. Chew (01/23/2020)

### SOURCING ALL VARIABLES / FUNCTIONS
source "../../bin/nanoparticle_rc.sh"

#######################
### INPUT VARIABLES ### 
#######################

## FORWARD SIMULATIONS


# ROT001
simulation_dir="20200427-From_Stampede"
simulation_name="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
# # # ROT012
# simulation_dir="20200120-US-sims_NPLM_rerun_stampede"
# simulation_name="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"


## REVERSE SIMULATIONS - ROT001
# simulation_dir="20200427-From_Stampede"
# # "20200207-US-sims_NPLM_reverse_ROT001"
# simulation_name="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT001_1"

# ## REVERSE SIMULATIONS - ROT012
# simulation_dir="20200124-US-sims_NPLM_reverse_stampede"
# simulation_name="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"



## PULLING SIMULATIONS
#simulation_dir="20200120-US-sims_NPLM_rerun_stampede"
## simulation_name="NPLM-DOPC_196-300.00-6_0.0005_2000-EAM_2_ROT001_1"
#simulation_name="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
## simulation_name="NPLM-DOPC_196-300.00-6_0.0005_2000-EAM_2_ROT006_1"


## PLUMED US SIMULATIONS
simulation_dir="20200615-US_PLUMED_rerun_with_10_spring"
simulation_name="UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
simulation_name="UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
# simulation_name="UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-50000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"

## NEW BENZENE SIMULATIONS
# simulation_dir="20200822-Hydrophobic_contacts_PMF_Bn_new_FF"
# simulation_name="UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-50000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"

## SECOND ITERATION
# simulation_dir="20200713-US_PLUMED_iter2"
# simulation_name="UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-25000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
# simulation_name="UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-25000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

#########################
### DEFAULT VARIABLES ### 
#########################

## DEFINING SIMULATION DIRECTORY
sim_dir_name="4_simulations"

## DEFINING INPUT PREFIX
input_prefix="nplm_prod"

## DEFINING OUTPUT FILE
output_prefix="nplm_prod_center"

## DEFINING INPUT PATH
input_path="${PATH2NPLMSIMS}/${simulation_dir}/${simulation_name}/${sim_dir_name}"

## GOLD
center_residue_name="AUNP"

## LIPID MEMBRANE RESIDUE
lipid_membrane_residue="DOPC"

## OUTPUT
output_residue_name="System"

###################
### MAIN SCRIPT ### 
###################

## CHECKING IF EXISTING
stop_if_does_not_exist "${input_path}"

## GOING INTO PATH
cd "${input_path}"

## LOOPING THROUGH EACH DIRECTORY
for each_dir in $(ls * -d | sort -V); do
    ## GOING INTO DIRECTORY
    echo "Working on ${each_dir}"
    cd "${each_dir}"
## CREATING INDEX FILE
gmx make_ndx -f "${input_prefix}.tpr" \
             -o "${output_prefix}.ndx" >/dev/null 2>&1 << INPUTS
keep 0
r ${center_residue_name}
r ${lipid_membrane_residue}
q
INPUTS

## CREATING GRO FILE WITH GOLD CORE
gmx trjconv -f "${input_prefix}.gro" \
            -o "${output_prefix}.gro" \
            -s "${input_prefix}.tpr" \
            -n "${output_prefix}.ndx" \
            -center -pbc mol >/dev/null 2>&1 << INPUTS
${center_residue_name}
${output_residue_name}
INPUTS

## CENTERING WITH LIPID MEMBRANE
center_lipid_membrane="${output_prefix}_lm"

## CREATING GRO FILE
gmx trjconv -f "${output_prefix}.gro" \
            -o "${center_lipid_membrane}.gro" \
            -s "${input_prefix}.tpr" \
            -n "${output_prefix}.ndx" \
            -center -pbc mol >/dev/null 2>&1 << INPUTS
${lipid_membrane_residue}
${output_residue_name}
INPUTS

### SHIFTING UPWARD WITH GOLD CORE CENTERED
shift_value="2" # DOPC half bilayer is approximately 1.73 nm, so this should work
shift_value_upwards="3.5" # "3"
shifted_gro="${output_prefix}_${shift_value}"

## CREATING GRO FILE
gmx trjconv -f "${output_prefix}.gro" \
            -o "${shifted_gro}.gro" \
            -s "${input_prefix}.tpr" \
            -n "${output_prefix}.ndx" \
            -trans "0" "0" "${shift_value}" \
            -pbc mol  >/dev/null 2>&1  << INPUTS
${output_residue_name}
INPUTS

## CENTERING LM
gmx trjconv -f "${shifted_gro}.gro" \
            -o "${shifted_gro}_lm.gro" \
            -s "${input_prefix}.tpr" \
            -n "${output_prefix}.ndx" \
            -center \
            -pbc mol  >/dev/null 2>&1  << INPUTS
${lipid_membrane_residue}
${output_residue_name}
INPUTS

## FINDING PBC 
read -a box_size <<< $(gro_measure_box_size "${shifted_gro}_lm.gro")

## FIGURING OUT SHIFT AMOUNT
new_z=$(awk -v z_dim=${box_size[2]} \
            -v shift=${shift_value_upwards} \
            'BEGIN{ printf "%.3f", -z_dim/2 + shift}')

## SHIFTING AGAIN
gmx trjconv -f "${shifted_gro}_lm.gro"  \
            -o "${shifted_gro}_lm_shifted.gro" \
            -s "${input_prefix}.tpr" \
            -n "${output_prefix}.ndx" \
            -trans "0" "0" "${new_z}" \
            -pbc mol  >/dev/null 2>&1  << INPUTS
${output_residue_name}
INPUTS

## CLEANING UP DIRECTORY
rm -f \#*

## GOING INTO PATH
cd "${input_path}"

done

