#!/bin/bash

# run_gmx_wham_analysis.sh
# The purpose of this function is to run gmx wham analysis after completion of the simulations. 
#
# RUNNING
#   bash run_gmx_wham_analysis.sh 300.00
# VARIABLES:
#   $1: temperature in Kelvins

### SOURCING ALL VARIABLES / FUNCTIONS
source "../../bin/nanoparticle_rc.sh"
    
## PRINTING
echo "*** run_gmx_wham_analysis ***"

## DEFINING UMBRELLA SAMPLING 
python_update_pullf="${MDBUILDER}/umbrella_sampling/umbrella_sampling_edit_pullf_different_times.py"

## FORWARD ROT012
sim_location="20200120-US-sims_NPLM_rerun_stampede"
specific_dir="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

## REVERSE
#sim_location="20200124-US-sims_NPLM_reverse_stampede"
#specific_dir="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"

## ROT012 REVERSE
# combine_sim_location="20200124-US-sims_NPLM_reverse_stampede"
# combine_specific_dir="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"


### ROT001

#### NEW ROT01 SIMS - FORWARD
# sim_location="20200427-From_Stampede"
# specific_dir="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
# specific_dir="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT001_1"

## REVERSE FOR ROT001
sim_location="20200427-From_Stampede"
specific_dir="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT001_1"

### COMBINATION DIRECDTORIES
## ROT001 REVERSE
combine_sim_location="20200207-US-sims_NPLM_reverse_ROT001_aci"
# "20200207-US-sims_NPLM_reverse_ROT001"
combine_specific_dir="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT001_1"

### COMBINING SIMS
want_combine=false # true if you want to combine forward and reverse

## DEFINING MAIN DIRECTORY
combine_main_dir="${PATH2NPLMSIMS}/${combine_sim_location}/${combine_specific_dir}"

## DEFINING MAIN DIRECTORY
input_main_dir="${PATH2NPLMSIMS}/${sim_location}/${specific_dir}"

## DEFINING NUMBER OF BINS
num_bins="200" # "300" # "2000" # Default 200

## TOTAL FRAMES FROM END
total_frames_to_analyze_from_end="40000" # ps
# "50000" # ps

## INCLUDING TEMPERATURE
temp="${1:-300.00}" # In Kelvin

input_simulation_md_folder="" # 1_MD
input_simulation_md_prefix="nplm_prod" # md prefix for simulation files
input_simulation_md_gro_files="${input_simulation_md_prefix}.gro"
input_pullf_xvg="_pullf.xvg" ## END TAIL OF X XVG (e.g. md0_pullf.xvg)


output_profile_xvg="profile.xvg"
output_histogram_xvg="histo.xvg"
output_units="kJ" # kCal kJ, kT, etc.
output_incomplete="incomplete_sims.txt"

## ANALYSIS DIRECTORY
output_analysis_folder="5_analysis"


## SEEING IF YOU WANT COMBINED
if [[ "${want_combine}" == true ]]; then
    ## DEFINING OUTPUT FOLDER
    output_analysis_folder="5_analysis_combined"
fi

## DEFINING PATH TO ANALYSIS
path_analysis="${input_main_dir}/${output_analysis_folder}"

## MAKING DIRECTORY
mkdir -p "${path_analysis}"

############################################
### COPYING OVER PULLF.XVG AND TPR FILES ###
############################################

## DEFINING ARRAY
declare -a sim_path_array=("${input_main_dir}")

if [[ "${want_combine}" == true ]]; then
    declare -a sim_path_array=("${input_main_dir}" "${combine_main_dir}")
fi

## KEEPING TRACK
sim_index=0

## LOOPING THROUGH EACH SIM
for each_sim in ${sim_path_array[@]}; do
    ## PRINTING
    echo "Working on: $(basename ${each_sim})"
    ## SEEING IF YOU HAVE MAIN SIM
    if [[ "${sim_index}" != "0" ]]; then
        current_suffix="_${sim_index}"
    else
        current_suffix=""
    fi
    
    ## DEFINING OUTPUT FILES
    output_tpr_dat="tpr_files${current_suffix}.dat"
    output_pullf_dat="pullf_files${current_suffix}.dat"

    ## CREATING DATA FILES FILE
    echo "Creating ${output_tpr_dat} and ${output_pullf_dat}"
    > "${path_analysis}/${output_tpr_dat}"
    > "${path_analysis}/${output_pullf_dat}"
    
    ## DEFINING DEFAULT DIRECTORIES
    input_simulation_folder_="${each_sim}/4_simulations"
    ## CHECKING FOR TOTAL NUMBER OF WINDOWS
    total_windows=$(ls ${input_simulation_folder_} | wc -l)
    total_windows_minus_one="$((${total_windows}-1))"
    
    ## DEFINING INPUT SUMMARY DATA
    input_summary_dat="${each_sim}/status.info"
    
    ## FINDING TOTAL PRODUCTION GRO FILES
    total_production_gro=$(find ${input_simulation_folder_} -name "${input_simulation_md_gro_files}" | wc -l)

    ## DEFINING INCOMPLETE VARIABLE
    first_incomplete=True

    ## SEEING IF THE TOTAL NUMBER OF WINDOWS MATCH THE TOTAL PRODUCTION
    if [[ "${total_windows}" != "${total_production_gro}" ]]; then
        echo "Error! The total number of windows is not equal to the available production gro files!"
        echo "Total windows: ${total_windows}"
        echo "Total production gro files: ${total_production_gro}"
        echo "Gro file search: ${input_simulation_md_gro_files}"
        echo "Continuing, just be careful!"; sleep 2
        # echo "Stopping here to prevent further errors!"; sleep 5
        # exit 1
    else
        echo "The total number of windows match production gro file. Continuing the analysis!"
    fi
    
    ## LOOPING THROUGH EACH WINDOW
    for each_counter in $(seq 0 ${total_windows_minus_one}); do
        ## DEFINING EACH WINDOW
        each_window=$(extract_summary_dat "${input_summary_dat}" "${each_counter}")

        ## DEFINING NAMES OF THE FILES
        tpr_file_name="${input_simulation_md_prefix}.tpr"
        pullf_file_name="${input_simulation_md_prefix}${input_pullf_xvg}"

        ## DEFINING GRO FILE
        gro_file_name="${input_simulation_md_prefix}.gro"

        ## DEFINING OUTPUT TPE FILE
        output_tpr_file_name="${input_simulation_md_prefix}_${each_window}${current_suffix}.tpr"
        output_pullf_file_name="${input_simulation_md_prefix}_${each_window}${input_pullf_xvg%.xvg}${current_suffix}.xvg"

        ## CHECKING IF YOUR FILE IS DONE
        if [[ -e "${input_simulation_folder_}/${each_window}/${input_simulation_md_folder}/${gro_file_name}" ]]; then

            ## DEFINING PATH TO MD
            path_to_md="${input_simulation_folder_}/${each_window}/${input_simulation_md_folder}"

            ## PRINTING
            echo "Copying over files for ${each_window}"
            ## COPYING OVER FILES
            cp -r "${path_to_md}/${tpr_file_name}" "${path_analysis}/${output_tpr_file_name}"
            cp -r "${path_to_md}/${pullf_file_name}" "${path_analysis}/${output_pullf_file_name}"

            ## PRINTING THE FILES INTO CORRESPONDING DAT FILES
            echo "${output_tpr_file_name}" >> "${path_analysis}/${output_tpr_dat}"
            echo "${output_pullf_file_name}" >> "${path_analysis}/${output_pullf_dat}"

        ## RECORDING SIMULATIONS THAT ARE NOT COMPLETE
        else
            echo "Writing incomplete files for ${each_window}"
            ## DEFINING FOLDER FOR INCOMPLETE SIMS
            path_output_incomplete="${input_main_dir}/${output_incomplete}"

            ## CREATING FILE IF FIRST TIME
            if [[ "${first_incomplete}" == "True" ]]; then
                echo "Creating incomplete file at : ${path_output_incomplete}"
                > "${path_output_incomplete}"
                ## CREATING HEADER
                echo "Window, simulation folder"
                ## CHANGING FIRST VARIABLE
                first_incomplete="False"
            fi
            echo "${each_window}, ${input_simulation_folder_}/${each_window}/${input_simulation_md_folder}" >> "${path_output_incomplete}"

        fi

    done
    
    ############################
    ### CORRECTING ALL FILES ###
    ############################

    ## GETTING PULLF FILES
    python3.6 "${python_update_pullf}" --parentpath "${input_main_dir}/${output_analysis_folder}" \
                                       --data_file "${output_pullf_dat}" \
                                       --total_time "${total_frames_to_analyze_from_end}"
    
    ## ADDING TO SIMULATION INDEX
    sim_index=$((${sim_index}+1))
done

## PRINTING
echo "----- COPYING IS COMPLETE -----"

## GOING TO ANALYSIS FOLDER
cd "${path_analysis}"
## CHECKING IF YOU WANT COMBINED
if [[ "${want_combine}" == true ]]; then
    
    ## REDEFINING OUTPUT
    output_tpr_dat="combine_tpr_files.dat"
    output_pullf_dat="combine_pullf_files.dat"
    ## COMBINING FILES
    cat "tpr_files.dat" "tpr_files${current_suffix}.dat" > "${output_tpr_dat}"
    cat "pullf_files.dat" "pullf_files${current_suffix}.dat" > "${output_pullf_dat}"
fi

########################
### RUNNING GMX WHAM ###
########################
## PRINTING
echo "*** GMX WHAM ***"
## GOING INTO ANALYSIS DIRECTORY
cd "${output_analysis_folder}"
## GMX WHAM
gmx wham -it "${output_tpr_dat}" \
         -if "${output_pullf_dat}" \
         -o "${output_profile_xvg}" \
         -hist "${output_histogram_xvg}" \
         -unit "${output_units}" \
         -bins "${num_bins}" \
         -temp "${temp}" \
                                 
echo "----- GMX WHAM IS COMPLETE -----"
echo "Number of bins: ${num_bins}"
echo "Temperature: ${temp}"
## GOING BACK TO ORIGINAL DIRECTORY
cd "${input_main_dir}"