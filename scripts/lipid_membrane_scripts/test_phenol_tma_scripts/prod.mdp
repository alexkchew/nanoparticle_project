; prod.mdp
; This MDP file is used to perform NVE simulations for phenol-TMA systems. 

; ADJUSTABLE VARIABLES
;   _DT_ <-- time step
;   _NSTEPS_ <-- number of steps

; DEFINING TITLE
title		= NVE simulation

; INTEGRATOR
integrator	= md		; leap-frog integrator
cutoff-scheme	= Verlet
nsteps		= _NSTEPS_	; 4 ns
dt		= _DT_		; 2 fs

; OUTPUT CONTROL
nstxout		= 0		    ;
nstvout		= 0		    ;
nstxtcout	= 500  		; save every 1 ps
nstenergy	= 500		; save energies every 1 ps
nstlog		= 100		; update log file every 0.2 ps
nstcalcenergy	= 100	; calculate energy every 1000 steps

; VELOCITIES
continuation   = yes    ; yes if you want continuation
gen_vel		   = no   ; assign velocities from Maxwell distribution

; CENTER OF MASS REMOVAL
comm-mode	= Linear	; remove COM motion; possibly freeze Au atoms?
nstcomm		= 100	; remove every 100 steps

; CONSTRAINTS
constraint_algorithm = lincs	    ; holonomic constraints
constraints	         = h-bonds    	; all bonds (even heavy atom-H bonds) constrained  -- could be "h-bonds", typical for CHARMM
lincs_iter	         = 1		    ; more accurate for initial equil
lincs_order	         = 4		    ; increase for more accuracy (can decrease to 5 later)

; VDW INTERACTIONS
vdwtype		   = Cutoff	        ; VDW cutoff type
vdw-modifier   = force-switch	; According to CHARMM, http://www.gromacs.org/Documentation/Terminology/Force_Fields/CHARMM
rvdw		   = 1.2		    ; for switching function -- updated for CHARMM
rvdw-switch	   = 1.0		    ; when to start switching off force -- updated for CHARMM

; ELECTROSTATIC INTERACTIONS
coulombtype	= PME
rcoulomb	= 1.2		; ibid -- updated for CHARMM

; NEIGHBOR SEARCHING
ns-type		= grid		; include all atoms in neighbor list
nstlist		= 10		; recommended for verlet
rlist		= 1.2		; for CHARMM

; TEMPERATURE COUPLING
tcoupl		= no ;
ref_t		= 300.0
tau_t		= 1.0            ; 0.1 to 1.0
tc-grps		= System

; PRESSURE COUPLING
pcoupl		    = no 	; Parinnello-Rahman used to enforce ensemble
pcoupltype      = semiisotropic 
tau_p		    = 5.0
compressibility    = 4.5e-5  4.5e-5
ref_p              = 1.0     1.0

; COORDINATE SCALING
refcoord_scaling   = com

; CORRECTION
DispCorr	= EnerPres

; PERIODIC BOUNDARY CONDITIONS
pbc		    = xyz ; PBC in all direction

