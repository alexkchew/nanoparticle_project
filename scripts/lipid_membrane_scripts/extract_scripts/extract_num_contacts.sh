#!/bin/bash

# extract_num_contacts.sh
# This function extracts the number of contacts
#
# Written by: Alex K. Chew (01/24/2020)
# 
# EDITABLE VARIABLES
#   _INPUT_PREFIX_ <-- input prefix
#   _PYTHONSCRIPT_ <-- python script used to compute number of contacts
#   _OUTPUT_PREFIX_ <-- output prefix
#   _SKIPFRAME_ <-- skip frame
#   _FIRSTFRAME_ <-- first frame
#   _LASTFRAME_ <-- last frame
# 	_SIMPATH_ <-- simulation path
# 	_SELECTION_ <-- selection type
#	_LOGFILE_ <-- log file
#	_LM_MEMBRANE_NAME_ <-- lipid membrane name
#	_CUTOFF_ <-- cutoff of contacts
#	_PATHPICKLE_ <-- path to the pickle file

###########################
### USER DEFINED INPUTS ###
###########################

## DEFINING PATH TO PICKLE
path_pickle="_PATHPICKLE_"

## GETTING CURRENT PATH
SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

## DEFINING SIMULATIONS
relative_sim_path="_SIMPATH_"

## DEFINING PYTHON CODE
python_script="_PYTHONSCRIPT_"

## DEFINING PREFIXES
input_prefix="_INPUT_PREFIX_"
output_prefix="_OUTPUT_PREFIX_"

## FRAME INFORMATION
skip_frame="_SKIPFRAME_"
first_frame="_FIRSTFRAME_"
last_frame="_LASTFRAME_"

## DEFINING SELECTION
selection="_SELECTION_"

## DEFINING REWRITE
rewrite="_REWRITE_"

## NUMBER OF CORES
num_cores="_NUMCORES_"

## DEFINING LOG FILE
log_file="_LOGFILE_"

## DEFINING LIPID MEMBRANE
lm_membrane_name="_LM_MEMBRANE_NAME_"

## DEFINING CUTOFF
cutoff="_CUTOFF_"

## DEFINING PATH TO SIM
sim_path="$( cd "${SCRIPTPATH}/${relative_sim_path}"; pwd -P)"

################################
### RUNNING GROMACS COMMANDS ###
################################

## GOING TO DIRECTORY
cd "${sim_path}"

## XTC FILE
if [ ! -e "${output_prefix}.xtc" ] ||  [ "${rewrite}" == true  ]; then

## CHECKING FOR FRAME RATE
if [[ "${last_frame}" == "-1" ]]; then
gmx trjconv -f "${input_prefix}.xtc" \
            -s "${input_prefix}.tpr" \
            -o "${output_prefix}.xtc" \
            -skip "${skip_frame}" \
            -b "${first_frame}" \
            -pbc mol << INPUTS
${selection}
INPUTS

else

gmx trjconv -f "${input_prefix}.xtc" \
            -s "${input_prefix}.tpr" \
            -o "${output_prefix}.xtc" \
            -skip "${skip_frame}" \
            -b "${first_frame}" \
            -e "${last_frame}" \
            -pbc mol << INPUTS
${selection}
INPUTS

fi

fi

## GRO FILE
if [ ! -e "${output_prefix}.gro" ] ||  [ "${rewrite}" == true  ]; then
gmx trjconv -f "${input_prefix}.xtc" \
            -s "${input_prefix}.tpr" \
            -o "${output_prefix}.gro" \
            -dump 0 \
            -pbc mol << INPUTS
${selection}
INPUTS
fi

## REMOVING ALL EXTRA FILES
rm -f "\#*"

#################################################
### RUNNING PYTHON SCRIPT TO GET ALL CONTACTS ###
#################################################
## RUNNING PYTHON CODE
python3.6 "${python_script}" --n_procs "${num_cores}" \
                             --path "${sim_path}" \
                             --gro "${output_prefix}.gro" \
                             --xtc "${output_prefix}.xtc" \
                             --log_file "${log_file}" \
                             --path_pickle "${path_pickle}" \
                             --lm_name "${lm_membrane_name}" \
                             --cutoff_radius "${cutoff}"
                             
                             
                             
                             
