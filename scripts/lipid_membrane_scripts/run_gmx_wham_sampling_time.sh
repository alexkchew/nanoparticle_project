#!/bin/bash

# run_gmx_wham_sampling_time.sh
# The purpose of this function is to compute the convergence of gmx wham.
#
# Written by Alex K. Chew (01/23/2020)

# 

### SOURCING ALL VARIABLES / FUNCTIONS
source "../../bin/nanoparticle_rc.sh"

## PRINTING
print_script_name

## FORWARD SIMS
sim_location="20200120-US-sims_NPLM_rerun_stampede"
specific_dir="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
#specific_dir="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
#
### DEFINING SIM LOCATION
sim_location="20200124-US-sims_NPLM_reverse_stampede"
specific_dir="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"

## FOR ROT 001 (FORWARD)
sim_location="20200120-US-sims_NPLM_rerun_stampede"
specific_dir="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

## FOR ROT001 (REVERSE)
sim_location="20200207-US-sims_NPLM_reverse_ROT001_aci"
specific_dir="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT001_1"


### NEW DATA
sim_location="20200427-From_Stampede"
specific_dir="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
specific_dir="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT001_1"

## DEFINING MAIN DIRECTORY
input_main_dir="${PATH2NPLMSIMS}/${sim_location}/${specific_dir}"
# "/home/akchew/scratch/nanoparticle_project/simulations/${sim_location}/${specific_dir}"

echo "--------------------------"
echo "Working on main directory:"
echo "${input_main_dir}"
echo "--------------------------"

## GOING INTO DIRECTORY
cd "${input_main_dir}"

## DEFINING NUMBER OF BINS
num_bins="200" # "300" # "2000" # Default 200

## DEFINING INTERVAL TO RUN SAMPLING TIMES
first_time="0"  # ps
interval="5000" # ps
last_time="50000" # ps

## DEFINING DEFAULT DIRECTORIES
input_simulation_folder_="4_simulations"
input_simulation_md_folder="" # 1_MD
input_simulation_md_prefix="nplm_prod" # md prefix for simulation files
input_simulation_md_gro_files="${input_simulation_md_prefix}*.gro"
input_pullf_xvg="_pullf.xvg" ## END TAIL OF X XVG (e.g. md0_pullf.xvg)
input_summary_dat="${input_main_dir}/status.info"

## DEFINING OUTPUT FILES
output_tpr_dat="tpr_files.dat"
output_pullf_dat="pullf_files.dat"
output_profile_xvg="profile.xvg"
output_histogram_xvg="histo.xvg"
output_units="kJ" # kCal kJ, kT, etc.
output_incomplete="incomplete_sims.txt"

## ANALYSIS DIRECTORY
output_analysis_folder="5_analysis"

## DEFINING DIRECTORY FOR SAMPLING
output_sampling_folder="sampling_time"

## CREATING DIRECTORY
mkdir -p "${output_analysis_folder}/${output_sampling_folder}"

## CALCULATING TOTAL REPEATS OF GMX WHAM
total_repeats=$(awk -v first=${first_time} -v last=${last_time} -v interval=${interval} 'BEGIN{ printf "%d", (last-first)/interval - 1 }')

########################
### RUNNING GMX WHAM ###
########################
## PRINTING
echo "*** GMX WHAM ***"
## GOING INTO ANALYSIS DIRECTORY
cd "${output_analysis_folder}"

### LOOPING THROUGH EACH REPEAT
for each_index in $(seq 0 ${total_repeats}); do
    ## FINDING CURRENT INTERVAL
    current_index_time=$(awk -v first=${first_time} -v current_index=${each_index} -v interval=${interval} 'BEGIN{ printf "%d", first + (current_index+1)*interval }')
    ## PRINTING
    echo "Running GMX WHAM for ${each_index} out of ${total_repeats} -- ${first_time} to ${current_index_time} ps -- OUTPUT SUPPRESSED"
    ## DEFINING NEW NAME
    current_index_output_profile_xvg="${output_profile_xvg%.xvg}_${first_time}_${current_index_time}.xvg"
    ## RUNNING GMX WHAM
    gmx wham -it "${output_tpr_dat}" \
             -if "${output_pullf_dat}" \
             -o "${output_sampling_folder}/${current_index_output_profile_xvg}" \
             -unit "${output_units}" \
             -b "${first_time}" \
             -e "${current_index_time}" \
             -bins "${num_bins}" >/dev/null 2>&1
done

# -hist "${output_histogram_xvg}" 
echo "----- GMX WHAM IS COMPLETE -----"
echo "Number of bins: ${num_bins}"

