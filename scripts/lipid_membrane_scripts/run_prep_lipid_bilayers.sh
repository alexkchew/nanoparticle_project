#!/bin/bash 
# run_prep_lipid_bilayers.sh
# The purpose of this script is to prepare lipid bilayers for production 
# runs. We assume that you have received the lipid membrane from the 
# CHARMM-gui. Now, we will complete equilibration and production. 

#################
### VARIABLES ###
#################
# _NUMCORES_ <-- number of cores
# _MDPEQUIL_ <-- mdp file for final equilibration
# _OUTPUTPREFIX_ <-- output prefix

## NUMBER OF CORES
num_cores="_NUMCORES_"

## DEFINING MDP FILE
mdp_equil="_MDPEQUIL_"
## DEFINING OUTPUT PREFIX
output_prefix="_OUTPUTPREFIX_"

################################
### COMMANDS FROM CHARMM-GUI ###
################################

### ENERGY MINIMIZATION
if [[ ! -e "step6.0_minimization.gro" ]]; then
    gmx grompp -f "step6.0_minimization.mdp" -o "step6.0_minimization.tpr" -c "step5_charmm2gmx.pdb" -r "step5_charmm2gmx.pdb" -p "topol.top"
    gmx mdrun -v -nt 1 -deffnm "step6.0_minimization"
else
    echo "step6.0_minimization.gro exists, continuing!"
fi

### EQUILIBRATION ###
cnt=1
cntmax=6

## LOOPING THROUGH EACH EQUILIBRATION
for((i=${cnt};i<=${cntmax};++i)) do
    ## COMPUTING COUNT
    pcnt=$((${i} - 1))
    # CHECKING IF GRO EXISTS
    if [[ ! -e "step6.${i}_equilibration.gro" ]]; then
        if [[ ${i} == 1 ]]; then
            ## CREATING TPR FILE
            gmx grompp -f "step6.${i}_equilibration.mdp" \
                       -o "step6.${i}_equilibration.tpr" \
                       -c "step6.${pcnt}_minimization.gro" \
                       -r "step5_charmm2gmx.pdb" \
                       -n "index.ndx" \
                       -p "topol.top"
            ## MDRUN
            gmx mdrun -v -nt "${num_cores}" \
                         -deffnm "step6.${i}_equilibration"
        else
            ## CREATING TPR FILE
            gmx grompp -f "step6.${i}_equilibration.mdp" \
                       -o "step6.${i}_equilibration.tpr" \
                       -c "step6.${pcnt}_equilibration.gro" \
                       -r "step5_charmm2gmx.pdb" \
                       -n index.ndx \
                       -p topol.top
            ## MDRUN
            gmx mdrun -v -nt "${num_cores}" \
                         -deffnm "step6.${i}_equilibration"
        fi
    else
        echo "step6.${i}_equilibration.gro exists, continuing!"
    fi
    ## ADDING ONE TO THE COUNT
    cnt=$((${cnt} + 1))
    
done

### PRODUCTION ###
cnt=1
cntmax=10

## LOOPING THROUGH EACH EQUILIBRATION
for((i=${cnt};i<=${cntmax};++i)) do
    ## COMPUTING COUNT
    pcnt=$((${i} - 1))
    # CHECKING IF GRO EXISTS
    if [[ ! -e "step7_${i}.gro" ]]; then
        if [[ ${i} == 1 ]]; then
            ## CREATING TPR FILE
            gmx grompp -f "step7_production.mdp" \
                       -o "step7_${i}.tpr" \
                       -c "step6.6_equilibration.gro" \
                       -n "index.ndx" \
                       -p topol.top    
            ## MDRUN
            gmx mdrun -v -nt "${num_cores}" \
                         -deffnm "step7_${i}"
        else
            ## CREATING TPR FILE
            gmx grompp -f "step7_production.mdp" \
                       -o "step7_${i}.tpr" \
                       -c "step7_${pcnt}.gro" \
                       -t "step7_${pcnt}.cpt" \
                       -n "index.ndx" \
                       -p "topol.top"
            ## MDRUN
            gmx mdrun -v -nt "${num_cores}" \
                         -deffnm "step7_${i}"
        fi
    else
        echo "step7_${i}.gro exists, continuing!"
    fi
done

## REMOVING ANY EXTRA FILES
rm -f \#*

####################################################
#### ADDITIONAL PRODUCTION USING CUSTOM MDP FILE ###
####################################################
#
### GENERATING TPR FILE
#gmx grompp -f "${mdp_equil}" -o ${output_prefix}_equil.tpr -c "step7_${cnt}.gro" -p "topol.top" -maxwarn 5
#
### RUNNING EQUILIBRATION
#gmx mdrun -nt "${num_cores}" -v -deffnm ${output_prefix}_equil
#
