#!/bin/bash

# test_phenol_tma_ff_generate_MD.sh
# This script creates a TMA-phenol job to test the force field parameters 
# for phenol - TMA interactions. The literature source is:
# Khan, H. M. et al. Improving the Force Field Description of Tyrosine-Choline Cation-π Interactions: QM Investigation of Phenol-N(Me)4+ Interactions. J. Chem. Theory Comput. 12, 5585–5595 (2016).

# Written by Alex K. Chew (09/02/2020)

# Algorithm:
#	- copy over force field information
#	- copy gro / itp / prm file
#	- create a combined gro file by first centering on phenol, then adding TMA using a set distance criteria
#	- create topology file
#	- create mdp + submission file
#	- perform MD simulation
#	- then, (outside this script), run analysis on the interaction energies

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

#######################
### DEFINING INPUTS ###
#######################

## DEFINING MOLECULES
molecule_1="phenol"
molecule_2="tetramethylammonium"

## DEFINING INITIAL DISTANCE
initial_dist="0.4" # Approximate distance between the two

## DEFINING DISTANCE TO EDGE (nm)
dist_to_edge=5

## DEFINING OUTPUT DIR NAME
output_parent_dir="20200902-phenol_tma_tests"

############################
### DEFINING MDP OPTIONS ###
############################

## TIME STEP
mdp_dt="0.002"
## PROD TIME
mdp_prod_time="2500000" # 5 ns

## GETTING TOTAL TIME
total_time=$(awk -v prod_steps=${mdp_prod_time} \
				 -v dt="${mdp_dt}" 'BEGIN{ printf "%.3f", prod_steps * dt }')

## DEFINING MDP FILES
mdp_em="em.mdp"
mdp_prod="prod.mdp"

######################
### DEFINING PATHS ###
######################

## DEFING TO SIM
path_to_sim="${PATH2NPLMSIMS}"

## PATH TO FORCE FIELD
path_forcefield="${PATH_FORCEFIELD}"

## DEFINING PATH TO SOLVENTS
path_to_molecules="${PREP_SOLVENT_FINAL}"

## DEFINING SCRIPT DIRECTORIES
path_to_script="${main_dir}/test_phenol_tma_scripts"

## DEFINING PATH TO MDP
path_to_mdp="${path_to_script}"

## DEFINING INPUT TOP
input_top="test_phenol_tma.top"
path_input_top="${path_to_script}/${input_top}"

## DEFINING FORCEFIELDS
declare -a ff_array=("${FORCEFIELD}" "${MODIFIED_FF}")
declare -a ff_prefix_array=("orig" "mod")

#############################
### DEFINING OUTPUT NAMES ###
#############################

## DEFINING OUTPUT PREFIX
output_prefix="nplm"

## DEFINING MOLECULE ARRAY
declare -a molecule_array=("${molecule_1}" "${molecule_2}")

## LOOPING THROUGH EACH FF
for each_ff in "${ff_array[@]}"; do

## OUTPUT DIRECTORY
output_dir="${molecule_1}-${molecule_2}-${initial_dist}-${each_ff}"

## DEFINING PATH TO DIRECTORY
path_to_output_dir="${path_to_sim}/${output_parent_dir}/${output_dir}"

###################
### MAIN SCRIPT ###
###################

## CREATING FOLDER
create_dir "${path_to_output_dir}" -f

## GOING TO FOLDER
cd "${path_to_output_dir}"

## COPY FORCE FIELD
cp -r "${path_forcefield}/${each_ff}" "${path_to_output_dir}"

## GETTING PRM AND ITP FILE
for each_mol in "${molecule_array[@]}"; do
	cp -r "${path_to_molecules}/${each_mol}/"{"${each_mol}".prm,"${each_mol}".itp,"${each_mol}".gro} "${path_to_output_dir}"
done


########################################
### STEP 1: CREATING GRO FILE SYSTEM ###
########################################
echo "-------------------------------"
echo "Step 1: Creating gro file system"
echo "-------------------------------"

## COPYING MOLECULE 1
cp "${molecule_1}.gro" "${output_prefix}.gro"

## CENTERING AND RE-SIZING
gmx editconf -f "${output_prefix}.gro" -o "${output_prefix}_center.gro" -center 0 0 0 -princ -box 2 2 2 << INPUTS
System
INPUTS

## ADDING POSITION
position_dat="positions.dat"
echo "0 0 ${initial_dist}" > "${position_dat}"

## USING INSERT MOLECULES
gmx insert-molecules -f "${output_prefix}_center.gro" \
					  -ci "${molecule_2}.gro" \
					  -ip "${position_dat}" \
					  -nmol "1" \
					  -o "${output_prefix}_center_added.gro" \
					  -rot z \
					  -dr 0 0 0

####################################
### STEP 2: CREATE TOPOLOGY FILE ###
####################################

echo "-------------------------------"
echo "Step 2: Creating topology file"
echo "-------------------------------"
## COPYING
cp -r "${path_input_top}" "${output_prefix}.top"

## EDITING
sed -i "s/_FORCEFIELD_/${each_ff}/g" "${output_prefix}.top"
sed -i "s/_MOLECULE1_/${molecule_1}/g" "${output_prefix}.top"
sed -i "s/_MOLECULE2_/${molecule_2}/g" "${output_prefix}.top"

## ADDING SINGLE MOLECULE OF EACH
for each_mol in "${molecule_array[@]}"; do
	## DEFINING ITP FILE
	itp_file="${each_mol}.itp"
	## READING RESIDUE NAME
	current_res_name="$(itp_get_resname ${itp_file})"
	## ADDING MOLECULES
	echo " ${current_res_name}   1" >> "${output_prefix}.top"
done

#################################
### STEP 3: CREATING MDP FILE ###
#################################

echo "-------------------------------"
echo "Step 3: Creating mdp files"
echo "-------------------------------"

## COPYING OVER FILES
cp -r "${path_to_mdp}"/{${mdp_em},${mdp_prod}} "${path_to_output_dir}"

## EDITING PROD FILE
sed -i "s/_NSTEPS_/${mdp_prod_time}/g" "${mdp_prod}"
sed -i "s/_DT_/${mdp_dt}/g" "${mdp_prod}"

###################################
### STEP 4a: BOX EXPANSION ###
###################################
echo "-------------------------------"
echo "Step 4a: Expanding the box"
echo "-------------------------------"

## CENTERING AND RE-SIZING
gmx editconf -f "${output_prefix}_center_added.gro" \
			 -o "${output_prefix}_center_added_expand.gro" \
			 -d "${dist_to_edge}"

###################################
### STEP 4: ENERGY MINIMIZATION ###
###################################
echo "-------------------------------"
echo "Step 4b: Energy minimizing"
echo "-------------------------------"

## ENERGY MINIMIZING
gmx grompp -f "${mdp_em}" \
		   -c "${output_prefix}_center_added_expand.gro" \
		   -p "${output_prefix}.top" \
		   -o "${output_prefix}_em.tpr"
gmx mdrun -v -nt 1 -deffnm "${output_prefix}_em"

## CREATING INDEX FILE
gmx make_ndx -f "${output_prefix}_em.tpr" -o "${output_prefix}.ndx" << INPUTS
q
INPUTS

###################################
### STEP 5: RUN/SUBMISSION FILE ###
###################################

echo "-------------------------------"
echo "Step 5: Generating run and submission file"
echo "-------------------------------"

## COPYING GENERAL RC
cp "${PATH_GENERAL_RC}" "${path_to_output_dir}"
## COPYING RUN AND SUBMIT
input_run="run.sh"
input_submit="submit.sh"

## DEFINING GROMACS VARIABLES
max_warn="5"
gromacs_command="gmx"
mdrun_command="gmx mdrun -nt"
mdrun_command_suffix=""
num_cores="28"

## COPYING FILES OVER
cp -r "${path_to_script}/"{${input_run},${input_submit}} "${path_to_output_dir}"

## EDITING RUN SCRIPT
sed -i "s/_RCFILE_/${GENERAL_RC_NAME}/g" "${input_run}"
sed -i "s/_PROD_MDPFILE_/${mdp_prod}/g" "${input_run}"
sed -i "s/_INITIALPREFIX_/${output_prefix}_em/g" "${input_run}"
sed -i "s/_MAXWARN_/${max_warn}/g" "${input_run}"
sed -i "s/_INDEXFILE_/${output_prefix}.ndx/g" "${input_run}"
sed -i "s/_INPUTTOPFILE_/${output_prefix}.top/g" "${input_run}"
sed -i "s/_TOTALTIMEPRODPS_/${total_time}/g" "${input_run}"
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" "${input_run}"

## EDITING SUBMIT SCRIPT
sed -i "s/_RUNSCRIPT_/${input_run}/g" "${input_submit}"
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${input_submit}"
sed -i "s/_MDRUNCOMMAND_/${mdrun_command}/g" "${input_submit}"
sed -i "s/_MDRUNCOMMANDSUFFIX_/${mdrun_command_suffix}/g" "${input_submit}"
sed -i "s/_GROMACSCOMMAND_/${gromacs_command}/g" "${input_submit}"
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${input_submit}"
sed -i "s/_JOB_NAME_/${output_dir}/g" "${input_submit}"

## ADDING TO JOB LIST
echo "${path_to_output_dir}/${input_submit}" >> "${JOB_SCRIPT}"
echo "Adding to job list: ${JOB_SCRIPT}"

done





