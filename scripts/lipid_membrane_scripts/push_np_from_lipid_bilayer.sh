#!/bin/bash

# push_np_from_lipid_bilayer.sh
# The purpose of this function is to push the np from lipid bilayer. 
#
# Instructions:
#   - Co to the directory
#   - copy this bash file to directory
#       cp -r /home/akchew/scratch/nanoparticle_project/scripts/lipid_membrane_scripts/push_np_from_lipid_bilayer.sh ./
#   - copy the MDP file to directory, located in:
#       cp "/home/akchew/scratch/nanoparticle_project/prep_files/input_files/MDP_FILES/charmm36/lipid_bilayers/pushing.mdp" ./
#   - run code:
#       bash push_np_from_lipid_bilayer.sh "." "pulling_edited.mdp" "pushing.mdp" "pushing_edited.mdp" "300.00"

## VARIABLES
#   $1: input path
#   $2: pulling mdp file -- useful for parameters within it
#   $3: input pushing mdp file
#   $4: output pushing mdp file
#   $5: temperature of the system in K

### FUNCTION TO STOP THE SCRIPT IF A FILE DOES NOT EXIST
# The purpose of this script is to stop the script if a file does not exist
# INPUTS:
#       $1: FILE NAME
# OUTPUT:
#       Prints out whether your file exists
# USAGE: stop_if_does_not_exist file_name
function stop_if_does_not_exist () {
    ## DEFINING INPUTS
    file_name_="$1"
    
    ## CHECKING
    if [ ! -e ${file_name_} ]; then
        echo "Error!!! ${file_name_} does not exist. It is an essential part of the script. Please double-check your inputs!"
        echo "Pausing here for 5 seconds so you can see this message!" ; sleep 5
        echo "Killing script ...."; sleep 3
        kill -s TERM $TOP_PID
    else
        echo "${file_name_} exists, continuing script!" ; sleep 2
    fi
}

#######################
### INPUT VARIABLES ###
#######################

## DEFINING INPUT PATH
input_path="$1"

## DEFINING INPUT MDP FILE
pulling_mdp_file="$2" # "pulling_edited.mdp"

## DEFINING PUSHING MDP FILE
input_pushing_mdp_file="$3"
output_pushing_mdp_file="$4"

## DEFINING TEMPERATURE
temperature="${5-300.00}"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING SUMMARY FILE
summary_file="np_to_lipid_bilayer_pulling.info"

###################
### MAIN SCRIPT ###
###################

## GOING TO WORKING DIRECTORY
cd "${input_path}"

## CHECKING FILES
stop_if_does_not_exist "${summary_file}"
stop_if_does_not_exist "${input_pushing_mdp_file}"

### EXTRACTION FROM SUMMARY FILE
## DEFINING TIME STEP
time_step=$(grep "Time step (ps)" ${summary_file} | awk '{print $NF}')
## DEFINING SPRING CONSTANT
spring_constant="$(grep "Spring constant (Gromacs units)" ${summary_file} | awk '{print $NF}')"
## TOTAL NUBER OF STEPS
total_steps=$(grep "Total number of steps" ${summary_file} | awk '{print $NF}')

## DEFINING PULL RATE
pull_rate="$(grep "Rate (nm/ps)" ${summary_file} | awk '{print $NF}')"

### EXTRACTING PULL GROUPS FROM MDP
pull_group1_name=$(grep "pull_group1_name" "${pulling_mdp_file}"  | awk '{print $NF}')
pull_group2_name=$(grep "pull_group2_name" "${pulling_mdp_file}"  | awk '{print $NF}')

########################
### EDITING MDP FILE ###
########################

sed "s/_DT_/${time_step}/g" "${input_pushing_mdp_file}" > "${output_pushing_mdp_file}"
sed -i "s/_GROUP_1_NAME_/${pull_group1_name}/g" "${output_pushing_mdp_file}"
sed -i "s/_GROUP_2_NAME_/${pull_group2_name}/g" "${output_pushing_mdp_file}"
sed -i "s/_NSTEPS_/${total_steps}/g" "${output_pushing_mdp_file}"
sed -i "s/_TEMPERATURE_/${temperature}/g" "${output_pushing_mdp_file}"
sed -i "s/_PULLRATE_/${pull_rate}/g" "${output_pushing_mdp_file}"
sed -i "s/_PULLKCONSTANT_/${spring_constant}/g" "${output_pushing_mdp_file}"
