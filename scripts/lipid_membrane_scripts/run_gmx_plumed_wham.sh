#!/bin/bash

# run_gmx_plumed_wham.sh
# This script is designed to run GROMACS PLUMED wham. 
# 
# Created on: 05/25/2020
# Written by: Alex K. Chew
# 
# Special thanks to Reid Van Lehn, Brad Dallin, and Jonathan Sheavly for developing the initial code
# 
# Reference:
# 	WHAM: 
#		http://membrane.urmc.rochester.edu/sites/default/files/wham/doc.pdf

### SOURCING ALL VARIABLES / FUNCTIONS
source "../../bin/nanoparticle_rc.sh"

### CONVERT SPRING TO KCAL
# The purpose of this function is to convert the spring constant in kJ/mol to kcal/mol.
# INPUTS:
#	$1: Energy in kJ units
# OUTPUTS:
# 	Energy in kcal units
# USAGE EXAMPLE:
#	spring_kcal="$(kJ2kcal ${spring_constant})"
function kJ2kcal () {
	## DEFINING ENERGY
	energy_kj_="$1"

	## DEFINING CONVERSION
	conversion="4.184" # kJ for 1 kcal

	## COMPUTING
	energy_kcal=$(awk -v energy_kJ=${energy_kj_} \
					  -v conv=${conversion} \
					  'BEGIN{ printf "%.5f", energy_kJ / conv }')
	## PRINTING
	echo "${energy_kcal}"
}

### FUNCTION TO CLEAN UP GROMACS PULLX FILES
# This function cleans pull x files outputted from GROMACS by removing all comments
# The comments of "#" and "@" are removed
# INPUTS:
#	$1: input_xvg_
#		input xvg file
#	$2: output_xvg_
#		output xvg file that is cleaned
# OUTPUTS:
#	clean svg file
# USAGE:
#	clean_pullx_files nplm_prod_pullx.xvg nplm_prod_pullx_TEST.xvg
function clean_pullx_files () {
	## DEFINING INPUTS
	input_xvg_="$1"
	output_xvg_="$2"

	if [ ! -z "${output_xvg_}" ]; then
		sed '/^#/d' "${input_xvg_}" | sed '/^@/d' > "${output_xvg_}"
	else
		echo "Error! No 'output_xvg_' inputted for 'clean_pullx_files' function"
	fi
}

##################
### INPUT VARS ###
##################

## DEFINING PATH TO SIM
full_path_to_sim="/home/akchew/scratch/nanoparticle_project/nplm_sims"

## DECLARING SIM FOLDERS
declare -a run_these_jobs_array=(\
	# "US_ALL_LIG_CONTACTS" \
	# "US_HYDRO_CONTACTS_1_SPR" \
	# "US_HYDRO_CONTACTS_10_SPR" \
	# "US_Z_PULLING" \
	# "US_Z_PUSHING" \
	"BN_NEW_FF_US_HYDRO_CONTACTS_10_SPR"
	)

## JOB DESCRIPTION:
#	US_ALL_LIG_CONTACTS: Umbrella sampling simulations using all contacts
#	US_HYDRO_CONTACTS_1_SPR: umbrella sampling simulations with only hydrophobic contacts, 1 spring constant
#	US_HYDRO_CONTACTS_10_SPR: same as above, but with 10 spring constants
#	US_Z_PULLING: umbrella sampling simulation with z-component pulling
#	US_Z_PUSHING: umbrella sampling simulation with z-component pushing


## GETTING JOB TYPE 
declare -a job_type_array=(\
	# "wham" \
	"sampling_time" \
	"sampling_time_rev" \
	)
# sampling_time_rev: sampling time but in the reverse, starting at the end
# sampling_time: This runs the sampling time algorithm
# wham: This just runs WHAM using all the data

## LOOPING THROUGH EACH JOB
for run_these_jobs in "${run_these_jobs_array[@]}"; do
	## PRINTING
	echo "Running the following job: ${run_these_jobs}"

###############################
### DEFAULT WHAM PARAMETERS ###
###############################

## WHAM PARAMETERS
WHAM_TOLERANCE=1e-6
# WHAM_TOLERANCE=1e-9 # Decreasing wham tolerance

## DEFINING NUMBER OF BINS
num_bins="300"
# 400 # <-- used before
# 400
# Increasing number of bins smoothes out the umbrella sampling curves!
# 200
# 200
sim_temperature=300

## DEFINING IF YOU WANT LAST 5 NS
wham_from_last_time="40000.000"
# "20000.000"
# "20000.000" # <-- used for final sims in plumed
# "10000.000"
# "10000.000"
# "12000.000"
# "10000.000"
# "15000.000"

## SAMPLING TIME DETAILS
first_time="0"  # ps
interval="5000" # Every 5 ns
# "2000"
# "3000" # ps
last_time="${wham_from_last_time}"
# "20000" # last time in ps
# "${wham_from_last_time}"
# "20000" # ps


# "5000.000"
# "7000.000"
# "5000.000"
# "10000.000" # 10000.000
# This functionality is designed to run wham using the last 10 ns of the umbrella sampling run. 
# The idea is that the PMF would need more time for specific contacts, thus we run simulations 
# for prolonged times, but compute wham with the same extents of time for each window.
# If this flag is "", then we will use the entire trajectory data to compute wham. 

## DEFAULT SPRING CONSTANT
spring_constant="1" # kJ/mol/contacts2

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING DIRECTORY STRUCTURE
relative_path_to_sim="4_simulations"

## DEFINING OUTPUT WHAM
relative_path_to_wham="wham"

## DEFINING META FILE NAME
meta_file_name="umbrella_metadata.dat"
pre_free_file="umbrella_free_energy.dat"
log_file="umbrella_free_energy.log"

## DEFINING PROD COVAR FILE
prod_covar_file="nplm_prod_COVAR.dat"

# # ------------- USING ALL NANOPARTICLE LIGAND ATOMS FOR CONTACTS
if [[ " ${run_these_jobs[@]} " =~ " US_ALL_LIG_CONTACTS " ]]; then
	## DEFINING PARENT DIR
	parent_dir="20200522-plumed_US_initial"

	## DEFINING SIMULATION FILES
	declare -a sim_folders=(\
		"US_1-NPLMplumedcontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1" \
		"US_1-NPLMplumedcontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
		)

	declare -a specific_contacts=("0" "5" "10" "20" "30" "40" "50" "60" "70" "80" "90" "100" "110" "120" "130" "140" "150" \
								  "160" "170" "180" "190" "200" "210" "220" "230")

	## DEFINING BIN
	bin_min=0
	bin_max=240

fi

# ------- USING ONLY HYDROPHOBIC ATOMS OF NP LIGANDS FOR CONTACTS -- 1 spring constant
if [[ " ${run_these_jobs[@]} " =~ " US_HYDRO_CONTACTS_1_SPR " ]]; then
	## DEFINING PARENT DIR
	parent_dir="20200609-US_with_hydrophobic_contacts"

	## DEFINING SIMULATION FILES
	declare -a sim_folders=(\
		"UShydrophobic_1-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1" \
		# "UShydrophobic_1-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
		)

	declare -a specific_contacts=("0" "5" "10" "20" "30" "40" "50" "60" "70" "80" "90" "100" "110" "120" "130" "140" "150")

	## DEFINING BINS
	bin_min=0
	bin_max=150

fi

# ------- USING ONLY HYDROPHOBIC ATOMS OF NP LIGANDS FOR CONTACTS -- 10 spring constant
if [[ " ${run_these_jobs[@]} " =~ " US_HYDRO_CONTACTS_10_SPR " ]]; then

	parent_dir="20200615-US_PLUMED_rerun_with_10_spring"
	parent_dir="20200615-US_PLUMED_rerun_with_10_spring_extended"

	## DEFINING SIMULATION FILES
	declare -a sim_folders=(\
		# "UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1" \
		"UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1" \
		# "UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-50000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1" \
		# "UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT004_1" \
		)

	# ## SECOND ITERATION
	# parent_dir="20200713-US_PLUMED_iter2"
	# # "20200615-US_PLUMED_rerun_with_10_spring"

	# ## DEFINING SIMULATION FILES
	# declare -a sim_folders=(\
	# 	"UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-25000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1" \
	# 	"UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-25000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1" \
	# 	# "UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-25000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1" \
	# 	)


	## DEFINING SPECIFIC CONTACTS
	declare -a specific_contacts=("0.0" "2.5" "5.0" "7.5" "10.0" "12.5" "15.0" "17.5" "20.0" \
								  "22.5" "25.0" "27.5" "30.0" "32.5" "35.0" "37.5" \
								  "40.0" "42.5" "45.0" "47.5" "50.0" "52.5" "55.0" "57.5" "60.0" "62.5" \
								  "65.0" "67.5" "70.0" "72.5" "75.0" "77.5" "80.0" "82.5" "85.0" "87.5" "90.0" \
								  "92.5" "95.0" "97.5" "100.0" \
                            	  "102.5" "105.0" "107.5" "110.0" "112.5" "115.0" "117.5" "120.0" "122.5" "125.0" \
	                              "127.0" "127.5" "130.0" "132.5" "135.0" "137.5" "140.0" "142.5" "145.0" "147.5" "150.0"
								  ) #  "4.0" 


	## DEFINING SPECIFIC CONTACTS
	declare -a specific_contacts=("0.0" "2.5" "5.0" "7.5" "10.0" "12.5" "15.0" "17.5" "20.0" \
								  "22.5" "25.0" "27.5" "30.0" "32.5" "35.0" "37.5" \
								  "40.0" "42.5" "45.0" "47.5" "50.0" "52.5" "55.0" "57.5" "60.0" "62.5" \
								  "65.0" "67.5" "70.0" "72.5" "75.0" "77.5" "80.0" "82.5" "85.0" "87.5" "90.0" \
								  "92.5" "95.0" "97.5" "100.0" \
                            	  "102.5" "105.0" "107.5" "110.0" "112.5" "115.0" "117.5" "120.0" "122.5" "125.0" \
	                              "127.0" "127.5" "130.0" "132.5" "135.0" "137.5" "140.0" "142.5" "145.0" "147.5" "150.0"
								  ) #  "4.0" 

	# ## DEFINING SPECIFIC CONTACTS
	# declare -a specific_contacts=("0.0" "2.5" "5.0" "7.5" "10.0" "12.5" "15.0" "17.5" "20.0" \
	# 							  "22.5" "25.0" "27.5" "30.0" "32.5" "35.0" "37.5" \
	# 							  "40.0" "42.5" "45.0" "47.5" "50.0" "52.5" "55.0" "57.5" "60.0" "62.5" \
	# 							  "65.0" "67.5" "70.0" "72.5" "75.0" "77.5" "80.0" "82.5" "85.0" "87.5" "90.0" \
	# 							  "92.5" "95.0" "97.5" "100.0" \
 #                            	  "102.5" "105.0" "107.5" "110.0" "112.5" "115.0" "117.5" "120.0" "122.5" "125.0" \
	#                               "127.0" "127.5" "132.5" "135.0" "137.5" "140.0" "142.5" "145.0" "147.5" "150.0"
	# 							  ) #  "4.0"   "130.0" 

	# declare -a specific_contacts=("0.0" "2.5" "5.0" "7.5" "10.0" "12.5" "15.0" "17.5" "20.0" "22.5" \
	#                "25" "27.5" "30" "32.5" "35" "37.5" "40" "42.5" "45" "47.5" "50" "52.5" "55" "57.5" \
	#                "60" "62.5" "65" "70" "75" "80" "85" "90" "95" "100")
	## DEFINING IF YOU WANT LAST 5 NS
	wham_from_last_time="60000.000"

	bin_min=0
	bin_max=160

	## SAMPLING TIME DETAILS
	first_time="0"  # ps
	interval="10000" # ps
	last_time="${wham_from_last_time}" # ps

	## DEFINING SPRING CONSTANT
	spring_constant="10" # kJ/mol/contacts2
	echo "*** Using spring constant of 10 for this directory! ***"

	## DEFINING BLOCK AVERAGING
	block_average_split="2"
	# TURNING OFF FOR ROT017
fi

## NEW FORCE FIELD PARAMETERS FOR BENZENE
if [[ " ${run_these_jobs[@]} " =~ " BN_NEW_FF_US_HYDRO_CONTACTS_10_SPR " ]]; then

	parent_dir="20200822-Hydrophobic_contacts_PMF_Bn_new_FF"

	## DEFINING SIMULATION FILES
	declare -a sim_folders=(\
		"UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-50000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1" \
		)



	## DEFINING SPECIFIC CONTACTS
	declare -a specific_contacts=("0.0" "2.5" "5.0" "7.5" "10.0" "12.5" "15.0" "17.5" "20.0" \
								  "22.5" "25.0" "27.5" "30.0" "32.5" "35.0" "37.5" \
								  "40.0" "42.5" "45.0" "47.5" "50.0" "52.5" "55.0" "57.5" "60.0" "62.5" \
								  "65.0" "67.5" "70.0" "72.5" "75.0" "77.5" "80.0" "82.5" "85.0" "87.5" "90.0" \
								  "92.5" "95.0" "97.5" "100.0" \
                            	  "102.5" "105.0" "107.5" "110.0" "112.5" "115.0" "117.5" "120.0" "122.5" "125.0" \
	                              "127.0" "127.5" "130.0" "132.5" "135.0" "137.5" "140.0" "142.5" "145.0" "147.5" "150.0"
								  ) #  "4.0" 

	## DEFINING IF YOU WANT LAST 5 NS
	wham_from_last_time="40000.000"

	bin_min=0
	bin_max=160

	## SAMPLING TIME DETAILS
	first_time="0"  # ps
	interval="10000" # ps
	last_time="${wham_from_last_time}" # ps

	## DEFINING SPRING CONSTANT
	spring_constant="10" # kJ/mol/contacts2
	echo "*** Using spring constant of 10 for this directory! ***"

	## DEFINING BLOCK AVERAGING (TURNED OFF)
	block_average_split="2"
fi

# ------- USING PULLING Z SIMULATIONS
if [[ " ${run_these_jobs[@]} " =~ " US_Z_PULLING " ]]; then

	## DEFINING PARENT DIR
	parent_dir="US_Z_PULLING_SIMS"

	## DEFINING SIMULATION FILES
	declare -a sim_folders=(\
		"US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1" \
		"US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1" 
		)

	## DEFINING SPECIFIC CONTACTS
	declare -a specific_contacts=""

	## DEFINING MIN AND MAX BINS
	# bin_min=0
	bin_min=1.2
	bin_max=6.6

	## DEFINING SPRING CONSTANT
	spring_constant="2000" # kJ/mol/contacts2
	echo "*** Using spring constant of 2000 for this directory! ***"

	## CHANGING COVAR
	prod_covar_file="nplm_prod_pullx.xvg"

	## CHANGING LAST TIME
	wham_from_last_time="40000.000"

	## SAMPLING TIME DETAILS
	first_time="0"  # ps
	interval="5000" # ps
	last_time="${wham_from_last_time}" # ps

	## DEFINING BLOCK AVERAGING
	block_average_split="2"

fi

# ------- USING PULLING Z SIMULATIONS
if [[ " ${run_these_jobs[@]} " =~ " US_Z_PUSHING " ]]; then

	## DEFINING PARENT DIR
	parent_dir="US_Z_PUSHING_SIMS"

	## DEFINING SIMULATION FILES
	declare -a sim_folders=(\
		"USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT001_1"
		"USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"
		)

	## DEFINING SPECIFIC CONTACTS
	declare -a specific_contacts=""

	## DEFINING MIN AND MAX BINS
	bin_min=1.2
	bin_max=6.6

	## DEFINING SPRING CONSTANT
	spring_constant="2000" # kJ/mol/contacts2
	echo "*** Using spring constant of 2000 for this directory! ***"

	## CHANGING COVAR
	prod_covar_file="nplm_prod_pullx.xvg"

	## CHANGING LAST TIME
	wham_from_last_time="40000.000"

	## SAMPLING TIME DETAILS
	first_time="0"  # ps
	interval="5000" # ps
	last_time="${wham_from_last_time}" # ps

	## DEFINING BLOCK AVERAGING
	block_average_split="2"

fi

## LOOPING THROUGH EACH SIM
for each_sim in "${sim_folders[@]}"; do
	## PRINTING
	echo -e "\n======================"
	echo "Working on: ${each_sim}"
	## DEFINING PATH TO SIMULATION
	path_to_parent_folder="${full_path_to_sim}/${parent_dir}/${each_sim}"

	######################
	### DEFINING PATHS ###
	######################

	## PATH TO SIMS
	path_to_sims="${path_to_parent_folder}/${relative_path_to_sim}"

	## PATH TO WHAM
	path_to_wham="${path_to_parent_folder}/${relative_path_to_wham}"

	## DEFINING PYTHON SCRIPTS
	PYTHON_SCRIPT_TRUNCATE="${MDBUILDER}/umbrella_sampling_plumed/truncate_data.py"

	###################
	### MAIN SCRIPT ###
	###################

	## CHECKING IF PARENT FILE EXISTS
	stop_if_does_not_exist "${path_to_parent_folder}"

	## CREATING 
	if [ ! -e "${path_to_wham}" ]; then
		mkdir "${path_to_wham}"
	fi

	## READING ALL POSSIBLE SIMS
	if [[ -z "${specific_contacts}" ]]; then
		read -a sims_list <<< $(ls "${path_to_sims}" | sort -V )
	else
		echo "Since specific contacts are specified, only analyzing those ones!"
		echo "Specific contacts: ${specific_contacts[@]}"
		declare -a sims_list=(${specific_contacts[@]})
	fi

	## DEFINING ENERGY IN KCAL
	spring_kcal="$(kJ2kcal ${spring_constant})"
	echo "Spring constant: ${spring_constant} kJ/mol/coord^2 (${spring_kcal} kcal/mol/coord^2)"

	## LOOPING THROUGH JOB NAME
	for job_type in ${job_type_array[@]}; do

		####################
		### RUNNING WHAM ###
		####################
		if [[ "${job_type}" == "wham" ]]; then

			if [ ! -z "${block_average_split}" ]; then
				echo "Block averaging split number: ${block_average_split}"
				echo "Dividing ${wham_from_last_time} into ${block_average_split} partitions"
    		else
    			block_average_split=1
    			echo "No block averaging performed!"
			fi

			## CREATING TIME STEP
			time_step=$(awk -v last=${wham_from_last_time} \
							-v num_split=${block_average_split} \
							 'BEGIN{ printf "%d", last / num_split }')
			## CREATING SEQUENCE OF ARRAY
			read -a array_seq <<< $(seq 0 ${time_step} ${wham_from_last_time})

			## LOOPING
			for each_block in $(seq 1 ${block_average_split}); do
				## GETTING TO ZERO
				block_to_zero=$((${each_block}-1))

				## INITIAL AND FINAL TIME
				current_initial_time=${array_seq[block_to_zero]}
				current_final_time=${array_seq[each_block]}

				echo "Initial: ${current_initial_time}"
				echo "Final: ${current_final_time}"

				## DEFINING META FILE NAME
				current_meta_file="${meta_file_name%.dat}_${block_to_zero}.dat"
				current_log_file="${log_file%.log}_${block_to_zero}.log"
				current_pre_free_file="${pre_free_file%.dat}_${block_to_zero}.dat"

				## DEFINING CURRENT PATH
				path_to_wham_log="${path_to_wham}/${current_log_file}"
				path_to_wham_fe="${path_to_wham}/${current_pre_free_file}"
				path_to_wham_metadata="${path_to_wham}/${current_meta_file}"

				## PERFORMING REMAINDER WHAM
				## CREATING META DATA FILE
				echo "# Path    loc_win_min     spring    [correl time]   [temperature]" > "${path_to_wham_metadata}"

				## LOOPING THROUGH SIM LIST
				for each_sim in "${sims_list[@]}"; do
					## DEFINING WORK SPACE
					wd="${path_to_sims}/${each_sim}"

					## GETTING FILE NAME AND EXTENSION
					filename="${prod_covar_file%.*}"
					extension="${prod_covar_file##*.}"

					## CHECKING IF EXISTS
					if [ -e "${wd}/${prod_covar_file}" ]; then

						## CLEANING DATA IF COVAR IS SPECIFIC FILE
						if [[ "${prod_covar_file}" == "nplm_prod_pullx.xvg" ]]; then
							## DEFINING NEW COVAR
							clean_covar="${filename}_clean.xvg"

							## CLEANING XVG
							clean_pullx_files "${wd}/${prod_covar_file}" "${wd}/${clean_covar}"

							## DEFINING NEW COVAR
							current_covar_file="${clean_covar}"

						else
							current_covar_file="${prod_covar_file}"
						fi

						## GETTING FILE NAME AND EXTENSION
						filename="${current_covar_file%.*}"
						extension="${current_covar_file##*.}"

						## PATH TO COVAR
						sim_to_covar="${wd}/${current_covar_file}"

						## SEEING IF YOU WANT LAST N NS
						if [[ ! -z "${wham_from_last_time}" ]]; then

							## FINDING THE LAST TIME
							current_last_time=$(tail -n1 "${sim_to_covar}" | awk '{print $1}')

							## FINDING THE INITIAL TIME BASED ON THE LAST TIME
							wham_current_initial_time=$(awk -v last=${current_last_time} \
				    									 	-v interval=${wham_from_last_time} \
				    								 		-v first_time=${current_initial_time} \
				    								 		'BEGIN{ printf "%.6f", last - interval + first_time }')

				    		## FINDING CURRENT FINAL TIME
							wham_current_final_time=$(awk -v last=${current_last_time} \
				    									 	-v interval=${wham_from_last_time} \
				    								 		-v first_time=${current_final_time} \
				    								 		'BEGIN{ printf "%.6f", last - interval + first_time }')
				    								 		# 


							## ECHOING
							echo "Running WHAM for the last ${wham_from_last_time} ps"
							echo "--> Initial time: ${wham_current_initial_time} ps"
							echo "--> Final time: ${wham_current_final_time} ps" 
							echo "--> Input covar file: ${current_covar_file}"
							echo "Working directory: ${wd}"

							## DEFINING NEW COVAR
							new_covar_file="${filename}-WHAM-${wham_current_initial_time}-${wham_current_final_time}_ps.dat"
							# last_${wham_from_last_time}_ps.dat"

							## RUNNING TRUNCATION CODE
							python3.6 "${PYTHON_SCRIPT_TRUNCATE}" --wd "${wd}" \
															   --in "${current_covar_file}" \
															   --out "${new_covar_file}" \
															   --begin "${wham_current_initial_time}" \
															   --end "${wham_current_final_time}"
							## DEFINING NEW COVAR PATH
							specific_path_to_covar="${wd}/${new_covar_file}"
						else
							specific_path_to_covar="${sim_to_covar}"
						fi

						## CHECKING SPRING CONSTANT
						if [[ "${each_sim}" == *"_"* ]]; then 
							echo "Since '_' is within sim name, we are editing the spring constant!"
							echo "Sim name: ${each_sim}"
							current_position="$(echo "${each_sim}" | awk '{split($0,a,"_"); print a[1]}')"
							current_spring_constant_kJ="$(echo "${each_sim}" | awk '{split($0,a,"_"); print a[2]}')"
							current_spring_constant_kcal="$(kJ2kcal ${current_spring_constant_kJ})"

						else
							current_position="${each_sim}"
							current_spring_constant_kJ="${spring_constant}"
							current_spring_constant_kcal="${spring_kcal}"
						fi

						echo "Spring constant: ${current_spring_constant_kJ} kJ/mol/coord^2 (${current_spring_constant_kcal} kcal/mol/coord^2)"
						## ADDING
						echo "${specific_path_to_covar} ${current_position} ${current_spring_constant_kcal}" >> "${path_to_wham_metadata}"
					else
						echo "Skipping covar file (does not exist): ${wd}/${prod_covar_file}"
					fi
				done

				## RUNNING WHAM
				echo "Running wham..."
				echo "Check log in: ${path_to_wham_log}"
				"${WHAM_SCRIPT}" ${bin_min} \
								 ${bin_max} \
								 ${num_bins} \
								 ${WHAM_TOLERANCE} \
								 ${sim_temperature} \
								 0 \
								 ${path_to_wham_metadata} \
								 ${path_to_wham_fe} > "${path_to_wham_log}"



				echo "${path_to_wham_metadata}"
			done


			## DEFINING PATH TO META DATA







		#####################
		### SAMPLING TIME ###
		#####################
		elif [[ "${job_type}" == "sampling_time" ]] || [[ "${job_type}" == "sampling_time_rev" ]] ; then

			## DEFINING TOTAL TIME FROM END
			time_from_end="${wham_from_last_time}"

			## CALCULATING TOTAL REPEATS OF GMX WHAM
			total_repeats=$(awk -v first=${first_time} \
							    -v last=${last_time} \
							    -v interval=${interval} 'BEGIN{ printf "%d", (last-first)/interval - 1 }')

			## PRINTING
			echo "-----------------------"
			echo "Sampling time is turned on!"
			echo "Total repeats: ${total_repeats}"
			echo "First time: ${first_time}"
			echo "Last time: ${last_time}"
			echo "Inverval: ${interval}"
			echo "-----------------------"
			## LOOPING THROUGH THE REPEATS
			for each_index in $(seq 0 ${total_repeats}); do

				## TIME FOR SAMPLING
				if [[ "${job_type}" == "sampling_time" ]]; then

					## DEFINING INITIAL TIME
					initial_time="${first_time}"

					## FINDING CURRENT INTERVAL
			    	current_index_time=$(awk -v first=${initial_time} \
			    							 -v current_index=${each_index} \
			    							 -v interval=${interval} \
			    							 'BEGIN{ printf "%d", first + (current_index+1)*interval }')

					## DEFINING CURRENT FREE ENERGY FILE
					current_fe_file="${pre_free_file%.dat}-forward-${time_from_end}_ps_from_end-${initial_time}_${current_index_time}.dat"

		    	elif [[ "${job_type}" == "sampling_time_rev" ]]; then
		    		# Run the reverse case
					## DEFINING INITIAL TIME
					initial_time=$(awk -v last=${last_time} \
		    							 -v current_index=${each_index} \
		    							 -v interval=${interval} \
		    							 'BEGIN{ printf "%d", last - (current_index+1)*interval }')

					## FINDING CURRENT INTERVAL
			    	current_index_time="${last_time}"

					## DEFINING CURRENT FREE ENERGY FILE
					current_fe_file="${pre_free_file%.dat}-rev-${time_from_end}_ps_from_end-${initial_time}_${current_index_time}.dat"

		    	fi

		    	echo "Initial times: ${initial_time}"
		    	echo "Current time: ${current_index_time}"

		    	# ------

		    	## PRINTING
				echo "Running WHAM for ${each_index} out of ${total_repeats} -- ${initial_time} to ${current_index_time} ps -- OUTPUT SUPPRESSED"

				## DEFINING META FILE
				current_meta_file="${current_fe_file%.dat}_metadata.txt"
				# "${meta_file_name%.dat}-${time_from_end}_ps_from_end-${initial_time}_${current_index_time}.dat"
				current_path_to_wham_metadata="${path_to_wham}/${current_meta_file}"

				## CREATING META DATA FILE
				echo "# Path    loc_win_min     spring    [correl time]   [temperature]" > "${current_path_to_wham_metadata}"

				## LOOPING THROUGH SIM LIST AND WORKING ON TRUNCATING
				for each_sim in "${sims_list[@]}"; do
					wd="${path_to_sims}/${each_sim}"

					## GETTING FILE NAME AND EXTENSION
					filename="${prod_covar_file%.*}"
					extension="${prod_covar_file##*.}"

					## CHECKING IF EXISTS
					if [ -e "${wd}/${prod_covar_file}" ]; then

						## CLEANING DATA IF COVAR IS SPECIFIC FILE
						if [[ "${prod_covar_file}" == "nplm_prod_pullx.xvg" ]]; then
							## DEFINING NEW COVAR
							clean_covar="${filename}_clean.xvg"

							## CLEANING XVG
							clean_pullx_files "${wd}/${prod_covar_file}" "${wd}/${clean_covar}"

							## DEFINING NEW COVAR
							current_covar_file="${clean_covar}"

							## GETTING FILE NAME AND EXTENSION
							filename="${current_covar_file%.*}"
							extension="${current_covar_file##*.}"

						else
							current_covar_file="${prod_covar_file}"
						fi

						## PATH TO COVAR
						sim_to_covar="${wd}/${current_covar_file}"

						## DEFINING NEW COVAR
						new_covar_file="${filename}-${time_from_end}_ps_from_end-${initial_time}_${current_index_time}.dat"

						## COMPUTING TIMES FOR TRUNCATION
						## FINDING THE LAST TIME
						f_time=$(tail -n1 "${sim_to_covar}" | awk '{print $1}')

						## FINDING THE INITIAL TIME BASED ON THE LAST TIME
						init_time=$(awk -v last=${f_time} \
		    							 -v interval=${time_from_end} \
		    							 -v current_time=${initial_time} \
		    							 'BEGIN{ printf "%.6f", last - interval + current_time }')

		    			## DEFINING FINAL TIME
		    			final_time=$(awk -v last=${f_time} \
		    							 -v interval=${time_from_end} \
		    							 -v current_time=${current_index_time} \
		    							 'BEGIN{ printf "%.6f", last - interval + current_time }')

						## ECHOING
						echo "    Running WHAM for the last ${time_from_end} ps // Initial - last time: ${init_time} - ${final_time} ps"

						## RUNNING TRUNCATION CODE
						python3.6 "${PYTHON_SCRIPT_TRUNCATE}" --wd "${wd}" \
														   --in "${current_covar_file}" \
														   --out "${new_covar_file}" \
														   --begin "${init_time}" \
														   --end "${final_time}"

						## DEFINING PATH TO NEW COVAR
						path_to_new_covar="${wd}/${new_covar_file}"

						## CHECKING SPRING CONSTANT
						if [[ "${each_sim}" == *"_"* ]]; then 
							echo "Since '_' is within sim name, we are editing the spring constant!"
							echo "Sim name: ${each_sim}"
							current_position="$(echo "${each_sim}" | awk '{split($0,a,"_"); print a[1]}')"
							current_spring_constant_kJ="$(echo "${each_sim}" | awk '{split($0,a,"_"); print a[2]}')"
							current_spring_constant_kcal="$(kJ2kcal ${current_spring_constant_kJ})"

						else
							current_position="${each_sim}"
							current_spring_constant_kJ="${spring_constant}"
							current_spring_constant_kcal="${spring_kcal}"

					fi
						echo "Spring constant: ${current_spring_constant_kJ} kJ/mol/coord^2 (${current_spring_constant_kcal} kcal/mol/coord^2)"
						## ADDING
						echo "${path_to_new_covar} ${current_position} ${current_spring_constant_kcal}" >> "${current_path_to_wham_metadata}"
					else
						echo "Skipping covar since it does not exist: ${wd}/${prod_covar_file}"
					fi

				done
				## DEFINING PATH TO FREE ENERGY
				current_path_to_fe="${path_to_wham}/${current_fe_file}"

				## DEFINING CURRENT LOG

				current_log_file="${current_fe_file%.dat}.log"
				# "${log_file%.log}-${initial_time}_${current_index_time}.log"
				current_path_to_log="${path_to_wham}/${current_log_file}"

				# ---
				## RUNNING WHAM
				echo "Running wham..."
				echo "Log file: ${current_path_to_log}"
				"${WHAM_SCRIPT}" ${bin_min} \
								 ${bin_max} \
								 ${num_bins} \
								 ${WHAM_TOLERANCE} \
								 ${sim_temperature} \
								 0 \
								 ${current_path_to_wham_metadata} \
								 ${current_path_to_fe} > "${current_path_to_log}"



			done


		fi

		# ------
	done

	
done


done