#!/bin/bash
# prep_energy_comparison_rerun_ff_for_pure_water.sh
# This function is to compare nanoparticles pure water simulations between 
# force field parameters. The idea is that we will copy both forcefield files 
# into the folder and re-analyze the system using each forcefield parameter. 
#
# Written by: Alex K. Chew (09/01/2020)


#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name


###########################
### INITIALIZING INPUTS ###
###########################

## DEFINING TYPE
analysis_folder_type="np_water_sims"
analysis_folder_type="phe_tma_sims"

if [[ "${analysis_folder_type}" == "np_water_sims" ]]; then

	## DEFINING PARENT SIMULATION
	parent_sim_path="${PATH2SIM}"

	## DEFINING SPECIFIC SIM PATHS
	declare -a specific_sim_paths=("ROT_WATER_SIMS/EAM_300.00_K_2_nmDIAM_ROT017_CHARMM36jul2017_Trial_1" \
								   "ROT_WATER_SIMS_MODIFIEDFF/EAM_300.00_K_2_nmDIAM_ROT017_CHARMM36jul2017mod_Trial_1")




	## DEFINING MDP FILE
	mdp_file="npt_double_prod_gmx5_charmm36_10ps.mdp"

	## DEFINING SIM PREFIX
	sim_prefix="sam"

elif [[ "${analysis_folder_type}" == "phe_tma_sims" ]]; then

	## DEFINING PARENT SIMULATION
	parent_sim_path="${PATH2NPLMSIMS}"

	## DEFINING SPECIFIC SIM PATHS
	declare -a specific_sim_paths=("20200902-phenol_tma_tests/phenol-tetramethylammonium-0.4-charmm36-jul2017.ff" \
								   "20200902-phenol_tma_tests/phenol-tetramethylammonium-0.4-charmm36-jul2017-mod.ff")
	

	## DEFINING MDP FILE
	mdp_file="prod.mdp"

	## DEFINING SIM PREFIX
	sim_prefix="nplm"



fi

## DEFINING GROMACS
num_cores="15"
# Could not do 20 cores!

## DEFINING IF YOU WANT TO REWRITE TPR
rewrite=true
rewrite_tpr=true

##############################
### FORCEFIELD INFORMATION ###
##############################

## DEFINING PATH TO FORCEFIELD
path_to_forcefield="${PATH_FORCEFIELD}"

## DEFINING FORCE FIELD NAMES
declare -a ff_array=("${FORCEFIELD}" "${MODIFIED_FF}")
declare -a ff_prefix_array=("orig" "mod")
# Note: prefixes should be same length as array

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING MAIN PREFIX
output_prefix="ffcomparison"

## DEFINING PREFIX
prod_prefix="${sim_prefix}_prod" # Production run prefix

## TOPOLOGY FILE
top_file="${sim_prefix}.top"

## DEFINING DEFAULT LIST
job_list="energy_comparison_job_list_pure_water.txt"
path_job_list="${main_dir}/${job_list}"

## DEFINING OUTPUT INDEX
output_index_file="${output_prefix}.ndx"

## DEFINING GROMACS COMMAND
gromacs_command="gmx"

## DEFININT OUTPUT MDP FILE
output_mdp_file="${output_prefix}.mdp"

## RUN SCRIPT
input_run_file="run_energy_comparison_rerun_ff.sh"
output_run_file="${output_prefix}_run.sh"

## DEFINING PATH TO INPUT
path_input_run_file="${main_dir}/${input_run_file}"

##################################################
### MAIN SCRIPT 
##################################################

## CREATING FILE
> "${path_job_list}"

## LOOPING
for specific_sim in "${specific_sim_paths[@]}"; do

## DEFINING PATH TO SIM
main_sim_path="${parent_sim_path}/${specific_sim}"

## CHECKING EXISTANCE
stop_if_does_not_exist "${main_sim_path}"

## GOING TO DIRECTORY
cd "${main_sim_path}"

#####################################
### STEP 1: EDITING TOPOLOGY FILE ###
#####################################
echo "--------------------------------------------"
echo "Step 1: Creating modified topology file"
echo "--------------------------------------------"

## GETTING CURRENT FORCEFIELD
current_ff_in_top=$(grep forcefield.itp "${top_file}" |  awk -F\/ '{print $1}' | awk -F\" '{print $NF}')

## PRINTING
echo "Current forcefield: ${current_ff_in_top}"

## LOOPING
for i in "${!ff_array[@]}"; do 
	## FORCE FIELD FILE
	ff_folder=${ff_array[$i]}
	ff_prefix=${ff_prefix_array[$i]}

	## PRINTING
	echo "Checking forcefield: ${ff_folder} [${ff_prefix}]"

	## SEEING IF EXISTENCE
	if [ ! -e "${ff_folder}" ]; then
		echo "Copying over force field: ${ff_folder}"
		cp -r "${path_to_forcefield}/${ff_folder}" "${main_sim_path}"
	fi

	## DEFINING TOP FILE
	current_ff_top="${output_prefix}_${ff_prefix}.top"

	## COPYING OVER
	cp -r "${top_file}" "${current_ff_top}"
	
	## EDITING
	sed -i "s#${current_ff_in_top}#${ff_folder}#g" "${current_ff_top}"

done

####################################
### STEP 2a: CREATING INDEX FILE ###
####################################

echo "--------------------------------------------"
echo "Step 2a: Create index file"
echo "--------------------------------------------"

if [[ "${analysis_folder_type}" == "np_water_sims" ]]; then

## CREATING INDEX FILE
if [[ ! -e "${output_index_file}" ]] || [[ "${rewrite}" == true ]]; then
"${gromacs_command}" make_ndx -f "${prod_prefix}.tpr" \
							  -o "${output_index_file}" << INPUTS
keep 0
q
INPUTS

fi

## CREATING INDEX FILE FOR TAIL GROUPS THAT ARE ABOVE A Z-CUTOFF
python3.6 "${PYTHON_GET_NPLM_INDEX}" --path_to_sim "${main_sim_path}" \
									 --gro_file "${prod_prefix}.gro" \
									 --index_file "${output_index_file}" \
									 --lm_residue_name "DOPC" \
									 --output_perm_file "${output_prefix}_perm.txt" \
									 --output_group_list "${output_prefix}_group.txt"

## DEFINING ENERGY GROUPS
energy_group_name="NP_ALK NP_NGRP NP_RGRP"

## DEFINING ARRAY OF ENERGY GROUPS OF INTEREST
declare -a energy_of_interest=("Total-Energy" \
							   "LJ-SR:NP_NGRP-NP_RGRP" \
							   "LJ-SR:NP_ALK-NP_NGRP" \
								)


elif [[ "${analysis_folder_type}" == "phe_tma_sims" ]]; then

"${gromacs_command}" make_ndx -f "${prod_prefix}.tpr" \
							  -o "${output_index_file}" << INPUTS
keep 0
r PHE
r TMA
q
INPUTS

## DEFINING ENERGY GROUPS
energy_group_name="PHE TMA"

## DEFINING ARRAY OF ENERGY GROUPS OF INTEREST
declare -a energy_of_interest=("Total-Energy" \
							   "LJ-SR:PHE-TMA" \
								)

fi

#######################################
### STEP 2b: CREATING ENERGY GROUPS ###
#######################################

echo "--------------------------------------------"
echo "Step 2b: Creating energy groups"
echo "--------------------------------------------"


## EDDINT ENERGY GROUPS
mdp_add_energy_groups "${mdp_file}" "${output_mdp_file}" "${energy_group_name}"

#####################################
### STEP 3: GENERATING TPR FILES ###
#####################################

echo "--------------------------------------------"
echo "Step 3: Creating tpr files for simulations"
echo "--------------------------------------------"

## STORING ARRAY
declare -a output_prefixes_array=()

## LOOPING AND CREATING TPRS
for i in "${!ff_array[@]}"; do 
	## DEFINING CURRENT FORCEFIELD PREFIX
	ff_prefix=${ff_prefix_array[$i]}

	## DEFINING TOP FILE
	current_prefix="${output_prefix}_${ff_prefix}"
	current_ff_top="${current_prefix}.top"
	current_ff_tpr="${current_prefix}.tpr"

	## CHECKING IF TPR EXISTS
	if [[ ! -e "${current_ff_tpr}" ]] || [[ "${rewrite_tpr}" == true ]]; then

		## PRINTING
		echo "Creating TPR file for: ${current_ff_tpr}"

		## CREATING TPR FILE FOR EACH TOPOLOGY FILE
		gmx grompp -f "${output_mdp_file}" \
				   -c "${prod_prefix}.gro" \
				   -p "${current_ff_top}" \
				   -o "${current_ff_tpr}" \
				   -n "${output_index_file}" 
		else
			echo "${current_ff_tpr} file exists!"
		fi

	## ADDING TO ARRAY
	output_prefixes_array+=("${current_prefix}")

done

wait
echo "TPR loading is complete!"



##################################
### STEP 4: CREATING RUN FILES ###
##################################

echo "--------------------------------------------"
echo "Step 4: Creating run files"
echo "--------------------------------------------"

## COPYING
cp -r "${path_input_run_file}" "${main_sim_path}/${output_run_file}"

## JOINING PREFIX TO STRING
output_prefixes_string=$(join_array_to_string " " "${output_prefixes_array[@]}")

## EDITING FILE
sed -i "s/_XTCFILE_/${prod_prefix}.xtc/g" "${output_run_file}"
sed -i "s#_PREFIXARRAY_#${output_prefixes_string}#g" "${output_run_file}"

## LOOPING THROUGH AND ADDING TO EACH LINE
for each_energy in "${energy_of_interest[@]}"; do
    sed -i "/_INPUTENERGYTYPES_/a ${each_energy}" "${output_run_file}"
done

# FINDING LINE NUMBER OF ENERGY TYPES
line_energy=$(grep -nE '_INPUTENERGYTYPES_' "${output_run_file}" | tail -1  | sed 's/\([0-9]*\).*/\1/')

## DELETING ENERGY LINE
sed -i ''${line_energy}'d' "${output_run_file}"

## CLEANING ANY EXTRA FILES
rm -f \#*

## ADDING RUN PATH
echo "${main_sim_path}/${output_run_file} ${num_cores} ${gromacs_command} ${rewrite}" >> "${path_job_list}"
echo "Adding to job list: ${path_job_list}"

done