#!/bin/bash

# center_AUNP_with_COM.sh
# The purpose fo this function is to center the gold core given a 
# NP-lipid membrane simulation. 

#################################################
### LOADING PRE-DEFINED VARIABLES / FUNCTIONS ###
#################################################
source "../../bin/nanoparticle_rc.sh"
print_script_name

## DEFINING SPECIFIC SIM
sim_dir="20200128-unbiased_ROT012"
specific_sim="NPLM_unb-1.300_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
specific_sim="NPLM_unb-1.900_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
specific_sim="NPLM_unb-2.100_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

## DEFINING INPUT SIMULATION
input_sim="/home/akchew/scratch/nanoparticle_project/simulations/${sim_dir}/${specific_sim}"

## GOING TO DIRECTORY
stop_if_does_not_exist "${input_sim}"
cd "${input_sim}"

## DEFINING PREFIX LIST
declare -a prefix_list=("nplm_equil" "nplm_prod")

## LOOPPING THROUGH PREFIX
for prefix in ${prefix_list[@]}; do

## COMPUTING DISTANCE
gmx distance -f "${prefix}.gro" \
             -s "${prefix}.tpr" \
             -oxyz "${prefix}_xyz.xvg" \
             -select "com of resname AUNP plus [0.000, 0.000, 0.000]" -nopbc >/dev/null 2>&1
             
## GETTING DISTANCE TO ORIGIN
read -a dist_origin <<< $(tail -n1 "${prefix}_xyz.xvg")

## GETTING GRO BOX SIZE
read -a box_size <<< $(gro_measure_box_size "${prefix}.gro")

## COMPUTING DISTANCES
x_trans=$(awk -v x_dim=${box_size[0]} \
              -v origin_x=${dist_origin[1]} \
              'BEGIN{ printf "%.3f", x_dim/2 + origin_x}')
y_trans=$(awk -v x_dim=${box_size[1]} \
              -v origin_x=${dist_origin[2]} \
              'BEGIN{ printf "%.3f", x_dim/2 + origin_x}')

## TRANSLATING
gmx trjconv -f "${prefix}.gro" \
            -s "${prefix}.tpr" -pbc mol \
            -trans "${x_trans}" "${y_trans}" "0" \
            -o "${prefix}_center.gro" << INPUTS
System      
INPUTS

## PRINTING
echo "Origin distance: ${dist_origin[@]}"
echo "Box size: ${box_size[@]}"
echo "Translation in X: ${x_trans}"
echo "Translation in Y: ${y_trans}"

## CLEANING ANY EXTRA DETAILS
rm -f \#*

done