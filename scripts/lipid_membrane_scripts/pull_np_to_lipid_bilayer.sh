#!/bin/bash

# pull_np_to_lipid_bilayer.sh
# The purpose of this function is to pull the nanoparticle to the lipid bilayer. We do this by using umbrella sampling tools that uses a pull simulation. The idea here is that we want a MDP file to run a quick pulling simulation. The goal is to pull the nanoparticle such that it is half the bilayer + radius of gyration distance.
# INPUTS:
#       - input gro file (assuming equilibrated gro file)
#       - input tpr file
#       - input directory
#       - input mdp file to work with
#       - pull rate
#       - output mdp file name
#       - pull residue 1 (static)
#       - pull residue 2 (this is used to move)

# OUTPUTS:
#       - index file
#       - updated MDP file

# -------------------------------------- # 
# VARIABLES:
#   $1: input path
#   $2: input_gro_file
#   $3: input_tpr_file

#   $4: pull_1_residue name (DOPC)
#   $5: pull_np_gold residue name (AUNP)
#   $6: pull_np_lig (e.g. RO1)

#   $7: input_pull_mdp
#   $8: output_pull_mdp

# -------------------------------------- # 

# Created on: 11/06/2018
# Written by: Alex K. Chew (alexkchew@gmail.com)

### FUNCTIONS ###

## INCLUDING IMPORTANT FUNCTIONS TO ENSURE THAT SCRIPT WORKS CORRECTLY
trap "exit 1" TERM
export TOP_PID=$$

### FUNCTION TO STOP THE SCRIPT IF A FILE DOES NOT EXIST
# The purpose of this script is to stop the script if a file does not exist
# INPUTS:
#       $1: FILE NAME
# OUTPUT:
#       Prints out whether your file exists
# USAGE: stop_if_does_not_exist file_name
function stop_if_does_not_exist () {
    ## DEFINING INPUTS
    file_name_="$1"
    
    ## CHECKING
    if [ ! -e ${file_name_} ]; then
        echo "Error!!! ${file_name_} does not exist. It is an essential part of the script. Please double-check your inputs!"
        echo "Pausing here for 5 seconds so you can see this message!" ; sleep 5
        echo "Killing script ...."; sleep 3
        kill -s TERM $TOP_PID
    else
        echo "${file_name_} exists, continuing script!" ; sleep 2
    fi
}

#######################
### INPUT VARIABLES ###
#######################

## DEFINING INPUT PATH
input_path="$1"

## DEFINING INPUT FILES
input_gro_file="$2" # "combined_correct_equil.gro"
input_tpr_file="$3" # "combined_correct_equil.tpr"

## DEFINING PULLING INFORMATION
pull_1_residue="$4" # "DOPC"
pull_np_gold="$5" # "AUNP"
pull_np_lig="$6" # "RO1"

## DEFINING INPUT MDP FILE
input_pull_mdp="$7" # "pulling.mdp"

## DEFINING OUTPUT MDP FILE
output_pull_mdp="$8" # "pulling_edited.mdp"

## DEFINING SPRING CONSTANT
spring_constant="$9"
# "7000"

## DEFINING PULL RATE
pull_rate="${10}"
# "0.001" # 0.001 # nm/ps

## DEFINING TIME STEP
time_step="${11}"
# "0.002" # ps (2 fs)

## DEFINING TEMPERATURE
temperature="${12}"
# "310.15" # K

## ASSUMING A HEIGHT FOR THE BILAYER -- MAY BE CHANGED LATER TO SUFFICE VARYING LIPID MEMBRANES
membrane_height="${13}"
# "1.86" # nm, found using gmx density looking only at P atoms, then finding the two peaks, taking the distance and dividing by two.
# e.g.:  
#   - gmx make_ndx -f combined_correct_equil.tpr -o test.ndx
#       --> input: r DOPC & a P
#   - gmx density -f combined_correct_equil.gro -s combined_correct_equil.tpr -n test.ndx -o density.xvg -d Z

########################
### OUTPUT VARIABLES ###
########################

## GOING TO WORKING DIRECTORY
cd "${input_path}"

# ---- COPY FILE MDP FILE ---- #
### DEFINING PATH TO MDP FILE
#mdp_path="/home/akchew/scratch/nanoparticle_project/prep_files/input_files/MDP_FILES/charmm36-nov2016.ff/lipid_bilayers"
#
### COPYING OVER MDP FILE ** MAY NOT BE NECESSARY IN THE FUTURE
#cp "${mdp_path}/${input_pull_mdp}" "${input_path}"

# ---- COPY FILE MDP FILE ---- #

## DEFINING SUMMARY FILE
summary_file="np_to_lipid_bilayer_pulling.info"

## DEFINING NP GYRATE XVG
gyrate_file="gyrate.xvg"

## DEFINING GENERAL POSITION XVG
pos_xvg_file="pos_xyz.xvg"

## DEFINING INDEX FILE
index_file="pull.ndx"
#   For this index file, we will need the following:
#       - lipid head group residue names
#       - all of gold nanoparticle (e.g. DOPC + AUNP)

## START BY GENERATING AN INDEX FILE
gmx make_ndx -f "${input_tpr_file}" -o "${index_file}" &> /dev/null << INPUTS
keep 0
r ${pull_1_residue}
r ${pull_np_gold}
r ${pull_np_gold} ${pull_np_lig}
q
INPUTS

## CHECKING IF INDEX FILE EXISTS
stop_if_does_not_exist "${index_file}"

####################################
### RADIUS OF GYRATION OF THE NP ###
####################################

# IMPORTANT NOTE: radius of gyration calculated with the assumption that index file has group 3 as AUNP + ligand.

## CALCULATING GYRATION USING GMX
gmx gyrate -f "${input_gro_file}" -s "${input_tpr_file}" -n "${index_file}" -o "${gyrate_file}" &> /dev/null << INPUTS
3
INPUTS

## EXTRACTION OF NP RADIUS OF GYRATION
np_gyration_radius=$(tail -n1 "${gyrate_file}" | awk '{print $2}')

## PRINTING
echo -e "\n---- CALCULATING NP RADIUS OF GYRATION ----"
echo "NP Radius: ${np_gyration_radius} nm"

## CHECKING IF RADIUS OF GYRATION WAS FOUND
stop_if_does_not_exist "${gyrate_file}"

#############################################
### DISTANCE BETWEEN NP AND LIPID BILAYER ###
#############################################

## CALCULATING DISTANCE BETWEEN LIPID BILAYER AND NANOPARTICLE (NO PBC)
gmx distance -f "${input_path}/${input_gro_file}" \
             -n "${input_path}/${index_file}" \
             -s "${input_path}/${input_tpr_file}" \
             -oxyz ${pos_xvg_file} \
             -select "com of group 1 plus com of group 2" -rmpbc yes &> /dev/null

## CHECKING IF DISTANCE WAS FOUND
stop_if_does_not_exist "${pos_xvg_file}"

## DEFINING CENTER OF MASS DISTANCE AND CREATING ARRAY
COM_dist=($(tail -n1 ${pos_xvg_file} | awk '{print$(NF-2),"\t",$(NF-1),$NF}'))

## DEFINING COM DISTANCE IN THE Z DIMENSION
COM_dist_z=${COM_dist[2]#-}

## GETTING FINAL DISTANCE
desired_distance=$(awk -v desired_height_off_bilayer=${desired_height_off_bilayer} \
                       -v monolayer_height=${membrane_height} \
                       'BEGIN{ printf "%.5f", np_radius + 0.5 * monolayer_height}')
                       # 20200109 - Changed to it's 1/2 monolayer height -- really embed NP to lipid membrane
                       
## CALCULATING PULL DISTANCE
pull_distance=$(awk -v full_dist=${COM_dist_z} \
                    -v desired_dist=${desired_distance} \
                    'BEGIN{ printf "%.5f", full_dist - desired_dist}')

## DEFINING MDP OPTIONS
total_time_pull_ps=$(awk -v rate=${pull_rate} \
                         -v desired_dist=${pull_distance} \
                         'BEGIN{ printf "%.5f", desired_dist / rate }') # Time in ps
## DEFINING TOTAL NUMBER OF PULL STEPS
total_time_pull_steps=$(awk -v total_time=${total_time_pull_ps} \
                            -v delta_t=${time_step} \
                            'BEGIN{ printf "%d", total_time / delta_t }')

########################
### EDITING MDP FILE ###
########################

sed "s/_DT_/${time_step}/g" "${input_pull_mdp}" > "${output_pull_mdp}"
sed -i "s/_GROUP_1_NAME_/${pull_np_gold}/g" "${output_pull_mdp}"
sed -i "s/_GROUP_2_NAME_/${pull_1_residue}/g" "${output_pull_mdp}"
sed -i "s/_NSTEPS_/${total_time_pull_steps}/g" "${output_pull_mdp}"
sed -i "s/_TEMPERATURE_/${temperature}/g" "${output_pull_mdp}"
sed -i "s/_PULLRATE_/${pull_rate}/g" "${output_pull_mdp}"
sed -i "s/_PULLKCONSTANT_/${spring_constant}/g" "${output_pull_mdp}"

## PRINTING
echo -e "\n---- CALCULATING DISTANCE BTWN LIPID BILAYER AND NP ----"
echo "Center of mass z-distance: ${COM_dist_z} nm"
echo "Desired distance btn COM of NP and lipid bilayer: ${desired_distance} nm"
echo "Total pull distance: ${pull_distance} nm"
echo "Total pull time: ${total_time_pull_ps} ps"
echo "Since time steps are ${time_step} ps, the total number of steps is: ${total_time_pull_steps}"
echo "Spring constant: ${spring_constant}"
echo "Exporting information to summary file: ${summary_file}"
echo "Current working directory: ${input_path}"

## PRINTING TO SUMMARY FILE
echo "---STATISTICS---" > ${summary_file}
echo "COM z dist btn NP and lipid membrane(nm): ${COM_dist_z}" >> ${summary_file}
echo "Height of lipid bilayer (nm): ${membrane_height}" >> ${summary_file}
echo "NP radius of gyration (nm): ${np_gyration_radius}" >> ${summary_file}
echo "Desired distance (nm): ${desired_distance}" >> ${summary_file}
echo "---PULLING INFORMATION---" >> ${summary_file}
echo "Total time of pulling required (ps): ${total_time_pull_ps}" >> ${summary_file}
echo "Time step (ps): ${time_step}" >> ${summary_file}
echo "Rate (nm/ps): ${pull_rate}" >> ${summary_file}
echo "Spring constant (Gromacs units): ${spring_constant}" >> ${summary_file}
echo "Total time based on rate: ${total_time_pull_ps}" >> ${summary_file}
echo "Total number of steps: ${total_time_pull_steps}" >> ${summary_file}
 