#!/bin/bash

# analysis_run_num_contacts.sh
# The purpose of this function is to run number of contacts code. 
# The number of contacts is between NP and lipid membrane systems.
# This function uses number of contacts code: extract_num_contacts.sh
# Written by: Alex K. Chew (01/24/2020)

#################################################
### LOADING PRE-DEFINED VARIABLES / FUNCTIONS ###
#################################################
source "../../bin/nanoparticle_rc.sh"
print_script_name

## DEFINING INPUTS
parent_path_sim="${PATH2SIM}"

## FOR FORWARD
parent_sim_dir="20200120-US-sims_NPLM_rerun_stampede"
# simulation_dir="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
simulation_dir="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

## FOR REVERSE
parent_sim_dir="20200124-US-sims_NPLM_reverse_stampede"
simulation_dir="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"

### FOR PUSHING SIMULATIONS FROM NP LIPID MEMBRANE
parent_sim_dir="20200123-Pulling_sims_from_US"
simulation_dir="pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT001_1"
#simulation_dir="pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"
#
### FOR PULLING SIMULATIONS
#parent_sim_dir="20200113-NPLM_PULLING"
#simulation_dir="NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

#### UNBIASED SIMULATIONS
#parent_sim_dir="20200128-unbiased_ROT012"
#simulation_dir="NPLM_unb-1.300_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
#simulation_dir="NPLM_unb-1.900_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
#simulation_dir="NPLM_unb-2.100_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

## ROT001
parent_sim_dir="20200205-unbiased_ROT001"
simulation_dir="NPLM_unb-1.900_2000-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"


## SEEING IF YOU WANT ONE SUBMISSION SCRIPT
want_one_submission=false
# true
# If true, then we will generate one submission script that could run all the commands

## DEFINING SIMULATION TYPE
if [[ "${simulation_dir}" == US* ]]; then
    ## DEFINING JOB TYPE
    job_type="US"
    
elif [[ "${simulation_dir}" == pullnplm* ]]; then
    job_type="PUSH"
elif [[ "${simulation_dir}" == NPLM_unb-* ]]; then
    job_type="UNBIASED"
elif [[ "${simulation_dir}" == NPLM* ]]; then
    job_type="PULL"
else
    echo "Error! Job type not defined for simulation directory: ${simulation_dir}"
    exit 1
fi

## DEFINING FULL SIMULATION PATH
full_sim_path="${parent_path_sim}/${parent_sim_dir}/${simulation_dir}"

## DEFINING PYTHON SCRIPT
python_script="${MDDESCRIPTOR}/application/np_lipid_bilayer/run_nplm_contacts_parallel.py"

## DEFINING EXTRACT NUM CONTACTS SCRIPT
bash_extract_script="${PATH_LIPID_MEMBRANE_SCRIPTS_EXTRACT}/extract_num_contacts.sh"

## DEFINING SUBMISSION SCRIPT
bash_submit_script="${PATH2SUBMISSION}/submit_run_analysis_num_contacts.sh"

##################################
### DEFINING DEFAULT VARIABLES ###
##################################
## CUTOFF FOR CONTACTS
cutoff="0.5" # nm

## DEFINING SIM DIR
if [[ "${job_type}" == "US" ]]; then
    within_sim_dir="4_simulations"
    ## PREFIX
    input_prefix="nplm_prod"
    ## DEFINING SKIP FRAMES
    skip_frame="10"
    ## DEFINING FIRST AND LAST FRAME
    first_frame="0"
    last_frame="50000"
    
elif [[ "${job_type}" == "PULL" ]] || [[ "${job_type}" == "PUSH" ]] || [[ "${job_type}" == "UNBIASED" ]]; then
    within_sim_dir=""
    ## PREFIX
    if [[ "${job_type}" == "PULL" ]]; then
        input_prefix="nplm_pull"    
    elif [[ "${job_type}" == "PUSH" ]]; then
        input_prefix="nplm_push"
    elif [[ "${job_type}" == "UNBIASED" ]]; then
        input_prefix="nplm_prod"
    fi
    ## DEFINING SKIP FRAMES
    skip_frame="1"
    ## DEFINING FIRST AND LAST FRAME
    first_frame="0"
    last_frame="-1"
fi

## DEFINING ANALYSIS DIRECTORY
analysis_dir="analysis"

## DEFINING SPECIFIC DIR
specific_analysis="num_contacts"

## DEFINING SELECTION
selection="non-Water"

## DEFINING NUMBER OF CORES
num_cores="28"

## DEFINING LOG FILE
log_file="output.log"

## MEMBRANE NAME
lm_membrane_name="DOPC" # Could replace with nomenclature later

## DEFINING REWRITE
rewrite=true
# "false"

####################
### MAIN SCRIPTS ###
####################

## WITHIN SIM
sim_path_within_sim="${full_sim_path}/${within_sim_dir}"

## DEFINING PATH TO STATUS INFO
path_status_info="${full_sim_path}/status.info"
echo "Total status information:"
echo ${path_status_info}

## OUTPUT SUBMIT FILE
output_submit="submit.sh"

## GOING INTO DIRECTORY
cd "${sim_path_within_sim}"

## GETTING LIST
if [[ "${job_type}" == "US" ]]; then
    read -a simulation_list <<< $(ls -d *)
else
    declare -a simulation_list=("/")
fi

## DEFINING RELATIVE SIMULATION PATH
relative_sim_path="../../"

if [[ "${want_one_submission}" == true ]]; then
    ## DEFINING SINGLE SUBMISSION SCRIPT
    path_submission_one_submit="${full_sim_path}/num_contacts_submit.sh"
    path_submission_one_run="${full_sim_path}/num_contacts_run.sh"
    ## CREATING NEW FILE
    > "${path_submission_one_run}"
    
fi

echo "${simulation_list[@]}"

## LOOPING FOR EACH SIMULATION
for specific_sim in ${simulation_list[@]}; do
    echo "Working on: ${specific_sim}"
    ## CHECKING FOR THE SPECIFIC SIMULATION
    if [[ -e "${path_status_info}" ]]; then
        ## EXTRACTING THE NUMBER OF SIMULATIONS
        total_traj_ps=$(grep "${specific_sim}" "${path_status_info}" | awk '{print $3}')
        echo "Total traj found: ${total_traj_ps}"
        ## CHECKING
        if [[ "${total_traj_ps}" == "100000.000" ]]; then
            first_frame="50000"
            last_frame="100000"
        else
            first_frame="0"
            last_frame="50000"
        fi
    fi

    ## DEFINING OUTPUT PREFIX
    output_prefix="${input_prefix}_skip_${skip_frame}_${selection}_${first_frame}_${last_frame}"

    ## DEFINING SPECIFIC DIRECTORY
    full_sim_path_with_sim="${sim_path_within_sim}/${specific_sim}"

    ## DEFINING JOB NAME
    job_name="num_contacts_${simulation_dir}_${specific_sim}"

    ## DEFINING PICKLE PATH
    path_to_analysis="${full_sim_path_with_sim}/${analysis_dir}/${specific_analysis}"

    ## STOPPING IF DOES NOT EXIST
    stop_if_does_not_exist "${full_sim_path_with_sim}"

    ############################
    ### CREATING BASH SCRIPT ###
    ############################
    mkdir -p "${path_to_analysis}"
    cp -r "${bash_extract_script}" "${path_to_analysis}" 

    ## EDITING BASH SCRIPT
    cd "${path_to_analysis}"

    ## DEFINING BASH SCRIPT NAME
    bash_extract_script_name=$(basename "${bash_extract_script}")

    sed -i "s#_SIMPATH_#${relative_sim_path}#g" "${bash_extract_script_name}"
    sed -i "s#_PYTHONSCRIPT_#${python_script}#g" "${bash_extract_script_name}"
    sed -i "s#_INPUT_PREFIX_#${input_prefix}#g" "${bash_extract_script_name}"
    sed -i "s#_OUTPUT_PREFIX_#${output_prefix}#g" "${bash_extract_script_name}"
    sed -i "s#_SKIPFRAME_#${skip_frame}#g" "${bash_extract_script_name}"
    sed -i "s#_FIRSTFRAME_#${first_frame}#g" "${bash_extract_script_name}"
    sed -i "s#_LASTFRAME_#${last_frame}#g" "${bash_extract_script_name}"
    sed -i "s#_SELECTION_#${selection}#g" "${bash_extract_script_name}"
    sed -i "s#_REWRITE_#${rewrite}#g" "${bash_extract_script_name}"
    sed -i "s#_NUMCORES_#${num_cores}#g" "${bash_extract_script_name}"
    sed -i "s#_LOGFILE_#${log_file}#g" "${bash_extract_script_name}"
    sed -i "s#_LM_MEMBRANE_NAME_#${lm_membrane_name}#g" "${bash_extract_script_name}"
    sed -i "s#_CUTOFF_#${cutoff}#g" "${bash_extract_script_name}"
    sed -i "s#_PATHPICKLE_#${path_to_analysis}#g" "${bash_extract_script_name}"

    ## COPYING SUBMISSION SCRIPT
    cp -r "${bash_submit_script}" "${path_to_analysis}/${output_submit}"
    ## EDITING SUBMISSION SCRIPT
    sed -i "s#_JOB_NAME_#${job_name}#g" "${output_submit}"
    sed -i "s#_RUNSCRIPT_#${bash_extract_script_name}#g" "${output_submit}"

    ### ADDING TO JOB LIST
    if [[ "${want_one_submission}" == true ]]; then
        ## ADDING TO SCRIPT
        echo "bash ${path_to_analysis}/${bash_extract_script_name}" >> "${path_submission_one_run}"
    else
        echo "${path_to_analysis}/${output_submit}" >> "${JOB_SCRIPT}"
    fi


done

## COPYING SUBMIT SCRIPT
if [[ "${want_one_submission}" == true ]]; then
    ## COPYING SUBMISSION SCRIPT
    cp -r "${bash_submit_script}" "${path_submission_one_submit}"
    ## DEFINING JOB NAME
    job_name="num_contacts_${simulation_dir}"
    ## EDITING SUBMISSION SCRIPT
    sed -i "s#_JOB_NAME_#${job_name}#g" "${path_submission_one_submit}"
    sed -i "s#_RUNSCRIPT_#$(basename ${path_submission_one_run})#g" "${path_submission_one_submit}"
    ## ADDING TO JOB LIST
    echo "${path_submission_one_submit}" >> "${JOB_SCRIPT}"

fi
