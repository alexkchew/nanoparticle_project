#!/bin/bash
# prep_energy_comparison_rerun_ff.sh
# In this script, we will generate energetics between two distinct forcefields and identify 
# whether or not the two force fields will have different results. 
#
# Written by: Alex K. Chew (08/12/2020)


## INPUTS:
#	- location for umbrella sampling simulations
#  	- MDP file production, which will be modified to get specific groups
#	- Submission / run file, which will perform the running

## OUTPUTS:
#	- EDR file for each forcefield parameterization

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name


###########################
### INITIALIZING INPUTS ###
###########################

## DEFINING PARENT FOLDER
parent_folder="20200615-US_PLUMED_rerun_with_10_spring"

## DEFINING SIM FOLDER
sim_folder="UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_300_0.35-50000_ns-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"
sim_folder="UShydrophobic_10-NPLMplumedhydrophobiccontactspulling-5.100_2_50_1000_0.35-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

## PERFORMING SAME RESULTS FOR Z=1.900 NM
parent_folder="20200613-US_otherligs_z-com"

## DEFINING SIM FOLDER
sim_folder="US-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"


# -------------

### FOR UPDATED FORCE FIELD PARAMETERS
parent_folder="20200818-Bn_US_modifiedFF"

## DEFINING SIM FOLDER
sim_folder="US-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"

## DEFINING KEY TYPE
key_type="z_com"

# -------------

## DEFINING MAIN SIMULATION PATH
main_sim_path="/home/akchew/scratch/nanoparticle_project/nplm_sims/${parent_folder}/${sim_folder}"

## DEFINING MAIN PREFIX
output_prefix="ffcomparison"

## DEFINING PREFIX
sim_prefix="nplm"
prod_prefix="${sim_prefix}_prod" # Production run prefix

## DEFINING INPUT MDP
if [[ "${key_type}" == "z_com" ]]; then
	mdp_file="umbrella_sampling_prod.mdp"
else
	mdp_file="plumed_us_prod.mdp"
fi

## DEFININT OUTPUT MDP FILE
output_mdp_file="${output_prefix}.mdp"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING DEFAULT LIST
job_list="energy_comparison_job_list.txt"
path_job_list="${main_dir}/${job_list}"

## CREATING FILE
> "${path_job_list}"

## TOPOLOGY FILE
top_file="${sim_prefix}.top"

## INDEX FILE
if [[ "${key_type}" == "z_com" ]]; then
	index_file="plumed_analysis_index.ndx"
else
	index_file="${sim_prefix}.ndx"
fi
## DEFINING SUFFIX
modified_suffix="mod"

## MODIFIED TOP
mod_top_file="${top_file%.top}_${modified_suffix}.top"

## RUN SCRIPT
input_run_file="run_energy_comparison_rerun_ff.sh"
output_run_file="${output_prefix}_run.sh"

## DEFINING PATH TO INPUT
path_input_run_file="${main_dir}/${input_run_file}"

## DEFINING IF YOU WANT TO REWRITE TPR
rewrite_tpr=true
# false
# true
# "false"

## DEFINING GROMACS
gromacs_command="gmx"
num_cores="15"
# "10"
rewrite="true"

######################
### DEFINING PATHS ###
######################

## PATH TO FORCE FIELD
path_to_ff="${main_sim_path}/1_input_files"

## DEFINING INPUT FORCE FIELD
input_forcefield="charmm36-jul2017.ff"

## DEFINING MODIFIED FORCEFIELD
modified_forcefield="charmm36-jul2017-mod.ff"

## DEFAULT NBFIX
nbfix_file="nbfix.itp"

###################
### MAIN SCRIPT ###
###################

## CHECKING EXISTANCE
stop_if_does_not_exist "${main_sim_path}"
stop_if_does_not_exist "${path_to_ff}"

# #####################################################
# ### STEP 1: GENERATING NEW FORCE FIELD PARAMETERS ###
# #####################################################
# echo "--------------------------------------------"
# echo "Step 1: Generating new force field parameters"
# echo "--------------------------------------------"

# ## CHECKING IF FF FILE EXISTS
# if [ ! -e "${path_to_ff}/${modified_forcefield}" ]; then

# 	## RUNNING PYTHON
# 	python3.6 "${PYTHON_MODIFY_FF}" --main_path "${path_to_ff}" \
# 								    --input_ff_folder "${input_forcefield}" \
# 								    --output_ff_folder "${modified_forcefield}" \
# 								    --ff_nbfix "${nbfix_file}"

# else
# 	echo "Modified force field exists! Skipping step for generating new force fields"
# 	echo "Path: ${path_to_ff}/${modified_forcefield}"
# fi


#####################################
### STEP 2: EDITING TOPOLOGY FILE ###
#####################################
echo "--------------------------------------------"
echo "Step 2: Creating modified topology file"
echo "--------------------------------------------"

## GOING TO FOLDER
cd "${path_to_ff}"

## DEFINING PATH TO FORCEFIELD
path_to_forcefield="${PATH_FORCEFIELD}"

## DECLARING FF ARRAY
declare -a ff_array=("${input_forcefield}" "${modified_forcefield}")
declare -a ff_prefix_array=("orig" "mod")

## GETTING CURRENT FORCEFIELD
current_ff_in_top=$(grep forcefield.itp "${top_file}" |  awk -F\/ '{print $1}' | awk -F\" '{print $NF}')

## PRINTING
echo "Current forcefield: ${current_ff_in_top}"

## DECLARING TOP
declare -a top_files=()

## LOOPING
for i in "${!ff_array[@]}"; do 
	## FORCE FIELD FILE
	ff_folder=${ff_array[$i]}
	ff_prefix=${ff_prefix_array[$i]}

	## PRINTING
	echo "Checking forcefield: ${ff_folder} [${ff_prefix}]"

	## SEEING IF EXISTENCE
	if [ ! -e "${ff_folder}" ]; then
		echo "Copying over force field: ${ff_folder}"
		cp -r "${path_to_forcefield}/${ff_folder}" "${path_to_ff}"
	fi

	## DEFINING TOP FILE
	current_ff_top="${output_prefix}_${ff_prefix}.top"

	## COPYING OVER
	cp -r "${top_file}" "${current_ff_top}"
	
	## EDITING
	sed -i "s#${current_ff_in_top}#${ff_folder}#g" "${current_ff_top}"

	## ADDING TOP FILE
	top_files+=("${current_ff_top}")

done

echo "Top files"
echo "${top_files[@]}"

sleep 2
# ## COPYING OVER
# echo "Copying over ${top_file} -> ${mod_top_file}"
# cp -r "${top_file}" "${mod_top_file}"

# ## EDITING
# sed -i "s#${input_forcefield}#${modified_forcefield}#g" "${mod_top_file}"

########################################
### STEP 3A: INCLUDING ENERGY GROUPS ###
########################################

echo "--------------------------------------------"
echo "Step 3A: Creating energy groups"
echo "--------------------------------------------"

## READING SIMULATIONS
read -a sim_path_list <<< $(ls -d "${main_sim_path}/4_simulations/"* | sort -V)
declare -a sim_path_list=(\
						  # "${main_sim_path}/4_simulations/150.0" \
						  # "${main_sim_path}/4_simulations/0.0" \
						  # "${main_sim_path}/4_simulations/1.900" \
						  # "${main_sim_path}/4_simulations/5.100" \
						  "${main_sim_path}/4_simulations/1.900" \
						  )

## LOOPING THROUGH
for current_sim_path in "${sim_path_list[@]}"; do

	## CHECKING FOR EXISTANCE
	stop_if_does_not_exist "${current_sim_path}"

	## GO TO CURRENT SIM PATH
	cd "${current_sim_path}"

	## DEFINING ENERGY GROUP NAMES
	if [[ "${key_type}" == "z_com" ]]; then
		energy_group_name="NP_RGRP NP_NGRP LM_HEADGRPS"
		# NP_ALK_RGRP
	else
		energy_group_name="NP_ALK_RGRP NP_NGRP HEADGRPS"
	fi
	# NP_LIGANDS

	## EDITING MDP FILE TO INCLUDE ENERGY GROUPS (Assuming no energy groups were included)
	mdp_add_energy_groups "${mdp_file}" "${output_mdp_file}" "${energy_group_name}"

	## EDITING MDP FILE IF PULLING Z
	if [[ "${key_type}" == "z_com" ]]; then
		sed -i "s#AUNP#NP_GOLD#g" "${output_mdp_file}"
		sed -i "s#DOPC#LM_TAILGRPS#g" "${output_mdp_file}"
	fi


	#####################################################################################
	### CHECKING IF INDEX FILE EXISTS. IF NOT, WE WILL CREATE A COMPARISON INDEX FILE ###
	#####################################################################################
	## DEFINING TEMPORARY INDEX FILE
	current_index_file="${output_prefix}.ndx"
	if [[ ! -e "${index_file}" ]] && [[ -e "${current_index_file}" ]]; then
		index_file="${current_index_file}"
	fi

	## CHECKING INDEX FILE
	if [[ ! -e "${index_file}" ]]; then

gmx make_ndx -f "${prod_prefix}.tpr" \
			  -o "${current_index_file}" << INPUTS
keep 0
q
INPUTS

		## CREATING INDEX FILE FOR TAIL GROUPS THAT ARE ABOVE A Z-CUTOFF
		python3.6 "${PYTHON_GET_NPLM_INDEX}" --path_to_sim "${current_sim_path}" \
											 --gro_file "${prod_prefix}.gro" \
											 --index_file "${current_index_file}" \
											 --lm_residue_name "DOPC" \
											 --output_perm_file "${output_prefix}_perm.txt" \
											 --output_group_list "${output_prefix}_groups.txt"
		## RELABELING INDEX FILE
		index_file="${current_index_file}"

	fi


	#####################################
	### STEP 3B: EXTRACTING TPR FILES ###
	#####################################

	echo "--------------------------------------------"
	echo "Step 3B: Creating tpr files for simulations"
	echo "--------------------------------------------"

	## DECLARING ARRAYS
	# declare -a top_files=("${top_file}" "${mod_top_file}")
	declare -a output_prefixes=("${output_prefix}_orig" "${output_prefix}_${modified_suffix}")

	## JOINING PREFIX TO STRING
	output_prefixes_string=$(join_array_to_string " " "${output_prefixes[@]}")

	## LOOPING TO CREATE TPR FILE
	for each_index in $(seq 0 $((${#top_files[@]}-1)) ); do

		## GETTING TOP FILE
		current_top_file="${top_files[each_index]}"
		current_output_prefix="${output_prefixes[each_index]}"

		if [[ ! -e "${current_output_prefix}.tpr" ]] || [[ "${rewrite_tpr}" == true ]]; then

		## PRINTING
		echo "Creating TPR file for: ${current_output_prefix}"

		## CREATING TPR FILE FOR EACH TOPOLOGY FILE
		gmx grompp -f "${output_mdp_file}" \
				   -c "${prod_prefix}.gro" \
				   -p "${path_to_ff}/${current_top_file}" \
				   -o "${current_output_prefix}.tpr" \
				   -n "${index_file}" 
		else
			echo "${current_output_prefix} tpr file exists!"
		fi
	done
	#  >/dev/null 2>&1 &
	## WAITING FOR ALL PROCESSES TO COMPLETE
	wait
	echo "TPR loading is complete!"

	##################################
	### STEP 4: CREATING RUN FILES ###
	##################################

	echo "--------------------------------------------"
	echo "Step 4: Creating run files"
	echo "--------------------------------------------"

	if [[ "${key_type}" == "z_com" ]]; then
		## DEFINING ARRAY OF ENERGY GROUPS OF INTEREST
		declare -a energy_of_interest=("Total-Energy" \
									   "LJ-SR:LM_HEADGRPS-LM_HEADGRPS" \
									   "LJ-SR:NP_RGRP-NP_NGRP" \
									   "LJ-SR:NP_RGRP-LM_HEADGRPS" \
									   "LJ-SR:NP_NGRP-NP_NGRP" \
									   "LJ-SR:NP_NGRP-LM_HEADGRPS" \
									   "LJ-SR:NP_RGRP-NP_RGRP" \
										)
	else
		## DEFINING ARRAY OF ENERGY GROUPS OF INTEREST
		declare -a energy_of_interest=("Total-Energy" \
									   "LJ-SR:HEADGRPS-HEADGRPS" \
									   "LJ-SR:NP_ALK_RGRP-NP_NGRP" \
									   "LJ-SR:NP_ALK_RGRP-HEADGRPS" \
									   "LJ-SR:NP_NGRP-NP_NGRP" \
									   "LJ-SR:NP_NGRP-HEADGRPS" \
									   "LJ-SR:NP_ALK_RGRP-NP_ALK_RGRP" \
										)
	fi

	## COPYING
	cp -r "${path_input_run_file}" "${current_sim_path}/${output_run_file}"

	## EDITING FILE
	sed -i "s/_XTCFILE_/${prod_prefix}.xtc/g" "${output_run_file}"
	sed -i "s#_PREFIXARRAY_#${output_prefixes_string}#g" "${output_run_file}"

	## LOOPING THROUGH AND ADDING TO EACH LINE
	for each_energy in "${energy_of_interest[@]}"; do
	    sed -i "/_INPUTENERGYTYPES_/a ${each_energy}" "${output_run_file}"
	done

	# FINDING LINE NUMBER OF ENERGY TYPES
	line_energy=$(grep -nE '_INPUTENERGYTYPES_' "${output_run_file}" | tail -1  | sed 's/\([0-9]*\).*/\1/')

	## DELETING ENERGY LINE
	sed -i ''${line_energy}'d' "${output_run_file}"

	## CLEANING ANY EXTRA FILES
	rm -f \#*

	## ADDING RUN PATH
	echo "${current_sim_path}/${output_run_file} ${num_cores} ${gromacs_command} ${rewrite}" >> "${path_job_list}"
	echo "Adding to job list: ${path_job_list}"

done


## GENERATING EDR FILE
# gmx mdrun -v -nt 1 -s ffcomparison_orig.tpr -rerun nplm_prod.xtc -e ffcomparison_orig.edr
# gmx mdrun -v -nt 1 -s ffcomparison_mod.tpr -rerun nplm_prod.xtc -e ffcomparison_mod.edr

## OUTPUTTING ENERGY COMPARISONS
# gmx energy -s ffcomparison_orig.tpr -f ffcomparison_orig.edr  -o ffcomparison_orig.xvg
# gmx energy -s ffcomparison_mod.tpr -f ffcomparison_mod.edr  -o ffcomparison_mod.xvg