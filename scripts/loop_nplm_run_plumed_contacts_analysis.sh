#!/bin/bash

# loop_nplm_run_plumed_contacts_analysis.sh
# This script simply looks through the nplm jobs and generates a submission script
# which could simply run the analysis across all jobs.
# 
# Written by: Alex K. Chew (05/18/2020)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

## DEFINING SCRIPT
bash_script="nplm_run_plumed_contacts_analysis.sh"

## CREATING DIRECTORY
analysis_dir="plumed_contacts_analysis"
if [ ! -e "${analysis_dir}" ]; then
	mkdir "${analysis_dir}"
fi

## DEFINING REWRITE
rewrite="false"
# "false"
# "true"

## DEFINING LIST OF JOBS
job_list="sims_list.txt"

## DEFINING PATH TO JOB LIST
path_job_list="${analysis_dir}/${job_list}"

## DEFINING INPUT PREFIX
input_prefix="nplm_prod"

## DEFINING SUMMARY
summary="plumed_analysis.summary"

## USING PYTHON TO GENERATE LIST OF SIMS
python3.6 "${PYTHON_NPLM_GET_SIM_LIST}" --output_path "${path_job_list}"

## DEFINING N BATCHS
N=5
# 5

## LOOPING THROUGH THE LIST AND RUNNING PLUMED ANALYSIS
while IFS= read -r each_line; do
	## DEFINING EACH SIM DIRECTORY
	each_sim_dir=$(awk '{print $1}' <<< ${each_line})
	input_prefix=$(awk '{print $2}' <<< ${each_line})
	ref_gro=$(awk '{print $3}' <<< ${each_line})
	echo "Each line: ${each_line}"
	echo "INPUT PREFIX: ${input_prefix}"
   	## RUNNING IN BATCHES
    ((i=i%N)); ((i++==0)) && wait
	## RUNNING CODE
	bash "${PATH2SCRIPTS}/${bash_script}" "${each_sim_dir}" \
						  				  "${input_prefix}" \
						  				  "${ref_gro}" \
						  				  "${rewrite}" &


done < "${path_job_list}"

## WAITING FOR COMPLETION
wait


