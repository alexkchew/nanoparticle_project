#!/bin/bash

# full_prep_most_likely_np_with_mixed_solvents.sh
# This script runs prep_most_likely_np_with_mixed_solvents for multiple solvent systems. 

## TODO: NEED TO UPDATE SCRIPT ***

# Written by Alex K. Chew (09/09/2019)

## USAGE:
#   bash full_prep_most_likely_np_with_mixed_solvents.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"


### DEFINING PATH TO SCRIPT
SCRIPT_PATH="$PATH2SCRIPTS/prep_most_likely_np_with_mixed_solvents.sh"

###########################################
### USER-DEFINED VARIABLES 
###########################################

## DEFINING SIMULATION TO GET DETAILS FROM
simulation_dir_name="190509-2nm_C11_Sims_updated_forcefield"

## DEFINING NEW SIMULATION DIRECTORY NAME
output_simulation_dir="190916-mixed_solvents_multiple_frames"

## DEFINING LIGAND NAMES
declare -a ligand_names=("C11COOH" "dodecanethiol") 
# "NONE"
# "C11COOH" "dodecanethiol"
#  NONE
#  C11COOH
#  dodecanethiol

## DEFINING SOLVENTS
declare -a solvent_names=("methanol") #  
# "formicacid" "formamide" "dimethylsulfide" "phenol" "toluene" "indole" "methanethiol"
# "methanol"

## DEFINING MASS FRACTION
declare -a cosolvent_mass_fracs=("10") # "50" 90  "90" "50"
# 50
## DEFINING DESIRED COSOLVENT  FRAME
declare -a cosolvent_frame=("9500" "9000" "8500") #   # in PS
# "10000" "9500" "9000" "8500"
# "10000" 

## DEFINING BOX LENGTH SIZE
declare -a box_length_sizes=("7")

###########################################

## LOOPING THROUGH EACH SOLVENT
for each_ligand in "${ligand_names[@]}"; do
    ## LOOPING THROUGH EACH MASS FRACTION
    for each_cosolvent in "${solvent_names[@]}"; do
        ## LOOPING THROUGH EACH BOX SIZE
        for each_mass_frac in "${cosolvent_mass_fracs[@]}"; do
            ## LOOPING THROUGH EACH FRAME
            for each_frame in "${cosolvent_frame[@]}"; do
                ## PRINTING
                echo "RUNNING THE FOLLOWING COMMAND:"
                ## LOOPING THROUGH NO LIGANDS
                if [[ "${each_ligand}" == "NONE" ]]; then
                    ## LOOPING THROUGH BOX LENGTH SIZES
                    for initial_box_length in "${box_length_sizes[@]}"; do
                        echo "bash "${SCRIPT_PATH}" "${each_solvent}" "${each_mass_frac}" "${each_box_size}" "${each_frame}" "${initial_box_length}""
                        ## RUNNING
                        bash "${SCRIPT_PATH}" "${simulation_dir_name}" "${output_simulation_dir}" "${each_ligand}" "${each_cosolvent}" "${each_mass_frac}" "${each_frame}" "${initial_box_length}"
                    done
                
                else

                    echo "bash "${SCRIPT_PATH}" "${each_solvent}" "${each_mass_frac}" "${each_box_size}" "${each_frame}""
                    ## RUNNING
                    bash "${SCRIPT_PATH}" "${simulation_dir_name}" "${output_simulation_dir}" "${each_ligand}" "${each_cosolvent}" "${each_mass_frac}" "${each_frame}"
                fi
                
            done
        done
    done
done

### PRINTING SUMMARY
echo "------SUMMARY------"
echo "Ligand names: ${ligand_names[@]}"
echo "Cosolvent frames: ${cosolvent_frame[@]}"
echo "Created systems for solvents: ${solvent_names[@]}"
echo "Cosolvent mass fractions: ${cosolvent_mass_fracs[@]}"