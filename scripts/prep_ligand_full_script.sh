#!/bin/bash

# prep_ligand_full_script.sh
# The purpose of this script is to take a new ligand itp, prm, etc. file and run the following algorithm:
#   - energy minimization of the ligands
#   - fragment builder to create fragment
#   - create a ligand file for that particular fragment
#   - update the ligand list (if not already there!)

## USAGE EXAMPLE: bash prep_ligand_full_script.sh butanethiol BUT

# Created on: 04/18/2018
# Written by: Alex K. Chew (alexkchew@gmail.com)

## ASSUMPTIONS:
#   We assume you are using the CHARMM36 force field and generated the itp, pdb, and prm file from the CGENFF software
#   We also assume that you want the entire molecule as a fragment. This is highly useful in terms of ensuring that your force field is parameterized correctly.
## NOTE: We assume that you have downloaded your itp, prm, and _ini.pdb file within the Main_dir > Prep_System > prep_ligand > input_files > molecules. Afterwards, this script should take care of everything else.

### LOADING PRE-DEFINED VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"
echo -e "\n----- prep_energy_full_script.sh ----- \n"

#######################
### INPUT VARIABLES ###
#######################

## DEFINING MOLECULE NAME
molecule_name="$1"

## DEFINING RESIDUE NAME
# NOTE: if "", we will just continue by finding a residue name
desired_residue_name="$2"

## DEFINING BASH SCRIPTS
bs_prep_ligand_em="${PATH2SCRIPTS}/prep_ligand_energy_min.sh"

## DEFINING PYTHON SRIPTS
py_prep_ligand="${MDBUILDER}/ligand/combine_head_group.py"

## DEFINING FORCE FIELD
forcefield="charmm36-nov2016.ff"

########################
### OUTPUT VARIABLES ###
########################

## DEFINING OUTPUT LIGANDS
output_lig_gro="${molecule_name}.gro"
output_lig_itp="${molecule_name}.itp"
output_lig_dir="${PREP_LIGAND_FINAL}/${molecule_name}"

## DEFINING OUTPUT FOR FRAGMENTS
if [ "${forcefield}" == "charmm36-nov2016.ff" ]; then
    output_prm_name="${molecule_name}_CHARMM36.prm"
else
    echo "Error! ${forcefield} not found in list of possbile force fields. Please check your force field parameters! Stopping here!"
    exit
fi

###################
### MAIN SCRIPT ###
###################

### DEFINING INPUT FILES
input_path="${INPUT_LIG_DIR}/${molecule_name}" # Path to input files

## DEFINING NAMES BASED ON INPUT
input_molecule_pdb_file="${molecule_name}.pdb"
input_molecule_itp_file="${molecule_name}.itp"
input_molecule_prm_file="${molecule_name}.prm"

## GETTING PATHS
input_itp_file_path="${input_path}/${input_molecule_itp_file}" # itp file
input_pdb_file_path="${input_path}/${input_molecule_pdb_file}" # pdb file
input_prm_file_path="${input_path}/${input_molecule_prm_file}" # parameter file

## DEFINING OUTPUT DIRECTORY
output_em_dir="${PREP_LIGAND_SIM_DIR}/${molecule_name}"

## FINDING RESIDUE NAME
residue_name="$(extract_itp_resname "${input_itp_file_path}")"

if [ "${desired_residue_name}" != "${residue_name}" ] && [ ! -z "${desired_residue_name}" ]; then
    echo "Since current residue name, ${residue_name}, does not match desired residue name, ${desired_residue_name}"
    echo "Fixing residue names of the original files!"
    ## FIXING ITP FILE
    sed -i "s/${residue_name}/${desired_residue_name}/g" ${input_itp_file_path}
    ## FIXING PDB FILE
    ## UPDATE THE RESIDUE NAME -- PDBs cannot have > 4 letter names!
    if [ "${#residue_name}" -gt "4" ]; then
        pdb_change_res_name=$(printf ${residue_name} | cut -c 1-4)
    else ## NO CHANGE
        pdb_change_res_name=${residue_name}
    fi
    sed -i "s/${pdb_change_res_name}/${desired_residue_name}/g" ${input_pdb_file_path}
else
    desired_residue_name=${residue_name}
fi

## COPYING NEW RESIDUE NAME
residue_name=${desired_residue_name}
    
echo "WORKING ON: ${molecule_name}, residue name: ${residue_name}"
echo "ITP FILE: ${input_itp_file_path}"
echo "PDB FILE: ${input_pdb_file_path}"
echo "PRM FILE: ${input_prm_file_path}"

## CHECKING MOLECULE AND RESIDUE NAMES BASED ON CURRENT WORK
readarray -t lig_molecule_name <<< "$(grep -v -e '^$' "${PREP_LIG_FILE}" | grep -v -e '^;'| awk -F', ' '{print $1}')"
readarray -t lig_residue_names <<< "$(grep -v -e '^$' "${PREP_LIG_FILE}" | grep -v -e '^;'| awk -F', ' '{print $2}')"

## SEEING IF WE HAVE DUPLICATES OF MOLECULE NAME, ETC.
if [[ " ${lig_residue_names[@]} " =~ " ${residue_name} " ]] || [[ " ${lig_molecule_name[@]} " =~ " ${molecule_name} " ]]; then
    echo "${molecule_name} may be a duplicate according to ${PREP_LIG_FILE}. Consider double-checking if the residue name or the molecule name is duplicated!"
    echo "Pausing for 5 seconds ... Cancel if necessary with CTRL + C"
    sleep 5
fi

## RUNNING ENERGY MINIMIZATION
bash "${bs_prep_ligand_em}" "${molecule_name}" "${residue_name}" "${input_itp_file_path}" "${input_pdb_file_path}" "${input_prm_file_path}" "${output_em_dir}" "${forcefield}"

## CREATING OUTPUT DIRECTORY FOR FINAL STRUCTURE
mkdir -p "${output_lig_dir}"

## RUNNING PREPARATION LIGAND
python3 "${py_prep_ligand}" --idir "${output_em_dir}" --igro "${molecule_name}.gro" --iitp "${molecule_name}.itp" --odir "${output_lig_dir}" --ogro "${output_lig_gro}" --oitp "${output_lig_itp}"

## EDITING GRO FILE PBC
gmx editconf -f "${output_lig_dir}/${output_lig_gro}" -o "${output_lig_dir}/${output_lig_gro}" -d 0

## CLEANING UP ANY DUPLICATES
rm ${output_lig_dir}/\#*

## COPYING PARAMETER FILE
echo "*** COPYING PARAMETER FILE ***"
cp -rv "${output_em_dir}/${molecule_name}.prm" "${output_lig_dir}/${output_prm_name}"

## CHECKING IF COMPLETION
if [ ! -e "${output_lig_dir}/${output_lig_gro}" ] || [ ! -e "${output_lig_dir}/${output_lig_itp}" ] || [ ! -e "${output_lig_dir}/${output_prm_name}" ]; then
    echo "ERROR! We are missing either gro, itp, prm file."
    echo "Please check ${output_lig_dir}"
    echo "You may be missing some inputs!"
    echo "Stopping here to prevent subsequent errors!"
    exit
fi

## RUNNING REFRESHING SYSTEM FOR LIGAND NAMES
prep_refresh_dir=$(dirname ${PREP_LIG_REFRESH_FILE})
## GOING AND RUNNING
cd "${prep_refresh_dir}"; bash "${PREP_LIG_REFRESH_FILE}"

## PRINTING SUMMARY
echo -e "\n********** SUMMARY **********"
echo "CREATED LIGANDS FOR: ${molecule_name}"
echo "RESIDUE NAME: ${residue_name}"
echo "Good luck in the simulations!"
