#!/bin/bash

# transfer_ligand_builder.sh
# This script transfers the self-assembly gold using new ligands with ligand builder

# Written by Alex K. Chew (12/24/2017)

### VARIABLES:
# $1: diameter
# $2: temperature
# $3: shape_type (i.e. what shape would you like?)
# $4: ligand_names: name of the ligands
# $5: trial number
# $6: simulation directory
# $XXX: ligand_frac: fraction of ligands (DEFAULT 1 FOR NOW)

## USAGE: 
#   bash transfer_ligand_builder.sh 2 310.15 hollow butanethiol 1 test_dir

## NOTE: If you are having LINCS warning -- you can update the transfer_ligand.py script and change the transition to half boxlength to outside the box. This serves as a good way of testing your NP bonding. So far, 50,000 does not have errors.

# ** UPDATES **
#   180223 - Added binding gold nanoparticles to sulfur atom script
#   180508 - Added directory functionality

### SOURCING ALL VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

# Getting script name and echoing
print_script_name

### --- USER-SPECIFIC PARAMETERS (BEGIN) --- ###

### INPUT VARIABLES

## DIAMETER
diameter="$1" # 2 nm

## TEMPERATURE
temp="$2" # "310.15" temperature in K of the system

## DEFINING SHAPE
shape_type="$3"
    # spherical: Spherical shape cutoff from {111} fcc lattice
    # hollow: Hollow spherical shape
    # EAM: embedded atom potential

## DEFINING DESIRED TRIAL
trial_num="$5"

## LIGAND PARAMETERS
ligand_names="$4" # "butanethiol" # name of ligand from .lig file

## DEFINING SIMULATION DIRECTORY
simulation_dir="$6"
ligand_fracs="${7-1}" # fraction of ligands

## DEFINING IF YOU WANT MODIFIED FORCEFIELD
modified_ff="${8-false}"
# If true, you will use modified force field that has new benzene parameters

## CALCULATING RADIUS
radius=$(awk -v diam=$diameter 'BEGIN{ printf "%.5f",diam/2}') # nms
dist_to_edge="1" # nm distance from edge of box
# dist_to_edge="1.5" # TESTING, CHANGE BACK TO 1 LATER
# dist_to_edge="2" # nm distance from edge of box <-- TEMP, TESTING

## FORCE FIELD
forcefield="${FORCEFIELD}" # Force Field (opls-aa or charmm36-nov2016.ff)

## IF MODIFIED FF IS TRUE
if [[ "${modified_ff}" == true ]]; then
    forcefield="${MODIFIED_FF}"
    echo "Using modified force field!"
    ## CHECKING IF FILE EXISTS
    if [[ ! -e "${PATH_FORCEFIELD}/${forcefield}" ]]; then
        echo ""
    ## RUNNING PYTHON
    python3.6 "${PYTHON_MODIFY_FF}" --main_path "${PATH_FORCEFIELD}" \
                                    --input_ff_folder "${FORCEFIELD}" \
                                    --output_ff_folder "${forcefield}" \
                                    --ff_nbfix "${NBFIX_FILE}"
    else
        echo "Modified force field exists! Skipping step for generating new force fields"
        echo "Path: ${PATH_FORCEFIELD}/${forcefield}"
    fi
else
    echo "Using original force field"

fi


#########################
### DEFAULT VARIABLES ###
#########################

## NUMBER OF CORES
num_cores="28"

## PREPARATION GOLD
prep_gold_folder="${PREP_GOLD_SIM}"
input_gold_folder="${shape_type}/${shape_type}_${diameter}_nmDIAM_300_K_2_nmEDGE_5_AREA-PER-LIG_4_nm_300_K_butanethiol_Trial_${trial_num}"
input_gold_gro="gold_ligand_equil.gro"

## PYTHON SCRIPT
transfer_python_script="transfer_ligand_builder.py"
bind_gold_sulfur_script="bind_nanoparticles.py"
py_transfer_script="${MDBUILDER}/builder/${transfer_python_script}"
py_bind_gold_sulfur_script="${MDBUILDER}/applications/nanoparticle/${bind_gold_sulfur_script}"

## REMOVING SCRIPT
py_remove_atoms_file="${MDBUILDER}/math/remove_atoms.py" # Removes residues within the center core (for hollow shell geometry)

########################
### FILE DIRECTORIES ###
########################

## MDP FILES
mdp_em="minim_sam_gmx5.mdp" # Energy minimization mdp script
mdp_equil="npt_double_equil_gmx5_charmm36.mdp"
mdp_prod="npt_double_prod_gmx5_charmm36_10ps.mdp" # production 10 ps script
# "npt_double_prod_gmx5_charmm36.mdp" # Production mdp script
mdp_dir="${INPUT_DIR}/MDP_FILES/charmm36"


## CHECKING MDP FOLDER
if [ "${shape_type}" == "spherical" ] || [ "${shape_type}" == "hollow" ] || [ "${shape_type}" == "EAM" ]; then
    mdp_folder="Spherical"
fi
path2mdp="${mdp_dir}/${mdp_folder}"

## DEFINING NUMBER OF CORES
num_cores="28"

## GROMPP OPTIONS
max_warn_index="5" # Number of times for maximum warnings

## JOB SCRIPT FILES
submitScript="submit_transfer_ligand_builder.sh"
output_submit="submit.sh" # output submit script
# "submit.sh"

### SIMULATION PARAMETERS
## NUMBER OF STEPS FOR EQUILIBRATION AND PRODUCTION -- # ps / .002 timesteps
n_steps_equil="2500000" # 5 ns equilibration
n_steps_prod="25000000" # 50 ns production
# n_steps_prod="50000000" # 100 ns production
# n_steps_prod="50000000" # 5 ns prod (DEBUG) "50000000" # "25000000" # 50 ns production
# 2500000 -- 5 ns
# 25000000 -- 50 ns
# 50000000 -- 100 ns

### CHECKING FORCEFIELD SUFFIX
forcefield_suffix=$(check_ff_suffix "${forcefield}")

### OUTPUT FILE DETAILS
if [[ "${ligand_fracs}" == "1" ]]; then
    output_dirname="${shape_type}_${temp}_K_${diameter}_nmDIAM_${ligand_names}_${forcefield_suffix}_Trial_${trial_num}" # _${input_gold_folder}"
else
    output_dirname="${shape_type}_${temp}_K_${diameter}_nmDIAM_${ligand_names}_${ligand_fracs}_${forcefield_suffix}_Trial_${trial_num}" # _${input_gold_folder}"
fi

output_dir="${PATH2SIM}/${simulation_dir}/$output_dirname" # <-- EDIT THIS IF YOU WANT THE SIMS TO GO TO A DIFFERENT DIRECTORY
output_file_name="sam"
output_itp="${output_file_name}.itp" # Name of itp file
output_top="${output_file_name}.top" # Name of topology
output_gro="${output_file_name}.gro" # Name of gro file
output_ndx="index.ndx" # name of the index file

### --- USER-SPECIFIC PARAMETERS (END) --- ###

###################
### MAIN SCRIPT ###
###################

### CHECKING IF DIRECTORY EXISTS
check_output_dir "${output_dir}"

### CHANGING DIRECTORY TO OUTPUT DIRECTORY
cd "${output_dir}"

## RUNNING PYTHON SCRIPT TO OUTPUT INTO THE OUTPUT FOLDER
echo "----- RUNNING ${transfer_python_script} SCRIPT ------"
python ${py_transfer_script} --names ${ligand_names} \
                             --prep ${PREP_LIGAND_FINAL} \
                             --ff ${forcefield} \
                             --ofold ${output_dir} \
                             --gfold "${prep_gold_folder}" \
                             --inputgoldfold "${input_gold_folder}" \
                             --goldgro "${input_gold_gro}" \
                             --fracs "${ligand_fracs}" \
                             --ogro "${output_gro}" \
                             --otop "${output_top}" \
                             --geom "${shape_type}"

## DEFINING LIGAND NAME ARRAY
read -a ligand_name_array <<< "$(str2array_by_delim "${ligand_names}" ",")"

## LOOPING THROUGH EACH LIGAND NAME AND COPYING PARAMETERS
for each_ligand in ${ligand_name_array[@]}; do

    ## SEE IF WE NEED TO ADD ADDITIONAL PARAMETERS
    input_ligand_parameter_file_name="${each_ligand}.prm" # _${forcefield_suffix}
    echo "Checking if there are extra ligand parameters for: ${input_ligand_parameter_file_name}"
    ## DEFINING LIGAND PARAMETER FULL PATH
    path_ligand_parameter="${PREP_LIGAND_FINAL}/${each_ligand}/${input_ligand_parameter_file_name}"
    if [ -e "${path_ligand_parameter}" ]; then
        # COPYING PARAMETER FILE
        cp -rv "${path_ligand_parameter}" "./"
        # ADDING PARAMETER TO TOPOLOGY FILE
        force_field_line_num=$(grep -nE "forcefield.itp" ${output_top} | grep -Eo '^[^:]+') # Line where you see forcefield.itp
        sed -i "$((${force_field_line_num}+1))i #include \"${input_ligand_parameter_file_name}\"" ${output_top}
    else
        echo "No additional ligand parameters found at ${path_ligand_parameter}"
        echo "Continuing without inclusion of *.prm files"
    fi

    ## COPYING OVER ITP FILE
    echo "--- COPYING ITP FILES ...---"
    cp -rv "${PREP_LIGAND_FINAL}/${each_ligand}/${each_ligand}.itp" ./

done

## RUNNING PYTHON SCRIPT TO BIND THE SULFUR TO GOLDS
echo "----- RUNNING ${bind_gold_sulfur_script} SCRIPT ------"
python ${py_bind_gold_sulfur_script} --names "${ligand_names}" \
                                     --ifold "${output_dir}"  \
                                     --igro "${output_gro}" \
                                     --top "${output_top}" \
                                     --ores "${output_file_name}" \
                                     --oitp "${output_itp}" \
                                     --geom "${shape_type}"

## COPYING FORCE FIELD TO OUTPUT DIRECTORY
echo -e "--- COPYING FORCE FIELD FILES TO CURRENT DIRECTORY ---\n"
cp -r "${PATH_FORCEFIELD}/${forcefield}" "${output_dir}"

## MOVING ALL MDP FILES INTO DIRECTORY
echo -e "--- COPYING MDP FILES TO CURRENT DIRECTORY ---\n"
cp -rv "${path2mdp}/"{${mdp_em},${mdp_equil},${mdp_prod}} ./

### USING SED TO FIX MDP FILE PARAMETERS
echo -e "--- CORRECTING MDP FILE OPTIONS ---\n"
# CHANGING NUMBER OF STEPS
sed -i "s/NSTEPS/${n_steps_equil}/g" ${mdp_equil}
sed -i "s/NSTEPS/${n_steps_prod}/g" ${mdp_prod}

# CHANGING TEMPERATURE
sed -i "s/TEMPERATURE/${temp}/g" ${mdp_equil}
sed -i "s/TEMPERATURE/${temp}/g" ${mdp_prod}

### FIXING GRO FILE SO IT IS EXPANDED
echo "** EXPANDING BOX LENGTH TO SOLUTE-BOX DISTANCE: ${dist_to_edge} **"
gmx editconf -f "${output_gro}" -o "${output_gro}" -d "${dist_to_edge}"

### SOLVATING WITH WATER
echo -e "--- SOLVATING ---\n"
gmx solvate -cs "spc216.gro" -cp "${output_gro}" -p "${output_top}" -o ${output_gro%.gro}_solvated.gro &> /dev/null # Silencing

# cp ${output_gro} ${output_gro%.gro}_solvated.gro

## REMOVING RESIDUES WITHIN THE CENTERS
if [ "${shape_type}" == "hollow" ]; then
    echo "*** Running ${py_remove_atoms_file} ***"
    python ${py_remove_atoms_file} --rad ${radius} --ofold ${output_dir} --ogro "${output_gro%.gro}_solvated.gro" --otop "${output_top}"
fi

## NEUTRALIZING IF NECESSARY
echo -e "--- NEUTRALIZING CHARGES ---\n"
echo "Creating TPR file for balancing of charge"
gmx grompp -f ${mdp_em} -c ${output_gro%.gro}_solvated.gro -p $output_top -o ${output_gro%.gro}_solvated.tpr -maxwarn ${max_warn_index}
gmx genion -s ${output_gro%.gro}_solvated.tpr -p $output_top -o ${output_gro%.gro}_solvated.gro -neutral << INPUT
SOL
INPUT

## ENERGY MINIMIZING
echo -e "--- ENERGY MINIMIZATION--- \n"
gmx grompp -f ${mdp_em} -c ${output_gro%.gro}_solvated.gro -p $output_top -o ${output_gro%.gro}_em.tpr -maxwarn ${max_warn_index}
gmx mdrun -v -nt 1 -deffnm ${output_gro%.gro}_em

# cp ${output_gro%.gro}_solvated.gro ${output_gro%.gro}_em.gro

## CHECKING RESIDUES WITHIN CORE AFTER ENERGY MINIMIZATION
if [ "${shape_type}" == "hollow" ]; then
    ## MAKING THE MOLECULES WHOLE
gmx trjconv -s ${output_gro%.gro}_em.tpr -f "${output_gro%.gro}_em.gro" -o "${output_gro%.gro}_em.gro" -pbc whole << INPUT
System
INPUT
    
    echo "*** Running ${py_remove_atoms_file} ***"
    python ${py_remove_atoms_file} --rad ${radius} --ofold ${output_dir} --ogro "${output_gro%.gro}_em.gro" --otop "${output_top}"
fi

# cp ${output_gro%.gro}_solvated.gro ${output_gro%.gro}_em.gro

#########################
### SUBMISSION SCRIPT ###
#########################

## DEFINING PATH OUTPUT SUBMISSION
path_output_submit="${output_dir}/${output_submit}"

## CREATING JOB SCRIPT FILE
echo -e "--- CREATING SUBMISSION FILE: $submitScript ---"
cp "${PATH2SUBMISSION}/${submitScript}" "${path_output_submit}"
## EDITING SUBMISSION SCRIPT
sed -i "s/_JOB_NAME_/${output_dirname}/g" "${path_output_submit}"
sed -i "s/_OUTPUTPREFIX_/${output_file_name}/g" "${path_output_submit}"
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${path_output_submit}"
sed -i "s/_EM_GRO_FILE_/${output_gro%.gro}_em.gro/g" "${path_output_submit}"
sed -i "s/_EQUILMDPFILE_/${mdp_equil}/g" "${path_output_submit}"
sed -i "s/_PRODMDPFILE_/${mdp_prod}/g" "${path_output_submit}"
sed -i "s/_TOPFILE_/${output_top}/g" "${path_output_submit}"

## CLEANING UP DIRECTORY FILE
echo -e "--- CLEANING UP DIRECTORY FILE ---"
setup_dir="setup_files"
mkdir -p ${setup_dir}

checkNMoveFile "${output_gro}" ${setup_dir}
checkNMoveFile "${output_gro%.gro}_solvated.gro" ${setup_dir}
mv \#* setup_files

## WRITING OUTPUT DIRECTORY TO JOB LIST FILE
echo "$output_dir" >> "${JOB_SCRIPT}"

