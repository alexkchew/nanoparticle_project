#!/bin/bash

# check_transfer_ligands.sh

# This code checks to see if transfer ligands was correct. It'll loop through and identify which GNPs are there. 
# This code is useful to figure out which jobs might have messed up. 

# Written by: Alex K. Chew (alexkchew@gmail.com)

## CODE TO MOVE FILES
# for file in $(cat output_error_transfer_ligands.txt ); do echo $file; mv $file /home/akchew/scratch/nanoparticle_project/simulations/archive_2021011-Failed_jobs; done


## CODE TO 
# for file in $(cat output_error_transfer_ligands.txt ); do
# 	line_num=$(grep -nE "${file}" job_list.txt  | sed 's/\([0-9]*\).*/\1/')
# 	if [[ ! -z "${line_num}" ]]; then
# 		sed -i -e "${line_num}d" job_list.txt 
# 	fi
# done


## GETTING DATABASE
gnp_path_database="/home/akchew/bin/pythonfiles/modules/gnpdescriptors/database/logP_cell_uptake_zeta_potential_database_GNP.csv"


## GETTING PATH
declare -a multiple_sim_path=(\
	# "/home/akchew/scratch/nanoparticle_project/simulations/20201216-80_of_134_GNPS" \
	# "/home/akchew/scratch/nanoparticle_project/simulations/20201216-6_of_134_GNPS" \
	# "/home/akchew/scratch/nanoparticle_project/simulations/20201216-48_of_134_GNPS" \

	"/home/akchew/scratch/nanoparticle_project/simulations/20210111-Rerun_GNPs_newGNPs" \

	## GETTING RERUN EXAMPLES
	# "/home/akchew/scratch/nanoparticle_project/simulations/20210111-Rerun_GNPs" \
	# "/home/akchew/scratch/nanoparticle_project/simulations/20210111-Rerun_GNPs_stampede2" \
	)

## GETTING SIM PATH
path_to_sim="/home/akchew/scratch/nanoparticle_project/simulations/20201216-80_of_134_GNPS"
path_to_sim="/home/akchew/scratch/nanoparticle_project/simulations/20201216-80_of_134_GNPS"

## DEFINING TRANSFER INFO
transfer_info="transfer_summary.txt"

## DEFINING PATH TO OUTPUT
output_txt_path="/home/akchew/scratch/nanoparticle_project/scripts/output_error_transfer_ligands.txt"
output_txt_path_gnps="/home/akchew/scratch/nanoparticle_project/scripts/output_error_transfer_ligands_gnps.txt"

> ${output_txt_path}
> ${output_txt_path_gnps}

declare -a gnp_names_storage=()

## LOOPING THROUGH PATHS
for path_to_sim in "${multiple_sim_path[@]}"; do

	## GETTING LIST OF DIRECTORIES
	read -a list_of_dirs <<< $(ls ${path_to_sim}/* -d)

	## PRINTING OUT EACH DIRECTORY
	for each_dir in ${list_of_dirs[@]}; do
		## PRINTING DIRECTORY
		echo "${each_dir}"

		## GETTING BASENAME
		current_basename=$(basename ${each_dir})

		## SPLITTING STRING AND GETTING GNP
		IFS='_' read -ra sim_names <<< "${current_basename}"

		## GETTING GNP
		gnp_name=${sim_names[3]}


		## GOING TO PATH
		cd "${each_dir}"

		## GETTING LIGANDS
		read -a output_ligand_array <<< $(grep "Ligands:" ${transfer_info} | sed 's/[^,:]*://g')
		echo "${gnp_name}, ${output_ligand_array[@]}"

		## GETTING DATABASE LIGANDS
		line_num=$(grep -nE "\b${gnp_name}\b" "${gnp_path_database}" | sed 's/\([0-9]*\).*/\1/')

	    ## GETTING THE SPECIFIC LINE
	    specific_line=$(sed -n -e ${line_num}p "${gnp_path_database}")

	    ## READING AS ARRAY
	    IFS=', ' read -r -a specific_string_array <<< "${specific_line}"

	    ## REMOVING SPECIFIC ARRAYS (i.e. GNP1 and diameter)
	    unset "specific_string_array[0]"
	    unset "specific_string_array[1]"
	    unset "specific_string_array[2]"

	    ## REMOVING BLANKS
	    for i in "${!specific_string_array[@]}"; do
	      [ -n "${specific_string_array[$i]}" ] || unset "specific_string_array[$i]"
	    done

	    ## LOOPING THROUGH INDEX
	    index=0

	    ## DECLARING EMPTY ARRAY
	    declare -a lig_array=()
	    declare -a num_lig_array=()
	    ## LOOPING THROUGH AND STORING ALL LIGANDS AND NUMBERS
	    for each_value in "${specific_string_array[@]}"; do
	        if [ $((index%2)) -eq 0 ];
	        then
	            ## STORING
	            lig_array+=("${each_value}")
	        else
	            num_lig_array+=("${each_value}")
	        fi
	        ## GETTING INDEX
	        index=$((${index}+1))
	    done

	    #### CHECKING IF ANY NUMBER OF LIGANDS ARE ZERO
	    ## GETTING NEW ARRAY (WHICH WILL CHECK NUMBER OF LIGANDS)
	    declare -a lig_array_updated=()
	    declare -a num_lig_array_updated=()

	    ## CHECKING IF ANY NUMBER OF LIGANDS IS ZERO. IF ZERO, THEN REMOVE THAT LIGAND COMPLETELY.
	    for i in "${!num_lig_array[@]}"; do 
	        ## GETTING CURRENT NUMBER
	        current_num="${num_lig_array[i]}"

	        ## GETTING INTEGER
	        current_num_int=${current_num%.*}

	        if [[ "${current_num_int}" -ne "0" ]]; then
	            ## GETTING INDEX TO REMOVE
	            lig_array_updated+=("${lig_array[i]}")
	            num_lig_array_updated+=("${current_num_int}")
	        fi

	    done

	    ## COMPARING
	    arr1="${output_ligand_array[@]}"
	    # arr2=("LIG100")
	    arr2="${lig_array_updated[@]}"

	    echo "--> Correct ligands: ${arr2[@]}"

		same_ligands_logical=$(diff <(printf "%s\n" "${arr1[@]}") <(printf "%s\n" "${arr2[@]}"))

		if [[ -z "${same_ligands_logical}" ]]; then
		    echo "TRUE"
		else
		    echo "FALSE"
		    echo "${each_dir}" >> ${output_txt_path}
		    gnp_names_storage+=("${gnp_name}")

		fi



	done

	## STORING GNP NAMES
	echo "${gnp_names_storage[@]}" >> ${output_txt_path_gnps}


done



