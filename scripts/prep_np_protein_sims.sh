#!/bin/bash

# prep_np_protein_sims.sh
# The purpose of this script is to create a nanoparticle-protein system in water. 

# USAGE: bash prep_np_protein_sims.sh
# Written by Alex K. Chew (alexkchew@gmail.com, 06/19/2018)

# ----- SETUP INFORMATION -----
#INPUTS:
#	• Protein/water structure
#		○ Parameter
#		○ itp file
#		○ gro file
#		○ lines for molecules (topology)
#	• Nanoparticle/water gro file + itp + prm file
#	• Topology file for protein-NP systems
#
#OUTPUTS:
#	• New gro file with nanoparticle / protein / water / counterions
#	• Topology file
#	• Quick MD simulation of NP-protein-water system
#
#ALGORITHM:
#	• Create a new directory
#	• Extraction of nanoparticle system with water
#   • Removal of water from the nanoparticle system
#	• Measure of box size of the protein
#	• Measure the box size of the nanoparticle
#	• Resize the boxes and connect them (see water box formation)
#	• Solvate the system with water
#	• Solvate the system with counterions
#	• Energy minimize the system
#	• Create a submission script for:
#		○ NPT equilibration
#       ○ NPT production run

## ** UPDATES ** ##
# 20180620-Completed draft of nanoparticle script

#####################
##### FUNCTIONS #####
#####################

### FUNCTION TO MEASURE BOX SIZE BASED ON GRO FILE
## The purpose of this function is to measure the box size of a gro file.
## INPUTS:
#   $1: gro file
## OUTPUTS:
#   array: array of length 3 with x, y, z components
## USAGE: read -a box_size <<< $(gro_measure_box_size "${gro_file}")
#   ACCESS X, Y, Z BOX LENGTH BY:
#   box_x=${array[0]}
#   box_y=${array[1]}
#   box_z=${array[2]}
function gro_measure_box_size ()
{
    ## DEFINING INPUTS
    input_gro_file_="$1"
    
    ## GET BOX VECTORS AND STORE THEM INTO COMPONENTS
    output=$(tail -n 1 ${input_gro_file_})
    ## CREATING ARRAY OF THE OUTPUTS
    array=($output)
    
    ## OUTPUTTING
    echo "${array[@]}"
}

### FUNCTION TO FIND THE TOTAL NUMBER OF GRO FILES
## The purpose of this function is to find the total number of atoms for a gro file
## INPUTS:
#   $1: gro files
## OUTPUTS:
#   total number of atoms for the gro file
## USAGE: total_atoms=$(gro_count_total_atoms gro_file)
function gro_count_total_atoms () {
    ## DEFINING INPUTS
    input_gro_file_="$1"
    
    ## FINDING TOTAL NUMBER OF ATOMS
    num_atoms=$(head -n 2 "${input_gro_file_}" | tail -n 1) # Gets total number of water molecules
    ## TRIMMING ANY EXCESS
    trim="${num_atoms%"${num_atoms##*[![:space:]]}"}"
    #remove leading whitespace too
    trim="${trim#"${trim%%[![:space:]]*}"}"
    ## PRINTING
    echo "${trim}"
}


### FUNCTION TO COMBINE TWO GRO FILES
## The purpose of this function is to combine two gro files. Note that this uses gmx commands, so gromacs must be installed. 
## INPUTS:
#   $1: gro file 1
#   $2: gro file 2
#   $3: output gro file name
## USAGE:  gro_combine_two_files beta_lactoglobulin_enlarge.gro np_no_water_center_enlarge.gro combined.gro
function gro_combine_two_files ()
{   
    ## DEFINING INPUTS
    input_gro_file_1_="$1"
    input_gro_file_2_="$2"
    
    ## DEFINING OUTPUTS
    output_gro_file_="$3"
    
    ## DEFINING TEMPORARY FILES
    temp_file_1_="${input_gro_file_1_%.gro}_temp.gro"
    temp_file_2_="${input_gro_file_2_%.gro}_temp.gro"
    
    ## CREATING TEMPORARY FILES
    cp "${input_gro_file_1_}" "${temp_file_1_}"
    cp "${input_gro_file_2_}" "${temp_file_2_}"
    
    ## FINDING TOTAL NUMBER OF ATOMS FOR EACH GRO FILE
    gro_1_total_atoms_="$(gro_count_total_atoms "${input_gro_file_1_}")"
    gro_2_total_atoms_="$(gro_count_total_atoms "${input_gro_file_2_}")"
    
    ## FINDING TOTAL NUMBER OF WATER MOLECULES
    total_num_atoms_=$((${gro_1_total_atoms_} + ${gro_2_total_atoms_}))
    
    ## ADJUSTING GRO FILES
    # REMOVING FIRST TWO LINES OF FILE 2
    sed -i '1d' "${temp_file_2_}"
    sed -i '1d' "${temp_file_2_}"
    # REMOVING LAST LINE OF FILE 1
    sed -i '$d' "${temp_file_1_}"
    
    ## COMBINING
    cat "${temp_file_1_}" "${temp_file_2_}" > "${output_gro_file_}"

    ## REPLACE NUMBER OF ATOMS WITH COMBINED NUMBER
    sed -i -e 0,/${gro_1_total_atoms_}/s/${gro_1_total_atoms_}/${total_num_atoms_}/ "${output_gro_file_}"
    
    ## RENUMBER USING GENCONF -- NO CENTERING
    gmx genconf -f "${output_gro_file_}" -o "${output_gro_file_}" -renumber &>/dev/null
    
    ## REMOVING TEMPORARY FILES
    rm "${temp_file_1_}" "${temp_file_2_}"
}

### FUNCTION TO CLEAR TOPOLOGY MOLECULES SECTION
## The purpose of this function is to clear the [ molecules ] section for any topology file
## INPUTS:
#   $1: topology file
## OUTPUTS:
#   Updated topology file with [ molecules ] cleared
## USAGE EXAMPLE: top_clear_molecules sam.top
function top_clear_molecules () {
    ## DEFINING INPUTS
    input_top_file_="$1"
    
    ## DEFINING TEMPORARY TOPOLOGY FILE
    temp_top_file_="${input_top_file_%.top}_temp.top"
    
    ## FINDING LINE WHERE MOLECULES SHOW UP
    line_num_molecules="$(grep -nE '\[ molecules \]' "${input_top_file_}" | sed 's/\([0-9]*\).*/\1/')"
    
    ## REMOVING ALL LINES
    head -n"${line_num_molecules}" "${input_top_file_}" > "${temp_top_file_}"
    
    ## COPYING OVER TOP FILE AND REMOVING THE TEMPORARY
    cp -r "${temp_top_file_}" "${input_top_file_}"
    rm "${temp_top_file_}"
    
    ## ADDING COMMENTS FOR THE TOPOLOGY FILE
    echo "; compound   nr_mol" >> "${input_top_file_}"
}

### FUNCTION TO READ TOPOLOGY MOLECULES (NO COMMENTS)
# The purpose of this function is to read topology file molecules section
## INPUTS:
#   $1: topology file
function top_read_molecules () {
    ## DEFINING INPUTS
    input_top_file_="$1"
    
    ## FINDING LINE WHERE MOLECULES SHOW UP
    line_num_molecules="$(grep -nE '\[ molecules \]' "${input_top_file_}" | sed 's/\([0-9]*\).*/\1/')"
    
    ## ADDING ONE TO LINE NUMBER
    line_num_molecules=$((${line_num_molecules}+1))
    
    ## PRINTING AND IGNORING COMMENTS (;)
    sed -n ''${line_num_molecules}',$p' "${input_top_file_}" | grep -vE "^;"

}

### LOADING PRE-DEFINED VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

## PRINTING SCRIPT NAME
print_script_name

#################################
### DEFINING INPUT PARAMETERS ###
#################################

############# NP SYSTEM #############
## MAIN DIRECTORY
np_main_dir="/home/akchew/scratch/nanoparticle_project/simulations/180610-RVL_Proposal/spherical_310.15_K_2_nmDIAM_ROT-NH3p-OH_CHARMM36_Trial_5"
## OUTPUT PREFIX AND FILES
np_output_prefix="sam"
np_gro_file="${np_main_dir}/${np_output_prefix}_prod.gro"
np_tpr_file="${np_main_dir}/${np_output_prefix}_prod.tpr"
np_top_file="${np_main_dir}/${np_output_prefix}.top"
np_itp_file="${np_main_dir}/${np_output_prefix}.itp"
## SELECTION FOR CHOOSING NANOPARTICLES
np_selection="Other"

############# PROTEIN SYSTEM #############
## PROTEIN NAME
pro_name="beta_lactoglobulin"
pro_gro_file="${INPUT_PRO_DIR}/${pro_name}/${pro_name}.gro"
pro_itp_file="${INPUT_PRO_DIR}/${pro_name}/${pro_name}.itp"
pro_prm_file="${INPUT_PRO_DIR}/${pro_name}/${pro_name}.prm"
pro_top_file="${INPUT_PRO_DIR}/${pro_name}/topol.top"

############# MD SIMULATIONS FILES #############
## FORCE FIELD
forcefield="charmm36-nov2016.ff"

## MDP FILES
mdp_em="minim_sam_gmx5.mdp" # Energy minimization mdp script
mdp_equil="npt_double_equil_gmx5_charmm36.mdp" # Equilibration mdp script
mdp_prod="npt_double_prod_gmx5_charmm36.mdp" # Production mdp script
mdp_dir="${INPUT_DIR}/MDP_FILES"
mdp_folder="Spherical"

## CHECKING MDP FOLDER
path2mdp="${mdp_dir}/${forcefield}/${mdp_folder}"

## NUMBER OF STEPS FOR EQUILIBRATION AND PRODUCTION -- # ps / .002 timesteps
n_steps_equil="2500000" # 5 ns equilibration
n_steps_prod="5000000" # 10 ns production

## TEMPERATURE
temp="310.15" # Kelvins

## JOB SUBMISSION SCRIPT
job_submission_script="submit.sh"

############# OUTPUT NAMES #############
## NAME OF THE DIRECTORY
output_name="${pro_name}_$(basename ${np_main_dir})"
## PREFIX FOR OUTPUT
output_prefix="sam"
## DIRECTORY TO SAVE SIMULATION IN
output_dir_name="180619-RVL_proposal_NP_Protein"
## PATH TO SIMULATION FOLDER
output_path="${PATH2SIM}/${output_dir_name}/${output_name}" 
## NAME OF OUTPUT FILE
output_setup_files_name="setup_files"
## PATH TO SETUP FOLDER
output_setup_files_path="${output_path}/${output_setup_files_name}"
## DEFINING OUTPUT FILE NAMES
# GRO FILE NAME
output_gro="${output_prefix}.gro"
# TOPOLOGY FILE
output_top="${output_prefix}.top"

#####################################################
#################### MAIN SCRIPT ####################
#####################################################

#############################################
### CREATING DIRECTORY AND MOVING INTO IT ###
#############################################
## CHECKING OUTPUT DIRECTORY
check_output_dir "${output_path}"
## MOVING TO OUTPUT DIRECTORY
cd "${output_path}"
## CREATING SETUP FILES
mkdir "${output_setup_files_name}"

#################################################
### REMOVAL OF WATER FROM NANOPARTICLE SYSTEM ###
#################################################

## PRINTING
echo -e "\n--------- REMOVING WATER FROM NANOPARTICLE SYSTEM ---------"
echo "NP WITH NO WATER: ${output_setup_files_path}/np_no_water.gro"
echo "NP CENTERED: ${output_setup_files_path}/np_no_water.gro"
## CREATING EMPTY GRO FILE
gmx trjconv -f "${np_gro_file}" -o "${output_setup_files_path}/np_no_water.gro" -s "${np_tpr_file}" &>/dev/null << INPUTS
${np_selection}
INPUTS

## USING EDITCONF TO CENTER AND CLEAR
gmx editconf -f "${output_setup_files_path}/np_no_water.gro" -o "${output_setup_files_path}/np_no_water_center.gro" -c -d 1.0 -bt cubic &>/dev/null

## COPYING OVER GRO FILE FOR PROTEIN
echo "COPYING OVER PROTEIN GRO FILE: ${pro_gro_file}"
cp -rv "${pro_gro_file}" "${output_setup_files_path}/${pro_name}.gro"

## DEFINING VARIABLES FOR SUBSEQUENT CALCULATIONS
output_setup_np_gro_file="np_no_water_center.gro"
output_setup_pro_gro_file="${pro_name}.gro"
output_setup_np_gro_path="${output_setup_files_path}/${output_setup_np_gro_file}"
output_setup_pro_gro_path="${output_setup_files_path}/${output_setup_pro_gro_file}"

###############################################################
### MEASURING BOX SIZES OF PROTEIN AND NANOPARTICLE SYSTEMS ###
###############################################################

## PRINTING
echo -e "\n--------- MEASURING BOX SIZES OF PROTEIN AND NANOPARTICLES ---------"

### BOX SIZE OF NP SYSTEM
read -a np_box_size <<< $(gro_measure_box_size "${output_setup_np_gro_path}")

### BOX SIZE OF PROTEIN SYSTEM
read -a pro_box_size <<< $(gro_measure_box_size "${output_setup_pro_gro_path}")

## PRINTING
echo "Nanoparticle box size: ${np_box_size[@]}"
echo "Protein box size: ${pro_box_size[@]}"

######################
### RESIZING BOXES ###
######################

## PRINTING
echo -e "\n--------- RESIZING BOXES ---------"

## FINDING TOTAL BOX SIZE
total_box_size=$(awk -v box_1=${np_box_size} -v box_2=${pro_box_size} 'BEGIN{ printf "%.5f", box_1 + box_2 }')

## DEFINING VARIABLES
# NANOPARTICLE GRO FILE (ENLARGED)
output_setup_np_gro_file_enlarge="${output_setup_np_gro_file%.gro}_enlarge.gro"
# PROTEIN GRO FILE (ENLARGED)
output_setup_pro_gro_file_enlarge="${output_setup_pro_gro_file%.gro}_enlarge.gro"

## PRINTING
echo "TOTAL BOX SIZE (nm): ${total_box_size}"
echo "GRO FILE FOR NP RESIZED: ${output_setup_np_gro_file_enlarge}"
echo "GRO FILE FOR PROTEIN RESIZED: ${output_setup_pro_gro_file_enlarge}"

## RESIZING NANOPARTICLE
gmx editconf -f "${output_setup_np_gro_path}" -o "${output_setup_files_path}/${output_setup_np_gro_file_enlarge}" -c -box "${np_box_size}" "${total_box_size}" "${total_box_size}" &>/dev/null

## RESIZING NANOPARTICLE BOX LENGTH
gmx editconf -f "${output_setup_np_gro_path}" -o "${output_setup_files_path}/${output_setup_np_gro_file_enlarge}" -noc -box "${total_box_size}" "${total_box_size}" "${total_box_size}" &>/dev/null

## RESIZING PROTEIN
gmx editconf -f "${output_setup_pro_gro_path}" -o "${output_setup_files_path}/${output_setup_pro_gro_file_enlarge}" -c -box "${pro_box_size}" "${total_box_size}" "${total_box_size}" &>/dev/null

## TRANSLATING THE PROTEIN
gmx editconf -f "${output_setup_pro_gro_path}" -o "${output_setup_files_path}/${output_setup_pro_gro_file_enlarge}" -noc -translate "${pro_box_size}" 0 0 -box "${total_box_size}" "${total_box_size}" "${total_box_size}" &>/dev/null

####################### 
### COMBINING BOXES ###
####################### 

## MOVING INTO SETUP FILES
cd "${output_setup_files_path}"

## DEFINING VARIABLE
combined_gro_file="combined.gro"

## PRINTING
echo -e "\n--------- COMBINING BOXES ---------"
echo "COMBINED GRO FILE: ${combined_gro_file}"

## COMBINING THE BOXES
gro_combine_two_files "${output_setup_np_gro_file_enlarge}" "${output_setup_pro_gro_file_enlarge}" "${combined_gro_file}"

####################
### RE-CENTERING ###
####################

echo -e "\n--------- RECENTERING COMBINED BOX ---------"
echo "RECENTERED BOX: ${combined_gro_file}"
gmx editconf -f "${combined_gro_file}" -o "${combined_gro_file}" -c -d 1.0 -bt cubic &>/dev/null

## CLEANING UP SETUP FILES
rm \#*

##################################
### INCLUSION OF TOPOLOGY FILE ###
##################################

## MOVING INTO SIMULATION FILE
cd "${output_path}"

echo -e "\n--------- INCLUSION OF TOPOLOGY INFORMATION ---------"
## COPYING TOPOLOGY FROM NANOPARTICLE SIMS
cp "${np_top_file}" "./${output_top}"
## COPYING ITP FILES
cp "${np_itp_file}" ./"$(basename ${np_itp_file})"
## COPYING OVER PARAMETER FILES
cp "${np_main_dir}/"*.prm ./
## COPYING PROTEIN ITP AND PRM FILE
cp "${pro_itp_file}" "${pro_prm_file}" ./

## ADDING PROTEIN PARAMETER FILE
# topology_add_include_specific "./${output_top}" "forcefield.itp" "#include \"$(basename ${pro_prm_file})\""

## NOTE! I did not include the protein parameter files --- will need to double-check parameters with the current force fields to ensure correct results

## ADDING PROTEIN ITP INTO THE TOPOLOGY FILE
topology_add_include_specific "./${output_top}" "sam.itp" "#include \"$(basename ${pro_itp_file})\""

## PRINTING
echo "Copying over nanoparticle topology file: ${np_top_file}"
echo "Copying over itp file for nanoparticle: ${np_itp_file}"
echo "Clearing topology and copying over topology files"

## CLEARING MOLECULES FOR TOPOLOGY
top_clear_molecules "./${output_top}"

## ADDING MOLECULES TO TOPOLOGY
# NANOPARTICLE
echo "  sam   1" >> "./${output_top}"
# PROTEIN (AND WATER)
top_read_molecules "${pro_top_file}" >> "./${output_top}"

## COPYING FORCE FIELD TO OUTPUT DIRECTORY
echo -e "--- COPYING FORCE FIELD FILES TO CURRENT DIRECTORY ---\n"
cp -r "${INPUT_DIR}/$forcefield" "${output_path}"

## COPYING OVER MDP FILES
echo "Copying over mdp files: ${mdp_em},${mdp_equil},${mdp_prod}"
echo "MDP file location: ${path2mdp}"
cp -r "${path2mdp}/"{${mdp_em},${mdp_equil},${mdp_prod}} "${output_path}"

### USING SED TO FIX MDP FILE PARAMETERS
echo -e "--- CORRECTING MDP FILE OPTIONS ---\n"
# CHANGING NUMBER OF STEPS
sed -i "s/NSTEPS/${n_steps_equil}/g" ${mdp_equil}
sed -i "s/NSTEPS/${n_steps_prod}/g" ${mdp_prod}

# CHANGING TEMPERATURE
sed -i "s/TEMPERATURE/${temp}/g" ${mdp_equil}
sed -i "s/TEMPERATURE/${temp}/g" ${mdp_prod}

##################################
### SOLVATING AND NEUTRALIZING ###
##################################

echo -e "\n--------- SOLVATING AND NEUTRALIZING ---------"
### SOLVATING
gmx solvate -cs spc216.gro -cp "${output_setup_files_path}/${combined_gro_file}" -p ./${output_top} -o "${output_setup_files_path}/${combined_gro_file%.gro}_solvated.gro" &> /dev/null # Silencing

### NEUTRALIZING
gmx grompp -f "${mdp_em}" -c "${output_setup_files_path}/${combined_gro_file%.gro}_solvated.gro" -p ${output_top} -o ${output_gro%.gro}_solvated.tpr
gmx genion -s "${output_gro%.gro}_solvated.tpr" -p ${output_top} -o "${output_gro%.gro}_solvated.gro" -neutral << INPUT
SOL
INPUT

###########################
### ENERGY MINIMIZATION ###
###########################
echo -e "--- ENERGY MINIMIZATION--- \n"
gmx grompp -f ${mdp_em} -c "${output_gro%.gro}_solvated.gro" -p ${output_top} -o ${output_gro%.gro}_em.tpr
gmx mdrun -v -nt 1 -deffnm ${output_gro%.gro}_em

####################################
### CREATING JOB SUBMISSION FILE ###
####################################
echo -e "--- CREATING SUBMISSION FILE: ${job_submission_script} ---"
sed "s/JOB_NAME/${output_name}/g" "${forcefield}/${job_submission_script}" > ./${job_submission_script}
## WRITING OUTPUT TO JOB LIST FILES
echo "${output_path}" >> "${JOB_SCRIPT}"

####################################
### CLEANING UP DIRECTORY SYSTEM ###
####################################
checkNMoveFile "${output_gro%.gro}_solvated.gro" "${output_setup_files_path}"
rm \#*

