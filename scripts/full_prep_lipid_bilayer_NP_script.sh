#!/bin/bash

# full_prep_lipid_bilayer_NP_script.sh
# This is a preparation script for NP-lipid membrane

# Written by: Alex K. Chew (01/09/2020)

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## ENABLING EXTERNAL MODULES
shopt -s extglob
# ^ Allows you to move all except 1

## PRINTING SCRIPT NAME
print_script_name

##########################
### DECLARING DEFAULTS ###
##########################
## DEFINING BASH CODE
bash_code="${PATH2SCRIPTS}/prep_lipid_bilayer_NP_script.sh"

#######################
### INPUT VARIABLES ###
#######################

## DECLARING LIGAND NAMES
declare -a ligand_names=("ROT017")
# "ROT017"
# "ROT010"
# "ROT004" "ROT011"
# "ROT004" "ROT011"
#  - Benzene
# "ROT012" "ROT001" 
# "ROT001" "ROT012" "ROT006"
# "ROT001" 
# "ROT006"
# "ROT012"
# "ROT001""ROT001"

## DEFINING INPUT SIMULATION PATH
np_main_dir_name="ROT_WATER_SIMS"
np_main_dir_name="ROT_WATER_SIMS_MODIFIEDFF"
# "EAM_COMPLETE"
# "191221-Rerun_all_EAM_models_1_cutoff"

## OUTPUT MAIN DIRECTORY
main_dir_name="20200816-modified_ff_pulling"
# "20200608-Pulling_other_ligs"
# "20200113-NP_lipid_bilayer_resize"

## LIPIID TYPE
lipid_type="DOPC"

## DEFINING LIPID SIZE
declare -a lipid_size=("196") 
# "225"
# "144" "196" 
#  "196" "244"

## DEFINING TEMPERATURE
temperature="300.00" # K

## DEFINING WANT DEBUG
want_debug=false
# false

## DEFINING IF YOU WNAT MODIFIED FORCE FIELD
modified_ff="true"

## DEFINING DISTANCE
np_lipid_membrane_dist="5"

###################
### MAIN SCRIPT ###
###################

## LOOPING THROUGH EACH PATH
for each_lig in ${ligand_names[@]}; do
    ## LOOPING THROUGH EACH LIPID SIZE
    for each_lipid_size in ${lipid_size[@]}; do
    
        ## RUNNING CODE
        bash "${bash_code}" \
                    "${np_main_dir_name}" \
                    "${main_dir_name}" \
                    "${each_lig}" \
                    "${temperature}" \
                    "${lipid_type}" \
                    "${each_lipid_size}" \
                    "${want_debug}" \
                    "${np_lipid_membrane_dist}" \
                    "${modified_ff}"
                
    done
    

done


