#!/bin/bash

# prep_frozen_planar_sam.sh
# The purpose of this script is to generate planar SAMs that are frozen. 
# The main idea is to take the last frame of the planar SAM, freeze it, then run hydrophobicity measurements when it is frozen. 
#
# Written by: Alex K. Chew (11/23/2019)
#
# 
# ALGORITHM:
#	- GMX TRJCONV to extract the last frame of the trajectory
#	- Generate a production job with 1 ps output (may need new MDP file)
# 	- Run the job

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## ENABLING EXTERNAL MODULES
shopt -s extglob
# ^ Allows you to move all except 1

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## FORCE FIELD
forcefield="${FORCEFIELD}" # Force Field (opls-aa or charmm36-nov2016.ff)

## DEFINING PATH
simulation_path="$1"
# "/home/akchew/scratch/nanoparticle_project/simulations/191121-Planar_SAMs_Running_10000_mdpfix"

## DEFINING SIMULATION DIR NAME
simulation_dir_name="$2"
# "Planar_300.00_K_dodecanethiol_10x10_CHARMM36jul2017_intffGold_Trial_1"

## DEFINING OUTPUT PATH
output_sim_path="$3"
# "/home/akchew/scratch/nanoparticle_project/simulations/191123-Frozen_SAM_sims"

## DEFINING FULL PATH
full_input_sim_path="${simulation_path}/${simulation_dir_name}"

## DEFINING LOGICALS
want_translate=true
## DEFINING JOB TYPE
job_type="NVT_spr"
# NVT_spr: GNP with a specified frozen group in the NVT ensemble
# unfrozen_sam: If you want unfrozen sam
# frozen_sam: If you want the entire SAM to be frozen

## DEFINING SPRING CONSTANT FOR THE LIGAND IN NVT SPIR
lig_spring_constant="${4-50}"
# "50"
lig_posre="lig_posre.itp" # ligand position restraints

## DEBUGGING
want_debug="${5-false}" # true if you want to debug this, which means 
# that the mdp file is 5 ns


## DEFINING VACUUM LAYER BOX
vacuum_layer_z_above="${6-4}" # N nm above and below the SAMs
# If this is zero, then nothing will be done to add vacuum layer

#########################
### DEFAULT VARIABLES ###
#########################

## OUTPUT FILES FOR SAM PLANAR
planar_sam_output_prefix="sam_prod"

## DEFINING XTC FILE, ETX.
planar_sam_output_tpr="${planar_sam_output_prefix}.tpr"
planar_sam_output_xtc="${planar_sam_output_prefix}.xtc"
planar_sam_output_top="sam.top"

## DEFINING OUTPUT PREFIX
output_prefix="sam"

## FRAME TO EXTRACT FROM
last_frame_ps="5000"
# "50000"

## GOLD RES NAME
gold_resname="AUI"

## DEFINING NUMBER OF CORES
num_cores="28"

## DEFINING MAX WARN
max_warn="5"

########################
### FILE INFORMATION ###
########################

## DEFINING MDP FILE
mdp_parent_path="${PATH_MDP_CHARMM36_PLANAR}"

## DEFINING IF YOU WANT NPT
want_npt_prod="false"
# true

## DEFINING IF YOU WANT EXPAND AFTER EQUIL
want_expand_after_equil=true

## DEFINING EQUIL
npt_equil_n_steps="2500000" # 5 ns
# "250000" # 5 ns
# "1000000" # 2 ns equilibration
npt_equil_mdp_file="npt_double_equil_gmx5_charmm36.mdp"
npt_equil_n_steps2="2500000" # 5 ns
# "250000" # 5 ns
# "1000000" # 2 ns equilibration
npt_equil_mdp_file2="npt_double_prod_gmx5_charmm36.mdp"

if [[ "${job_type}" == "unfrozen_sam" ]]; then
    mdp_file_name="nvt_double_prod_gmx5_charmm36_frozen_sam.mdp"
else
    if [[ "${want_npt_prod}" == true ]]; then
        ## TURNING EQUIL NPT OFF
        npt_equil_mdp_file2=""

        ## TURNING NPT ON
        mdp_file_name="npt_double_prod_gmx5_charmm36_frozen_gold_only.mdp"
    else

        mdp_file_name="nvt_double_prod_gmx5_charmm36_frozen_gold_only.mdp"

    fi
fi

## DEFINING MDP FILE TIME
if [[ "${want_debug}" == true ]]; then
    mdp_file_time="2500000" # 5 ns
else
    mdp_file_time="25000000" # 50 ns
fi

## DEFINING SUBMISSION FILE INFORMATION
submit_parent_path="${PATH2SUBMISSION}"
submit_file_name="submit_sam_planar_frozen_sam.sh"

## DEFINING OUTPUT SUBMIT NAME
output_submit_name="submit.sh"

## DEFINING INDEX FILE
index_file="index.ndx"

## DEFINING OUTPUT PREFIX
output_prefix="sam"

## DEBUGGIN SLEEP TIME
sleep_time="0"
# "2"

########################
### OUTPUT VARIABLES ###
########################

## EXTRACTING PLANAR SAM NAME
read -a extract_array <<< $(np_extract_planar_sam_output_dirname ${simulation_dir_name})

## GETTING LIGAND NAME
lig_name="${extract_array[3]}"

## DEFINING TEMPERATURE
temp="${extract_array[1]}"

## GETTING OUTPUT NAME
if [[ "${job_type}" == "unfrozen_sam" ]]; then
    output_name_prefix="Frozen"
elif [[ "${job_type}" == "NVT_spr" ]]; then
    output_name_prefix="NVTspr_${lig_spring_constant}_"
else
    output_name_prefix="FrozenGold"
fi

output_name="${output_name_prefix}${simulation_dir_name}-${last_frame_ps}_ps"

## DEFINING FULL OUTPUT SIM PATH
full_output_sim_path="${output_sim_path}/${output_name}"

###################
### MAIN SCRIPT ###
###################

## CREATING DIRECTORY
create_dir "${full_output_sim_path}" -f

## GOING INTO DIRECTORY
cd "${full_output_sim_path}"

## GETTING FUNCTIONS
function_name="nanoparticle_functions.sh"
function_path="${PATH2BIN}/${function_name}"

## COPYING
cp -r "${function_path}" "${full_output_sim_path}"

##############################
### 1. EXTRACTING GRO FILE ###
##############################
echo "-------------------------------"
echo "Step 1: Extracting gro file"
echo "   Input simulation path: ${full_input_sim_path}"
echo "   Output simulation path: ${full_output_sim_path}"
echo "   Output gro file name: ${output_prefix}.gro"
sleep "${sleep_time}"

## GMX TRJCONV to extract files
gmx trjconv -f "${full_input_sim_path}/${planar_sam_output_xtc}" \
			-s "${full_input_sim_path}/${planar_sam_output_tpr}" \
			-o "${full_output_sim_path}/${output_prefix}.gro" \
			-dump "${last_frame_ps}" \
			-pbc mol \
			-nocenter \
            << INPUTS
System
INPUTS

# >/dev/null 2>&1 \

###################################
### TRANSLATING FOR PLANAR CASE ###
###################################
if [[ "${want_translate}" == true ]]; then
    translate_planar_sam_z "${output_prefix}.gro" \
                           "${full_input_sim_path}/${planar_sam_output_tpr}" \
                           "${gold_resname}" "${output_prefix}_translated.gro"
else
    cp "${output_prefix}.gro" "${output_prefix}_translated.gro"
fi

## CHECKING IF TRANSLATED CORRECTLY
if [ -e "${output_prefix}_translated.gro" ]; then
    ## CREATING SETUP FILES AND RENAMING
    mkdir setup_files
    mv "${output_prefix}.gro" setup_files
    mv "${output_prefix}_translated.gro" "${output_prefix}.gro" 
fi

###########################
### ADDING VACUUM LAYER ###
###########################
## SEEING IF VACUUM LAYER IS DESIRED
if [[ "${vacuum_layer_z_above}" != "0" ]] && [[ "${want_expand_after_equil}" == false ]]; then
    echo "Adding vacuum layer of: ${vacuum_layer_z_above} nm"

    ## GETTING GRO SIZE
    read -a gro_box_size <<< $(gro_measure_box_size "${output_prefix}.gro")

    ## GETTING THE Z DIMENSION TOTAL SIZE
    total_z_size=$(awk -v z_dim=${gro_box_size[2]} -v new_z_layer=${vacuum_layer_z_above}} 'BEGIN{ printf "%.3f", z_dim + 2 * new_z_layer }')

    ## RE-SIZING (no centering)
    gmx editconf -f "${output_prefix}.gro" \
                 -box ${gro_box_size[0]} ${gro_box_size[1]} ${total_z_size} \
                 -o "${output_prefix}_vacuum.gro" \
                 -noc

    ## TRANSLATING
    translate_planar_sam_z "${output_prefix}_vacuum.gro" \
                           "${full_input_sim_path}/${planar_sam_output_tpr}" \
                           "${gold_resname}" \
                           "${output_prefix}_vacuum_translated.gro"

    ## CHECKING IF TRANSLATED CORRECTLY
    if [ -e "${output_prefix}_vacuum_translated.gro" ]; then
        ## CREATING SETUP FILES AND RENAMING
        mkdir setup_files
        mv "${output_prefix}.gro" "${output_prefix}_vacuum.gro" "${output_prefix}_vacuum_translated.gro" setup_files
        cp -r "setup_files/${output_prefix}_vacuum_translated.gro" "${output_prefix}.gro" 
    fi

fi


#############################
### 2. COPYING OVER FILES ###
#############################
echo "-------------------------------"
echo "Step 2: Copying forcefield, itp, mdp files"
sleep "${sleep_time}"

## FORCE FIELD
cp -r "${full_input_sim_path}/${forcefield}" "${full_output_sim_path}"

## TOPOLOGY
cp -r "${full_input_sim_path}/${planar_sam_output_top}" "${full_output_sim_path}"

## MDP FILE (EQUL AND PRODUCTION)
cp -r "${mdp_parent_path}/${npt_equil_mdp_file}" "${full_output_sim_path}"
if [ ! -z "${npt_equil_mdp_file2}" ]; then 
    cp -r "${mdp_parent_path}/${npt_equil_mdp_file2}" "${full_output_sim_path}"
fi
cp -r "${mdp_parent_path}/${mdp_file_name}" "${full_output_sim_path}"

## SUBMISSION FILE
cp -r "${submit_parent_path}/${submit_file_name}" "${full_output_sim_path}/${output_submit_name}"

########################
### 3. EDITING FILES ###
########################
echo "-------------------------------"
echo "Step 3: Editing files"
sleep "${sleep_time}"

## GETTING ITP FILE
ligand_itp_file="${full_output_sim_path}/${forcefield}/${lig_name}.itp"
ligand_resname="$(itp_get_resname ${ligand_itp_file})"

## DEFINING ATOM NAME OF SULFUR
sulfur_atom_name="${SULFUR_ATOM_NAME}"

## MAKING INDEX FILE 
gmx make_ndx -f "${full_input_sim_path}/${planar_sam_output_tpr}" \
			 -o "${full_output_sim_path}/${index_file}" \
             >/dev/null 2>&1 << INPUTS
keep 0
r ${gold_resname}
r ${ligand_resname}
a ${sulfur_atom_name}
q
INPUTS

## ADDING TO INDEX
if [[ "${job_type}" == "NVT_spr" ]]; then

## DEFINING VARIABLE
name_for_lig_no_sulfur="${ligand_resname}_heavy_atoms_no_sulfur"

## ADDING TO INDEX
gmx make_ndx -f "${full_input_sim_path}/${planar_sam_output_tpr}" \
             -n "${full_output_sim_path}/${index_file}" \
             -o "${full_output_sim_path}/${index_file}" \
             >/dev/null 2>&1  << INPUTS
r ${ligand_resname} & !a H* & ! a ${sulfur_atom_name}
name 4 ${name_for_lig_no_sulfur}
q
INPUTS

## GENERATING POSITION RESTRAINTS FOR GOLD
gmx genrestr -f "${output_prefix}.gro" \
             -o "${lig_posre}" \
             -n "${index_file}" \
             -fc "${lig_spring_constant}" \
                 "${lig_spring_constant}" \
                 "${lig_spring_constant}"  << INPUTS
${name_for_lig_no_sulfur}
INPUTS
# >/dev/null 2>&1

## FIXING THE POSITION RESTRAINTS
fix_posre_for_single_residue "${lig_posre}" "${ligand_itp_file}"

## ADDING POSITION RESTRAINTS TO TOPOLOGY FILE
topology_add_include_specific "${planar_sam_output_top}" "${lig_name}.itp" "#include \"${lig_posre}\""

## COMMENTING OUT FREEZE GROUPS
mdp_comment_out_specific_flags "${mdp_file_name}" "freezegrps"
mdp_comment_out_specific_flags "${mdp_file_name}" "freezedim"

fi

## EDITING EQUIL MDP FILE
sed -i "s/_NSTEPS_/${npt_equil_n_steps}/g" "${npt_equil_mdp_file}"
sed -i "s/_TEMPERATURE_/${temp}/g" "${npt_equil_mdp_file}"

## EDITING EQUIL MDP FILE 2
if [ ! -z "${npt_equil_mdp_file2}" ]; then 
    sed -i "s/_NSTEPS_/${npt_equil_n_steps2}/g" "${npt_equil_mdp_file2}"
    sed -i "s/_TEMPERATURE_/${temp}/g" "${npt_equil_mdp_file2}"
fi

## EDITING MDP FILE
sed -i "s/_NSTEPS_/${mdp_file_time}/g" "${mdp_file_name}"
sed -i "s/_TEMPERATURE_/${temp}/g" "${mdp_file_name}"
sed -i "s/_GOLDRESNAME_/${gold_resname}/g" "${mdp_file_name}"
if [[ "${job_type}" != "unfrozen_sam" ]]; then
    sed -i "s/_LIGANDRESNAME_/${ligand_resname}/g" "${mdp_file_name}"
else
    sed -i "s/_SULFURNAME_/${sulfur_atom_name}/g" "${mdp_file_name}"
fi

## EDITING SUBMISSION FILE
sed -i "s/_NUM_CORES_/${num_cores}/g" "${output_submit_name}"
sed -i "s/_INDEXFILE_/${index_file}/g" "${output_submit_name}"
sed -i "s/_EQUILMDP_/${npt_equil_mdp_file}/g" "${output_submit_name}"
sed -i "s/_EQUILMDP2_/${npt_equil_mdp_file2}/g" "${output_submit_name}"
sed -i "s/_PRODMDP_/${mdp_file_name}/g" "${output_submit_name}"
sed -i "s/_MAXWARN_/${max_warn}/g" "${output_submit_name}"
sed -i "s/_JOB_NAME_/${output_name}/g" "${output_submit_name}"
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" "${output_submit_name}"
sed -i "s/_DISTAFTEREQUIL_/${vacuum_layer_z_above}/g" "${output_submit_name}"
sed -i "s/_FUNCTIONSRC_/${function_name}/g" "${output_submit_name}"

# ##############################
# ### 4. GENERATING TPR FILE ###
# ##############################

# echo "-------------------------------"
# echo "Step 4: Generating TPR file"
# sleep "${sleep_time}"
# ## USING TRJCONV
# gmx grompp -f "${mdp_file_name}"\
# 		   -c "${output_prefix}.gro" \
# 		   -p "${planar_sam_output_top}" \
# 		   -o "${output_prefix}_prod" \
# 		   -n "${index_file}" \
# 		   -maxwarn "${max_warn}" 
           
#            # >/dev/null 2>&1

## ADDING JOB LIST
echo "${full_output_sim_path}/${output_submit_name}" >> "${JOB_SCRIPT}"

