#!/bin/bash

# prep_lm_NP_pulling_then_unbiased_sims.sh
# The purpose of this code is to run pulling, equilibration, then production unbiased sims.
# The idea is to see if the nanoparticle would desorb after pulling. 

# Written by: Alex K. Chew (alexkchew@gmail.com, 04/22/2020)

#################################################
### LOADING PRE-DEFINED VARIABLES / FUNCTIONS ###
#################################################
source "../bin/nanoparticle_rc.sh"
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING INPUT SIMULATION DIR
input_sim_dir_name="20200120-US-sims_NPLM_rerun_stampede"

## DEFINING INPUT DIRECTORY
input_directory="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
declare -a distance_array=("5.100")
input_directory="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
declare -a distance_array=("4.900")


input_sim_dir_name="20200613-US_otherligs_z-com"
input_directory="US-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT004_1"
declare -a distance_array=("5.100")


## DEFINING OUTPUT DIRECTORY
output_sim_dir_name="20200423-pulling_unbiased_full"
# _full"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING PYTHON SCRIPT TO FIND CLOSEST NP
py_find_closest_lig="${MDDESCRIPTOR}/application/np_lipid_bilayer/find_ligand_closest_to_lipid_bilayer.py"

## DEFINING SPRING CONSTANT
spring_constant="2000"

## DEFINING TEMPERATURE
temp="300.00"

## DEFINING DEBUG
want_debug=false
# true

## FORCE FIELD
forcefield="${FORCEFIELD}"

## RUN PARAMETERS
## DEFINING MAX WARN
maxwarn="5"

## RUN INFORMATION
num_cores="28"

## DEFINING EM NUMBER OF CORES
em_num_cores="28"

## DEFINING MD RUN COMMAND
mdrun_command="gmx mdrun -nt"

## DEFINING SUFFIX
mdrun_command_suffix=""

## LOOPING DISTANCES
for desired_distance in ${distance_array[@]}; do

####################
### OUTPUT NAMES ###
####################

## DEFINING OUTPUT NAME
output_prefix="nplm"

## DEFINING PREFIX
if [[ ${input_directory} = "USrev"* ]]; then
    want_reverse=true
else
    want_reverse=false
fi

## DEFINING PREFIX
if [[ "${want_reverse}" == true ]]; then
    ## GETTING NEW NAME
    truncated_name=$(sed "s/.*USrev//g" <<< ${input_directory})
    prefix="NPLMpulling2unb_rev"
else
    prefix="NPLMpulling2unb"
    ## GETTING NEW NAME
    truncated_name=$(sed "s/.*NPLM//g" <<< ${input_directory})
fi

## DEFINING OUTPUT NAME
output_directory="${prefix}-${desired_distance}_${spring_constant}${truncated_name}"
echo "Output name: ${output_directory}"

########################
### MDP FILE DETAILS ###
########################

## DEFINING TEMPERATURE
temp="300.00"

## DEFINING MDP FILE
mdp_em="em.mdp"
mdp_pull="pulling.mdp"
mdp_equil="umbrella_sampling_equil.mdp"
mdp_prod="unbiased_prod.mdp"

## DEFINING MDP DETAILS
dt="0.002"

## DEFINING PULL CONSTANT
pull_constant="2000" # kJ/mol/nm2, as defined by previous papers

## NUMBER OF STEPS
if [[ ${want_debug} == true ]]; then
    echo "Since debug is turned ON, lowering number of steps"
    sleep 1
    mdp_equil_nsteps="1000" # 500 ps
    mdp_prod_nsteps="1000" # 50 ns production
    pull_rate="0.1" # even faster pull rate
    # "0.01" # Fast pull rate
else
    mdp_equil_nsteps="5000000" # 10 ns equilibration
    # "1000000" # 2 ns equilibration -- not enough
    # "250000" # 500 ps
    mdp_prod_nsteps="25000000" # 50 ns production
    pull_rate="0.0005" # nm / ps -- PRODUCTION
fi

######################
### DEFINING PATHS ###
######################

## DEFINING INPUT DIRECTORY
path_input_dir="${PATH2NPLMSIMS}/${input_sim_dir_name}/${input_directory}"

## DEFINING OUTPUT DIR
path_output_dir="${PATH2NPLMSIMS}/${output_sim_dir_name}/${output_directory}"

## CHECKING IF EXISTS
stop_if_does_not_exist "${path_input_dir}"

## MDP FILE INFORMATION
path_mdp_parent="${INPUT_MDP_PATH}/charmm36"
path_mdp_dir="${path_mdp_parent}/lipid_bilayers"

####################################
### DEFINING DIRECTORY STRUCTURE ###
####################################

## FOR INPUT CONFIGS INITIAL
path_input_dir_input_files="${path_input_dir}/1_input_files"
path_input_dir_configs="${path_input_dir}/2_configurations"
path_input_dir_sims="${path_input_dir}/4_simulations"

## DEFINING PREFIX
input_prefix="nplm_prod"

## DEFINING GRO FILES
config_gro="${input_prefix}.gro"
config_tpr="${input_prefix}.tpr"

## DEFINING PATH TO GRO
path_gro="${path_input_dir_sims}/${desired_distance}/${config_gro}"
path_tpr="${path_input_dir_sims}/${desired_distance}/${config_tpr}"

## DEFINING TOP FILE
input_top="nplm.top"
## DEFINING TOPPAR
toppar_file="toppar"

## DEFINING GOLD CORE RESIDUE NAME
gold_core_resname="AUNP"

## DEFINING LM RESIDUE NAME
lipid_name="DOPC"
lm_residue_name="DOPC"
lipid_membrane_temp="300.00"
lipid_size="196"

## LIPID MEMBRANE ANALYSIS FILES
lipid_membrane_analysis="analysis"
lipid_membrane_text_file="lipid_membrane_info.txt"

## DEFINING NP ITP FILE
np_itp_file="sam.itp"

###################
### MAIN SCRIPT ###
###################

## MAKING DIRECTORY
create_dir "${path_output_dir}" -f

## GOING TO DIRECTORY
cd "${path_output_dir}"

## CREATING INDEX FILE
gold_index_file="gold.ndx"
echo "Creating gold index file: ${gold_index_file}"
gmx make_ndx -f "${path_tpr}" \
             -o "${path_output_dir}/${gold_index_file}" >/dev/null 2>&1 << INPUTS
keep 0
r ${gold_core_resname}
q
INPUTS

echo "Creating gro file with NP centered: ${output_prefix}.gro"
## COPYING OVER GRO FILE
gmx trjconv -f "${path_gro}" \
            -s "${path_tpr}" \
            -pbc res \
            -n "${path_output_dir}/${gold_index_file}" \
            -o "${path_output_dir}/${output_prefix}.gro" \
            -center >/dev/null 2>&1 << INPUTS
${gold_core_resname}
System
INPUTS

## COPYING FORCE FIELD
cp -r "${path_input_dir_input_files}/"{${forcefield},${toppar_file}} "${path_output_dir}"
## COPYING ITP AND PRM
cp -r "${path_input_dir_input_files}/"{*.itp,*.prm} "${path_output_dir}"
## COPYING TOP FILE
cp -r "${path_input_dir_input_files}/${input_top}" "${path_output_dir}/${output_prefix}.top"

######################################
### PART 1: FINDING LIGAND CLOSEST ###
######################################

echo ""
echo "-------------------------"
echo "Part 1: Finding ligand closest to lipid membrane"
echo "-------------------------"
summary_file="np_closest.summary"
pickle_file="np_closest.pickle"

echo "Summary file: ${summary_file}"
echo "Pickle file: ${pickle_file}"

## RUNNING PYTHON SCRIPT
python3.6 "${py_find_closest_lig}" --path_sim "${path_output_dir}" \
                                   --gro_file "${output_prefix}.gro" \
                                   --itp_file "${np_itp_file}" \
                                   --summary "${summary_file}" \
                                   --pickle "${pickle_file}" \
                                   --lm_resname "${lm_residue_name}" 

## EXTRACTING INDEX
stop_if_does_not_exist "${summary_file}"
closest_atom_index=$(grep 'CLOSEST_ATOM_INDEX' ${summary_file} | awk '{print $2}')
closest_dist_z=$(grep 'ATOM_DISTANCE_TO_LM' ${summary_file} | awk '{print $2}')

## ADDING 1 SINCE GROMACS STARTS COUNTING AT 1
closest_atom_index_plusone=$((${closest_atom_index}+1))

## EXTRACTION OF LIPID MEMBRANE THICKNESS INFORMATION
## DEFINING PATH TO TEXT

## DEFINING FOLDER NAME
lipid_membrane_folder=$(get_lipid_membrane_folder_name "${lipid_name}" \
                                                       "${lipid_membrane_temp}" \
                                                       "${lipid_size}" 
                                                       )
## DEFINING PATH TO LIPID MEMBRANE
path_lipid_membrane="${INPUT_LIPID_MEMBRANE}/${lipid_membrane_folder}/gromacs"
path_lipid_membrane_text_file="${path_lipid_membrane}/${lipid_membrane_analysis}/${lipid_membrane_text_file}"
stop_if_does_not_exist "${path_lipid_membrane_text_file}"
## GETTING MEMBRANE HALF THICKNESS
membrane_half_thickness="$(grep "Membrane_half_thickness" "${path_lipid_membrane_text_file}" | awk '{print $2}')"

echo "-------"
echo "Closest atom index: ${closest_atom_index}"
echo "Closest atom index plus 1: ${closest_atom_index_plusone}"
echo "Z-distance to lipid membrane(nm): ${closest_dist_z}"
echo "Membrane half thickness (nm): ${membrane_half_thickness}"

##################################################
### PART 2: CREATING MDP FILES AND INDEX FILES ###
##################################################

echo ""
echo "-------------------------"
echo "Part 2: Creating index files and computing distances"
echo "-------------------------"

## CREATING INDEX FILE
index_file="${output_prefix}.ndx"
closest_atom_name="CLOSEST_ATOM"

## CREATING INDEX FILE
echo "Creating index file: ${index_file}"
gmx make_ndx -f "${output_prefix}.gro" \
             -o "${index_file}" >/dev/null 2>&1  << INPUTS
keep 0
a ${closest_atom_index_plusone}
name 1 ${closest_atom_name}
r ${lm_residue_name}
q
INPUTS


## FINAL DISTNACE
final_distance=$(awk -v half_height=${membrane_half_thickness} 'BEGIN{ printf "%.3f", half_height/2 }')

## GETTING DISTANCE
total_distance=$(awk -v total_z_dist=${closest_dist_z} \
                     -v final_dist=${final_distance} 'BEGIN{ printf "%.3f", total_z_dist - final_dist }')

## COMPUTING TOTAL SIMULATION TIME
total_sim_time_ps=$(awk -v total_dist=${total_distance} \
                         -v pull_rate=${pull_rate} 'BEGIN{ printf "%.3f", total_dist/pull_rate }')

## COMPUTING TOTAL STEPS
total_steps=$(awk -v total_time_ps=${total_sim_time_ps} \
                   -v dt=${dt} 'BEGIN{ printf "%d", total_time_ps/dt }')

echo "Final distance between groups: ${final_distance} nm"
echo "Total distance to move: ${total_distance} nm"
echo "Distance computed based on total distance - half_membrane_thickness /2"
echo "Total simulation time in ps: ${total_sim_time_ps} [Pull rate: ${pull_rate}]"
echo "Total simulation steps: ${total_steps} [dt: ${dt} ps / step]"


####################################################
### PART 3: CREATING SUBMISSION, MDP FILES, ETC. ###
####################################################

echo ""
echo "-------------------------"
echo "Part 3: Creating mdp files, submission files etc."
echo "-------------------------"

## COPYING MDP FILE
cp -r "${path_mdp_dir}"/{${mdp_em},${mdp_pull},${mdp_equil},${mdp_prod}} "${path_output_dir}"

## EDITING PULL FILE
sed -i "s/_GROUP_1_NAME_/${closest_atom_name}/g" "${mdp_pull}" "${mdp_equil}"
sed -i "s/_GROUP_2_NAME_/${lm_residue_name}/g" "${mdp_pull}" "${mdp_equil}"
sed -i "s/_TEMPERATURE_/${temp}/g" "${mdp_pull}" "${mdp_equil}" "${mdp_prod}"
sed -i "s/_DT_/${dt}/g" "${mdp_pull}" "${mdp_equil}" "${mdp_prod}"
sed -i "s/_PULLRATE_/${pull_rate}/g" "${mdp_pull}"
sed -i "s/_PULLKCONSTANT_/${pull_constant}/g" "${mdp_pull}" "${mdp_equil}"
sed -i "s/_NSTEPS_/${total_steps}/g" "${mdp_pull}"

## EDITING EQUIL FILE
sed -i "s/_NSTEPS_/${mdp_equil_nsteps}/g" "${mdp_equil}"
sed -i "s/_INITIALDIST_/${final_distance}/g" "${mdp_equil}"

## EDITING PRODUCTION FILE
sed -i "s/_NSTEPS_/${mdp_prod_nsteps}/g" "${mdp_prod}"

## SUBMISSION SCRIPT
run_file="run_lm_NP_pulling_then_unbiased.sh"
submit_file="submit_run_lm_NP_pulling_then_unbiased.sh"
path_run_file="${PATH2SUBMISSION}/${run_file}"
path_submit_file="${PATH2SUBMISSION}/${submit_file}"

## DEFINING OUTPUT
output_run="run.sh"
output_submit="submit.sh"
gromacs_command="gmx"

## COPYING
cp -r "${path_run_file}" "${path_output_dir}/${output_run}"
cp -r "${path_submit_file}" "${path_output_dir}/${output_submit}"

## COPYING RC
cp -r "${PATH_GENERAL_RC}" "${path_output_dir}"

## EDITING
sed -i "s/_RCFILE_/${GENERAL_RC_NAME}/g" "${output_run}"

sed -i "s/_INPUTTOPFILE_/${output_prefix}.top/g" "${output_run}"
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" "${output_run}"
sed -i "s/_MAXWARN_/${maxwarn}/g" "${output_run}"

sed -i "s/_EM_MDP_FILE_/${mdp_em}/g" "${output_run}"
sed -i "s/_PULL_MDP_FILE_/${mdp_pull}/g" "${output_run}"
sed -i "s/_EQUIL_MDP_FILE_/${mdp_equil}/g" "${output_run}"
sed -i "s/_PROD_MDP_FILE_/${mdp_prod}/g" "${output_run}"

sed -i "s/_INDEXFILE_/${index_file}/g" "${output_run}"
sed -i "s/_INITIALPREFIX_/${output_prefix}/g" "${output_run}"

## EDITTING SUBMISSION SCRIPT
sed -i "s/_JOB_NAME_/${output_directory}/g" "${output_submit}"
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${output_submit}"
sed -i "s/_MDRUNCOMMAND_/${mdrun_command}/g" "${output_submit}"
sed -i "s/_MDRUNCOMMANDSUFFIX_/${mdrun_command_suffix}/g" "${output_submit}"
sed -i "s/_GROMACSCOMMAND_/${gromacs_command}/g" "${output_submit}"
sed -i "s/_RUNSCRIPT_/${output_run}/g" "${output_submit}"

## DEFINING SUMMARY FILE
summary_file="transfer_summary.txt"

summary_string="
OUTPUT DIRECTORY: ${output_directory}
INPUT PARENT DIR: ${input_sim_dir_name}
INPUT SIM PATH: ${input_directory}
----------------------
SPRING CONSTANT: ${spring_constant}
PULL RATE (nm/ps): ${pull_rate}
DT: ${dt}
PULL GROUP 1: ${closest_atom_name}
PULL GROUP 2: ${lm_residue_name}
----------------------
CLOSEST ATOM INDEX: ${closest_atom_index}
CLOSEST ATOM INDEX PLUS ONE: ${closest_atom_index_plusone}
Z DISTANCE TO MEMBRANE: ${closest_dist_z}
HALF MEMBRANE THICKNESS: ${membrane_half_thickness}
PULL SIM TIME (ps): ${total_sim_time_ps}
PULL SIM STEPS: ${total_steps}
FINAL DIST BTN GRPS (nm): ${final_distance}
TOTAL DISTANCE TRAVERSED (nm): ${total_distance}
----------------------
DEBUG: ${want_debug}
MDP EQUIL STEPS: ${mdp_equil_nsteps}
MDP PROD STEPS: ${mdp_prod_nsteps}
"

echo "${summary_string}" > "${summary_file}"
cat "${summary_file}"

## ADDING TO JOB LIST
echo "${path_output_dir}/${output_submit}" >> "${JOB_SCRIPT}"

done
