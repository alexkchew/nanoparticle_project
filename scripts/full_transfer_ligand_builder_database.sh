#!/bin/bash

# full_transfer_ligand_builder_database.sh
# This script runs transfer ligand builder in large scale
# Author(s):
#   Alex K. Chew (alexkchew@gmail.com)

## !!!! BEFORE RUNNING CODE, MAKE SURE TO LOAD PYTHON
# source activate py36_mdd

### SCRIPT VARIABLES (transfer_ligand_builder.sh):
# $1: diameter
# $2: temperature
# $3: shape_type (i.e. what shape would you like?)
# $4: ligand_names: name of the ligands
# To get GNPs within file
# declare -a array_storage=(); for file in $(ls); do IFS="_" read -ra files_array <<< ${file}; echo ${files_array[3]}; array_storage+=(${files_array[3]});  done
# echo ${array_storage[@]}

### SOURCING ALL VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

### DEFINING PATH TO SCRIPTS
SCRIPT_PATH="$PATH2SCRIPTS/transfer_ligand_builder_database.sh"

## TEMPERATURE OF THE SYSTEM
temp="300.00" 
# Room temperature: 300.00
# Temperature of normal cell proliferation is 310.15

## DEFINING SHAPE TYPE
shape_type="spherical"

## DEFINING TRIAL
trial=1
# trial=2

## DEFINING TRANSFER LIGAND DIRECTORY NAME
simulation_dir="20201107-GNPs_database_water_sims-try15_gold_addition"
simulation_dir="20201111-GNPs_database_water_sims-try28_subset_ligs-retry"
simulation_dir="20201126-GNPs_database_new_24_gnps"
# simulation_dir="20201110-GNPs_database_water_sims-try27_retry_all_gnps"

## DEFINING NEW DIRECTORY
simulation_dir="20201216-GNPs_database_water_sims-try28_all_ligs_revive_failed_try2"
simulation_dir="20201216-GNPs_database_water_sims-try28_all_ligs_revive_failed_try3"

## TESTING FOR HIGH SPRING CONSTANT
simulation_dir="20210106-GNP_test_all_with_high_spring"
simulation_dir="20210106-GNP_test_all_with_high_spring_try3"
simulation_dir="20210111-Rerun_GNPs_ROTs"

## DEFINING NEW SIMULATION DIRECTORY FOR TRIAL 2
simulation_dir="20210121-Rerun_GNPs_Trial_2"

simulation_dir="20210315-Rerun_GNPs_ROT_corrected_ligand_nums"

# simulation_dir="20210111-Rerun_GNPs_corrected_mixed_ligands"
# simulation_dir="20210111-Rerun_GNPs_newGNPs"
# simulation_dir="20210111-Rerun_GNPs_newGNPs_corrected_mixed_ligands"

## DEFINING CSV FILE
path_database="/home/akchew/scratch/MDLigands/database_ligands/"
gnp_csv="logP_exp_data_GNP.csv"
ligand_csv="logP_exp_data_LIGANDS.csv"

path_database="/home/akchew/bin/pythonfiles/modules/gnpdescriptors/database"
gnp_csv="logP_cell_uptake_database_GNP.csv"
ligand_csv="logP_cell_uptake_database_LIGANDS.csv"

gnp_csv="logP_cell_uptake_zeta_potential_database_GNP.csv"
ligand_csv="logP_cell_uptake_zeta_potential_database_LIGANDS.csv"

gnp_csv="rotello_gnp_database_GNP.csv"
ligand_csv="rotello_gnp_database_LIGANDS.csv"

## USING MODIFIED FF?
modified_ff=false
# true

## DEFINING FORCE FIELD
forcefield="charmm36-jul2020.ff"
# "charmm36-mar2019.ff"

## DEFINING PATH
path_gnp_csv="${path_database}/${gnp_csv}"
path_lig_csv="${path_database}/${ligand_csv}"


## DEFINING SPECIFIC GNPS TO DEVELOP
declare -a specific_gnps=(ROT30 ROT31 ROT32 ROT33 ROT34 ROT35 ROT36 ROT37 ROT38 ROT39 ROT40 ROT41)

# ROT27 ROT28 ROT29
# ROT2 ROT3 ROT4 ROT5 ROT6 ROT7 ROT8 ROT9 ROT10 ROT11 ROT12 ROT13 ROT14 ROT15 ROT16 ROT17 ROT18 ROT19 ROT20 ROT21 ROT22 ROT23 ROT24 ROT25 ROT26
# ROT1 

# GNP165 GNP171 
# GNP18
#  GNP26 GNP68
# GNP183
# GNP175
#  GNP177
# GNP165
# GNP187
#  
# GNP110 GNP111 GNP112 GNP115 GNP116 GNP117 GNP118
# declare -a specific_gnps=(GNP124 GNP125 GNP126 GNP127 GNP128 GNP131 GNP132 GNP133 GNP135 GNP136 GNP137 GNP139 GNP140 GNP141 GNP143 GNP144 GNP145 GNP148 GNP149 GNP150 GNP157 GNP158 GNP159 GNP160 GNP161 GNP187 GNP164 GNP165 GNP166 GNP167 GNP168 GNP169 GNP170 GNP171 GNP172 GNP173 GNP174 GNP175 GNP176 GNP177 GNP178 GNP179 GNP180 GNP181 GNP182 GNP183 GNP184 GNP185 GNP186)

	# 'GNP113')
# GNP286 GNP287 GNP288
# GNP109 GNP110 GNP111 GNP112 GNP113 GNP114 GNP115 GNP116 GNP117 GNP118 GNP119 GNP281 GNP282 GNP284 GNP285 GNP286 GNP287 GNP288 GNP398
# GNP283
# GNP109 GNP110 GNP111 GNP112 GNP113 GNP114 GNP115 GNP116 GNP117 GNP118 GNP119 GNP281 GNP282 GNP284 GNP285 GNP286 GNP287 GNP288 GNP398
# 'GNP175' 'GNP183'
# 'GNP142'
# 'GNP14' 'GNP123' 'GNP130' 'GNP27' 'GNP151' 'GNP85'

## FOR ALL GNPS
# read -a specific_gnps <<< $(tail -n+2 ${path_gnp_csv} | awk -F ',' '{print $1}')

echo "Total GNPs: ${#specific_gnps[@]}"

sleep 2


## REMOVING SPECIFIC ARRAYS
delete=()
# 'GNP85' 'GNP14' 'GNP123' 'GNP130' 'GNP27' 'GNP151' 'GNP13' 'GNP15' 'GNP19' 'GNP1' 'GNP20' 'GNP3' 'GNP5' 'GNP6'
for target in "${delete[@]}"; do
  for i in "${!specific_gnps[@]}"; do
    if [[ ${specific_gnps[i]} = $target ]]; then
      unset 'specific_gnps[i]'
    fi
  done
done

## LOOPING THROUGH EACH
for each_specific_gnp in "${specific_gnps[@]}"; do
    
    ##################################
    ### EXTRACTING GNP INFORMATION ###
    ##################################

    ## LOCATING LINE NUMBER
    line_num=$(grep -nE "\b${each_specific_gnp}\b" "${path_gnp_csv}" | sed 's/\([0-9]*\).*/\1/')

    ## GETTING THE SPECIFIC LINE
    specific_line=$(sed -n -e ${line_num}p "${path_gnp_csv}")

    ## READING AS ARRAY
    IFS=', ' read -r -a specific_string_array <<< "${specific_line}"

    ## EXTRACTING DIAMETER (2 decimal places)
    diameter=$(printf "%0.2f\n" ${specific_string_array[1]})
    
    ## EXTRACTING SUBSTRUCTURE
    assembly_type_info=${specific_string_array[2]}

    ## REMOVING SPECIFIC ARRAYS (i.e. GNP1 and diameter)
    unset "specific_string_array[0]"
    unset "specific_string_array[1]"
    unset "specific_string_array[2]"

    ## REMOVING BLANKS
    for i in "${!specific_string_array[@]}"; do
      [ -n "${specific_string_array[$i]}" ] || unset "specific_string_array[$i]"
    done

    ## LOOPING THROUGH INDEX
    index=0

    ## DECLARING EMPTY ARRAY
    declare -a lig_array=()
    declare -a num_lig_array=()
    ## LOOPING THROUGH AND STORING ALL LIGANDS AND NUMBERS
    for each_value in "${specific_string_array[@]}"; do
        if [ $((index%2)) -eq 0 ];
        then
            ## STORING
            lig_array+=("${each_value}")
        else
            num_lig_array+=("${each_value}")
        fi
        ## GETTING INDEX
        index=$((${index}+1))
    done


    ########################################
    #### CHECKING IF ANY NUMBER OF LIGANDS ARE ZERO
    ## GETTING NEW ARRAY (WHICH WILL CHECK NUMBER OF LIGANDS)
    declare -a lig_array_updated=()
    declare -a num_lig_array_updated=()

    ## CHECKING IF ANY NUMBER OF LIGANDS IS ZERO. IF ZERO, THEN REMOVE THAT LIGAND COMPLETELY.
    for i in "${!num_lig_array[@]}"; do 
        ## GETTING CURRENT NUMBER
        current_num="${num_lig_array[i]}"

        if [[ "${current_num}" == "MAX" ]]; then
            echo "Max number, saving ${lig_array[i]}"
            num_lig_array_updated="None"
            ## GETTING INDEX TO REMOVE
            lig_array_updated+=("${lig_array[i]}")
            break

        else

            ## GETTING INTEGER
            current_num_int=${current_num%.*}

            echo "Ligand and number: ${lig_array[i]}, ${current_num_int}"

            if [[ "${current_num_int}" -ne "0" ]]; then

                ## GETTING MAX NUMBER ARRAY
                num_lig_array_updated+=("${current_num_int}")
                echo "Nonzero number, saving ${lig_array[i]}"
                ## GETTING INDEX TO REMOVE
                lig_array_updated+=("${lig_array[i]}")
            fi
        fi

    done

    ## RESTORING ARRAY
    lig_array=( "${lig_array_updated[@]}" )

    echo "Num lig array updated: ${num_lig_array_updated}"

    if [[ "${num_lig_array_updated}" == "None" ]]; then
        num_lig_array="None"
        total_ligs="None"
        frac_lig_array=("1.00")
    else
        num_lig_array=( "${num_lig_array_updated[@]}" )
        ## NORMALIZE LIGAND BY PERCENTAGES
        total_ligs=$(dc <<< '[+]sa[z2!>az2!>b]sb'"${num_lig_array[*]}lbxp")

        ## LOOPING AND GETTING FRACTION OF LIGANDS
        declare -a frac_lig_array=()

        ## LOOPING THROUGH EACH NUMBER
        for each_num_lig in "${num_lig_array[@]}"; do
            ## NORMALIZING USING AWK
            frac=$(awk -v num_lig=${each_num_lig} \
                       -v total_lig=${total_ligs} \
                       'BEGIN{ printf "%.2f", num_lig / total_lig }')

            ## STORING
            frac_lig_array+=("${frac}")
        done
    fi

    #####################################
    ### EXTRACTING LIGAND INFORMATION ###
    #####################################

    # ## GETTING LIGAND INFORMATION
    # first_lig=${lig_array[0]}

    # ## GETTING LIGAND LINE
    # first_lig_line=$(awk -F"," '{print $1}' ${path_lig_csv} | grep -nE "\b${first_lig}\b" | sed 's/\([0-9]*\).*/\1/')

    # ## LOCATING LIGAND FROM CSV AND EXTRACTING ASSEMBLY TYPE INFORMATION
    # assembly_type_info="$(sed -n "${first_lig_line}p" "${path_lig_csv}" | awk -F',' '{print $4}')"


## GETTING SUMMARY STRING
summary_string="
=== Working on: ${each_specific_gnp}, Line number: ${line_num} ===
--> ${specific_line}
-----> Specific string array: ${specific_string_array[@]}
-----> Diameter (nm): ${diameter}
=============
-----> Ligands: ${lig_array[@]}
-----> #Ligs: ${num_lig_array[@]}
-----> Total_ligs: ${total_ligs}
-----> Fraction_of_ligands: ${frac_lig_array[@]}
=============
-----> Assembly_type: ${assembly_type_info}
"
    ## CONVERTING STRING TO COMMAS
    lig_string_array_string=$(join_array_to_string "," ${lig_array[@]})
    lig_frac_array_string=$(join_array_to_string "," ${frac_lig_array[@]})

    ## PRINTING
    echo "${summary_string}"

    ## RUNNING MAIN CODE
    bash "${SCRIPT_PATH}" \
            "${diameter}" \
            "${temp}" \
            "${shape_type}" \
            "${lig_string_array_string}" \
            "${trial}" \
            "${simulation_dir}" \
            "${lig_frac_array_string}" \
            "${modified_ff}" \
            "${assembly_type_info}" \
            "${forcefield}" \
            "${each_specific_gnp}" \
            "${total_ligs}" \
            "${summary_string}"
    
done

