#!/bin/bash

# full_transfer_ligand_builder.sh
# This script runs transfer ligand builder in large scale
# Author(s):
#   Alex K. Chew (alexkchew@gmail.com)

### SCRIPT VARIABLES (transfer_ligand_builder.sh):
# $1: diameter
# $2: temperature
# $3: shape_type (i.e. what shape would you like?)
# $4: ligand_names: name of the ligands

### SOURCING ALL VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

### DEFINING PATH TO SCRIPTS
SCRIPT_PATH="$PATH2SCRIPTS/transfer_ligand_builder.sh"

## TEMPERATURE OF THE SYSTEM
temp="300.00" 
# Room temperature: 300.00
# Temperature of normal cell proliferation is 310.15

### DEFINING GENERAL VARIABLES
## PARTICLE SHAPE
# "spherical" "hollow" "EAM"
declare -a shape_type=("EAM")
## DIAMETER
### "2" "4" "6"
declare -a diameter=("2")
# "6"
# "2"
# "4" "6"
# "4"
# "2"
# "2"

## LIGAND NAMES
# "butanethiol" "octanethiol" "dodecanethiol" "hexadecanethiol"
# "C11NH3p" "C11COOm" "C11OH"  "octanethiol" "dodecanethiol" "hexadecanethiol" "hexanethiol" "ROT-NH3p-OH" "ROT001"
# "ROT001" "ROT002" "ROT003" "ROT004" "ROT005" "ROT006" "ROT007" "ROT008" "ROT009" "ROT010" "ROT011" "ROT012" 
declare -a ligand_names=("ROT017")
# "C11branch6NH2" "C11branch6CH3" "C11branch6COOH" "C11branch6CONH2" "C11branch6CF3"
# "C11double67NH2" "C11double67COOH" "C11double67CONH2") # "ROT008" "ROT013" "ROT014"
# "C11double67CF3"
# "C11COOH"
# "C11COO,C11COOH"
# "C11NH3,C11NH2"
# "C11branch6OH"
# "C11OH"
# "dodecanethiol"
# "dodecen-1-thiol" "C11double67OH"
# "ROTPE1" "ROTPE2" "ROTPE3" "ROTPE4"
# "dodecanethiol" "C11OH"
# "C11CF3"
# "C11NCH33" "C11CCH33"
# "ROT010"
# "ROT001" "ROT002" "ROT003" "ROT004" "ROT005" "ROT006" "ROT007" "ROT008" "ROT009" "ROT011" "ROT012" "ROT013" "ROT015" "ROT016" "C11CCH3_3" "C11NCH3_3"
# "ROT010"
# "C11NH3"
# "C11NH2" "C11CONH2" "C11OH" "C11COO"
# "ROT012" "C11COOH"
#  "C11NH2" "C11CONH2" "C11OH" "C11COOH" "C11COO" "C11NH3" "C11CF3" "C11CCH3_3" "C11NCH3_3" "ROT001" "ROT002" "ROT003" "ROT004" "ROT005" "ROT006" "ROT007" "ROT008" "ROT009" "ROT010" "ROT011" "ROT012" "ROT013" "ROT015" "ROT016"
# "ROT015" "ROT016"
# "ROTPE1" "ROTPE2" "ROTPE3" "ROTPE4"
# "C11OH" "dodecanethiol"
# "C11COO" "C11NH3"
# "C11NH2" "C11OH" "C11COOH" "dodecanethiol" "C11CONH2"
# ROTELLO LIGANDS
# "ROT001" 
# "C11NH2" "C11OH" "C11COOH" "dodecanethiol" "C11CONH2"
# "hexadecanethiol" "dodecen-1-thiol" "C11CF3"
## TRIAL NUMBER
# "1" "2" "3" "5"
declare -a trial_num=("1")

## DEFINING LIGAND FRACTION
ligand_fracs="1"
# "0.53,0.47"
# 1

## USING MODIFIED FF?
modified_ff=true

## DEFINING TRANSFER LIGAND DIRECTORY NAME
simulation_dir="ROT_WATER_SIMS_MODIFIEDFF"
# "20200416-new_unsaturated_ligands_aci"
# "20200410-mixed_sams_charged"
# "20200326-EAM_branch_ligands"
# "20200301-4nm_dod"
# "2012210-Rerun_all_EAM_models_1_cutoff"
# _6nm
# "191216-ROT_15_16"
# "191023-6nm_EAM_particles"
# "190903-Rotello_lig_cyclohexane"
# "190613-2nm_new_ligands_testing"

###############
### LOOPING ###
###############
## LOOP NOTES
### LOOP THROUGH SPHERICAL TYPES
### LOOP THROUGH DIAMETER
### LOOP THROUGH LIGAND NAMES

## LOOPING THROUGH SPHERICAL TYPES
for current_shape_type in "${shape_type[@]}"; do
    ## LOOPING THROUGH DIAMETERS
    for current_diam in "${diameter[@]}"; do
        ## LOOPING THROUGH LIGAND NAMES
        for current_lig_name in "${ligand_names[@]}"; do
            ## LOOPING THROUGH TRIAL NUMBERS
            for current_trial in "${trial_num[@]}"; do
                ### RUNNING THE SCRIPT
                echo "RUNNING THE FOLLOWING COMMAND: (pausing for 3 seconds)"
                echo "bash "${SCRIPT_PATH}" "${current_diam}" "${temp}" "${current_shape_type}" "${current_lig_name}" "${current_trial}" "${simulation_dir}""
                sleep 3
                bash "${SCRIPT_PATH}" \
                        "${current_diam}" \
                        "${temp}" \
                        "${current_shape_type}" \
                        "${current_lig_name}" \
                        "${current_trial}" \
                        "${simulation_dir}" \
                        "${ligand_fracs}" \
                        "${modified_ff}"
            done
        done
    done
done

### PRINTING SUMMARY
echo "------SUMMARY------"
echo "Created systems for ligand types: ${ligand_names[@]}"
echo "Shape type: ${shape_type[@]}"
echo "Sphere diameter: ${diameter[@]}"
echo "Temperature: ${temp}"
echo "Trial numbers: ${trial_num[@]}"
