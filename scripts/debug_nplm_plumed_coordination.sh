#!/bin/bash

# debug_nplm_plumed_coordination.sh
# This script attempts to debug the plumed coordination number code

# Written by: Alex K. chew (05/13/2020)
# 
# 

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################


## US FORWARD ROT012
input_sim_dir_name="20200120-US-sims_NPLM_rerun_stampede"
input_directory="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

## US FORWARD ROT001
# input_sim_dir_name="20200427-From_Stampede"
# input_directory="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

declare -a distance_array=("5.100")
# 2.900
# "1.300"
#  "1.900" "2.900"
# "1.300"
# "1.900"
# "1.300"
# "5.100"

## LOOPING DISTANCES
for desired_distance in ${distance_array[@]}; do

######################
### DEFINING PATHS ###
######################

## DEFINING INPUT DIRECTORY
path_input_dir="${PATH2NPLMSIMS}/${input_sim_dir_name}/${input_directory}"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING LM RESIDUE NAME
lm_residue_name="DOPC"

## DEFINING GROMACS COMMAND
gromacs_command="gmx_mpi"
# Assume that gmx_mpi version is loaded: load_gmx_2016-6_plumed_2-5-1

####################################
### DEFINING DIRECTORY STRUCTURE ###
####################################

## FOR INPUT CONFIGS INITIAL
path_input_dir_input_files="${path_input_dir}/1_input_files"
path_input_dir_configs="${path_input_dir}/2_configurations"
path_input_dir_sims="${path_input_dir}/4_simulations"


## PATH TO ITP
np_itp="sam.itp"
path_to_np_itp="${path_input_dir_input_files}/${np_itp}"

## DEFINING PATH INPUT SIMS
path_input_dir_sims="${path_input_dir_sims}/${desired_distance}"

## DEFINING PREFIX
input_prefix="nplm_prod-non-Water"
# "nplm_prod"

## DEFINING GRO FILES
config_gro="${input_prefix}.gro"
config_tpr="${input_prefix}.tpr"

echo "GRO file: ${config_gro}"
###################
### MAIN SCRIPT ###
###################

## GOING TO PATH
stop_if_does_not_exist "${path_input_dir_sims}"
cd "${path_input_dir_sims}"

## DEFINNING INDEX FILE
index_file="coord.ndx"
np_lig_name="NP_LIGANDS"
lm_grp_name="TAILGRPS"

## GETTING RESIDUE NAME
lig_resname="$(itp_get_residue_name_within_atom ${path_to_np_itp})"
echo "Ligand residue name: ${lig_resname}"

## CREATING INDEX FILE
if [[ ! -e "${index_file}" ]]; then
	nplm_generate_index_file "${gromacs_command}" \
							 "${config_gro}" \
							 "${index_file}" \
							 "${lig_resname}" \
							 "${np_lig_name}" \
						 	 "${lm_residue_name}" \
						 	 "${lm_grp_name}"
fi

## CREATING INDEX FILE FOR TAIL GROUPS THAT ARE ABOVE A Z-CUTOFF
python3.6 "${PYTHON_GET_LM_TOP_BOT_INDEX}" --path_to_sim "${path_input_dir_sims}" \
										   --gro_file "${config_gro}" \
										   --index_file "${index_file}" \
										   --lm_residue_name "${lm_residue_name}"

sleep 3
##############
### PLUMED ###
##############

## DEFINING RADIUS
radius=0.5

## DEFINING DEFAULT
nn=6
mm=12
d0=0.0

## GETTING ARRAY OF RADIUS
# declare -a r_0_array=("0.1" "0.2" "0.3" "0.4" "0.5")

## DEFINING WHICH ARRAY TO VARY
vary_type="D_MAX"
# vary_type="R_0"
# 
## FOR VARYING INDEX
vary_type="INDEX"

#########################
### VARYING R_0 ARRAY ###
#########################

if [[ "${vary_type}" == "R_0" ]]; then

## DEFINING PLUMED
plumed_dat="plumed_vary_R_0.dat"
covar_dat="COVAR_R_0.dat"

## GENERATING SEQUENCE -- FIRST, STEP, LAST
r_0_array=($(seq 0.05 0.05 0.5))

coord_prefix=" COORDINATION GROUPA=coms1 GROUPB=coms2"
coord_string=""

## DEFINING INDEX
index=0

## LOOPING AND GETTING DIFFERENT VALUES
for each_r_0 in ${r_0_array[@]}; do
	new_coord_string=$"r_${index}:${coord_prefix} SWITCH={RATIONAL NN=${nn} MM=${mm} D_0=${d0} R_0=${each_r_0}}
"
	## ADDING
	coord_string+="${new_coord_string}"
	index=$((${index}+1))
done


fi
###########################
### VARYING D_MAX ARRAY ###
###########################
if [[ "${vary_type}" == "D_MAX" ]]; then

plumed_dat="plumed_vary_D_MAX.dat"
covar_dat="COVAR_D_MAX.dat"

## DEFINING D MAX
d_max_array=($(seq 0 1 8))
r_0="0.35"

coord_prefix=" COORDINATION GROUPA=coms1 GROUPB=coms2"

## AT THE BEGINNING -- NO DMAX
coord_string=$"ref:${coord_prefix} SWITCH={RATIONAL NN=${nn} MM=${mm} D_0=${d0} R_0=${r_0}}
"

## DEFINING INDEX
index=0

## LOOPING AND GETTING DIFFERENT VALUES
for each_dmax in ${d_max_array[@]}; do
	new_coord_string=$"r_${index}:${coord_prefix} SWITCH={RATIONAL NN=${nn} MM=${mm} D_0=${d0} R_0=${r_0} D_MAX=${each_dmax}}
"
	## ADDING
	coord_string+="${new_coord_string}"
	index=$((${index}+1))
done


fi

############################
### VARYING INDEX GROUPS ###
############################

if [[ "${vary_type}" == "INDEX" ]]; then
	## VARYING THE DAT FILE
	plumed_dat="plumed_vary_INDEX.dat"
	covar_dat="COVAR_INDEX.dat"
	## DEFINING DMAX AND R_0
	r_0="0.35"
	d_max="4"

	## DEFINING NEW GROUP NAMES
	new_lm_grp="TOP_TAILGRPS"

	coord_string="
		orig_coord: COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL NN=${nn} MM=${mm} D_0=${d0} R_0=${r_0} D_MAX=${d_max}}

		## DEFINING NEW GROUP
		new2: GROUP NDX_FILE=${index_file} NDX_GROUP=${new_lm_grp}
		new_coord: COORDINATION GROUPA=coms1 GROUPB=new2 SWITCH={RATIONAL NN=${nn} MM=${mm} D_0=${d0} R_0=${r_0} D_MAX=${d_max}}

	"

fi


string="
# DEFINING UNITS
UNITS LENGTH=nm ENERGY=kj/mol TIME=ps

# NP GROUP (LIGANDS)
coms1: GROUP NDX_FILE=${index_file} NDX_GROUP=${np_lig_name}

# LIPID MEMBRANE GROUPS (TAIL GROUPS ONLY)
coms2: GROUP NDX_FILE=${index_file} NDX_GROUP=${lm_grp_name}

# TESTING DIFFERENT COORDINATION NEIGHBOR LISTS
${coord_string}

PRINT ...
    STRIDE=100
    ARG=*
    FILE=${covar_dat}
... PRINT

ENDPLUMED" 

echo "${string}" > "${plumed_dat}"

## RUNNING PLUMED
plumed driver --mf_xtc "${input_prefix}.xtc" --plumed "${plumed_dat}"


done

# cn0: COORDINATION GROUPA=coms1 GROUPB=coms2 R_0=${radius}
# test0: COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL R_0=${radius} NOSTRETCH}
# test1: COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL R_0=${radius} D_MAX=3.0 NOSTRETCH}
# test2: COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL R_0=${radius} STRETCH}
# test3: COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL R_0=${radius} D_MAX=8.0}
# test4: COORDINATION GROUPA=coms1 GROUPB=coms2 SWITCH={RATIONAL R_0=${radius} D_MAX=50}
# D_MAX=3 
# GROUPA=coms1 GROUPB=coms2
# D_MAX=3