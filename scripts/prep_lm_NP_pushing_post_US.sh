#!/bin/bash

# prep_lm_NP_pushing_post_US.sh
# The purpose of this code is to take the last configuration of a 
# fully equilibrated system and push the nanoparticle away from the 
# lipid bilayer. The idea is to generate configurations that are of 
# pulling simulations of NP from lipid bilayer. 

# Written by: Alex K. Chew (alexkchew@gmail.com, 01/23/2020)



#################################################
### LOADING PRE-DEFINED VARIABLES / FUNCTIONS ###
#################################################
source "../bin/nanoparticle_rc.sh"
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING INPUT SIMULATION DIR
input_sim_dir_name="20200120-US-sims_NPLM_rerun_stampede"

## DEFINING OUTPUT DIRECTORY NAME
output_sim_dir_name="20200123-Pulling_sims_from_US"

## DEFINING INPUT SIMULATION NAME
input_specific_sim_name="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
input_specific_sim_name="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

## EXTRACTING 
read -a extract_array <<< $(extract_us_np_lipid_membrane_name ${input_specific_sim_name})

## EXTRACTING LIGAND NAME
ligand_name="${extract_array[6]}"
## DEFINING PULL CONSTANT
spring_constant="${extract_array[8]}" # kJ/mol/nm2, as defined by previous papers
## DEFINING LIPID MEMBRANE NAME
lm_name="${extract_array[3]}"
lm_size="${extract_array[9]}"

## DEFINING NANOPARTICLE DETAILS
np_size="${extract_array[10]}"
np_shape="${extract_array[11]}"
np_trial="${extract_array[12]}"

## DEFINING PULL RATE
pull_rate="${extract_array[7]}" # nm / ps -- PRODUCTION

## DEFINING DESIRED DIRECTORY
desired_dir="1.300"

## DEFINING FINAL DISTANCE
final_distance="5.000"

## DEFINING TEMPERATURE
temperature="300.00"

## DEFINING MAXWARN
num_max_warn="5"

#########################
### DEFAULT VARIABLES ###
#########################

## RUN INFORMATION
num_cores="28"

## DEFINING MD RUN COMMAND
mdrun_command="gmx mdrun -nt"

## DEFINING SUFFIX
mdrun_command_suffix=""

## GROUPS
pull_group1_name="AUNP"
pull_group2_name="DOPC"

## DEFINING INPUT PREFIX
input_sim_prefix="nplm_prod"

## DEFINING INPUT SIM TOP
input_sim_top="nplm.top"

## SIMULATION DIRECTORY FOR US
us_sim_dir="4_simulations"
us_input_dir="1_input_files"

## DEFINING PATH TO SIMULATION
path_input_sim="${PATH2SIM}/${input_sim_dir_name}/${input_specific_sim_name}/${us_sim_dir}/${desired_dir}"

## DEFINING INPUT 
path_input_molecules="${PATH2SIM}/${input_sim_dir_name}/${input_specific_sim_name}/${us_input_dir}"

## GETTING OUTPUT NAME
output_name=$(get_np_lm_pulling_name "${desired_dir}" \
                                     "${final_distance}"\
                                     "${pull_rate}" \
                                     "${spring_constant}" \
                                     "${lm_name}" \
                                     "${lm_size}" \
                                     "${np_shape}" \
                                     "${np_size}" \
                                     "${ligand_name}" \
                                     "${np_trial}" \
                                     )
                                     
## PATH TO OUTPUT SIM
path_output_sim="${PATH2SIM}/${output_sim_dir_name}/${output_name}"

## DEFINING OUTPUT PREFIX
output_prefix="nplm"

############################
### MDP FILE INFORMATION ###
############################

## ENERGY MINIMIZATION
push_mdp="pushing.mdp"

## DEFINING TIME STEP
time_step="0.002"

## MDP FILE INFORMATION
mdp_parent_path="${INPUT_MDP_PATH}/charmm36"

## MDP FILE PATH
mdp_file_path="${mdp_parent_path}/lipid_bilayers"

############################
### RUN FILE INFORMATION ###
############################

## DEFINING RUN FILE
input_run_file="run_nplm_pushing_post_us.sh"
input_submit_file="submit_run_nplm_pushing_post_us.sh"
input_run_path="${PATH2SUBMISSION}/${input_run_file}"
input_submit_path="${PATH2SUBMISSION}/${input_submit_file}"

## DEFINING OUTPUT RUN
output_run_file="run.sh"
output_submit_name="submit.sh"

###################
### MAIN SCRIPT ###
###################

## CHECKING IF INPUT SIMULATION EXISTS
stop_if_does_not_exist "${path_input_sim}"

## CREATING DIRECTORY
create_dir "${path_output_sim}" -f

## GOING INTO DIRECTORY
cd "${path_output_sim}"

## COPYING GRO FILE
cp "${path_input_sim}/${input_sim_prefix}.gro" "${path_output_sim}/${output_prefix}.gro"

## COPYING FORCE FIELD PARAMETERS
cp -r "${path_input_molecules}"/* "${path_output_sim}"

# REMOVING MDP FOLDER
rm -rf "mdp_files"

## COPYING MDP FILE
cp -r "${mdp_file_path}/"${push_mdp} "${path_output_sim}"

## CALCULATING PULL DISTANCE
pull_distance=$(awk -v full_dist=${final_distance} \
                    -v desired_dist=${desired_dir} \
                    'BEGIN{ printf "%.5f", full_dist - desired_dist}')

## DEFINING MDP OPTIONS
total_time_pull_ps=$(awk -v rate=${pull_rate} \
                         -v desired_dist=${pull_distance} \
                         'BEGIN{ printf "%.5f", desired_dist / rate }') # Time in ps
## DEFINING TOTAL NUMBER OF PULL STEPS
total_time_pull_steps=$(awk -v total_time=${total_time_pull_ps} \
                            -v delta_t=${time_step} \
                            'BEGIN{ printf "%d", total_time / delta_t }')

## EDITING MDP FILE
sed -i "s/_DT_/${time_step}/g" "${push_mdp}" 
sed -i "s/_GROUP_1_NAME_/${pull_group1_name}/g" "${push_mdp}"
sed -i "s/_GROUP_2_NAME_/${pull_group2_name}/g" "${push_mdp}"
sed -i "s/_NSTEPS_/${total_time_pull_steps}/g" "${push_mdp}"
sed -i "s/_TEMPERATURE_/${temperature}/g" "${push_mdp}"
sed -i "s/_PULLRATE_/${pull_rate}/g" "${push_mdp}"
sed -i "s/_PULLKCONSTANT_/${spring_constant}/g" "${push_mdp}"

## GENERATING TPR FILE
gmx grompp -f "${push_mdp}" -c "${output_prefix}.gro" \
                            -p ${input_sim_top} \
                            -o ${output_prefix}_push.tpr \
                            -maxwarn "${num_max_warn}"
                            
########################
### COPYING RUN FILE ###
########################

## COPY RUN FILE
cp -r "${input_run_path}" "${output_run_file}"

## EDITING RUN FILE
sed -i "s/_OUTPUTPREFIX_/${output_prefix}_push/g" "${output_run_file}"

## COPYING SUBMIT FILE
cp -r "${input_submit_path}" "${output_submit_name}"

## EDITING SUBMIT FILE
sed -i "s/_JOB_NAME_/${output_name}/g" "${output_submit_name}"
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${output_submit_name}"
sed -i "s/_MDRUNCOMMAND_/${mdrun_command}/g" "${output_submit_name}"
sed -i "s/_MDRUNCOMMANDSUFFIX_/${mdrun_command_suffix}/g" "${output_submit_name}"
sed -i "s/_RUNSCRIPT_/${output_run_file}/g" "${output_submit_name}"

## ADDING TO JOB LIST
echo "${path_output_sim}/${output_submit_name}" >> "${JOB_SCRIPT}"
echo "Adding path to job list:"
echo "${path_output_sim}/${output_submit_name}"

## CREATING A SUMMARY FILE
summary_file="transfer_summary.txt"
echo "Input directory: ${input_sim_dir_name}" > "${summary_file}"
echo "Specific simulation: ${input_specific_sim_name}" >> "${summary_file}"
echo "Temperature: ${temperature}" >> "${summary_file}"
echo "Desired initial distance: ${desired_dir}" >> "${summary_file}" 
echo "Path to input simulation: ${path_input_sim}" >> "${summary_file}"
echo "Path to output simulation: ${path_output_sim}" >> "${summary_file}" 
echo "Output name: ${output_name}" >> "${summary_file}" 
echo "Output prefix: ${output_prefix}_push" >> "${summary_file}"
echo "---------------------------" >> "${summary_file}"
echo "Total pull distance (nm): ${pull_distance}" >> "${summary_file}"
echo "Total pull time (ps): ${total_time_pull_ps}" >> "${summary_file}" 
echo "Total steps: ${total_time_pull_steps}" >> "${summary_file}"

## PRINTING SUMMARY FILE
cat "${summary_file}"
