#!/bin/bash 
# SWARM Re-Submit Script (reset_job.sh)

#SBATCH -p compute
#SBATCH -t WALLTIME
#SBATCH --nodes=1
#SBATCH --ntasks=NUMBEROFCPUS        # total number of "tasks" (cores) requested
#SBATCH --ntasks-per-node=NUMBEROFCPUS              # total number of mpi tasks requested
#SBATCH --mail-user=NETID@wisc.edu
#SBATCH --mail-type=all  # email me when the job starts
#SBATCH -J JOB_NAME

# EXECUTABLE COMMANDS
# Number of threads
NumberOfThreads=NUMBEROFCPUS

## INPUT FILES ##
gro_file="sam.gro"
input_top="sam.top"
output_prefix="sam"

## MDP FILES ##
em_mdp="minim_sam_gmx5.mdp"
equil_mdp="npt_double_equil_gmx5_charmm36.mdp"
prod_mdp="npt_double_prod_gmx5_charmm36.mdp"

# Energy minimization
gmx grompp -f ${em_mdp} -c ${gro_file} -p ${input_top} -o ${output_prefix}_em -maxwarn 5
gmx mdrun -v -ntmpi 1 -ntomp ${NumberOfThreads} -deffnm ${output_prefix}_em

# Equilibration
gmx grompp -f ${equil_mdp} -c ${output_prefix}_em -p ${input_top} -o ${output_prefix}_equil -maxwarn 5
gmx mdrun -v -ntmpi 1 -ntomp ${NumberOfThreads} -deffnm ${output_prefix}_equil

# Production Runs
gmx grompp -f ${prod_mdp} -c ${output_prefix}_equil.gro -p ${input_top} -o ${output_prefix}_prod -maxwarn 5

# Checking PRODUCTION time
gmx convert-tpr -s ${output_prefix}_prod.tpr -until 50000.000 -o ${output_prefix}_prod.tpr

# Running production runs
gmx mdrun -v -ntmpi 1 -ntomp ${NumberOfThreads} -deffnm ${output_prefix}_prod

