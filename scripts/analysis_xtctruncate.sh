#!/bin/bash

# analysis_xtctruncate.sh
# This bash script will take the xtc files and truncate all of them given a xtc file, tpr file, etc. The point of this is to slice out the data that you don't need so you can spend more time on the data you do need.
# Written by Alex Chew (04/10/2017)

## ** Updates ** ##
# 2017-04-14: Made array for quick extraction -- For looping
# 2017-06-29: Added functions to add pdb files, indexing, etc.
# 2017-07-31: Added arrays for extraction directories

# Usage: bash analysisTools_xtcTruncate.sh True True True False OUTPUT_DIR True
## VARIABLES:
#   $1: wantXTC: "True" if you want XTC file
#   $2: wantNDX: "True" if you want an index file
#   $3: wantPDB: "True" if you want a PDB file
#   $4: wantSplit: "True" if you want to split xtcs to multiple chunks
#   $5: output directory (analysis dir)
#   $6: want_self_assembled_monolayer: True if you want to swtich to self_assembly directory

## IF YOU NEED TO CREATE PDB:
#gmx trjconv -s mixed_solv_prod.tpr -f mixed_solv_prod.gro -pbc whole -o mixed_solv_prod_structure.pdb

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="../bin/nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${script_dir}/${global_file_name}"
# -- USER-SPECIFIC PARAMETERS -- #

# Printing
echo -e "----- Running analysis_xtctruncate.sh ----- \n"

# Defining desires
wantXTC="$1" # True if you want XTC file
wantNDX="$2" # True if you want index file
wantPDB="$3" # True if you want pdb file
wantSplit="$4" # True if you want the PDB file to be split in multiple chunks

# Deletion of original xtc file (saving space)
wantOrigXTC="True" # False if you want the original XTC file deleted
want_self_assembled_monolayer="$6" # True if you want to change simulation directory to that of the self-assembled monolayer

# Defining split definitions
totalTime="2000" # picoseconds
chunkSize="100" # picoseconds
split_dir="${totalTime}_ps_${chunkSize}_chunk"
finalTime="5000" # Total time in a single trajectory <-- used as a way of figuring out how much time is required for each interval

# Defining extraction directory
#declare -a extractDirectories=("${Main_Extract_Dir}")
declare -a extractDirectories=("$5") # 

# Defining .gro, .xtc, and .tpr files
if [ "${want_self_assembled_monolayer}" == "True" ]; then
    gro_file="gold_ligand_equil.gro" # Not really necesary
    xtc_file="gold_ligand_equil.xtc" # "mixed_solv_prod_10_ns_whole.xtc" # "mixed_solv_prod.xtc"
    tpr_file="gold_ligand_equil.tpr"
    xtc_trunc_file="gold_ligand_equil_whole.xtc" # 
    timeOfCut="0" # "10000" # in ps <-- Means 90000 and onward to 100000 ps = 10 ns 40000 #10000
    end_time="50000" # 0 if you want all the way
else
    gro_file="sam_prod.gro" # Not really necesary
    xtc_file="sam_prod.xtc" # "mixed_solv_prod_10_ns_whole.xtc" # "mixed_solv_prod.xtc"
    tpr_file="sam_prod.tpr"
    xtc_trunc_file="sam_prod_10_ns_whole.xtc" # "mixed_solv_prod_final_10ns_testing.xtc" # "mixed_solv_prod_10_ns_whole.xtc" # 90 ns extraction
    timeOfCut="10000" # "10000" # in ps <-- Means 90000 and onward to 100000 ps = 10 ns 40000 #10000
    end_time="50000" # 0 if you want all the way
fi

## Defining OUTPUT Files
# Defining truncated .xtc file
ndx_file="index.ndx" # Index file

pdb_structure_file=${gro_file%.gro}_structure.pdb

# NDX File parameters
ResidueName="LGA" # Residue "tBuOH" "XYL"
SolventName="SOL" # Water
CosolventName="GVLL" # | cut -d '_' -f 9- <-- could get you the name of the cosolvent

# Re-defining indexes
Res_Water_Index="r ${ResidueName} | r ${SolventName}"
Not_Cosolvent_Index="! r ${CosolventName}"

# Creating callback names
Res_Water_Index_Callback="$(echo ${ResidueName}_${SolventName} | tr [a-z] [A-Z])" # Callback
Not_Cosolvent_Index_Callback="!$(echo ${CosolventName} | tr [a-z] [A-Z])" # Callback


# Defining truncated time in fs. Should be the time onward
#timeOfCut="3000" # in ps <-- Means 90000 and onward to 100000 ps = 10 ns 40000 #10000

Truncate_selection="System" #"${Res_Water_Index_Callback}" # "System"

# Defining parameters for extraction
production_file_start=""

# -- USER-SPECIFIC PARAMETERS -- #

for extractDir in "${extractDirectories[@]}"; do

    # Defining path to extraction
    path2Extraction="${PATH2ANALYSIS}/${extractDir}"

    # Defining directories
    directories=$(echo ${path2Extraction}/${production_file_start}*)

    # First, we need to get all the data files we need - Done by straight copying
    for currentSeq in $directories; do

        echo -e "Working on: $currentSeq \n"

        # Go to the directory
        cd "$currentSeq"

    # Creating index file first
    if [ "${wantNDX}" == "True" ]; then
    # Creating index file
gmx make_ndx -f ${tpr_file} -o ${ndx_file} << INPUT
${Res_Water_Index}
q
INPUT
    fi


    # Then, use gmx trjconv to extract xtc files
    if [[ "${wantXTC}" == "True" ]]; then
        
        # Check if index file is even there
        if [[ "${wantNDX}" != "True" ]]; then
        
gmx trjconv -f ${xtc_file} -o ${xtc_trunc_file} -s ${tpr_file} -b $timeOfCut -e ${end_time} -pbc whole << INPUT
${Truncate_selection}
INPUT
        else
        # Use the Index file
gmx trjconv -f ${xtc_file} -o ${xtc_trunc_file} -s ${tpr_file} -n ${ndx_file} -b $timeOfCut -e ${end_time} -pbc whole  << INPUT
${Truncate_selection}
INPUT
        fi
    fi
    
    # See if you want to delete the original XTC file
    if [[ "${wantOrigXTC}" == "False" ]]; then
        # Checking if file exists
        if [[ -e "${xtc_trunc_file}" ]]; then
            echo "${xtc_trunc_file} exists, so we are deleting the original: ${xtc_file}"
            echo "This was done to save disk space"
            rm -r ${xtc_file}
        fi
    fi
    


    if [ "${wantNDX}" == "True" ]; then
        # Check if index file exists
        if [[ ! -e "${ndx_file}" ]]; then
            echo "Error, no index file in ${currentSeq}"
            echo "Stopping here!"
            exit
        fi    

    # Then, create a PDB file if desired
    if [ "${wantPDB}" == "True" ]; then
gmx trjconv -s ${tpr_file} -f ${gro_file} -n ${ndx_file} -o ${pdb_structure_file} -pbc whole << INPUT
${Truncate_selection}
INPUT
    fi
    
    else # No index file
        # Then, create a PDB file if desired
    if [ "${wantPDB}" == "True" ]; then
gmx trjconv -s ${tpr_file} -f ${gro_file} -o ${pdb_structure_file} -pbc whole << INPUT
${Truncate_selection}
INPUT
    fi
    
    
    fi
    
    
    ## SPLITTING PDB FILES ##
    if [ "${wantSplit}" == "True" ]; then
      # Calculating number of splits required
      numSplits=$(awk -v total_time=${totalTime} -v chunk=${chunkSize} 'BEGIN{ printf "%d",total_time/chunk}')
      echo "Number of splits: $numSplits"
      
      # Now, creating a directory
      mkdir -p ${split_dir}
      
      # Going through a for-loop for each split
      for currentSplit in $(seq 1 ${numSplits}); do
        # Calculate new split trajectory name
          splitValue=$(awk -v current_split=${currentSplit} -v chunk=${chunkSize} 'BEGIN{ printf "%d",current_split*chunk}')
          
        # Defining new name for pdb file
        split_file_name="mixed_solv_prod_${splitValue}"
        echo $split_file_name
        
        # Calculating initial and final time frames
        split_initial_time=$(awk -v current_split=${currentSplit} -v final_time=${finalTime} -v chunk=${chunkSize} 'BEGIN{ printf "%d",final_time-current_split*chunk}')
        split_final_time=$(awk -v current_split=${currentSplit} -v final_time=${finalTime} -v chunk=${chunkSize} 'BEGIN{ printf "%d",final_time-(current_split-1)*chunk}')
        
        
# Now, running gmx trjconv to get a new pdb file
gmx trjconv -s ${tpr_file} -f ${xtc_file} -b ${split_initial_time} -e ${split_final_time} -n ${ndx_file} -o ${split_dir}/${split_file_name}.pdb << INPUT
${Res_Water_Index_Callback}
q
INPUT
      
      done
      
      
    fi

    done
    
done

## Printing what we have done ##
echo "----------SUMMARY----------"
echo "Directories: ${extractDirectories[@]}"
echo "Truncated xtc files for: $path2Extraction"
echo "Trunction cutoff step: $timeOfCut fs"

