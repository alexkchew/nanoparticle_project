#!/bin/bash

# gnp_descriptors_hydration_sims.sh
# This script generates hydration map simulations. Then, we will 
# perform a down-stream analysis on the hydration maps.
# 
# This code uses the "prep_most_likely_np.sh" as a way of generating simulations
#
# Written by: Alex K. Chew (11/25/2020)
# 
# ALGORITHM:
#	- Extract GNP configuration (typically we run a most likely config, but skipping that part)
#	- Copy topology and force field
#	- Create MDP file
#	- Create index file
#	- Add freeze groups
#	- Create submission script


#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"


#######################
### INPUT VARIABLES ###
#######################

## INPUT PATH TO SIM
input_sim_path="${1-/home/akchew/scratch/nanoparticle_project/simulations/20201111-GNPs_database_water_sims-try28_subset_ligs-retry/spherical_300.00_K_GNP14_CHARMM36jul2020_Trial_1}"

## DEFINING REWRITE
rewrite=${2-false}
# true if you want to rewrite directory

## DEFINING DESIRED TRAJECTORY TIME IN PS
extract_traj_time_ps="20000"

## DEFINING TEMPERATURE
temp="300" # default

########################
### OUTPUT VARIABLES ###
########################

## DEFINING OUTPUT SIM
output_dir="hydrationmap-${extract_traj_time_ps}"

## DEFINING OUTPUT PATH
output_path="${input_sim_path}/${output_dir}"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING PYTHON CODE TO GET INDICES
py_get_gnp_ligand_index="${GNPDESCRIPTOR}/prep/get_GNP_ligand_indices.py"

## DEFINING GOLD RESIDUE NAME
gold_residue_name="AUNP"
output_lig_name="LIGANDS"

## DEFINING SPRING CONSTANTS
gold_posre="gold_posre.itp"
gold_spring_constant="500000" # to be consistent with the planar SAM case
## LIGAND SPRING CONSTANT
lig_spring_constant="50"
lig_posre="lig_posre.itp"

## DEFINING INPUT FILES
input_prefix="sam_prod"

## DEFINING INPUT TOP
input_top="sam.top"

## DEFINING ITP FILE FOR SAM
np_itp_file="sam.itp"

## DEFINING OUTPUT PREFIX
output_prefix="sam"

## DEFINING OUTPUT INDEX
output_ndx="gold_ligand.ndx"

## DEFINING NUMBER OF CORES
num_cores="28"
num_em_cores="10"

## GROMPP OPTIONS
max_warn_index="5" # Number of times for maximum warnings

#########################
### DEFINING MDP FILE ###
#########################

### MDP FILE INFORMATION
mdp_parent_path="${INPUT_MDP_PATH}/charmm36/Spherical"

## ENERGY MINIMIIZATION
em_mdp_file="minim_sam_gmx5.mdp"

## DEFINING PRODUCTION MDP FILE
prod_mdp_file_nsteps="2500000" # 5 ns
# "10000000" # 20 ns
prod_mdp_file="nvt_double_prod_gmx5_charmm36_frozen_gold.mdp"
# "npt_double_prod_gmx5_charmm36_frozen_gold.mdp"

## DEFINING SUBMISSION FILE INFORMATION
submit_parent_path="${PATH2SUBMISSION}"
submit_file_name="submit_hydration_sims.sh"

## DEFINING OUTPUT SUBMIT NAME
output_submit_name="submit.sh"

#################
### MAIN CODE ###
#################

## CHECKING IF INPUT PATH EXISTS
stop_if_does_not_exist "${input_sim_path}"

## DEFINING OUTPUT GRO
path_input_sim_gro="${input_sim_path}/${input_prefix}.gro"

## STARTING MAIN CODE
if [[ -e "${path_input_sim_gro}" ]]; then
if [[ ${rewrite} == true ]] || [[ ! -e "${output_path}" ]]; then
    ## CREATING DIRECTORY
    create_dir "${output_path}" -f

    ## DEFINING PATH OUTPUT INDEX
    path_output_idx="${output_path}/${output_ndx}"

    ## DEFINING NAME
    lig_heavy_atom_name="LIG_HEAVY_ATOMS"

    ## GOING TO OUTPUT DIRECTORY
    cd "${output_path}"

    ##########################
    ### PART 1: EXTRACTION ###
    ##########################

    # In this part, we need indices of gold and ligands separately
    python "${py_get_gnp_ligand_index}" --path_to_sim "${input_sim_path}" \
                                        --input_prefix "${input_prefix}" \
                                        --output_idx_file "${path_output_idx}" \
                                        --output_lig_name "${output_lig_name}"


    ## ADDING INDICES
    last_index=$(index_compute_last_index_ "${path_output_idx}")
    last_index_plus_one=$((${last_index}+1))

## ADDING INDICES
gmx make_ndx -n "${path_output_idx}" \
			 -f "${path_input_sim_gro}" \
			 -o "${path_output_idx}" >/dev/null 2>&1 << INPUTS
"${output_lig_name}" & !a H*
name ${last_index_plus_one} ${lig_heavy_atom_name}
q
INPUTS


    ## CREATING FILE
    echo "Creating ${output_prefix}_initial.gro..."

    ## GETTING TIME BEFORE
    time_before=$(awk -v output_time=${extract_traj_time_ps} 'BEGIN{ printf "%.3f", output_time - 1000.000}')

    ## RUNNING TRJCONV
gmx trjconv -f "${input_sim_path}/${input_prefix}.xtc" \
            -s "${input_sim_path}/${input_prefix}.tpr" \
            -o "${output_path}/${output_prefix}_initial.gro" \
            -n "${path_output_idx}" \
            -b "${time_before}" \
            -dump "${extract_traj_time_ps}" -pbc mol -center >/dev/null 2>&1 << INPUTS
${gold_residue_name}
System
INPUTS
    
	##################################
	### PART 2: COPYING OVER FILES ###
	##################################
    echo "--- COPYING OVER FORCE FIELD FILES, ETC. ---"
    ## TOPOLOGY
    copy_top_include_files "${input_sim_path}/${input_top}" "${output_path}"
    cp -r "${input_sim_path}/${input_top}" "${output_path}"

    ## MDP FILE
    cp -r "${mdp_parent_path}/"{${em_mdp_file},${prod_mdp_file}} "${output_path}"

	# SUBMISSION FILE
	cp -r "${submit_parent_path}/${submit_file_name}" "${output_path}/${output_submit_name}"

    ##########################################
    ### PART 3: ADDING POSITION RESTRAINTS ###
	##########################################
	echo "--- ADDING POSITION RESTRAINTS ---"

## GENERATING POSITION RESTRAINTS FOR GOLD
gmx genrestr -f "${output_prefix}_initial.gro" \
             -o "${gold_posre}" \
             -n "${output_ndx}" \
             -fc "${gold_spring_constant}" \
                 "${gold_spring_constant}" \
                 "${gold_spring_constant}" >/dev/null 2>&1 << INPUTS
${gold_residue_name}
INPUTS

## GENERATING POSITION RESTRAINTS FOR LIGAND
gmx genrestr -f "${output_prefix}_initial.gro" \
             -o "${lig_posre}" \
             -n "${output_ndx}" \
             -fc "${lig_spring_constant}" \
                 "${lig_spring_constant}" \
                 "${lig_spring_constant}" >/dev/null 2>&1 << INPUTS
${lig_heavy_atom_name}
INPUTS

	## ADDING POSITION RESTRAINTS TO TOPOLOGY FILE
	topology_add_include_specific "${input_top}" "${np_itp_file}" "#include \"${gold_posre}\""
	topology_add_include_specific "${input_top}" "${np_itp_file}" "#include \"${lig_posre}\""
	
    ##########################################
    #### CREATING ENERGY MINIMIZATION FILE ###
    ##########################################
    echo "*** Creating tpr file ***"
    gmx grompp -f "${em_mdp_file}" \
               -c "${output_prefix}_initial.gro" \
               -p "${input_top}" \
               -o "${output_prefix}_em" -maxwarn ${max_warn_index}
    ## ENERGY MINIMIZING
    gmx mdrun -nt ${num_em_cores} -v -deffnm "${output_prefix}_em" 

	########################################
	### PART 4: MDP AND SUBMISSION FILES ###
	########################################

    echo -e "--- EDITING MDP AND SUBMISSION FILES ---"

    ## COMMENTING OUT FREEZE GROUPS
    mdp_comment_out_specific_flags "${prod_mdp_file}" "freezegrps"
    mdp_comment_out_specific_flags "${prod_mdp_file}" "freezedim"

    ## DEFINING JOB NAME
    job_name="${output_dir}_$(basename ${input_sim_path})"

	## MDP FILES ( PRODUCTION )
	sed -i "s/NSTEPS/${prod_mdp_file_nsteps}/g" ${prod_mdp_file}
	sed -i "s/TEMPERATURE/${temp}/g" ${prod_mdp_file}

    ## SUBMISSION FILE
    sed -i "s/_JOB_NAME_/${job_name}/g" ${output_submit_name}
    sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" ${output_submit_name}
    sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" ${output_submit_name}
    sed -i "s/_EM_PREFIX_/${output_prefix}_em/g" ${output_submit_name}
    sed -i "s/_MDPPROD_/${prod_mdp_file}/g" ${output_submit_name}
    sed -i "s/_TOPFILE_/${input_top}/g" ${output_submit_name}
    sed -i "s/_INDEXFILE_/${output_ndx}/g" ${output_submit_name}
	sed -i "s/_MAXWARN_/${max_warn_index}/g" ${output_submit_name}

    ## ADDING TO JOB LIST
    echo "${output_path}/${output_submit_name}" >> "${JOB_SCRIPT}"


else
    echo "${output_path} exists"


fi


fi





