#!/bin/bash

# prep_lm_NP_umbrella_sampling.sh
# The purpose of this code is to prepare lipid membrane and nanoparticles 
# umbrella sampling simulations. 

# Written by: Alex K. Chew (alexkchew@gmail.com, 01/15/2020)

## INPUTS:
#   - folder for np-lipid membrane pulling simulations
#   - increment size
#   - minima position
#   - maxima position

# OUTPUTS:
#   - folder with umbrella sampling simulations

## ALGORITHM:
#   - Copy over topology information
#   - Generate submission files
#   - Generate MDP files
#
## SWITCH SUBMIT CODE
#  bash switch_submit.sh 20200116-US_sims_NPLM change_header_multiple

#################################################
### LOADING PRE-DEFINED VARIABLES / FUNCTIONS ###
#################################################
source "../bin/nanoparticle_rc.sh"
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## DEFINING INPUT SIMULATION DIR (PULLING SIMS)
input_sim_dir_name="20200123-Pulling_sims_from_US"
input_sim_dir_name="20200608-Pulling_other_ligs"
input_sim_dir_name="20200816-modified_ff_pulling"

# "20200113-NP_lipid_bilayer_resize"

## DEFINING OUTPUT SIM DIRECTORY
output_sim_dir_name="20200207-US-sims_NPLM_reverse_ROT001"
output_sim_dir_name="20200613-US_otherligs_z-com"
output_sim_dir_name="20200818-Bn_US_modifiedFF"
# "20200120-US-sims_NPLM_rerun_stampede"
# "20200116-US_sims_NPLM_with_equil"

## DEFINING IF YOU WNAT MODIFIED FORCE FIELD
modified_ff="true"

## DEFINING IF YOU WANT THE REVERSE (PUSHING)
want_reverse=false
# true

if [[ "${want_reverse}" == true ]]; then
    desired_dir="1.300"
    final_distance="5.000"
    # "5.000"
fi

## DEFINING NANOPARTICLE INFORMATION
np_shape="EAM"
np_diameter="2"
## DEFINING LIGAND NAME
ligand_name="ROT017"
# "ROT010"
# "ROT011"
# "ROT004"
# "ROT001"
# "ROT012" # ROT001
# "ROT012"

## DEFINING TRIAL NUMBER
trial="1"
## FORCE FIELD
## FORCE FIELD
if [[ "${modified_ff}" == true ]]; then
  echo "Generating modified force field parameters"
  forcefield="${MODIFIED_FF}"
else
  forcefield="${FORCEFIELD}" # Force Field (opls-aa or charmm36-nov2016.ff)
fi
## DEFINING SYSTEM PARAMETERS
temperature="300.00"
## LIPID MEMBRANE INFORMATION
## DEFINING LIPID NAME
lipid_name="DOPC"

## DEFINING SIZE
lipid_size="196"
## DEFINING DEFAULT NP TO LIPID MEMBRANE DISTANCE
np_lipid_membrane_dist="5"

## DEFINING PULL CONSTANT
pull_constant="2000" # kJ/mol/nm2, as defined by previous papers

## DEFINING PULL RATE
pull_rate="0.0005" # nm / ps -- PRODUCTION

## DEFINING OUTPUT NAME
if [[ "${want_reverse}" != true ]]; then
    input_dir_name="$(get_np_lipid_membrane_output_name \
                            ${lipid_name} \
                            ${lipid_size} \
                            ${temperature} \
                            ${np_lipid_membrane_dist} \
                            ${pull_rate} \
                            ${pull_constant} \
                            ${np_shape} \
                            ${np_diameter} \
                            ${ligand_name} \
                            ${trial}
                            )"
else
    ## GETTING OUTPUT NAME
    input_dir_name=$(get_np_lm_pulling_name "${desired_dir}" \
                                         "${final_distance}"\
                                         "${pull_rate}" \
                                         "${pull_constant}" \
                                         "${lipid_name}" \
                                         "${lipid_size}" \
                                         "${np_shape}" \
                                         "${np_diameter}" \
                                         "${ligand_name}" \
                                         "${trial}" \
                                         )
fi
                        


### DEFINING INPUT VARIABLES
echo "${input_dir_name}"

## DEFINING SLEEP TIME
sleeptime="2"
# "1"

## DEFINING US DETAILS
us_increment="0.2" # increments
if [[ "${want_reverse}" != true ]]; then
    us_initial="1.3" 
else
    us_initial="1.5" 
fi

## REDEFINING INITIAL AND FINAL
us_initial="5.1"
us_final="5.1"

## DEFINING REWRITE
rewrite=false
# true if you want to redo all computations

####################
### OUTPUT NAMES ###
####################
## DEFINING OUTPUT NAMES
if [[ "${want_reverse}" != true ]]; then
    output_name_prefix="US"
else
    output_name_prefix="USrev"
fi
## OUTPUT NAME
# output_name="${output_name_prefix}-${us_initial}_${us_final}_${us_increment}-${input_dir_name}" # Original naming for ROT001 AND ROT010
output_name="${output_name_prefix}-${input_dir_name}"

## OUTPUT SUBMIT
output_submit_prefix="submit"
output_run_prefix="run"

######################
### DEFINING PATHS ###
######################
## PATH TO INPUT SIM FOLDER
path_input_sim_folder="${PATH2NPLMSIMS}/${input_sim_dir_name}/${input_dir_name}"

## PATH TO OUTPUT SIM FOLDER
path_output_sim_folder="${PATH2NPLMSIMS}/${output_sim_dir_name}/${output_name}"

## MDP FILE INFORMATION
mdp_parent_path="${INPUT_MDP_PATH}/charmm36"
mdp_file_path="${mdp_parent_path}/lipid_bilayers"

## SUBMISSION FILE
run_submit_file="run_nplm_umbrella_sampling.sh"
run_multiple_index="run_nplm_us_sims_multiple_index.sh"
submit_script="submit_nplm_us_sims.sh"

## CHECKING PATHS
stop_if_does_not_exist "${path_input_sim_folder}"

####################################
### DEFINING DIRECTORY STRUCTURE ###
####################################

## CONTAINS ALL INPUT INFORMATION
output_inputs="${path_output_sim_folder}/1_input_files"
## CONTAINS ALL GRO CONFIGURATIONS
output_configs="${path_output_sim_folder}/2_configurations"
## CONTAINS ALL SUBMISSION FILES
output_submit="${path_output_sim_folder}/3_submit_files"
## CONTAINS ALL SIMULATIONS
output_sims="${path_output_sim_folder}/4_simulations"
## CONTAINS ALL ANALYSIS
output_analysis="${path_output_sim_folder}/5_analysis"

## SUB DIRECTORYS WITHIN SIM
output_sub_dir_0_em="0_EM"
output_sub_dir_1_md="1_MD"

## DEFINING SUB DIRECTORY
output_inputs_mdp_folder="${output_inputs}/mdp_files"

#########################
### DEFAULT VARIABLES ###
#########################
## DEFINING PULL GROUP
pull_group_1="AUNP"
pull_group_2="${lipid_name}" # NEED TO UPDATE WITH RESIDUE NAME IN FUTURE

## DEFINING PREFIX FOR NPLM
nplm_prefix="nplm"

## DEFINING TYPE
if [[ "${want_reverse}" != true ]]; then
    direction_type="pull"
else
    direction_type="push"
fi
## PULLING INPUTS
pull_prefix="nplm_${direction_type}"
pull_tpr_file="${pull_prefix}.tpr"
pull_xtc_file="${pull_prefix}.xtc"
pull_index="${direction_type}.ndx"

## DEFINING OUTPUT SUMMARY FILE
output_summary="${direction_type}_summary.xvg"

## PYTHON SCRIPT
python_find_configurations="${MDBUILDER}/umbrella_sampling/umbrella_sampling_get_desired_configs.py"

## DEFINING OUTPUT CONFIG SUMMARY
config_summary="config_summary.csv"

## DEFINING TOPPAR FILE
toppar="toppar"

## DEFINING TIME STEP
time_step="0.002"

## DEFINING DEBUG
want_debug=false
# false
# false
# false

## DEFINING DEFAULT FILE TO MONITOR
job_info="status.info"

## DEFINING JOB STATUS
job_status="current_job_status.info"

## DEFINING MAXWARN
num_max_warn="5"

## RUN INFORMATION
num_cores="28"

## DEFINING EM NUMBER OF CORES
em_num_cores="28"

## DEFINING MD RUN COMMAND
mdrun_command="gmx mdrun -nt"

## DEFINING SUFFIX
mdrun_command_suffix=""

## DEFINING GROMACS COMMAND
gromacs_command="gmx"

###################
### MAIN SCRIPT ###
###################

#################################################
### STEP 1: GETTING DISTANCES IN PULLING SIMS ###
#################################################
## GOING INTO INPUT PATH
cd "${path_input_sim_folder}"
echo "-----------------------------------------------------"
echo "--- STEP 1: COMPUTING DISTANCES FROM PULLING SIMS ---"
echo "-----------------------------------------------------"
## PRINTING
echo "Input path: ${path_input_sim_folder}"

## SEEING IF INDEX FILE WAS MADE
if [ ! -e "${pull_index}" ]; then
    echo "Since ${pull_index} does not exist, creating index file!"
## CREATING INDEX FILE
gmx make_ndx -f "${pull_tpr_file}" \
             -o "${pull_index}" << INPUTS
keep 0
r ${pull_group_1}
r ${pull_group_2}
q
INPUTS

else
    echo "${pull_index} exists, continuing!"
fi

## SEEING IF EXISTS
if [ ! -e "${output_summary}" ]; then
    ## PRINTING
    echo "Generating summary file: ${output_summary}"
    ## COMPUTING CENTER OF MASS DISTANCES
    gmx distance -s "${pull_tpr_file}" \
                 -f "${pull_xtc_file}"  \
                 -oxyz "${output_summary}" \
                 -select "com of group ${pull_group_2} plus com of group ${pull_group_1}" \
                 -n "${pull_index}" > /dev/null 2>&1

else
    echo "Summary file (${output_summary}) exists! Continuing script..."
             
fi

sleep "${sleeptime}"

############################################
### STEP 2: GETTING CONFIGS AND TOPOLOGY ###
############################################

echo "-----------------------------------------"
echo "--- STEP 2: GENERATING CONFIGURATIONS ---"
echo "-----------------------------------------"

## CHECKING IF DIRECTORY EXISTS
create_dir "${path_output_sim_folder}" -f

## GOING TO PATH
cd "${path_output_sim_folder}"

## CREATING SUB FOLDERS
mkdir -p "${output_inputs}" \
         "${output_configs}" \
         "${output_submit}" \
         "${output_sims}" \
         "${output_analysis}" \
         "${output_inputs_mdp_folder}"

## DEFINING PATH TO OUTPUT CSV
path_config_csv="${output_configs}/${config_summary}"

## RUNNING PYTHON CODE
python3.6 "${python_find_configurations}" \
            --sum "${path_input_sim_folder}/${output_summary}" \
            --out "${path_config_csv}" \
            --upper "${us_final}" \
            --lower "${us_initial}" \
            --inc "${us_increment}" \
            --reverse
            
    
sleep "${sleeptime}"

##################################
### STEP 3: COPYING OVER FILES ###
##################################

echo "----------------------------------"
echo "--- STEP 3: COPYING FILES OVER ---"
echo "----------------------------------"

## COPYING TOPOLOGY
echo "Copying topology, force field, itp, prm, and toppar files"
echo "Input directory: ${path_input_sim_folder}"
echo "Output directory: ${output_inputs}"
cp -r "${path_input_sim_folder}"/${nplm_prefix}.top "${output_inputs}"
## FORCEFIELD
cp -r "${path_input_sim_folder}"/${forcefield} "${output_inputs}"
## TOPPAR FILE
## ITP AND PARAMETER FILES
cp -r "${path_input_sim_folder}"/{*.itp,*.prm} "${output_inputs}"
cp -r "${path_input_sim_folder}"/${toppar} "${output_inputs}"

#########################################
### STEP 4: GENERATING CONFIGURATIONS ###
#########################################

echo "--------------------------------"
echo "--- STEP 4: GENERATE CONFIGS ---"
echo "--------------------------------"
## READING EACH LINE
while read -r line; do
    ## GETTING INDEX
    index=$(awk -F',' '{print $1}' <<< ${line})
    ## GETTING ACTUAL DISTANCE
    desired_dist=$(awk -F',' '{print $2}' <<< ${line})
    ## GETTING ACTUAL DIST
    actual_dist=$(awk -F',' '{print $3}' <<< ${line})
    ## GETTING TIME
    time_ps=$(awk -F',' '{print $4}' <<< ${line})
    
    ## PRINTING
    echo "Generating configurations: ${index}, desired ${desired_dist} nm, actual ${actual_dist} nm, time ${time_ps} ps"
    
    ## DEFINING NAME
    config_name="${desired_dist}.gro"
    
    ## EXTRACTING GRO FILE
gmx trjconv -f "${path_input_sim_folder}/${pull_xtc_file}" \
            -s "${path_input_sim_folder}/${pull_tpr_file}" \
            -o "${output_configs}/${config_name}" \
            -dump "${time_ps}" > /dev/null 2>&1  << INPUTS &
System      
INPUTS
    
done <<< "$(tail -n+2 ${path_config_csv})"
## PRINTING
echo "Waiting for configurations to complete! Time: $(date)"
wait
echo "Configuration extraction is complete at: $(date)"

sleep "${sleeptime}"

###############################################
### STEP 5: ADDING MDP FILES AND SUBMISSION ###
###############################################

echo "-------------------------------------------"
echo "--- STEP 5: GENERATE MDP AND SUBMISSION ---"
echo "-------------------------------------------"

## DEFINING MDP FILES
em_mdp="em.mdp"
equil_mdp="umbrella_sampling_equil.mdp"
prod_mdp="umbrella_sampling_prod.mdp"

## DEFINING PRODUCTION TIME STEPS
if [[ "${want_debug}" == true ]]; then
    equil_time_steps="1000"
    prod_time_steps="1000" # 1 ps
    # "5000" # 10 ps
else
    equil_time_steps="250000" # 500 ps
    prod_time_steps="25000000" # 50 ns
fi

## COPYING MDP FILES
cp "${mdp_file_path}/"{${em_mdp},${equil_mdp},${prod_mdp}} "${output_inputs_mdp_folder}"

## EDITING MDP FILE
echo "Editing MDP file"
path_output_prod_mdp="${output_inputs_mdp_folder}/${prod_mdp}"
path_output_equil_mdp="${output_inputs_mdp_folder}/${equil_mdp}"
sed -i "s#_GROUP_1_NAME_#${pull_group_1}#g" "${path_output_prod_mdp}" "${path_output_equil_mdp}"
sed -i "s#_GROUP_2_NAME_#${pull_group_2}#g" "${path_output_prod_mdp}" "${path_output_equil_mdp}"
sed -i "s#_DT_#${time_step}#g" "${path_output_prod_mdp}" "${path_output_equil_mdp}"
sed -i "s#_NSTEPS_#${prod_time_steps}#g" "${path_output_prod_mdp}"
sed -i "s#_NSTEPS_#${equil_time_steps}#g" "${path_output_equil_mdp}"
sed -i "s#_TEMPERATURE_#${temperature}#g" "${path_output_prod_mdp}" "${path_output_equil_mdp}"
sed -i "s#_PULLKCONSTANT_#${pull_constant}#g" "${path_output_prod_mdp}" "${path_output_equil_mdp}"

## DEFINING PATH TO JOB OUTPUT
path_job_info="${path_output_sim_folder}/${job_info}"

# DEFINING OUTPUT PREFIX
output_prefix="nplm"

## CREATING INDEX LIST
index=0

## READING EACH LINE
while read -r line; do
    ## GETTING ACTUAL DISTANCE
    desired_dist=$(awk -F',' '{print $2}' <<< ${line})
    ## DEFINING NAME
    config_name="${desired_dist}.gro"
    ## DEFINING PATH TO CONFIG
    path_config_gro="${output_configs}/${config_name}"
    
    ## DEFINING PATH TO SPECIFIC SIM
    path_current_sim="${output_sims}/${desired_dist}"
    
    ## DEFINING MDP FILE
    path_current_sim_mdp_prod="${path_current_sim}/${prod_mdp}"
    path_current_sim_mdp_equil="${path_current_sim}/${equil_mdp}"
    
    ## PRINTING
    echo "Generating TPR file and MDP file for: ${index}, ${desired_dist} nm"
    
    ## ADDING EM AND PROD DIRECTORIES
    mkdir -p "${path_current_sim}"
    
    ## GOING TO PATH
    cd "${path_current_sim}"
    
    ## GENERATING TPR FILE FOR EM STEP
    gmx grompp -f "${output_inputs_mdp_folder}/${em_mdp}" \
               -c "${path_config_gro}" \
               -p "${output_inputs}/${nplm_prefix}.top" \
               -o "${path_current_sim}/${output_prefix}_em.tpr" \
               -maxwarn "${num_max_warn}" > /dev/null 2>&1 &
               
    ## ADDING MDP FILE
    sed "s#_INITIALDIST_#${desired_dist}#g" "${path_output_prod_mdp}" > "${path_current_sim_mdp_prod}"
    sed "s#_INITIALDIST_#${desired_dist}#g" "${path_output_equil_mdp}" > "${path_current_sim_mdp_equil}"
    
    ## ADDING INFORMATION TO JOB
    echo "${index} ${desired_dist}" >> ${path_job_info}
    
    ## ADDING TO INDEX
    index=$(( ${index}+1 ))
    

done <<< "$(tail -n+2 ${path_config_csv})"

echo "Waiting for TPR development to complete! Current time: $(date)"
wait
echo "TPR development is complete at $(date)"

sleep "${sleeptime}"

## COPYING SUBMISSION FILE
cp "${PATH2SUBMISSION}/${run_submit_file}" "${output_submit}"
echo "Editing submission script: ${run_submit_file}"

## EDITING SUBMISSION FILE
path_output_run_file="${output_submit}/${run_submit_file}"

sed -i "s/_SIMFOLDER_/$(basename ${output_sims})/g" "${path_output_run_file}"
sed -i "s/_INPUTFOLDER_/$(basename ${output_inputs})/g" "${path_output_run_file}"
sed -i "s/_INPUTTOPFILE_/${nplm_prefix}.top/g" "${path_output_run_file}"
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" "${path_output_run_file}"
sed -i "s/_US_MDPFILE_/${prod_mdp}/g" "${path_output_run_file}"
sed -i "s/_EQUIL_MDPFILE_/${equil_mdp}/g" "${path_output_run_file}"
sed -i "s/_STATUSFILE/${job_info}/g" "${path_output_run_file}"
sed -i "s/_CURRENTJOBSTATUS_/${job_status}/g" "${path_output_run_file}"
sed -i "s/_MAXWARN_/${num_max_warn}/g" "${path_output_run_file}"

sleep "${sleeptime}"

##################################
### STEP 6: SUBMISSION SCRIPTS ###
##################################

echo "---------------------------------"
echo "--- STEP 6: SPLITTING WINDOWS ---"
echo "---------------------------------"

## GETTING TOTAL WINDOWS
total_windows=$(tail -n+2 ${path_config_csv} | wc -l)

## FINDING LAST WINDOW BASED ON -1
final_window_based_on_zero=$((${total_windows}-1))

## DEFINING NUMBER OF SPLITS
if [[ "${want_debug}" == true ]]; then
num_split="1"
else
num_split="${total_windows}"
fi
# "19"
# "6"

## FINDING DIVIDED VALUES INTO MULTIPLE JOBS (ROUNDING DOWN)
divided_value="$((${total_windows} / ${num_split}))"

echo "Total windows: ${total_windows}"
echo "Total number of splits is: ${num_split}"
echo "Divided value: ${divided_value}"
sleep "${sleeptime}"

## going to output directory
cd "${path_output_sim_folder}"

## LOOPING THROUGH EACH SPLIT
for each_split_index in $(seq 0 $((${num_split}-1)) ); do
        ## DEFINING EACH LAMBDA
        ### INITIAL SPLIT
        if [ "${each_split_index}" == 0 ]; then
            initial_lambda="$((${each_split_index}*${divided_value}))"
            final_lambda="$(( ${each_split_index}+${divided_value}-1 ))"
        ### FINAL SPLIT
        elif [ "${each_split_index}" == "$((${num_split}-1))" ]; then
            initial_lambda="$((${final_lambda}+1))"
            final_lambda="${final_window_based_on_zero}"
        ### MIDDLE SPLITS
        else
            initial_lambda="$((${final_lambda}+1))"
            final_lambda="$(( ${initial_lambda}+${divided_value}-1 ))"
        fi

        ## DEFINING SUBMISSION SCRIPT
        path_split_submit="${path_output_sim_folder}/${output_submit_prefix}_${each_split_index}.sh"
        path_split_run="${path_output_sim_folder}/${output_run_prefix}_${each_split_index}.sh"
        # "${}"
        
        ## DEFINING JOB NAME
        job_name="${output_name}_${each_split_index}"
        
        ## PRINTING
        echo "Creating submission and run: $(basename ${path_split_submit}) and $(basename ${path_split_run})"
        
        ## COPYING SUBMISISON
        cp -r "${PATH2SUBMISSION}/${submit_script}" "${path_split_submit}"
        cp -r "${PATH2SUBMISSION}/${run_multiple_index}" "${path_split_run}"
        
        ## EDITING RUN FILE
        sed -i "s/_INITIALINDEX_/${initial_lambda}/g" "${path_split_run}"
        sed -i "s/_FINALINDEX_/${final_lambda}/g" "${path_split_run}"
        sed -i "s#_RUNSCRIPT_#./$(basename ${output_submit})/${run_submit_file}#g" "${path_split_run}"
        
        ## EDITING SUBMISSION FILE
        sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${path_split_submit}"
        sed -i "s/_JOB_NAME_/${job_name}/g" "${path_split_submit}"
        sed -i "s/_EMNUMCORES_/${em_num_cores}/g" "${path_split_submit}"
        sed -i "s#_MDRUNCOMMAND_#${mdrun_command}#g" "${path_split_submit}"
        sed -i "s#_MDRUNCOMMANDSUFFIX_#${mdrun_command_suffix}#g" "${path_split_submit}"
        sed -i "s#_RUNSCRIPT_#./$(basename ${path_split_run})#g" "${path_split_submit}"
        sed -i "s#_GROMACSCOMMAND_#${gromacs_command}#g" "${path_split_submit}"

        ## ADDING TO JO LIST
        echo "${path_split_submit}" >> "${JOB_SCRIPT}"

done
