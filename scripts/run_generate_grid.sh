#!/bin/bash

# run_generate_grid.sh
# This script runs the generate grid 

# 
# Written by: Alex K. Chew (10/29/2019)
# DEFINING VARIABLES
#	_OUTPUTPREFIX_ <-- OUTPUT PREFIX
#	_GRIDBASH_ <-- bash script for grid
#	_MESH_ <-- mesh
#	_GRIDPROC_ <-- grid processors
#	_ANALYSISDIR_ <-- analysis directory

## DEFINING NUMBER OF CORES
num_cores="$1"

## DEFINING PREFIX
output_prefix="_OUTPUTPREFIX_"

## DEFINING BASH CODE
grid_bash_code="_GRIDBASH_"

## INPUTS FOR GRIDDING
mesh="_MESH_"

## DEFINING GRID VALUES
grid_procs="_GRIDPROC_"

## DEFINING ANALYSIS DIR
analysis_dir="_ANALYSISDIR_"

## DEFINIGN PATH TO SIMS
path_sims="_PATHSIMS_"

######################
### 1. RUNNING XTC ###
######################

## SEEING IF GRO FILE EXISTS
if [ ! -e "${output_prefix}.gro" ]; then
	## SEEING IF XTC FILE EXISTS
	if [ -e "${output_prefix}.xtc" ]; then
		## RESTARTING
		gmx mdrun -nt "${num_cores}" -v -s "${output_prefix}.tpr" -cpi "${output_prefix}.cpt" -append -deffnm "${output_prefix}"
	else
		## STARTING
		gmx mdrun -nt "${num_cores}" -v -deffnm "${output_prefix}"
	fi
else
	echo "Simulation has completed!"
fi

###################
### 2. GRIDDING ###
###################

## DEFINING INPUTS
grid_output_prefix="out"

## GRO AND TPR
tpr_file="${output_prefix}.tpr"
xtc_file="${output_prefix}.xtc"

## RUNNING CODE
bash "${grid_bash_code}" "${path_sims}" \
                         "${mesh}" \
                         "${grid_output_prefix}" \
                         "${tpr_file}" \
                         "${xtc_file}" \
                         "${grid_procs}" \
                         "${analysis_dir}"