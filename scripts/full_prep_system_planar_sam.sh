#!/bin/bash

# full_prep_system_planar_sam.sh
# The purpose of this script is to take the script "prep_system_ligandbuilder_planar.sh" and generate multiple systems from it. I will take the results from Huber's side project and re-apply the mass job creation. 
# Originally written by Alex Chew (03/30/2017)
# 17-04-11: Added force field option
# 17-11-26: Changed name to be simply full prep ligand builder
# 18-01-08: Updating to include ligand builder rc

# Usage: bash Full_System_Preparation.sh

### SOURCING ALL VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

## DEFINING LIGAND BUILDER SSCRIPT
PATH2LIGANDBUILDER="$PATH2SCRIPTS/prep_system_planar_sam.sh"

### DEFINING VARIABLES
# TYPE OF MONOLAYER
monolayer_type="Planar" # Spherical or Planar
# TEMPERATURE
temp="300.00"
# "310.15" # Temperature of normal cell proliferation is 310.15
# SIMULATION DIRECTORY
simulation_dir="20200514-dod_planar_SAM"
# "20200403-Planar_SAMs-5nmbox"
# "20200401-Planar_SAMs_no_temp_anneal_or_vacuum_with_npt_equil"
# "20200328-Planar_SAMs_new_protocol-shorterequil"
# "20200326-Planar_SAMs_with_larger_z_8nm"
# "20200117-Planar_SAM_ROTPE"
# "200102-Planar_SAM_temp_annealing_try4"
# "191205-Planar_SAMs_dod_trial_2_charged"
# "191118-Planar_SAMs_Running"
# "180919-Planar_rerun_full_correction_multiple_trials_2_3"

## GENERAL VARIABLES
# Ligand Names
# "ROT_TMMA" "ROT_NS" "ROT_SN" "ROT_COO" "ROT_NP5" "ROT_OH" 
# "butanethiol" "octanethiol" "dodecanethiol" "hexadecanethiol"
# "C11NH3p" "C11COOm" "C11OH"  "octanethiol" "dodecanethiol" 
declare -a ligand_names=("butanethiol" "C3OH") # "ROT001" "ROT009"
# "C11NH2" "C11CONH2" "C11COOH" "C11CF3"
# "dodecanethiol" "C11OH"
# "C11NH2" "C11CONH2" "C11OH" "C11COOH" "C11CF3"
# "dodecanethiol"
#  "C11OH"
# "ROTPE1" "ROTPE2" "ROTPE3" "ROTPE4"
# "C11NCH33" "C11CCH33"
# "ROT001" "ROT002" "ROT003" "ROT004" "ROT005" "ROT006" "ROT007" "ROT008" "ROT009"
# "C11COO" "C11NH3"
# "C11COOH" "C11NH2" "C11CONH2" "C11OH"
# "dodecanethiol"
# "C11COOH"
# "ROT002" "ROT003"
# "C11NH3" "C11COO"
# "C11NH2" "C11CONH2" "C11OH" "C11COOH"
# "dodecanethiol"
# "ROT002" "ROT003" "ROT004" "ROT005" "ROT006" "ROT007" "ROT008" "ROT009"
# "ROT001" 
# "C11NH2" "C11CONH2" "C11OH" "C11COOH"
# "C11COOH"
# dodecanethiol
# "C11OH" "C11NH2" "C11CONH2"
# "C11COOH"s
# "C11COOH"
#  "C11COOm" "C11OH"  "hexadecanethiol" #  "dodecanethiol"
# "butanethiol" "pentanethiol" "hexanethiol" "octanethiol" "decanethiol" "dodecanethiol" "hexadecanethiol"
# Force field type
declare -a forcefield=("charmm36-jul2017.ff")
# "charmm36-nov2016.ff") #  "opls-aa" "charmm36-nov2016.ff" "charmm36-nov2016.ff"

## PLANAR VARIABLES
# Gold on or off
declare -a wantGold=("True") #  "True" "False"

## TRIAL NUMBER
# "1" "2" "3" "5"
declare -a trial_num=("1")
# "2" "3"

## DEFINING WATER BOX
water_box_z="10.0" # 6 nm top and bottom
# "8.0" # 4nm top and bottom
# "6.0" # ORIGINALLY 4.0

# Varying ligands
for currentligandname in "${ligand_names[@]}"; do
    # Varying force field
    for currentforcefield in "${forcefield[@]}"; do
        if [[ ${monolayer_type} == "Planar" ]]; then
            # Varying Gold
            for goldOnOrOff in "${wantGold[@]}"; do
                ## LOOPING THROUGH TRIAL NUMBERS
                for current_trial in "${trial_num[@]}"; do
                    # Using preparation of system
                    bash ${PATH2LIGANDBUILDER} "${monolayer_type}" \
                                               "${currentligandname}" \
                                               "${currentforcefield}" \
                                               "${goldOnOrOff}" \
                                               "${temp}" \
                                               "${simulation_dir}" \
                                               "${current_trial}" \
                                               "${water_box_z}"
                done
            done
        else
            echo "Error, no correct monolayer type selected. Please check preparation script!"
            echo "Exiting here..."
            exit
        fi

    done
    
done

# Echoing what we have done
echo "------SUMMARY------"
echo "Created systems for ligand types: ${ligand_names[@]}"
echo "System type: ${monolayer_type}"
echo "Gold True/False: ${wantGold[@]}"
echo "Forcefields: ${forcefield[@]}"