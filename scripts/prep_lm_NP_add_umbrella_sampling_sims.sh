#!/bin/bash

# prep_lm_NP_add_umbrella_sampling_sims.sh
# The purpose of this code is to add frames to an existing umbrella sampling 
# simulation. This is typically done after you have completed an umbrella 
# sampling simulation and desire to run additional umbrella sampling simulations on different windows. This procedure is a little more complicated because we need the original transfer information and need to select on a case-by-case basis. 

## INPUTS:
#   input simulation directory
#   pulling simulation directory
#   

## OUTPUTS:
#   new simulation directory
#   new simulation submission script
#   need number of windows definition
#   previous simulation scripts are moved (?) Yes, could input

#################################################
### LOADING PRE-DEFINED VARIABLES / FUNCTIONS ###
#################################################
source "../bin/nanoparticle_rc.sh"
print_script_name

#######################
### INPUT VARIABLES ###
#######################

#### FOR PULLING SIMULATION - ROT12

# ## DEFINING INPUT SIMULATION DIRECTORY
# input_sim_dir_name="US_Z_PULLING_SIMS"
# ## DEFINING SPECIFIC DIRECTORY
# input_specific_dir_name="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"

# ## DFEINING PULLING SIMULATION NAME
# pulling_sim_dir_name="20200113-NPLM_PULLING"
# pulling_sim_specific_dir_name="NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"


#### FOR PULLING SIMULATION - ROT001
## DEFINING INPUT SIMULATION DIRECTORY
input_sim_dir_name="US_Z_PULLING_SIMS"
# "20200120-US-sims_NPLM_rerun_stampede"
## DEFINING SPECIFIC DIRECTORY
input_specific_dir_name="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

## DFEINING PULLING SIMULATION NAME
pulling_sim_dir_name="20200113-NPLM_PULLING"
pulling_sim_specific_dir_name="NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

# #### FOR BENZENE SIMS
# ## DEFINING INPUT SIMULATION DIRECTORY
# input_sim_dir_name="20200613-US_otherligs_z-com"
# ## DEFINING SPECIFIC DIRECTORY
# input_specific_dir_name="US-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"
# ## PULLING SIMS
# pulling_sim_dir_name="20200608-Pulling_other_ligs"
# pulling_sim_specific_dir_name="NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"


#### FOR PUSHING SIMULATION - ROT12

# ## DEFINING INPUT SIMULATION DIRECTORY
# input_sim_dir_name="20200124-US-sims_NPLM_reverse_stampede"
# ## DEFINING SPECIFIC DIRECTORY
# input_specific_dir_name="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"

# ## DFEINING PULLING SIMULATION NAME
# pulling_sim_dir_name="20200123-Pulling_sims_from_US"
# pulling_sim_specific_dir_name="pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"


# #### PUSHING SIMULATION FOR ROT001
# ## DEFINING INPUT SIMULATION DIRECTORY
# input_sim_dir_name="20200207-US-sims_NPLM_reverse_ROT001_aci"
# ## DEFINING SPECIFIC DIRECTORY
# input_specific_dir_name="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT001_1"

# ## DFEINING PULLING SIMULATION NAME
# pulling_sim_dir_name="20200123-Pulling_sims_from_US"
# pulling_sim_specific_dir_name="pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT001_1"


## ROT001
# ## DEFINING INPUT SIMULATION DIRECTORY
# input_sim_dir_name="20200120-US-sims_NPLM_rerun_stampede"
# ## DEFINING SPECIFIC DIRECTORY
# input_specific_dir_name="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

# ## DFEINING PULLING SIMULATION NAME
# pulling_sim_dir_name="20200113-NPLM_PULLING"
# pulling_sim_specific_dir_name="NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

## FOR BENZENE (MODIFIED FF)

# ## DEFINING INPUT SIMULATION DIRECTORY
# input_sim_dir_name="20200818-Bn_US_modifiedFF"
# ## DEFINING SPECIFIC DIRECTORY
# input_specific_dir_name="US-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"

# ## DFEINING PULLING SIMULATION NAME
# pulling_sim_dir_name="20200816-modified_ff_pulling"
# pulling_sim_specific_dir_name="NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"

## DEFINING IF YOU WANT THE REVERSE (PUSHING)
if [[ ${pulling_sim_specific_dir_name} = "pullnplm"* ]]; then
    want_reverse=true
else
    want_reverse=false
fi

## DEFINING SLEEP TIME
sleeptime="0"

## DEFINING CONFIGS THAT NEED EXTENED TIMES
declare -a extended_configs=("1.300" "1.500" "1.700")
#  "5.100"

## DEFINING DESIRED CONFIGS
declare -a desired_configs=("6.100" "6.300" "6.500")
# "1.925"
# "1.900"
# "1.950"
# "1.900"
# "2.000"
# "5.100" "5.300" "5.500" "5.700" "5.900"
# "6.100" "6.300" "6.500"
# "5.100"
# "5.100" "5.300" "5.500" "5.700" "5.900" "6.100"
#  "6.3" "6.5"
# "1.500" "1.700" 
# "1.300" 
# "1.500" "1.700" 
# "5.1"

## DESIRED SPRING CONSTANT
desired_spring_constant="2000"
# "4000"
# If not 2000, then we will change the spring constant!

## CONVERTING CONFIGS TO COMMAS
config_comma_list="$(printf "%s," "${desired_configs[@]}")"
## REMOVING END COMMA
config_comma_list="${config_comma_list%,}"

#  "5.300" "5.500" "5.700" "5.900" "6.100"

## PYTHON SCRIPT TO GET DESIRED CONFIGURATIONS
python_find_configurations="${MDBUILDER}/umbrella_sampling/umbrella_sampling_get_desired_configs.py"

## EXTRACTING NAME
read -a extract_name <<< $(extract_us_np_lipid_membrane_name "${input_specific_dir_name}")

## GETTING LIPID NAME
lipid_name="${extract_name[3]}"

######################
### DEFINING PATHS ###
######################
## PATH TO INPUT SIM FOLDER
path_input_sim_folder="${PATH2NPLMSIMS}/${pulling_sim_dir_name}/${pulling_sim_specific_dir_name}"

## PATH TO OUTPUT SIM FOLDER
path_output_sim_folder="${PATH2NPLMSIMS}/${input_sim_dir_name}/${input_specific_dir_name}"

## MDP FILE INFORMATION
mdp_parent_path="${INPUT_MDP_PATH}/charmm36"
mdp_file_path="${mdp_parent_path}/lipid_bilayers"

## SUBMISSION FILE
run_submit_file="run_nplm_umbrella_sampling.sh"
run_multiple_index="run_nplm_us_sims_multiple_index.sh"
submit_script="submit_nplm_us_sims.sh"

## CHECKING PATHS
stop_if_does_not_exist "${path_input_sim_folder}"

####################################
### DEFINING DIRECTORY STRUCTURE ###
####################################

## CONTAINS ALL INPUT INFORMATION
output_inputs="${path_output_sim_folder}/1_input_files"
## CONTAINS ALL GRO CONFIGURATIONS
output_configs="${path_output_sim_folder}/2_configurations"
## CONTAINS ALL SUBMISSION FILES
output_submit="${path_output_sim_folder}/3_submit_files"
## CONTAINS ALL SIMULATIONS
output_sims="${path_output_sim_folder}/4_simulations"
## CONTAINS ALL ANALYSIS
output_analysis="${path_output_sim_folder}/5_analysis"
## CONTAINS ALL ARCHIVE SUBMISSION
output_archive="${path_output_sim_folder}/6_archive_submission"

## SUB DIRECTORYS WITHIN SIM
output_sub_dir_0_em="0_EM"
output_sub_dir_1_md="1_MD"

## DEFINING SUB DIRECTORY
output_inputs_mdp_folder="${output_inputs}/mdp_files"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING DEFAULT SPRING CONSTANT
default_spring_constant="2000"

## DEFINING PULL GROUP
pull_group_1="AUNP"
pull_group_2="${lipid_name}" # NEED TO UPDATE WITH RESIDUE NAME IN FUTURE

## DEFINING PREFIX FOR NPLM
nplm_prefix="nplm"

## DEFINING TYPE
if [[ "${want_reverse}" != true ]]; then
    direction_type="pull"
else
    direction_type="push"
fi

## PULLING INPUTS
pull_prefix="nplm_${direction_type}"
pull_tpr_file="${pull_prefix}.tpr"
pull_xtc_file="${pull_prefix}.xtc"
pull_index="${direction_type}.ndx"

## DEFINING OUTPUT SUMMARY FILE
output_summary="${direction_type}_summary.xvg"

## DEFINING OUTPUT CONFIG SUMMARY
config_summary="config_summary.csv"

## DEFINING DEFAULT FILE TO MONITOR
job_info="status.info"

## DEFINING DEBUG
want_debug=false

## OUTPUT SUBMIT
output_submit_prefix="submit"
output_run_prefix="run"

## RUN INFORMATION
num_cores="28"

## DEFINING EM NUMBER OF CORES
em_num_cores="28"

## DEFINING MD RUN COMMAND
mdrun_command="gmx mdrun -nt"

## DEFINING GROMACS COMMAND
gromacs_command="gmx"

## DEFINING SUFFIX
mdrun_command_suffix=""

## DEFINING MDP FILES
em_mdp="em.mdp"
equil_mdp="umbrella_sampling_equil.mdp"
prod_mdp="umbrella_sampling_prod.mdp"

## DEFINING MAXWARN
num_max_warn="5"

## DEFINING JOB STATUS
job_status="current_job_status.info"

# DEFINING OUTPUT PREFIX
output_prefix="nplm"

## DEFINING DEFAULT PRODUCTION TIME
default_prod_time="50000.000"
extend_prod_time="100000.000"


##################################
### STEP 0: COPYING OVER FILES ###
##################################

## EDITING SUBMISSION FILE
path_output_run_file="${output_submit}/${run_submit_file}"

#if [ ! -e "${path_output_run_file}" ]; then
## COPYING SUBMISSION FILE
cp "${PATH2SUBMISSION}/${run_submit_file}" "${path_output_run_file}"
echo "Editing submission script: ${run_submit_file}"

sed -i "s/_SIMFOLDER_/$(basename ${output_sims})/g" "${path_output_run_file}"
sed -i "s/_INPUTFOLDER_/$(basename ${output_inputs})/g" "${path_output_run_file}"
sed -i "s/_INPUTTOPFILE_/${nplm_prefix}.top/g" "${path_output_run_file}"
sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" "${path_output_run_file}"
sed -i "s/_US_MDPFILE_/${prod_mdp}/g" "${path_output_run_file}"
sed -i "s/_EQUIL_MDPFILE_/${equil_mdp}/g" "${path_output_run_file}"
sed -i "s/_STATUSFILE/${job_info}/g" "${path_output_run_file}"
sed -i "s/_CURRENTJOBSTATUS_/${job_status}/g" "${path_output_run_file}"
sed -i "s/_MAXWARN_/${num_max_warn}/g" "${path_output_run_file}"
#fi

sleep "${sleeptime}"

#################################################
### STEP 1: GETTING DISTANCES IN PULLING SIMS ###
#################################################
## GOING INTO INPUT PATH
cd "${path_input_sim_folder}"
echo "-----------------------------------------------------"
echo "--- STEP 1: COMPUTING DISTANCES FROM PULLING SIMS ---"
echo "-----------------------------------------------------"
## PRINTING
echo "Input path: ${path_input_sim_folder}"

## SEEING IF INDEX FILE WAS MADE
if [ ! -e "${pull_index}" ]; then
    echo "Since ${pull_index} does not exist, creating index file!"
## CREATING INDEX FILE
gmx make_ndx -f "${pull_tpr_file}" \
             -o "${pull_index}" << INPUTS
keep 0
r ${pull_group_1}
r ${pull_group_2}
q
INPUTS

else
    echo "${pull_index} exists, continuing!"
fi

## SEEING IF EXISTS
if [ ! -e "${output_summary}" ]; then
    ## PRINTING
    echo "Generating summary file: ${output_summary}"
    ## COMPUTING CENTER OF MASS DISTANCES
    gmx distance -s "${pull_tpr_file}" \
                 -f "${pull_xtc_file}"  \
                 -oxyz "${output_summary}" \
                 -select "com of group ${pull_group_2} plus com of group ${pull_group_1}" \
                 -n "${pull_index}" > /dev/null 2>&1

else
    echo "Summary file (${output_summary}) exists! Continuing script..."
             
fi

sleep "${sleeptime}"

############################################
### STEP 2: GETTING CONFIGS AND TOPOLOGY ###
############################################

echo "-----------------------------------------"
echo "--- STEP 2: GENERATING CONFIGURATIONS ---"
echo "-----------------------------------------"

## CREATING DIRECTORY IF NECESSARY
if [ ! -e "${path_output_sim_folder}" ]; then
    mkdir -p "${path_output_sim_folder}"
fi

## GOING TO PATH
cd "${path_output_sim_folder}"

## CREATING SUB FOLDERS
mkdir -p "${output_inputs}" \
         "${output_configs}" \
         "${output_submit}" \
         "${output_sims}" \
         "${output_analysis}" \
         "${output_inputs_mdp_folder}" \
         "${output_archive}"

## DEFINING PATH TO OUTPUT CSV
path_config_csv="${output_configs}/${config_summary}"

## RUNNING PYTHON CODE
python3.6 "${python_find_configurations}" \
            --sum "${path_input_sim_folder}/${output_summary}" \
            --out "${path_config_csv}" \
            --configs "${config_comma_list}" \
            --dim "z" \
            --reverse
            
#            --upper "${us_final}" \
#            --lower "${us_initial}" \
#            --inc "${us_increment}" \
            
    
sleep "${sleeptime}"

##################################
### STEP 3: GENERATING CONFIGS ###
##################################

echo "--------------------------------"
echo "--- STEP 3: GENERATE CONFIGS ---"
echo "--------------------------------"
## READING EACH LINE
while read -r line; do
    ## GETTING INDEX
    index=$(awk -F',' '{print $1}' <<< ${line})
    ## GETTING ACTUAL DISTANCE
    desired_dist=$(awk -F',' '{print $2}' <<< ${line})
    ## GETTING ACTUAL DIST
    actual_dist=$(awk -F',' '{print $3}' <<< ${line})
    ## GETTING TIME
    time_ps=$(awk -F',' '{print $4}' <<< ${line})
    
    ## GETTING DISTANCE IN THREE DECIMALS
    desired_dist_3dec=$(printf "%0.3f" ${desired_dist})
    
    ## PRINTING
    echo "Generating configurations: ${index}, desired ${desired_dist_3dec} nm, actual ${actual_dist} nm, time ${time_ps} ps"
    
    ## DEFINING NAME
    config_name="${desired_dist_3dec}.gro"
    
    ## CHECKING IF EXISTING
if [ ! -e "${output_configs}/${config_name}" ]; then
    
    ## EXTRACTING GRO FILE
gmx trjconv -f "${path_input_sim_folder}/${pull_xtc_file}" \
            -s "${path_input_sim_folder}/${pull_tpr_file}" \
            -o "${output_configs}/${config_name}" \
            -dump "${time_ps}" > /dev/null 2>&1  << INPUTS &
System      
INPUTS

else
    echo "Configuration is complete for: ${config_name}"

fi
done <<< "$(tail -n+2 ${path_config_csv})"
## PRINTING
echo "Waiting for configurations to complete! Time: $(date)"
wait
echo "Configuration extraction is complete at: $(date)"

sleep "${sleeptime}"

###############################################
### STEP 4: ADDING MDP FILES AND SUBMISSION ###
###############################################

echo "-------------------------------------------"
echo "--- STEP 4: GENERATE MDP AND SUBMISSION ---"
echo "-------------------------------------------"

## PATH TO MDP FILE
path_output_equil_mdp="${output_inputs_mdp_folder}/${equil_mdp}"
path_output_prod_mdp="${output_inputs_mdp_folder}/${prod_mdp}"

## DEFINING PATH TO JOB OUTPUT
path_job_info="${path_output_sim_folder}/${job_info}"

## READING LAST INDEX
if [ -e "${path_job_info}" ]; then
    index="$(tail -n1 "${path_job_info}" | awk '{print $1}' )"
    ## ADDING TO INDEX
    index=$(( ${index}+1 ))
else
    index="0"
fi

## LOOPING FOR EACH CONFIGURATION
for each_configuration in ${desired_configs[@]}; do
    ## GETTING DISTANCE IN THREE DECIMALS
    desired_dist=$(printf "%0.3f" ${each_configuration})

    ## SEEING IF DESIRED 
    if [[ "${desired_spring_constant}" != "${default_spring_constant}" ]]; then
        echo "-------------------------"
        echo "Note! Desired spring constant is not the same as default"
        echo "Default: ${default_spring_constant}"
        echo "New: ${desired_spring_constant}"
        echo "Changing spring constant for mdp files"

        ## CHANGING DESIRED
        desired_dist_with_spr="${desired_dist}_${desired_spring_constant}"
        echo "Desired dist: ${desired_dist}"
        echo "-------------------------"

        sleep 1
    else
        desired_dist_with_spr="${desired_dist}"
    fi

    ## DEFINING NAME
    config_name="${desired_dist}.gro"
    ## DEFINING PATH TO CONFIG
    path_config_gro="${output_configs}/${config_name}"
    ## DEFINING PATH TO SPECIFIC SIM
    path_current_sim="${output_sims}/${desired_dist_with_spr}"
    
    ## DEFINING MDP FILE
    path_current_sim_mdp_prod="${path_current_sim}/${prod_mdp}"
    path_current_sim_mdp_equil="${path_current_sim}/${equil_mdp}"
    
    ## ADDING EM AND PROD DIRECTORIES
    mkdir -p "${path_current_sim}"
    
    ## GOING TO PATH
    cd "${path_current_sim}"
    
    if [ ! -e "${path_current_sim}/${output_prefix}_em.tpr" ]; then
    ## GENERATING TPR FILE FOR EM STEP
    gmx grompp -f "${output_inputs_mdp_folder}/${em_mdp}" \
               -c "${path_config_gro}" \
               -p "${output_inputs}/${nplm_prefix}.top" \
               -o "${path_current_sim}/${output_prefix}_em.tpr" \
               -maxwarn "${num_max_warn}" 
    # > /dev/null 2>&1 &
    fi
    
    ## ADDING MDP FILE
    if [ ! -e "${path_current_sim_mdp_prod}" ]; then
        sed "s#_INITIALDIST_#${desired_dist}#g" "${path_output_prod_mdp}" > "${path_current_sim_mdp_prod}"
    fi
    
    if [ ! -e "${path_current_sim_mdp_equil}" ]; then
        sed "s#_INITIALDIST_#${desired_dist}#g" "${path_output_equil_mdp}" > "${path_current_sim_mdp_equil}"
    fi
    
    ## SEEING IF DESIRED 
    if [[ "${desired_spring_constant}" != "${default_spring_constant}" ]]; then

        ## LOOPING THROUGH EACH
        declare -a path_mdps=("${path_current_sim_mdp_equil}" "${path_current_sim_mdp_prod}")

        ## LOOPING
        for each_mdp in ${path_mdps[@]}; do
            ## FINDING LINE
            line_num=$(grep -nE "pull_coord1_k" "${each_mdp}"  | sed 's/\([0-9]*\).*/\1/')
            ## EDITING
            sed -i "${line_num}s/${default_spring_constant}/${desired_spring_constant}/" "${each_mdp}"

            ## PRINTING
            echo "Editing: ${each_mdp}"

        done

    fi

    ## SEEING TOTAL TIME
    if [[ ! " ${extended_configs[@]} " =~ " ${desired_dist} " ]]; then
        ## DEFINING PROD
        current_prod_time="${default_prod_time}"
    else
        ## EXTENDED TIME
        current_prod_time="${extend_prod_time}"
    fi
    
    ## SEEING IF VALUE IS ADDED TO JOB
    read -a distances_array <<< $(awk '{ print $2 }' ${path_job_info})
    ## CHECKING IF VALUE IS THERE
    if [[ ! " ${distances_array[@]} " =~ " ${desired_dist_with_spr} " ]]; then

        ## ADDING INFORMATION TO JOB
        echo "${index} ${desired_dist_with_spr} ${current_prod_time}" >> ${path_job_info}
        ## PRINTING
        echo "Generating TPR file and MDP file for: ${index}, ${desired_dist} nm"
        ## ADDING TO INDEX
        index=$(( ${index}+1 ))
    else
        ## LOCATING THE LINE
        line_num=$(awk '{ print $2 }' ${path_job_info} | grep -nE "${desired_dist_with_spr}" | sed 's/\([0-9]*\).*/\1/' | head -n1)
        line_text="$(sed -n ${line_num}p ${path_job_info})"
        ## GETTING INDEX
        current_index=$(awk '{print $1}'<<< "${line_text}")
        
        ## DEFINING CURRENT JOB LINE
        current_job_line="${current_index} ${desired_dist_with_spr} ${current_prod_time}"
        ## REPLACE THE LINE
        sed -i "s/${line_text}/${current_job_line}/g" ${path_job_info}
    
        ## PRINTING
        echo "Refreshing job index: ${current_index}, ${desired_dist_with_spr}"

    fi
            
done

##################################
### STEP 5: SUBMISSION SCRIPTS ###
##################################

echo "---------------------------------"
echo "--- STEP 5: SPLITTING WINDOWS ---"
echo "---------------------------------"

## GOING TO PATH
cd "${path_output_sim_folder}"

## MOVING ANY SUBMISSIN FILES TO ARCHIVE (SILENT MOVE)
mv -vf "orig-submit"* "${output_archive}" 2>/dev/null
mv -vf "submit_"* "${output_archive}" 2>/dev/null 
mv -vf "slurm."*".out"* "${output_archive}" 2>/dev/null

## GETTING TOTAL WINDOWS
total_windows="${#desired_configs[@]}"

## DEFINING NUMBER OF SPLITS
if [[ "${want_debug}" == true ]]; then
    num_split="1"
else
    num_split="${total_windows}"
fi

## FINDING LAST WINDOW BASED ON -1
final_window_based_on_zero=$((${total_windows}-1))

## FINDING DIVIDED VALUES INTO MULTIPLE JOBS (ROUNDING DOWN)
divided_value="$((${total_windows} / ${num_split}))"

## GETTING TOTAL NUMBER OF WINDOWS
echo "Total windows: ${total_windows}"
echo "Total number of splits is: ${num_split}"
echo "Divided value: ${divided_value}"

## LOOPING THROUGH EACH SPLIT
for idx in $(seq 0 $((${num_split}-1)) ); do
    ## GETTING DISTANCE
    each_configuration="${desired_configs[idx]}"
    ## GETTING DISTANCE IN THREE DECIMALS
    desired_dist=$(printf "%0.3f" ${each_configuration})
    ## GETTING SPLIT INDEX
    each_split_index="$(grep "${desired_dist}" ${path_job_info} | awk '{print $1}')"

    ## DEFINING SUBMISSION SCRIPT
    path_split_submit="${path_output_sim_folder}/${output_submit_prefix}_${each_split_index}.sh"
    path_split_run="${path_output_sim_folder}/${output_run_prefix}_${each_split_index}.sh"

    ## DEFINING JOB NAME
    job_name="${input_specific_dir_name}_${each_split_index}"

    ## PRINTING
    echo "Creating submission and run: $(basename ${path_split_submit}) and $(basename ${path_split_run})"

    ## COPYING SUBMISISON
    cp -r "${PATH2SUBMISSION}/${submit_script}" "${path_split_submit}"
    cp -r "${PATH2SUBMISSION}/${run_multiple_index}" "${path_split_run}"

    ## EDITING RUN FILE
    sed -i "s/_INITIALINDEX_/${each_split_index}/g" "${path_split_run}"
    sed -i "s/_FINALINDEX_/${each_split_index}/g" "${path_split_run}"
    sed -i "s#_RUNSCRIPT_#./$(basename ${output_submit})/${run_submit_file}#g" "${path_split_run}"

    ## EDITING SUBMISSION FILE
    sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${path_split_submit}"
    sed -i "s/_JOB_NAME_/${job_name}/g" "${path_split_submit}"
    sed -i "s/_EMNUMCORES_/${em_num_cores}/g" "${path_split_submit}"
    sed -i "s#_MDRUNCOMMAND_#${mdrun_command}#g" "${path_split_submit}"
    sed -i "s#_MDRUNCOMMANDSUFFIX_#${mdrun_command_suffix}#g" "${path_split_submit}"
    sed -i "s#_RUNSCRIPT_#./$(basename ${path_split_run})#g" "${path_split_submit}"
    sed -i "s#_GROMACSCOMMAND_#${gromacs_command}#g" "${path_split_submit}"
    ## ADDING TO JOB LIST
    echo "${path_split_submit}" >> "${JOB_SCRIPT}"
done



