#!/bin/bash

# gnp_descriptors_switch_solvents.sh
# The purpose of this script is to switch one pure solvent to another. 
# This is useful when you want to test the effects of cosolvents in another system.

# ASSUMPTIONS:
#   - You have already run the NP in pure water
#   - You want to switch the GNP to a different solvent

# ALGORITHM:
#   1 - Check the output file to see if it's correct
#   2 - Create a new folder within folder
#   3 - Copy over only the gnp without any solvent solution
#   4 - Re-solvate system with new solvent
#   5 - Energy minimize
#   6 - Create submission scripts and mdp files

# VARIABLES:
#   $1: input_sim_path -- path to input simulation
#   $2: cosolvent -- cosolvent name of interest

# Written by Alex K. Chew (11/23/2020)
# 
# Run by:
#   bash gnp_descriptors_switch_solvents.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

## PRINTING SCRIPT NAME
print_script_name

#################
### FUNCTIONS ###
#################

#######################
### INPUT VARIABLES ###
#######################

## INPUT PATH TO SIM
input_sim_path="${1-/home/akchew/scratch/nanoparticle_project/simulations/20201111-GNPs_database_water_sims-try28_subset_ligs-retry/spherical_300.00_K_GNP14_CHARMM36jul2020_Trial_1}"

## DEFINING COSOLVENT
cosolvent="${2-dmso}"

## DEFINING REWRITE
rewrite=${3-false}
# true if you want to rewrite directory

## DEFINING DESIRED TRAJECTORY TIME IN PS
extract_traj_time_ps="20000"

## DEFINING TEMPERATURE
temp="300" # default

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING PYTHON FILES
py_count_gro_residues="${PY_COUNT_GRO_FILE}"

## DEFINING PYTHON CODE TO GET INDICES
py_get_gnp_ligand_index="${GNPDESCRIPTOR}/prep/get_GNP_ligand_indices.py"

## DEFINING DISTANCE TO EDGE
dist_to_edge="1.5"

## DEFINING INPUT PREFIX
input_prefix="sam_prod"

## DEFINING INPUT TOP
input_top="sam.top"

## DEFINING ITP FILE FOR SAM
np_itp_file="sam.itp"

## DEFINING OUTPUT PREFIX
output_prefix="sam"

## DEFINING OUTPUT INDEX
output_ndx="gold_ligand.ndx"

## DEFINING OUTPUT KEY NAME
gold_residue_name="AUNP"
output_key_name="GNP_LIGANDS"

## DEFINING SUMMARY FILE
gro_summary="solvation.summary"

## DEFINING OUTPUT BOX TYPE
output_box_type="dodecahedron"

## GROMPP OPTIONS
max_warn_index="5" # Number of times for maximum warnings

###################
### MDP DETAILS ###
###################

### MDP FILE INFORMATION
mdp_parent_path="${INPUT_MDP_PATH}/charmm36/Spherical"

## MDP FILES
mdp_em="minim_sam_gmx5.mdp" # Energy minimization mdp script
mdp_equil="npt_double_equil_gmx5_charmm36.mdp" # Equilibration mdp script
mdp_prod="npt_double_prod_gmx5_charmm36_10ps.mdp" # Production mdp script

## DEFINING SUBMISSION FILE INFORMATION
submit_parent_path="${PATH2SUBMISSION}"
submit_file_name="submit_most_likely_np_mixed_solvents.sh"

## DEFINING OUTPUT SUBMIT NAME
output_submit_name="submit.sh"

## DEFINING NUMBER OF CORES
num_cores="28"
num_em_cores="10"

## DEFINING EQUIL MDP TIME
equil_mdp_time="500000" # 1 ns equilibration
equil_mdp_temp="${temp}"

## DEFINING MDP FILE TIME
mdp_file_time="10000000" # 20 ns
# "15000000"  # 30ns
# 20 ns  - 10000000
mdp_temperature="${temp}"

##########################################
### DEFINING OUTPUT SIMULATION DETAILS ###
##########################################

## DEFINING OUTPUT SIM
output_dir="switchsolvent-${cosolvent}-${extract_traj_time_ps}"

## DEFINING OUTPUT PATH
output_path="${input_sim_path}/${output_dir}"

#################
### MAIN CODE ###
#################

## CHECKING IF INPUT PATH EXISTS
stop_if_does_not_exist "${input_sim_path}"

## STARTING MAIN CODE
if [[ ${rewrite} == true ]] || [[ ! -e "${output_path}" ]]; then
    ## CREATING DIRECTORY
    create_dir "${output_path}" -f

    ## GOING TO OUTPUT DIRECTORY
    cd "${output_path}"

    ## DEFINING PATH OUTPUT INDEX
    path_output_idx="${output_path}/${output_ndx}"

    ##############################
    ### PART 1: EXTRACTING GNP ###
    ##############################
    # In this part, we extract the gold nanoparticle alone
    python "${py_get_gnp_ligand_index}" --path_to_sim "${input_sim_path}" \
                                        --input_prefix "${input_prefix}" \
                                        --output_idx_file "${path_output_idx}" \
                                        --output_key_name "${output_key_name}"

    ## CREATING FILE
    echo "Creating ${output_prefix}_initial.gro..."

    ## GETTING TIME BEFORE
    time_before=$(awk -v output_time=${extract_traj_time_ps} 'BEGIN{ printf "%.3f", output_time - 1000.000}')

    ## RUNNING TRJCONV
gmx trjconv -f "${input_sim_path}/${input_prefix}.xtc" \
            -s "${input_sim_path}/${input_prefix}.tpr" \
            -o "${output_path}/${output_prefix}_initial.gro" \
            -n "${path_output_idx}" \
            -b "${time_before}" \
            -dump "${extract_traj_time_ps}" -pbc mol -center >/dev/null 2>&1 << INPUTS
${gold_residue_name}
${output_key_name}
INPUTS
    
    ## RUNNING EDITCONF
gmx editconf -f "${output_path}/${output_prefix}_initial.gro" \
             -o "${output_path}/${output_prefix}_initial_resize.gro" \
             -d "${dist_to_edge}" \
             -bt "${output_box_type}" >/dev/null 2>&1  << INPUTS
System
INPUTS

    ## CHECKING EXISTANCE
    stop_if_does_not_exist "${output_path}/${output_prefix}_initial_resize.gro"

    ##############################
    ### COSOLVENT PATH DETAILS ###
    ##############################
    ## PATH TO INPUT MOLECULE
    path_input_molecule="${NP_PREP_SOLVENT_EQUIL}/${cosolvent}"

    ## CHECKING INPUT FILE
    check_setup_file "${path_input_molecule}" "${cosolvent}"

    ## DEFINING MOLECULE GRO, ITP, PRM FILES
    molecule_gro_file="${cosolvent}.gro"
    molecule_itp_file="${cosolvent}.itp"
    molecule_prm_file="${cosolvent}.prm"

    ####################################################
    ### PART 2: COPYING OVER FORCE FIELD FILES, ETC. ###
    ####################################################
    echo "--- COPYING OVER FORCE FIELD FILES, ETC. ---"
    ## TOPOLOGY
    copy_top_include_files "${input_sim_path}/${input_top}" "${output_path}"
    cp -r "${input_sim_path}/${input_top}" "${output_path}"

    ## MDP FILE
    cp -r "${mdp_parent_path}/"{${mdp_em},${mdp_equil},${mdp_prod}} "${output_path}"

    ## SUBMISSION FILE
    cp -r "${submit_parent_path}/${submit_file_name}" "${output_path}/${output_submit_name}"

    # COSOLVENT ITP AND PRM FILE
    cp -r "${path_input_molecule}/"{${molecule_itp_file},${molecule_prm_file}} "${output_path}"

    ##############################
    ### UPDATING TOPOLOGY FILE ###
    ##############################
    ## CLEARING TOPOLOGY
    top_clear_molecules ${output_prefix}.top
    ## ADDING NP INTO TOPOLOGY
    np_residue_name=$(itp_get_resname ${np_itp_file})
    echo "   ${np_residue_name}   1" >> "${output_path}/${input_top}" 
    ## GETTING COSOLVENT RESIDUE NAME
    cosolvent_residue_name="$(itp_get_resname ${cosolvent}.itp)"

    #########################
    ### PART 3: SOLVATING ###
    #########################
    echo "--- SOLVATING ---"
    ## RUNNING GMX SOLVATE
    gmx solvate -cs "${path_input_molecule}/${molecule_gro_file}" \
                -cp "${output_path}/${output_prefix}_initial_resize.gro" \
                -p "${output_path}/${input_top}" \
                -o "${output_path}/${output_prefix}_solvated.gro" &> /dev/null # Silencing

    ## CHECKING FILE IF EXISTING
    stop_if_does_not_exist "${output_prefix}_solvated.gro"

    ################################
    ### CORRECTING TOPOLOGY FILE ###
    ################################

    echo "--- CORRECTING TOPOLOGY FILE ---"
    ## RUNNING PYTHON SCRIPT TO COUNT GRO FILE
    python "${py_count_gro_residues}" \
            --igro "${output_path}/${output_prefix}_solvated.gro" \
            --sum "${output_path}/${gro_summary}"

    ## FINDING NUMBER OF SOLVENTS
    num_solvent=$(get_residue_num_from_sum_file "${cosolvent_residue_name}" "${output_path}/${gro_summary}")
    echo "Updating solvents: ${cosolvent_residue_name} ${num_solvent}"
    echo "   ${cosolvent_residue_name}   ${num_solvent}" >> "${output_path}/${input_top}" 

    ## COSOLVENT TOPOLOGY PRM
    topology_add_b4_include_specific "${output_path}/${input_top}" "${np_itp_file}" "#include \"${cosolvent}.prm\""

    ## COSOLVENT TOPOLOGY ITP
    topology_add_include_specific "${output_path}/${input_top}" "${np_itp_file}" "#include \"${cosolvent}.itp\""

    #################################
    ### NEUTRALIZING IF NECESSARY ###
    #################################
    ## DEFINING INDEX FILE
    cosolvent_index_file="cosolvent.ndx"

    echo -e "--- NEUTRALIZING CHARGES ---"
    echo "Creating TPR file for balancing of charge"
    ## CREATING TPR FILE
gmx grompp -f "${mdp_em}" \
           -c "${output_prefix}_solvated.gro" \
           -p "${input_top}" \
           -o "${output_prefix}_solvated.tpr" \
           -maxwarn "${max_warn_index}"

## CREATING INDEX FOR SOLVENT
index_make_ndx_residue_ "${output_prefix}_solvated.tpr" "${cosolvent_index_file}" "${cosolvent_residue_name}"
## USING GENION
gmx genion -s "${output_prefix}_solvated.tpr" \
           -p "${input_top}" \
           -o "${output_prefix}_neutral.gro" \
           -n "${cosolvent_index_file}" -neutral &> /dev/null << INPUT
${cosolvent_residue_name}
INPUT

    ########################################
    ### EDITING MDP AND SUBMISSION FILES ###
    ########################################
    echo -e "--- EDITING MDP AND SUBMISSION FILES ---"

    ## DEFINING JOB NAME
    job_name="${output_dir}_$(basename ${input_sim_path})"
    ## EQUIL MDP FILES
    sed -i "s/NSTEPS/${equil_mdp_time}/g" ${mdp_equil}
    sed -i "s/TEMPERATURE/${equil_mdp_temp}/g" ${mdp_equil}

    ## PROD MDP FILES
    sed -i "s/NSTEPS/${mdp_file_time}/g" ${mdp_prod}
    sed -i "s/TEMPERATURE/${temp}/g" ${mdp_prod}

    ## SUBMISSION FILE
    sed -i "s/_JOB_NAME_/${job_name}/g" ${output_submit_name}
    sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" ${output_submit_name}
    sed -i "s/_OUTPUTPREFIX_/${output_prefix}/g" ${output_submit_name}
    sed -i "s/_EM_GRO_FILE_/${output_prefix}_em.gro/g" ${output_submit_name}
    sed -i "s/_EQUILMDPFILE_/${mdp_equil}/g" ${output_submit_name}
    sed -i "s/_PRODMDPFILE_/${mdp_prod}/g" ${output_submit_name}
    sed -i "s/_TOPFILE_/${input_top}/g" ${output_submit_name}

    ##########################################
    #### CREATING ENERGY MINIMIZATION FILE ###
    ##########################################
    echo "*** Creating tpr file ***"
    gmx grompp -f "${mdp_em}" \
               -c "${output_prefix}_solvated.gro" \
               -p "${input_top}" \
               -o "${output_prefix}_em" -maxwarn 1
    ## ENERGY MINIMIZING
    gmx mdrun -nt ${num_em_cores} -v -deffnm "${output_prefix}_em" 

    ###########################
    ### STORING SETUP FILES ###
    ###########################
    ## DEFINING SETUP DIRECTORY
    setup_file="setup_file"

    ## DEFINING TRANSFER FILE
    transfer_summary="transfer.summary"

    ## CLEANING UP FILES
    rm \#*

    ## CREATING DIRECTORY
    mkdir "${setup_file}"
    ## MOVING
    mv "${output_prefix}_solvated.gro" \
       "${output_prefix}_solvated.tpr" \
       "${output_prefix}_initial_resize.gro" \
       "${output_prefix}_initial.gro" \
       "${setup_file}"

    summary_text="""
Cosolvent name: ${cosolvent}
Cosolvent residue name: ${cosolvent_residue_name}
Distance to edge (nm): ${dist_to_edge}
Time step extraction: ${extract_traj_time_ps}
    """
    echo "${summary_text}" > "${transfer_summary}"

    ## ADDING TO JOB LIST
    echo "${output_path}/${output_submit_name}" >> "${JOB_SCRIPT}"
    echo "------"
    echo "Complete! Check ${JOB_SCRIPT}"
    echo "Path: ${output_path}"
    echo ""
    

else
    echo "${output_path} exists"


fi
