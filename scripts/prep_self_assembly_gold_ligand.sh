#!/bin/bash

# prep_self_assembly_gold_ligand.sh
# This script follows the prep_self_assembly_ligands script -- main goal is to add ligands to the gold surface

# NOTE: the MDP files for this script uses 1 fs (instead of the usual 2 fs). I am sure it should not play a major role, but it may be important

## USAGE: 
# This will run a 2 nm diameter hollow core with trial 0
#   bash prep_self_assembly_gold_ligand.sh 2 hollow 0
# This will run a planar sam with trial 1
#   bash prep_self_assembly_gold_ligand.sh 1 planar 1


### VARIABLES:
#   $1: diameter of the gold nanoparticle in nm
#   $2: geometry:
#       hollow: hollow sphere (vacuum within)
#       spherical: fcc cut spherical surface
#   $3: trial number (integer value)
#   $4: length of the cylinder in nm ** TURNED OFF **

# Written by Alex Chew (12/05/2017)

## ALGORITHM
#   - Define the geometry
#   - Define the ligand of input (usually  butanethiol)
#   - Use a python script to create the gold core
#   - Determine the box size
#   - Solvate with butanethiol
#   - Add butanethiol to topology
#   - Expand the box based on distance allowing gas phase
#   - Energy minimize
#   - Output a submission script for you

### *** UPDATES *** ###
# 20180304 - Included geometry (spherical and hollow)
# 20180312 - Included debugging script
# 20180502 - Updated script global variables
# 20180712 - Updated script ot correctly generate cylinders
# 20180919 - Added functionality to have planar assembly approaches

### SOURCING ALL VARIABLES / FUNCTIONS
source "../bin/nanoparticle_rc.sh"

### BEGINNING OF SCRIPT ###

## PRINTING SCRIPT NAME
print_script_name

#######################
### INPUT VARIABLES ###
#######################

## FORCE FIELD
forcefield="pool.ff" # Force field type

## SPHERICAL DETAILS
diameter="$1" # nm
geometry="$2" 
    ## spherical: spherical cut off from a {111} gold facet
    ## hollow: spherical from brownian motion ("perfect sphere")
    ## EAM: spherical from embedded atom potential
    ## cylindrical_curved_ends: cylinder with curved ends
trial="$3" # Trial number of the same simulation

## LIGANDS
ligand_name="${4-butanethiol}"

## SEEING IF YOU WANT NONFRAG
want_non_fragment="${5-true}"

## DEFINING IF YOU WANT TO REWRITE
rewrite="${6-false}"
# True if you want to rewrite current output directory.

## DEFINING JOB LIST
job_list="${JOB_SCRIPT}"
job_list="/home/akchew/scratch/nanoparticle_project/scripts/job_self_assembly.txt"

length="10" # length of the cylinder
## DEFINITIONS FOR PLANAR SAMS
num_x_lig="10"
num_y_lig="10"

## EAM INPUTS
EAM_final_dir_structure_text_file="${PREP_GOLD_EAM_FINAL_STRUCTURES}/final_structure_list.txt"

## FINDING THE INPUT DIRECTORY
input_dir="${PREP_GOLD_EAM_FINAL_STRUCTURES}/${diameter}/${diameter}nm_Trial_${trial}"

## CHECKING IF SUCH DIRECTORY EXISTS
if [[ ! -e ${input_dir} ]] && [[ ${geometry} == "EAM" ]]; then
    echo "Error! EAM folder was not found! Check ${input_dir}"
    echo "STOPPING HERE!"
    exit
fi
input_file="vcsgc_em_final.xyz"

## CALCULATING SPHERICAL RADIUS
radius=$(awk -v diam=$diameter 'BEGIN{ printf "%.5f",diam/2}') # nms
dist_to_edge="1" # nm distance from edge of box

## DEFINING SIMULATION PARAMETERS
num_steps="2000000" # 2 ns quick sims
# "10000000" # 10 ns
# "30000000" # 30 ns
## DEFINING TEMPERATURE
temp="300" # Kelvins
## DISTANCE FROM SOLUTE TO EDGE OF BOX (EXPANSION)
dist_from_solute="2" # nms
## AVG SURFACE AREA PER THIOL
avg_surf_area_per_thiol="5" # "21.5" # "40" # "21.5" # Angstroms^2 / ligand (for flat surface)

molar_ligand_volume="0.30" # nm^3 -- measured with gmx editconf: -- 0.21 -- fudging to 0.3
# gmx editconf -f butanethiol.gro -bt cubic -d 0.000 -o butanethiol_shrink.gro
## CALCULATING TOTAL VOLUME REQUIRED
# FINDING VOLUME OF THE PARTICLE
if [ ${geometry} == "cylindrical_curved_ends" ]; then
    ## CYLINDER CASE
    ## CALCULATING TOTAL LIGANDS REQUIRED
    total_ligands=$(awk -v area=${avg_surf_area_per_thiol} -v rad=${radius} -v len=${length} 'BEGIN{ printf "%d",(4*3.14159*rad**2 + 2*3.14159*rad*(len-2*rad))/(area/100.0)}')
    # FINDING VOLUME OF THE PARTICLE
    np_volume=$(awk -v rad=$radius -v len=${length} 'BEGIN{ printf "%.5f", 4/3*3.14159*rad**3 +  (len - 2 * rad)*2*rad }')
    
else

    ## CALCULATING TOTAL LIGANDS REQUIRED
    total_ligands=$(awk -v area=${avg_surf_area_per_thiol} -v rad=${radius} 'BEGIN{ printf "%d",4*3.14159*rad**2/(area/100.0)}')
    # FINDING VOLUME OF THE PARTICLE
    np_volume=$(awk -v rad=$radius 'BEGIN{ printf "%.5f", 4/3*3.14159*rad**3 }')

fi
## COMPUTING TOTAL LIGANDS REQUIRED

## MAX WARN
max_warn_index="5" # Number of times to warn me for gmx grompp

## SURFACTANTS
input_surfactant_folder="4_nm_300_K_${ligand_name}" # Folder in Prep_system -> prep_gold -> surfactant
input_gro_file="${ligand_name}_equil.gro"
input_itp_file="${ligand_name}.itp"

## DEFINING PARENT FOLDER
if [[ "${want_non_fragment}" == true ]]; then
    echo "Using nonfragmented ligand simulations"
    parent_folder="${PREP_GOLD_SURF_NONFRAG}"
    output_folder_gold_assembly="${PREP_GOLD_SIM_NONFRAG}"

else
    parent_folder="${PREP_GOLD_SURF}"
    output_folder_gold_assembly="${PREP_GOLD_SIM}"
fi

## DEFINING FULL PATHS TO SURFACTANT
path2surfactant="${parent_folder}/${input_surfactant_folder}"
path2surfactant_gro="${path2surfactant}/${input_gro_file}"
path2surfactant_itp="${path2surfactant}/${input_itp_file}"

## GOLD
python_script_gold="${MDBUILDER}/builder/make_gold_spherical_111.py"
gold_resname="AUNP"
gold_molecule_name="AU"

## REMOVING SCRIPT
py_remove_atoms_file="${PATH2SCRIPTS}/remove_atoms.py" # Removes residues within the center core (for hollow shell geometry)

## MDP FILES -- located in prep_gold folder
energy_min_mdp="minim_sam_gmx5_pool.mdp"
equil_mdp="npt_equil_gmx5_pool.mdp"
## SUBMISSION FILES
submit_file="submit.sh"
submit_file_loc="${PREP_GOLD_INPUT_FILE}/Submit_Scripts"

## DEFINING OUTPUT NAMES
output_name="gold_ligand"
output_gro="${output_name}.gro"
output_top="${output_name}.top"

## CREATING OUTPUT DIRECTORY NAME
if [ ${geometry} == "planar" ]; then
output_dir_name="${geometry}_${num_x_lig}x${num_y_lig}_${temp}_K_${dist_from_solute}_nmEDGE_${avg_surf_area_per_thiol}_AREA-PER-LIG_${input_surfactant_folder}_Trial_${trial}"
else
output_dir_name="${geometry}_${diameter}_nmDIAM_${temp}_K_${dist_from_solute}_nmEDGE_${avg_surf_area_per_thiol}_AREA-PER-LIG_${input_surfactant_folder}_Trial_${trial}"
fi
output_dir="${output_folder_gold_assembly}/${geometry}/${output_dir_name}"

## CHECKING IF OUTPUT DIRECTORY EXISTS
if [[ ! -e "${output_dir}" ]] || [[ "${rewrite}" == true ]]; then

    ## CHECKING IF DIRECTORY EXISTS
    check_output_dir "${output_dir}"

    ## COPYING TO DIRECTORY
    # MDP FILES
    cp -rv "${PREP_GOLD_MDP_DIR}/"{${energy_min_mdp},${equil_mdp}} "${output_dir}"
    # FORCE FIELD
    if [[ "${forcefield}" != 'pool.ff' ]]; then
        cp -rv "${INPUT_DIR}/${forcefield}" "${output_dir}"
    fi
    # SUBMISSION FILE
    cp -rv "${submit_file_loc}/${submit_file}" "${output_dir}"
    # ITP FILE
    cp -rv "${path2surfactant_itp}" "${output_dir}"

    ## GOING TO DIRECTORY
    cd "${output_dir}"

    ## EDITING MDP FILE
    sed -i "s/NSTEPS/${num_steps}/g" ${equil_mdp} # Changing equilibrium file number of steps
    sed -i "s/TEMPERATURE/${temp}/g" ${equil_mdp} # Changing equilibrium file

    ## EDITING SUBMISSION FILE
    sed -i "s/JOB_NAME/${output_dir_name}/g" ${submit_file} # Changing job name to match output
    sed -i "s/LIGAND/${output_name}/g" ${submit_file} # Changing ligand names
    sed -i "s/EQUIL_MDP/${equil_mdp}/g" ${submit_file} # Changing equilibrium file name

    ### RUNNING PYTHON SCRIPT TO OUTPUT INTO THE OUTPUT FOLDER
    echo "----- RUNNING ${python_script_gold} SCRIPT ------"
    python3 ${python_script_gold} --rad "${radius}" --ff "${forcefield}" --ofold "${output_dir}" --otop "${output_top}" --ogro "${output_gro}" --dist "${dist_to_edge}" --resname "${gold_resname}" --itp "${gold_molecule_name}" --geom "${geometry}" --idir "${input_dir}" --ifile "${input_file}" --len ${length} --numligx "${num_x_lig}" --numligy "${num_y_lig}"

    ## CALCULATING TOTAL AREA FOR PLANAR
    if [ ${geometry} == "planar" ]; then
        ## FINDING BOX DIMENSIONS
        read -a box_lengths <<< $(read_gro_box_lengths "${output_gro}")
        
        ## CALCULATING TOTAL LIGANDS REQUIRED
        total_ligands=$(awk -v area=${avg_surf_area_per_thiol} -v x_dim=${box_lengths[0]} -v y_dim=${box_lengths[1]} 'BEGIN{ printf "%d", 2 * x_dim * y_dim / (area / 100)}') # Note, 2 times because you have above and below
        
        ## CALCULATING TOTAL VOLUME
        np_volume=$(read_gro_box_volumes "${output_gro}")
        
        
    fi

    ###########################################
    ### CALCULATING TOTAL VOLUME AND LENGTH ###
    ###########################################
    total_volume=$(awk -v ligand_vol=${molar_ligand_volume} -v num_lig=${total_ligands} -v np_vol=${np_volume} 'BEGIN{ printf "%.5f", np_vol + num_lig*ligand_vol }')
    ## CALCULATING BOX LENGTH
    box_length=$(awk -v total_vol=$total_volume 'BEGIN{ printf "%.5f", total_vol**(1/3) }')

    ## CHANGING BOX SIZE
    if [ ${geometry} == "cylindrical_curved_ends" ]; then
        ## FINDING UPDATED Z POSITION
        z_updated=$(awk -v len=${length} -v rad="${radius}" -v box_length="${box_length}" 'BEGIN{ printf "%.5f", (len/(2*rad))^0.5 * box_length }')
        xy_updated=$(awk -v z_value=${z_updated} -v vol="${total_volume}" 'BEGIN{ printf "%.5f", (vol/z_value)^(0.5) }')
        gmx editconf -f "${output_gro}" -o "${output_name}_expand.gro" -box ${xy_updated} ${xy_updated} ${z_updated} -c
        
    elif [ ${geometry} == "planar" ]; then
        ## FINDING TOTAL Z DIMENSION
        z_updated=$(awk -v x_dim=${box_lengths[0]} -v y_dim="${box_lengths[1]}" -v vol="${total_volume}" 'BEGIN{ printf "%.5f", vol/ (x_dim*y_dim)  }')
        ## USING EDIT CONF
        gmx editconf -f "${output_gro}" -o "${output_name}_expand.gro" -box ${box_lengths[0]} ${box_lengths[1]} ${z_updated}  # -c
        
    else
        gmx editconf -f "${output_gro}" -o "${output_name}_expand.gro" -box ${box_length} ${box_length} ${box_length} -c

    fi


    ## SOLVATING
    echo "SOLVATING, SAVING RESULTS TO --> solvation_results.txt"
    gmx solvate -cs "${path2surfactant_gro}" -cp "${output_name}_expand.gro" -p "${output_top}" -o "${output_name}_solv.gro" -maxsol "${total_ligands}" &> "solvation_results.txt"

    ## FINDING TOTAL NUMBER OF RESIDUES ADDED
    total_residues_added=$(grep -nE "Generated solvent containing" "solvation_results.txt" | awk '{print $(NF-1)}')

    ## FINDING RESIDUE ADDED NAME
    residue_name_added=$(sed -n -e '3p' ${input_itp_file} | awk '{print $1}')

    ## ADDING TO TOPOLOGY
    echo "   ${residue_name_added}    ${total_residues_added}" >> "${output_top}"

    ## ADDING LIGAND TO TOPOLOGY
    add_include_topology "${output_top}" "${input_itp_file}"

    ## REMOVING RESIDUES WITHIN THE CENTER
    if [ "${geometry}" == "hollow" ]; then
        echo "*** Running ${py_remove_atoms_file} ***"
        python ${py_remove_atoms_file} --rad ${radius} --ofold ${output_dir} --ogro "${output_name}_solv.gro" --otop "${output_top}"
    fi

    ## FINDING TOTAL GRO BOX LENGTHS
    if [ ${geometry} == "planar" ]; then
        read -a before_expansion_gro_box_lengths <<< $(read_gro_box_lengths "${output_name}_solv.gro")
    fi

    ## EDITING THE BOX SIZE TO EXPAND IT ALLOWING GAS PHASE
    gmx editconf -f "${output_name}_solv.gro" -o "${output_name}_solv_expand.gro" -d "${dist_from_solute}"

    ## EDITING BOX SIZE TO ADJUST FOR X AND Y
    if [ ${geometry} == "planar" ]; then
        ## FINDING NEW BOX LENGTHS
        read -a after_expansion_gro_box_lengths <<< $(read_gro_box_lengths "${output_name}_solv_expand.gro")
        ## USING EDIT CONF TO CORRECT THE BOX LENGTHS
        gmx editconf -f "${output_name}_solv_expand.gro" -o "${output_name}_solv_expand.gro" -box ${before_expansion_gro_box_lengths[0]} ${before_expansion_gro_box_lengths[1]} ${after_expansion_gro_box_lengths[2]}
    fi

    ## ENERGY MINIMIZING
    echo "----- ENERGY MINIMIZING ------"
    gmx grompp -f ${energy_min_mdp} -c "${output_name}_solv_expand.gro" -p "${output_top}" -o ${output_name}_em.tpr -maxwarn ${max_warn_index}
    gmx mdrun -v -nt 1 -deffnm "${output_name}_em"

    ### PRINTING SUMMARY
    echo "------------- SUMMARY -------------"
    echo "SYSTEM PARAMETERS:"
    echo "GEOMETRY: ${geometry}"
    echo "CURRENT RADIUS: ${radius} nm"
    echo "TEMPERATURE: ${temp} K"
    echo "TOTAL LIGANDS: ${total_ligands}"
    echo "BOX VOLUME: ${total_volume} nm^3"
    echo "BOX LENGTH: ${box_length} nm"
    echo "ADDING TO JOB SCRIPT: ${job_list}"
    echo "${output_dir}" >> "${job_list}"
else
    echo "Output directory exists! Rewrite turned off. Skipping current files! Path: ${output_dir}"
fi