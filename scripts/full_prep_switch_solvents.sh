#!/bin/bash

# full_prep_switch_solvents.sh
# This script runs prep_switch_solvents.sh for multiple solvent systems. 

## TODO: NEED TO UPDATE SCRIPT ***

# Written by Alex K. Chew (09/09/2019)

## USAGE:
#   bash full_prep_switch_solvents.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

### DEFINING PATH TO SCRIPT
SCRIPT_PATH="$PATH2SCRIPTS/prep_switch_solvents.sh"

###########################################
### USER-DEFINED VARIABLES 
###########################################

## DEFINING SIMULATION TO GET DETAILS FROM
simulation_dir_name="EAM_COMPLETE"
# "191221-Rerun_all_EAM_models_1_cutoff"
# "191216-ROT_15_16"
# "HYDROPHOBICITY_PROJECT_ROTELLO"
# "190520-2nm_ROT_Sims_updated_forcefield_new_ligands"

## DEFINING NEW SIMULATION DIRECTORY NAME
output_simulation_dir="20200106-ROT_FROZEN_DMSO"
# "191208-Switching_solvents_ROT_others"
# "190909-Switching_solvents_large_box"

## DEFINING LIGAND NAMES
declare -a ligand_list=("ROT010" "ROT011" "ROT012" "ROT014" "ROT015" "ROT016") #  
# "ROT001" "ROT002" "ROT003" "ROT004" "ROT005" "ROT006" "ROT007" "ROT008" "ROT009"
# "ROT015" "ROT016
# "ROT003" "ROT004" "ROT007" "ROT008" "ROT009"
# "ROT001" "ROT010" "ROT011" "ROT006"
# "ROT005" "ROT014"

## DEFINING SOLVENTS
declare -a solvent_list=("dmso") #  
# "tetrahydrofuran" "dmso" "dioxane" "methanol"

###########################################

## LOOPING THROUGH EACH LIGAND
for current_lig in "${ligand_list[@]}"; do
    ## LOOPING THROUGH EACH COSOLVENT
    for current_cosolvent in "${solvent_list[@]}"; do
        ## PRINTING
        echo "RUNNING THE FOLLOWING COMMAND:"
        echo "bash "${SCRIPT_PATH}" "${simulation_dir_name}" "${output_simulation_dir}" "${current_lig}" "${current_cosolvent}""
        ## RUNNING
        bash "${SCRIPT_PATH}" "${simulation_dir_name}" "${output_simulation_dir}" "${current_lig}" "${current_cosolvent}"
    done
done

### PRINTING SUMMARY
echo "------SUMMARY------"
echo "Created systems for ligands: ${current_lig[@]}"
echo "Cosolvents: ${solvent_list[@]}"