#!/bin/bash

# gnp_descriptors_hydration_analysis.sh
# This script generates analysis information post-hydration simulations. 
# 
# Written by: Alex K. Chew (11/27/2020)
# 
# Algorithm:
#   0 - Copy run script that performs the following:
#	1 - Generate system with only water heavy atoms (i.e. no GNPs)
#	2 - Create submission code to run the following:
#		2a - Run code to generate WC interface
#		2b - Run code to generate hydration maps
#	3 - Create a pickle and a datafile for each system
# 
# Run code:
#   bash gnp_descriptors_hydration_analysis.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"

############################
### DEFINING GLOBAL VARS ###
############################

## PATH TO HYDRATION SCRIPTS
hydration_scripts="${main_dir}/hydration_map_scripts"

## GETTING SUBMISSION SCRIPT
path_input_submission="${hydration_scripts}/submit_hydration_maps.sh"

## DEFINING GRID BASHRC
gridrc="gridrc.sh"

## DEFINING GRID CODE
bash_grid_code="generate_grid.sh"

## DEFINING EXTRACTION CODE
bash_hydration_file_name="extract_hydration_maps_with_python.sh"

#######################
### INPUT VARIABLES ###
#######################

## INPUT PATH TO SIM
input_sim_path="${1-/home/akchew/scratch/nanoparticle_project/simulations/20210111-Rerun_GNPs/spherical_300.00_K_GNP100_CHARMM36jul2020_Trial_1}"

## DEFINING REWRITE
rewrite=${2-false}

## DEFINING FOLDER
folder_within_sim_to_analyze="hydrationmap-20000"

#########################
### DEFAULT VARIABLES ###
#########################

## DEFINING INPUT FILES
input_prefix="sam_prod"

## DEFINING OUTPUT VARIABLES
output_folder="analysis"

## DEFINING INPUT FILES
input_gro_file="${input_prefix}.gro"
input_tpr_file="${input_prefix}.tpr"
input_xtc_file="${input_prefix}.xtc"

###################
### WC DEFAULTS ###
###################

## DEFINING TOTAL TIME TO TRUNCATE FROM BEGINNING
truncate_time="0" # ps

## DEFINING GRID SIZE
mesh="0.3,0.3,0.3"
# "0.3,0.3,0.3"
# "0.2,0.2,0.2"

## DEFINING COUNTOUR
contour="26"

## DEFINING INDEX FILE
index_file="no_hydrogens.ndx"

## OUTPUT SUBMIT
output_submit="submit.sh"

## DEFINING SIGMA
sigma="0.24"

## DEFINING GRID BUFFER
buffer="0.5"

## DEFINING BEGINNING AND END TRAJ
begin_traj="0"
# "19000"
# "0"
end_traj="5000"
# end_traj="20000"
# "20000"

## DEFINING DETAILS FOR PROCESSING
grid_n_procs="20"

## DEFINING PROCS FOR PYTHON CODE (HYDRATION MAPS)
n_procs="28"
# "20"

## DEFINING PURE WATER SIMULATIONS
pure_water_sim="false"

## DEFINING FRAME RATE - Frame rate of loading sims
frame_rate="1000"
# "28"

## DEFINING GRID BASHRC
gridrc="gridrc.sh"

## GRID LOCATION
grid_folder="wc_grid"

## DEFINING IF YOU WANT FROM THE END
want_grid_from_end=true

## DEFINING IF YOU WANT WC ONLY
want_wc_only=false
# true
# false

## GETTING END TRAJ GRID
begin_traj_grid="0"
end_traj_grid="1000"
# "1000"
# "4000"
# "2000"
# "1000"
# "1000" # last 5 ns
# "150"
# "500"
# 5000"

## DEFINING IF YOU WANT SAMPLING TIME
want_wc_sampling_time="False"
# "True"
# False if you do not want sampling time measurements

## DEFINING CUTOFF RADIUS FOR COMPUTING HYDRATION MAPS
cutoff_radius="0.33"

## DEFINING PLANAR SAM
planar_sam="False"

## REWRITE WC INTERFACE
rewrite_wc_interface=false

## DEFAULT RESIDUE LIST IS ALL HEAVY
residue_list='all_heavy'

## DEFINING OUTPUT PREFIX
output_prefix="out"

######################
### DEFINING PATHS ###
######################

## PATH TO SIM
path_to_sim="${input_sim_path}/${folder_within_sim_to_analyze}"

###################
### MAIN SCRIPT ###
###################

## DEFINING OUTPUT FOLDER
output_file="${sigma}-${cutoff_radius}-${begin_traj}-${end_traj}"

## CHECKING IF EXISTS
stop_if_does_not_exist "${path_to_sim}"
stop_if_does_not_exist "${path_to_sim}/${input_prefix}.gro"

## GOING TO FOLDER
cd "${path_to_sim}"

## CHECKING IF OUTPUT PATH HAS SPECIFIC FILE
specific_folder="grid-4000_5000"

## DEFINING PATH TO OUTPUT
output_path="${path_to_sim}/${output_file}"

## DEFINING CHECK PATH
check_path="${output_path}/${specific_folder}"

## STARTING MAIN CODE
if [[ ${rewrite} == true ]] || [[ ! -e "${output_path}" ]] || [[ ! -e "${check_path}" ]]; then

	## CREATING DIRECTORY
	create_dir "${output_path}" -f

	################################
	### GETTING TRAJECTORY TIMES ###
	################################

	## CHECKING IF XTC IS CORRECT
	check_xtc_time current_xtc_time "${input_prefix}"

	## CHECKING THE TIME
	if (( $(echo "${end_traj} > ${current_xtc_time}" | bc -l) )); then
	    echo "--------------------------------------------"
	    echo "Warning! Trajectory specified (${end_traj} ps) is greater than available trajectory of ${current_xtc_time}"
	    echo "If you do not have the correct trajectory, it may skew your analysis!"
	    echo "Stopping here:"
	    echo "${path_sims}"
	    echo "Please check your trajectory time!"
	    echo "Pausing for 3 seconds"
	    echo "--------------------------------------------"
	    sleep 3
	    exit
	fi

	## GETTING TRAJECTORY TIMES
	if [[ "${want_grid_from_end}" == true ]]; then
	    echo "Computing WC interface from the end"
	    ##  COMPUTING INITIAL TRAJ
	    begin_traj_grid=$(awk -v current_time=${end_traj} \
	                       -v end_traj=${end_traj_grid} 'BEGIN{ printf "%d", current_time - end_traj }')

	    ## FINDING END TRAJ
	    end_traj_grid=$(awk -v current_time=${end_traj} \
	                        'BEGIN{ printf "%d", current_time }')
	    echo "New traj times: ${begin_traj_grid} - ${end_traj_grid} ps"
	fi

	## GOING INTO OUTPUT PATH
	cd "${output_path}"

	#############################################
	### CREATING FILES NECESSARY FOR ANALYSIS ###
	#############################################

	## SCRIPT LOCATION
	scripts_folder="scripts"
	module_folder=${scripts_folder}/modules

	## DEFINING CODE TO GENERATE GNP FILES
	bash_generate_pdb_files="generate_gnp_pdb_files.sh"

	## CREATING FILES
	mkdir -p "${module_folder}"

	## COPYING OVER SCRIPTS
	echo "Copying GNP descriptors!"
	cp -r "${GNPDESCRIPTOR}" "${module_folder}"
	echo "Copying MDDescriptors!"
	cp -r "${MDDESCRIPTOR}" "${module_folder}"
	echo "Copying MDBuilders!"
	cp -r "${MDBUILDER}" "${module_folder}"
	echo "Copying hydration maps!"
	cp -r "${hydration_scripts}"/{${bash_grid_code},${gridrc},${bash_generate_pdb_files}} "${scripts_folder}"
	## COPYING EXTRACTION CODE
	cp -r "${hydration_scripts}/${bash_hydration_file_name}" "${output_path}"

	## DEFINING RELATIVE GRIC RC
	# relative_gridrc_path="${scripts_folder}/$(basename ${PATH_GRID_SCRIPTS})/${gridrc}"

	######################################
	### EDITING FILES BASED ON DETAILS ###
	######################################

	## DEFINING RELATIVE SIM PATH
	relative_sim_path="."

	## RELATIVE PATH TO GRIC RC
	relative_gridrc_path="${output_file}/${scripts_folder}/${gridrc}"

	## DEFINING PYTHON FUNC
	python_func="python"

	## USING SED TO EDIT DETAILS FOR BASH SCRIPT
	sed -i "s/_MESH_/${mesh}/g" "${bash_hydration_file_name}"
	sed -i "s/_NPROCS_/${n_procs}/g" "${bash_hydration_file_name}"
	sed -i "s/_NGRIDPROCS_/${grid_n_procs}/g" "${bash_hydration_file_name}"
	sed -i "s/_WANTPLANAR_/${planar_sam}/g" "${bash_hydration_file_name}"
	sed -i "s/_GROFILE_/${input_gro_file}/g" "${bash_hydration_file_name}"
	sed -i "s/_XTCFILE_/${input_xtc_file}/g" "${bash_hydration_file_name}"
	sed -i "s/_TPRFILE_/${input_tpr_file}/g" "${bash_hydration_file_name}"
	sed -i "s#_OUTPUTFILE_#${output_file}#g" "${bash_hydration_file_name}"
	sed -i "s#_OUTPUTPREFIX_#${output_prefix}#g" "${bash_hydration_file_name}"

	## EDITING BUFFER
	sed -i "s/_BUFFER_/${buffer}/g" "${bash_hydration_file_name}"

	## DEFINING RELATIVE SIM PATH
	sed -i "s#_RELATIVE_SIM_PATH_#${relative_sim_path}#g" "${bash_hydration_file_name}"

	## EDITING PYTHON
	sed -i "s#_PYTHONFUNC_#${python_func}#g" "${bash_hydration_file_name}"

	## EDITING BASH GRID
	sed -i "s#_BASHGRID_#${bash_grid_code}#g" "${bash_hydration_file_name}"
	sed -i "s#_BEGINTRAJGRID_#${begin_traj_grid}#g" "${bash_hydration_file_name}"
	sed -i "s#_ENDGRIDTRAJ_#${end_traj_grid}#g" "${bash_hydration_file_name}"
	sed -i "s#_PATHTOGRIDRC_#${relative_gridrc_path}#g" "${bash_hydration_file_name}"
	sed -i "s#_GRIDDIR_#${grid_folder}#g" "${bash_hydration_file_name}"
	sed -i "s#_REWRITEWCINTERFACE_#${rewrite_wc_interface}#g" "${bash_hydration_file_name}"

	## DEFINING REWRITE
	sed -i "s#_REWRITE_#${rewrite_wc_interface}#g" "${bash_hydration_file_name}"
	sed -i "s#_INDEXFILE_#${index_file}#g" "${bash_hydration_file_name}"

	sed -i "s#_BEGINTRAJ_#${begin_traj}#g" "${bash_hydration_file_name}"
	sed -i "s#_ENDTRAJ_#${end_traj}#g" "${bash_hydration_file_name}"

	## SAMPLING TIME
	sed -i "s#_WANTSAMPLINGTIME_#${want_wc_sampling_time}#g" "${bash_hydration_file_name}"

	## DEFINING ANALYSIS DIRECTORY
	sed -i "s#_ANALYSISDIR_#${output_file}#g" "${bash_hydration_file_name}"

	## RESIDUE LIST
	sed -i "s#_RESIDUELIST_#${residue_list}#g" "${bash_hydration_file_name}"
	sed -i "s#_CUTOFFRADIUS_#${cutoff_radius}#g" "${bash_hydration_file_name}"
	sed -i "s#_FRAMERATE_#${frame_rate}#g" "${bash_hydration_file_name}"

	## CONTOUR LEVEL AND ALPHA
	sed -i "s#_CONTOUR_#${contour}#g" "${bash_hydration_file_name}"
	sed -i "s#_SIGMA_#${sigma}#g" "${bash_hydration_file_name}"

	## WC INTEFACE ONLY
	sed -i "s#_WANTWCONLY_#${want_wc_only}#g" "${bash_hydration_file_name}"

	## PURE WATER
	sed -i "s#_WANTPUREWATER_#${pure_water_sim}#g" "${bash_hydration_file_name}"

	####################################
	### GENERATING SUBMISSION SCRIPT ###
	####################################
	## COPYING FILE
	cp -r "${path_input_submission}" "${output_submit}"

	## DEFINING JOB NAME
	job_name="$(basename ${input_sim_path})_${folder_within_sim_to_analyze}_${output_file}"
	## EDITING FILE
	sed -i "s#_JOBNAME_#${job_name}#g" "${output_submit}"
	sed -i "s#_BASHSCRIPT_#${bash_hydration_file_name}#g" "${output_submit}"

	## ADDING TO JOB LIST
    echo "${output_path}/${output_submit}" >> "${JOB_SCRIPT}"

else
	echo "${output_path} exists!"

fi