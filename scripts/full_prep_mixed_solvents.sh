#!/bin/bash

# full_prep_mixed_solvents.sh
# This script runs prep_mixed_solvents for multiple solvent systems. 

# Written by Alex K. Chew (08/30/2019)

## USAGE:
#   bash full_prep_mixed_solvents.sh

#####################################
### SOURCING ALL GLOBAL FUNCTIONS ###
#####################################
## DEFINING MAIN DIRECTORY
main_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
## DEFINING GLOBAL VARS NAME
global_file_name="nanoparticle_rc.sh"
## SOURCING GLOBAL VARIABLES
source "${main_dir}/../bin/${global_file_name}"


### DEFINING PATH TO SCRIPT
SCRIPT_PATH="$PATH2SCRIPTS/prep_mixed_solvent.sh"

###########################################
### USER-DEFINED VARIABLES 
###########################################

## DEFINING SOLVENTS
declare -a solvent_names=("toluene") #  
# "formicacid" "formamide" "dimethylsulfide" "phenol" "toluene" "indole" "methanethiol"
# "methanol"
# "tetrahydrofuran" "dmso" "dioxane" "methanol"

## DEFINING MASS FRACTION
declare -a cosolvent_mass_fracs=("2" "5" "7") # "50" # "10"
# 50
## DEFINING SIZES
declare -a box_sizes=("7") # "6" "6.5" "7"

## LOOPING THROUGH EACH SOLVENT
for each_solvent in "${solvent_names[@]}"; do
    ## LOOPING THROUGH EACH MASS FRACTION
    for each_mass_frac in "${cosolvent_mass_fracs[@]}"; do
        ## LOOPING THROUGH EACH BOX SIZE
        for each_box_size in "${box_sizes[@]}"; do
        
            ## PRINTING
            echo "RUNNING THE FOLLOWING COMMAND:"
            echo "bash "${SCRIPT_PATH}" "${each_solvent}" "${each_mass_frac}" "${each_box_size}""
            ## RUNNING
            bash "${SCRIPT_PATH}" "${each_solvent}" "${each_mass_frac}" "${each_box_size}"
            
        done
    done
done


### PRINTING SUMMARY
echo "------SUMMARY------"
echo "Created systems for solvents: ${solvent_names[@]}"
echo "Cosolvent mass fractions: ${cosolvent_mass_fracs[@]}"
echo "Box sizes (nm): ${box_sizes[@]}"