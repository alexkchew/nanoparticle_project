#!/bin/bash

# prep_lm_NP_unbiased_sims.sh
# The purpose of this code is to run unbiased at specified locations. 
# We will base the unbiased simulations from umbrella sampling simulations, though the configurations may be taken directly from a pulling simulation. 
# 
# INPUTS:
#   - configurations (gro)
#   - topology files
#   - MDP files
# OUTPUTS:
#   - run file that runs energy minimization, NPT equilibration, and NPT production runs
#   - new directory

# Written by: Alex K. Chew (alexkchew@gmail.com, 01/20/2020)

#################################################
### LOADING PRE-DEFINED VARIABLES / FUNCTIONS ###
#################################################
source "../bin/nanoparticle_rc.sh"
print_script_name

#######################
### INPUT VARIABLES ###
#######################

### FORWARD SIMULATIONS
## DEFINING INPUT SIMULATION DIR
input_sim_dir_name="20200613-US_otherligs_z-com"
# "20200120-US-sims_NPLM_rerun_stampede"

# "20200116-US_sims_NPLM_with_equil"

## DEFINING OUTPUT SIM DIRECTORY
output_sim_dir_name="20200720-unbiased_after_US_1.9_Benzene"
# "20200414-Unbiased_at_3.500nm_C1_C10"
# "20200407-unbiased_ROT001_rev"
# "20200330-unbiased_C12_at_minimum"
# "20200120-Unbiased_NPLM_correct_mdp"

## DEFINING INPUT DIRECTORY
input_directory="US-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"
# "US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
# input_directory="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
# input_directory="US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
# "US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
# "US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"
# "US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT012_1"
# "US-1.3_5_0.2-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT001_1"

## BENZENE, AFTER FF MODIFICATIONS
input_sim_dir_name="20200818-Bn_US_modifiedFF"
input_directory="US-NPLM-DOPC_196-300.00-5_0.0005_2000-EAM_2_ROT017_1"
output_sim_dir_name="20200818-Bn_US_modifiedFF_UNBIASED_AFTER_US"

## USING THE REVERSE SIMULATIONS

# ## FOR ROT012
# ## DEFINING INPUT SIMULATION DIRECTORY
# input_sim_dir_name="20200124-US-sims_NPLM_reverse_stampede"
# ## DEFINING SPECIFIC DIRECTORY
# input_directory="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT012_1"


# ## FOR ROT001 (REVERSE)
# input_sim_dir_name="20200207-US-sims_NPLM_reverse_ROT001"
# input_directory="USrev-1.5_5_0.2-pullnplm-1.300_5.000-0.0005_2000-DOPC_196-EAM_2_ROT001_1"



## DEFINING SPRING CONSTANT
spring_constant="2000"

## DEFINING INPUT DISTANCE
declare -a distance_array=("1.900")
# "3.500"
# "4.900"
# "5.100"
# "5.300"
# "1.700"
# "2.100" "2.300"
# "1.900"
# "1.300"
# "1.900" "2.100"

## LOOPING DISTANCES
for desired_distance in ${distance_array[@]}; do

## DEFINING PULL GROUP
pull_group_1="AUNP"
pull_group_2="DOPC" # NEED TO UPDATE WITH RESIDUE NAME IN FUTURE

## DEFINING TEMPERATURE
temp="300.00"

## DEFINING DEBUG
want_debug=false
# true

########################
### MDP FILE DETAILS ###
########################

## DEFINING MDP FILE
mdp_em="em.mdp"
mdp_equil="umbrella_sampling_equil.mdp"
mdp_prod="unbiased_prod.mdp"

## DEFINING MDP DETAILS
mdp_dt="0.002"

## NUMBER OF STEPS
if [[ ${want_debug} == true ]]; then
    mdp_equil_nsteps="1000" # 500 ps
    mdp_prod_nsteps="1000" # 50 ns production
else
    mdp_equil_nsteps="250000" # 500 ps
    mdp_prod_nsteps="25000000" # 50 ns production
fi

if [[ ${input_directory} = "USrev"* ]]; then
	want_reverse=true
else
	want_reverse=false
fi

#######################
### SUBMISSION FILE ###
#######################

## DEFINING INPUT SUBMISSION
input_submit_file="submit_run_np_lipid_bilayer_equil_and_pull.sh"
output_submit_file="submit.sh"

## DEFINING INPUT RUN FILE
input_run_file="run_lm_NP_unbiased_sims.sh"
output_run_file="run.sh"

#########################
### DEFAULT VARIABLES ###
#########################

## FORCE FIELD
forcefield="${FORCEFIELD}"

## DEFINING MAX WARN
maxwarn="5"

## RUN INFORMATION
num_cores="28"

## DEFINING EM NUMBER OF CORES
em_num_cores="28"

## DEFINING MD RUN COMMAND
mdrun_command="gmx mdrun -nt"

## DEFINING SUFFIX
mdrun_command_suffix=""

####################
### OUTPUT NAMES ###
####################

## DEFINING PREFIX
if [[ "${want_reverse}" == true ]]; then
	## GETTING NEW NAME
	truncated_name=$(sed "s/.*USrev//g" <<< ${input_directory})
	prefix="NPLM_rev_unb"
else
	prefix="NPLM_unb"
	## GETTING NEW NAME
	truncated_name=$(sed "s/.*NPLM//g" <<< ${input_directory})
fi


## DEFINING OUTPUT NAME
output_directory="${prefix}-${desired_distance}_${spring_constant}${truncated_name}"
echo "Output name: ${output_directory}"

## DEFINING OUTPUT NAME
output_prefix="nplm"

######################
### DEFINING PATHS ###
######################

## DEFINING INPUT DIRECTORY
path_input_dir="${PATH2NPLMSIMS}/${input_sim_dir_name}/${input_directory}"

## DEFINING OUTPUT DIR
path_output_dir="${PATH2NPLMSIMS}/${output_sim_dir_name}/${output_directory}"

## CHECKING IF EXISTS
stop_if_does_not_exist "${path_input_dir}"

## MDP FILE INFORMATION
path_mdp_parent="${INPUT_MDP_PATH}/charmm36"
path_mdp_dir="${path_mdp_parent}/lipid_bilayers"

####################################
### DEFINING DIRECTORY STRUCTURE ###
####################################

## FOR INPUT CONFIGS INITIAL
path_input_dir_input_files="${path_input_dir}/1_input_files"
path_input_dir_configs="${path_input_dir}/2_configurations"
path_input_dir_sims="${path_input_dir}/4_simulations"

### DEFINING GRO FILES
#config_gro="${desired_distance}.gro"
#
### DEFINING PATH TO GRO
#path_gro="${path_input_dir_configs}/${config_gro}"

## DEFINING PREFIX
input_prefix="nplm_prod"

## DEFINING GRO FILES
config_gro="${input_prefix}.gro"

## DEFINING PATH TO GRO
path_gro="${path_input_dir_sims}/${desired_distance}/${config_gro}"

## DEFINING TOP FILE
input_top="nplm.top"
## DEFINING TOPPAR
toppar_file="toppar"

###################
### MAIN SCRIPT ###
###################

## MAKING DIRECTORY
create_dir "${path_output_dir}" -f

## GOING TO DIRECTORY
cd "${path_output_dir}"

## COPYING OVER GRO FILE
cp -r "${path_gro}" "${output_prefix}.gro"

## CHECKING FORCEFIELD
if [ ! -e "${path_input_dir_input_files}/${forcefield}" ]; then
	echo "${forcefield} does not exist. Switching to modified forcefield"
	forcefield="${MODIFIED_FF}"
	echo "Forcefield: ${forcefield}"
	sleep 1
fi

## COPYING FORCE FIELD
cp -r "${path_input_dir_input_files}/"{${forcefield},${toppar_file}} "${path_output_dir}"
## COPYING ITP AND PRM
cp -r "${path_input_dir_input_files}/"{*.itp,*.prm} "${path_output_dir}"
## COPYING TOP FILE
cp -r "${path_input_dir_input_files}/${input_top}" "${path_output_dir}/${output_prefix}.top"
## COPYING MDP FILE
cp -r "${path_mdp_dir}"/{${mdp_em},${mdp_equil},${mdp_prod}} "${path_output_dir}"


########################
### EDITING MDP FILE ###
########################

## EDITING GROUPS IN EQUILIBRATION
sed -i "s#_GROUP_1_NAME_#${pull_group_1}#g" "${mdp_equil}"
sed -i "s#_GROUP_2_NAME_#${pull_group_2}#g" "${mdp_equil}"
sed -i "s#_DT_#${mdp_dt}#g" "${mdp_equil}"
sed -i "s#_NSTEPS_#${mdp_equil_nsteps}#g" "${mdp_equil}"
sed -i "s#_TEMPERATURE_#${temp}#g" "${mdp_equil}"
sed -i "s#_PULLKCONSTANT_#${spring_constant}#g" "${mdp_equil}"
sed -i "s#_INITIALDIST_#${desired_distance}#g" "${mdp_equil}"

## EDITING PRODUCTION
sed -i "s#_DT_#${mdp_dt}#g" "${mdp_prod}"
sed -i "s#_NSTEPS_#${mdp_prod_nsteps}#g" "${mdp_prod}"
sed -i "s#_TEMPERATURE_#${temp}#g" "${mdp_prod}"

###############################
### EDITING SUBMISSION FILE ###
###############################

## COPYING RUN FILE
cp "${PATH2SUBMISSION}/${input_run_file}" "${output_run_file}"
## COPYING SUBMISSION FILE
cp "${PATH2SUBMISSION}/${input_submit_file}" "${output_submit_file}"

## EDITING RUN FILE
sed -i "s#_INPUTTOPFILE_#${output_prefix}.top#g" "${output_run_file}"
sed -i "s#_MAXWARN_#${maxwarn}#g" "${output_run_file}"
sed -i "s#_EM_MDPFILE_#${mdp_em}#g" "${output_run_file}"
sed -i "s#_EQUIL_MDPFILE_#${mdp_equil}#g" "${output_run_file}"
sed -i "s#_PROD_MDPFILE_#${mdp_prod}#g" "${output_run_file}"
sed -i "s#_OUTPUTPREFIX_#${output_prefix}#g" "${output_run_file}"

## EDITING SUBMISSION FILE
sed -i "s/_NUMBER_OF_CORES_/${num_cores}/g" "${output_submit_file}"
sed -i "s/_JOB_NAME_/${output_directory}/g" "${output_submit_file}"
sed -i "s/_EMNUMCORES_/${em_num_cores}/g" "${output_submit_file}"
sed -i "s#_MDRUNCOMMAND_#${mdrun_command}#g" "${output_submit_file}"
sed -i "s#_MDRUNCOMMANDSUFFIX_#${mdrun_command_suffix}#g" "${output_submit_file}"
sed -i "s#_RUNSCRIPT_#${output_run_file}#g" "${output_submit_file}"

##########################
### ADDING TO JOB LIST ###
##########################

## PRINTING
echo "${path_output_dir}/${output_submit_file}" >> "${JOB_SCRIPT}"

## ADDING SUMMARY
summary_file="transfer_summary.txt"
echo "Input directory: ${input_directory}" > "${summary_file}"
echo "Spring constant: ${spring_constant}" >> "${summary_file}"
echo "Desired distance: ${desired_distance}" >> "${summary_file}"
echo "Temperature: ${temp}" >> "${summary_file}"
echo "-----------------------------" >> "${summary_file}"
echo "Pull group 1: ${pull_group_1}" >> "${summary_file}"
echo "Pull group 2: ${pull_group_2}" >> "${summary_file}"
echo "NPT equil steps: ${mdp_equil_nsteps}" >> "${summary_file}"
echo "NPT prod steps: ${mdp_prod_nsteps}" >> "${summary_file}"
## PRINTING
cat "${summary_file}"

done