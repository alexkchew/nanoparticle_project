#!/bin/bash

# nanoparticle_rc.sh
# This contains all functions / global variables you want to present to a script
# Created by: Alex K. Chew (12/15/2017)

### GLOBAL VARIABLES
#   CURRENTWD: Main working directory
#   PATH2SCRIPTS: Paths to script folder
#   PATH2BIN: Path to bin folder

## PRINTING
echo "-------- Reloading nanoparticle_rc.sh --------"

## DEFINING USER
NETID="akchew"

### MAIN DIRECTORIES
CURRENTWD="${HOME}/scratch/nanoparticle_project" # Current working directory

## CORRECTING FOR STAMPEDE
if [[ $(echo $HOSTNAME) == *"stampede"* ]]; then # Check if we are at the CHTC Supercomputers
    CURRENTWD="${WORK}/scratch/nanoparticle_project"
fi

## DEFINING BIN PATH
PATH2BIN="${CURRENTWD}/bin" # Where all ligandbuilder important global variables are stored

## MDLIGANDS RC
MDLigands_rc="${HOME}/scratch/MDLigands/scripts/global_funcs.sh"
## SOURCING MDLIGANDS
source "${MDLigands_rc}"

### LOADING GLOBAL VARIABLES
source "${PATH2BIN}/nanoparticle_global_vars.sh"

### LOADING GLOBAL GENERAL FUNCTIONS
source "${PATH2BIN}/nanoparticle_functions.sh"

### LOADING GLOBAL FUNCTIONS
source "${HOME}/bin/bashfiles/server_general_research_functions.sh"

## LOADING HOME FUNCTIONS
source "${HOME}/bin/bashfiles/server_bashrc.sh"

### LOADING UMBRELLA SAMPLING FUNCTIONS
source "${HOME}/bin/bashfiles/server_umbrella_sampling.sh"

#### PYTHON PATHS
## MDBUILDER
export PYTHONPATH="${HOME}/bin/pythonfiles/modules:$PYTHONPATH"

