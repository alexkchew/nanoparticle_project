#!/bin/bash

# nanoparticle_global_vars.sh
# This contains all of nanoparticles's global variables including current working directory, etc.
# Created by: Alex K. Chew (12/15/2017)

### GLOBAL VARIABLES DEFINED IN RC
#   CURRENTWD: Main working directory
#   PATH2BIN: Path to bin folder

#################################
### DEFINING GLOBAL VARIABLES ###
#################################
### MAIN DIRECTORY PATHS
#   PATH2SCRIPTS: Paths to script folder
#   PATH2PREP: Path to preparation files
#   PATH2PREPSYS: Path to preparation systems
#   PATH2ANALYSIS: Path to analysis folder
#   PATH2SIM: Path to simulations

### SUB DIRECTORIES
## PREPARATION FILE
#   INPUT_DIR: where all the input files are stored
#   INPUT_LIG_DIR: where all the ligand information is stored
#   INPUT_PRO_DIR: where all proteins are stored
#   PATH2SUBMISSION: path to submit.sh files

## MDP FILE PATH
#	PATH_MDP_CHARMM36_PLANAR: mdp charmm36 planar case
#	PATH_MDP_CHARMM36_SPHERICAL: mdp charmm36 spherical case
#	

# 	MODIFIED_FF: modified force field name

## PREP_LIGAND FILES
#   PREP_LIGAND_MAIN_DIR: main directory for preparation ligands
#   PREP_LIGAND_INPUT_DIR: input directory for preparation ligands
#   PREP_LIGAND_INPUT_TOP: input topology directory for preparation ligands
#   PREP_LIGAND_INPUT_MDP: input mdp files for preparation ligands
#   PREP_LIGAND_SIM_DIR: simulation directory for energy minimization
#   PREP_LIGAND_INPUT_MOLECULES: input ligand molecules (within prep_files)
#   PREP_LIGAND_FINAL: directory where all final ligands are placed

## PREP_GOLD FILES
#   PREP_GOLD_MAIN_DIR: main directory for preparation of gold core
#   PREP_GOLD_INPUT_FILE: contains input files for gold self assembly
#   PREP_GOLD_MDP_DIR: contains mdp files for gold self assembly
#   PREP_GOLD_EAM_DIR: main directory for EAM potentials
#   PREP_GOLD_EAM_FINAL_STRUCTURES: final structure for EAM potentials
#   PREP_GOLD_SIM: self assembly simulation folder

## SCRIPTS
#   MDBUILDER: Location of MDBuilder
#   PATH2POST_SCRIPTS: path to post extraction scripts

## IMPORTANT FILES
#   PREP_LIG_FILE: shows all ligand names and residue names
#   JOB_SCRIPT: Job lists

## IMPORTANT SCRIPTS:
#   PREP_LIG_REFRESH_FILE: refreshes the ligand names
#	PYTHON_MODIFY_FF: python script that modifies the force field parameters

## PRINTING
echo "*** LOADING NANOPARTICLE GLOBAL VARIABLES (nanoparticle_global_vars.sh) ***"

#### GLOBAL VARIABLES ####

### CHECKING MAIN DIRECTORY
#if [[ $(hostname) == *"smic"* ]]; then # Check if we are in the smic server
#    CURRENTWD="/work/achew/scratch/nanoparticle_project"
#elif [[ $(hostname) == *"stampede"* ]]; then # Check if we are in the stampede server
#    CURRENTWD="${WorkFolder}/scratch/nanoparticle_project"
#fi
#################################
### DEFINING MAIN DIRECTORIES ###
#################################

PATH2SCRIPTS="${CURRENTWD}/scripts" # Where all script files are stored
PATH2PREP="${CURRENTWD}/prep_files" # Where all preparation files are stored
PATH2PREPSYS="${CURRENTWD}/prep_system" # Where all preparation systems are stored
PATH2ANALYSIS="${CURRENTWD}/analysis" # Where all analysis is done
PATH2SIM="${CURRENTWD}/simulations" # Where all simulations are stored
PATH2DATABASE="${CURRENTWD}/database" # Where all databases are stored

## DEFINING PATH TO SIM FOR NPLM
PATH2NPLMSIMS="${CURRENTWD}/nplm_sims"

## DEFINING LIPID MEMBRANE SCRIPTS
PATH_LIPID_MEMBRANE_SCRIPTS="${PATH2SCRIPTS}/lipid_membrane_scripts"
PATH_LIPID_MEMBRANE_SCRIPTS_EXTRACT="${PATH_LIPID_MEMBRANE_SCRIPTS}/extract_scripts"

################################
### DEFINING SUB DIRECTORIES ###
################################

## PREPARATION FILES
INPUT_DIR="${PATH2PREP}/input_files" # Where all input files are located
INPUT_LIG_DIR="${PATH2PREP}/ligands" # Where all the ligands are stored
INPUT_PRO_DIR="${PATH2PREP}/proteins" # Where all the proteins are stored
INPUT_FRAG_LIG_PREP="${PATH2PREP}/fragment_based_ligands"
PATH2SUBMISSION="${INPUT_DIR}/submission" # Where all submission scripts (*.sh) files are stored
INPUT_LIPID_MEMBRANE="${PATH2PREP}/lipid_bilayers" # Where all lipid membrane models are
INPUT_MDP_PATH="${INPUT_DIR}/MDP_FILES"

## DEFINING GENERALRC
GENERAL_RC_NAME="general_rc.sh"
PATH_GENERAL_RC="${PATH2SUBMISSION}/${GENERAL_RC_NAME}" # Where all submission scripts (*.sh) files are stored


## PATH TO MDP CHARMM
PATH_MDP_CHARMM36="${INPUT_MDP_PATH}/charmm36"
PATH_MDP_CHARMM36_PLANAR="${PATH_MDP_CHARMM36}/Planar"
PATH_MDP_CHARMM36_SPHERICAL="${PATH_MDP_CHARMM36}/Spherical"

## DEFINING TOPOLOGY FILE
INPUT_TOPOLOGY_PATH="${INPUT_DIR}/topology"

## SCRIPTS
PATH2SUBMITSCRIPT="$PATH2SCRIPTS/submit_scripts" # Where all switch submission scripts are stored
PATH2POST_SCRIPTS="${PATH2SCRIPTS}/post_extraction_scripts" # Where all post extraction scripts are stored

## PREP_LIGAND FILES (LIGAND_PREP)
PREP_LIGAND_INPUT_MOLECULES="${PATH2PREP}/ligands" # Directory where all completed structures are
PREP_LIGAND_MAIN_DIR="${PATH2PREPSYS}/prep_ligand" # Directory where all energy minimizations take place
PREP_LIGAND_INPUT_DIR="${PREP_LIGAND_MAIN_DIR}/input_files" # Directory where all completed structures are
PREP_LIGAND_INPUT_TOP="${PREP_LIGAND_INPUT_DIR}/topology" # Directory where all topology files exist
PREP_LIGAND_INPUT_MDP="${PREP_LIGAND_INPUT_DIR}/mdp" # Directory where all mdp files exist
PREP_LIGAND_SIM_DIR="${PREP_LIGAND_MAIN_DIR}/minimized_structures" # Directory where 

## PREP LIPID MEMBRANE FILES
PREP_LIPID_MEMBRANE="${PATH2PREPSYS}/lipid_bilayers"

## PREP_GOLD FILES
PREP_GOLD_MAIN_DIR="${PATH2PREPSYS}/prep_gold" # Path to main directory for gold
# INPUT FILES AND MDP DIRECTORY
PREP_GOLD_INPUT_FILE="${PREP_GOLD_MAIN_DIR}/input_files"
PREP_GOLD_MDP_DIR="${PREP_GOLD_INPUT_FILE}/MDP_FILES"
# SURFACANTS
PREP_GOLD_SURF="${PREP_GOLD_MAIN_DIR}/self_assembly_surfactants"
PREP_GOLD_SURF_NONFRAG="${PREP_GOLD_MAIN_DIR}/self_assembly_surfactants_nonfrag"
# EMBEDDED ATOM POTENTIAL VARIABLES
PREP_GOLD_EAM_DIR="${PREP_GOLD_MAIN_DIR}/vcsgc_simulations"
PREP_GOLD_EAM_FINAL_STRUCTURES="${PREP_GOLD_EAM_DIR}/final_structures"
# SIMULATIONS FOLDER
PREP_GOLD_SIM="${PREP_GOLD_MAIN_DIR}/self_assembly_simulations"
PREP_GOLD_SIM_NONFRAG="${PREP_GOLD_MAIN_DIR}/self_assembly_simulations_nonfrag"


## WITHIN SCRIPTS
# MDBUILDER="${PATH2SCRIPTS}/MDBuilder"
MDBUILDER="${HOME}/bin/pythonfiles/modules/MDBuilder"
MDDESCRIPTOR="${HOME}/bin/pythonfiles/modules/MDDescriptors"
GNPDESCRIPTOR="${HOME}/bin/pythonfiles/modules/gnpdescriptors"
JOB_SCRIPT="${PATH2SCRIPTS}/job_list.txt"

## KEY FILES
PREP_LIG_FILE="${PREP_LIGAND_MAIN_DIR}/lig_names.txt" # file to store all ligand names

## KEY SCRIPTS

## DEFINING BUNDLING DIR
BUNDLING_SCRIPT_DIR="${PATH2SCRIPTS}/bundling_scripts"
PREP_LIG_REFRESH_FILE="${PREP_LIGAND_MAIN_DIR}/lig_names_refresh.sh"
NP_MOST_LIKELY_CODE="${MDDESCRIPTOR}/application/nanoparticle/np_most_likely_config.py"
## SCRIPT TO COUNT GRO FILE
PY_COUNT_GRO_FILE="${MDBUILDER}/core/count_gro_residues.py"

## DEFINING FORCEFIELD
PATH_FORCEFIELD="${MDLIG_FF_DIR}"
FORCEFIELD="charmm36-jul2017.ff"
MODIFIED_FF="charmm36-jul2017-mod.ff"

# charmm36-nov2016.ff
# all completed structures are stored here
# PREP_LIGAND_FINAL="${PREP_LIGAND_MAIN_DIR}/final_ligands"
PREP_LIGAND_FINAL="${MDLIG_FINAL_LIGS}/${FORCEFIELD}"

## DEFINING LIGAND NAMES
LIGAND_NAMES_TXT="${PREP_LIGAND_FINAL}/${LIG_NAMES_TXT}"

## DEFINING SOLVENT
PREP_SOLVENT_FINAL="${MDLIG_SOLVENTS_FINAL}/${FORCEFIELD}"
NP_PREP_SOLVENT_EQUIL="${PATH2PREP}/solvents"

## DEFINING INPUTS FOR PACKMOL
INPUT_PACKMOL_PATH="${INPUT_DIR}/packmol"
INPUT_PACKMOL_WATER_PDB="${INPUT_PACKMOL_PATH}/water.pdb"

## DEFINING PATH TO PACKMOL
PATH_PACKMOL="${HOME}/packmol"
PACKMOL_SCRIPT="${PATH_PACKMOL}/packmol"

## DEFINING VDW RADIUS
VDW_RADII_LIST="${INPUT_PACKMOL_PATH}/vdwradii.dat"

## DEFINING MIXED SOLVENT SYSTEMS
PREP_MIXED_SOLVENTS="${PATH2PREPSYS}/prep_mixed_solvents" # Path to main directory for gold

## MIXED SOLVENTS WITH ENERGY GROUPS
PREP_MIXED_SOLVENTS_ENGRPS="${PATH2PREPSYS}/prep_mixed_solvents_engrps" # Path to main directory for gold
PREP_MIXED_SOLVENTS_MULTIPLE="${PATH2PREPSYS}/prep_mixed_solvents_multiple" # Path to main directory for gold

## DEFINING REFERENCE FILE TO MOLECULAR WEIGHTS
MOLECULAR_WEIGHT_REF="${NP_PREP_SOLVENT_EQUIL}/molecular_weights_ref.txt"
MOLECULAR_WEIGHT_FILE="${NP_PREP_SOLVENT_EQUIL}/molecular_weights_list.txt"
MOLECULAR_VOLUME_FILE="${NP_PREP_SOLVENT_EQUIL}/molecular_volumes_list.txt"
MOLECULAR_GMX_FREEVOL="${NP_PREP_SOLVENT_EQUIL}/molecular_gmx_freevolume_list.txt"

## DEFINING GROMACS SCRIPTS
GROMACS_SCRIPTS="${HOME}/bin/bashfiles/gromacs_scripts"

## DEFINING FUNCTION THAT COULD EXTRACT SOLVENT DETAILS
BASH_EXTRACT_SOLVENT_DETAILS="${GROMACS_SCRIPTS}/extract_solvent_details.sh"
BASH_FULL_EXTRACT_SOLVENT_DETAILS="${GROMACS_SCRIPTS}/full_extract_solvent_details.sh"
BASH_EXTRACT_SOLVENT_NP_DEFAULTS="${GROMACS_SCRIPTS}/extract_solvent_details_defaults_np.sh"

## DEFINING SCRIPT TO FULLY ANALYZE ALL SOLVENTS
BASH_REFRESH_SOLVENTS="${PATH2SCRIPTS}/solvent_scripts/full_refresh_solvent_info.sh"

## SCRIPT TO RUN MOST LIKELY
BASH_MOST_LIKELY_SCRIPT="${BUNDLING_SCRIPT_DIR}/compute_most_likely.sh"

## DEFINING SOLVENT INFO DETAILS
MULTIPLE_SOLVENT_INFO_TXT="${PREP_MIXED_SOLVENTS_MULTIPLE}/solvent_info.txt"

## DEFINING GRIDDING FUNCTION
HYDROPHOBICITY_PROJECT="/home/shared/np_hydrophobicity_project"
HYDROPHOBICITY_RUN_SCRIPTS="${HYDROPHOBICITY_PROJECT}/run_scripts"
GRID_BASH_SCRIPT="${HYDROPHOBICITY_RUN_SCRIPTS}/generate_grid.sh"

## PYTHON SCRIPTS
PYTHON_GET_GRO_COM="${MDBUILDER}/core/compute_gro_center_of_mass.py"

## DEFINING WHAM SCRIPT
WHAM_SCRIPT="${HOME}/wham/wham/wham/wham"

## PYTHON SCRIPT TO COSOLVENTMAP
PYTHON_COSOLVENT_MAPPING="${MDDESCRIPTOR}/application/nanoparticle/nanoparticle_cosolvent_surface_mapping.py"
EXTRACT_COSOLVENT_MAPPING="${PATH2POST_SCRIPTS}/extract_cosolvent_map.sh"

## PYTHON SCRIPT TO LIPID MEMBRANEGROUPS
PYTHON_GET_LM_TOP_BOT_INDEX="${MDDESCRIPTOR}/application/np_lipid_bilayer/divide_lipid_membrane_leaflets.py"
PYTHON_GET_NPLM_INDEX="${MDDESCRIPTOR}/application/np_lipid_bilayer/nplm_generate_index_list.py"
PYTHON_NPLM_GET_SIM_LIST="${MDDESCRIPTOR}/application/np_lipid_bilayer/nplm_generate_list_of_sims.py"

## DEFAULT ATOM NAMES
SULFUR_ATOM_NAME="S1" # head group sulfur atom
GOLD_RESIDUE_NAME="AUNP" # Gold residue name

### DEFAULT SCRIPTS ###
PYTHON_MODIFY_FF="${MDDESCRIPTOR}/application/np_lipid_bilayer/modify_charmm36_ff_parameters.py"
## DEFAULT NBFIX
NBFIX_FILE="nbfix.itp"

## DEFINING DATABASE FOLDER
DATABASE_SELFASSEMBLY_LIGS="${PATH2DATABASE}/self_assembly_ligands.xlsx"
