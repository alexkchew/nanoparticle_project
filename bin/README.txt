bin

This folder contains important bash and other scripts vital for the nanoparticle project. 

Files:
	nanoparticle_functions.sh: contains all important functions
	nanoparticle_global_vars.sh: contains all global variables
	nanoparticle_rc.sh: bash rc equivalent -- needs to be loaded before scripts are run!

Written by: Alex K. Chew
Created on: 05/01/2018