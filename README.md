# nanoparticle_project

A warm welcome to the nanoparticle_project!

In this project, we will develop nanoparticles for various different types of ligands. Furthermore, we will develop tools to quickly generate nanoparticles. This serves as a second verion of LigandBuilder, but with the set goal of quickly creating ligands by using force field generators.

# Directories
- analysis: this is where all the analysis is completed (DEPRECIATED, will remove in later date)
- bin: contains all important bash scripts, which contain global variables
- prep_files: contains all important preparation files, like input files, etc.
- prep_system: contains all preparation systems (e.g. gold core, lipid bilayers, etc.)
- scripts: location for all bash scripts
- simulation: main simulation folder that the scripts usually output to

# Procedure to prepare gold cores
The available gold cores are listed below:
- Spherical gold cores (`spherical`): Spherical gold core cut from a fcc 111 Au lattice. 
- Hollow gold cores (`hollow`): Hollow gold core developed by estimating number of gold atoms using 21.6 A^2/lig, then repulsive simulations to generate the core
- EAM gold cores (a.k.a. Faceted gold cores (FGC), `EAM`): Faceted gold cores computed from VCSGC simulations (uses LAMMPS)


Note that spherical and hollow gold cores do not need any preparation steps. You could just jump right into "self-assembly simulations" to prepare these gold cores. Only the FGC model require a subsequent step to generate optimized gold core structures with LAMMPS. 





# Procedure to run self-assembly simulations
Self-assembly simulations consists of excess butanethiol ligands that are adsorbed onto the gold core. This is typically performed over 30 ns, where the last frame is used for "transfer ligand" simulations. 

The procedure to begin self-assembly simulations is:
- Open `full_prep_self_assembly_gold_ligand.sh` in the `scripts` folder.
- Edit the following variables:
```
## PARTICLE SHAPE
declare -a shape_type=("spherical")
## DIAMETER
declare -a diameter=("7")
## TRIAL NUMBER
declare -a trial_num=("4" "5" "6")
```

The description of each varaible is described below:
- `shape_type`: shape type of the gold core, either "spherical", "hollow", or "EAM"
- `diameter`: diameter in nanometers that you want the gold core to be. We currently have particles between 2-7 nm in integer increments.
- `trial_num`: trial number for the particle. The original work used trial numbers 1, 2, and 3. These represent different iterations of gold core self-assembly simulations. 

- After editing, run the code:
```
bash full_prep_self_assembly_gold_ligand.sh
```

- The code should create folders and files necessary to run each self-assembly simulations. The list of jobs and submission are located in `scripts/job_list.txt`.


To add new gold cores, go within `prep_gold` folder and run the following:
```
git add -f */*/gold_ligand_equil.gro
```
Note that only `gold_ligand_equil.gro` is required for transfer ligands to work, since we only care about the gold atoms and the sulfur positions of the butanethiol ligands.



# Procedure to run transfer ligands (i.e. gold nanoparticles in water)
We denote "transfer ligands" as when we remove butanethiol ligands from a self-assembled simulations, then replace the ligands with our desired ligand. The procedure to do this is:

- Open the `full_transfer_ligand_builder.sh` within `scripts` folder. 
- Edit the following variables:
```
declare -a shape_type=("EAM")
declare -a diameter=("2")
declare -a ligand_names=("ROT017")
declare -a trial_num=("1")
ligand_fracs="1"
simulation_dir="ROT_WATER_SIMS"
```
Description of each variable is shown below:
- `shape_type`: shape type of the gold core, either "spherical", "hollow", or "EAM"
- `diameter`: diameter in nanometers that you want the gold core to be. We currently have particles between 2-7 nm in integer increments.
- `ligand_names`: ligands that you want to replace the butanethiol ligands with. If you want a mixed ligand, include multiple ligands that are separated by commas, e.g. "ROT001,ROT012". You could include as many as you like, just make sure you update `ligand_fracs` to match the same number of ligands. The ligands will be randomly selected for mixed-ligands. 
- `ligand_fracs`: Fraction of ligands per residue. For example, "1" means you have only one ligand and 100% of the surface is covered by that ligand. Alternatively, "0.53,0.47" means you have 53% of ligand 1 and 47% of ligand 2. 
- `simulation_dir`: Output simulation directory name, which will be stored within `simulations` folder.

Note that `shape_type`, `diameter`, `ligand_names`, `trial_num` are all array variables, so feel free to add multiple of these, separated by a space, so you could screen across multiple gold nanoparticles. 

- After editing the variables, run the code:
```
bash full_transfer_ligand_builder.sh
```

- The code should create folders and files necessary to run each gold nanoparticle system. The list of jobs and submission are located in `scripts/job_list.txt`. Simply run each of the submission script to perform a 50 ns gold-nanoparticle-water NPT simulation. 
